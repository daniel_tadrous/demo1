//
//  Almofire+Rx.swift
//  TAMMApp
//
//  Created by kerolos on 4/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

extension Request: ReactiveCompatible {}

extension Reactive where Base: DataRequest {
    
    func responseJSON(errorHandler:@escaping ()->Void = {}) -> Observable<Any> {
        return Observable.create { observer in
            
            let request = self.base.responseJSON { response in
                switch response.result {
                case .success(let value):
                    observer.onNext(value)
                    observer.onCompleted()
                    response.request?.cURL
                case .failure(let error):
                   // observer.onError(error)
                    self.retryRequest(errorHandler: errorHandler,error:error as NSError)
                }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
    func retryRequest(errorHandler:@escaping ()->Void,error:NSError){
        if ErrorView.show{
            let noInternetView = ErrorView(frame: UIScreen.main.bounds,message: L10n.errorMessage,onRetryClickHandler:{
                errorHandler()
            })
            UIApplication.shared.keyWindow!.addSubview(noInternetView)
            UIApplication.shared.keyWindow!.bringSubview(toFront: noInternetView)
            
        }
    }
    
    
    
    
    func responseString() -> Observable<Any> {
        return Observable.create { observer in
            let request = self.base.responseString { response in
                switch response.result {
                case .success(let value):
                    observer.onNext(value)
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
    
}
