//
//  AspectsOfLifeCoordinator.swift
//  TAMMApp
//
//  Created by kerolos on 4/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

protocol AspectsOfLifeCoordinatorDelegate: class {
    func aspectsOfLifeCoordinatorIsDismissed()
}
class AspectsOfLifeCoordinator: NavigationCoordinator {
    
    var navigationController: UINavigationController = UINavigationController()
    var tabbarNavigationController:UINavigationController

    // MARK: - Properties
    let tabBarController:UITabBarController
    let container: Container
    var vc:UIViewController?
    //let authenticationService : AuthenticationService
    var delegate: AspectsOfLifeCoordinatorDelegate?
    
    init(container: Container, aspectsOfLifeNavigationController: UINavigationController, tabBarControlor:UITabBarController) {
        self.container = container
        self.tabbarNavigationController = aspectsOfLifeNavigationController
        self.tabbarNavigationController.setNavigationBarHidden(true, animated: false)
        self.tabBarController = tabBarControlor
        // self.authenticationService = authenticationService
    }

    
    // MARK: - Coordinator core
    
    func start() {
        if(self.vc != nil){
            tabBarController.selectedViewController = self.vc
        }
        else{
            let vc = container.resolveViewController(AspectsOfLifeViewController.self)
            //self.navigationController = vc.navigationController!
            vc.delegate = self
//            vc.viewModel = AolDetailsViewModel()
            //navigationController.pushViewController(vc, animated: true)
            let navigationController = tabBarController.selectedViewController as? UINavigationController
            self.navigationController = navigationController!
            navigationController?.setViewControllers([vc], animated: false)
        
        }
    }
    
    func openAspectOfLifeDetail(id: String , title:String) {
        let parentVC =  container.resolveViewController(AoIDetailsParentViewController.self)
        parentVC.delegate = self
        parentVC.viewModel.title = title
        parentVC.viewModel.id = id

        self.navigationController.pushViewController(parentVC, animated: true)
    }
    func openAolSubTopicsViewController(id: String , title:String) {
        let vc = container.resolveViewController(AolSubTopicsViewController.self)
        vc.viewModel?.topicId.value = id
        vc.viewModel?.topicTitle.value = title
        vc.serviceSelectedDelegate = self
        self.tabbarNavigationController.setNavigationBarHidden(true, animated: false)
        self.navigationController.pushViewController(vc, animated: true)
    }
    func openAolServiceWithFaqsViewController(id: String , title:String) {
        let vc = container.resolveViewController(AolServiceWithFaqsViewController.self)
        vc.viewModel?.serviceId.value = id
        vc.viewModel?.serviceTitle.value = title
        
        self.tabbarNavigationController.setNavigationBarHidden(true, animated: false)
        self.navigationController.pushViewController(vc, animated: true)
    }
   
}

extension AspectsOfLifeCoordinator: AspectsOfLifeViewControllerDelegate{

    
    func openAspectsOfLifeList() {
        let parentVC = self.navigationController.topViewController as? AspectsOfLifeViewController
        let childVC = container.resolveViewController(AspectsOfLifeListViewController.self)
        childVC.delegate = self
        embedViewController(child: childVC, To: parentVC!)
        //childVC.viewModel = MyServicesViewModel()
        //vc.delegate = self
        
    }
    
    func openJourneys() {
        let parentVC = self.navigationController.topViewController as? AspectsOfLifeViewController
        let childVC = container.resolveViewController(JourneysViewController.self)
        embedViewController(child: childVC, To: parentVC!)
        //childVC.viewModel = MyJourenysViewModel()
        //vc.delegate = self
    }
    
    fileprivate func embedViewController(child: UIViewController , To parent:AspectsOfLifeViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    fileprivate func embedViewController(child: UIViewController , To parent:AoIDetailsParentViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
}

extension AspectsOfLifeCoordinator : AoIDetailsParentViewControllerDelegate{
    func openAspectsOfLifeDetails(id: String , title:String) {
        let parentVC = self.navigationController.topViewController as? AoIDetailsParentViewController
        let childVC = container.resolveViewController(AolDetailsViewController.self)
        childVC.viewModel?.aspectId.value = id
        childVC.viewModel?.aspectTitle.value = title
        childVC.delegate = self
        childVC.selectDelegate = self
        self.embedViewController(child: childVC, To: parentVC!)
    }
    
    func openJourneysInDetails(){
        let parentVC = self.navigationController.topViewController as? AoIDetailsParentViewController
        let childVC = container.resolveViewController(JourneysViewController.self)
        embedViewController(child: childVC, To: parentVC!)
    }
    
    
}

extension AspectsOfLifeCoordinator: AolDetailsViewControllerDelegate{
    func aspectOfLifeSubtopicDidFinish() {
        print("will dismiss My Services")
        self.delegate?.aspectsOfLifeCoordinatorIsDismissed()
        
    }
}
extension AspectsOfLifeCoordinator: AspectsOfLifeListViewControllerSelectionDelegate{
    func didSelectAspect(itemId: String, title:String) {
        openAspectOfLifeDetail(id:itemId, title: title)
    }
    
    
}
extension AspectsOfLifeCoordinator: AspectsOfLifeTopicsTableViewDelegate{
    func selectedTopic(topic: AspectOfLifeServiceTopicViewData) {
        openAolSubTopicsViewController(id: topic.serviceTopicId, title: topic.serviceTopicTitle)
    }
}

extension AspectsOfLifeCoordinator: AspectsOfLifeServiceTableViewDelegate{
    func selectedService(service: AspectOfLifeSubTopicServiceViewData, subTopicTitle: String) {
        openAolServiceWithFaqsViewController(id: service.subTopicServiceId, title: subTopicTitle)
    }
}

