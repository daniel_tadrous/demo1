//
//  AuthenticationCoordinator.swift
//  TAMMApp
//
//  Created by kerolos on 3/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

protocol AuthenticationCoordinatorDelegate: class {
    func authenticationCoordinatorDidFinish()
}

class AuthenticationCoordinator: NavigationCoordinator {
    
    // MARK: - Properties
    var justCameFromIntro: Bool
    var navigationController: UINavigationController
    let container: Container
    let authenticationService : AuthenticationService
     var delegate: AuthenticationCoordinatorDelegate?
    
    init(container: Container, navigationController: UINavigationController, authenticationService: AuthenticationService, justCameFromIntro: Bool) {
        self.container = container
        self.navigationController = navigationController
        self.authenticationService = authenticationService
        self.justCameFromIntro = justCameFromIntro
    }
    
    // MARK: - Coordinator core
    
    func start() {
        let vc = container.resolveViewController(LoginViewController.self)
        vc.justCameFromIntro = self.justCameFromIntro
        vc.delegate = self
        vc.navigationItem.hidesBackButton = true
        
//        navigationController.setBackButton()
        navigationController.pushViewController(vc, animated: true)
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        let appDellegate = (UIApplication.shared.delegate as! AppDelegate)
        
        if appDellegate.shouldTriggerLoginAfterIntro{
            appDellegate.shouldTriggerLoginAfterIntro = false
            self.login()
        }
    }

}

// MARK: - Delegate

extension AuthenticationCoordinator: LoginViewControllerDelegate {
    
    func authenticationViewControllerDismissed() {
        delegate?.authenticationCoordinatorDidFinish()
    }
    func login() -> Observable<Bool> {
        let authed = authenticationService.authenticate(authenticationServiceState: self)
        _ = authed.subscribe(onNext: {[weak self] b in
            if b{
                self?.delegate?.authenticationCoordinatorDidFinish()
            }
            
        })
        
        return authed
    }
    func reload(){
        let emptyVc = container.resolveViewController(EmptyFlipViewController.self)

        
        self.navigationController.setViewControllers([emptyVc], animated: false)
        


        
        self.navigationController.navigationItem.hidesBackButton = true
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { ( finished) -> Void in
            let vc = self.container.resolveViewController(LoginViewController.self)
            vc.delegate = self
            
            self.navigationController.setViewControllers([vc], animated: false)
        }
    }
    
}

extension AuthenticationCoordinator: AuthenticationServiceState {
    var presenter: UIViewController {
        return navigationController.visibleViewController!
    }
    
    
}



