//
//  SuggestionTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 9/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class SuggestionTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var bodyLbl: LocalizedLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(_ item: SearchSuggestions.SearchSuggestion){
        titleLbl.attributedText = item.Html?.getAttributedString()
        bodyLbl.text = item.Payload
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
