//
//  ErrorView.swift
//  TAMMApp
//
//  Created by kerolos on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

class ErrorView : UIView{
    static var show: Bool = true
    @IBOutlet weak var messageBody: LocalizedLabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var exclamationIconView: UIView!
    @IBOutlet weak var exclamationIconLabel: UILabel!
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    
    var message:String = ""
    var onRetryClickHandler:(()->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    init(frame: CGRect,message:String,onRetryClickHandler:@escaping ()->Void) {
        super.init(frame: frame)
        self.onRetryClickHandler = onRetryClickHandler
        self.message = message
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(okBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        exclamationIconLabel.text = "\u{e007}"

        okButtonView.addRoundCorners(radious: 25, borderColor: UIColor.turquoiseBlue)

        dialogView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        exclamationIconView.addRoundAllCorners(radious: exclamationIconView.bounds.width / 2 )
        setupUiData()
    }
    func setupUiData(){
        self.messageBody.text = self.message
    }
    @objc func okBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
        onRetryClickHandler?()
        
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
}
