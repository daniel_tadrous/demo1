//
//  ExpandedTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ExpandedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var serviceTitleLabel: LocalizedLabel!
    
    @IBOutlet weak var serviceDescriptionLabel: LocalizedLabel!
    
    
    @IBOutlet weak var collapseServiceCellButton: UIButton!
    
    
    @IBOutlet weak var viewServiceDetailButton: UITammResizableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        viewServiceDetailButton.applyRoundCorners = true
//        viewServiceDetailButton.applyCornersIfActive()
//          viewServiceDetailButton.addRoundCorners(radious: CGFloat(23))
//          viewServiceDetailButton.layer.borderColor = UIColor.turquoiseBlue.cgColor
//          viewServiceDetailButton.layer.borderWidth = 0.5
          //viewServiceDetailButton.layer.borderColor = UIColor(patternImage: UIImage(named: "dash")!).cgColor
         // self.containerView.addRoundCorners(radious: 5)
        containerView.layer.shadowOpacity = 0.1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
