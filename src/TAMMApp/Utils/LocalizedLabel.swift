//
//  LocalizedLabel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class LocalizedLabel: UILabel, CustomFontComponent {
    
    
    var disposeBag = DisposeBag()
    
    @IBInspectable
    var Radius: CGFloat = 0{
        didSet{
            if self.AllRounded{
                self.addRoundAllCorners(radious: self.Radius)
            }else{
                self.addRoundCorners(radious: self.Radius)
            }
        }
    }
    @IBInspectable
    var AllRounded: Bool = false{
        didSet{
            if self.AllRounded{
                self.addRoundAllCorners(radious: self.Radius)
            }else{
                self.addRoundCorners(radious: self.Radius)
            }
        }
    }
    @IBInspectable
    var HasBorder: Bool = false{
        didSet{
            if self.HasBorder{
                self.layer.borderWidth = 1
            }
        }
    }
    @IBInspectable
    var BorderColor: UIColor = UIColor.clear{
        didSet{
            if self.HasBorder{
                self.layer.borderColor = self.BorderColor.cgColor
            }
        }
    }
    
    @IBInspectable
    // "ar" or "en"
    var fixedLanguage: String?
    
    var isRTL: Bool {
        if fixedLanguage == "en"{
            return false
        }
        if fixedLanguage == "ar"{
            return true
        }
        
        return L102Language.currentAppleLanguage().contains("ar")
    }

    
    
    
    var attributedTextProxy: NSAttributedString?{
        get{
            return attributedText
        }
        set{
            attributedText = newValue
        }
    }
    
    var textProxy: String?{
        return text
    }
    
    var lineBreakModeProxy: NSLineBreakMode?{
        
        return lineBreakMode
    }
    
    var fontProxy: UIFont?{
        get{
            return self.font
        }
        set {
            self.font = newValue
        }
    }
    
    @IBInspectable
    var applyFontScalling: Bool = false
    
    @IBInspectable var RTLFont:String = "Swissra-Normal"
    
//    // properties currently not used 
//    @IBInspectable
//    var customFontFamilyCode: Int = 0
//    @IBInspectable
//    var customFontStyleCode: Int = 0
//    @IBInspectable
//    var customFontSize: CGFloat = 16
    
    var originalFont: UIFont?
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        registerTextObserver()
        self.originalFont = fontProxy
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        self.invalidateIntrinsicContentSize()
        super.layoutSubviews()
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        self.adjustAlignment(RTL: isRTL)
        
      // self.adjustFont(RTL: isRTL)
    }
    
    fileprivate func adjustAlignment(RTL:Bool){
        if(RTL){
            if(self.textAlignment != .center){
                self.textAlignment = .right
            }
        }
        else{
            if(self.textAlignment != .center){
                self.textAlignment = .left
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        adjustFont()
    }
    
    fileprivate func registerTextObserver() {
        (UIApplication.shared.delegate as! AppDelegate).isTraitChanged.asObservable().subscribe(onNext: { [unowned self] (isTrue) in
                self.adjustFont()
        }).disposed(by: disposeBag)
    
        self.addObserver(self, forKeyPath: #keyPath(text), options: [.old, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(text) {
            adjustFont()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: #keyPath(text) )
    }
   
  
}




