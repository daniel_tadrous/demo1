//
//  TammService.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class PrivacyModel: Mappable {
    var privacyPolicy: String?
   
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        privacyPolicy <- map["privacyPolicy"]
    }
    
}
