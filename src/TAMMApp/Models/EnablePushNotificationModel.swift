//
//  PushNotificationModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

import ObjectMapper

class EnablePushNotificationModel : Mappable{
    
    var isEnabled:Bool?
    var reminder:Int?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        isEnabled <- map["isEnabled"]
        reminder <- map["reminder"]
    }
}

