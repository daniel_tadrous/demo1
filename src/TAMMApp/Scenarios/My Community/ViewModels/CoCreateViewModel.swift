//
//  PollsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class CoCreateViewModel: ShareViewModel{
    private(set) var viewData: Variable<MyCommunityModel.CoCreate?> = Variable(nil)
    private(set) var start: Variable<Bool?> = Variable(nil)
    private(set) var answersViewData:Variable<[MyCommunityModel.Poll.Answer]?> = Variable(nil)
    private(set) var poll: Variable<MyCommunityModel.Poll?> = Variable(nil)
    fileprivate var apiService: ApiMyCommunityTypes!
    private(set) var collaborate: Variable<CollaborateModel?> = Variable(nil)
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiMyCommunityTypes) {
        super.init()
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        start.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        poll.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.submitPoll()
            })
            .disposed(by:disposeBag)
        collaborate.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.submitCollaborate()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if start.value != nil && start.value!{
            apiService.getCoCreate()
                .subscribe(onNext: { [unowned self] cocreate in
                    self.viewData.value = cocreate
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func submitPoll(){
        if poll.value != nil{
            apiService.submitPoll(poll: poll.value!)
                .subscribe(onNext: { [unowned self] answers in
                    self.answersViewData.value = answers
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func submitCollaborate(){
        if collaborate.value != nil{
            apiService.collaborate(collaborate: collaborate.value!)
                .subscribe(onNext: { [unowned self] emptyRes in
                    self.submitCollaborationStatusPublisher.onNext(emptyRes)
                    }, onCompleted:{ [unowned self] in
                    self.submitCollaborationStatusPublisher.onCompleted()
                }
                           ).disposed(by: disposeBag)
        }
    }
    func getUserEmail()->String{
        let savedUser:User? = User.getUser()
        
        var userEmail = ""
        
        if(savedUser != nil){
            userEmail = (savedUser?.email) ?? ""
        }
        return userEmail
    }
    
    private let submitCollaborationStatusPublisher: PublishSubject<EmptyResponse?> = PublishSubject()
    
    public var submitCollaborationStatus: Observable<EmptyResponse?>{
        return submitCollaborationStatusPublisher.asObservable()
    }
    
}
