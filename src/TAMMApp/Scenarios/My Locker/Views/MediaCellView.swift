//
//  MediaCellView.swift
//  TAMMApp
//
//  Created by marina on 6/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class MediaCellView:UITableViewCell{

    @IBOutlet weak var borderedView: UIView!
    @IBOutlet weak var icon: UILabel!
    @IBOutlet weak var mediaNameLbl: LocalizedLabel!
    @IBOutlet weak var mediaResolutionLbl: LocalizedLabel!
    @IBOutlet weak var mediaSizeLbl: LocalizedLabel!
    @IBOutlet weak var deleteBtn: UIButton!
    var index:Int = -1
    var onRemoveClick: ((_ index:Int)->Void)?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    
    }
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        onRemoveClick?(index)
    }
    private func commonInit(){
        
        self.autoresizingMask = [.flexibleWidth]
    }
}
