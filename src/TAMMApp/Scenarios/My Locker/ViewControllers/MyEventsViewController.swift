//
//  MyEventsViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift



class MyEventsViewController: TammViewController,MyEventsChildStoryboardLoadable {
    
    let expandedCellIdentifier = "EventCell"
    let collapsedCellIdentifier = "EventCellCollapsed"
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var searchView: TammSearchView!
    
    var eventFilterModel = EventsFilterApplyModel()
    var reloadTimes = 0
    public var viewModel:MyEventsViewModel!
    private var disposeBag = DisposeBag()
    
    var events:[Event]{
        get{
            return viewModel.events.value ?? []
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        // Do any additional setup after loading the view.
         eventsTableView.register(UINib(nibName: expandedCellIdentifier, bundle: nil), forCellReuseIdentifier: expandedCellIdentifier)
         eventsTableView.register(UINib(nibName: collapsedCellIdentifier, bundle: nil), forCellReuseIdentifier: collapsedCellIdentifier)
        setNavigationBar(title: L10n.myEvents, willSetSearchButton: true)
        sortButton.setTitle("\u{e008}", for: .normal)
        searchView.PlaceholderTextContent = L10n.search
        self.searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClicked)))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1).getAdjustedColor()
        filterButton.setTitleColor( #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor() ,  for: .normal)
        sortButton.setTitleColor( #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor() , for: .normal)
        searchView.applyColorTheme()
        eventsTableView.reloadData()
    }
   
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
       let filterView = EventsFilterView(frame: UIScreen.main.bounds, viewModel: viewModel) { (filter) in
            self.viewModel.filterObject.value = filter
            self.applyFilter()
        }
        
        UIApplication.shared.keyWindow!.addSubview(filterView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: filterView)
    }
    
    @IBAction func sortButtonAction(_ sender: UIButton) {
        viewModel.sort()
    }
    
    @objc func searchClicked(){
        let searchViewOverLay = SearchOverlayView(frame: UIScreen.main.bounds, currentViewModel: self.viewModel!, text: self.searchView.searchField.text!, currentSearchModel: self.viewModel!.mySearchViewModel)
        searchViewOverLay.delegate = self
        UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay)
        print("Search View Clicked")
    }
    
}
extension MyEventsViewController:UITableViewDelegate, UITableViewDataSource{
    
    func getData(){
        viewModel!.events.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] eventsRes in
                if(eventsRes == nil){
                    //TODO: should do somting ????
                    
                }else{
                    print(eventsRes!.count)
                    self.eventsTableView.reloadData()
                    
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = events[indexPath.row]
        var cell:MyEventCell!
        cell = tableView.dequeueReusableCell(withIdentifier: expandedCellIdentifier) as! MyEventCell
        cell.commonInit()
        if event.isExpanded {
           cell.collapsedView.isHidden = true
            cell.expandedView.isHidden = false
            cell.expand()
        }
        else{
            cell.collapsedView.isHidden = false
            cell.expandedView.isHidden = true
            cell.collapse()
        }
        
        cell.eventTitle.text = event.title
        cell.eventTime?.text = event.getFormattedTime()
        cell.eventTimeExpanded?.text = event.getFormattedTime()
        if (event.addresses?.count)! > 1 {
            cell.eventVenues?.text = "Multiple Venues"
            cell.eventVenuesExpanded?.text = "Multiple Venues"
        }
        else if (event.addresses?.count)! == 1 {
            cell.eventVenues?.text = event.addresses![0].description
            cell.eventVenuesExpanded?.text = event.addresses![0].description
        }
        else{
            cell.eventVenues?.text = ""
            cell.eventVenuesExpanded?.text = ""
        }
       
        
        cell.eventStartMonth.text = ""
        cell.eventStartDay.attributedText = event.getFormattedDateAttributed()
        cell.eventEndMonth.text = event.getFormattedEndMonth(monthFormatOfthree: false)
        cell.eventEndDay.text = event.getFormattedEndDay()
//        cell.categoryLabel.text = event.category?.title
        cell.eventDescription?.text = event.description
        cell.addToCalenderClosure = {
                self.viewModel.addToCalendar(event: event)
            }
            cell.buyTicketsClosure = {
                self.viewModel.buyTickets()
            }
            if event.hijri! != ""{
                cell.hijriLabel.text = "| \(event.hijri!)"
            }
        
        if !event.isReloaded && indexPath.row==2{
            reloadTimes+=1
           event.isReloaded = true
            tableView.reloadData()
       }
        
        if event.isExpanded {
            let constraint = cell.imageAspectRatioConstraint.constraintWithMultiplier(event.imageAspectRatio)
            cell.eventImage.removeConstraint(cell.imageAspectRatioConstraint)
            cell.imageAspectRatioConstraint = constraint
            cell.eventImage.addConstraint(cell.imageAspectRatioConstraint)
        }
        
        
        ImageCachingUtil.loadImage(url: URL(string: (event.image)!)!, dispatchInMain: true
            , callBack: { (url, image) in
                if event.isExpanded && !event.isReloaded{
                    
                }else{
                    event.isReloaded = true
                    event.imageAspectRatio = (image?.size.width)!/(image?.size.height)!
                    let constraint = cell.imageAspectRatioConstraint.constraintWithMultiplier(event.imageAspectRatio)
                    cell.eventImage.removeConstraint(cell.imageAspectRatioConstraint)
                    cell.imageAspectRatioConstraint = constraint
                    cell.eventImage.addConstraint(cell.imageAspectRatioConstraint)
                }
                
                cell.eventImage.image = image
        })
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let event = events[indexPath.row]
        event.isExpanded = !event.isExpanded
        
        let cell = tableView.cellForRow(at: indexPath) as! MyEventCell
        //let rectOfCellInTableView = tableView.rectForRow(at: indexPath)
        if event.isExpanded{
            cell.expand()
        }else{
            cell.collapse()
        }
       
        tableView.beginUpdates()
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 36
    }
    
}

extension MyEventsViewController:SearchOverlayViewDelegate{
    
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchView.searchField.text = message.name
        self.searchView.PlaceholderTextContent = message.name
        self.viewModel?.searchedText.value = message.name
    }
    func backPressed(){
        self.searchView.searchField.text = ""
    }
    
}

extension MyEventsViewController:FilterViewDelegate{
    var filterModel: EventsFilterApplyModel {
        get {
            return eventFilterModel
        }set{
            eventFilterModel = newValue
        }
    }
    
    func applyFilter() {
        viewModel.getMyEvents()
    }
    
    
    
}
