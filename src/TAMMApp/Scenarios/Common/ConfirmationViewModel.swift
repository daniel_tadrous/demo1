//
//  ConfirmationViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 7/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
class ConfirmationViewModel{

    private(set) var type: ConfirmationType!
    
    private(set) var title: String?
    private(set) var summary: String?
    private(set) var reference: String?
    private(set) var details: String?
    private(set) var iconLetter: String?
    private(set) var gotoLabel: String?
//    private(set) var onCompleted: ()->() = {}
    
    
    
    func initialize(type: ConfirmationType, title: String?, summary: String?, reference: String?, details: String?, iconLetter: String?, gotoLabel: String?
//        , onCompleted: @escaping ()->()
        ){
        
        self.type = type
        self.title = title
        self.summary = summary
        self.reference = reference
        self.details = details
        self.iconLetter = iconLetter
        self.gotoLabel = gotoLabel
//        self.onCompleted = onCompleted
    }
    
    
}
