//
//  ProfileSwitchView.swift
//  TAMMApp
//
//  Created by kerolos on 4/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
@IBDesignable
class ProfileSwitchView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @IBOutlet weak var personal: UICircularImage!
    @IBOutlet weak var business: UICircularImage!
    @IBOutlet weak var personalLabel: UILabel!
    @IBOutlet weak var businessLabel: UILabel!
    @IBOutlet weak var personalIconLabel: StyledLabel!
    @IBOutlet weak var businessIconLabel: StyledLabel!
    
    
    var contentView:UIView?
    @IBInspectable var nibName:String? = "ProfileSwitchView"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
        setupUI()
        
    }
    
    
    
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        
        view.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        addSubview(view)
        contentView = view

    }
    

    
    
    @objc func setIsBusiness(){
        setBusinessOrPersonal(isPersonal: false)
    }
    
    fileprivate func setBusinessOrPersonal(isPersonal: Bool){
        setPersonal(selected: isPersonal)
        setBusiness(selected: !isPersonal)
    }
    
    @objc func setIsPersonal(){
        setBusinessOrPersonal(isPersonal: true)
    }
    
    fileprivate func setPersonal(selected: Bool){
        
        personalIconLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if selected{
            personalLabel.textColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            personalLabel.alpha = 1
            personal.backgroundColor =  #colorLiteral(red: 1, green: 0.8666666667, blue: 0, alpha: 1)
        }else{
        
            personalLabel.textColor =  #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            personalLabel.alpha = 0.5
            personal.backgroundColor =  #colorLiteral(red: 0.2666666667, green: 0.2509803922, blue: 0.3725490196, alpha: 1)
        }
    }
    
    fileprivate func setBusiness(selected: Bool){
        if(selected){
            business.backgroundColor =  #colorLiteral(red: 1, green: 0.8666666667, blue: 0, alpha: 1)
            businessIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1)
            businessIconLabel.alpha = 1.0
            businessLabel.alpha = 1.0
        }else{
            business.backgroundColor =  #colorLiteral(red: 0.2588235294, green: 0.2509803922, blue: 0.3725490196, alpha: 1)
            businessIconLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            businessIconLabel.alpha = 0.5
            businessLabel.alpha = 0.5

        }
    }
    
    
    func setupUI(){
        let view = self
        
        let newView = contentView!
//        newView.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
        let heightConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
        
        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        setBusinessOrPersonal(isPersonal: true)
        
        // adding events for the views
        
        
        let tabGestureBusiness = UITapGestureRecognizer(target: self, action: #selector( businessClicked ) )
        business.addGestureRecognizer(tabGestureBusiness)
        
        let tabGesturePersonal = UITapGestureRecognizer(target: self, action: #selector( setIsPersonal ) )
        //personal.addGestureRecognizer(tabGesturePersonal)
        
        


        
    }
    
    @objc func businessClicked(){
        Toast(message:L10n.commingSoon).Show()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        personalLabel.text = L10n.personal
        businessLabel.text = L10n.business
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
        setIsBusiness()
    }
    
    
    
    
    //
    //    let nibName = "ReusableCustomView"
    //    var contentView: UIView?
    //
    //    @IBOutlet weak var label: UILabel!
    //    @IBAction func buttonTap(_ sender: UIButton) {
    //        label.text = "Hi"
    //    }
    //
    //    required init?(coder aDecoder: NSCoder) {
    //        super.init(coder: aDecoder)
    //
    //        guard let view = loadViewFromNib() else { return }
    //        view.frame = self.bounds
    //        self.addSubview(view)
    //        contentView = view
    //    }
    //
    //    func loadViewFromNib() -> UIView? {
    //        let bundle = Bundle(for: type(of: self))
    //        let nib = UINib(nibName: nibName, bundle: bundle)
    //        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    //    }
    
}
