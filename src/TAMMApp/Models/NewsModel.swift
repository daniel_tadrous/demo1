//
//  NewsModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsModel : Mappable{
    
    var id:String?
    var title:String?
    var category:String?
    var date:String?
    var imageUrl: String?
    var newsUrl:String?
    
    
    var isLoaded:Bool = false
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        category <- map["category"]
        date <- map["date"]
        imageUrl <- map["image"]
        newsUrl <- map["url"]
    }
}
