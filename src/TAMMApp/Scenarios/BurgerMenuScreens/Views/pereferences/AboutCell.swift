//
//  AboutCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AboutCell: UITableViewCell {
    
    @IBOutlet weak var privacyPolicyLogo: UILabel!
    @IBOutlet weak var termsOfUseLogo: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var termsOfUseView: RoundedView!
    @IBOutlet weak var privacyPolicyView: RoundedView!
    var gotoTermsOfUseClosure:(()->Void)!
    var goToPrivacyPolicyClosure:(()->Void)!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        commonInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit() {
        privacyPolicyLogo.text = "\u{e005}"
        self.backgroundColor = UIColor.paleGreyTwo
        termsOfUseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoTermsOfUse)))
        privacyPolicyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoPrivacyPolicy)))
    }
    
    @objc func gotoTermsOfUse(){
        gotoTermsOfUseClosure()
    }
    @objc func gotoPrivacyPolicy(){
        goToPrivacyPolicyClosure()
    }

}
