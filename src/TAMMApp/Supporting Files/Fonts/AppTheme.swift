//
//  Themes.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/27/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import UIKit

struct AppTheme {
    
    static var backgroundColor:UIColor = UIColor.paleGreyTwo
    static var buttonTextColor:UIColor = UIColor.petrol
    static var buttonBackgroundColor:UIColor = UIColor.duckEggBlue
    static var buttonBorderColor:UIColor = UIColor.turquoiseBlue
    static var tabBarBacgroundColor:UIColor = UIColor.darkBlueGrey
    static var tabBartextColor:UIColor = UIColor.white
    
    static public func defaultTheme() {
        self.backgroundColor = UIColor.paleGreyTwo
        self.buttonTextColor = UIColor.petrol
        self.buttonBackgroundColor = UIColor.duckEggBlue
        self.buttonBorderColor = UIColor.turquoiseBlue
        self.tabBarBacgroundColor = UIColor.darkBlueGrey
        self.tabBartextColor = UIColor.white
        updateDisplay()
    }
    
    static public func darkTheme() {
        self.backgroundColor = backgroundColor.inverted
        self.buttonTextColor = buttonTextColor.inverted
        self.buttonBackgroundColor = buttonBackgroundColor.inverted
        self.buttonBorderColor = buttonBorderColor.inverted
        self.tabBarBacgroundColor = tabBarBacgroundColor.inverted
        self.tabBartextColor = tabBartextColor.inverted
        updateDisplay()
    }
    
    static public func updateDisplay() {
//        let proxyButton = UITammButton.appearance()
//        proxyButton.setTitleColor(AppTheme.buttonTextColor, for: .normal)
//        proxyButton.backgroundColor = AppTheme.buttonBackgroundColor
//        proxyButton.layer.borderColor = AppTheme.buttonBorderColor.cgColor
        
//        let proxyView =  UITammButton.appearance()
//        proxyView.backgroundColor = AppTheme.backgroundColor
//
           let navigationBarview = UINavigationBar.appearance()
           navigationBarview.barTintColor = UIColor.brown
            //AppTheme.tabBarBacgroundColor

        
           let tabBarView = UITabBar.appearance()
           tabBarView.barTintColor = AppTheme.tabBarBacgroundColor
           tabBarView.tintColor = AppTheme.tabBartextColor
           tabBarView.unselectedItemTintColor = AppTheme.tabBartextColor

    }
}

extension UIColor {
    var inverted: UIColor {
        var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: (1 - r), green: (1 - g), blue: (1 - b), alpha: a) // Assuming you want the same alpha value.
    }
}
