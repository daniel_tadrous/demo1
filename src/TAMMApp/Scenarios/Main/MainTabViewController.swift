//
//  MainTabViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import SideMenu
//import Crashlytics

protocol MainTabViewControllerDelegate: class {
    func startMyLocker()
    func startMyLockerWithSupport()
    func startMyLockerWithMyEvents()
    func startMyLockerAtNewDraft(isFromOfflineScreen: Bool)
    func startAspectsOfLife()
    func startMyCommunity(atItem: MyCommunityItems)
}
extension UIImage {
    // Top Indicator
    func createMainTabBarSelectionIndicator(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.topBarItemIndicatorColor.setFill()
        //UIRectFill(CGRect(origin: CGPoint(x: 0,y :size.height - lineWidth), size: CGSize(width: size.width, height: lineWidth)))
        UIRectFill(CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: size.width, height: 4)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension UIColor {
    open class var topBarItemIndicatorColor: UIColor{
        return UIColor.init(red: 0, green: 161/255, blue: 189/255, alpha: 1)
    }
}
enum MainTabItems :Int{
    case myUpdates = 0
    case myLocker = 1
    case aspectsOfLife = 2
    case myCommunity = 3
    case menu = 4
    case incident = 5
    case myEvents = 6
}
class MainTabViewController:TammTabBarController, MainTabStoryboardLodable{
    
    private static weak var current_tab_controller: MainTabViewController?
    weak var vcDelegate: MainTabViewControllerDelegate?
    var sideMenuViewController: SideMenuViewController?
    let kBarHeight:CGFloat = 61;
    
    public static func applyCurrentTheme(){
        current_tab_controller?.adjustTabbarInversionStatus()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // should be in the constructor but will worke here for now
        MainTabViewController.current_tab_controller = self
        
        self.selectedIndex = MainTabItems.myUpdates.rawValue
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addfloatingMenu()
        // vcDelegate?.startMyLocker()
        setupRightSideMenu()
        // Crashlytics.sharedInstance().crash()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font:UIFont.init(name: L10n.Lang == "en" ? "Roboto-Regular" : "Swissra-Normal", size: 11)!], for: .normal )
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tabBar.invalidateIntrinsicContentSize()
        self.view.layoutIfNeeded()
        var tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
        tabFrame.size.height = kBarHeight;
        if #available(iOS 11.0, *) {
            tabFrame.size.height = kBarHeight + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!
            tabFrame.origin.y = self.view.frame.height - kBarHeight - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!
        } else {
            tabFrame.origin.y = self.view.frame.height - kBarHeight
        };
        self.tabBar.frame = tabFrame;
        //UITabBarItem.appearance()
        
        tabBar.selectionIndicatorImage = UIImage().createMainTabBarSelectionIndicator(size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.heightOfTabbar = tabBar.frame.height
    }
    fileprivate func adjustTabbarInversionStatus() {
        //tabBar.beginCustomizingItems(self.tabBar.items ?? [])
        
        for  i in 0..<(self.tabBar.items?.count ?? 0) {
            let item = self.tabBar.items![i]
            
            // my updates
            
            
            switch i{
            case 0:
                item.image =  #imageLiteral(resourceName: "MyUpdates").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                item.selectedImage = #imageLiteral(resourceName: "MyUpdates-Active").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            case 1:
                item.image =  #imageLiteral(resourceName: "MyLocker").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                item.selectedImage = #imageLiteral(resourceName: "MyLocker-Active").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            case 2:
                item.image =  #imageLiteral(resourceName: "MyServices").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                item.selectedImage = #imageLiteral(resourceName: "MyServices-Active").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            case 3:
                item.image =  #imageLiteral(resourceName: "MyComunity").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                item.selectedImage = #imageLiteral(resourceName: "MyComunity-Active").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            case 4:
                item.image =  #imageLiteral(resourceName: "Menu").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                item.selectedImage = #imageLiteral(resourceName: "Menu").getAdjustedImage().withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                
                
            default:
                break
            }
            
            
        }
        
        
        
        tabBar.tintColor = UIColor.white.getAdjustedColor()
        tabBar.unselectedItemTintColor = UIColor.white.getAdjustedColor()
        
        tabBar.setNeedsDisplay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adjustTabbarInversionStatus()

        
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        // call
        //        print(self.selectedIndex)
        
        
        let selected = tabBar.items!.index(of: item)!
        print ("\(selected)")
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { // change 2 to desired number of seconds
//            // Your code with delay
//
//            print(self.selectedIndex)
//            switch(self.selectedIndex){
//            case MainTabItems.myLocker.rawValue:
//                self.vcDelegate?.startMyLocker()
//                break;
//            case MainTabItems.aspectsOfLife.rawValue:
//                //        default:
//                self.vcDelegate?.startAspectsOfLife()
//                break;
//            default: break
//            }
//        }
    }
    
    
    
    func setupRightSideMenu(){
        
        self.delegate = self
        
//        let sideMenuViewController = SideMenuViewController()
        // injected in coordinator
        let menuRightNavigationController = RightMenuNavigationController(rootViewController: sideMenuViewController!)
        //        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        //        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        //        // let menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as! UISideMenuNavigationController
        if(L102Language.currentAppleLanguage().contains("ar")){
            SideMenuManager.default.menuLeftNavigationController = menuRightNavigationController
            SideMenuManager.default.menuRightNavigationController = nil

        }
        else{
           SideMenuManager.default.menuLeftNavigationController = nil
           SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
        }
        
        menuRightNavigationController.isNavigationBarHidden = true
        menuRightNavigationController.hidesBottomBarWhenPushed = true
        //
        //        // Enable gestures. The left and/or right menus must be set up above for these to work.
        //        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        //
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuBlurEffectStyle = UIBlurEffectStyle.dark
        SideMenuManager.default.menuAnimationFadeStrength = CGFloat(0.4)
        
        SideMenuManager.default.menuShadowOpacity = 0.0
        
        
        SideMenuManager.default.menuAnimationTransformScaleFactor = 1.0
        
        SideMenuManager.default.menuFadeStatusBar = true
        
        SideMenuManager.default.menuAnimationBackgroundColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1)
        SideMenuManager.default.menuShadowColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1)
        SideMenuManager.default.menuShadowOpacity = 1
        
//        SideMenuManager.default.menuWidth =  min(round(min((appScreenRect.width), (appScreenRect.height)) * 0.75), 240)
        SideMenuManager.default.menuWidth =  round(min((appScreenRect.width), (appScreenRect.height)) * 0.80)

        
    }
    internal var appScreenRect: CGRect {
        let appWindowRect = UIApplication.shared.keyWindow?.bounds ?? UIWindow().bounds
        return appWindowRect
    }
    func showSideMenu()  {
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }
        else{
            self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    
//    func hideSideMenu()  {        
//        dismiss(animated: true, completion: nil)
//    }
    
}

extension MainTabViewController: UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController is UINavigationController { // DummyViewController{
            let tabViewControllers = tabBarController.viewControllers!
            guard let toIndex = tabViewControllers.index(of: viewController) else {
                return false
            }
            self.animateToTab(toIndex: toIndex)
            
            return true
        }
        print ("should show side menu")
        showSideMenu()
        return false
    }
    
    fileprivate func navigateToSelected(onComplete: @escaping ()->Void = {}) {
       // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { // change 2 to desired number of seconds
            // Your code with delay
            print(self.selectedIndex)
            switch(self.selectedIndex){
            case MainTabItems.myLocker.rawValue:
                self.vcDelegate?.startMyLocker()
                break;
            case MainTabItems.aspectsOfLife.rawValue:
                //        default:
                self.vcDelegate?.startAspectsOfLife()
                break;
            case MainTabItems.myCommunity.rawValue:
        
                self.vcDelegate?.startMyCommunity(atItem: .Home)
                
                break;
            default: break
            }
            onComplete()
        //}
    }
    
    public func navigateToCoCreate(){
//        self.vcDelegate?.startMyCommunity(atItem: .Home)
        self.vcDelegate?.startMyCommunity(atItem: .CoCreation)
    }
    func forceAnimateToTab(toIndex: Int){
        let tabViewControllers = viewControllers!
        let fromView = selectedViewController?.view
        let toView = tabViewControllers[toIndex].view
        var fromIndex: Int = 0
        if let vc = selectedViewController{
            fromIndex = tabViewControllers.index(of: vc)!
        }
        guard fromIndex != toIndex else {return}
        // Add the toView to the tab bar view
        fromView?.superview!.addSubview(toView!)
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        let scrollRight = toIndex > fromIndex
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView?.center = CGPoint(x: (fromView?.center.x ?? (UIScreen.main.bounds.origin.x/2)) + offset, y: (toView?.center.y)!)
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
            // Slide the views by -offset
            fromView?.center = CGPoint(x: (fromView?.center.x ?? (UIScreen.main.bounds.origin.x/2)) - offset, y: (fromView?.center.y ?? (UIScreen.main.bounds.origin.y/2)))
            toView?.center   = CGPoint(x: (toView?.center.x)! - offset, y: (toView?.center.y)!)
            fromView?.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        
    }
    func animateToTab(toIndex: Int) {
        let tabViewControllers = viewControllers!
        let fromView = selectedViewController?.view
        let toView = tabViewControllers[toIndex].view
        var fromIndex: Int = 0
        if let vc = selectedViewController{
            fromIndex = tabViewControllers.index(of: vc)!
        }
        guard fromIndex != toIndex else {return}
        
        // Add the toView to the tab bar view
        fromView?.superview!.addSubview(toView!)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width;
        let scrollRight = toIndex > fromIndex
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView?.center = CGPoint(x: (fromView?.center.x ?? (UIScreen.main.bounds.origin.x/2)) + offset, y: (toView?.center.y)!)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            // Slide the views by -offset
            fromView?.center = CGPoint(x: (fromView?.center.x ?? (UIScreen.main.bounds.origin.x/2)) - offset, y: (fromView?.center.y ?? (UIScreen.main.bounds.origin.y/2)))
            toView?.center   = CGPoint(x: (toView?.center.x)! - offset, y: (toView?.center.y)!)
            
        }, completion: { finished in
            
            // Remove the old view from the tabbar view.
            fromView?.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }
    
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        navigateToSelected()
    }
    
    func setSelected(item: MainTabItems){
        switch item {
        case .incident:
            self.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
            self.selectedViewController = self.viewControllers?[MainTabItems.myLocker.rawValue]
            self.vcDelegate?.startMyLockerWithSupport()
            return
        case .myEvents:
            self.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
            self.selectedViewController = self.viewControllers?[MainTabItems.myLocker.rawValue]
            self.vcDelegate?.startMyLockerWithMyEvents()
            return
        default:
            break
        }
        self.animateToTab(toIndex: item.rawValue)
//        self.selectedIndex = item.rawValue
        self.selectedViewController = self.viewControllers?[item.rawValue]
        navigateToSelected()
//        tabBar.selectedItem = tabBar.items![item.rawValue] as UITabBarItem
    }
    
    
    func setMyLockerSelectedAtNewDraft(isFromOfflineScreen: Bool = false){
        self.selectedIndex = MainTabItems.myLocker.rawValue
        self.selectedViewController = self.viewControllers?[self.selectedIndex]
        self.vcDelegate?.startMyLockerAtNewDraft(isFromOfflineScreen:isFromOfflineScreen)
    }
    func setMyLockerSelected(onComplete: @escaping ()->Void = {}){
        self.selectedIndex = MainTabItems.myLocker.rawValue
        self.selectedViewController = self.viewControllers?[self.selectedIndex]
        navigateToSelected {
            onComplete()
        }
        //        tabBar.selectedItem = tabBar.items![item.rawValue] as UITabBarItem
    }
    
    
}


