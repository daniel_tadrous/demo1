//
//  XibView.swift
//  TAMMApp
//
//  Created by kerolos on 6/27/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


@IBDesignable
class XibView : UIView {
    
    var contentView:UIView?
    @IBInspectable var nibName:String?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        xibSetup()
    }
    
    internal func updateBoundsAndSizes() {
        contentView?.frame = bounds
        contentView?.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        contentView = view
        updateBoundsAndSizes()
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateBoundsAndSizes()
        
    }

    
}
