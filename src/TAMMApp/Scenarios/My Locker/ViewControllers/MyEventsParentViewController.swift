//
//  MyEventsParentViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MyEventsParentViewControllerDelegate {
    func openEvents(parentVC:MyEventsParentViewController)
    func openPublicHolidays(parentVC:MyEventsParentViewController)
}

class MyEventsParentViewController: TammViewController{
    
    var delegate:MyEventsParentViewControllerDelegate?
    
    @IBOutlet weak var fullLineView: UIView!
    @IBOutlet weak var eventsButton: LocalizedSelectableButton!
    @IBOutlet weak var publicHolidaysButton: LocalizedSelectableButton!
    @IBOutlet weak var containerView: UIView!
    
    fileprivate var eventsSelected = false
    fileprivate var publicHolidaysSelected = false
    
    @IBOutlet weak var underLine: UIView!
    fileprivate var underLineColor = UIColor(red: 0, green: 89, blue: 113, alpha: 1)
    
    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    fileprivate var selectedFontColor = UIColor(hexString: "005971")
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138")
    
    var selectedViewController:UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventsButton.originalFont = unSelectedFont
        eventsButton.fontProxy = unSelectedFont
        eventsButton.selectedFontReference = selectedFont

        
        publicHolidaysButton.originalFont = unSelectedFont
        publicHolidaysButton.fontProxy = unSelectedFont
        publicHolidaysButton.selectedFontReference = selectedFont


        self.title = "My Events"
        
        
        selectButtonAction(forButton: eventsButton, isEvents: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        underLine.backgroundColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        
        eventsButton.originalColor = unSelectedFontColor.getAdjustedColor()
        eventsButton.selectedColor = selectedFontColor.getAdjustedColor()
        publicHolidaysButton.originalColor = unSelectedFontColor.getAdjustedColor()
        publicHolidaysButton.selectedColor = selectedFontColor.getAdjustedColor()
        
    }
    

    @IBAction func eventsButtonAction(_ sender: LocalizedSelectableButton) {
        selectButtonAction(forButton: sender, isEvents: true)
    }
    
    @IBAction func publicHolidaysButtonAction(_ sender: LocalizedSelectableButton) {
        selectButtonAction(forButton: sender, isEvents: false)
    }
    
    fileprivate func selectButtonAction(forButton button:LocalizedSelectableButton, isEvents:Bool ) {
        eventsSelected = false
        publicHolidaysSelected = false
        eventsButton.isButtonSelected = false
        publicHolidaysButton.isButtonSelected = false
        
        
        
        if isEvents, !eventsSelected{
            eventsSelected = true
            //moveUnderLine(to: button)
            button.isButtonSelected = true
            delegate?.openEvents(parentVC: self)
        }
        else if !isEvents, !publicHolidaysSelected{
            publicHolidaysSelected = true
            //moveUnderLine(to: button)
            button.isButtonSelected = true
            // goto public holidays
        }
        
    }
        
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        if button == eventsButton {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
            underLine.frame.origin.x = fullLineView.frame.origin.x
        }
            
            
        else if button == publicHolidaysButton{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        if eventsSelected {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
            
            
        else if publicHolidaysSelected{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }


}
