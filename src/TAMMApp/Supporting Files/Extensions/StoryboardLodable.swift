//
//  StoryboardLodable.swift
//  iOSSampleApp
//
//  Created by Igor Kulman on 03/10/2017.
//  Copyright © 2017 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

protocol StoryboardLodable: class {
    @nonobjc static var storyboardName: String { get }
}

protocol SetupStoryboardLodable: StoryboardLodable {
}

protocol FeedStoryboardLodable: StoryboardLodable {
}

protocol AboutStoryboardLodable: StoryboardLodable {
}

protocol AuthenticationStoryboardLodable: StoryboardLodable {
}

protocol MainTabStoryboardLodable: StoryboardLodable {
    
}

protocol MyLockerStoryboardLodable: StoryboardLodable {
    
}

protocol MyCommunityStoryboardLodable: StoryboardLodable {
    
}

protocol AspectsOfLifeStoryboardLodable: StoryboardLodable {
}

protocol AolDetailsStoryboardLodable: StoryboardLodable {
}

protocol AolDetailsParentStoryboardLodable: StoryboardLodable {
}

protocol AolSubTopicsStoryboardLodable: StoryboardLodable {
    
}
protocol AolServiceWithFaqsStoryboardLodable: StoryboardLodable {
    
}
protocol AspectsOfLifeListDetailsStoryboardLodable: StoryboardLodable {
}

protocol JourneysStoryboardLodable: StoryboardLodable {
}
protocol SideMenuStoryboardLodable: StoryboardLodable {
}
protocol MySupportMainStoryboardLodable: StoryboardLodable {
}

protocol TalkToUsLandingStoryboardLodable: StoryboardLodable {
}
protocol TalkToUsStoryboardLodable: StoryboardLodable {
}

protocol TalkToUsTabBarStoryboardLodable: StoryboardLodable {
}

protocol TalkToUsDraftsStoryboardLodable: StoryboardLodable {
}
protocol MessagesStoryboardLodable: StoryboardLodable {
}
protocol CheckStatusStoryboardLodable: StoryboardLodable {
}

protocol FAQStoryboardLodable: StoryboardLodable {
}
protocol PollsStoryboardLodable: StoryboardLodable {
}
protocol CoCreateStoryboardLodable: StoryboardLodable {
}
protocol EventsStoryboardLodable: StoryboardLodable {
}
protocol OfflineScreenStoryboardLodable: StoryboardLodable {
}
protocol IncidentConfirmationStoryboardLodable: StoryboardLodable {
}

protocol IntroScreensStoryboardLoadable: StoryboardLodable {
}

protocol CommonStoryboardLodable: StoryboardLodable {
}
protocol GovernmentEntitiesStoryboardLodable: StoryboardLodable {
}

protocol NewsStoryboardLodable: StoryboardLodable {
}

protocol EventsParentStoryboardLoadable: StoryboardLodable {
}
protocol NewsAndPressReleasesStoryboardLoadable: StoryboardLodable {
}
protocol MyEventsChildStoryboardLoadable: StoryboardLodable {
}

protocol PublicHolidaysStoryboardLoadable: StoryboardLodable {
}

protocol MediaCenterStoryboardLoadable: StoryboardLodable {
}

protocol FactsAndFiguresStoryboardLoadable: StoryboardLodable {
}

protocol InitiativesStoryboardLoadable: StoryboardLodable {
}

protocol PerfernecesAboutStoryboardLoadable: StoryboardLodable {
}
protocol MediaSettingsStoryboardLoadable: StoryboardLodable {
}

protocol PressReleasesStoryboardLoadable: StoryboardLodable {
}
protocol HtmlStoryboardLoadable: StoryboardLodable {
}

protocol CaseDetailsStoryboardLoadable: StoryboardLodable {
}

protocol MyDocumentsStoryboardLoadable: StoryboardLodable {
}

protocol ActiveDocumentsStoryboardLoadable: StoryboardLodable {
}

protocol MySubscriptionsParentStoryboardLoadable: StoryboardLodable {
}

protocol MySubscriptionsChildStoryboardLoadable: StoryboardLodable {
}

extension SetupStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Setup"
    }
}

extension FeedStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Feed"
    }
}

extension AboutStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "About"
    }
}

extension AuthenticationStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Login"
    }
}

extension MainTabStoryboardLodable where Self: UITabBarController {
    @nonobjc static var storyboardName: String {
        return "MainTab"
    }
}
extension MyLockerStoryboardLodable where Self: UICollectionViewController {
    @nonobjc static var storyboardName: String {
        return "MyLocker"
    }
}

protocol LocationPickerStoryboardLodable: StoryboardLodable {
}
protocol EntityDetailsStoryboardLodable: StoryboardLodable {
}
//extension TestStoryboardLodable where Self: UIViewController {
//    @nonobjc static var storyboardName: String {
//        return "TestStory"
//    }
//}
extension AspectsOfLifeStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "AspectsOfLife"
    }
}

extension AolDetailsStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "AolDetails"
    }
}

extension AolDetailsParentStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "AolDetailsParent"
    }
}

extension AolSubTopicsStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "AolSubTopics"
    }
}
extension AolServiceWithFaqsStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "AolServiceWithFaqs"
    }
}

extension AspectsOfLifeListDetailsStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "AspectsOfLifeList"
    }
}

extension JourneysStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Journeys"
    }
}

extension SideMenuStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "SideMenuStoryboard"
    }
}

extension MySupportMainStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "MySupportMain"
    }
}

extension TalkToUsStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "TalkToUs"
    }
}


extension CheckStatusStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "CheckStatus"
    }
}


extension FAQStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "FAQ"
    }
}
extension TalkToUsLandingStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "TalkToUsLanding"
    }
}

extension TalkToUsDraftsStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "TalkToUsDrafts"
    }
}
extension MessagesStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "Messages"
    }
}
extension TalkToUsTabBarStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "TalkToUsTabBar"
    }
}

extension IncidentConfirmationStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "IncidentConfirmation"
    }
}

protocol CameraStoryboardLoadable: StoryboardLodable {
}
extension CameraStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "Camera"
    }
}

extension IntroScreensStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "IntroScreens"
    }
}
extension OfflineScreenStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "OfflineScreen"
    }
}

extension LocationPickerStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "LocationPicker"
    }
}

extension MyCommunityStoryboardLodable where Self: UICollectionViewController {
    @nonobjc static var storyboardName: String {
        return "MyCommunity"
    }
}

extension PollsStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "Polls"
    }
}
extension EventsStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "Events"
    }
}
extension CoCreateStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "CoCreate"
    }
}


extension CommonStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "Common"
    }
}

extension GovernmentEntitiesStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "GovernmentEntities"
    }
}


extension EntityDetailsStoryboardLodable  where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "EntityServices"
    }
}

extension NewsStoryboardLodable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "News"
    }
}



extension EventsParentStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "EventsParent"
    }
}
extension NewsAndPressReleasesStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "NewsAndPressReleases"
    }
}
extension MyEventsChildStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "MyEvents"
    }
}

extension PublicHolidaysStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "PublicHolidays"
    }
}
protocol EntitiesInformationStoryboardLoadable: StoryboardLodable {}
extension EntitiesInformationStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "EntitiesInformation"
    }
}


protocol TopTappedStoryboardLoadable: StoryboardLodable {}
extension TopTappedStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "TopTapped"
    }
}

extension MediaCenterStoryboardLoadable where Self: UIViewController  {
    @nonobjc static var storyboardName: String {
        return "MediaCenter"
    }
}

protocol PreferencesStoryboardLoadable: StoryboardLodable {}
extension PreferencesStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Preferences"
    }
}


protocol ProfileStoryboardLoadable: StoryboardLodable {}
extension ProfileStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Profile"
    }
}

extension FactsAndFiguresStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "FactsAndFigures"
    }
}

protocol LanguageSwitchStoryboardLoadable: StoryboardLodable {}
extension LanguageSwitchStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "LanguageSwitch"
    }
}

extension InitiativesStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Initiative"
    }
}

protocol AccessibilityStoryboardLoadable: StoryboardLodable {}
extension AccessibilityStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Accessibility"
    }
}


protocol EnablePushNotificationStoryboardLoadable: StoryboardLodable {}
extension EnablePushNotificationStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "EnablePushNotification"
    }
}

protocol SetReminderStoryboardLoadable: StoryboardLodable {}
extension SetReminderStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "SetReminder"
    }
}
    
    
    


extension PerfernecesAboutStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "PreferencesAbout"
    }
}

extension MediaSettingsStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MediaSettings"
	}
}
extension PressReleasesStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "PressReleases"

    }
}

extension HtmlStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Html"
        
    }
}

protocol ManageStoryboardLoadable: StoryboardLodable {}
extension ManageStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "Manage"
    }
}

extension CaseDetailsStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "CaseDetails"
        
    }
}


protocol MsgsParentStoryboardLoadable: StoryboardLodable {}
extension MsgsParentStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MsgsParent"
    }
}

extension MyDocumentsStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MyDocument"
    }
}

extension ActiveDocumentsStoryboardLoadable where Self: UIViewController {
        @nonobjc static var storyboardName: String {
            return "ActiveDocuments"
    }
}


protocol ManageParentStoryboardLoadable: StoryboardLodable {}
extension ManageParentStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "ManageParent"
    }
}

protocol ManageMediaStoryboardLoadable: StoryboardLodable {}
extension ManageMediaStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "ManageMedia"
    }
}
extension MySubscriptionsParentStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MySubscriptionsParent"
    }
}


protocol MyPaymentsParentStoryboardLoadable: StoryboardLodable {}
extension MyPaymentsParentStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MyPaymentsParent"
    }
}
protocol MyPaymentStoryboardLoadable: StoryboardLodable {}
extension MyPaymentStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MyPayment"
    }
}
extension MySubscriptionsChildStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MySubscriptionsChild"
    }
}

protocol DownloadMapStoryboardLoadable: StoryboardLodable {}
extension DownloadMapStoryboardLoadable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "DownloadMap"
    }
}

protocol MapParentStoryboardLodable: StoryboardLodable {
}
extension MapParentStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "MapParent"
    }
}
protocol OfflineMapStoryboardLodable: StoryboardLodable {
}
extension OfflineMapStoryboardLodable where Self: UIViewController {
    @nonobjc static var storyboardName: String {
        return "OfflineMap"
    }
}

