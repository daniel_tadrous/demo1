//
//  AolServiceFaqCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class PollCell: UITableViewCell {
    
    @IBOutlet weak var conatainerView: UIView!
    
    @IBOutlet weak var titleLable: LocalizedLabel!
    
    
    @IBOutlet weak var collapseExpandBtn: UIButton!
    
    @IBOutlet weak var participantLabel: UILabel!
    
    @IBOutlet weak var participantNumLabel: LocalizedLabel!
    
    @IBOutlet weak var closeDateLabel: LocalizedLabel!
    
    @IBAction func CollapseExpandClickHandler(_ sender: UIButton) {
        OnCollapseExpandBtnClick?()
    }
    var OnCollapseExpandBtnClick: (()->Void)?
    
    
    @IBOutlet weak var shareLabel: UILabel?
    
    @IBOutlet weak var shareTextLabel: LocalizedLabel?
    @IBOutlet weak var shareDimmerBtn: UIButton?
    
    @IBAction func ShareBtnClickHandler(_ sender: UIButton) {
        OnShareBtnClick?()
    }
    var OnShareBtnClick: (()->Void)?
   
    
    @IBOutlet weak var stackViewContainer: UIStackView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //self.sec2ContainerView?.applyRoundCorners()
        self.participantLabel.text = "\u{e00b}"
        self.shareLabel?.text = "\u{e003}"
        self.conatainerView.addRoundCorners(radious: 5)
        
    }
    
    func colorizeFooter(isCurrent:Bool){
        let color = isCurrent ? UIColor.petrol : UIColor.coolGrey
        self.participantLabel.textColor = color
        self.participantNumLabel.textColor = color
        self.shareLabel?.textColor = color
        self.shareTextLabel?.textColor = color
        self.closeDateLabel.textColor = color
    }
    func setClosureDate(poll: MyCommunityModel.Poll){
        let regularFont = UIFont(name: "Roboto-Regular", size: 13.0)!
        let boldFont = UIFont(name: "Roboto-Bold", size: 13.0)!
        
        
        let regularArabicFont = UIFont(name: "Swissra-Normal", size: 13.0)!
        let regularArabicBoldFont = UIFont(name: "Swissra-Bold", size: 13.0)!
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        if L10n.Lang == "en" {
            dateFormatterPrint.dateFormat = "MMM dd"
        }else {
            dateFormatterPrint.dateFormat = "dd MMM"
        }
        
        dateFormatterPrint.locale = Locale(identifier: L10n.Lang)
        if let date = dateFormatterGet.date(from: poll.closureDate!){
            let dateStr = dateFormatterPrint.string(from: date)
            let str = poll.isCurrent! ? L10n.MyCommunity.Polls.pollClosesOn(dateStr: dateStr) : L10n.MyCommunity.Polls.pollClosedOn(dateStr: dateStr)
            let currentFont:UIFont?
            let currentBoldFont:UIFont?
            if L10n.Lang == "en" {
                currentFont = regularFont
                currentBoldFont = boldFont
            }else {
                currentFont = regularArabicFont
                currentBoldFont = regularArabicBoldFont
            }
            let attributedString = NSMutableAttributedString(string: str, attributes: [
                .font:  currentFont!
                ])
            
            if L10n.Lang == "en" {
                attributedString.addAttribute(.font, value: currentBoldFont!, range: NSRange(location: str.count - dateStr.count, length: dateStr.count))
            }else{
                attributedString.addAttribute(.font, value: currentBoldFont!, range: NSRange(location: str.count - 1 - dateStr.count, length: dateStr.count+1))
            }
            self.closeDateLabel.attributedText = attributedString
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
