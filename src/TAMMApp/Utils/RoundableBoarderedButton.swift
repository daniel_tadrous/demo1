//
//  RoundableBoarderedButton.swift
//  TAMMApp
//
//  Created by kerolos on 7/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class RoundableBoarderedButton: UIButton {
    @IBInspectable public var borderColor: UIColor?
    @IBInspectable public var upperLeftCornerStyle: Bool = false
    @IBInspectable public var cornerRadiusToHeght: CGFloat = 0.5
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func roundCorners(radius: CGFloat) {
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = 1
        if #available(iOS 11.0, *) {
            if(L102Language.currentAppleLanguage().contains("ar")){
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            self.layer.cornerRadius = radius
        } else {
            // Fallback on earlier versions
            self.layer.cornerRadius = radius
        }
        
    }
    
    private var cornerRadius: CGFloat{
        return cornerRadiusToHeght * frame.height
    }
    
    fileprivate func updateCorners() {
        if (upperLeftCornerStyle){
            roundCorners(radius: cornerRadius )
        } else {
            self.layer.borderColor = borderColor?.cgColor
            self.layer.borderWidth = 1
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateCorners()
    }
    
    
    override func prepareForInterfaceBuilder() {
        updateCorners()
    }
    
}
