//
//  MySupportMainViewModel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/23/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift


enum AvailablePhones: String{
    case phone1 = "800 555"
    case phone2 = "+971 2 666 4442"
    case police = "999"
    case ambulance = "998"
    case civilDefense = "997"
    case electricity = "991"
}

class MySupportMainViewModel {
    
    private let phoneNumberVar: Variable<String> = Variable("")
    private let emailVar: Variable<String> = Variable("")
    private let emailService:EmailServiceType
    weak var emailServicePresenter:EmailServiceTypePresenter?
    

    private let phoneCallService:PhoneCallServiceType
    
    
    var phoneNumber: Observable<String>{
        get{
            return phoneNumberVar.asObservable()
        }
    }
    var email: Observable<String>{
        get{
            return emailVar.asObservable()
        }
    }
    
    
    init(emailService:EmailServiceType, phoneCallService:PhoneCallServiceType){
        self.emailService = emailService
        self.phoneCallService = phoneCallService
        
    }
    private(set) var mailErrorToast = ""
    func sendEmail(onCompleted: @escaping ()->()) -> Bool{
        let savedUser:User? = User.getUser()
        
        var userEmail = ""
        
        if(savedUser != nil){
            userEmail = (savedUser?.email) ?? ""
        }
        let mailCanOpen = emailService.sendEmail(to: ["Contact_us@tamm.com"], cc: [userEmail], presenter: emailServicePresenter!, onCompleted: onCompleted)
        if !mailCanOpen {
            mailErrorToast = "make sure your mail client is configured correctly"
        }

        return mailCanOpen
        
//        mailErrorToast = "you seem to be logged in as a guest, please log in as a user to use this feature"
//        return false
    }
    
    func makeAPhoneCall(phone: AvailablePhones) -> Bool {
        return phoneCallService.makeAPhoneCall(number: phone.rawValue)
    }
    
}

