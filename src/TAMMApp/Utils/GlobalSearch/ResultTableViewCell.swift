//
//  ResultTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 9/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryBtn: LocalizedButton!
    
    @IBOutlet weak var titleLbl: LocalizedLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupCell(_ item: SearchResults.SearchResult){
        categoryBtn.setTitle(item.Html, for: .normal)
        titleLbl.text = item.Name
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
