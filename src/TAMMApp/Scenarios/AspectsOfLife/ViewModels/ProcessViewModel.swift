//
//  ProcessViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class ProcessViewModel{
    private(set) var viewData: Variable<ProcessModel?> = Variable(nil)
    private(set) var serviceId: Variable<String?> = Variable(nil)
    fileprivate var apiService: ApiServiceAspectsOfLifeType
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        serviceId.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if serviceId.value != nil && !serviceId.value!.isEmpty{
            apiService.getServiceProcess(id: serviceId.value!)
                .subscribe(onNext: { [unowned self] process in
                    self.viewData.value = process
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
}
