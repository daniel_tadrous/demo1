//
//  TammAolServiceTopic.swift
//  TAMMApp
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class AspectOfLifeServiceTopicModel: Mappable {
    var id: String?
    var title: String?
    var description: String?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id              <- map["id"]
        title           <- map["title"]
        description     <- map["description"]
    }
    
}
