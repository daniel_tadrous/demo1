//
//  OurCalloutView.swift
//  TAMMApp
//
//  Created by kerolos on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import ArcGIS

class OurCalloutView: UIView {

    static let nibName = "OurCalloutView"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var describtionLabel: UILabel!
    @IBOutlet weak var confirmationButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var container: UIView!
    
    var title: String!{
        didSet{
            titleLabel.text = title
//            titleLabel.sizeToFit()
//            titleLabel.setNeedsLayout()
//            self.setNeedsLayout()
//            self.superview?.setNeedsLayout()
//
//            titleLabel.layoutIfNeeded()
//            self.sizeToFit()
//            self.layoutIfNeeded()
//            self.superview?.layoutIfNeeded()

        }
    }
    
    
    var details: String!{
        didSet{
            describtionLabel.text = details
//            describtionLabel.sizeToFit()
//            describtionLabel.setNeedsLayout()
//            self.setNeedsLayout()
//            self.superview?.setNeedsLayout()
//            describtionLabel.layoutIfNeeded()
//            self.sizeToFit()
//            self.layoutIfNeeded()
//            self.superview?.layoutIfNeeded()
        }
    }
    
    func setButtonText(string:String){
        confirmationButton.setTitle(string, for: .highlighted)
        confirmationButton.setTitle(string, for: .normal)
        confirmationButton.setTitle(string, for: .focused)
        confirmationButton.setTitle(string, for: .selected)
    }
    
    func setDetailsTextColor(color:UIColor){
        describtionLabel.textColor = color
    }
    
    var widthConstraint: NSLayoutConstraint!
    
    var buttonClickedAction: (()->())?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func confirmationButtonPressed(_ sender: UIButton) {
        buttonClickedAction?()
        //self.resizeToFitSubviews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.resizeToFitSubviews()
        
    }
    static func loadView() -> OurCalloutView{
        let theView = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)!.first as! OurCalloutView
        
//        theView.widthConstraint = NSLayoutConstraint(for: theView.scrollView, withWidth: 300)
//        theView.scrollView.addConstraint(theView.widthConstraint)
        
        return theView
        
    }
}
