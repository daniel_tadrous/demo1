//
//  MyEventCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class MyEventCell: UITableViewCell {
    
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var addToCalendarContainer: UIView!
    @IBOutlet weak var addToCalendarLabel: LocalizedLabel!
    
    @IBOutlet weak var venuesIconLabelCollapsed: LocalizedLabel!
    @IBOutlet weak var clockIconLabelCollapsed: LocalizedLabel!
    @IBOutlet weak var imageAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var holdingView: UIView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventTime: UILabel?
    @IBOutlet weak var eventTimeExpanded: LocalizedLabel?
    @IBOutlet weak var eventVenues: UILabel?
    @IBOutlet weak var eventVenuesExpanded: LocalizedLabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventDescription: UILabel?
    @IBOutlet weak var eventStartMonth: UILabel!
    @IBOutlet weak var eventStartDay: UILabel!
    @IBOutlet weak var addToCalenderButton: LocalizedButton?
    @IBOutlet weak var eventEndMonth: UILabel!
    @IBOutlet weak var eventEndDay: UILabel!
    @IBOutlet weak var conatinerView: UIView!
    @IBOutlet weak var categoryButton: LocalizedButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var hijriLabel: LocalizedLabel!
    @IBOutlet weak var expandedView: UIView!
    @IBOutlet weak var collapsedView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var clockIconLabel: LocalizedLabel?
    @IBOutlet weak var eventVenuesIconLabel: LocalizedLabel?
    var addToCalenderClosure:(()->Void)!
    var buyTicketsClosure:(()->Void)!
    var isExpanded = false;
    var isReloaded = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addToCalenderAction(_ sender: UIButton) {
        addToCalenderClosure()
    }
    
    @IBAction func buyTicketAction(_ sender: LocalizedButton) {
        buyTicketsClosure()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.addToCalenderButton?.layer.cornerRadius = (self.addToCalenderButton?.frame.height)!/2   
    }
    
    
    func commonInit() {
        conatinerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        roundedView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        categoryLabel.textColor = #colorLiteral(red: 0.9813664556, green: 0.646107614, blue: 0.1420807242, alpha: 1).getAdjustedColor()
        categoryButton.setTitleColor(#colorLiteral(red: 0.9813664556, green: 0.646107614, blue: 0.1420807242, alpha: 1).getAdjustedColor(), for: .normal)
        
        eventTitle.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        clockIconLabel?.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        clockIconLabelCollapsed?.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()

        eventTimeExpanded?.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        eventTime?.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()

        
        eventVenuesIconLabel?.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        venuesIconLabelCollapsed?.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        eventVenuesExpanded?.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        eventVenues?.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        
        
        eventStartDay?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        eventStartMonth?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        eventEndDay?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        eventEndMonth?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        hijriLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        
        
        eventDescription?.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        
        
        addToCalendarContainer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        addToCalenderButton?.BorderColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        addToCalenderButton?.setTitleColor( #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor(), for: .normal)
        addToCalenderButton?.backgroundColor =        #colorLiteral(red: 0.9167622924, green: 0.9743430018, blue: 0.9845786691, alpha: 1).getAdjustedColor()
        
        addToCalendarLabel.textColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        
        
        
        
        categoryButton?.addRoundCorners(radious: 14, borderColor: UIColor.orange.getAdjustedColor())
        
        conatinerView.addRoundCorners(radious: 17)
        
        addToCalenderButton?.Radius = (addToCalenderButton?.frame.height)!/2
//        holdingView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        //addSubview(holdingView)
//        print(self.frame)
//        print(holdingView.frame)
//        holdingView.frame = self.bounds
//        print(holdingView.frame)
//        holdingView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //setConstraints(view: self, childView: holdingView)
    }
    
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func expand() {
        self.bottomConstraint.priority = .defaultLow
        self.stackTopConstraint.constant = -48
        self.collapsedView.isHidden = true
        self.expandedView.isHidden = false
    }
    
    func collapse() {
        self.bottomConstraint.priority = .defaultHigh
        
        self.stackTopConstraint.constant = -118
        self.collapsedView.isHidden = false
        self.expandedView.isHidden = true
    }
    
    
    
    public func applyColorTheme(){
        commonInit()
    }
}
