//
//  EventsViewController.swift
//  TAMMApp
//
//  Created by Daniel on 6/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage


class EventsViewController: TammViewController,EventsStoryboardLodable {
   // let collapsedIdentifier = "EventCellCollapsed"
    let expandedIdentifier = "EventCellExpended"
    public var viewModel:EventsViewModel!
    private var disposeBag = DisposeBag()
 
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var eventsTableView: UITableView!
    var events: [Event]{
        get{
            return viewModel.viewData.value ?? []
        }
    }
    
    
    @IBOutlet weak var searchIconLabel: UILabel!
    private var textFromMic:Bool =  false
    private let debounceTimeInSec: Double = 0.5
    public weak var eventsSearchOverlayView:EventsSearchOverlayView!
    
    @IBAction func OnFilterClickHandler(_ sender: UIButton) {
        let filterView = EventsFilterView(frame: UIScreen.main.bounds, viewModel: viewModel) { (eventsFilter:EventsFilterApplyModel ) in
            self.viewModel.eventsFilterApplyModel.value = eventsFilter
        }
        UIApplication.shared.keyWindow!.addSubview(filterView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: filterView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(title: L10n.MyCommunity.getCommunityItemText(item: .CommunityEvents), willSetSearchButton: true)
        self.searchView.addRoundCorners(radious: 30)
        self.searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClick)))
        setupHooks()
    }
    
    @IBOutlet weak var filterIvonLabel: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9529411765, alpha: 1).getAdjustedColor()
        searchView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        searchIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        searchTF.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
        
        filterIvonLabel.setTitleColor( #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        eventsTableView.reloadData()
        
        
    }
 
    func setupHooks(){
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] events in
                if(events == nil){
                    //TODO: should do somting ????
                }else{
                     self.eventsTableView.reloadData()
                    
                }
            })
            .disposed(by: disposeBag)
       
    }
    
    @objc func searchClick(){
        let searchView = EventsSearchOverlayView(frame: UIScreen.main.bounds, eventsViewModel: self.viewModel,text:self.searchTF.text!)
        searchView.delegate = self
        UIApplication.shared.keyWindow!.addSubview(searchView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchView)
        
    }
    
}
extension EventsViewController : EventsSearchOverlayViewDelegate{
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchTF.text = message.name
        let eventSearch = EventsFilterApplyModel()
        eventSearch.q = message.name
        self.viewModel.eventsFilterApplyModel.value = eventSearch
    }
    func backPressed(){
        self.searchTF.text = ""
    }
}

extension EventsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event: Event = events[indexPath.row]
        var cell: EventCell
       // if event.isExpanded{
            cell = tableView.dequeueReusableCell(withIdentifier: expandedIdentifier) as! EventCell
        
        cell.commonInit()
        
        
        
        cell.dateLb.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        cell.titleLb.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        cell.titleLb.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        cell.titleLb.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        
        
        
            cell.descrLb?.text = event.description
            cell.shareClickHandlerClosure = {
                // set up activity view controller
                self.viewModel.share(viewController: self, text: event.link!)
            }
            cell.addToMyEventsClickHandlerClosure = {
                self.viewModel.addToCalendar(event:event)
            }
            cell.findOutMoreClickHandlerClosure = {
                Toast(message:L10n.commingSoon).Show()
            }
       // }else{
       //     cell = tableView.dequeueReusableCell(withIdentifier: collapsedIdentifier) as! EventCell
       // }
        if !event.isExpanded{
            cell.bottomConstraint.priority = .defaultHigh
        }
        ImageCachingUtil.loadImage(url: URL(string: (event.image)!)!, dispatchInMain: true
            , callBack: { (url, image) in
                cell.img.image = image
                let const = cell.imageAspectRatioConstraint.constraintWithMultiplier(image!.size.width/image!.size.height)
                cell.img.removeConstraint(cell.imageAspectRatioConstraint)
                cell.imageAspectRatioConstraint = const
                cell.img.addConstraint(const)
                if !event.isImageLoaded{
                    event.isImageLoaded = true
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
        })
        cell.categoryLb.text = event.category?.title?.uppercased()
        cell.dateLb.text = event.getFormattedDate()
        cell.titleLb.text = event.title
        cell.containerView.addRoundCorners(radious: 17)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event: Event = events[indexPath.row]
        event.isExpanded = !event.isExpanded
        //let animation = event.isExpanded ? UITableViewRowAnimation.bottom : UITableViewRowAnimation.top
        let cell = tableView.cellForRow(at: indexPath) as! EventCell
        
       
        if event.isExpanded{
            cell.expand()
        }else{
            cell.collapse()
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 36
    }
    
    
}
