//
//  SearchSuggestions.swift
//  TAMMApp
//
//  Created by Daniel on 9/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchSuggestions: Mappable {
    
    var Results:[SearchSuggestion] = []
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        Results            <- map["Results"]
    }
    
    class SearchSuggestion: Mappable {
        
        var Term: String?
        var Payload: String?
        var Html: String?
        
        required init?(map: Map) {
            
        }
        
        // Mappable
        func mapping(map: Map) {
            Term            <- map["Term"]
            Payload            <- map["Payload"]
            Html            <- map["Html"]
        }
    }
}
