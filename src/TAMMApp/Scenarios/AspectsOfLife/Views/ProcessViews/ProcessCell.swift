//
//  OnGoingAspectsOfLifeCell.swift
//  TammUI
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import UIKit

class ProcessCell: UITableViewCell {
    
    @IBOutlet weak var bulletsTabelView: UITableView?
    var bullets: [String]? = [String]()
    @IBOutlet weak var textFormatLabel: LocalizedLabel?
    @IBOutlet weak var bulletTitleLabel: LocalizedLabel?
    var bulletCellHeight = 30
    @IBOutlet weak var bulletTableHeight: NSLayoutConstraint?
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var stepTitleLabel: UILabel!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        
        self.autoresizingMask = [.flexibleWidth]
        self.backgroundColor = UIColor.paleGrey
      
        self.contentView.layer.shadowColor = UIColor.lightGray.cgColor;
        self.contentView.layer.shadowOpacity=0.5;
        self.contentView.layer.shadowRadius=2.0;
        self.contentView.layer.shadowOffset = CGSize(width:0, height:0);
        self.contentView.layer.masksToBounds = false;
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        self.bgView.addDashedBorder()
        self.footerView.addDashedBorder()
    }
    
}
extension ProcessCell : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.bullets?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BulletCell")
        
        let label = cell?.viewWithTag(1) as? UILabel
        
        label?.attributedText = (self.bullets?[indexPath.row])!.getAttributedString()
        let iconLabel = cell?.viewWithTag(2) as? UILabel
        iconLabel?.text = "\u{e015}"
        iconLabel?.textColor = UIColor.coolGrey
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(bulletCellHeight)
    }
    
}
