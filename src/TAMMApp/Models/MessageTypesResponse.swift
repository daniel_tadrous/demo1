//
//  MessageTypesResponse.swift
//  TAMMApp
//
//  Created by kerolos on 4/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageTypesResponse : Mappable{
    
    var types: [MessageTypeModel]?
    var selectedId: Int?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        types <- map["types"]
        selectedId <- map["selectedId"]
    }
    
    
}
