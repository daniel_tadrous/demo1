//
//  TalkToUsViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 4/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift



protocol TalkToUsItemViewDataType {
    var name: String {get}
}
protocol TrendingItemViewDataType: TalkToUsItemViewDataType{
}
protocol PoppularItemViewDataType: TalkToUsItemViewDataType{
}
protocol QueryResultItemItemViewDataType: TalkToUsItemViewDataType{
}


class TalkToUsLandingViewModel{
    let speechDuration: Double = 10.0
    //items.append("How to apply for a visa?")
    //items.append("Start a business?")
    //items.append("We need more parking slots in Abu Dhabi")
    //items.append("Military service registration")
    //items.append("We need ")
    //items.append("parking slots in Abu Dhabi")
    private var speechDoneSubject: PublishSubject<String?>?
    func startSpeechService() -> Observable<String?>{
        speechRecognitionService.start(delegate: self, duration: speechDuration)
        speechDoneSubject = PublishSubject<String?>()
//        micRecognizedVar.value = ""
        return (speechDoneSubject!)
    }
    private let messagesTypesVar: Variable<MessageTypesResponse?> = Variable(nil)
    
    var messagesTypes: Observable<MessageTypesViewDataType?>
    {
        get{
            return self.messagesTypesVar.asObservable().map( { (res: MessageTypesResponse?) -> MessageTypesViewDataType? in
                guard let res = res  else {
                    return nil
                }
                
                let types = res.types?.map({ (type: MessageTypeModel) -> MessageTypeViewDataType in
                    return MessageTypeViewData(messageName: (type.text)!, messageId: (type.id)!)
                })
                // whe n the API returns -1 it means no Suggested type for the given title query
                // nil means no selection id found
                return MessageTypesViewData(types: types, selectedId: res.selectedId == -1 ? nil : res.selectedId)
            })
        }
    }
    
    var popular: Observable<[PoppularItemViewDataType]?> {
        get {
            return popularVar.asObservable().map({ (arr) -> [PoppularItemViewDataType]? in
                return arr?.map({ (str) -> PoppularItemViewDataType in
                    return PoppularItemViewData(name: str)
                })
            })
        }
    }
    var trending: Observable<[TrendingItemViewDataType]?> {
        get {
            return trendingVar.asObservable().map({ (arr) -> [TrendingItemViewDataType]? in
                return arr?.map({ (str) -> TrendingItemViewDataType in
                    return TrendingItemViewData(name: str)
                })
            })
        }
    }
    var queryResult: Observable<[QueryResultItemItemViewDataType]?> {
        get {
            return queryResultVar.asObservable().map({ (arr) -> [QueryResultItemItemViewDataType]? in
                return arr?.map({ (str) -> QueryResultItemItemViewDataType in
                    return QueryResultItemItemViewData(name: str)
                })
            })
        }
    }
    
    var currentQuery: String?{
        didSet{
            
            currentQueryVar.value = currentQuery
        }
    }
    
    func reloadPopular(){
        getPopular(userId: userId.value)
    }
    
    
    private let currentQueryVar : Variable<String?> = Variable(nil)
    
    
    private let supportApiService: ApiSupportServicesType
    private let userConfigService: UserConfigServiceType
    private let speechRecognitionService: SpeechRecognitionServiceType
    private let apiMessageServicesTypes:ApiMessageServicesTypes
    //    private let micRecognizedVar: Variable<String?> = Variable(nil)
    
//    private let micRecodingIsEnabledVar: Variable<Bool?> = Variable(true)
    
//    private let micRecordingErrorVar: Variable<String?> = Variable(nil)
    
    private let disposeBag =  DisposeBag()
    // can be accesed in some cases but currently we update its value from the user configuration service
    private var userId : Variable<Int> = Variable(-1)
    
    
    init(supportApiService: ApiSupportServicesType ,speechRecognitionService: SpeechRecognitionServiceType, userConfigService: UserConfigServiceType, apiMessageServicesTypes:ApiMessageServicesTypes) {
        
        self.userConfigService = userConfigService
        self.supportApiService = supportApiService
        self.speechRecognitionService = speechRecognitionService
        self.apiMessageServicesTypes = apiMessageServicesTypes
        userId.value = userConfigService.getUserId()
        
        
        setUpBinding()
    }
    
    private func setUpBinding(){
        self.messagesTypesVar.value = apiMessageServicesTypes.getStaticMessageTypes()
        userId.asObservable().subscribe(onNext:{ [unowned self] userId in
            self.getPopular(userId: userId)
            // the following does not depend on userId
            self.getTrending(userId: userId)
        }).disposed(by: disposeBag)
        
        
        //    .throttle(3, latest: false, scheduler: MainScheduler.instance)
        
        currentQueryVar.asObservable().subscribe(onNext:{ [unowned self] query in
            self.searchFor(userId: self.userId.value,  query:query)
        }).disposed(by: disposeBag)
        
        
    }
    
    
    
    //    # inistializing with empty string arrays
    private let popularVar : Variable<[String]?> = Variable(nil)
    private let trendingVar : Variable<[String]?> = Variable(nil)
    private let queryResultVar : Variable<[String]?> = Variable(nil)
    
    
    
    private func getPopular(userId: Int){
        supportApiService.getPopularSearchMessages(userId: userId).subscribe(onNext:{
            [unowned self] popArr in
            self.popularVar.value = popArr
        }).disposed(by: disposeBag)
    }
    
    private func getTrending(userId: Int){
        supportApiService.getTrendingSearchMessages(userId: userId).subscribe(onNext:{
            [unowned self] trendArr in
            self.trendingVar.value = trendArr
        }).disposed(by: disposeBag)
    }
    
    private func searchFor(userId: Int, query: String?){
        if query != nil && !(query?.isEmpty)!{
            supportApiService.searchForMessages(userId: userId, query: query!).subscribe(onNext:{
                [unowned self] searchResultArr in
                
                self.queryResultVar.value = searchResultArr
            }).disposed(by: disposeBag)
        }
        else{
            // empty the results in case of no input
            // do not call the api
            self.queryResultVar.value = []
        }
    }
    
    
    
}
extension TalkToUsLandingViewModel: SpeechRecognitionServiceTypeDelegate{
    func recognizerAvailable(isAvailable: Bool) {
        // indecates that the recognizer is available and most likely will start
        // should inform the subscribers with the change
        // disable the recording for some time
        if( isAvailable){
//            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onNext("")

        }
        else{
//            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onError("Service is not currently available" as! Error)
        }
        
    }
    
    func textRecived(text: String) {
//        micRecognizedVar.value = text
        speechDoneSubject?.onNext(text)
    }
    
    func recognitionIsDone(_ error: Error?) {
        // should inform the ui with the status and reset the availablility
//        micRecodingIsEnabledVar.value = true
//        micRecordingErrorVar.value = "\(String(describing: error))"
//
        speechDoneSubject?.onCompleted()
    }
    
    
}

struct TrendingItemViewData: TrendingItemViewDataType{
    var name: String = ""
}

struct PoppularItemViewData: PoppularItemViewDataType{
    var name: String = ""
}


struct QueryResultItemItemViewData: QueryResultItemItemViewDataType{
    var name: String = ""
}

