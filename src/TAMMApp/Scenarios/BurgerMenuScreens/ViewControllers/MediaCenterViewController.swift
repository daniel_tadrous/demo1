//
//  MediaCenterViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol MediaCenterViewControllerDelegate {
    func openNews()
    func openEvents()
    func openPressReleases()
    func openSocial()
}

class MediaCenterViewController: TammViewController,MediaCenterStoryboardLoadable {
    
    @IBOutlet weak var mediaTableView: UITableView!
    
    @IBOutlet weak var searchView: TammSearchView!
    
    var delegate:MediaCenterViewControllerDelegate?
    
    
    
    let headerViewIdentifier = "HeaderCell"
    let eventsIdentifier = "EventsCell"
    let socialIdentifier = "SocialCell"
    let pressIdentifier = "pressCell"
    
    public var viewModel:MediaCenterViewModel!
    private var disposeBag = DisposeBag()
    
    var mediaObject: MediaCenterModel{
        get{
            return viewModel.mediaReleases.value ?? MediaCenterModel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: L10n.mediaCenter, willSetSearchButton: true)
        
        mediaTableView.delegate = self
        mediaTableView.dataSource = self
        
        let bundle = Bundle(for: type(of: self))
        mediaTableView.register(UINib(nibName: "MediaCenterHeaderCell", bundle: bundle), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        mediaTableView.register(UINib(nibName: "MediaCenterEventsCell", bundle: bundle), forCellReuseIdentifier: eventsIdentifier)
        mediaTableView.register(UINib(nibName: "MediaCenterSocialCell", bundle: bundle), forCellReuseIdentifier: socialIdentifier)
        mediaTableView.register(UINib(nibName: "MediaCenterPressReleaseCell", bundle: bundle), forCellReuseIdentifier: pressIdentifier)
        setupHooks()
    }
    
    
}

extension MediaCenterViewController:UITableViewDelegate, UITableViewDataSource{
    
    func setupHooks(){
        viewModel!.mediaReleases.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] mediaRes in
                if(mediaRes == nil){
                }else{
                    print(mediaRes)
                    self.mediaTableView.reloadData()
                }
            })
            .disposed(by: disposeBag)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier) as? MediaCenterHeaderCell
        headerView?.backgroundColor = UIColor.paleGreyTwo
        if section == 0 {
            headerView?.title.text = L10n.news
            headerView?.onClick = self.goToNews
        }
        if section == 1 {
            headerView?.title.text = L10n.events
            headerView?.onClick = self.goToEvents
        }
        if section == 2 {
            headerView?.title.text = L10n.social
            headerView?.onClick = self.goToSocial
        }
        if section == 3 {
            headerView?.title.text = L10n.pressRelease
            headerView?.onClick = self.goToPressReleases
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = mediaObject.events?.count{
            if section == 3 {
                return 2
            }
            else{
                return 1
            }
        }
        return 0
    }
    
    func nothing() {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 2{
            var cell = tableView.dequeueReusableCell(withIdentifier: socialIdentifier) as! MediaCenterSocialCell
//            cell.webView1.loadHTMLString(mediaObject.social![0], baseURL: nil)
//            cell.webView2.loadHTMLString(mediaObject.social![1], baseURL: nil)
            cell.webView1.attributedText = mediaObject.social![0].htmlToAttributedString
            cell.webView2.attributedText = mediaObject.social![0].htmlToAttributedString
            return cell
        }
        else if indexPath.section == 3{
            var cell = tableView.dequeueReusableCell(withIdentifier: pressIdentifier) as! MediaCenterPressReleaseCell
            var release = mediaObject.pressReleases![indexPath.row]
            cell.release = release
            cell.titleLabel.text = release.title
            cell.dateLabel.text = release.getFormattedDate()
            cell.sizeLabel.text = release.size
            cell.descriptionLabel.text = release.excerpt
            cell.url = release.url
            cell.fileName = String(release.title!)
            cell.downloadClosure = {url,fileName in
                self.downloadFile(url: url, name: fileName, onCompletion: {
                    release.isDownloading = false
                    tableView.reloadRows(at: [indexPath], with: .none)
                })
            }
            if release.isDownloading{
                cell.downloadIndicator.startAnimating()
                cell.btnDownload.isEnabled = false
            }else{
                cell.downloadIndicator.stopAnimating()
                cell.btnDownload.isEnabled = true
            }
            return cell
        }
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: eventsIdentifier) as! MediaCenterEventsCell
            setCellFor(cell: cell, section: indexPath.section, ofOrder: 0)
            setCellFor(cell: cell, section: indexPath.section, ofOrder: 1)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func setCellFor(cell:MediaCenterEventsCell, section:Int, ofOrder index:Int) {
        if section == 1 { //events
                let event = mediaObject.events?[index]
                cell.eventViews[index].venuesView.isHidden = false
                ImageCachingUtil.loadImage(url: URL(string: event!.image!)!, dispatchInMain: true
                    , callBack: { (url, image) in
                        cell.eventViews[index].mediaImage.image = image
                })
            
                if L10n.Lang == "en" {
                    cell.eventViews[index].typeLabel.text = L10n.events.uppercased()
                }else{
                    cell.eventViews[index].typeLabel.text = L10n.events
                }
                cell.eventViews[index].typeButton.addRoundAllCorners(radious: 5, borderColor:UIColor.orange)
                cell.eventViews[index].typeLabel.textColor = UIColor.orange
            
            cell.eventViews[index].startDay.attributedText = viewModel.getAttributedText(event: event!,days:(event?.getFormattedStartDay())!,month:(event?.getFormattedStartMonth(monthFormatOfthree: false))!)
                
                cell.eventViews[index].startMonth.text = ""
            
                if event?.getFormattedEndDay() != ""{
                    cell.eventViews[index].endDay.attributedText = viewModel.getAttributedText(event: event!,days:(event?.getFormattedEndDay())!,month:(event?.getFormattedEndMonth(monthFormatOfthree: false))!)
                    cell.eventViews[index].endMonth.text =  ""
                }
                else{
                    cell.eventViews[index].endDateView.isHidden = true
                }
                cell.eventViews[index].descriptionLabel.text = event!.description
                if event!.addresses!.count > 1 {
                cell.eventViews[index].venuesLabel.text = "Multiple Venues"
                }
                else{
                    cell.eventViews[index].venuesLabel.text = event!.addresses![0].description
                }
            
        }
        else{ // news
            let event = mediaObject.news?[index]
            cell.eventViews[index].venuesView.isHidden = true
            ImageCachingUtil.loadImage(url: URL(string: event!.image!)!, dispatchInMain: true
                , callBack: { (url, image) in
                    cell.eventViews[index].mediaImage.image = image
            })
            if L10n.Lang == "en" {
                cell.eventViews[index].typeLabel.text = L10n.news.uppercased()
            }else{
                cell.eventViews[index].typeLabel.text = L10n.news
            }
            
            
            cell.eventViews[index].typeButton.addRoundAllCorners(radious: 5, borderColor: UIColor.petrol)

            cell.eventViews[index].typeLabel.textColor = UIColor.petrol
            
            cell.eventViews[index].startDay.text = ""
            cell.eventViews[index].startMonth.text = event?.getFormattedDate()
            cell.eventViews[index].endDateView.isHidden = true
            cell.eventViews[index].descriptionLabel.text = event!.title
        }
        cell.eventViews[index].layoutSubviews()
        
    }
    

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        mediaTableView.reloadSections( [1], with: .none)
    }
    
}
extension MediaCenterViewController{ // to add functionalities
    
    @objc func goToNews() {
        delegate?.openNews()
    }
    @objc func goToEvents() {
        delegate?.openEvents()
    }
    @objc func goToPressReleases() {
        delegate?.openPressReleases()
    }
    @objc func goToSocial() {
        delegate?.openSocial()
    }
    func downloadFile(url: String, name:String,onCompletion: @escaping ()->Void){
        viewModel.downloadPDF(url: url, name: name, onCompletion: { downloadedUrl in
                onCompletion()
            self.openUrl(withUrl: downloadedUrl)
        })
        
    }
    func openUrl(withUrl url:URL){
        let docController:UIDocumentInteractionController = UIDocumentInteractionController.init(url: url)
        docController.delegate = self
        hideFloatingMenu()
        docController.presentPreview(animated: true)
    }
    
}
extension MediaCenterViewController : UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        displayFloatingMenu()
    }
}



