//
//  DALDrafts.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RealmSwift


class DALDrafts: Object{
    
    override static func primaryKey() -> String? {
        return "draftId"
    }
    
    @objc dynamic var draftId:String = UUID().uuidString
    @objc dynamic var draftTitle:String?
    @objc dynamic var typeId:Int = 0
    
    var mediaUrls = List<MediaViewModelData>()
    var mediaUris: [MediaViewModelData]{
        get{
            var arr = [MediaViewModelData]()
            self.mediaUrls.forEach { (item) in
                arr.append(item)
            }
            return arr
        }
    }
    @objc dynamic var voiceUri: String?
    @objc dynamic var comments: String?
    
    
    @objc dynamic var location:String?
    @objc dynamic var locationCoordinates: String?
    
    @objc dynamic var hasMedia:Bool = false
    @objc dynamic var isInformationPublic:Bool = false
    @objc dynamic var notifiedByEmail:Bool = false
    @objc dynamic var notifiedByMobileApplication:Bool = false
    @objc dynamic var notifiedByPhone:Bool = false
    @objc dynamic var draftDate:Date?
    @objc dynamic var long:Double = 0
    @objc dynamic var lat:Double = 0
    
    convenience init(draftTitle:String , typeId: Int ,location:String, locationCoordinates: String?, mediaURis media:[MediaViewModelData] , hasMedia:Bool , isInformationPublic:Bool , notifiedByEmail:Bool , notifiedByMobileApplication:Bool , notifiedByPhone:Bool , draftDate:Date, voiceNote: String, comment: String,long:Double, lat:Double ) {
        self.init()
        
        self.draftTitle = draftTitle
        self.typeId = typeId
        self.location = location
        media.forEach { (item) in
            self.mediaUrls.append(item)
        }
        
        self.hasMedia = hasMedia
        self.isInformationPublic = isInformationPublic
        self.notifiedByEmail = notifiedByEmail
        self.notifiedByMobileApplication = notifiedByMobileApplication
        self.notifiedByPhone = notifiedByPhone
        self.draftDate = draftDate
        self.locationCoordinates = locationCoordinates
        self.voiceUri = voiceNote
        self.comments = comment
        self.long = long
        self.lat = lat
    }
    
    class func getAll()->[DALDrafts]{
        let realm = try! Realm()
        
        let drafts = realm.objects(DALDrafts.self)
        return drafts.map({ (item) -> DALDrafts in
            item
        })
    }
    class func getById(id: String)->DALDrafts?{
        let realm = try! Realm()
        let draft = realm.object(ofType: DALDrafts.self, forPrimaryKey: id) // (DALDrafts.self).filter("draftId = \(id)")

        return draft
    }
    
    func save(){
        do {
        let realm = try! Realm()
            try realm.write {
                realm.add(self, update: true)
            }
        }
        catch let e as Realm.Error{
            print( e )
        }
        catch let e{
            print( e )
        }
    }
    
    func delete()->Bool{
        do {
            let realm = try! Realm()
            try realm.write {
                realm.delete(self)
            }
        }
        catch let e as Realm.Error{
            print( e )
            return false
        }
        catch let e{
            print( e )
            return false
        }
        return true
    }
    
    func updateAddress(newAddress:String)->Bool{
        do {
            let realm = try! Realm()
            try realm.write {
                
                self.location = newAddress
               // realm.delete(self)
            }
        }
        catch let e as Realm.Error{
            print( e )
            return false
        }
        catch let e{
            print( e )
            return false
        }
        return true
    }
    
    

}
