//
//  DownloadMapViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import ArcGIS
import RxSwift
// service Url https://arcgis.sdi.abudhabi.ae/arcgis/rest/services/Pub/BaseMapEng_LightGray_WM/MapServer

// url offline map https://arcgis.sdi.abudhabi.ae/portal/home/item.html?id=33dd40591f924f3291d53a6f779909d1

class DownloadMapViewController: TammViewController , DownloadMapStoryboardLoadable {

    @IBOutlet weak var mapView: AGSMapView!
    
    @IBOutlet weak var errorLabel: LocalizedLabel!

    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet var redBorders: [UIView]!
    
    @IBOutlet weak var cancelBtn: LocalizedButton!
    
    @IBOutlet weak var downloadBtn: LocalizedButton!
    
    @IBOutlet weak var loadingTxt: LocalizedLabel!
    private   var tiledLayer:AGSArcGISTiledLayer! = AGSArcGISTiledLayer(url: URL(string: "https://arcgis.sdi.abudhabi.ae/arcgis/rest/services/Pub/BaseMapEng_LightGray_WM/MapServer")!)
    private  var job:AGSExportTileCacheJob!
    private  var exportTask:AGSExportTileCacheTask!
    
    private var isPositionInAbuDhabi = Variable(true)
    private var redBordersAppear = Variable(false)
    private var downloadprogress: Variable<Int?> = Variable(nil)
    let disposeBag = DisposeBag()

    private let country =  "ARE"
    private let lang = L102Language.currentAppleLanguage().contains("ar") ? "AR": "EN"
    private weak var timer: Timer?
    
    private var filterText: [String]{
        return ConstantStrings.suggestionIncludeOneOrMoreFilter
    }
    
    // for Extension
    private  var geocodeParameters: AGSGeocodeParameters!
    private  var reverseGeocodeParameters: AGSReverseGeocodeParameters!
    private  var locatorTask: AGSLocatorTask!
    private  var locatorTaskOperation: AGSCancelable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
        redBordersState(ishidden:true)
        initialize()
        showMap()
        setupUI()
        addTapGetsuresOnMap()
        self.loadingView.isHidden = false
        
    }
    
    
    fileprivate func addTapGetsuresOnMap() {
        let whenClickmap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        whenClickmap.numberOfTapsRequired = 2
        //    let whenLongClickmap = UILongPressGestureRecognizer(target: self, action: #selector(handleLongpress(sender:)))
        let swipeOnMap = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        let zoomOnMap = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)))
        self.mapView.addGestureRecognizer(whenClickmap)
        //  self.mapView.addGestureRecognizer(whenLongClickmap)
        self.mapView.addGestureRecognizer(swipeOnMap)
        self.mapView.addGestureRecognizer(zoomOnMap)
    }
    
    
    fileprivate func setupUI() {
        self.downloadBtn.layer.borderWidth = 1
        self.downloadBtn.layer.borderColor = UIColor.turquoiseBlue.getAdjustedColor().cgColor
        self.downloadBtn.addRoundCorners(radious: (self.downloadBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.removeFloatingMenu()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    fileprivate func showMap() {
       // self.tiledLayer = AGSArcGISTiledLayer(url: URL(string: "https://arcgis.sdi.abudhabi.ae/arcgis/rest/services/Pub/BaseMapEng_LightGray_WM/MapServer")!)
        let map = AGSMap(basemap: AGSBasemap(baseLayer: self.tiledLayer))
        self.showloadingView(isShown: false)
        self.mapView.map = map
        
        mapView.interactionOptions.isRotateEnabled = false
        
        //mapView.interactionOptions.zoomFactor =
        self.mapView.map?.load(completion: { [weak self] _ in
            self?.loadingView.isHidden = true
            
//
//            val leftPoint = Point( 54.336827, 24.441573, SpatialReference.create(4326))
//            val rightPoint = Point( 54.450068, 24.493446, SpatialReference.create(4326))
//            let centerPoint = AGSPoint(x: 54.398070, y: 24.422535,  spatialReference: AGSSpatialReference(wkid: 4326))
//
//            let rightPoint = AGSPoint(x: 55.250976, y: 23.989154,  spatialReference: AGSSpatialReference(wkid: 4326))
            
            let centerPoint = AGSPoint(x: 54.336827, y: 24.441573,  spatialReference: AGSSpatialReference(wkid: 4326))
            
            let rightPoint = AGSPoint(x: 54.450068, y: 24.493446,  spatialReference: AGSSpatialReference(wkid: 4326))
            let vp = AGSViewpoint(targetExtent: AGSEnvelope(min: centerPoint, max: rightPoint))
            
            self?.mapView.setViewpoint(vp, duration: 3, completion: nil)
        })
        
       
    }
    
    
    @objc func handlePan(sender: UIPanGestureRecognizer) {
        timer?.invalidate()
        if loadingView.isHidden == false {
            if sender.state == .ended {
                // handling code
                startTimer()
            }
        }
        
    }
    
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        timer?.invalidate()
        if loadingView.isHidden == false {
            
                if sender.state == .ended {
                    // handling code
                    startTimer()
                }
        }
       
    }
    
//    @objc func handleLongpress(sender: UILongPressGestureRecognizer) {
//        if sender.state == .ended {
//            // handling code
//            startTimer()
//        }
//    }
    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        timer?.invalidate()
        if loadingView.isHidden == false {
            
            if sender.state == .ended {
                // handling code
                startTimer()
            }
        }
        
    }
    
    
    func startTimer() {
        
        timer = Timer.scheduledTimer(timeInterval: 2.0,
                                     target: self,
                                     selector: #selector(isLocarionRightRequest(timer:)),
                                     userInfo: nil,
                                     repeats: false)
    }
    
    // Timer expects @objc selector
    @objc func isLocarionRightRequest(timer: Timer!) {
        let point = mapView.visibleArea?.extent.center
        print(point)
        timer.invalidate()
        reverseGeocode(point:point!)
    }
    
    
    
    
    
    
    fileprivate func setupBinding() {
        isPositionInAbuDhabi.asObservable().subscribe(onNext: { [unowned self] positionRight in
            
            if positionRight {
                self.downloadBtn.isHidden = false
                self.redBordersAppear.value = true
                self.errorLabel.text = ""
            }else {
                self.downloadBtn.isHidden = true
                self.redBordersAppear.value = false
                self.errorLabel.text = "Choose a valid Location"
            }
            
        }).disposed(by: disposeBag)
        
        redBordersAppear.asObservable().subscribe(onNext: { [unowned self] isAppeared in
            
            self.redBordersState(ishidden:isAppeared)
        }).disposed(by: disposeBag)
    }
    
    func frameToExtent() -> AGSEnvelope {
        let frame = self.mapView.convert(self.loadingView.frame, from: self.view)
        
        let minPoint = self.mapView.screen(toLocation: frame.origin)
        let maxPoint = self.mapView.screen(toLocation: CGPoint(x: frame.origin.x+frame.width, y: frame.origin.y+frame.height))
        let extent = AGSEnvelope(min: minPoint, max: maxPoint)
        return extent
    }
    
 
    
    fileprivate func redBordersState(ishidden:Bool) {
        for border in redBorders {
            border.isHidden = ishidden
        }
    }
    
//    private func cancelDownload() {
//        if self.job != nil {
//            SVProgressHUD.dismiss()
//            //TODO: Cancel the job when the API is available
//            //self.job.cancel
//            self.job = nil
//            //self.downloading = false
//           // self.visualEffectView.isHidden = true
//        }
//    }
    
    private func initiateDownload() {
        deleteTpkFile()
        //get the parameters by specifying the selected area,
        //mapview's current scale as the minScale and tiled layer's max scale as maxScale
        let minScale = self.mapView.mapScale
        let maxScale = self.tiledLayer.maxScale
        
        //TODO: Remove this code once design has been udpated
//        if minScale == maxScale {
//            SVProgressHUD.showError(withStatus: "Min scale and max scale cannot be the same")
//            return
//        }
        
        //set the state
       // self.downloading = true
        
        //delete previous existing tpks
       // self.deleteAllTpks()
        
        //initialize the export task
        self.exportTask = AGSExportTileCacheTask(url: self.tiledLayer.url!)
        //self.mapView.mapScale  self.tiledLayer.maxScale
        self.showloadingView(isShown:true)
        self.exportTask.exportTileCacheParameters(withAreaOfInterest: self.frameToExtent(), minScale: 9000000, maxScale: 45000) { [weak self] (params: AGSExportTileCacheParameters?, error: Error?) in
            
            if let error = error {
              print(error)
                self?.showloadingView(isShown:false)
            }
            else {
                self?.exportTilesUsingParameters(params!)
            }
        }
    }
    

    private func exportTilesUsingParameters(_ params: AGSExportTileCacheParameters) {
        //destination path for the tpk, including name
        let path = IncidentUrl.getDirectoryForIncident().path
        let destinationPath = "\(path)/map.tpk"
        
        //get the job
        self.job = self.exportTask.exportTileCacheJob(with: params, downloadFileURL: URL(string: destinationPath)!)
        
        //run the job
        self.job.start(statusHandler: { [unowned self] (status: AGSJobStatus) -> Void in
            
            //self.showloadingView(isShown:true)
            //update Loading Text
            self.loadingTxt.text = "Downloading map ... \(self.job.progress.completedUnitCount)%"
            //show job status
            
            print(self.job.progress)
           // SVProgressHUD.show(withStatus: status.statusString())
        }) { [weak self] (result: AnyObject?, error: Error?) -> Void in
        //    self?.downloading = false
            
            if let error = error {
                self?.showloadingView(isShown: false)
                print(error)
              //  SVProgressHUD.showError(withStatus: (error as NSError).localizedFailureReason)
            }
            else {
                self?.showloadingView(isShown: false)
                //hide progress view
             //   SVProgressHUD.dismiss()
               // self?.visualEffectView.isHidden = false
                
               // let tileCache = result as! AGSTileCache
                //let newTiledLayer = AGSArcGISTiledLayer(tileCache: tileCache)
               // self?.previewMapView.map = AGSMap(basemap: AGSBasemap(baseLayer: newTiledLayer))
            }
        }
    }
    
    //show loading View
    fileprivate func showloadingView(isShown:Bool) {
         self.loadingView.isHidden = !isShown
         self.downloadBtn.isHidden = isShown
         self.cancelBtn.isHidden = isShown
    }
    
   
    
    
    
    @IBAction func cancelAction(_ sender: Any) {
        //self.mapView.map = AGSMap()
        self.mapView.removeFromSuperview()
        dismiss(animated: false, completion: {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.addfloatingMenu()
        })

    }
    
    
    
    @IBAction func downloadAction(_ sender: Any) {
        // prepare data for download
        initiateDownload()
    }
    
    
    // delete previous map
    fileprivate func deleteTpkFile() {
        var removedUrl:URL!
        let documentsURL = IncidentUrl.getDirectoryForIncident()
        let docs = try? FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
        for item in docs!  {
            if item.pathExtension.caseInsensitiveCompare("tpk") == ComparisonResult.orderedSame {
                removedUrl = item
                break
            }
        }
        if removedUrl == nil {
            return
        }
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: removedUrl)
        }
        catch let error as NSError {
            print("Something went wrong: \(error)")
        }
    }
    
}



extension DownloadMapViewController {
    
    
    fileprivate func containsAnyOfFilterText(_ label: String) -> Bool{
        print(label)
        let label = label.lowercased()
        for filterItem in self.filterText{
            let stringMatch = label.contains(filterItem)
            if stringMatch {
                return true
            }
        }
        return false
    }
    
    
    private func initialize(){
        //initialize geocode params
        self.geocodeParameters = AGSGeocodeParameters()
        //        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["*"])
        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["Match_addr"])
        //        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["LongLabel"])
        // will return alot of results it was 75 but I will try 50 for a while
        self.geocodeParameters.minScore = 50
        self.geocodeParameters.outputLanguageCode = lang
        self.geocodeParameters.countryCode = country
        
        //initialize reverse geocode params
        self.reverseGeocodeParameters = AGSReverseGeocodeParameters()
        self.reverseGeocodeParameters.maxResults = 1
        self.reverseGeocodeParameters.resultAttributeNames.append(contentsOf: ["*"])
        self.reverseGeocodeParameters.outputLanguageCode = lang
        
        self.locatorTask = AGSLocatorTask(url: URL(string: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer")!)
    }

    
    private func reverseGeocode(point:AGSPoint) {
        
        
        //normalize the point
        let normalizedPoint = AGSGeometryEngine.normalizeCentralMeridian(of: point) as! AGSPoint
        
        
        
        
        //cancel all previous operations
        self.locatorTaskOperation?.cancel()
        
        let graphic = self.graphicForPoint(normalizedPoint, attributes: [String:AnyObject]())
        //self.graphicsOverlay.graphics.add(graphic)
        
        //perform reverse geocode
        self.locatorTaskOperation = self.locatorTask.reverseGeocode(withLocation: normalizedPoint, parameters: self.reverseGeocodeParameters) { [weak self] (results: [AGSGeocodeResult]?, error: Error?) -> Void in
            
            print ("reverse geo code done")
            
            if let error = error as NSError? , error.code != NSUserCancelledError {
                //print error instead alerting to avoid disturbing the flow
                print(error.localizedDescription)
                
            }
            else {
                //if a result is found extract the required attributes
                //assign the attributes to the graphic
                //and show the callout
                if let results = results , results.count > 0 {
                    graphic.attributes.addEntries(from: results.first?.attributes ?? [:])
                    
                    let address = graphic.attributes["LongLabel"] as? String ?? ""
                    
                    if !(self?.containsAnyOfFilterText(address))!{
                        self?.isPositionInAbuDhabi.value = false
                    }else {
                        self?.isPositionInAbuDhabi.value = true
                    }
                   
                    
                    return
                }
                else {
                    //no result was found
                    //using print in log instead of alert to
                    //avoid breaking the flow
                    print("No address found")
                    //self?.showAlert("No address found")
                    //dismiss the callout if already visible
                    self?.mapView.callout.dismiss()
                    
                }
            }
            //in case of error or no results, remove the graphics
            //self?.graphicsOverlay.graphics.remove(graphic)
        }
    }
    
    
    private func graphicForPoint(_ point: AGSPoint, attributes:[String:AnyObject]?) -> AGSGraphic {
        let markerImage = UIImage.init(color: .darkBlueGrey, size: CGSize(width: 20, height: 20 ))! // #imageLiteral(resourceName: "About") // UIImage(named: "RedMarker")!
        let symbol = AGSPictureMarkerSymbol(image: markerImage)
        symbol.leaderOffsetY = 0 // markerImage.size.height/2
        symbol.offsetY = 0 //markerImage.size.height/2
        let graphic = AGSGraphic(geometry: point, symbol: symbol, attributes: attributes)
        return graphic
    }
    

    }
    
    
    

    
    
    
    
    
    
    
    
    

