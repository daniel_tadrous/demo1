//
//  PublicHolidaysViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import RxSwift
import EventKit

class PublicHolidaysViewModel:SeacrhViewModel , SearchViewModelDelegate {
    private var apiService: ApiMyCommunityTypes!
    var shareViewModel = ShareViewModel()
    
    let ascendingOrder = "asc"
    let descendingOrder = "desc"
    var sortDirection = Variable("")
    var filterObject = Variable(EventsFilterApplyModel())
    
    var searchedText:Variable<String?> = Variable(nil)
    var mySearchViewModel:SeacrhViewModel?
    var isToGetTrending: Variable<Bool?> = Variable(nil)
    var trendingVar: Variable<[String]?> = Variable(nil)
    var queryResultVar: Variable<[String]?> = Variable(nil)
    var currentQueryVar: Variable<String?> = Variable(nil)
    
    private(set) var holidays: Variable<[Event]?> = Variable(nil)
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService:ApiMyCommunityTypes) {
        super.init()
        self.apiService = apiService
        setupBinding()
    }
    
    func setupBinding() {
        
        sortDirection.asObservable().subscribe(onNext: { [unowned self] (object) in
            self.getMyEvents()
        })
            .disposed(by: disposeBag)
        
        isToGetTrending.asObservable().subscribe(onNext: { (go:Bool?) in
            if go != nil{
                self.getTrending()
            }
        }).disposed(by: disposeBag)
        
        currentQueryVar.asObservable().subscribe(onNext: { (descriptionText) in
            if descriptionText != nil {
                self.getQueryResults(q:descriptionText!)
            }
        }).disposed(by: disposeBag)
        
        searchedText.asObservable().subscribe(onNext: { [unowned self] (object) in
            self.filterObject.value.q = self.searchedText.value ?? ""
            self.getMyEvents()
        })
            .disposed(by: disposeBag)
        
    }
    
    func getQueryResults(q: String) {
        apiService.searchFor(query: q, type: 1).asObservable().subscribe(onNext: { items in
            self.queryResultVar.value = items
            print("\(items)")
        }).disposed(by: disposeBag)
    }
    
    func getTrending() {
        apiService.searchFor(query: "", type: 0).asObservable().subscribe(onNext: { trendingItems in
            self.trendingVar.value = trendingItems
            print("\(trendingItems)")
        }).disposed(by: disposeBag)
    }
    
    func getMyEvents() {
        apiService.getPublicHolidays(filterObject:filterObject.value).subscribe(onNext: { (eventsRes) in
            self.holidays.value = eventsRes
        })
        .disposed(by: disposeBag)
    }
    
    func addToCalendar(event:Event){
        let eventStore : EKEventStore = EKEventStore()
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        eventStore.requestAccess(to: .event) { (granted, error) in
            if (granted) && (error == nil) {
                let eventC:EKEvent = EKEvent(eventStore: eventStore)
                eventC.title = event.title
                eventC.startDate = event.start?.getDate(format: inputFormat)
                eventC.endDate = event.end?.getDate(format: inputFormat)
                eventC.notes = event.description
                eventC.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(eventC, span: .thisEvent)
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                DispatchQueue.main.async {
                    Toast(message: L10n.MyCommunity.Events.eventAddedSuccessfully).Show()
                }
            }else{
                DispatchQueue.main.async {
                    let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.MyCommunity.Events.permessionDenied, onComplete: {
                        self.addToCalendar(event: event)
                    })
                    UIApplication.shared.keyWindow!.addSubview(permessionView)
                    UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
                }
            }
        }
    }
    
    func findOutMore() {
        
        Toast(message: L10n.commingSoon).Show()
    }
    
    func sort() {
        switch sortDirection.value {
        case ascendingOrder:
            filterObject.value.sort = descendingOrder
            sortDirection.value = descendingOrder
        default:
            filterObject.value.sort = ascendingOrder
            sortDirection.value = ascendingOrder
        }
    }
}
extension PublicHolidaysViewModel:filterViewModelDelegate{
    var apiServiceObject: ApiFilterServiceType {
        return apiService
    }
    
}
