//
//  ErrorView.swift
//  TAMMApp
//
//  Created by kerolos on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

protocol  GoToSettingsDelegate{
    func closedpressed()
}


class GoToSettingsView : UIView{
    
    public var shouldDisplayFloatingMenuWhenDismissed = true
    @IBOutlet weak var messageBody: LocalizedLabel!
    @IBOutlet var contentView: UIView!
    
    var delegate:GoToSettingsDelegate?
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    
    var message:String = ""
    var onComplete:(()->Void)?
    
    @IBAction func closeClickHandler(_ sender: UIButton) {
        delegate?.closedpressed()
        self.removeFromSuperview()
        if (shouldDisplayFloatingMenuWhenDismissed ){
            self.displayFloatingMenu()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    init(frame: CGRect,message:String,onComplete:@escaping ()->Void) {
        super.init(frame: frame)
        self.onComplete = onComplete
        self.message = message
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("GoToSettingsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(okBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        okButtonView.addRoundCorners(radious: 25, borderColor: UIColor.turquoiseBlue)
        dialogView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        setupUiData()
    }
    func setupUiData(){
        self.messageBody.text = self.message
    }
    @objc func okBtnClicked(){
        self.removeFromSuperview()
       // self.displayFloatingMenu()
        if let url = URL(string:UIApplicationOpenSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: {isEnabled in
                    self.onComplete?()
                })
            }
        }
        
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
}
