//
//  FactsAndFigureModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class FactsAndFiguresModel : Mappable{
    
    var content:String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        content <- map["content"]
    }
}
