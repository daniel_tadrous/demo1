//
//  UserDefaultsKeys.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class UserDefaultsKeys {
    
    static let isFirstTimePayments = "isPaymentsFirstTime"
    static let isUploadWifi = "isUploadWifi"
    static let isofflinePopupApeared = "OfflinePopup"
    
    class func getisWifiOnlyStatus() ->Bool {
        let defaults = UserDefaults.standard
        
        let wifiStatus = defaults.object(forKey: UserDefaultsKeys.isUploadWifi)
        
        if let status = wifiStatus {
            return status as! Bool
        }else {
            return false
        }
        
    }
    
    class func getIsOfflinePopupAppeared() -> Bool{
        let defaults = UserDefaults.standard
        
        let wifiStatus = defaults.object(forKey: UserDefaultsKeys.isofflinePopupApeared)
        
        if let status = wifiStatus {
            
            return status as! Bool
        }else {
            return false
        }
    }
    
    class func  changeisofflinePopupApearedValue() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: UserDefaultsKeys.isofflinePopupApeared)
        defaults.synchronize()
    }
    
    
}
