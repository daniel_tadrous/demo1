//
//  Event.swift
//  TAMMApp
//
//  Created by Daniel on 7/31/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: Mappable {
    var id: Int?
    var title: String?
    var description: String?
    var image: String?
    var category: Category?
    var start: String?
    var end: String?
    var completionDate:String?
    var link: String?
    var hijri:String?
    var isAllDay:Bool?
    var addresses:[Address]?
    
    var isExpanded: Bool = false
    var isImageLoaded: Bool = false
    var isReloaded = false;
    var imageAspectRatio:CGFloat = 1.0
    
    init(){
        
    }
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        image <- map["image"]
        category <- map["category"]
        start <- map["start"]
        end <- map["end"]
        completionDate <- map["completionDate"]
        link <- map["link"]
        hijri <- map["hijri"]
        isAllDay <- map["isAllDay"]
        addresses <- map["addresses"]
    }
    
    class Category: Mappable{
        var id:String?
        var title:String?
        
        required init?(map: Map) {
            
        }
        
        // Mappable
        func mapping(map: Map) {
            id <- map["id"]
            title <- map["title"]
        }
    }
    
}
extension Event{
    func getFormattedStartDay()->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let outputFormat = "dd MMM"
        let dayFormat = "dd"
        let monthFormat = "MMM"
        
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        if startDate?.getFormattedString(format: outputFormat) == endDate?.getFormattedString(format: outputFormat){
            res = startDate?.getFormattedString(format: dayFormat) ?? ""
        }else if startDate?.getFormattedString(format: monthFormat) == endDate?.getFormattedString(format: monthFormat){
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "") - \(endDate?.getFormattedString(format: dayFormat) ?? "")"
        }else{
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "")"
        }
        
        return res
    }
    
    func getFormattedEndDay()->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let outputFormat = "dd MMM"
        let dayFormat = "dd"
        let monthFormat = "MMM"
        
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        if startDate?.getFormattedString(format: outputFormat) == endDate?.getFormattedString(format: outputFormat){
            res = ""
        }else if startDate?.getFormattedString(format: monthFormat) == endDate?.getFormattedString(format: monthFormat){
            res = ""
        }else{
            res = "\(endDate?.getFormattedString(format: dayFormat) ?? "")"
        }
        
        return res
    }
    
    func getFormattedStartMonth(monthFormatOfthree:Bool = true)->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        var monthFormat = ""
        if monthFormatOfthree {
            monthFormat = "MMM"
        }
        else{
            monthFormat = "MMMM"
        }
        
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
        return res
    }
    
    func getFormattedEndMonth(monthFormatOfthree:Bool = true)->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        var monthFormat = ""
        if monthFormatOfthree {
            monthFormat = "MMM"
        }
        else{
            monthFormat = "MMMM"
        }
        
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        if startDate?.getFormattedString(format: monthFormat) == endDate?.getFormattedString(format: monthFormat){
            res = ""
        }else{
            res = "\(endDate?.getFormattedString(format: monthFormat) ?? "")"
        }
        
        return res
    }
    
    func getFormattedTime()->String{
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let outputFormat = "h:mm a"
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        
        res = "\(startDate?.getFormattedString(format: outputFormat) ?? "") - \(endDate?.getFormattedString(format: outputFormat) ?? "")"
        
        return res
    }
    func getFormattedDate()->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let outputFormat = "dd MMM"
        let dayFormat = "dd"
        let monthFormat = "MMM"
        
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        if startDate?.getFormattedString(format: outputFormat) == endDate?.getFormattedString(format: outputFormat){
            res = startDate?.getFormattedString(format: outputFormat) ?? ""
        }else if startDate?.getFormattedString(format: monthFormat) == endDate?.getFormattedString(format: monthFormat){
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "") - \(endDate?.getFormattedString(format: outputFormat) ?? "")"
        }else{
            res = "\(startDate?.getFormattedString(format: outputFormat) ?? "") - \(endDate?.getFormattedString(format: outputFormat) ?? "")"
        }
        
        return res
    }
    func getFormattedDateAttributed(threeLetterFormat: Bool = true) -> NSMutableAttributedString{
        if L10n.Lang.contains("en") {
            return getFormattedAttributedEnglish(threeLetterFormat : threeLetterFormat)
        }
        else{
            return getFormattedAttributedArabic(threeLetterFormat : threeLetterFormat)
        }
    }
    func getFormattedAttributedArabic(threeLetterFormat:Bool = true)->NSMutableAttributedString{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let outputFormat = "dd MMM"
        let dayFormat = "dd"
        var monthFormat = "MMM"
        if !threeLetterFormat {
            
            var monthFormat = "MMMM"
        }
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        if startDate?.getFormattedString(format: outputFormat) == endDate?.getFormattedString(format: outputFormat){
            let monthAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Swissra-Bold", size: 16)!]
            let dayAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Swissra-Bold", size: 26)!]
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "")"
            var dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
            var monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            var result = NSMutableAttributedString(attributedString: dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
        }else if startDate?.getFormattedString(format: monthFormat) == endDate?.getFormattedString(format: monthFormat){
            let monthAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Swissra-Bold", size: 16)!]
            let dayAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Swissra-Bold", size: 26)!]
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "") - \(endDate?.getFormattedString(format: dayFormat) ?? "")"
            let dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
            let monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            let result = NSMutableAttributedString(attributedString: dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
            return result
        }else{
            let monthAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Swissra-Bold", size: 16)!]
            let dayAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Swissra-Bold", size: 26)!]
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "")"
            var dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
            var monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            var result = NSMutableAttributedString(attributedString: dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
            ////
            res = "\(endDate?.getFormattedString(format: dayFormat) ?? "")"
            dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(endDate?.getFormattedString(format: monthFormat) ?? "")"
            monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            result.append(NSAttributedString(string: " - "))
            result.append(dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
            
            return result
            
            res = "\(startDate?.getFormattedString(format: outputFormat) ?? "") - \(endDate?.getFormattedString(format: outputFormat) ?? "")"
        }
        var attrRes = NSMutableAttributedString(string: res)
        return attrRes
    }
    
    func getFormattedAttributedEnglish(threeLetterFormat:Bool = true)->NSMutableAttributedString{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let outputFormat = "dd MMM"
        let dayFormat = "dd"
        var monthFormat = "MMM"
        if !threeLetterFormat {
            
            var monthFormat = "MMMM"
        }
        var res = ""
        let startDate = self.start?.getDate(format: inputFormat)
        let endDate = self.end?.getDate(format: inputFormat)
        if startDate?.getFormattedString(format: outputFormat) == endDate?.getFormattedString(format: outputFormat){
            let monthAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 16)!]
            let dayAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 26)!]
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "")"
            var dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
            var monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            var result = NSMutableAttributedString(attributedString: dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
        }else if startDate?.getFormattedString(format: monthFormat) == endDate?.getFormattedString(format: monthFormat){
            let monthAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 16)!]
            let dayAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 26)!]
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "") - \(endDate?.getFormattedString(format: dayFormat) ?? "")"
            let dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
            let monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            let result = NSMutableAttributedString(attributedString: dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
            return result
        }else{
            let monthAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 16)!]
            let dayAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 26)!]
            res = "\(startDate?.getFormattedString(format: dayFormat) ?? "")"
            var dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(startDate?.getFormattedString(format: monthFormat) ?? "")"
            var monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            var result = NSMutableAttributedString(attributedString: dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
            ////
            res = "\(endDate?.getFormattedString(format: dayFormat) ?? "")"
            dayStr = NSMutableAttributedString(string:res, attributes: dayAttr)
            
            res = "\(endDate?.getFormattedString(format: monthFormat) ?? "")"
            monthStr = NSMutableAttributedString(string:res, attributes: monthAttr)
            result.append(NSAttributedString(string: " - "))
            result.append(dayStr)
            result.append(NSAttributedString(string: " "))
            result.append(monthStr)
            
            return result
            
            res = "\(startDate?.getFormattedString(format: outputFormat) ?? "") - \(endDate?.getFormattedString(format: outputFormat) ?? "")"
        }
        var attrRes = NSMutableAttributedString(string: res)
        return attrRes
    }
}
