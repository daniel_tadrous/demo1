//
//  EntityCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class EntityCell: UITableViewCell {
   
    @IBOutlet weak var closureIconLbl: UILabel?
    @IBOutlet weak var closureDateHolderView: UIView!
    @IBOutlet weak var entityNameLbl: UILabel?
    @IBOutlet weak var entityImg: UIImageView?
    @IBOutlet weak var closureDateLbl: UILabel?
    @IBOutlet weak var submittedOnLbl: UILabel!
    @IBOutlet weak var caseTypeLbl: UILabel!
    @IBOutlet weak var caseTypeSubLbl: UILabel!
    @IBOutlet weak var entityIconLbl: UILabel?
    
    @IBOutlet weak var entityHolderView: UIView?
    @IBOutlet weak var addressLine1Lbl: UILabel!
    
    @IBOutlet weak var addressLine2Lbl: UILabel!
    
    @IBOutlet weak var locationHolderView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.closureIconLbl?.text = "\u{e004}"
        self.entityIconLbl?.text = "\u{e00e}"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
