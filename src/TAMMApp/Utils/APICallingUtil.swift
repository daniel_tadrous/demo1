//
//  File.swift
//  TAMMApp
//
//  Created by kerolos on 4/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import ObjectMapper

class APICallingUtil{
    
    static var isDownloadRunning:Bool = false
    
    static func getJsonFromApi<String>(_ returnType: Array<String.Type>, _ urlRequest: URLRequest) -> Observable<[String]>{
        var urlRequest1 = urlRequest
        urlRequest1.httpMethod = HTTPMethod.get.rawValue
        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let ret = PublishSubject<[String]>()
        executeRequest(ret: ret, urlRequest: urlRequest1)
        
        return ret
    }
        static func postJsonToApi<String>(_ returnType: String.Type, _ urlRequest: URLRequest) -> Observable<String>{
            let urlRequest1 = urlRequest
            let ret = PublishSubject<String>()
            executeRequest(ret: ret, urlRequest: urlRequest1)
            return ret
        }
    
    static func getJsonFromApi<T>(_ returnType: Array<T.Type>, _ urlRequest: URLRequest) -> Observable<[T]> where T : Mappable{
        var urlRequest1 = urlRequest
        urlRequest1.httpMethod = HTTPMethod.get.rawValue
        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
        let ret = PublishSubject<[T]>()
        executeRequest(ret: ret, urlRequest: urlRequest1)
        return ret
    }
    
    
    static func getJsonFromApi<T>(_ returnType: T.Type, _ urlRequest: URLRequest) -> Observable<T> where T :  Mappable {
        
        var urlRequest1 = urlRequest
        urlRequest1.httpMethod = HTTPMethod.get.rawValue
        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
        let ret = PublishSubject<T>()
        executeRequest(ret: ret, urlRequest: urlRequest1)
        return ret
    }
    
    static func postJsonToApi<T>(_ returnType: T.Type, _ urlRequest: URLRequest) -> Observable<T> where T :  Mappable {
        
        var urlRequest1 = urlRequest
        urlRequest1.httpMethod = HTTPMethod.post.rawValue
        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
        let ret = PublishSubject<T>()
        executeRequest(ret: ret, urlRequest: urlRequest1)
        return ret 
    }
    
    
    static func customJsonToApi<T>(_ returnType: T.Type, _ urlRequest: URLRequest, method: HTTPMethod = HTTPMethod.post) -> Observable<T> where T :  Mappable {
        
        var urlRequest1 = urlRequest
        urlRequest1.httpMethod = method.rawValue
        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
        let ret = PublishSubject<T>()
        executeRequest(ret: ret, urlRequest: urlRequest1)
        return ret
    }
    static func customJsonToApi<T>(_ returnType: Array<T.Type>, _ urlRequest: URLRequest, method: HTTPMethod = HTTPMethod.post) -> Observable<[T]> where T : Mappable{
        var urlRequest1 = urlRequest
        urlRequest1.httpMethod = method.rawValue
        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
        let ret = PublishSubject<[T]>()
        executeRequest(ret: ret, urlRequest: urlRequest1)
        return ret
    }
    static func executeRequest<String>(ret: PublishSubject<String>,urlRequest: URLRequest){
        let request = Alamofire.request(urlRequest)
        let req = request.rx.responseJSON(errorHandler: {
            executeRequest(ret: ret, urlRequest: urlRequest)
        }).map { (value) -> String in
            print(value)
            return value as! String
        }
        _ = req.subscribe(onNext: { (ele) in
            ret.onNext(ele)
            ret.onCompleted()
        })
    }
    static func executeRequest<String>(ret: PublishSubject<[String]>,urlRequest: URLRequest){
        let request = Alamofire.request(urlRequest)
        let req = request.rx.responseJSON(errorHandler: {
            executeRequest(ret: ret, urlRequest: urlRequest)
        }).map { (value) -> [String] in
            print(value)
            let json = value as? [String] ?? []
            let mapped = json
            return mapped
        }
        _ = req.subscribe(onNext: { (ele) in
            ret.onNext(ele)
            ret.onCompleted()
        })
    }
    static func executeRequest<T>(ret: PublishSubject<[T]>,urlRequest: URLRequest) where T : Mappable{
        let request = Alamofire.request(urlRequest)
        let req = request.rx.responseJSON(errorHandler: {
            executeRequest(ret: ret, urlRequest: urlRequest)
        }).map { (value) -> [T] in
            print(value)
            let json = value as? [[String: Any]] ?? [[:]]
            let mapped = [T](JSONArray: json)
            return mapped
        }
        _ = req.subscribe(onNext: { (ele) in
            ret.onNext(ele)
            ret.onCompleted()
        })
    }
    static func executeRequest<T>(ret: PublishSubject<T>,urlRequest: URLRequest) where T : Mappable{
        let request = Alamofire.request(urlRequest)
        let req = request.rx.responseJSON(errorHandler: {
            executeRequest(ret: ret, urlRequest: urlRequest)
        }).map { (value) -> T in
            
            print(value)
            let json = value as? [String: Any] ?? [:]
            
            let mapped = T(JSON: json)
            
            return mapped!
        }
        _ = req.subscribe(onNext: { (ele) in
            ret.onNext(ele)
            ret.onCompleted()
        })
    }
    
    static func downloadFile(_ urlRequest:URLRequest, fileName:String,fileFormat:String,toIncidentsDir: Bool = false) -> Observable <URL?>{
        let downloadURL:Variable<URL?> = Variable(nil)
        if fileFormat == "vtpk" {
            AppDelegate.isDownloadRunning = true
        }
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let documentsURL = toIncidentsDir ? IncidentUrl.getDirectoryForIncident() : FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("\(fileName).\(fileFormat)")
            print("*/*/*/*/*/*/*/*/*/**/*/===== file url =======/*/*/*/**")
            print(fileURL)
            
            return (fileURL, [.removePreviousFile])
        }
        
        Alamofire.download(urlRequest, to: destination).downloadProgress(closure: { (progress) in
            print(progress)
            }).response{  response in
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
                
            if response.error == nil, let path = response.destinationURL?.path {
                downloadURL.value = URL(fileURLWithPath: path)
                AppDelegate.isDownloadRunning = false
                if fileFormat == "vtpk" {
                    appDelegate?.sendNotification(isDownloadComplete: true)
                }
            }
            else{
                downloadURL.value = nil
                AppDelegate.isDownloadRunning = true
                if fileFormat == "vtpk" {
                   appDelegate?.sendNotification(isDownloadComplete: false)
                }
            }
            
        }
        
        
//        Alamofire.download(urlRequest, to: destination).response { response in
//            if response.error == nil, let path = response.destinationURL?.path {
//                downloadURL.value = URL(fileURLWithPath: path)
//            }
//            else{
//                downloadURL.value = nil
//            }
//        }
        return downloadURL.asObservable()
    }
}

//    static func getJsonFromApi<String>(_ returnType: String.Type, _ urlRequest: URLRequest) -> Observable<String>{
//
//        var urlRequest1 = urlRequest
//        urlRequest1.httpMethod = HTTPMethod.get.rawValue
//        urlRequest1.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        let request = Alamofire.request(urlRequest1)
//        let ret = request.rx.responseString().map { (value) -> String in
//
//            print(value)
////            let json = value as? [String] ?? []
////
////            let mapped = json
////
//            return value as! String
//        }
//        print (urlRequest.cURL)
//        return ret
//    }







