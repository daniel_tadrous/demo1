//
//  VoicePlayer.swift
//  AVFoundation Recorder
//
//  Created by kerolos on 6/26/18.
//  Copyright © 2018 Gene De Lisa. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

protocol VoicePlayerDelegate : class{
    func voicePlayerDidStartPlaying()
    func voicePlayerProgress(progress: Double, of: Double)
    func voicePlayerDidStopPlaying()
}

class VoicePlayer: NSObject{
    
    
    weak var delegate: VoicePlayerDelegate?
    
    var player: AVAudioPlayer?
    
    public func play(url: URL) {
        print("\(#function)")
//        self.player .stop()
        setSessionPlayback()
        print("playing \(String(describing: url))")
        
        do {
            self.player = try AVAudioPlayer(contentsOf: url)
            
            player!.delegate = self
            player!.prepareToPlay()
            player!.volume = 1.0
            player!.play()
            startTimer()
            delegate?.voicePlayerDidStartPlaying()
        } catch {
            self.player = nil
            print(error.localizedDescription)
        }
    }
    
    var timer: Timer?
    
    func startTimer(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: { (currentTimer) in
            self.delegate?.voicePlayerProgress(progress: self.player!.currentTime, of: self.player!.duration)
            
            
        })

    }
    
    
    private func setSessionPlayback() {
        print("\(#function)")
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback, with: .defaultToSpeaker)
            
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func stop(){
        timer?.invalidate()
        player?.stop()
    }
    
    func isPlaying()->Bool{
        return self.player?.isPlaying ?? false
    }
    
}


// MARK: AVAudioPlayerDelegate
extension VoicePlayer: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("\(#function)")
        
        print("finished playing \(flag)")
        timer?.invalidate()
        delegate?.voicePlayerDidStopPlaying()
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("\(#function)")
        timer?.invalidate()
        delegate?.voicePlayerDidStopPlaying()
        if let e = error {
            print("\(e.localizedDescription)")
        }
        
    }
}
