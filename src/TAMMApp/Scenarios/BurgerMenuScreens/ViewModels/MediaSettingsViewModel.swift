//
//  MediaSettingsViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/9/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift


struct MediaType {
    var sizeOfFiles:Double
    var numberOfFiles:Int
}


class MediaSettingsViewModel {

    var WifiOnly: Variable<Bool?> = Variable(nil)
    fileprivate var disposeBag = DisposeBag()
    
    
    
    
    init() {
        setupHooks()
    }
    func setupHooks() {
        WifiOnly.asObservable().subscribe(onNext: {  [unowned self] (bool) in
            // do something
        })
            .disposed(by: disposeBag)    }
    
    
    
    fileprivate func getAudioData() ->(numberOfFiles:Int, sizeOfFiles:Double) {
        do {
            let documentsURL = IncidentUrl.getDirectoryForIncident()
            let docs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            let audios =  docs.filter{
                for extensionType in MediaFormats.audioFormats {
                    if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame && $0.createThumbnailOfVideoFromFileURL() == nil { return true }
                }
                return false
            }
            var size:Double = 0
                for audio in audios {
                    size += audio.sizePerMB()
                }
             //let audiosSize = String(size.rounded()) + " MB"
             return ( audios.count , size)
        } catch {
            print(error)
        }
        return (0,0)
    }
    
    
    fileprivate func getDocumentsData() ->(numberOfFiles:Int, sizeOfFiles:Double) {
        do {
            let documentsURL = IncidentUrl.getDirectoryForIncident()
            let docsContent = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            let docs =  docsContent.filter{
                for extensionType in MediaFormats.docsFormat {
                    if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame { return true }
                }
                return false
            }
            var size:Double = 0
            for doc in docs {
                size += doc.sizePerMB()
            }
            //let audiosSize = String(size.rounded()) + " MB"
            return ( docs.count , size)
        } catch {
            print(error)
        }
        return (0,0)
    }
    
    fileprivate func getPhotosData() ->(numberOfFiles:Int, sizeOfFiles:Double) {
        do {
            let documentsURL = IncidentUrl.getDirectoryForIncident()
            let docs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            let images =
                docs.filter{
                    for extensionType in MediaFormats.imgFormats {
                        if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame { return true }
                    }
                    return false
                }
            
            var size:Double = 0
            for img in images {
                size += img.sizePerMB()
            }
            return ( images.count , size)
        } catch {
            print(error)
        }
        return (0,0)
    }
    
    
    fileprivate func getVideoData() ->(numberOfFiles:Int, sizeOfFiles:Double) {
        do {
            let documentsURL = IncidentUrl.getDirectoryForIncident()
            let docs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            let videos = docs.filter{
                for extensionType in MediaFormats.vedioFormats {
                    if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame && $0.createThumbnailOfVideoFromFileURL() != nil{ return true }
                }
                return false
            }
            var size:Double = 0
            for video in videos {
                size += video.sizePerMB()
            }
            
            return ( videos.count , size)
        } catch {
            print(error)
        }
        return (0,0)
    }
    
    
    func getMediaData() -> (photos:MediaType,audios:MediaType,videos:MediaType, documents:MediaType) {
        let photosInfo = self.getPhotosData()
        let audiosInfo = self.getAudioData()
        let videosInfo = self.getVideoData()
        let documentsInfo = self.getDocumentsData()
        
        let photos:MediaType = MediaType(sizeOfFiles: photosInfo.sizeOfFiles, numberOfFiles: photosInfo.numberOfFiles)
        let audios:MediaType = MediaType(sizeOfFiles: audiosInfo.sizeOfFiles, numberOfFiles: audiosInfo.numberOfFiles)
        let videos:MediaType = MediaType(sizeOfFiles: videosInfo.sizeOfFiles, numberOfFiles: videosInfo.numberOfFiles)
        let documents:MediaType = MediaType(sizeOfFiles: documentsInfo.sizeOfFiles, numberOfFiles: documentsInfo.numberOfFiles)
        
        return (photos,audios,videos,documents)
    }
    
    func getDisplayedNumber(number:Double)->String {
        let number = Double(round(100*(number))/100)
        if number == 0 {
            return "0"
        }else {
            return String(number)
        }
    }
    
    func getSwitchStatus() ->Bool {
        let defaults = UserDefaults.standard
        
        let wifiStatus = defaults.object(forKey: UserDefaultsKeys.isUploadWifi)
        
        if let status = wifiStatus {
            return status as! Bool
        }else {
            return false
        }
 
    }
    
    func setSwitchStatus(withValue value:Bool) {
        let defaults = UserDefaults.standard
        
        defaults.set(value, forKey: UserDefaultsKeys.isUploadWifi)
        defaults.synchronize()
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
