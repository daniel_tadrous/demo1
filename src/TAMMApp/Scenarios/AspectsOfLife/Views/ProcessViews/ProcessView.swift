//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ProcessView : UIView{
    
    
    var onViewMapClick: (()->Void)?
    var onViewPageClick: (()->Void)?
    
    @IBOutlet weak var processHeaderView: ProcessHeaderView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    var loadMoreButton: LocalizedButton!
    var numberViewed: Int = 0
    var selectedBtnIndex = 1
    let rowHeight: Float = 20
    let bottomMargin = 0
    fileprivate let heightOfShowMoreButton:CGFloat = 50
    public var viewModel:  ProcessViewModel?
    var disposeBag = DisposeBag()
    var cells : [Int: ProcessCell] = [:]
    var process: ProcessModel?
    var defaultReuseIdentifier = "ProcessCell"
    var defaultReuseIdentifierBullet = "ProcessCellBullet"
    var defaultReuseIdentifierText = "ProcessCellText"
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    convenience init(frame: CGRect, id: String, viewModel: ProcessViewModel) {
        self.init(frame: frame)
        self.viewModel = viewModel
        self.viewModel?.serviceId.value = id
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "ProcessView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        
        setupHooks()
       
        self.tableView?.register(UINib(nibName: "ProcessCell", bundle: nil), forCellReuseIdentifier: defaultReuseIdentifier)
        self.tableView?.register(UINib(nibName: "ProcessCellBullet", bundle: nil), forCellReuseIdentifier: defaultReuseIdentifierBullet)
        self.tableView?.register(UINib(nibName: "ProcessCellText", bundle: nil), forCellReuseIdentifier: defaultReuseIdentifierText)
        processHeaderView.onFirstBtnClick = {
            
            let ind : IndexPath = IndexPath(row: Int(self.processHeaderView.firstBtn.title(for: .normal)!)! - 1, section: 0)
            self.tableView.scrollToRow(at: ind, at: UITableViewScrollPosition.top, animated: true)
            self.processHeaderView.makeBtnSelected(index: 1)
            self.selectedBtnIndex = 1
        }
        processHeaderView.onSecondBtnClick = {
            self.processHeaderView.makeBtnSelected(index: 2)
            self.selectedBtnIndex = 2
            let row = Int(self.processHeaderView.secondBtn.title(for: .normal)!)! - 1
            if row  >= self.numberViewed{
                self.cells = [:]
                self.numberViewed = (self.process?.processSteps.count)!
                self.loadMoreButton.isHidden = true
                self.tableView.reloadData()
            }
            let ind : IndexPath = IndexPath(row: row, section: 0)
            self.tableView.scrollToRow(at: ind, at: UITableViewScrollPosition.top, animated: true)
            
        }
        processHeaderView.onThirdBtnClick = {
            self.processHeaderView.makeBtnSelected(index: 3)
            self.selectedBtnIndex = 3
            if self.numberViewed != (self.process?.processSteps.count)!{
                self.cells = [:]
                self.numberViewed = (self.process?.processSteps.count)!
                self.tableView.reloadData()
                self.loadMoreButton.isHidden = true
            }
            
            let ind : IndexPath = IndexPath(row: self.numberViewed - 1, section: 0)
            self.tableView.scrollToRow(at: ind, at: UITableViewScrollPosition.top, animated: true)
            
        }
        
    }
    
    @objc func cancelBtnClicked(){
        let height = UIApplication.shared.keyWindow!.bounds.height
        
        
        // animate from current position down out of the screen
        let superView = self.superview!
        superView.isUserInteractionEnabled = false
        UIView.animate(withDuration: Dimensions.animationDuration1, animations: { [weak self] in
            self?.center = (self?.center.applying(CGAffineTransform.init(translationX: 0, y: height)))!
        }, completion: { (finished) in
            // remove the view and put back the floating menu
            self.removeFromSuperview()
            self.displayFloatingMenu()
            superView.isUserInteractionEnabled = true
        })
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
        
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] process in
                if(process == nil){
                    //TODO: should do somting ????
                }else{
                    
                    self.process = process
                    self.numberViewed = (self.process?.processSteps.count)! > 5 ? 5 : (self.process?.processSteps.count)!
                    if self.numberViewed < (self.process?.processSteps.count)!{
                        self.addLoadMore()
                    }
                    self.setupUiData()
                }
            })
            .disposed(by: disposeBag)
    }
    
    func setupUiData(){
        
        processHeaderView.makeBtnSelected(index: self.selectedBtnIndex)
        processHeaderView.thirdBtn.setTitle("\(self.process?.processSteps.count ?? 3)", for: .normal)
        self.tableView.reloadData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
    }
}
extension ProcessView: UITableViewDataSource, UITableViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        onScroll()
    }
    func onScroll() {
        let indexPaths = self.tableView.indexPathsForVisibleRows
        let firstVisibleIndexPath = indexPaths?.count != 0 ? indexPaths?[0] : self.tableView.indexPathsForRows(in: CGRect(x: 15, y: 200, width: self.tableView.frame.width, height: 300))?[0]
        var index = (firstVisibleIndexPath?.row)!
        let firstIndex = index + 1
        let delta =  index + 3 - (self.process?.processSteps.count)!
        index = delta > 0 ? index - delta : index
        self.processHeaderView.firstBtn.setTitle( "\(index + 1)", for: .normal)
        self.processHeaderView.secondBtn.setTitle( "\(index + 2)", for: .normal)
        if Int(self.processHeaderView.firstBtn.title(for: .normal)!)! == firstIndex{
            self.processHeaderView.makeBtnSelected(index: 1)
        }else if Int(self.processHeaderView.secondBtn.title(for: .normal)!)! == firstIndex {
            self.processHeaderView.makeBtnSelected(index: 2)
        }else{
            self.processHeaderView.makeBtnSelected(index: 3)
        }
    }
    
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return UIView()
//    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberViewed
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let processStep :  ProcessModel.ProcessStep  = (self.process?.processSteps[indexPath.row])!
        let format = processStep.detailsFormat!
        
        var cell: ProcessCell
        switch format {
        case .TEXT:
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifierText) as! ProcessCell
            if (processStep.details?.count)! > 0{
                cell.bottomMargin.constant = 30
                cell.textFormatLabel?.attributedText = processStep.details![0].getAttributedString()
                
            }else{
                cell.bottomMargin.constant = 0
            }
        case .BULLET:
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifierBullet) as! ProcessCell
            cell.bottomMargin.constant = 30
            if (processStep.detailsHeaders?.count)! > 0{
                cell.bulletTitleLabel?.text = processStep.detailsHeaders![0]
            }
             cell.bulletsTabelView?.register(UINib(nibName: "BulletCell", bundle: nil), forCellReuseIdentifier: "BulletCell")
            cell.bulletTableHeight?.constant = CGFloat((processStep.details?.count)! * cell.bulletCellHeight)
            cell.bulletsTabelView?.delegate = cell
            cell.bulletsTabelView?.dataSource = cell
            cell.bullets = processStep.details
            cell.bulletsTabelView?.reloadData()
        case .TABLE:
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifier) as! ProcessCell
            cell.bottomMargin.constant = 30
            for view in cell.detailsView.subviews {
                view.removeFromSuperview()
            }
            
            if (processStep.details?.count)! > 0{
                
                let cols = (processStep.detailsHeaders?.count)!
                let width = Int(UIScreen.main.bounds.width - 108) / cols
                for index in 0..<processStep.detailsHeaders!.count {
                    let headerLabel: LocalizedLabel = getInitializedLabel(frame: CGRect(x: index * width, y: 0, width: width, height: Int(self.rowHeight)))
                    headerLabel.RTLFont = "Swissra-Bold"
                    headerLabel.font = UIFont(name: "Roboto-Bold", size: 12)
                    headerLabel.text = processStep.detailsHeaders![index]
                    headerLabel.textColor = UIColor.coolGrey
                    cell.detailsView.addSubview(headerLabel)
                }
                
                for index in 0..<processStep.details!.count {
                    let xIndex = index % cols
                    let yIndex =  index / cols
                    let detailLabel: LocalizedLabel = getInitializedLabel(frame: CGRect(x: xIndex * width, y: yIndex * Int(rowHeight) + Int(rowHeight), width: width, height: 20))
                    detailLabel.attributedText = processStep.details![index].getAttributedString()
                    cell.detailsView.addSubview(detailLabel)
                    if index == processStep.details!.count - 1{
                        NSLayoutConstraint.activate([NSLayoutConstraint(item: detailLabel, attribute: .top, relatedBy: .equal, toItem: cell.detailsView, attribute: .top, multiplier: 1, constant: CGFloat(yIndex * Int(rowHeight) + Int(rowHeight))) , NSLayoutConstraint(item: detailLabel, attribute: .bottom, relatedBy: .equal, toItem: cell.detailsView, attribute: .bottom, multiplier: 1, constant: CGFloat(self.bottomMargin))])
                    }
                    
                    
                }
                
            }
            
            
        }
        cell.stepTitleLabel.text = self.process?.processSteps[indexPath.row].title!
 
        
        
//        }else{
//            cell.detailsView.frame = CGRect(x: cell.detailsView.frame.origin.x, y: cell.detailsView.frame.origin.y, width: cell.detailsView.frame.width, height: 0)
//            NSLayoutConstraint.activate([NSLayoutConstraint(for: cell.detailsView, withHeight: 0)])
//            cell.bottomMargin.constant = 0
//        }
        
        if indexPath.row == (self.process?.processSteps.count)! - 1{
            cell.footerView.isHidden = true
        }else{
            cell.footerView.isHidden = false
        }
        self.cells[indexPath.row] = cell
        return cell
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func getInitializedLabel(frame: CGRect)-> LocalizedLabel{
        let label: LocalizedLabel = LocalizedLabel(frame: frame)
        label.applyFontScalling = true
        label.originalFont = UIFont(name: "CircularStd-Medium", size: 12)
        label.RTLFont = "Swissra-Normal"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = UIColor.clear
        return label
    }
    
   
}

extension ProcessView{
    
    fileprivate func initiateLoadMoreButton(){
        loadMoreButton = LocalizedButton()
        loadMoreButton.frame = CGRect(x: 20, y: 0, width: UIScreen.main.bounds.width - 70, height: heightOfShowMoreButton)
        loadMoreButton.backgroundColor = UIColor.init(hexString: "#DFF4F7")
        loadMoreButton.addTarget(self, action: #selector(loadMoreBtnClicked), for: .touchUpInside)
        loadMoreButton.RTLFont = "Swissra-Bold"
        loadMoreButton.applyFontScalling = true
        loadMoreButton.setTitle(L10n.show_more, for: .normal)
        loadMoreButton.setTitleColor(UIColor.petrol, for: .normal)
        
        loadMoreButton.titleLabel?.font = UIFont.init(name: "CircularStd-Bold", size: 16)
        loadMoreButton.originalFont = UIFont.init(name: "CircularStd-Bold", size: 16)
        loadMoreButton.layer.borderWidth = 1
        loadMoreButton.layer.borderColor = UIColor.turquoiseBlue.cgColor
        loadMoreButton.addRoundCorners(radious: 17)
        
    }
    fileprivate func addLoadMore(){
        if(loadMoreButton == nil){
            initiateLoadMoreButton()
        }
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: heightOfShowMoreButton +  150))
        self.tableView.tableFooterView?.addSubview(loadMoreButton)
    }
    fileprivate func removeLoadMore(){
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: heightOfShowMoreButton +  50))
        
    }
    @objc func loadMoreBtnClicked(){
        
        self.numberViewed = self.numberViewed + 5 > (self.process?.processSteps.count)! ? (self.process?.processSteps.count)! : self.numberViewed + 5
        
        if self.numberViewed <= (self.process?.processSteps.count)!{
            self.removeLoadMore()
        }
        self.cells = [:]
        self.tableView.reloadData()
        
    }
}

