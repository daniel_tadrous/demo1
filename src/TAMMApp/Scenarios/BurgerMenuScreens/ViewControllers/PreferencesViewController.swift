//
//  PrefrencesViewController.swift
//  TAMMApp
//
//  Created by kero1 on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol PreferencesViewControllerDelegate: class {
    func preferencesWantsToGoToProfilePage()
    func preferencesWantsToGoToLanguageSwitchPage()
    func preferencesWantsToGoToAccessibilityPage()
    func preferencesWantsToGoToEnablePushNotificationPage()
    func preferencesWantsToGoToAboutPage()
    func preferencesWantsToGoToMediaSettings()
}

class PreferencesViewController: TammViewController, PreferencesStoryboardLoadable {
 
    @IBOutlet weak var tableView: UITableView!
    
    private let headerNibName = "PreferencesSectionHeader"
    
    public var viewModel: PreferencesViewModel!
    
    public weak var delegate: PreferencesViewControllerDelegate?

    
    
    let myLoacationService = LocationService()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.barTintColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.tabBarController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.navigationController?.navigationBar.backgroundColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.barTintColor =  UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white.getAdjustedColor()]
        self.navigationController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white.getAdjustedColor()
        self.tableView.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: L10n.Preferences.settingsTitle, willSetSearchButton: true, backTitleString: L10n.Preferences.settingsTitle)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: headerNibName, bundle: nil), forHeaderFooterViewReuseIdentifier: headerNibName)
        // Do any additional setup after loading the view.
        
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerNibName)

        footer?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        footer?.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: tableView.bounds.width, height: 50))
        tableView.tableFooterView = footer
        myLoacationService.Locationdelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private var items: [[PreferencesViewModel.PreferencesViewData]]  {
        return viewModel.items
    }
    
}



extension PreferencesViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let sectionIndex = indexPath.section
        let index = indexPath.row
        let item = items[sectionIndex][index]
    
        switch item.type {
        case .navigationType:
            break
        case .selectionType:
            break
        case .switchType:
            return nil
        }
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let sectionIndex = indexPath.section
        let index = indexPath.row
        let item = items[sectionIndex][index]
        
        switch item.type {
        case .navigationType:
            break
        case .selectionType:
            break
        case .switchType:
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)

        switch item.item{
        case .profile:
            delegate?.preferencesWantsToGoToProfilePage()
            return
        case .language:
            delegate?.preferencesWantsToGoToLanguageSwitchPage()
            return
        case .accessibility:
            delegate?.preferencesWantsToGoToAccessibilityPage()
            return
        case .notifications:
            delegate?.preferencesWantsToGoToEnablePushNotificationPage()
            return
        case .mediaSettings:
            delegate?.preferencesWantsToGoToMediaSettings()
            return
        case .enableLocationServices:
            break
        case .syncCalendar:
            break
        case .about:
            delegate?.preferencesWantsToGoToAboutPage()
            return
        case .tellAFriend:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let urlString = "https://itunes.apple.com/us/app/fpl/id1237328534?mt=8"
            let urlToShare = URL(string:urlString)
            let activityViewController = UIActivityViewController(activityItems: [urlToShare!],applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view //so that ipads won't crash
            present(activityViewController,animated: true,completion: nil)
            
            activityViewController.completionWithItemsHandler = {(_, _, _, _) in
                appDelegate.addfloatingMenu()
            }
             appDelegate.removeFloatingMenu()
            return
        case .appFeatures:
            break
        
        case .downloadOfflineMaps:
            
            let apiSuportService = SupportServices()
            let vc = ConfirmationMessageViewController()
            
            vc.initialize(title: L10n.downloadOfflineMap, isFirstTimeMap:false, CancelBtnTitle: L10n.cancel, confirmationBtnTitle: L10n.ok, okClickHandler: {}, apiService: apiSuportService)

            self.present(vc, animated: true, completion: nil)
            
            return
        }
        displayToast(message: L10n.commingSoon)
    }
}
extension PreferencesViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionIndex = indexPath.section
        let index = indexPath.row
        let item = items[sectionIndex][index]
        var cell: PreferencesTableViewCell
        var title = ""
        var selectedItem: String? = nil
        var switchState: Bool? = nil
        var switchHandler: ((Bool)->())?
        switch item.type {
        case .navigationType:
            cell = tableView.dequeueReusableCell(withIdentifier: PreferencesTableViewCell.identifierNavigation) as! PreferencesTableViewCell
        case .selectionType:
            cell = tableView.dequeueReusableCell(withIdentifier: PreferencesTableViewCell.identifierSelection) as! PreferencesTableViewCell
        case .switchType:
            cell = tableView.dequeueReusableCell(withIdentifier: PreferencesTableViewCell.identifierSwitch) as! PreferencesTableViewCell
        }
        
        cell.backgroundColor = UIColor.white.getAdjustedColor()
        
        cell.selectedItemTitle?.textColor = UIColor.petrol.getAdjustedColor()
        cell.titleLabel.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        cell.arrowLabel?.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        switch item.item{
            
        case .profile:
            title = L10n.Preferences.profile
        case .language:
            title = L10n.Preferences.language
            selectedItem = viewModel.getLanguageLitral()
        case .notifications:
            title = L10n.Preferences.notifications
        case .mediaSettings:
            title = L10n.Preferences.mediaSettings
        case .enableLocationServices:
            title = L10n.Preferences.enableLocationServices
            cell.switchView?.addTarget(self, action: #selector(switchChanged(_:)), for: UIControlEvents.valueChanged)
            switchState = myLoacationService.isLocationPermissionAvailable()
                //viewModel.getLocationServiceStatus()
        case .syncCalendar:
            title = L10n.Preferences.syncCalendar
            cell.switchView?.addTarget(self, action: #selector(calenderSwitchChanged(_:)), for: UIControlEvents.valueChanged)
            switchState = viewModel.getCalenderSyncStatus()
        case .about:
            title = L10n.Preferences.about
        case .tellAFriend:
            title = L10n.Preferences.tellAFriend
        case .appFeatures:
            title = L10n.Preferences.appFeatures
        case .accessibility:
            title = L10n.Preferences.accessibility
        case .downloadOfflineMaps:
            title = L10n.downloadOfflineMap
                //L10n.Preferences.accessibility
        }
        
        
//        title = item.item.rawValue
        
        cell.set(title: title, selectedItem: selectedItem, switchItemState: switchState, switchStateChanged: switchHandler)
        return cell
    }
    
    @objc func switchChanged(_ mySwitch: UISwitch) {
        if mySwitch.isOn {
            myLoacationService.requestPermissionStatus()
        }else {
            myLoacationService.removelocationPermission()
            //
        }
    }
    @objc func calenderSwitchChanged(_ mySwitch: UISwitch) {
        self.viewModel.setCalanderSyncStatus(isOn: mySwitch.isOn)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: headerNibName)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
//    func removelocationPermission() {
//        DispatchQueue.main.async {
//            let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.locationPermissionNeeded, onComplete: {
//            })
//            UIApplication.shared.keyWindow!.addSubview(permessionView)
//            UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
//
//        }
//    }
    
//    func requestlocationPermission() {
//        DispatchQueue.main.async {
//            let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.locationPermissionNeeded, onComplete: {
//            })
//            permessionView.delegate = self
//            UIApplication.shared.keyWindow!.addSubview(permessionView)
//            UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
//
//        }
//    }
    
}

extension PreferencesViewController : LocationServiceDelegate {
    func viewDismissed() {
        self.tableView.reloadData()
//        let myindexPath = IndexPath(row: 2, section: 1)
//        self.tableView.reloadRows(at: [myindexPath], with: .none)
    }
}
