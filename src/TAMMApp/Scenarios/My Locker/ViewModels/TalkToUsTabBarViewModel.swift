//
//  TalkToUsTabBarViewModel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
enum LunchMode{
    case NEW, EDIT, DRAFT, EMPTY
}
class TalkToUsTabBarViewModel {
    //public var isToOpenPendingDrafts: Bool = false
    static var LunchModeStatic: LunchMode = .EMPTY
    var lunchMode: LunchMode{
        get{
            return TalkToUsTabBarViewModel.LunchModeStatic
        }
        set{
            TalkToUsTabBarViewModel.LunchModeStatic = newValue
        }
    }
    static var itemStatic: TalkToUsItemViewDataType!
    public var item: TalkToUsItemViewDataType! {
        get{
            return TalkToUsTabBarViewModel.itemStatic
        }
        set {
            TalkToUsTabBarViewModel.itemStatic = newValue
            
        }
    }
    
    
    static var titleStatic: String = ""
    public var title: String {
        get{
            return TalkToUsTabBarViewModel.titleStatic
        }
        set {
            TalkToUsTabBarViewModel.titleStatic = newValue
        }
    }
    
    private var draftVar: DraftViewModelData!
    public var draft: DraftViewModelData {
        get{
            return draftVar
        }
        set {
            draftVar = newValue
        }
    }
    
    public func getItemName()->String{
        if(title == nil){
            return item.name
        }
        return title
    }

}
