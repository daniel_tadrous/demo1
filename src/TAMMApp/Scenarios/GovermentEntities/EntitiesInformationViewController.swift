//
//  EntitiesInformationViewController.swift
//  TAMMApp
//
//  Created by kerolos on 7/9/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift


enum EntitiesInformationViewControllerCellType{
    case about
    case office
    case buttons
}

protocol EntitiesInformationViewControllerDelegate: class{
    
}

class EntitiesInformationViewController: UIViewController, EntitiesInformationStoryboardLoadable {

    
    private var disposeBag = DisposeBag()

    weak var delegate: EntitiesInformationViewControllerDelegate?
    var viewModel: EntitiesInformationViewModel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tabelView.register(UINib(nibName: InformationAboutTableViewCell.namedNibName, bundle: nil), forCellReuseIdentifier: InformationAboutTableViewCell.namedNibName)
        
        tabelView.register(UINib(nibName: InformationOfficeTableViewCell.namedNibName, bundle: nil), forCellReuseIdentifier: InformationOfficeTableViewCell.namedNibName)
        
        tabelView.register(UINib(nibName: InformationOfficeTableViewCell.namedNibNameCollapsed, bundle: nil), forCellReuseIdentifier: InformationOfficeTableViewCell.namedNibNameCollapsed)
        
        tabelView.register(UINib(nibName: InformationButonsTableViewCell.namedNibName, bundle: nil), forCellReuseIdentifier: InformationButonsTableViewCell.namedNibName)
        
        tabelView.dataSource = self
        tabelView.delegate = self
        tabelView.delegate = self
        tabelView.estimatedRowHeight = 200
        tabelView.rowHeight = UITableViewAutomaticDimension
        
        setupHooks()
    }
    
    private var informationData: InformationPageViewData?{
        didSet{
            DispatchQueue.main.async {
                self.tabelView.reloadData()
            }
        }
    }

    private func setupHooks(){
        self.viewModel.data.subscribe(onNext: {[unowned self] (data) in
            self.informationData = data
        }).disposed(by: disposeBag)
    }
        

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var tabelView: UITableView!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EntitiesInformationViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // first cell will be the about cell
        // I am using a tabel view for its better scrolling handeling
        return aboutCount + officeCount + buttonsCount
    }
    private var aboutCount: Int{
        if let informationData = informationData {
            return 1
        }
        return 0
    }
    private var officeCount: Int{
        if let informationData = informationData {
            return informationData.officeViewData.count
        }
        return 0
    }
    private var buttonsCount: Int{
        return 3
    }
    
    private func getItemTypeAndIndex(row: Int) -> (EntitiesInformationViewControllerCellType, Int) {
        if row < aboutCount {
            return (.about, row)
        }
        else if row < aboutCount + officeCount {
            return (.office, row - aboutCount)
        }
        else if row < aboutCount + officeCount + buttonsCount {
            return (.buttons, row - aboutCount - officeCount)
        }
        return (.about, 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        let (type, index) = getItemTypeAndIndex(row: row)
        
        switch type{
            // get about cell
        case .about:
            let cell = tabelView.dequeueReusableCell(withIdentifier: InformationAboutTableViewCell.namedNibName) as! InformationAboutTableViewCell
            
            cell.images = (informationData?.aboutViewData.images) ?? []
            cell.logoUrl = informationData?.aboutViewData.logo
            cell.header = informationData?.aboutViewData.header
            cell.details = informationData?.aboutViewData.details
            
            cell.setOfficeHours(title: informationData?.aboutViewData.officeTime.title, days: informationData?.aboutViewData.officeTime.label1, hours: informationData?.aboutViewData.officeTime.label2)
            
            
            cell.setPublicServiceHours(title: informationData?.aboutViewData.publicTime.title, days: informationData?.aboutViewData.publicTime.label1, hours: informationData?.aboutViewData.publicTime.label2)

            
            return cell
        
        case .office:
            
            let office = informationData?.officeViewData[index]
        
            if office?.expanded ?? false{
                let cell = tabelView.dequeueReusableCell(withIdentifier: InformationOfficeTableViewCell.namedNibName) as! InformationOfficeTableViewCell
                
                cell.office = office
                cell.delegate = self
                return cell
            }else{
                let cell = tabelView.dequeueReusableCell(withIdentifier: InformationOfficeTableViewCell.namedNibNameCollapsed) as! InformationOfficeTableViewCell
                
                cell.office = office
                cell.delegate = self
                
                return cell
            }
        case .buttons:
            let cell = tabelView.dequeueReusableCell(withIdentifier: InformationButonsTableViewCell.namedNibName) as! InformationButonsTableViewCell

            cell.selectionStyle = .none
            switch index{
            case 0:
                cell.clearSetting()
                cell.setLeftContent(text: "About Us", fontIcon: "\u{e021}", #colorLiteral(red: 0.968627451, green: 0.5803921569, blue: 0.1098039216, alpha: 0.1528788527), #colorLiteral(red: 0.9333333333, green: 0.5647058824, blue: 0.4823529412, alpha: 1))
                { [weak self] in
                    self?.displayToast(message: L10n.commingSoon)
                }
                
                cell.setRightContent(text: "Media Center", fontIcon: "\u{e021}", #colorLiteral(red: 0.5294117647, green: 0.7529411765, blue: 0.5176470588, alpha: 0.1510059932), #colorLiteral(red: 0.6431372549, green: 0.7764705882, blue: 0.5568627451, alpha: 1))
                { [weak self] in
                    self?.displayToast(message: L10n.commingSoon)
                }

                return cell
            case 1:
                cell.clearSetting()
                cell.setLeftContent(text: "Publication", fontIcon: "I", #colorLiteral(red: 0.9333333333, green: 0.5647058824, blue: 0.4823529412, alpha: 0.15), #colorLiteral(red: 0.9333333333, green: 0.5647058824, blue: 0.4823529412, alpha: 1))
                { [weak self] in
                        self?.displayToast(message: L10n.commingSoon)
                }
                
                return cell
            default:
                // empty space
                cell.clearSetting()
                return cell
            }
            
        }
        return UITableViewCell()
    }
}

extension EntitiesInformationViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // give nice effect for the selection
        tabelView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        
        let (type, index) = getItemTypeAndIndex(row: row)
        
        switch type{
        // get about cell
        case .about:
            break
        case .office:
            let expand = !(informationData?.officeViewData[index].expanded ?? false)
            informationData?.officeViewData[index].expanded = expand
            // viewModel.setOffice(index: index, expanded: expand)
            tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            
            if expand {
                tabelView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: true)
            }
        case .buttons:
            break
        }
    }
}


extension EntitiesInformationViewController: InformationOfficeTableViewCellDelegate {

    func mapButtonClicked(office: EntitiesInformationOfficeViewDataType?) {
        displayToast(message: L10n.commingSoon)
    }
    func phoneButtonClicked(office: EntitiesInformationOfficeViewDataType?) {
        displayToast(message: L10n.commingSoon)
    }
    func emailButtonClicked(office: EntitiesInformationOfficeViewDataType?) {
        displayToast(message: L10n.commingSoon)
    }
    func websiteButtonClicked(office: EntitiesInformationOfficeViewDataType?) {
        displayToast(message: L10n.commingSoon)
    }
    
}
