//
//  IntroViewCoordinator.swift
//  TAMMApp
//
//  Created by kerolos on 5/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import Swinject

protocol IntroViewCoordinatorDelegate: class {
    func introViewCoordinatorDidFinish()
}


class IntroViewCoordinator: NavigationCoordinator{
    internal var navigationController: UINavigationController
    weak var delegate: IntroViewCoordinatorDelegate?
    internal var container: Container
    private var navigationBarOriginalState: Bool!
    
    private func hideNavigationBar(){
        navigationBarOriginalState = navigationController.navigationBar.isHidden
        navigationController.navigationBar.isHidden = true
    }
    private func restoreNavigationBar() {
        navigationController.navigationBar.isHidden = navigationBarOriginalState
    }
    
    func start() {
        hideNavigationBar()
        startLanguageSwitch()
    }
    private func end(){
        restoreNavigationBar()
    }
    
    private func startLanguageSwitch(){
        let vc = container.resolveViewController(IntroLanguageSwitchViewController.self)
        vc.delegate = self
        navigationController.setViewControllers([vc], animated: true)
        
    }
    
    
    private func startPagingIntroPages() {
    
        let vc = container.resolveViewController(IntroPagingViewController.self)
        vc.coordinationDelegate = self
        vc.allViewControllers = getAllIntroViewControllers()
        navigationController.setViewControllers([vc], animated: true)
        
    }
    
    
    
    init(container: Container, navigationController: UINavigationController) {
        self.container = container
        self.navigationController = navigationController
    }
    
    func getAllIntroViewControllers() -> [IntroScreenViewController] {
        var controllers = [IntroScreenViewController]()
        for i in 0..<4{
           let vc = container.resolveViewController(IntroScreenViewController.self)
            vc.viewModel.pageNumber = i
            vc.viewModel.pageCount = 4
            controllers.append(vc)
        }
        return controllers
    }
    
  
}

extension IntroViewCoordinator: PagingViewControllerDelegate{
    func pageViewControllerDidFinish() {
        end()
        delegate?.introViewCoordinatorDidFinish()
    }
    
    
}

extension IntroViewCoordinator : IntroLanguageSwitchViewControllerDelegate{
    func introLanguageSelectionIsDone() {
        startPagingIntroPages()
    }
    
    
}

