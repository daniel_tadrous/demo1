//
//  EditFieldView.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class EditFieldView:UIView{
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var textField: LocalizedTextField!
    @IBOutlet weak var cancelButton: LocalizedButton!
    @IBOutlet weak var okButton: LocalizedButton!
    @IBOutlet weak var errorLabel: LocalizedLabel!
    
    var currentName = ""
    var closeClosure:(()->Void)!
    var okClosure:((_ :String)->Void)!
    
    @IBAction func cancelAction(_ sender: LocalizedButton) {
        self.removeFromSuperview()
        closeClosure()
    }
    
    @IBAction func okAction(_ sender: LocalizedButton) {
        if textField.text != "" && (textField.text?.count)! <= 16 {
            errorLabel.text = ""
            okClosure(textField.text!)
            self.removeFromSuperview()
        }
        else {
            errorLabel.text = L10n.letterCount
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    init(frame: CGRect,currentName:String) {
        super.init(frame: frame)
        self.currentName = currentName
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("EditFieldView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        errorLabel.text = ""
        textField.becomeFirstResponder()
        textField.text = currentName
    }
}
