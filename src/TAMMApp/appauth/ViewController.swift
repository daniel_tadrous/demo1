////
////  ViewController.swift
////  TAMMApp
////
////  Created by Marina.Riad on 3/25/18.
////  Copyright © 2018 Marina.Riad. All rights reserved.
////
//
//import UIKit
//import AppAuth
//import JWT
//
//
//class ViewController: UIViewController, OIDAuthStateChangeDelegate, OIDAuthStateErrorDelegate {
//
//    @IBOutlet var authAutoButton: UIButton!
//    @IBOutlet var authManual: UIButton!
//    @IBOutlet var codeExchangeButton: UIButton!
//    @IBOutlet var userinfoButton: UIButton!
//    @IBOutlet var clearAuthStateButton: UIButton!
//    @IBOutlet var logTextView: UITextView!
//
//    /*! @var kIssuer
//     // issuer: https://stage.smartpass.government.net.ae:443/secure/oauth2/TRA
//     @brief The OIDC issuer from which the configuration will be discovered.
//     */
//    // to be used in SmartPass
//    var kAuthorizationEndpoint:String?
//    // AuthorizationEndpoint: https://stage.smartpass.government.net.ae/secure/oauth2/authorize?realm=/TRA
//    var kTokenEndpoint:String?
//    // TokenEndpoint: https://stage.smartpass.government.net.ae/secure/oauth2/access_token
//    var kUserInfoEndpoint:String?
//    // UserInfoEndpoint: https://stage.smartpass.government.net.ae/secure/oauth2/userinfo?realm=/TRA
//    /*! @var kClientID
//     @brief The OAuth client ID.
//     The client should be registered with the "iOS" type.
//     */
//    var kClientID :String?
//
//    /*! @var kRedirectURI
//     @brief The OAuth redirect URI for the client @c kClientID.
//     @discussion With Google, the scheme of the redirect URI is the reverse DNS notation of the
//     client id. This scheme must be registered as a scheme in the project's Info
//     property list ("CFBundleURLTypes" plist key). Any path component will work, we use
//     'oauthredirect' here to help disambiguate from any other use of this scheme.
//     */
//    var kRedirectURI :String?
//
//    /*! @var kAppAuthExampleAuthStateKey
//     @brief NSCoding key for the authState property.
//     */
//    let kAppAuthExampleAuthStateKey = "authState";
//    let kAppAuthExampleUserInfoKey = "userInfo";
//    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//    /*! @property authState
//     @brief The authorization state. This is the AppAuth object that you should keep around and
//     serialize to disk.
//     */
//   var authState:OIDAuthState?
//    var jsonDictionaryUserInfo:Any?
//    // MARK:Methods
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.InitializeEndPoints()
//
//    }
//
//    func InitializeEndPoints(){
//        kClientID = (Bundle.main.object(forInfoDictionaryKey: "ClientID") as? String)!
//        kAuthorizationEndpoint = (Bundle.main.object(forInfoDictionaryKey: "AuthorizationEndpoint") as? String)!
//        kTokenEndpoint = (Bundle.main.object(forInfoDictionaryKey: "TokenEndpoint") as? String)!
//        kUserInfoEndpoint = (Bundle.main.object(forInfoDictionaryKey: "UserInfoEndpoint") as? String)!
//        kRedirectURI = (Bundle.main.object(forInfoDictionaryKey: "RedirectURI") as? String)!
//
//        // verifies that the custom URI scheme has been updated in the Info.plist
//        let urlTypes = Bundle.main.object(forInfoDictionaryKey: "CFBundleURLTypes") as? NSArray
//        assert(urlTypes != nil && (urlTypes?.count)! > 0, "No custom URI scheme has been configured for the project.")
//        let urlSchemes = (urlTypes!.object(at: 0) as! NSDictionary).object(forKey: "CFBundleURLSchemes") as? NSArray
//        // assert(urlSchemes != nil && (urlSchemes?.count)! > 0,"No custom URI scheme has been configured for the project.")
//        let urlScheme = urlSchemes!.object(at: 0) as? NSString
//    }
//
//    /*! @fn saveState
//     @brief Saves the @c OIDAuthState to @c NSUSerDefaults.
//     */
//    func saveState(){
//        // for production usage consider using the OS Keychain instead
//        if authState != nil{
//            let archivedAuthState = NSKeyedArchiver.archivedData(withRootObject: authState!)
//            UserDefaults.standard.set(archivedAuthState, forKey: kAppAuthExampleAuthStateKey)
//        }
//        else{
//            UserDefaults.standard.set(nil, forKey: kAppAuthExampleAuthStateKey)
//        }
//
//        UserDefaults.standard.synchronize()
//    }
//    func saveUserInfo(){
//        // for production usage consider using the OS Keychain instead
//        if jsonDictionaryUserInfo != nil{
//            let archivedUserInfo = NSKeyedArchiver.archivedData(withRootObject: jsonDictionaryUserInfo!)
//            UserDefaults.standard.set(archivedUserInfo, forKey: kAppAuthExampleUserInfoKey)
//        }
//        else{
//            UserDefaults.standard.set(nil, forKey: kAppAuthExampleUserInfoKey)
//        }
//
//        UserDefaults.standard.synchronize()
//    }
//
//    /*! @fn loadState
//     @brief Loads the @c OIDAuthState from @c NSUSerDefaults.
//     */
//    func loadState(){
//        // loads OIDAuthState from NSUSerDefaults
//        guard let archivedAuthState = UserDefaults.standard.object(forKey: kAppAuthExampleAuthStateKey) as? Data else{
//            return
//        }
//        guard let authState = NSKeyedUnarchiver.unarchiveObject(with: archivedAuthState) as? OIDAuthState else{
//            return
//        }
//        assignAuthState(authState)
//    }
//
//    func loadUserInfo(){
//        // loads OIDAuthState from NSUSerDefaults
//        guard let archivedUserInfo = UserDefaults.standard.object(forKey: kAppAuthExampleUserInfoKey) as? Data else{
//            return
//        }
//        guard let userInfo = NSKeyedUnarchiver.unarchiveObject(with: archivedUserInfo) as? OIDAuthState else{
//            return
//        }
//        self.jsonDictionaryUserInfo = userInfo;
//
//    }
//
//    func assignAuthState(_ authState:OIDAuthState?){
//        self.authState = authState
//        self.authState?.stateChangeDelegate = self
//        self.stateChanged()
//    }
//
//    /*! @fn updateUI
//     @brief Refreshes UI, typically called after the auth state changed.
//     */
//    func updateUI(){
////        // dynamically changes authorize button text depending on authorized state
////        if authState != nil {
////            userinfoButton.isEnabled = authState!.isAuthorized
////            clearAuthStateButton.isEnabled = true
////            //codeExchangeButton.isEnabled = (authState!.lastAuthorizationResponse.authorizationCode != nil) && (authState!.lastTokenResponse == nil)
////            //authAutoButton.setTitle("Re-authorize", for: UIControlState())
////            //authAutoButton.setTitle("Re-authorize", for:.highlighted)
////            authManual.setTitle("Re-authorize (Manual)", for: UIControlState())
////            authManual.setTitle("Re-authorize (Manual)", for:.highlighted)
////        }
////        else{
////            userinfoButton.isEnabled = false
////            clearAuthStateButton.isEnabled = false
////            //codeExchangeButton.isEnabled = false
////            //authAutoButton.setTitle("Authorize", for: UIControlState())
////            //authAutoButton.setTitle("Authorize", for:.highlighted)
////            authManual.setTitle("Authorize (Manual)", for: UIControlState())
////            authManual.setTitle("Authorize (Manual)", for:.highlighted)
////        }
//    }
//
//    func stateChanged(){
//        self.saveState()
//        self.updateUI()
//    }
//
//
//    func didChange(_ state: OIDAuthState) {
//        authState = state
//        authState?.stateChangeDelegate = self
//        self.stateChanged()
//    }
//
//    func authState(_ state: OIDAuthState, didEncounterAuthorizationError error: Error) {
//        self.logMessage("Received authorization error: \(error)")
//    }
//
//
//    /*! @fn authNoCodeExchange:
//     @brief Authorization code flow without a the code exchange (need to call @c codeExchange:
//     manually)
//     @param sender IBAction sender.
//     */
//    @IBAction func authNoCodeExchange(_ sender: AnyObject) {
//        //let issuer = URL(string: kIssuer)
//        let redirectURI = URL(string: kRedirectURI!)
//
//        //self.logMessage("Fetching configuration for issuer: \(issuer!)")
//
//        // discovers endpoints
//        let authorizationEndpoint = URL(string: kAuthorizationEndpoint!);
//        let tokenEndpoint = URL(string: kTokenEndpoint!);
//
//        //self.logMessage("Fetching configuration for issuer: \(issuer!)")
//        self.logMessage("Fetching configuration for authorization Endpoint: \(authorizationEndpoint!)")
//        self.logMessage("Fetching configuration for token Endpoint: \(tokenEndpoint!)")
//
//        // discovers endpoints
//        let configuration = OIDServiceConfiguration(authorizationEndpoint:authorizationEndpoint!,tokenEndpoint:tokenEndpoint!);
//
//        self.logMessage("Got configuration: \(configuration)")
//
//        // builds authentication request
//        let request = OIDAuthorizationRequest(configuration: configuration, clientId: self.kClientID!, scopes: [OIDScopeOpenID, OIDScopeProfile], redirectURL: redirectURI!, responseType: OIDResponseTypeCode, additionalParameters: nil)
//        let state = request.state;
//        // performs authentication request
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        self.logMessage("Initiating authorization request: \(request)")
//        appDelegate.currentAuthorizationFlow = OIDAuthorizationService.present(request, presenting: self){
//            authorizationResponse, error in
//            if authorizationResponse != nil{
//                self.assignAuthState(OIDAuthState(authorizationResponse: authorizationResponse!))
//                 self.logMessage("Authorization response : \(authorizationResponse!)")
//                self.logMessage("Authorization response with code: \(authorizationResponse!.authorizationCode!)")
//                if(authorizationResponse!.state == state){
//                    self.codeExchangeLogic();
//                }
//                else{
//                    self.logMessage("Authorization error: state retrieved is invalid")
//                }
//                // could just call [self tokenExchange:nil] directly, but will let the user initiate it.
//            }
//            else{
//                self.logMessage("Authorization error: \(error!.localizedDescription)")
//            }
//        }
//    }
//
//
//    /*! @fn codeExchange:
//     @brief Performs the authorization code exchange at the token endpoint.
//     @param sender IBAction sender.
//     */
//    func codeExchangeLogic(){
//        // performs code exchange request
//        let tokenExchangeRequest = authState?.lastAuthorizationResponse.tokenExchangeRequest()
//        self.logMessage("Performing authorization code exchange with request [\(tokenExchangeRequest!)]")
//        OIDAuthorizationService.perform(tokenExchangeRequest!){
//            tokenResponse,error in
//            if tokenResponse == nil{
//                self.logMessage("Token exchange error: \(error!.localizedDescription)")
//            }
//            else{
//                self.logMessage("Received token response : \(tokenResponse!)")
//                self.logMessage("Received token response with accessToken: \(tokenResponse!.accessToken!)")
//
//            }
//            self.authState?.update(with: tokenResponse, error: error)
//
//            if(self.validateTokenId(idToken: tokenResponse!.idToken!)){
//                self.getUserInfo();
//            }
//            else{
//                // put the error recieved from validateToken
//                self.logMessage("Received id token is not valid")
//            }
//        }
//    }
//
//    /*! @fn clearAuthState:
//     @brief Nils the @c OIDAuthState object.
//     @param sender IBAction sender.
//     */
//    func validateTokenId(idToken:String)->Bool{
//        self.logMessage("validate id token")
////        do {
////            let claims: ClaimSet = try JWT.decode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.2_8pWJfyPup0YwOXK7g9Dn0cF1E3pdn299t4hSeJy5w", algorithm: .hs384("secret".data(using: .utf8)!))
////            print(claims)
////        } catch {
////            print("Failed to decode JWT: \(error)")
////        }
////        let claim:ClaimSet = try! JWT.decode(idToken, algorithm: .hs512("secret".data(using: .utf8)!),leeway:10);
////        self.logMessage("Decode Id Token : \(claim)")
//        // 1- decode
//        //2- check if it is valid signature
//        //3- check if iss is equal to https://smartpass.government.ae:443/secure/oauth2/TRA
//        //4- check aud is equal to cliend id
//        //5- check azp is equal to cliend id
//        //6- check if current time is before exp
//        //7- check if acr is equal to request.acr (additional parameters)
//        //8- check if time between auth-time and current-time is not greater than acceptable range(30 mins)
//        self.logMessage("id token validation succeeded")
//        return true;
//
//    }
//    @IBAction func clearAuthState(_ sender: AnyObject) {
//        self.assignAuthState(nil)
//    }
//
//    /*! @fn clearLog:
//     @brief Clears the UI log.
//     @param sender IBAction sender.
//     */
//    @IBAction func clearLog(_ sender: AnyObject) {
//        logTextView.text = ""
//    }
//
//    /*! @fn userinfo:
//     @brief Performs a Userinfo API call using @c OIDAuthState.withFreshTokensPerformAction.
//     @param sender IBAction sender.
//     */
//    func getUserInfo(){
//        let userInfoEndpoint = URL(string: kUserInfoEndpoint!);
//        if userInfoEndpoint == nil{
//            self.logMessage("Userinfo endpoint not declared in discovery document")
//            return
//        }
//        let currentAccessToken = authState?.lastTokenResponse?.accessToken
//
//        self.logMessage("Performing userinfo request")
//
//        authState?.performAction(freshTokens: {(accessToken,idToken,error) in
//
//            if error != nil{
//                self.logMessage("Error fetching fresh tokens: \(error!.localizedDescription)")
//                return
//            }
//
//            // log whether a token refresh occurred
//            if currentAccessToken != accessToken{
//                self.logMessage("Access token was refreshed automatically (\(currentAccessToken!) to \(accessToken!)")
//            }
//            else{
//                self.logMessage("Access token was fresh and not updated [\(accessToken!)]")
//            }
//
//            // creates request to the userinfo endpoint, with access token in the Authorization header
//            var request = URLRequest(url: userInfoEndpoint!)
//            let authorizationHeaderValue = "Bearer \(accessToken!)"
//            request.addValue(authorizationHeaderValue, forHTTPHeaderField: "Authorization")
//
//            let configuration = URLSessionConfiguration.default
//            let session = URLSession(configuration: configuration)
//
//            // performs HTTP request
//            let postDataTask = session.dataTask(with: request, completionHandler: {
//                (data,response,error) in
//                DispatchQueue.main.async{
//                    guard let httpResponse = response as? HTTPURLResponse else{
//                        self.logMessage("Non-HTTP response \(String(describing: error))")
//                        return
//                    }
//                    do{
//                        self.logMessage("HTTP response :\(String(describing: response))")
//                        let jsonDictionaryOrArray = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
//                        if httpResponse.statusCode != 200{
//                            let responseText = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//                            if httpResponse.statusCode == 401{
//                                // "401 Unauthorized" generally indicates there is an issue with the authorization
//                                // grant. Puts OIDAuthState into an error state.
//                                let oauthError = OIDErrorUtilities.resourceServerAuthorizationError(withCode: 0, errorResponse: jsonDictionaryOrArray as? [AnyHashable: Any], underlyingError: error)
//                                self.authState?.update(withAuthorizationError: oauthError)
//                                //log error
//                                self.logMessage("Authorization Error (\(oauthError)). Response: \(String(describing: responseText))")
//                            }
//                            else{
//                                self.logMessage("HTTP: \(httpResponse.statusCode). Response: \(String(describing: responseText))")
//                            }
//                            return
//                        }
//                        self.logMessage("Success: \(jsonDictionaryOrArray)")
//                        self.jsonDictionaryUserInfo = jsonDictionaryOrArray;
//                        self.saveUserInfo();
//                        let Profile = self.storyboard?.instantiateViewController(withIdentifier: "Profile") as! ProfileViewController;
//                        Profile.setVC(userInfo: self.jsonDictionaryUserInfo! , displayLog: true);
//                        self.present(Profile, animated: true, completion: nil);
//                    }
//                    catch{
//                        self.logMessage("Error while serializing data to JSON")
//                    }
//                }
//            })
//            postDataTask.resume()
//        })
//    }
//
//
//    /*! @fn logMessage
//     @brief Logs a message to stdout and the textfield.
//     @param format The format string
//     */
//    func logMessage(_ message:String){
//        // outputs to stdout
//        print(message)
//
//        // appends to output log
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "hh:mm:ss"
//        let dateString = dateFormatter.string(from: Date())
//        appDelegate.logText = appDelegate.logText +  ((appDelegate.logText.isEmpty) ? "" : "\n") + dateString + ":" + message + "\n"
//
//    }
//}
//
//
//
