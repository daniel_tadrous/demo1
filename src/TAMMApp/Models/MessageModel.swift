//
//  MessageModel.swift
//  TAMMApp
//
//  Created by Daniel on 7/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper
enum CASE_USER_INTERACTION{
    case none , approved , rejected
}
class MessageModel:Mappable{
    
    var id: Int?
    var status: MsgStatus?
    var statusMessage: String?
    var statusDates: [String] = []
    var statusHistory:[Status] = []
    var location: Address?
    
    var type: CaseType?
    var caseId: String?
    var isArchived: Bool?
    var isDeleted: Bool?
    var isStarred: Bool?
    var tag:String?
    var receivedBy:String?
    var createDate:String?
    var expectedClosureDate:String?
    var entity: MsgEntity?
    var attachments: [MsgAttachment] = []
    var additionalComments:[MsgAdditionalComments] = []
    var contacts: Contact?
    var caseDetails:CaseMsg?
    var caseTitle: String?
    
    var isOpened:Bool = false
    var isSelected:Bool = false
    var caseUserInteraction :CASE_USER_INTERACTION = .none
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        status <- map["status"]
        statusMessage <- map["statusMessage"]
        type <- map["type"]
        caseId <- map["caseId"]
        isArchived <- map["isArchived"]
        isDeleted <- map["isDeleted"]
        isStarred <- map["isStarred"]
        tag <- map["tag"]
        receivedBy <- map["receivedBy"]
        createDate <- map["createDate"]
        entity <- map["entity"]
        attachments <- map["attachments"]
        additionalComments <- map["additionalComments"]
        statusDates <- map["statusDates"]
        expectedClosureDate <- map["expectedClosureDate"]
        contacts <- map["contacts"]
        caseDetails <- map["caseDetails"]
        caseTitle <- map["caseTitle"]
        statusHistory <- map["statusHistory"]
        location <- map["location"]
    }
    
    func isCaseDetailsRecordDownloaded()->Bool{
        
        return FileManager.default.fileExists(atPath: caseDetailsRecordFullUrl.path)
    }
    
    var caseDetailsRecordFullUrl:URL{
        
        let documentsURL =  IncidentUrl.getDirectoryForIncident()
        let fileURL = documentsURL.appendingPathComponent("\(caseDetailsRecordFileName).mp3")
        
        return fileURL
    }
    var caseDetailsRecordFileName:String{
        return "case_details_audio_record\(self.id ?? 0)"
    }
    
    
    class QuestionModel:Mappable{
        
        var id: Int?
        var title: String?
        var answers: [AnswerModel] = []
        
        required init?(map: Map) {
            
        }
        // Mappable
        func mapping(map: Map) {
            id <- map["id"]
            title <- map["title"]
            answers <- map["answers"]
        }
        
    }
    class AnswerModel:Mappable{
        
        var id: Int?
        var text: String?
        var qIndex: Int?
        required init?(map: Map) {
            
        }
        
        // Mappable
        func mapping(map: Map) {
            id <- map["id"]
            text <- map["text"]
        }
        
    }
    
    
}

class MsgAdditionalComments:Mappable{
    

    var text: String?
    var date: String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        text            <- map["text"]
        date         <- map["date"]
    }
}

class Status:Mappable{
    var id: Int?
    var name: String?
    var date: String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        date <- map["date"]
    }
}
class CaseType: Mappable{
    var name: String?
    var id : Int?
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
