//
//  ClosedMsgsViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

import RxSwift

class ClosedMsgsViewModel {
    
    private(set) var viewData: Variable<[MessageModel]?> = Variable(nil)
    private(set) var start: Variable<Bool?> = Variable(nil)
    private(set) var messages:Variable<[MessageModel]?> = Variable(nil)
    private(set) var messagesFilter: Variable<MessagesFilter?> = Variable(nil)
    private(set) var messagesUpdated: Variable<[MessageModel]?> = Variable(nil)
    fileprivate var apiService: ApiSupportServicesType!
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiSupportServicesType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        start.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        messagesFilter.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetailsWithFilter()
            })
            .disposed(by:disposeBag)
        messages.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.updateMessage()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if start.value != nil && start.value!{
            apiService.getMyMessages(filter: nil)
                .subscribe(onNext: { [unowned self] polls in
                    self.viewData.value = polls
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
    fileprivate func requestDetailsWithFilter(){
        if messagesFilter.value != nil {
            apiService.getMyMessages(filter: messagesFilter.value)
                .subscribe(onNext: { [unowned self] messages in
                    self.viewData.value = messages
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func updateMessage(){
        if messages.value != nil{
            apiService.updateMessage(messages: messages.value!)
                .subscribe(onNext: { [unowned self] messagesAr in
                    
                    for mess in self.messages.value!{
                        let index = 0
                        for m in self.viewData.value!{
                            if mess.id == m.id{
                                
                                m.isArchived = mess.isArchived
                                //m.isDeleted = message.isDeleted
                                m.isStarred = mess.isStarred
                                if mess.isDeleted!{
                                    self.viewData.value?.remove(at: index)
                                }
                                
                                break
                                
                                
                            }
                        }
                    }
                    self.messagesUpdated.value = self.messages.value
                    
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
}

