//
//  AttachementsPopupViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/29/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import BSImagePicker
import Photos
import MediaPlayer
import MobileCoreServices
import Swinject


class AttachementsPopupViewController: UIViewController {

    var container: Container?
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var mainView: RoundedView!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    var isAddingData : Variable<Bool> = Variable(false)
    var isSizeRight : Variable<Bool> = Variable(true)
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    @IBOutlet weak var closeView: UIView!
    
    let maxFileSize:Double = 8
    
    @IBOutlet weak var addMediaView: RoundedView!
    @IBOutlet weak var mediaView: RoundedView!
    
    @IBOutlet weak var fileNameLabel: UILabel!
    
    @IBOutlet weak var fileSizeLabel: UILabel!
    private var mediaActionSheet:GalleryActionSheet?
    private let internetService:InternetCheckingService = InternetCheckingService.shared
    
    @IBOutlet weak var addMediaIcon: UILabel!
    @IBOutlet weak var addMediaLabel: LocalizedLabel!
    @IBOutlet weak var grayView: UIView!
    @IBOutlet weak var uploadTitleLabel: LocalizedLabel!
    @IBOutlet weak var submitBtn: LocalizedButton!
    
    @IBOutlet weak var closeLabel: UILabel!
    @IBOutlet weak var errorLabel: LocalizedLabel!

    @IBOutlet weak var mediaIcon: IconLabel!
    @IBOutlet weak var cancelBtn: LocalizedButton!
    var parentVC:CaseDetailsViewController?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let disposeBag = DisposeBag()
    private var okClickHandler: ((_ mediaURL:URL)->Void)?

    @IBOutlet weak var viewAroundmediaSelected: RoundedView!
    
    @IBOutlet weak var closeInSelectedMediaBtn: UIButton!
    @IBOutlet weak var blackCircleInselectedMedia: UILabel!
    var mediaUrl:URL? = nil
    //    var titleStr:String?
    //    var message:String?
    //    var okBtnTitle:String?
    //    var cancelBtnTitle:String?
    
    @IBAction func cancelClickHandler(_ sender: LocalizedButton) {
        dismiss(animated: true) {self.appDelegate.addfloatingMenu()}
    }
    
    
    @IBAction func removeMediaAction(_ sender: Any) {
        self.isAddingData.value = false
        self.isSizeRight.value = true
        errorLabel.isHidden = true
        mediaUrl = nil
    }
    
    @IBAction func submitBtnAction(_ sender: Any) {
        
        if mediaUrl != nil && isSizeRight.value{
            // media found
            
            
             if UserDefaultsKeys.getisWifiOnlyStatus() {
                // upload via wifi selected
                if !WifiInternetService.isWifiConnected() && self.internetService.hasInternet.value!{
                    //wifi is not connected
                    
                    let vc = ConfirmationMessageViewController()
                    //save to draft is the submit while Ok is cancel
                    
                    vc.initialize(title: L10n.upload_Over_WIFI, message: L10n.youHaveChosenToUpload, CancelBtnTitle: L10n.cancel, confirmationBtnTitle: L10n.uploadMediaOverNetwork , okClickHandler: {
                        self.dismissOverlay()
                        self.okClickHandler?(self.mediaUrl!);
                    } , saveToDraftsHandler: {
                        
                        
                    })
                     self.present(vc, animated: true, completion: nil)
                }else {
                    okClickHandler?(mediaUrl!)
                    dismissOverlay()
                }
             }else {
                okClickHandler?(mediaUrl!)
                dismissOverlay()
            }
           
           
        }else{
            // there is no url or file is Larger than 8 MB
            if isSizeRight.value == false {
                //Toast(message:"size").Show()
            }else {
                 Toast(message:L10n.selectMediaFirst).Show()
            }
            
        }
        
       
    }
    
//    func saveMessageToDrafts(NewMessage:IncidentMessageViewData){
//        let media = NewMessage.media ?? []
//        let voiceURI = (NewMessage.voiceURI?.absoluteString) ?? ""
//        let draft = DALDrafts(draftTitle: NewMessage.title, typeId: NewMessage.messageTypeId!, location: NewMessage.incidentLocation!, locationCoordinates: NewMessage.locationCoordinates, mediaURis: media , hasMedia: NewMessage.hasMedia!, isInformationPublic: NewMessage.isInformationPublic, notifiedByEmail: NewMessage.notifiedByEmail!, notifiedByMobileApplication: NewMessage.notifiedByMobileApplication!, notifiedByPhone: NewMessage.notifiedByPhone!, draftDate: Date(), voiceNote: voiceURI , comment: NewMessage.comment ?? "" )
//        draft.save()
//
//    }
    
    
    
    
    func dismissOverlay() {
        dismiss(animated: true) {self.appDelegate.addfloatingMenu()}
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        addDashedBorderForAddMediaView(view: addMediaView)
        addDashedBorderForAddMediaView(view: mediaView)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        self.appDelegate.removeFloatingMenu()
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeIconClicked))
        closeView.addGestureRecognizer(tap)
        
        let tapOnAddMedia = UITapGestureRecognizer(target: self, action: #selector(addMediaClicked))
        addMediaView.addGestureRecognizer(tapOnAddMedia)
        
//        addDashedBorderForMediaView()
//        addDashedBorderForAddMediaView()
        
        mediaView.isHidden = true
        setupUI()
        applyInvertColors()
    }
    
    private func applyInvertColors() {
 
        backgroundView.backgroundColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1).getAdjustedColor()
        mainView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        closeLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        titleLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        descriptionLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        uploadTitleLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        addMediaLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        errorLabel.textColor = #colorLiteral(red: 1, green: 0.3061913165, blue: 0.1476819464, alpha: 1).getAdjustedColor()
        addMediaIcon.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        mediaIcon.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        fileSizeLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        fileNameLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        grayView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1).getAdjustedColor()
        submitBtn.backgroundColor = #colorLiteral(red: 1, green: 0.8666666667, blue: 0, alpha: 1).getAdjustedColor()
        submitBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        cancelBtn.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        cancelBtn.setTitleColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor(), for: .normal)
        closeInSelectedMediaBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor(), for: .normal)
        blackCircleInselectedMedia.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
    }
    
    
    
    
    
    private func setupUI() {
        isAddingData.asObservable().subscribe(onNext: { boolValue in
            if boolValue {
                self.mediaView.isHidden = false
                self.addMediaView.isHidden = true
            }else {
                self.mediaView.isHidden = true
                self.addMediaView.isHidden = false
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func addMediaClicked() {
        
        self.view.endEditing(true)
        self.mediaActionSheet = GalleryActionSheet(vc:self)
        self.mediaActionSheet?.delegate = self
        self.hideFloatingMenu()
        
        //self.mediaActionSheet!.show(inView: (self.tabBarController?.view) ?? (self.view)!)
        self.mediaActionSheet!.show(inView: self.view)
        //self.delegate?.openMediaActionbar(actionSheet: self.mediaActionSheet!)
    }
    
    private func addDashedBorderForAddMediaView(view: UIView) {
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.init(red: 204/255, green: 221/255, blue: 226/255, alpha: 1).getAdjustedColor().cgColor
        yourViewBorder.lineDashPattern = [5, 2]
        let rect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 100, height: view.bounds.height)
        yourViewBorder.frame = rect
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: rect).cgPath
        view.layer.addSublayer(yourViewBorder)
    }
    
    
    @objc func closeIconClicked() {
        dismiss(animated: true, completion: {
            
            self.appDelegate.addfloatingMenu()
        })
    }
    
    
    func initialize(okClickHandler: @escaping (_ comment:URL)->Void, container: Container) {
        self.okClickHandler = okClickHandler
        self.container = container
        commonIntializer()
    }
    
    
    
    
    private func commonIntializer(){
        self.modalPresentationStyle = .overFullScreen
    }
    
    private func commonInit(){
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey.getAdjustedColor())
        self.cancelBtn.layer.borderWidth = 1
        self.cancelBtn.layer.borderColor = UIColor.turquoiseBlue.getAdjustedColor().cgColor
        self.submitBtn.addRoundCorners(radious: (self.submitBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
        // setupUiData()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.submitBtn.addRoundCorners(radious: (self.submitBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
    }
    

}

extension AttachementsPopupViewController : GalleryActionSheetDelegate {

    
   
    func openDocuments() {
        
        DispatchQueue.main.async {
            //self.isAddingData = true
            let docPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeText)], in: UIDocumentPickerMode.import)
            docPicker.delegate = self
            
            self.present(docPicker, animated: true, completion: nil)
        }
    }

    func openSoundFiles() {
        let status = MPMediaLibrary.authorizationStatus()
        switch status {
            
        case .notDetermined:
            MPMediaLibrary.requestAuthorization() { status in
                if status == .authorized {
                    //self.isAddingData = true
                    DispatchQueue.main.async {
                        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.anyAudio)
                        mediaPicker.allowsPickingMultipleItems = true
                        mediaPicker.delegate = self
                        self.present(mediaPicker, animated: true, completion: nil)
                    }
                }
            }
        case .denied:
            DispatchQueue.main.async {
                let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: "Sound permission denied", onComplete: {
                })
                UIApplication.shared.keyWindow!.addSubview(permessionView)
                UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
                
            }
        case .restricted:
            break
        case .authorized:
            DispatchQueue.main.async {
                //self.isAddingData = true
                let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.anyAudio)
                mediaPicker.allowsPickingMultipleItems = true
                mediaPicker.delegate = self
                
                self.present(mediaPicker, animated: true, completion: nil)
            }
        }
    }

    func openCamera() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.removeFloatingMenu()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.navigationController?.setNavigationBarHidden(true, animated: false)
        let vc = container?.resolveViewController(CameraViewController.self)
        vc?.delegate =  self
        vc?.numberOfImages = CGFloat(1)
        vc?.hidesBottomBarWhenPushed = true
        self.present(vc!, animated: false, completion: nil)
        //self.navigationController?.pushViewController(vc!, animated: true)
        
    }
//
    func openGallery() {
        //        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
        //            let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //            appDelegate.removeFloatingMenu()
        //            let myPickerController = UIImagePickerController()
        //            myPickerController.delegate = self;
        //            myPickerController.sourceType = .photoLibrary
        //            self.present(myPickerController, animated: true, completion: nil)
        //        }

        if GalleryPermission.isOpenGalleryAvailable() {
            
            let vc = BSImagePickerViewController()
            vc.maxNumberOfSelections = 1
            //vc.maxNumberOfSelections = viewModel.availableAssets
            self.hideFloatingMenu()
            bs_presentImagePickerController(vc, animated: true,
                                            select: { (asset: PHAsset) -> Void in
                                                print("Selected: \(asset)")
            }, deselect: { (asset: PHAsset) -> Void in
                print("Deselected: \(asset)")
            }, cancel: { (assets: [PHAsset]) -> Void in
                DispatchQueue.main.async{
                    self.displayFloatingMenu()
                }
                //self.isAddingData = false
                print("Cancel: \(assets)")
            }, finish: { (assets: [PHAsset]) -> Void in
                DispatchQueue.main.async{
//                    self.displayFloatingMenu()
//                    self.viewModel.setRawMedia(rawMedia: assets)
                    
                     let imageURL = self.saveImagesLocally(image: assets[0])
                    self.fileAdded(imageURL:imageURL)
                    
//                    self.incidentView.exceededLimitCheck = self.viewModel.exceededLimitCheck
//                    self.incidentView.notSupportedCheck = self.viewModel.notSupportedCheck
//                    self.exceededLimitCheck = self.viewModel.exceededLimitCheck
//                    self.notSupportedCheck = self.viewModel.notSupportedCheck
//                    self.incidentView.mediaList = self.viewModel.getMediaList()

                }

               // self.isAddingData = false
                print("Finish: \(assets)")
            }, completion: nil)
        }
    }


    fileprivate func fileAdded(imageURL:URL) {
        self.isAddingData.value = true
      
        self.mediaUrl = imageURL
        
       // let fileSize = imageURL.sizePerMB()
        let fileSize = Double(imageURL.fileSizeInKB)
        
        if fileSize > maxFileSize*1024 {
            self.isSizeRight.value = false
        }
        
        if imageURL.isSizeRight() == false {
            self.errorLabel.isHidden = false
        }
        
        self.fileSizeLabel.text = "\(fileSize/1024) " + L10n.megaByte
        self.fileNameLabel.text = imageURL.lastPathComponent
       self.mediaIcon.text = nil
        switch imageURL.type {
        case .AUDIO:
            self.mediaIcon.text = "\u{e04d}"
        case .VIDEO:
            self.mediaIcon.text = "\u{e020}"
        case .IMAGE:
            self.mediaIcon.text = "\u{e041}"
        case.TXT:
            self.mediaIcon.text = "\u{e046}"
        }

    }


    func saveImagesLocally(image:PHAsset) -> URL{
        let directory = IncidentUrl.getDirectoryForIncident()
            let imgInfo = image.getFileSizeAndSizeFormatedAndName()
            image.getURL(completionHandler: { imgUrl in
                let imageData = try? Data(contentsOf: imgUrl!)
                do {
                    try imageData?.write(to: directory.appendingPathComponent(imgInfo.2!))

                } catch {
                    print(error.localizedDescription)

                }

            })
            return directory.appendingPathComponent(imgInfo.2!)
        }
    }


//
extension AttachementsPopupViewController: MPMediaPickerControllerDelegate{
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true, completion: nil)
    }
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
//        viewModel.setsoundMedia(rawMedia: mediaItemCollection.items,closure:{
//            mediaPicker.dismiss(animated: true, completion: nil)
//        })
        
        let mediaItem = mediaItemCollection.items[0]
       // let directory = IncidentUrl.getDirectoryForIncident()
       // let mediaData = try? Data(contentsOf: mediaItem.assetURL!)
        
        mediaItem.export(completionHandler: { url, error in
            
            if url != nil {
                 DispatchQueue.main.async{
                    self.fileAdded(imageURL: url!)
                }
               
            }
            mediaPicker.dismiss(animated: true, completion: nil)
            })
        
//        do {
//            try mediaData?.write(to: directory.appendingPathComponent((mediaItem.assetURL?.lastPathComponent)!))
//
//        } catch {
//            print(error.localizedDescription)
//        }
//        let mediaUrl =  directory.appendingPathComponent((mediaItem.assetURL?.lastPathComponent)!)
        
        
        
    }
}

extension AttachementsPopupViewController:UIDocumentMenuDelegate{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
    }
    
    public func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController){
        documentMenu.dismiss(animated: true, completion: nil)
    }
}

extension AttachementsPopupViewController:UIDocumentPickerDelegate{
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController){
        controller.dismiss(animated: true, completion: nil)
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]){
        if urls.count > 0  {
            
            let docURL = urls.last
            mediaUrl = docURL
            let directory = IncidentUrl.getDirectoryForIncident()
            let imageData = try? Data(contentsOf: docURL!)
            do {
                try imageData?.write(to: directory.appendingPathComponent((docURL?.lastPathComponent)!))
                
            } catch {
                print(error.localizedDescription)
                
            }

            self.fileAdded(imageURL:docURL!)
            
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

extension AttachementsPopupViewController : CameraViewControllerDelegate {
    func cameraIsDismissed(assests: [PHAsset]) {
        if assests.count == 0 || assests == nil {
            // camera dismissed without selections
        }else {
            
            let imageURL = self.saveImagesLocally(image: assests.last!)
            self.fileAdded(imageURL:imageURL)
           
        }
    }
    
    
}
