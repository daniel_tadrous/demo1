//
//  PrefrencesViewController.swift
//  TAMMApp
//
//  Created by Daniel on 8/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AccessibilityViewController: TammViewController, AccessibilityStoryboardLoadable {
 
    @IBOutlet weak var slider: UISlider!

    @IBOutlet weak var invertColorSwitch: UISwitch!
    
    @IBOutlet var labelsInView: [LocalizedLabel]!

    @IBOutlet var whiteViews: [UIView]!
    
    @IBOutlet weak var greyView: UIView!

    let disposeBag = DisposeBag()
    var viewModel: AccessibilityViewModel!
    
    @IBAction func onSliderChange(_ sender: UISlider) {
        let value = Float(Int(sender.value))
        viewModel.setFontAccessibilitySize(size: CGFloat(value))
        sender.value = value
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: L10n.Preferences.accessibility, willSetSearchButton: true)
        slider.value = Float(viewModel.getFontAccessibilitySize())
        invertColorSwitch.isOn = viewModel.getInvertedStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.barTintColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.tabBarController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.navigationController?.navigationBar.backgroundColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.barTintColor =  UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white.getAdjustedColor()]
        self.navigationController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white.getAdjustedColor()
        for currentView in whiteViews {
            currentView.backgroundColor = UIColor.white.getAdjustedColor()
        }
        for currentLabel in labelsInView {
            currentLabel.backgroundColor = UIColor.white.getAdjustedColor()
            currentLabel.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        }
        self.greyView.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.slider.backgroundColor = UIColor.white.getAdjustedColor()
        self.slider.tintColor = UIColor.turquoiseBlue.getAdjustedColor()
    }
    
    
    @IBAction func swicthAction(_ sender: Any) {
        let myswitch = sender as! UISwitch
        self.viewModel.setInvertedStatus(withValue: myswitch.isOn)
        self.tabBarController?.tabBar.barTintColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.tabBarController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.navigationController?.navigationBar.backgroundColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.barTintColor =  UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white.getAdjustedColor()]
        self.navigationController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white.getAdjustedColor()
        for currentView in whiteViews {
            currentView.backgroundColor = UIColor.white.getAdjustedColor()
        }
        for currentLabel in labelsInView {
            currentLabel.backgroundColor = UIColor.white.getAdjustedColor()
            currentLabel.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        }
        self.greyView.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.slider.backgroundColor = UIColor.white.getAdjustedColor()
        self.slider.tintColor = UIColor.turquoiseBlue.getAdjustedColor()
       // self.invertColorSwitch.tintColor = UIColor.petrol.getAdjustedColor()
        adjustPreviousPagees()
        MainTabViewController.applyCurrentTheme()
        

    }
    

}
