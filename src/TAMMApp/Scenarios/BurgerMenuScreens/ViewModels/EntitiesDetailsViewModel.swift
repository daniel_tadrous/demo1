//
//  EntitiesDetailsViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
class EntitiesDetailsViewModel :  SearchViewModelDelegate {
    
    

    var serviceCategoryId:Variable<String?> = Variable(nil)
    var apiService:ApiEntityServices
    var disposeBag = DisposeBag()
    var servicecategories : Variable<[ServiceCategoryEntityModel]?> = Variable(nil)
    var isToGetTrending: Variable<Bool?> = Variable(nil)
    var trendingVar: Variable<[String]?> = Variable(nil)
    var queryResultVar: Variable<[String]?> = Variable(nil)
    var currentQueryVar: Variable<String?> = Variable(nil)
    var searchedText:Variable<String?> = Variable(nil)
    var searchviewModel:SeacrhViewModel = SeacrhViewModel()
    
    init(apiService: ApiEntityServices) {
        self.apiService = apiService
        setupBinding()
    }
    
    

    fileprivate func setupBinding(){
        
        serviceCategoryId.asObservable().subscribe( { _ in
            self.requestDetails()
        }).disposed(by: disposeBag)
        
        searchedText.asObservable().subscribe( { _ in
            self.requestDetails()
        }).disposed(by: disposeBag)
        
        isToGetTrending.asObservable().subscribe(onNext: { (go:Bool?) in
            if go != nil{
                self.getTrending()
            }
        }).disposed(by: disposeBag)
        
        currentQueryVar.asObservable().subscribe(onNext: { (descriptionText) in
            if descriptionText != nil {
                self.getQueryResults(q:descriptionText!)
            }
        }).disposed(by: disposeBag)
        
    }
    
    
    fileprivate func requestDetails(){
        
        if serviceCategoryId.value != nil && !serviceCategoryId.value!.isEmpty {
                        apiService.getAllCategoryServices(withid: serviceCategoryId.value!)
                            .asObservable().subscribe(onNext: { [unowned self] serviceCategoryList in
                                                self.servicecategories.value = serviceCategoryList
                                                print("\(self.servicecategories)")
                                                print("Hello from View Model")
                            }).disposed(by: disposeBag)
    }
    
    }
    
    func getQueryResults(q:String){
        apiService.getSearchServices(q: q, type:1)
            .subscribe(onNext:{
                [unowned self] popArr in
                self.queryResultVar.value = popArr
            }).disposed(by: disposeBag)
    }
    
    
    func getTrending(){
        apiService.getSearchServices(q: nil, type: 0)
            .asObservable()
            .subscribe(onNext: { trendingItems in
                self.trendingVar.value = trendingItems
                print("\(trendingItems)")
            }).disposed(by: disposeBag)
    }
    
    
    
    
    
    
    
    
    
    
}
