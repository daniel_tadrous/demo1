//
//  HolidayCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class HolidayCell: UITableViewCell {

    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageAspectRatioConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var holdingView: UIView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventVenues: UILabel?
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventDescription: UILabel?
    @IBOutlet weak var eventStartMonth: UILabel!
    @IBOutlet weak var eventStartDay: UILabel!
    @IBOutlet weak var addToCalenderButton: LocalizedButton?
    @IBOutlet weak var eventEndMonth: UILabel!
    @IBOutlet weak var eventEndDay: UILabel!
    @IBOutlet weak var Hijri: LocalizedLabel?
    @IBOutlet weak var conatinerView: UIView!
    @IBOutlet weak var categoryButton: LocalizedButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var findOutMoreButton: LocalizedButton?
    @IBOutlet weak var shareEventButton: UIButton!
    @IBOutlet weak var timeLabel: LocalizedLabel?
    
    @IBOutlet weak var expandedView: UIView!
    @IBOutlet weak var collapsedView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var eventVenuesExpanded: LocalizedLabel?
    @IBOutlet weak var eventTimeExpanded: LocalizedLabel?
    
    var addToCalenderClosure:(()->Void)!
    var findOutMoreClosure:(()->Void)!
    var shareEventClosure:(()->Void)!

    var isExpanded = false
    var isReloaded = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func shareEventAction(_ sender: UIButton) {
        shareEventClosure()
    }
    
    @IBAction func addToCalenderAction(_ sender: UIButton) {
        addToCalenderClosure()
    }
    
    @IBAction func findOutMoreAction(_ sender: LocalizedButton) {
        findOutMoreClosure()
    }
    
    func commonInit() {
        self.selectionStyle = .none
        categoryButton.addRoundAllCorners(radious: 10, borderColor: UIColor.lichen, borderWidth: 1)
        addToCalenderButton?.Radius = (addToCalenderButton?.frame.height)!/2
        findOutMoreButton?.Radius = (findOutMoreButton?.frame.height)!/2
        conatinerView.addRoundCorners(radious: 17)
        self.backgroundColor = UIColor.clear
        
//        holdingView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//
//        //addSubview(holdingView)
//        print(self.frame)
//        print(holdingView.frame)
//        self.frame = holdingView.frame
//        print(holdingView.frame)
//        holdingView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //setConstraints(view: self, childView: holdingView)
    }
    
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.addToCalenderButton?.layer.cornerRadius = (self.addToCalenderButton?.frame.height)!/2
        self.findOutMoreButton?.layer.cornerRadius = (self.findOutMoreButton?.frame.height)!/2
    }
    
    func expand() {
        self.bottomConstraint.priority = .defaultLow
        self.stackTopConstraint.constant = -48
        self.collapsedView.isHidden = true
        self.expandedView.isHidden = false
    }
    
    func collapse() {
        self.bottomConstraint.priority = .defaultHigh
        
        self.stackTopConstraint.constant = -118
        self.collapsedView.isHidden = false
        self.expandedView.isHidden = true
    }
}


