//
//  GovernmentEntityModel.swift
//  TAMMApp
//
//  Created by kero1 on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class GovernmentEntityInformationModel : Mappable{
    
    class Address: Mappable{
        
        var place: String?
        var address: String?
        var lat: Double?
        var lng: Double?
        var map: String?
        var phone: String?
        var email: String?
        var website: String?
        
        required init?(map: Map) {
            
        }
        // Mappable
        func mapping(map: Map) {
            place <- map["place"]
            address <- map["address"]
            lat <- map["lat"]
            lng <- map["lng"]
            self.map <- map["map"]
            phone <- map["phone"]
            email <- map["email"]
            website <- map["website"]
        }
    }
    
    var id:String?
    var name:String?
    var icon:String?
    var description: String?
    var images: [String]?
    
    var officeHours: String?
    var publicServiceHours: String?

    var addresses: [Address]?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        icon <- map["icon"]
        
        description <- map["description"]
        images <- map["images"]
        officeHours <- map["officeHours"]
        publicServiceHours <- map["publicServiceHours"]
        addresses <- map["addresses"]
        
    }
}

