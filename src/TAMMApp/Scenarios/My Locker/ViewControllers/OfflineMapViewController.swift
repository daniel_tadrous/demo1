//
//  OfflineMapViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import RxSwift
import UIKit
import ArcGIS

class OfflineMapViewController: TammViewController ,  OfflineMapStoryboardLodable{

    @IBOutlet weak var mapView: AGSMapView!
    weak var parentVC:MapParentViewController!
    private var graphicsOverlay: AGSGraphicsOverlay!
    
    private var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showOfflineMap()
        // Do any additional setup after loading the view.
    }

    fileprivate func showOfflineMap() {
        prepareOfflineMap()
        addHandleTap()

    }
    
    @IBAction func testZoom(_ sender: UIButton) {
        let point =  AGSPoint(x: 6103742.467482, y: 2784621.088530, spatialReference: AGSSpatialReference(wkid: 3857))

        mapView.setViewpointCenter(point, completion: {[unowned self](loaded) in
            if loaded{
                let graphic = self.parentVC.graphicForPoint(point, attributes: [String:AnyObject]())
                self.graphicsOverlay.graphics.removeAllObjects()
                self.graphicsOverlay.graphics.add(graphic)
                
            }
        })
        
    }
    
    func goToSelectedLocation(){
        if self.graphicsOverlay != nil {
            self.graphicsOverlay.graphics.removeAllObjects()
        }
        self.graphicsOverlay = AGSGraphicsOverlay()
        self.mapView.graphicsOverlays.add(self.graphicsOverlay)
        
        
        if parentVC.selectedPlace != nil {
            
            let point = parentVC.selectedPlace?.mapPoint!
            
            mapView.setViewpointCenter(point!, scale: 1000, completion: {[unowned self](loaded) in
                if loaded{
                    let graphic = self.parentVC.graphicForPoint(point!, attributes: [String:AnyObject]())
                    self.graphicsOverlay.graphics.removeAllObjects()
                    self.graphicsOverlay.graphics.add(graphic)
                }
            })
           // showCalloutForOffline(graphic, tapLocation: point!, animated: true, offset: false)
        }else {
            mapView.setViewpointCenter(AGSPoint(x: 6103742.467482, y: 2784621.088530, spatialReference: AGSSpatialReference(wkid: 3857)), scale: 10, completion: nil)
        }
    }
    
    fileprivate func addHandleTap() {
        let handlelongTap = UILongPressGestureRecognizer(target: self, action: #selector(handleLongTap(sender:)))
        self.mapView.addGestureRecognizer(handlelongTap)
        
    }
    

    
    //self.mapView.callout.dismiss()
    @objc func handleLongTap(sender: UILongPressGestureRecognizer) {
        
        if sender.state == .ended {
            // handling code
            let point = sender.location(in: self.mapView)
            let myAGSPoint = self.mapView.screen(toLocation: point)
            print(myAGSPoint)
            let graphic = self.parentVC!.graphicForPoint(myAGSPoint, attributes: [String:AnyObject]())
            self.graphicsOverlay.graphics.removeAllObjects()
            self.graphicsOverlay.graphics.add(graphic)
            
            showCalloutForOffline(graphic,tapLocation:myAGSPoint, animated:true, offset:false)
            
        }
    }
    
    // open Offline Map
    fileprivate func prepareOfflineMap() {
        let path = fetchMapFile()
        if path != nil {

            let localTiledLayer = AGSArcGISVectorTiledLayer(vectorTileCache: AGSVectorTileCache(fileURL: path!))
            let map = AGSMap(basemap: AGSBasemap(baseLayer: localTiledLayer))
            self.mapView.map = map
            goToSelectedLocation()
        }
    }
    
    

    
    
    
    //get Map File path
    fileprivate func fetchMapFile() -> URL?{
        //let fileManager = FileManager.default
        let documentsURL = IncidentUrl.getDirectoryForIncident()
            //fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        
        
        let docs = try? FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
        for item in docs!  {
            
            if item.pathExtension.caseInsensitiveCompare("vtpk") == ComparisonResult.orderedSame {
                return item
            }
        }
        Toast(message:L10n.noMapAvailable).Show()
        return nil
    }
    
    private func showCalloutForOffline(_ graphic:AGSGraphic,tapLocation:AGSPoint, animated:Bool, offset:Bool) {
        //print(graphic.attributes)
        mapView.callout.dismiss()
        let customView = OurCalloutView.loadView()
        self.mapView.callout.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //customView.bounds = CGRect(x: 0, y: 0, width: 300, height: 300)
        self.mapView.callout.customView = customView
        customView.frame =  CGRect(origin: CGPoint.zero, size: CGSize(width: customView.frame.width, height: customView.frame.height))
        
//        let lat = tapLocation.toCLLocationCoordinate2D().latitude
//        let long = tapLocation.toCLLocationCoordinate2D().longitude
        
        
        let lat = tapLocation.x
        let long = tapLocation.y
        
        
        
        
        if isFirstTime &&  parentVC.selectedPlace != nil {
            //first address to show in offline map and I have an adress already from the place picker
            customView.title = L10n.address
            customView.details = parentVC.selectedPlace?.addressLocation!
            
        }else {
            
            
            parentVC.updateSelectedlocation(adress: L10n.fullOfflineAddress(point1: "\(long)", point2: "\(lat)"), mapPoint: tapLocation)

            customView.title = L10n.offlineAddress
            customView.details = L10n.fullOfflineAddress(point1: "\(long)", point2: "\(lat)")
        }
       
        //        customView.title =  graphic.attributes["PlaceName"] as? String ?? ""
        //        customView.details = graphic.attributes["LongLabel"] as? String ?? ""
        var location = LocationPickerLocation(locationAddress: graphic.attributes["LongLabel"] as? String ?? "", location: CLLocationCoordinate2D(latitude: lat, longitude: long), country: graphic.attributes["CountryCode"] as? String ?? "", region: graphic.attributes["Region"] as? String ?? "", subregion: graphic.attributes["Subregion"] as? String ?? "")
        
        customView.buttonClickedAction = {
            [weak self] in
            let viewpoint = AGSViewpoint(latitude: lat, longitude: long, scale: 10000)
            self?.mapView.setViewpoint(viewpoint, duration: 0, curve: AGSAnimationCurve.linear) { [weak self](finished) -> Void in
                if (!finished){
                    print("User interrupted Animation")
                }
                else{
                    self?.mapView.exportImage(completion: { (image, error) in
                        if let er = error{
                            print("nil image")
                        }
                        else{
                            location.image = image
                            self?.parentVC.delegate?.locationPickerDidSelectLocation((self?.parentVC!)!,  location)
                        }
                    })
                }
            }
            
        }
        
        customView.setDetailsTextColor(color:.black)
        customView.setButtonText(string: L10n.confirm)
        
        if !offset {
            self.mapView.callout.show(for: graphic, tapLocation: tapLocation, animated: animated)
        }
        else {
            self.mapView.callout.show(at: tapLocation, screenOffset: self.parentVC.magnifierOffset, rotateOffsetWithMap: false, animated: animated)
        }
      
        isFirstTime = false
    }
    
    
//    func checkForAddress() ->Bool {
//
//        if isFirstTime {
//            return true
//        }else {
//             return
//        }
//        if parentVC.selectedPlace != nil {
//            if (parentVC.selectedPlace?.addressLocation?.contains("OfflineAdress"))!{
//                return false
//            }
//        }
//
//        return true
//    }
//
   
}


extension OfflineMapViewController: AGSGeoViewTouchDelegate {
    
    fileprivate func displayCalloutIfGraphicIsTouched(_ screenPoint: CGPoint, _ mapPoint: AGSPoint) {
        self.mapView.callout.dismiss()
        
        self.mapView.identify(self.graphicsOverlay, screenPoint: screenPoint, tolerance: 12, returnPopupsOnly: false) { [weak self] (result: AGSIdentifyGraphicsOverlayResult)  in
            
            if let error = result.error{
                //self?.showAlert(error.localizedDescription)
            } else if result.graphics.count > 0{
                let graphic = result.graphics.first!
                    self?.showCalloutForOffline(graphic, tapLocation: mapPoint, animated: true, offset: false)
              
                
            }
        }
    }

    
    func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        
        displayCalloutIfGraphicIsTouched(screenPoint, mapPoint)
    }
    
    

    func geoView(_ geoView: AGSGeoView, didEndLongPressAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        
        //the callout right now will be at an offset
        //update the callout to show on top of the graphic
        displayCalloutIfGraphicIsTouched(screenPoint, mapPoint)
        
    }
}
