//
//  FeedBackThankYouViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
//Cocreate Feedback
class FeedBackThankYouViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: LocalizedLabel!

    @IBOutlet weak var summaryLabel: LocalizedLabel!
    
    @IBOutlet weak var refrenceTelLAbel: LocalizedLabel!
    
    @IBOutlet weak var detailsLabel: LocalizedLabel!
    
    @IBOutlet weak var okBtn: LocalizedButton!
    
    @IBOutlet weak var mainView: RoundedView!

    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var closeIcon: UIView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBAction func okAction(_ sender: Any) {
        dismiss(animated: true, completion: {
            
            self.appDelegate.addfloatingMenu()
        })
    }
    
    var titleStr:String?
    var summary:String?
    var details:String?
    var refrebcenumber:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeIconClicked))
        closeIcon.addGestureRecognizer(tap)
        
        self.appDelegate.removeFloatingMenu()
        // Do any additional setup after loading the view.
    }

    @objc func closeIconClicked() {
       dismiss(animated: true, completion: {
        
        self.appDelegate.addfloatingMenu()
       })
    }

    
//    func initialize(okClickHandler: @escaping ()->Void) {
//        self.okClickHandler = okClickHandler
//        commonIntializer()
//    }
    

    
    func initialize(title:String ,summary:String,details:String,refernceNumber:String?) {
        
        self.titleStr = title
        self.summary = summary
        //self.details = details
        self.refrebcenumber = refernceNumber
        commonIntializer()
    }
    
    private func commonIntializer(){
        self.modalPresentationStyle = .overFullScreen
    }
    
    private func commonInit(){
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
        setupUiData()
        
    }
    
    
    
    func setupUiData(){
        
        if self.titleStr != nil{
            self.titleLabel.text = self.titleStr
        }
        if self.summary != nil{
            self.summaryLabel.text = self.summary
        }
        
        if self.details != nil{
            self.detailsLabel.text = self.details
        }
        if self.refrebcenumber != nil{
            self.refrenceTelLAbel.text = self.refrebcenumber
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
    }
    

}
