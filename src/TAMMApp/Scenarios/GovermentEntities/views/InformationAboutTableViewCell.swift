//
//  InformationAboutTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 7/9/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import FSPagerView

class InformationAboutTableViewCell: UITableViewCell {

    
    static let namedNibName = "InformationAboutTableViewCell"
    
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet{
            pageControl.setImage(#imageLiteral(resourceName: "dot_intro") , for: .normal)
            pageControl.setImage(UIImage(color: #colorLiteral(red: 0.03529411765, green: 0.3607843137, blue: 0.4549019608, alpha: 1), size: CGSize(width: 3, height: 3)), for: .selected)
        }
    }
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
//            let nibName = "InformationAboutTableViewCell"
            let nib = UINib(nibName: InformationAboutPagerViewCell.namedNibName, bundle: nil)
            
            self.pagerView.register(nib, forCellWithReuseIdentifier: InformationAboutPagerViewCell.namedNibName )
            pagerView.dataSource = self
            pagerView.delegate = self
            pagerView.transformer = FSPagerViewTransformer(type: .zoomOut)
            
        }
    }
    
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    var images: [URL] = []
    
    @IBOutlet weak var officeHoursLabel: UILabel!
    @IBOutlet weak var officeHoursDaysLabel: UILabel!
    @IBOutlet weak var officeHoursTimesLabel: UILabel!
  
    @IBOutlet weak var serviceHoursLabel: UILabel!
    @IBOutlet weak var serviceHoursDaysLabel: UILabel!
    @IBOutlet weak var serviceHoursTimesLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    var logoUrl: URL!{
        didSet{
            ImageCachingUtil.loadImage(url: logoUrl, dispatchInMain: true
                , callBack: { [weak self] (url, image) in
                    self?.logoImage.image = image
            })
        }
    }
    
    
    public var header: String?{
        didSet{
            titleLabel.text = header
        }
    }

    public var details: String?{
        didSet{
            detailsLabel.text = details
        }
    }


    public func setOfficeHours(title: String?, days: String?, hours: String? ){
            self.officeHoursLabel.text = title
            self.officeHoursDaysLabel.text = days
            self.officeHoursTimesLabel.text = hours
    }
    
    
    public func setPublicServiceHours(title: String?, days: String?, hours: String? ){
        self.serviceHoursLabel.text = title
        self.serviceHoursDaysLabel.text = days
        self.serviceHoursTimesLabel.text = hours
    }

    
    
    
}


extension InformationAboutTableViewCell: FSPagerViewDataSource{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        pageControl.numberOfPages = images.count
        return images.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: InformationAboutPagerViewCell.namedNibName, at: index) as! InformationAboutPagerViewCell
        
        let item = images[index]
        cell.viewIdentfier = item
        
        ImageCachingUtil.loadImage(url: item, dispatchInMain: true
            , callBack: { (url, image) in
                guard let item = item as? URL else {return}
                if item == url{
                    cell.imageView1.image = image
                }
        })
        pageControl.currentPage = index
        return cell
    }
}
extension InformationAboutTableViewCell: FSPagerViewDelegate{
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool{
        return false
    }
    
}
