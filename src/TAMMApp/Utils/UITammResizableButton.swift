//
//  UITammResizableButton.swift
//  TAMMApp
//
//  Created by kerolos on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
class UITammResizableButton: UITammButton {
    @IBInspectable public var referenceHeigh: CGFloat = 40{
        didSet{
            heightConstraint?.constant = referenceHeigh
        }
    }
    @IBInspectable public var referenceWidth: CGFloat = 120{
        didSet{
            widthConstraint?.constant = referenceWidth
        }
    }
    @IBInspectable public var ignoreWidthConstrints: Bool = false
    
    @IBInspectable public var applyRoundCorners: Bool = false
    @IBInspectable public var cornerRadiusToHeightRatio: CGFloat = 0.5
    
    
    @IBInspectable public var maxScaleFactor: CGFloat = CGFloat.infinity
    
    private var widthConstraint: NSLayoutConstraint?
    private var heightConstraint: NSLayoutConstraint?
    
    var scaleFactor: CGFloat{
        return min( maxScaleFactor, UIFont.fontSizeMultiplier)
    }
    
    var currentHeight: CGFloat{
        return heightConstraint!.constant
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func applyCornersIfActive() {
        if(applyRoundCorners){
            roundCorners(radius: cornerRadiusToHeightRatio * (heightConstraint?.constant)!)
        }
    }
    
    func applyDimensions() {
        widthConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: referenceWidth * scaleFactor )
        
        heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: referenceHeigh * scaleFactor )
        if(!ignoreWidthConstrints){
            self.addConstraints([widthConstraint!])
        }
        self.addConstraints([heightConstraint!])
        
        applyCornersIfActive()
    }
    
    override func awakeFromNib() {
        applyDimensions()
    }
    
    private func commonInit(){
        // add constraints for the height and the width
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        heightConstraint?.constant = referenceHeigh * scaleFactor
        widthConstraint?.constant = referenceWidth * scaleFactor
        applyCornersIfActive()
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
}
