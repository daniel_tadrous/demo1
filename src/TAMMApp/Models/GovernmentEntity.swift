//
//  GovernmentEntity.swift
//  TAMMApp
//
//  Created by Daniel on 7/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class GovernmentEntity : Mappable{
    var id:String?
    var name:String?
    var icon:String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        icon <- map["icon"]
    }
}
