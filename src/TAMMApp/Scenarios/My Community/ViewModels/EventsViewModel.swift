//
//  EventsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
import EventKit

protocol FilterEventsViewModelProtocol {
    func setLocationQueryValue(q:String?)
    func setIsToGetCategories()
    func startLocationService()
    func getCategoriesVar()->Variable<[Event.Category]?>
    func getLocationsVar()->Variable<[String]?>
    func getCurrentLocation()->Variable<CLLocation?>
}
class EventsViewModel: ShareViewModel{
     let speechDuration: Double = 10.0
    private var speechDoneSubject: PublishSubject<String?>?
    private var speechRecognitionService: SpeechRecognitionServiceType!
    private var locationService: LocationServiceType!
    func startSpeechService() -> Observable<String?>{
        speechRecognitionService.start(delegate: self, duration: speechDuration)
        speechDoneSubject = PublishSubject<String?>()
        //        micRecognizedVar.value = ""
        return (speechDoneSubject!)
    }
    
   
    
    private(set) var currentQueryVar : Variable<String?> = Variable(nil)
    private(set) var isToGetTrending : Variable<Bool?> = Variable(nil)
    //    # inistializing with empty string arrays
    private(set) var trendingVar : Variable<[String]?> = Variable(nil)
    private(set) var queryResultVar : Variable<[String]?> = Variable(nil)
   
    private func getQueryResults(q:String){
        apiService.getSearchEvents(q: q).subscribe(onNext:{
            [unowned self] popArr in
            self.queryResultVar.value = popArr
        }).disposed(by: disposeBag)
    }
    
    private func getTrending(){
        apiService.getTrendingEvents().subscribe(onNext:{
            [unowned self] trendArr in
            self.trendingVar.value = trendArr
        }).disposed(by: disposeBag)
    }
    
    
    
    
    
    private(set) var viewData: Variable<[Event]?> = Variable(nil)
    private(set) var categoriesViewData: Variable<[Event.Category]?> = Variable(nil)
    private(set) var locationsViewData: Variable<[String]?> = Variable(nil)
    private(set) var currentLocationViewData: Variable<CLLocation?> = Variable(nil)
    private(set) var eventsFilterApplyModel: Variable<EventsFilterApplyModel?> = Variable(nil)
    private(set) var isToGetCategories: Variable<Bool?> = Variable(nil)
    private(set) var locationsQuery: Variable<String?> = Variable(nil)
    //private(set) var isToGetCurrentLocation: Variable<Bool?> = Variable(nil)
    private var apiService: ApiMyCommunityTypes!
    fileprivate var disposeBag = DisposeBag()
    init(apiService: ApiMyCommunityTypes,speechRecognitionService: SpeechRecognitionServiceType,locationService: LocationServiceType) {
        super.init()
        self.apiService = apiService
        self.locationService = locationService
        
        self.setupBinding()
        self.speechRecognitionService = speechRecognitionService
    }
    
    fileprivate func setupBinding(){
        eventsFilterApplyModel.asObservable()
            .subscribe(onNext:{ [unowned self] events in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        isToGetCategories.asObservable().subscribe(onNext:{ [unowned self] cats in
            self.getCategories()
        }).disposed(by:disposeBag)
        
        locationsQuery.asObservable().subscribe(onNext:{ [unowned self] locations in
            self.getLocations()
        }).disposed(by:disposeBag)

        
        currentQueryVar.asObservable().subscribe(onNext: { (q:String?) in
            if q != nil{
                self.getQueryResults(q: q!)
            }
        }).disposed(by: disposeBag)
        isToGetTrending.asObservable().subscribe(onNext: { (go:Bool?) in
            if go != nil{
                self.getTrending()
            }
        }).disposed(by: disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if eventsFilterApplyModel.value != nil{
            apiService.getEvents(eventsFilterApplyModel: eventsFilterApplyModel.value!)
                .subscribe(onNext: { [unowned self] events in
                    self.viewData.value = events
                    }
                ).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
    fileprivate func getCategories(){
        if isToGetCategories.value != nil && isToGetCategories.value!{
            apiService.getFilterCategories()
                .subscribe(onNext: { [unowned self] cats in
                    self.categoriesViewData.value = cats
                }).disposed(by: disposeBag)
        }else{
            self.categoriesViewData.value  = nil
        }
    }
    fileprivate func getLocations(){
        if  (locationsQuery.value != nil) && !(locationsQuery.value?.isEmpty)!
        {
            apiService.getFilterLocations(q: locationsQuery.value!)
                .subscribe(onNext: { [unowned self] locations in
                    self.locationsViewData.value = locations
                }).disposed(by: disposeBag)
        }else{
            self.locationsViewData.value  = nil
        }
    }
    
}

extension EventsViewModel{
    func addToCalendar(event:Event){
        let eventStore : EKEventStore = EKEventStore()
        
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        eventStore.requestAccess(to: .event) { (granted, error) in
            
            if (granted) && (error == nil) {
                let userConfig = UserConfigService()
                if userConfig.getCalanderSyncStatus(){
                    let eventC:EKEvent = EKEvent(eventStore: eventStore)
                    eventC.title = event.title
                    eventC.startDate = event.start?.getDate(format: inputFormat)
                    eventC.endDate = event.end?.getDate(format: inputFormat)
                    eventC.notes = event.description
                    eventC.calendar = eventStore.defaultCalendarForNewEvents
                    do {
                        try eventStore.save(eventC, span: .thisEvent)
                    } catch let error as NSError {
                        print("failed to save event with error : \(error)")
                    }
                    print("Saved Event")
                    DispatchQueue.main.async {
                        Toast(message: L10n.MyCommunity.Events.eventAddedSuccessfully).Show()
                    }
                }else{
                    DispatchQueue.main.async {
                        Toast(message: L10n.MyCommunity.Events.eventAddedSuccessfully).Show()
                    }
                }
                
            }else{
                DispatchQueue.main.async {
                    let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.MyCommunity.Events.permessionDenied, onComplete: {
                        self.addToCalendar(event: event)
                    })
                    UIApplication.shared.keyWindow!.addSubview(permessionView)
                    UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)

                }
            }
            
        }
    }
}
extension EventsViewModel: SpeechRecognitionServiceTypeDelegate{
    func recognizerAvailable(isAvailable: Bool) {
        // indecates that the recognizer is available and most likely will start
        // should inform the subscribers with the change
        // disable the recording for some time
        if( isAvailable){
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onNext("")
            
        }
        else{
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onError("Service is not currently available" as! Error)
        }
        
    }
    
    func textRecived(text: String) {
        //        micRecognizedVar.value = text
        speechDoneSubject?.onNext(text)
    }
    
    func recognitionIsDone(_ error: Error?) {
      
        speechDoneSubject?.onCompleted()
    }
}

extension EventsViewModel: LocationServiceTypeDelegate{
    
    func onLocationUpdated(location: CLLocation) {
        self.currentLocationViewData.value = location
    }
}
extension EventsViewModel:FilterEventsViewModelProtocol{
    func setIsToGetCategories() {
        self.isToGetCategories.value = true
    }
    
   
    func setLocationQueryValue(q: String?) {
        self.locationsQuery.value = q
    }
    
    func getCategoriesVar() -> Variable<[Event.Category]?> {
        return self.categoriesViewData
    }
    
    func getLocationsVar() -> Variable<[String]?> {
        return self.locationsViewData
    }
    func startLocationService(){
        self.locationService.start(delegate: self)
    }
    func getCurrentLocation() -> Variable<CLLocation?> {
        return self.currentLocationViewData
    }
    
    
}
