//
//  EnablePushNotificationViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/7/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class EnablePushNotificationViewModel {
    
    var ScreenOpened : Variable<Int?> = Variable(nil)
    var apiResponse : Variable<EnablePushNotificationModel?> = Variable(nil)
    let disposeBag = DisposeBag()
    var currentFont:UIFont?
    var currentBoldFont:UIFont?
    
    let regularFont = UIFont(name: "Roboto-Regular", size: UIFont.fontSizeMultiplier * 14.0)!
    let boldFont = UIFont(name: "Roboto-Bold", size: UIFont.fontSizeMultiplier * 16.0)!
    
    let regularArabicFont = UIFont(name: "Swissra-Normal", size: UIFont.fontSizeMultiplier * 14.0)!
    let regularArabicBoldFont = UIFont(name: "Swissra-Bold", size: UIFont.fontSizeMultiplier * 16.0)!
    
    let str1 = L10n.Preferences.note
    let str2 = L10n.Preferences.stillReceiveCrucialNotif
    
    var apiService:ApiEnablePushNotificationServiceType
    
    
    init(apiService: ApiEnablePushNotificationServiceType) {
        self.apiService = apiService
        setUpBinding()
    }
    
    func setUpBinding() {
        ScreenOpened.asObservable().subscribe( { _ in
            self.getNotificationStatus()
        }).disposed(by: disposeBag)
    }
    
   func getNotificationStatus() {
    apiService.getEnablePushNotificationState().subscribe(onNext:{ [unowned self] respone in
        self.apiResponse.value = respone
        print(respone)
    } ).disposed(by: disposeBag)
    }
    
    
    func setCurrentFont() {
        if L10n.Lang == "en" {
            currentFont = regularFont
            currentBoldFont = boldFont
        }else {
            currentFont = regularArabicFont
            currentBoldFont = regularArabicBoldFont
        }
    }
    
    func prepareTextInNoteLabel()->NSMutableAttributedString {
        setCurrentFont()
        let noteText = str1 + str2
        let noteattributedString = NSMutableAttributedString(string: noteText, attributes: [
            .font:  currentFont!])
        noteattributedString.addAttribute(.font, value: currentBoldFont!, range: NSRange(location: 0, length: str1.count))
        noteattributedString.addAttribute(.foregroundColor, value: UIColor.darkBlueGrey.getAdjustedColor(), range: NSRange(location: str1.count, length: str2.count))
        noteattributedString.addAttribute(.foregroundColor, value: UIColor.petrol.getAdjustedColor(), range: NSRange(location: 0, length: str1.count))
        
        return noteattributedString
    }
    
    
}
