//
//  EmptyResponse.swift
//  TAMMApp
//
//  Created by Daniel on 7/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class EmptyResponse : Mappable{
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
    }
}
