//
//  FaqFilterView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift

class FaqFilterView : UIView{
    var categories: [PickerItem] = [PickerItem(id: 0,displayName: L10n.Individual), PickerItem(id: 1,displayName: L10n.Business), PickerItem(id: 2,displayName: L10n.Tourist)]
    
    var aols: [PickerItem] = [PickerItem]()
    let info: [Int:Int] = [
        1 : 1,
        2 : 2,
        3 : 4,
        4 : 8,
        5 : 16 ]
    private var viewModel:FAQViewModel!
    var faqFilter = FaqFilterModel()
    var selectedCategoryId = -1
    var selectedAolId = -1
    
    @IBOutlet weak var applyFiltersBtn: LocalizedButton!
    @IBOutlet weak var categoryArrowLabel: UILabel!
    @IBOutlet weak var aolArrowLabel: UILabel!
    
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var aolTextField: UITextField!
    
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var aolsView: UIView!
    
    @IBOutlet weak var clearCategoryBtn: LocalizedButton!
    @IBOutlet weak var clearAolBtn: LocalizedButton!
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var clearAllBtn: LocalizedButton!
    
    
    
    var onApplyFilterClicked:((FaqFilterModel)->Void)?
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
  
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    init(frame: CGRect,viewModel: FAQViewModel,onApplyFilterClicked:@escaping (FaqFilterModel)->Void){
        super.init(frame: frame)
        self.viewModel = viewModel
        self.onApplyFilterClicked = onApplyFilterClicked
        commonInit()
        setupHooks()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "FaqFilterView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        setupHooks()
        setupUiData()
    }
   
    
    @objc func cancelBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
        viewModel.isToGetAspectOfLifeNames.value = true
        viewModel!.viewDataAspectOfLife.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] aspectsOfLifes in
                if(aspectsOfLifes == nil){
                    //TODO: should do somting ????
                }else{
                    for aspectOfLife in self.viewModel.viewDataAspectOfLife.value!{
                        self.aols.append(PickerItem(id: aspectOfLife.id!,displayName: aspectOfLife.title!))
                    }
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    @IBOutlet weak var l1: LocalizedLabel!
    
    @IBOutlet weak var l2: LocalizedLabel!
    
    @IBOutlet weak var l3: LocalizedLabel!
    
    @IBOutlet weak var l4: LocalizedLabel!
    
    @IBOutlet weak var l5: LocalizedLabel!
    
    @IBAction func clearAllBtnClickListner(_ sender: LocalizedButton) {
        for i in 1...5{
            let checkBox = self.viewWithTag(i) as! UICheckbox
           checkBox.isSelected = false
        }
        sender.isHidden = true
    }
    
    @IBAction func checkBoxChecked(_ sender: UICheckbox) {
        self.showHideClearAllBtn()
//        print(self.l1.customFontSize)
        
        let fontSize  = CGFloat(16)
        var enFont = L10n.Lang == "en" ? UIFont(name: "Roboto-Bold", size: fontSize) : UIFont(name: "Swissra-Bold", size: fontSize)
        var arFont = "Swissra-Bold"
        if !sender.isSelected{
            enFont = L10n.Lang == "en" ? UIFont(name: "Roboto-Regular", size: fontSize) : UIFont(name: "Swissra-Normal", size: fontSize)
            arFont = "Swissra-Normal"
        }
        
        switch sender.tag {
        case 1:
            self.l1.font = enFont
            self.l1.originalFont = enFont
            self.l1.RTLFont = arFont
        case 2:
            self.l2.font = enFont
            self.l2.originalFont = enFont
            self.l2.RTLFont = arFont
        case 3:
            self.l3.font = enFont
            self.l3.originalFont = enFont
            self.l3.RTLFont = arFont
        case 4:
            self.l4.font = enFont
            self.l4.originalFont = enFont
            self.l4.RTLFont = arFont
        case 5:
            self.l5.font = enFont
            self.l5.originalFont = enFont
            self.l5.RTLFont = arFont
        default:
            print("not found")
        }
        
    }
    func showHideClearAllBtn(){
        self.clearAllBtn.isHidden = true
        for i in 1...5{
           let checkBox = self.viewWithTag(i) as! UICheckbox
            if checkBox.isSelected{
                self.clearAllBtn.isHidden = false
                return
            }
        }
    }
    func setupUiData(){
        
        //categories drop down
        self.categoryArrowLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.categoriesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCategoryDropDownClick)))
        self.categoryTextFieldDidChange(self.categoryTextField)
        self.categoriesView.addRoundAllCorners(radious: 5)
        self.categoriesView.layer.borderWidth = 1
        self.categoriesView.layer.borderColor = UIColor.cloudyBlue.cgColor
        
         self.categoryTextField.attributedPlaceholder =  NSAttributedString(string: L10n.select_category,                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkBlueGrey])
        
        //aspects of life drop down
        self.aolArrowLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.aolsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onAolDropDownClick)))
        self.AolTextFieldDidChange(self.aolTextField)
        self.aolsView.addRoundAllCorners(radious: 5)
        self.aolsView.layer.borderWidth = 1
        self.aolsView.layer.borderColor = UIColor.cloudyBlue.cgColor
        self.aolTextField.attributedPlaceholder =  NSAttributedString(string: L10n.select_aspect_of_life,                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkBlueGrey])
        //check boxes
        for i in 1...5{
            let checkBox = self.viewWithTag(i) as! UICheckbox
            //self.checkBoxChecked(checkBox)
            checkBox.onSelectStateChanged = { (checkb , isSelected) in
                self.checkBoxChecked(checkb)
            }

        }
        //apply btn
        self.applyFiltersBtn.addRoundCorners(radious: 23)
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
    }
    
    
    @IBAction func ApplyFilterClickHandler(_ sender: LocalizedButton) {
        for i in 1...5{
            let checkBox = self.viewWithTag(i) as! UICheckbox
            if checkBox.isSelected{
                self.faqFilter.info.append(self.info[i]!)
            }
        }
        self.onApplyFilterClicked?(self.faqFilter)
        self.cancelBtnClicked()
    }
    // Mark:  categories
    @objc func onCategoryDropDownClick(){
        self.openCategoryPickerView()
    }
    
    @IBAction func categoryChangedListner(_ sender: UITextField) {
        categoryTextFieldDidChange(sender)
    }
    func categoryTextFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            self.clearCategoryBtn.isHidden = true
        }else{
            self.clearCategoryBtn.isHidden = false
        }
    }
   
    private func openCategoryPickerView(){
        let picker = PickerView()
        picker.items = self.categories
        var selectedIndex = -1
        for i in 0..<self.categories.count {
            if self.categories[i].id as! Int == selectedCategoryId {
                selectedIndex = i
                break
            }
        }
        if selectedIndex != -1{
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        self.categoryTextField.inputView = picker
        picker.tag = 1
        picker.viewDelegate = self
        self.categoryTextField.becomeFirstResponder()
    }
    @IBAction func clearBtnClickHandler(_ sender: LocalizedButton) {
        self.categoryTextField.text = ""
        sender.isHidden = true
        self.faqFilter.cat = -1
    }
    
    // Mark: Aspect of life
    
    @objc func onAolDropDownClick(){
        self.openAolPickerView()
    }
    @IBAction func AolChangedListner(_ sender: UITextField) {
        AolTextFieldDidChange(sender)
    }
    func AolTextFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)!{
            self.clearAolBtn.isHidden = true
        }else{
            self.clearAolBtn.isHidden = false
        }
    }
    
    private func openAolPickerView(){
        let picker = PickerView()
        picker.items = self.aols
        var selectedIndex = -1
        for i in 0..<self.aols.count {
            if self.aols[i].id as! Int == selectedAolId {
                selectedIndex = i
                break
            }
        }
        if selectedIndex != -1{
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        self.aolTextField.inputView = picker
        picker.tag = 2
        picker.viewDelegate = self
        self.aolTextField.becomeFirstResponder()
    }
    @IBAction func clearAolBtnClickHandler(_ sender: LocalizedButton) {
        self.aolTextField.text = ""
        sender.isHidden = true
        self.faqFilter.aspect = -1
    }
    
    
    // Mark check boxes section
    
    
    
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.scrollView.resizeToFitContent()
    }
    //drop down similar to android
    //let dropDown = DropDown()
    //   func setupHooks(){
    //        self.categoryArrowLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
    //        self.dropDown.anchorView = self.categoriesView
    //
    //        self.dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
    //        self.categoriesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onDropDownClick)))
    //
    //        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
    //           self.categoryLabel.text = item
    //        }
    //    }
    
    //    @objc func onDropDownClick(){
    //        self.dropDown.show()
    //    }
    
}
extension FaqFilterView: PickerViewDelegate{
    func itemSelected(item: PickerItemType, pickerView: PickerView) {
        if pickerView.tag == 1{
            self.categoryTextField.text = item.displayName
            self.faqFilter.cat = item.id as! Int
            self.selectedCategoryId = item.id as! Int
        }else{
            self.aolTextField.text = item.displayName
            self.faqFilter.aspect = item.id as! Int
            self.selectedAolId = item.id as! Int
        }
    }
    
    func clickOutsidePicker() {
        self.categoryTextField.resignFirstResponder()
    }
    
    
}
