//
//  SavePHAsset.swift
//  TAMMApp
//
//  Created by kerolos on 6/26/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Photos


class SavePHAsset{
    
    class func saveAudioPHAsset(_ url: URL, _ completion: @escaping ( (_ asset: PHAsset?) -> ()) ){
        
        var localId: String = ""
        
        PHPhotoLibrary.shared().performChanges( {
            let creationRequest = PHAssetCreationRequest.forAsset()
            creationRequest.addResource(with: .video, fileURL: url, options:nil)
            localId = (creationRequest.placeholderForCreatedAsset?.localIdentifier)!
        },completionHandler: { success, error in
            DispatchQueue.main.async {
                let assetResult = PHAsset.fetchAssets(withLocalIdentifiers: [localId], options: nil)
                let asset = assetResult.firstObject
                
                completion(asset)
            }
        })
        
    }
    
}
