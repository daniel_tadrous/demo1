//
//  Test2ViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 4/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation


protocol Test2ViewModelDelegate{
    func test2IsDone()
}
class Test2ViewModel{
    weak var delegate: TestViewModelDelegate?

}
