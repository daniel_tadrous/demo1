//
//  ThankYouViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {

    @IBOutlet weak var closeIcon: UIView!
    
    @IBOutlet weak var titleLable: LocalizedLabel!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    @IBOutlet weak var okBtn: LocalizedButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBAction func okBtn(_ sender: Any) {
         dismiss(animated: true, completion: {
            self.appDelegate.addfloatingMenu()
         })
    }

    var titleStr:String?
    var BtnTitleStr:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appDelegate.removeFloatingMenu()
        commonInit()
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeIconClicked))
        closeIcon.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func closeIconClicked() {
        dismiss(animated: true, completion: {
        self.appDelegate.addfloatingMenu()
        })
    }
    
    
    //    func initialize(okClickHandler: @escaping ()->Void) {
    //        self.okClickHandler = okClickHandler
    //        commonIntializer()
    //    }
    
    
    
    func initialize(title:String ,btnTitle:String) {
        
        self.titleStr = title
        self.BtnTitleStr = btnTitle
        commonIntializer()
    }
    
    private func commonIntializer(){
        self.modalPresentationStyle = .overFullScreen
    }
    
    private func commonInit(){
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
        setupUiData()
        
    }
    
    
    
    func setupUiData(){
        
        if self.titleStr != nil{
            self.titleLable.text = self.titleStr
        }
        if self.BtnTitleStr != nil{
            self.okBtn.setTitle(BtnTitleStr, for: .normal)
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
    }
    


}
