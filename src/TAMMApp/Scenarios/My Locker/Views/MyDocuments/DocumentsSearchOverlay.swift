//
//  DocumentsSearchOverlay.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/26/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DocumentsSearchOverlay: UIView,UITextFieldDelegate {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var suggestTableView: UITableView!
    @IBOutlet weak var dateErrorLabel: UILabel!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var fromField: LocalizedTextField!
    @IBOutlet weak var toField: LocalizedTextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var didYouMeanLabel: LocalizedLabel!
    
    var documents:[MyDocumentModel]{
        get{
            return viewModel.searchedDocuments.value?.documents ?? []
        }
    }
    
    var searchingClosure:((_:DocumentsModel,_:String) -> Void)!
    
    var dateOk = true
    var fieldOk = true
    
    var viewModel:MyDocumentsViewModel!
    var count = 0
    let disposeBag = DisposeBag()
    var documentType:DocumentType!
    
    let cellIdentifier = "DocumentSearchCell"
    var dateFormat = "MMM yyyy"
    
    var toDate = Date()
    var fromDate = Date()
    
    init(frame: CGRect,model:MyDocumentsViewModel,type:DocumentType) {
        super.init(frame: frame)
        viewModel = model
        documentType = type
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        Bundle.main.loadNibNamed("DocumentsSearchOverlay", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        suggestTableView.delegate = self
        suggestTableView.dataSource = self
        searchField.becomeFirstResponder()
        
        suggestTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        switch documentType {
        case .Active:
            datePickerView.isHidden = true
        default:
            break
        }
        if L10n.Lang == "ar"{
            backButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        viewModel.searchedDocuments.value = nil
        //from date
        let dateStr = Date().getFormattedString(format: self.dateFormat)
        self.fromField.text = dateStr
        self.fromField.inputView = getDatePicker(textField: self.fromField)
        //to date
        self.toField.text = dateStr
        self.toField.inputView =  getDatePicker(textField: self.toField)
        self.didYouMeanLabel.text = ""
        self.searchButton.isEnabled = true
        self.searchButton.alpha = 1.0
        self.searchButton.backgroundColor = UIColor.sunflowerYellow
        searchField.delegate = self
        setupBinding()
        
    }
    
    @IBAction func searchFieldEndEditing(_ sender: UITextField) {
        if self.documentType == .Active{
            let text = sender.text
            var callObject:DoumentsApiCallModel = DoumentsApiCallModel()
            callObject.q = text!
            self.viewModel.searchActiveDocuments(callObject: callObject)
        }
    }
    
    func setupBinding(){
//        searchField.rx.text.asObservable().subscribe(onNext: { (text) in
//            if self.documentType == .Active{
//                if text!.count >= 3{
//                    self.textfieldChanged(text: text!)
//                    var callObject:DoumentsApiCallModel = DoumentsApiCallModel()
//                    self.viewModel.searchActiveDocuments(callObject: callObject)
//                }
//                else{
//                }
//            }else{
//                if text!.count >= 3 {
//                    self.fieldOk = true
//                    if self.dateOk{
//                        self.searchButton.isEnabled = true
//                        self.searchButton.alpha = 1.0
//                        self.searchButton.backgroundColor = UIColor.sunflowerYellow
//                    }
//                    text
//                }else{
//                    self.fieldOk = false
//                    self.searchButton.isEnabled = false
//                    self.searchButton.alpha = 0.1
//                    self.searchButton.backgroundColor = UIColor.paleGreyTwo
//                }
//            }
//        }).disposed(by: disposeBag)
        viewModel.searchedDocuments.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] docRes in
                if(docRes == nil || docRes?.documents.count == 0){
                    
                    self.didYouMeanLabel.text = ""
                }else{
                    self.removeFromSuperview()
                    self.searchingClosure(docRes!,self.searchField.text!)
                }
            })
            .disposed(by: disposeBag)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.documentType == .Active{
            let text = textField.text
            var callObject:DoumentsApiCallModel = DoumentsApiCallModel()
            callObject.q = text!
            self.viewModel.searchActiveDocuments(callObject: callObject)
        }
        return true
    }
    
    func textfieldChanged(text:String){
        searchField.text = text
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        removeFromSuperview()
    }
    
    @IBAction func clearAction(_ sender: UIButton) {
        textfieldChanged(text: "")
        self.searchButton.isEnabled = false
        self.searchButton.alpha = 0.1
        self.searchButton.backgroundColor = UIColor.paleGreyTwo
    }
    
    
    @IBAction func micAction(_ sender: UIButton) {
        
        if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: viewModel.speechDuration)
            //currentSearchModel.startSpeechService()
            
            self.viewModel.startSpeechService().asObservable().subscribe(onNext: { (str:String?) in
                self.searchField.text = str
                self.textfieldChanged(text: str!)
            }).disposed(by: disposeBag)
        }
        
    }
    @IBAction func searchAction(_ sender: UIButton) {
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        var callObject:DoumentsApiCallModel = DoumentsApiCallModel()
        callObject.from = fromDate.getFormattedString(format: inputFormat)
        callObject.to = toDate.getFormattedString(format: inputFormat)
        callObject.q = searchField.text!
        viewModel.searchExpiredDocuments(callObject: callObject)
    }
    
    func displayBlockingToast(message:String, duration: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            alert.show()
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true, completion: nil)
            let txt = self.searchField.text
            if txt != nil && txt != ""{
                self.searchField.becomeFirstResponder()
                self.searchField.text = txt
            }
        }
    }
    
    func getDatePicker(textField: UITextField)->TammMonthYearPickerView{
        var allowPrev = false
        if documentType == .Expired{
            allowPrev = true
        }
        let monthYearPickerView = TammMonthYearPickerView(allowPrevious: allowPrev)
        monthYearPickerView.onDateSelectedDate = { (date: Date) in
            textField.text = date.getFormattedString(format: self.dateFormat)
            if textField.tag == 1{
                self.fromDate = date
            }else{
                self.toDate = date
            }
            let interval = self.toDate.timeIntervalSince(self.fromDate)
            let duration = interval/3600/24/30
            let components = Calendar.current.dateComponents([.month,.day,.year], from: self.fromDate, to: self.toDate)
            if (interval >= 0 && duration < 6) {
                self.dateOk = true
                if self.fieldOk{
                    self.dateErrorLabel.isHidden = true
                    self.searchButton.isEnabled = true
                    self.searchButton.alpha = 1.0
                    self.searchButton.backgroundColor = UIColor.sunflowerYellow
                }
            }else{
                self.dateOk = false
                self.dateErrorLabel.isHidden = false
                if duration > 6 {
                    self.dateErrorLabel.text = "Range must be less than 6 months"
                }else if interval < 0{
                    self.dateErrorLabel.text = L10n.MyDocsStrings.fromDateBeforeTo
                }
                self.searchButton.isEnabled = false
                self.searchButton.alpha = 0.1
                
                self.searchButton.backgroundColor = UIColor.paleGreyTwo
            }
        }
        return monthYearPickerView
    }
}
extension DocumentsSearchOverlay: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! DocumentSearchCell
        let document = documents[indexPath.row]
        cell.detailLabel.text = document.type
        cell.titleLabel.text = document.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let id = documents[indexPath.row].Id{
            let detailedOverLay = DocumentsDetails(frame: UIScreen.main.bounds,model:viewModel,type:documentType,id:id)
            UIApplication.shared.keyWindow!.addSubview(detailedOverLay)
            UIApplication.shared.keyWindow!.bringSubview(toFront: detailedOverLay)
            viewModel.getDocument(id: id)
        }
        
    }
    
}

