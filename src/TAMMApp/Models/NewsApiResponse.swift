//
//  NewsApiResponse.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsApiResponse : Mappable{
    
    var pageCount:Int?
    var news:[NewsModel]?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        pageCount <- map["pageCount"]
        news <- map["news"]
    }
}

