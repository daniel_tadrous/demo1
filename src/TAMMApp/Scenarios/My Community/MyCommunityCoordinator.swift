//
//  MyCommunityCoordinator.swift
//  TAMMApp
//
//  Created by Daniel Tadrous on 6/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

enum MyCommunityItems:String {
    case FactsAndFigures
    case CommunityEvents
    case Initiative
    case AboutAbudhabi
    case Polls
    case CoCreation
    case Home
    case News
    case Press
//    func getIndex()->Int{
//        switch self {
//        case .FactsAndFigures:
//            return 0
//        case .CommunityEvents:
//            return 1
//        case .Initiative:
//            return 2
//        case .AboutAbudhabi:
//            return 3
//        case .CoCreation:
//            return 4
//        case .Polls:
//            return 5
//        
//        case .Home:
//            return -1
//        }
//    }
   
//    static func getItemByIndex(index: Int)->MyCommunityItems{
//        
//        switch index {
//        case 0:
//            return FactsAndFigures
//        case 1:
//            return CommunityEvents
//        case 2:
//            return Initiative
//        case 3:
//            return AboutAbudhabi
//        case 4:
//            return CoCreation
//        case 5:
//            return Polls
//        default:
//            return Home
//        }
//    }
}
protocol MyCommunityCoordinatorDelegate {
    
}
class MyCommunityCoordinator: NavigationCoordinator {
    var navigationController: UINavigationController
    
    
    // MARK: - Properties
    var tabBarController:UITabBarController?
    let container: Container
//    var vc:UIViewController?
    //let authenticationService : AuthenticationService
     var delegate: MyCommunityCoordinatorDelegate?
    
    init(container: Container, myCommunityNavigationController: UINavigationController, tabBarControlor:UITabBarController) {
        self.container = container
        self.navigationController = myCommunityNavigationController
        self.tabBarController = tabBarControlor
        // self.authenticationService = authenticationService
    }
    init(container: Container, myCommunityNavigationController: UINavigationController) {
        self.container = container
        self.navigationController = myCommunityNavigationController
    }
    // MARK: - Coordinator core
    
    fileprivate func openInitialView(animated: Bool) {
        let vc = container.resolveViewController(MyCommunityViewController.self)
        //self.navigationController = vc.navigationController!
        vc.delegate = self;
        vc.viewModel = MyCommunityViewModel();
        navigationController.setViewControllers([vc], animated: animated)
    }
    
    func start(){
        start( withItem: .Home)
    }
    
    func start( withItem: MyCommunityItems = .Home ) {
        initialize()
        switch withItem {
        case .Home:
            openInitialView(animated: false)
        case .CoCreation:
            openCoCreatePage(animated: false)
        case .CommunityEvents:
            openEvents()
        case .News:
            openNewsAndPress(type: .NEWS)
        case .Press:
             openNewsAndPress(type: .PRESS)
        default:
            openInitialView(animated: false)

        }
    }
    
    
    private func initialize(setBarHidden: Bool = true){
        //navigationController.pushViewController(vc, animated: true)
        self.navigationController.setNavigationBarHidden(setBarHidden, animated: false)
        let navigationController = tabBarController?.selectedViewController as? UINavigationController
        self.navigationController = navigationController!
    }
    
    fileprivate func openCoCreatePage(animated: Bool) {
        let vc = container.resolveViewController(CoCreateViewController.self)
        vc.viewModel.start.value = true
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: animated)
    }
    
}

extension MyCommunityCoordinator: MyCommunityViewControllerDelegate{
    
    func openPolls(){
        let vc = container.resolveViewController(PollsViewController.self)
        vc.viewModel.start.value = true
        self.navigationController.pushViewController(vc, animated: true)
    }
    func openEvents(){
        let vc = container.resolveViewController(EventsParentViewController.self)
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    func openInitiative(){
        let vc = container.resolveViewController(InitiativeViewController.self)
        vc.viewModel.getInitiatives()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    func openFactsAndFigures(){
        let vc = container.resolveViewController(FactsAndFiguresViewController.self)
        vc.viewModel.pageOpened.value = true
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    func openCoCreate(){
        let vc = container.resolveViewController(CoCreateViewController.self)
        vc.viewModel.start.value = true
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    func openNewsAndPress(type:NewsAndPressReleasesEnum = .NEWS) {
        let vc = container.resolveViewController(NewsAndPressReleasesViewController.self)
        vc.selectedTap = type
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
}
extension MyCommunityCoordinator: CoCreateViewControllerDelegate{
    func coCreateDoPresentCollaborateConfirmation() {
        
        // view model and data will be set in the helper factory
        let vc = ConfirmationFactory.create(container: container, type: .collaborate, referenceNumber: nil)
        
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        self.navigationController.setViewControllers([vc], animated: true)
    }
    
    func openPollsFromCoCreate() {
        openPolls()
    }
    
    
}

extension MyCommunityCoordinator: ConfirmationViewControllerDelegate{
    func confirmationDidFinish(type: ConfirmationType) {
        switch type {
        case .collaborate:
            initialize(setBarHidden: false)
            openInitialView(animated: true)
            
//            self.navigationController.popToRootViewController(animated: true)
//        default:
            
        }
    }
    
    
}

extension MyCommunityCoordinator: EventsParentViewControllerDelegate{
    
    fileprivate func embedViewController(child: UIViewController , To parent:EventsParentViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    func openEventsTab() {
//        let childVCont = container.resolveViewController(EventsViewController.self)
//        childVCont.viewModel.eventsFilterApplyModel.value = EventsFilterApplyModel()
//        embedViewController(child: childVCont, To: parentVC)
    }
    
    func openPublicHolidays() {
//        let childVCont = container.resolveViewController(PublicHolidaysViewController.self)
//        childVCont.viewModel.getMyEvents()
//        childVCont.setupHooks()
//        embedViewController(child: childVCont, To: parentVC)
        
    }
    
    
}

extension MyCommunityCoordinator : NewsAndPressReleasesViewControllerDelegate{
    
    fileprivate func embedViewController(child: UIViewController , To parent:NewsAndPressReleasesViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    
    func openNews() {
//        let childVCont = container.resolveViewController(NewsViewController.self)
//        childVCont.viewModel?.newsPage.value = 1
//        embedViewController(child: childVCont, To: parent)
    }
    
    func openPressReleases() {
//        let childVCont = container.resolveViewController(PressReleasesViewController.self)
//        embedViewController(child: childVCont, To: parent)
    }
    
    
}


