//
//  LocalizedTextView.swift
//  TAMMApp
//
//  Created by kerolos on 6/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class LocalizedTextView: UITextView, CustomFontComponent, UITextViewDelegate {
      var disposeBag = DisposeBag()
    @IBInspectable
    var placeholderTextColor: UIColor?
    var placeHolderVariable:UILabel!
    var attributedTextProxy: NSAttributedString?{
        get{
            return attributedText
        }
        set{
            attributedText = newValue
        }
    }
    
    var textProxy: String?{
        return text
    }
    
    var lineBreakModeProxy: NSLineBreakMode?{
        
        return NSLineBreakMode.byWordWrapping
    }
    
    var fontProxy: UIFont?{
        get{
            return self.font
        }
        set {
            self.font = newValue
        }
    }
    
    @IBInspectable
    var applyFontScalling: Bool = false
    
    @IBInspectable var RTLFont:String = "Swissra-Normal"

    
    var originalFont: UIFont?

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        self.adjustAlignment(RTL: isRTL)
        //        self.adjustFont(RTL: isRTL)
    }
    
    
    fileprivate func adjustAlignment(RTL:Bool){
        if(RTL){
            if(self.textAlignment != .center){
                self.textAlignment = .right
                
            }
        }
        else{
            if(self.textAlignment != .center){
                self.textAlignment = .left
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        adjustFont()
        
    }
    
    fileprivate func registerTextObserver() {
        //        print(fontProxy?.fontName)
        (UIApplication.shared.delegate as! AppDelegate).isTraitChanged.asObservable().subscribe(onNext: { [unowned self] (_) in
            self.adjustFont()
            
        }).disposed(by: disposeBag)
    }
    
   
    
    override var description: String{
        get {
            return super.description + " text=\(String(describing: self.text))"
        }
    }
    func getPlaceHolderLabel(_ placeholderText: String) -> UILabel {
        
        let placeholderLabel = LocalizedLabel()
        placeholderLabel.font = self.font
        placeholderLabel.originalFont = self.originalFont
        placeholderLabel.applyFontScalling = true
        placeholderLabel.RTLFont = self.RTLFont
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        return placeholderLabel
    }
    
    
    
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    @IBInspectable
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
    
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.characters.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    
//
//    internal func getPlaceHolderLabel(_ placeholderText: String) -> UILabel {
//        let placeholderLabel = UILabel()
//        placeholderLabel.text = placeholderText
//        placeholderLabel.sizeToFit()
//
//        placeholderLabel.font = self.font
//        placeholderLabel.textColor = UIColor.lightGray
//        return placeholderLabel
//    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = getPlaceHolderLabel(placeholderText)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
        placeHolderVariable = placeholderLabel
    }
    
    
    
}

