//
//  IncidentHeaderViewController.swift
//  TAMMApp
//
//  Created by Daniel on 8/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
enum CaseDetailEnum:Int{
    case RECEIVED=0,UNDER_REVIEW=1,REQUEST_FOR_INFORMATION=2,IN_PROGRESS=3,RESOLVED=4,CLOSED=5,REJECTED=6
    
    static func fromString(rawValue:String)->CaseDetailEnum {
        switch rawValue {
        case CaseDetailEnum.RECEIVED.toString():
            return CaseDetailEnum.RECEIVED
        case CaseDetailEnum.REQUEST_FOR_INFORMATION.toString():
            return CaseDetailEnum.REQUEST_FOR_INFORMATION
        case CaseDetailEnum.UNDER_REVIEW.toString():
            return CaseDetailEnum.UNDER_REVIEW
        case CaseDetailEnum.IN_PROGRESS.toString():
            return CaseDetailEnum.IN_PROGRESS
        case CaseDetailEnum.RESOLVED.toString():
            return CaseDetailEnum.RESOLVED
        case CaseDetailEnum.CLOSED.toString():
            return CaseDetailEnum.CLOSED
        case CaseDetailEnum.REJECTED.toString():
            return CaseDetailEnum.REJECTED
        default:
            return CaseDetailEnum.RECEIVED
        }
    }
    func toString()->String{
        switch self {
        case .RECEIVED:
            return "RECEIVED"
        case .UNDER_REVIEW:
            return "UNDER_REVIEW"
        case .REQUEST_FOR_INFORMATION:
            return "REQUEST_FOR_INFORMATION"
        case .IN_PROGRESS:
            return "IN_PROGRESS"
        case .RESOLVED:
            return "RESOLVED"
        case .CLOSED:
            return "CLOSED"
        case .REJECTED:
            return "REJECTED"
        }
    }
    
}
class IncidentHeaderViewController: UIViewController {

    
    let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let outputFormat = "dd MMM"
    
    @IBOutlet weak var line1: UIView!
    @IBOutlet weak var line2: UIView!
    @IBOutlet weak var line3: UIView!
    
    @IBOutlet weak var icon1: UILabel!
    @IBOutlet weak var icon2: UILabel!
    @IBOutlet weak var icon3: UILabel!
    @IBOutlet weak var icon4: UILabel!
    
    @IBOutlet weak var date1: LocalizedLabel!
    @IBOutlet weak var date2: LocalizedLabel!
    @IBOutlet weak var date3: LocalizedLabel!
    @IBOutlet weak var date4: LocalizedLabel!
    
    @IBOutlet weak var greyLine: UIView!
    
    @IBOutlet weak var title1: LocalizedLabel!
    @IBOutlet weak var title2: LocalizedLabel!
    @IBOutlet weak var title3: LocalizedLabel!
    @IBOutlet weak var title4: LocalizedLabel!
    
    var state: CaseDetailEnum = .RECEIVED
    
    var greyColor: UIColor!{
        get{
            
            return self.greyLine.backgroundColor
        }
    }
    var greenColor: UIColor!{
        get{
            
            return self.line1.backgroundColor
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        updateState()
    }
    
    
    
    func updateState(){
        switch state {
        case .RECEIVED:
            self.line1.isHidden = true
            self.line2.isHidden = true
            self.line3.isHidden = true
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greyColor
            self.title3.textColor = self.greyColor
            self.title4.textColor = self.greyColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greyColor
            self.icon3.textColor = self.greyColor
            self.icon4.textColor = self.greyColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "s"
            self.icon3.text = "s"
            self.icon4.text = "s"
        
        case .UNDER_REVIEW:
            self.line1.isHidden = false
            self.line2.isHidden = true
            self.line3.isHidden = true
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greenColor
            self.title3.textColor = self.greyColor
            self.title4.textColor = self.greyColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greenColor
            self.icon3.textColor = self.greyColor
            self.icon4.textColor = self.greyColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "\u{e040}"
            self.icon3.text = "s"
            self.icon4.text = "s"
            
            self.title2.text = L10n.getCaseTitle(type: state)
        case .REQUEST_FOR_INFORMATION:
            self.line1.isHidden = false
            self.line2.isHidden = true
            self.line3.isHidden = true
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greenColor
            self.title3.textColor = self.greyColor
            self.title4.textColor = self.greyColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greenColor
            self.icon3.textColor = self.greyColor
            self.icon4.textColor = self.greyColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "\u{e040}"
            self.icon3.text = "s"
            self.icon4.text = "s"
            
            self.title2.text = L10n.getCaseTitle(type: state)
        case .IN_PROGRESS:
            self.line1.isHidden = false
            self.line2.isHidden = false
            self.line3.isHidden = true
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greenColor
            self.title3.textColor = self.greenColor
            self.title4.textColor = self.greyColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greenColor
            self.icon3.textColor = self.greenColor
            self.icon4.textColor = self.greyColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "\u{e040}"
            self.icon3.text = "\u{e040}"
            self.icon4.text = "s"
            
        case .RESOLVED:
            self.line1.isHidden = false
            self.line2.isHidden = false
            self.line3.isHidden = false
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greenColor
            self.title3.textColor = self.greenColor
            self.title4.textColor = self.greenColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greenColor
            self.icon3.textColor = self.greenColor
            self.icon4.textColor = self.greenColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "\u{e040}"
            self.icon3.text = "\u{e040}"
            self.icon4.text = "\u{e040}"
            
            self.title4.text = L10n.getCaseTitle(type: state)
            self.title4.textColor = UIColor.darkishGreen
        case .CLOSED:
            self.line1.isHidden = false
            self.line2.isHidden = false
            self.line3.isHidden = false
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greenColor
            self.title3.textColor = self.greenColor
            self.title4.textColor = self.greenColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greenColor
            self.icon3.textColor = self.greenColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "\u{e040}"
            self.icon3.text = "\u{e040}"
            self.icon4.text = "\u{e040}"
            
            self.title4.text = L10n.getCaseTitle(type: state)
            self.title4.textColor = UIColor.darkishGreen
            self.icon4.textColor = UIColor.darkishGreen
        case .REJECTED:
            self.line1.isHidden = false
            self.line2.isHidden = false
            self.line3.isHidden = false
            
            self.title1.textColor = self.greenColor
            self.title2.textColor = self.greenColor
            self.title3.textColor = self.greenColor
            self.title4.textColor = self.greenColor
            
            self.icon1.textColor = self.greenColor
            self.icon2.textColor = self.greenColor
            self.icon3.textColor = self.greenColor
            self.icon4.textColor = self.greenColor
            
            self.icon1.text = "\u{e040}"
            self.icon2.text = "\u{e040}"
            self.icon3.text = "\u{e040}"
            self.icon4.text = "\u{e044}"
            
            self.title4.text = L10n.getCaseTitle(type: state)
            self.title4.textColor = UIColor.red
            self.icon4.textColor = UIColor.red
        }
    }
    func setDates(datesStr: [String]){
        self.date1.text = datesStr.count > 0 ? datesStr[0].getDate(format: inputFormat)?.getFormattedString(format: outputFormat) : ""
        self.date2.text =  datesStr.count > 1 ? datesStr[1].getDate(format: inputFormat)?.getFormattedString(format: outputFormat) : ""
        self.date3.text =  datesStr.count > 2 ? datesStr[2].getDate(format: inputFormat)?.getFormattedString(format: outputFormat) : ""
        self.date4.text =  datesStr.count > 3 ? datesStr[3].getDate(format: inputFormat)?.getFormattedString(format: outputFormat) : ""
    }
    func setState(caseDetail: CaseDetailEnum){
        self.state = caseDetail
        self.updateState()
    }

}
