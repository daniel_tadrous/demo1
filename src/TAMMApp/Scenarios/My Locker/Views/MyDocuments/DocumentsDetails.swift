//
//  DocumentsDetails.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class DocumentsDetails: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var expiresOnLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var detailsTable: UITableView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var documentType:DocumentType?
    var id = -1
    var viewModel:MyDocumentsViewModel!
    var document:MyDocumentModel?{
        get{
            return viewModel.detailedDocument.value
        }
    }
    var disposeBag = DisposeBag()
    
    
    let headerViewIdentifier = "DetailedDocumentHeader"
    let cellIdentifier = "DetailedDocumentCell"
    
    var shareClosure:(()->Void)!
    var editClosure:(()->Void)!
    var closeClosure:(()->Void)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    init(frame: CGRect,model:MyDocumentsViewModel,type:DocumentType,id:Int) {
        super.init(frame: frame)
        viewModel = model
        self.id = id
        documentType = type
        commonInit()
        setupBinding()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        Bundle.main.loadNibNamed("DocumentDetails", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        shareLabel.text = "\u{e003}"
        detailsTable.delegate = self
        detailsTable.dataSource = self
        if documentType == .Expired {
            editLabel.text = ""
            editButton.isEnabled = false
            shareLabel.text = ""
            shareButton.isEnabled = false
        }else{
        
            editLabel.text = "\u{e045}"
        }
        detailsTable.register(UINib(nibName: headerViewIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        detailsTable.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        
    }
    func setupBinding(){
        viewModel.detailedDocument.asObservable()
            .subscribeOn(MainScheduler.instance).subscribe(onNext: { (doc) in
                if doc == nil{
                    // should do domething here
                }else{
                    self.setOutlets()
                    self.detailsTable.reloadData()
                }
            })
    }
    
    func setOutlets(){
        ImageCachingUtil.loadImage(url: URL(string: (document?.icon)!)!, dispatchInMain: true
            , callBack: { (url, image) in
                self.logoImage?.image = image
        })
        typeLabel.text = document?.type
        if (document?.name?.count)! > 0{
            idLabel.text = document?.name
        }else{
            idLabel.text = "Account ID - \(document?.idNumber)"
        }
        if documentType == .Active {
            expiresOnLabel.text = L10n.MyDocsStrings.expiresOn
        }
        expirationDateLabel.text = document?.expidationDate?.getDate(format: "yyyy-MM-dd'T'HH:mm:ss.SSS")?.getFormattedString(format: "dd MMM, YYYY")
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        editDocument()
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        shareDocument()
    }
    
    func editDocument() {
        let editOverlay = EditFieldView(frame: UIScreen.main.bounds,currentName:(document?.name)!)
        editOverlay.closeClosure = closeAction
        editOverlay.okClosure = okAction(_:)
        UIApplication.shared.keyWindow!.addSubview(editOverlay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: editOverlay)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    func shareDocument() {
        if document != nil{
            let shareOverlay = DocumentEmailView(frame: UIScreen.main.bounds)
            shareOverlay.closeClosure = closeAction
            shareOverlay.sendClosure = sendAction(_:_:)
            shareOverlay.subjectField.text = "Tawtheeq - \((document?.idNumber)!)"
            UIApplication.shared.keyWindow!.addSubview(shareOverlay)
            UIApplication.shared.keyWindow!.bringSubview(toFront: shareOverlay)
        }
    }
    
    func closeAction() -> Void {
        
    }
    
    func sendAction(_ to:String,_ subject:String)->Void{
        viewModel.shareDocument(id: (document?.Id)!, to: to,subject: subject,closure:emailSentClosure)
    }
    
    func emailSentClosure() ->Void {
        let viewOverlay = EmailSentView(frame: UIScreen.main.bounds)
        UIApplication.shared.keyWindow!.addSubview(viewOverlay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: viewOverlay)
    }
    
    func okAction(_ nickname:String) -> Void {
        idLabel.text = nickname
        document?.name = nickname
        viewModel.updateDocument(document: document!)
    }
    
}

extension DocumentsDetails: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier) as? DetailedDocumentHeader
        headerView?.title.text = document?.sections?[section].name
        let docSection = (document?.sections?[section])!
        headerView?.onClick = {
            docSection.isExpanded = !docSection.isExpanded
            tableView.reloadData()
        }
        headerView?.rotateExpandLabel(docSection.isExpanded)
        
        return headerView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return document?.sections?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let section = document?.sections?[section]{
            if section.isExpanded{
                return section.fields?.count ?? 0
            }
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! DetailedDocumentCell
        let section = document?.sections![indexPath.section]
        let field = section!.fields![indexPath.row]
        cell.titleLabel.text = field.key
        cell.detailLabel.text = field.value
        return cell
    }


}
