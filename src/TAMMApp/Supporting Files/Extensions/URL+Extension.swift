//
//  URL+Extension.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

extension URL  {
    func getMediaTime() -> String {
        
        let duration = AVURLAsset(url: self).duration.seconds
        print(duration)
        
//        let time: String
//        if duration > 3600 {
//            time = String(format:"%d : %d : %d",
//                          Int(duration/3600),
//                          Int((duration/60).truncatingRemainder(dividingBy: 60)),
//                          Int(duration.truncatingRemainder(dividingBy: 60)))
//        } else {
//            time = String(format:"%d : %d",
//                          Int((duration/60).truncatingRemainder(dividingBy: 60)),
//                          Int(duration.truncatingRemainder(dividingBy: 60)))
//        }
        return adjustTimeToDisplay(duration:duration)
    }
    
    
    func createThumbnailOfVideoFromFileURL() -> UIImage? {
        let asset = AVAsset(url: self)
        print(asset.isCompatibleWithAirPlayVideo)
        
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            return nil
        }
    }
    
    fileprivate func adjustTimeToDisplay(duration:Double)->String{
        let hrs:String
        let min:String
        let sec:String
        let time : String
        
        let hours = Int(duration/3600)
        let minutes = Int((duration/60).truncatingRemainder(dividingBy: 60))
        let seconds = Int(duration.truncatingRemainder(dividingBy: 60))
        
        if hours == 0 {
            hrs = ""
        }else {
            hrs =  String(hours) + ":"
        }
        
        if minutes == 0 {
            min = "00:"
        }else if minutes < 10 {
            min = "0" + String(minutes) + ":"
        }else {
            min = String(minutes) + ":"
        }
        
        if seconds == 0 {
            sec = "00"
        }else if seconds < 10 {
            sec = "0" + String(seconds)
        }else {
            sec = String(seconds)
        }
        
        return hrs+min+sec
    }
    
    func sizePerMB() -> Double {
        return  Double(self.fileSizeInKB)/1024
    }
    

    func isSizeRight()->Bool {
        
        let data = try? Data(contentsOf: self)
        if data != nil {
            let size = (data?.count)!/1024
            if size > 8192 {
                return false
            }else {
                return true
            }
        }else{
            //handle  ???
            return true
        }
        
    }

    var fileSizeInKB:Int{
        get{
            var size = 0
            let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
            let resourceValues = try? self.resourceValues(forKeys: keys)
            
            let unsignedInt64 = (resourceValues?.fileSize ?? resourceValues?.totalFileSize ?? 0)
            size = Int(Int64(bitPattern: UInt64(unsignedInt64)))/1024
            if size == 0{
               let data = try? Data(contentsOf: self)
                if data != nil{
                    size = (data?.count ?? 0)/8/1024
                }
            }
            return size
        }
    }
    
    func export(completionHandler: @escaping (_ fileURL: URL?,_ error: Error?)->()){
        let name = self.lastPathComponent
        let mgr = FileManager()
        let destination = IncidentUrl.getDirectoryForIncident().appendingPathComponent("\(UUID().uuidString)\(name)")
        do {
            try mgr.copyItem(at: self, to: destination)
            completionHandler(destination,nil)
        }catch{
            completionHandler(nil,error)
        }
    }
    var type: FileTypeEnum{
        get{
            let videoFormats = ["mp4","avi","mpeg"]
            let audioFormats = ["mp3","arm","wav","m4a"]
            let imgFormats = ["jpg","jpeg","png","svg"]
            
            let name = self.lastPathComponent
            
            let format:String = String((name.split(separator: ".")[1]) )
            if videoFormats.contains(where: { (vformat) -> Bool in
                return vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.VIDEO
            }else if audioFormats.contains(where: { (vformat) -> Bool in
                return vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.AUDIO
            }else if imgFormats.contains(where: { (vformat) -> Bool in
                return  vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.IMAGE
            }else{
                return FileTypeEnum.TXT
            }
            
        }
    }
}
