//
//  FactsAndFiguresViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift


protocol  FactsAndFiguresViewControllerDelegate{
    
}


class FactsAndFiguresViewController: TammViewController,FactsAndFiguresStoryboardLoadable{

  //  @IBOutlet weak var htmlLabel: UILabel!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var factsAndFiguresWebView: UIWebView!
    public var viewModel:FactsAndFiguresViewModel!
    fileprivate var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = L10n.factsAndFigures
        //factsAndFiguresWebView.clipsToBounds = true
        //factsAndFiguresWebView.layer.shadowColor = UIColor.black.cgColor
        factsAndFiguresWebView.clipsToBounds = true
        factsAndFiguresWebView.layer.cornerRadius = 5
        
        parentView.addRoundCorners(radious: 5)
        //parentView.clipsToBounds = true
        parentView.layer.shadowOpacity = 0.1
        
        setupUIBinding()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    fileprivate func setupUIBinding(){
        
        viewModel.content.asObservable().subscribe(onNext: { response in
            if response != nil {
                
                self.factsAndFiguresWebView.loadHTMLString(response!, baseURL: nil)
                
                //self.factsAndFiguresWebView.scalesPageToFit = true
//                self.factsAndFiguresWebView.frame.width = self.view.frame.size.width
//                self.factsAndFiguresWebView.scalesPageToFit = true
                //self.factsAndFiguresWebView.sizeToFit()
//                self.factsAndFiguresWebView.contentMode = UIViewContentMode.scaleAspectFit
            }
            
        }).disposed(by: disposeBag)
    }
    
 
    
    
//        viewModel.content.asObservable().subscribe(onNext: { response in
//            self.htmlLabel.attributedText = response?.getAttributedStringWithList(width:Float((self.view.frame.size.width-16)))
//        }).disposed(by: disposeBag)
//    }
    
}
