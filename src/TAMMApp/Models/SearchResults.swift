//
//  SearchSuggestions.swift
//  TAMMApp
//
//  Created by Daniel on 9/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchResults: Mappable {
    
    var Results:[SearchResult] = []
    var Count: Int?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        Results <- map["Results"]
        Count <- map["Count"]
    }
    
    class SearchResult: Mappable {
        
        var Id: String?
        var Language: String?
        var Path: String?
        var Url: String?
        var Name: String?
        var Html: String?
        
        required init?(map: Map) {
            
        }
        
        // Mappable
        func mapping(map: Map) {
            Id <- map["Id"]
            Language <- map["Language"]
            Path <- map["Path"]
            Url <- map["Url"]
            Name <- map["Name"]
            Html <- map["Html"]
        }
    }
}
