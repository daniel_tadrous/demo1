//
//  FilterView.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift
import CoreLocation

protocol FilterViewDelegate {
    
    var filterModel : EventsFilterApplyModel {get set}
    func applyFilter() ->Void
    
}

class FilterView : UIView{
    
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fromTF: LocalizedTextField!
    
    @IBOutlet weak var toTF: LocalizedTextField!
    
    @IBOutlet weak var applyFiltersBtn: LocalizedButton!
    
    @IBOutlet weak var categoryStackView: UIStackView!
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var toBtn: LocalizedButton!
    @IBOutlet weak var fromBtnClick: LocalizedButton!
    
    //var onApplyFilterClicked:((EventFilterModel)->Void)?
    var enableSearchLocation = true
    var filterViewDelegate:FilterViewDelegate?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var disposeBag = DisposeBag()
    
    var viewModel: FilterViewModel?
    var delegateViewModel:filterViewModelDelegate?
    
    //private var eventsFilterApplyModel = EventsFilterApplyModel()
    
    var filterObject = EventsFilterApplyModel()
    
    private var locations = [String]()
    var selectedIndex = -1
    var dateFormat = "MMM yyyy"
    
    var currentAddress: Variable<String?> = Variable(nil)
    
    @IBOutlet weak var locationsStackView: UIStackView!
    @IBOutlet weak var locationStackContainer: UIView!
    
    var toDate = Date()
    var fromDate = Date()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    
    
    init(frame: CGRect,delegateViewModel: filterViewModelDelegate,filterViewDelegate:FilterViewDelegate,enableSearchLocation:Bool){
        super.init(frame: frame)
        self.delegateViewModel = delegateViewModel
        self.filterViewDelegate = filterViewDelegate
        //self.onApplyFilterClicked = filterViewDelegate.applyFilter(_:)
        self.enableSearchLocation = enableSearchLocation
        commonInit()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        let viewFileName: String = "EventsFilterView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        
        searchView.isHidden = !enableSearchLocation
        if !enableSearchLocation {
            searchViewHeight.constant = 0
        }
        
        viewModel = FilterViewModel(apiService: (delegateViewModel?.apiServiceObject)!, isGetLocations: enableSearchLocation, locationService: LocationService())
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        layoutSubviews()
        self.viewModel?.locationsQuery.value = nil
        
        setupHooks()
        setupUiData()
    }
    
    
    @objc func cancelBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
        self.disposeBag = DisposeBag()
        
    }
    
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
        if enableSearchLocation{
            self.viewModel?.startLocationService()
            
            viewModel?.locationsViewData.asObservable()
                .subscribeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] locs in
                    if(locs == nil){
                        
                    }else{
                        self.locations = locs!
                        self.setupLocations()
                    }
                })
                .disposed(by: disposeBag)
            currentAddress.asObservable().subscribeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] locs in
                    self.setupLocations()
                }).disposed(by: disposeBag)
            
            viewModel?.currentLocationViewData.asObservable()
                .subscribe(onNext: { [unowned self] location in
                    location?.getAddress(completion: { (address:String?) in
                        
                        self.currentAddress.value = address ?? ""
                        
                    },parts: [EAddressPart.Locality, EAddressPart.AdministrativeArea])
                    
                })
                .disposed(by: disposeBag)
        }
        
        viewModel?.categoriesViewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] cats in
                if(cats == nil){
                  //nil
                }else{
                    self.setupCategories(cats: cats!)
                }
            })
            .disposed(by: disposeBag)
        //viewModel?.isToGetCategories.value = true
        
        
        
        
    }
    
    
    private func setupLocations(){
        for view in self.self.locationsStackView.arrangedSubviews{
            view.isHidden = true
            view.removeFromSuperview()
        }
        if let loc =  self.currentAddress.value{
            let currentLocationView = LocationView(frame: CGRect(x: 0, y: 0, width: self.locationsStackView.frame.width, height: 50), isCurrentLocation: true, location: loc)
            currentLocationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(locationSelected(_:))))
            self.locationsStackView.addArrangedSubview(currentLocationView)
        }
        for loc in self.locations{
            let locationView = LocationView(frame: CGRect(x: 0, y: 0, width: self.locationsStackView.frame.width, height: 50), isCurrentLocation: false, location: loc)
            locationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(locationSelected(_:))))
            self.locationsStackView.addArrangedSubview(locationView)
        }
    }
    
    @objc func locationSelected(_ sender: Any){
        let locationView = (sender as! UITapGestureRecognizer).view as! LocationView
        if locationView.isCurrentLocation{
            self.searchTF.text = self.currentAddress.value
        }else{
            self.searchTF.text = locationView.location
        }
        self.locationStackContainer.isHidden = true
    }
    
    private func setupCategories(cats:[String]){
        for v in  self.categoryStackView.arrangedSubviews{
            v.isHidden = true
        }
        for cat in cats{
            self.categoryStackView.addArrangedSubview(CategoryCheckboxView(frame: CGRect(x: 0, y: 0, width: self.categoryStackView.frame.width, height: 60), category: cat, OnCheckChanged: { (category:String, isSelected:Bool) in
                if isSelected{
                    self.filterObject.categories.append(category)
                }else{
                    for i in 0..<self.filterObject.categories.count{
                        if self.filterObject.categories[i] == category{
                            self.filterObject.categories.remove(at: i)
                            break
                        }
                    }
                }
            }))
        }
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        layoutSubviews()
    }
    
    
    func setupUiData(){
        
        self.searchView.layer.borderColor = UIColor.paleGrey.cgColor
        self.searchView.addRoundAllCorners(radious: 5)
        self.locationStackContainer.addRoundAllCorners(radious: 5)
        self.locationStackContainer.layer.borderWidth = 1
        self.locationStackContainer.layer.borderColor = UIColor.paleGrey.cgColor
        self.locationStackContainer.isHidden = true
        self.searchTF.addTarget(self, action: #selector(searchTextChanged), for: [.editingChanged, .editingDidBegin])
        //from date
        let dateStr = Date().getFormattedString(format: self.dateFormat)
        self.fromTF.text = dateStr
        self.fromTF.inputView = getDatePicker(textField: self.fromTF)
        //to date
        self.toTF.text = dateStr
        self.toTF.inputView =  getDatePicker(textField: self.toTF)
        
        //apply btn
        self.applyFiltersBtn.addRoundCorners(radious: 23)
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
    }
    
    @objc func searchTextChanged() {
        self.viewModel?.locationsQuery.value = self.searchTF.text
        self.locationStackContainer.isHidden = false
        if (self.searchTF.text?.isEmpty)! {
            self.locationStackContainer.isHidden = true
        }
    }
    
    @IBAction func ApplyFilterClickHandler(_ sender: LocalizedButton) {
        self.filterObject.location = self.searchTF.text!
        let dF = DateFormatter(withFormat: "yyyy-MMM", locale: "en")
        self.filterObject.from = dF.string(from: fromDate)
        self.filterObject.to = dF.string(from: toDate)
        filterViewDelegate?.filterModel = filterObject
        filterViewDelegate?.applyFilter()
        self.cancelBtnClicked()
        print("elzorar etdas ya rgalaaaaa")
    }
    
    
    
    
    func getDatePicker(textField: UITextField)->TammMonthYearPickerView{
        let monthYearPickerView = TammMonthYearPickerView()
        monthYearPickerView.onDateSelectedDate = { (date: Date) in
            textField.text = date.getFormattedString(format: self.dateFormat)
            if textField.tag == 1{
                self.fromDate = date
            }else{
                self.toDate = date
            }
        }
        return monthYearPickerView
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.scrollView.resizeToFitContent()
    }
    
    
}

