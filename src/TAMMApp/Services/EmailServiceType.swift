//
//  EmailServiceType.swift
//  TAMMApp
//
//  Created by kerolos on 4/23/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit


protocol EmailServiceTypePresenter: class {
    var presenter:UIViewController { get }
}

protocol EmailServiceType {
    func sendEmail(to: [String], cc:[String], presenter: EmailServiceTypePresenter, onCompleted: @escaping ()->()) -> Bool
}
