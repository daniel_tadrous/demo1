//
//  MsgFilter.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

enum MsgFilterType :String {
    case filter = "filter"
    case sort = "sort"
}


protocol MsgSortDelegate {
    //implemented by View Controller
    func applySortClicked(applyFilterOn:String)
}


class MsgSortOverLay: UIView , UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var filterTableView: UITableView!
    
    var delegate:MsgSortDelegate?
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var filterByLabel: LocalizedLabel!
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var roundedView: RoundedView!
    
    @IBOutlet weak var applyFilterBtn: LocalizedButton!
    
    @IBOutlet weak var closeLabel: UILabel!
    @IBOutlet weak var optionsStackView: UIStackView!
    let cellIdentifier = "FilterCell"
    var hasAll = true
    var tableHeight:CGFloat = CGFloat(0)
    var isSelected:[Bool] = [Bool]()
    var isLoaded = false
    var hasStarred = true
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    let sortOptions = [L10n.MyMessages.desc,L10n.MyMessages.asc,L10n.MyMessages.starred]
    var selectedOption:String?
    var filterOptions:[MsgStatus]?
    var type:MsgFilterType = MsgFilterType.filter
    
    @IBAction func applyFilterAction(_ sender: Any) {

            delegate?.applySortClicked(applyFilterOn: selectedOption!)
            closeView()

    }
    
     ///init methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    init(frame: CGRect,availableStatuses:[MsgStatus], selectedOption:String?){
        super.init(frame: frame)
        self.filterOptions = availableStatuses
        if selectedOption == nil || selectedOption == "All" {
            isSelected = [true]
        }else {
            self.selectedOption = selectedOption
            isSelected = [false]
            
        }
        setFilterSelection()
        commonInit()
        setupViews()
    }
    
    init(frame: CGRect,type :MsgFilterType,hasStarred:Bool, selectedOption:String?){
        super.init(frame: frame)
        //self.filterOptions = availableStatuses
        self.type = type
        self.hasStarred = hasStarred
        self.selectedOption = selectedOption
        setSortSelection()
        commonInit()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func setupViews(){
        self.filterTableView.reloadData()
    }
    
    private func commonInit(){
        let viewFileName: String = "MsgSortView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        if type != MsgFilterType.filter {
            filterByLabel.text = L10n.MyMessages.sortBy
            UIView.performWithoutAnimation {
                applyFilterBtn.setTitle(L10n.MyMessages.applySort, for: .normal)
                applyFilterBtn.layoutIfNeeded()
            }
            
        }
        self.filterTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        self.filterTableView.delegate = self
        self.filterTableView.dataSource = self
        applyColors()
    }
    
    
    fileprivate func applyColors() {
        closeLabel.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        backgroundView.backgroundColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1).getAdjustedColor()
        roundedView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        backView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        filterByLabel.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        applyFilterBtn.BorderColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor()
        applyFilterBtn.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        applyFilterBtn.setTitleColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor(), for: .normal)
    }
    
    /// filter methods
    func setFilterSelection() {
        for item in filterOptions! {
            if item.name == selectedOption {
                isSelected.append(true)
            }else {
                isSelected.append(false)
            }
        }
    }
    
    
    
    
    
    
    //sort methods
    func setSortSelection() {
        if  let selection = self.selectedOption {
            if selection == L10n.MyMessages.desc {
                isSelected = [true, false,false]
            }else if selection == L10n.MyMessages.asc{
                isSelected = [false, true,false]
                }else {
                isSelected = [false, false,true]
            }
        }else {
            isSelected = [true, false,false]
        }
    }
    
    
    
    @objc func cancelBtnClicked(){
        closeView()
    }
    
    func closeView() {
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == MsgFilterType.filter {
            return (filterOptions?.count)!+1
                //getNumberOfFilterOptions()
        }else {
            
            return sortOptions.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = filterTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! FilterTableViewCell
        if type  == MsgFilterType.filter  {
                        if hasAll && indexPath.row == 0 {
                            cell.filterTitleLabel.text = "All"
                        }else{
                            cell.filterTitleLabel.text = filterOptions![indexPath.row-1].name
                        }
        }else {
            //Sort Case
            cell.filterTitleLabel.text = sortOptions[indexPath.row]
        }
            cell.backgroundColor = UIColor.white.getAdjustedColor()
            cell.applyInvertColors()
            if isSelected[indexPath.row] {
                cell.selectedRoundedView.backgroundColor = UIColor.darkBlueGrey.getAdjustedColor()
                selectedOption = cell.filterTitleLabel.text
                if L10n.Lang == "en" {
                    cell.filterTitleLabel.font = UIFont(name: "CircularStd-Bold", size: UIFont.fontSizeMultiplier * 16)
                }else {
                    cell.filterTitleLabel.font = UIFont(name: "Swissra-Bold", size: UIFont.fontSizeMultiplier * 16)
                }
    
            }else{
                cell.selectedRoundedView.backgroundColor = UIColor.white.getAdjustedColor()
                if L10n.Lang == "en" {
                        cell.filterTitleLabel.font = UIFont(name: "CircularStd-Medium", size: UIFont.fontSizeMultiplier * 16)
                }else {
                cell.filterTitleLabel.font = UIFont(name: "Swissra-Normal", size: UIFont.fontSizeMultiplier * 16)
                }
                
        }
        
        if !hasStarred && type == MsgFilterType.sort && indexPath.row == 2{
            // case sort and last index and starr is false , star should be gray
            cell.filterTitleLabel.textColor = UIColor.gray
        }
        
        
        return cell
    }
        
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let  rowHeight = UITableViewAutomaticDimension
//        tableHeight += rowHeight
//        if indexPath.row == (filterOptions?.count)!-1 && !isLoaded {
//            isLoaded = true
//            tableViewHeight.constant = tableHeight
//            filterTableView.reloadData()
//        }
        
        return rowHeight
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let  rowHeight = UITableViewAutomaticDimension
        
        return rowHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !hasStarred && type == MsgFilterType.sort && indexPath.row == 2{
            // case sort and last index and starr is false , star should be gray
            return
        }else{
            isSelected.removeAll()
            for index in 0..<tableView.numberOfRows(inSection: 0) {
                if indexPath.row == index {
                    isSelected.append(true)
                }else{
                    isSelected.append(false)
                }
                
            }
            
            self.filterTableView.reloadData()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isLoaded  {
            
            tableViewHeight.constant = CGFloat(tableView.numberOfRows(inSection: 0) * 80)
            //filterTableView.contentSize.height +
            isLoaded = true
            filterTableView.reloadData()
        }
        
    }
    
    
    
    
//    private func getNumberOfFilterOptions() -> Int {
//        var noOfRows:Int?
//        if (filterOptions?.count)! > 0 {
//            hasAll = true
//            noOfRows = (filterOptions?.count)!+1
//        }else {
//            noOfRows = (filterOptions?.count)!
//        }
//
////            for _ in 0..<noOfRows! {
////                //booleanList.append(Fal)
////                isSelected.append(false)
////            }
//
//        //isSelected = booleanList
//        return noOfRows!
//    }

    
}
