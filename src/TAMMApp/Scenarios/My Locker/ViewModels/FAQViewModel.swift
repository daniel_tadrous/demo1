//
//  FAQViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 4/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class FAQViewModel{
    private(set) var viewData: Variable<[AspectOfLifeServiceFaqsModel]?> = Variable(nil)
    private(set) var viewDataAspectOfLife: Variable<[JourneyModel]?> = Variable(nil)
    private(set) var pageId: Variable<Int?> = Variable(nil)
    private(set) var isToGetAspectOfLifeNames: Variable<Bool?> = Variable(nil)
    var faqFilter: FaqFilterModel? = FaqFilterModel()
    fileprivate var api: ApiSupportServicesType
    fileprivate var aspectOfLifeApi: ApiServiceAspectsOfLifeType
    var speechRecognitionService: SpeechRecognitionServiceType!
    fileprivate var disposeBag = DisposeBag()
    let recognizedTextVar: Variable<String?> = Variable(nil)
    let speechDuration: Double = 10.0
    
    init(api: ApiSupportServicesType,aspectOfLifeApi: ApiServiceAspectsOfLifeType, speechRecognitionService: SpeechRecognitionServiceType) {
        self.api = api
        self.aspectOfLifeApi = aspectOfLifeApi
        self.speechRecognitionService = speechRecognitionService
        setupBinding()
    }
    
   
    func startSpeechService(){
        speechRecognitionService.start(delegate: self, duration: self.speechDuration)
    }
    
    fileprivate func setupBinding(){
        pageId.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        isToGetAspectOfLifeNames.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestAspectOfLifeNames()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if pageId.value != nil && pageId.value! != 0{
            api.getFaqs(pageId: pageId.value ?? 1, cat: (self.faqFilter?.cat)!, aspect: (self.faqFilter?.aspect)!, info: (self.faqFilter?.info)!).subscribe(onNext: { [unowned self] faqs in
                    self.viewData.value = faqs
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func requestAspectOfLifeNames(){
        if isToGetAspectOfLifeNames.value != nil && isToGetAspectOfLifeNames.value! != false{
            
            aspectOfLifeApi.getAspectsOfLifeNames().subscribe(onNext: { [unowned self] aspectsOfLife in
                self.viewDataAspectOfLife.value = aspectsOfLife
            }).disposed(by: disposeBag)
            
            
        }else{
            self.viewDataAspectOfLife.value  = nil
        }
    }
}

extension FAQViewModel : SpeechRecognitionServiceTypeDelegate{
    func recognizerAvailable(isAvailable: Bool) {
        
    }
    
    func textRecived(text: String) {
        self.recognizedTextVar.value = text
    }
    
    func recognitionIsDone(_ error: Error?) {
        
    }
    
    
}
