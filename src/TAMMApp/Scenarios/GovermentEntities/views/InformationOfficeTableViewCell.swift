//
//  InformationOfficeTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 7/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


protocol InformationOfficeTableViewCellDelegate: class {
    func mapButtonClicked(office: EntitiesInformationOfficeViewDataType?)
    func phoneButtonClicked(office: EntitiesInformationOfficeViewDataType?)
    func emailButtonClicked(office: EntitiesInformationOfficeViewDataType?)
    func websiteButtonClicked(office: EntitiesInformationOfficeViewDataType?)
}

class InformationOfficeTableViewCell: UITableViewCell {

    public static let namedNibName = "InformationOfficeTableViewCell"
    
    public static let namedNibNameCollapsed = "CollapsedInformationOfficeTableViewCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    @IBOutlet weak var mapImageView: UIImageView!
    
    @IBAction func mapButtonClicked(_ sender: UITammResizableButton) {
        delegate?.mapButtonClicked(office: office)
    }
    @IBAction func phoneButtonClicked(_ sender: Any) {
        delegate?.phoneButtonClicked(office: office)
    }
    @IBAction func emailButtonClicked(_ sender: Any) {
        delegate?.emailButtonClicked(office: office)
    }
    @IBAction func websiteButtonClicked(_ sender: Any) {
       delegate?.websiteButtonClicked(office: office)
    }
    
    public weak var delegate: InformationOfficeTableViewCellDelegate?
    
    public var details: String?{
        didSet{
            detailsLabel.text = details
        }
    }
    
    public var header: String?{
        didSet{
            headerLabel.text = header
        }
    }
    
    public var mapUrl: URL!{
        didSet{
            ImageCachingUtil.loadImage(url: mapUrl!, dispatchInMain: true
                , callBack: { [weak self] (url, image) in
                    if self?.mapUrl == url{
                        self?.mapImageView?.image = image
                    }
            })
        }
    }
    
    
    public var office: EntitiesInformationOfficeViewDataType?{
        didSet{
            self.header = office?.header
            self.details = office?.details
            self.mapUrl = office?.mapData.image

        }
    }
    
    
}
