//
//  SheetCancelView.swift
//  cameraTest
//
//  Created by kerolos on 5/2/18.
//  Copyright © 2018 kerolos. All rights reserved.
//

import Foundation
import UIKit

class SheetCancelView: UIView{
    
    @IBOutlet weak var mainView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit(){
//        roundCorners(view: mainView, radius: 25)
    }
    
    func roundCorners(view: UIView, radius: CGFloat) {
//        self.layer.borderColor = BorderColor?.cgColor
//        view.layer.borderWidth = 1
        if #available(iOS 11.0, *) {
            if(L102Language.currentAppleLanguage().contains("ar")){
                view.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                view.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            view.layer.cornerRadius = radius
        } else {
            // Fallback on earlier versions
            view.layer.cornerRadius = radius
        }
        
    }
    
//
//    override func layerWillDraw(_ layer: CALayer) {
//
//        super.layerWillDraw(layer)
//
//    }
    
    override func layoutSubviews() {
        roundCorners(view: mainView, radius: 25)
        super.layoutSubviews()
    }
    
    
}
