//
//  MediaSettingsViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/9/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


protocol  MediaSettingsViewControllerDelegate{
    func openManageScreen()
}


class MediaSettingsViewController: TammViewController,MediaSettingsStoryboardLoadable {

    var viewModel:MediaSettingsViewModel!
    var delegate:MediaSettingsViewControllerDelegate?

    @IBOutlet weak var uploadOnWifiSwitch: UISwitch!
    
    
    
    @IBOutlet weak var photosNumberLabel: LocalizedLabel!
    
    @IBOutlet weak var photosSizeLabel: LocalizedLabel!
    
    @IBOutlet weak var videosNumberLabel: LocalizedLabel!
    
    
    @IBOutlet weak var videosSizeLabel: LocalizedLabel!
    
    
    @IBOutlet weak var audioNumberLabel: LocalizedLabel!
    
    
    @IBOutlet weak var audioSizeLabel: LocalizedLabel!
    
    
    @IBOutlet weak var totalSizeLabel: LocalizedLabel!
    
    
    @IBOutlet weak var manageView: UIView!
    
    
    @IBOutlet weak var documentNumberLabel: LocalizedLabel!
    
    @IBOutlet weak var documentSizeLabel: LocalizedLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar(title: L10n.Preferences.mediaSettings, willSetSearchButton: true)
     //   let bundle = Bundle(for: type(of: self))
        view.backgroundColor = UIColor.paleGreyTwo
        
        uploadOnWifiSwitch.isOn = viewModel.getSwitchStatus()
        setupHooks()
        addTapGesturseOnManageView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupData()
    }
    
    fileprivate func addTapGesturseOnManageView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(setReminderManageViewClicked))
        manageView.addGestureRecognizer(tap)
    }
    
    
    @objc func setReminderManageViewClicked() {
        
        
        
      //  NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: "/Users/")
        
        
        delegate?.openManageScreen()
    }
    
    let price = 123.436 as NSNumber
    
    
   
    

    

    
    func setupData() {
        let  mediaData = viewModel.getMediaData()
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        if L10n.Lang == "ar" {
             formatter.locale = Locale(identifier: "az-Arab")
        }else {
            formatter.locale = Locale(identifier: "en")
        }
       
     

        
        audioNumberLabel.text = String(mediaData.audios.numberOfFiles)
        audioSizeLabel.text =  formatter.string(from: NSNumber(value: mediaData.audios.sizeOfFiles))! + " " + L10n.megaByte
       // viewModel.getDisplayedNumber(number: mediaData.audios.sizeOfFiles)  + " " + "MB"
        photosNumberLabel.text = String(mediaData.photos.numberOfFiles)
        photosSizeLabel.text =  formatter.string(from: NSNumber(value: mediaData.photos.sizeOfFiles))!  + " " + L10n.megaByte
        
        videosNumberLabel.text = String(mediaData.videos.numberOfFiles)
        videosSizeLabel.text =  formatter.string(from: NSNumber(value: mediaData.videos.sizeOfFiles))!  + " " + L10n.megaByte
        
        documentNumberLabel.text = String(mediaData.documents.numberOfFiles)
        documentSizeLabel.text = formatter.string(from: NSNumber(value: mediaData.documents.sizeOfFiles))!  + " " + L10n.megaByte
        
        let totalSize = mediaData.videos.sizeOfFiles+mediaData.photos.sizeOfFiles+mediaData.audios.sizeOfFiles+mediaData.documents.sizeOfFiles
        
        totalSizeLabel.text = formatter.string(from: NSNumber(value: totalSize))! + " " + L10n.megaByte
            //viewModel.getDisplayedNumber(number: totalSize)
    }
    
    func setupHooks() {
        uploadOnWifiSwitch.rx.isOn.subscribe(onNext: { (bool) in
            self.viewModel.setSwitchStatus(withValue: bool)
            self.viewModel.WifiOnly.value = bool
        })
    }

//    @IBAction func goToManageAction(_ sender: Any) {
//        self.displayToast(message: L10n.commingSoon)
//    }
    
}
