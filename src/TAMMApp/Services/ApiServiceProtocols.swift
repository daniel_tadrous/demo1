//
//  ApiServiceProtocols.swift
//  TAMMApp
//
//  Created by kerolos on 4/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

protocol ApiServiceAspectsOfLifeType{
    
    func getAspectsOfLife () -> Observable<[JourneyModel]>
    
    func getAspectsOfLifeNames() -> Observable<[JourneyModel]>
    
    func getAspectOfLifeDetails(id:String) -> Observable<AspectOfLifeDetailModel>
    
    //return topic and its subtopics
    func getAspectOfLifeTopicDetails(id: String) -> Observable<AspectOfLifeServiceTopicDetailModel> 
    
    //return service faqs
    func getAspectOfLifeServiceWithFaqs(id: String) -> Observable<ServiceModel>
    
    //return responsible entity
    func getResponsibleEntity(id: String) -> Observable<ResponsibleEntity>
    //return service process
    func getServiceProcess(id: String) -> Observable<ProcessModel>
    
    func getUserJourneys(userId:Int) -> Observable<[UserJourneyModel]>
    //return hamburger menu governments entities
    func getGovernmentEntities(q:String?,aolId: String?,category:String?) -> Observable<[GovernmentEntity]>
    
    //return filter categories
    func getGovernmentEntitiesFilter() -> Observable<[GovernmentEntityFilter]>
    
    //return popup content
    func getServicePopupContent(popupType:PopupType,id: String) -> Observable<ServicePopupModel>
    //submit service feedback
    func submitFeedback(comments:String,helpful:Bool,id: String)-> Observable<EmptyResponse>
}

protocol ApiSupportServicesType{
    func getPopularSearchMessages(userId:Int) -> Observable<[String]>
    func getTrendingSearchMessages(userId:Int) -> Observable<[String]>
    func searchForMessages(userId:Int, query: String) -> Observable<[String]>
    func getFaqs(pageId: Int, cat: Int , aspect: Int , info: [Int]) -> Observable<[AspectOfLifeServiceFaqsModel]>

    func getMyMessages(filter:MessagesFilter?)->Observable<[MessageModel]>
    func updateMessage(messages:[MessageModel])->Observable<[MessageModel]>
    
    
    func getMsgs(withpage page: Int, andSearchedText searchedtxt: String?, andSorted sortedParam: String?,andFilterBy filter:String?, andMsgType msgType:String) -> Observable<MsgResponse>
    func getMessageDetails(id:Int)-> Observable<MessageModel>
    func downloadFile(url:String,forName name:String,fileFormat: String) -> Observable <URL?>
    func putMessage(msgId:String,comment:String) -> Observable<FeedbackResponseModel>
    func putMessage(msgId:String,mediaURL:URL) -> Observable<FeedbackResponseModel>
    func getApproveCaseQuestions(id:Int) -> Observable<[MessageModel.QuestionModel]>
    func approveCase(id:Int,answers:[Int]) -> Observable<EmptyResponse>
    func rejectCase(id:Int,ratingId:Int,comment:String) -> Observable<EmptyResponse>
}


protocol ApiMessageServicesTypes{
    // returns the message types allowed along with a selected Id if a string is provided
    func getMessageTypes(query: String) -> Observable<MessageTypesResponse>
    // returns the message ID
    func submitMessage(message: AddMessageRequestModel) -> Observable<String>
    
    func getStaticMessageTypes() -> MessageTypesResponse
}

protocol ApiMyCommunityTypes:ApiFilterServiceType{
    
    func getPolls() -> Observable<[MyCommunityModel.Poll]>

    func submitPoll(poll:MyCommunityModel.Poll) -> Observable<[MyCommunityModel.Poll.Answer]>
    
    func getEvents(eventsFilterApplyModel: EventsFilterApplyModel) -> Observable<[Event]>
    func getTrendingEvents()->Observable<[String]>
    func getSearchEvents(q:String)->Observable<[String]>
    func getCoCreate() -> Observable<MyCommunityModel.CoCreate>
    func collaborate(collaborate: CollaborateModel)-> Observable<EmptyResponse>
    func getPublicHolidays(filterObject:EventsFilterApplyModel?) -> Observable<[Event]>
    func searchFor(query:String, type:Int) -> Observable<[String]>
    func getPressReleases() -> Observable<PressReleasesModel>
    func downloadPDF(url:String,forName name:String) -> Observable <URL?>
}

protocol FeedbackServiceType{
    func sendFeedBack(emotion: String, msg: String) -> Observable<FeedbackResponseModel>
}

protocol ApiEntityServices {
    func getAllCategoryServices(withid id:String) -> Observable<[ServiceCategoryEntityModel]>
    func getSearchServices(q:String?, type:Int)->Observable<[String]>
}

protocol ApiNewsServiceType {
    func getAllNews(withpage page:Int, andSearchedText searchedtxt:String?, andSorted sortedParam:String?) -> Observable<NewsApiResponse>
    func getSearchNews(q:String?, type:Int)->Observable<[String]>
}

protocol GovernmentEntityInformationServiceType{
    func getGovernmentEntityInformation(entityId: String) -> Observable<GovernmentEntityInformationModel>
}


protocol ApiMyEventsServiceType:ApiFilterServiceType {
    func getMyEvents(filterObject:EventsFilterApplyModel?) -> Observable<[Event]>
    func searchFor(query:String, type:Int) -> Observable<[String]>
}

protocol ApiMediaCenterServiceType {
    func getMediaReleases() -> Observable<MediaCenterModel>
    func downloadPDF(url:String,forName name:String) -> Observable <URL?>
}

protocol ApiFilterServiceType{
    func getFilterCategories() -> Observable<[Event.Category]>
    func getFilterLocations(q:String) ->Observable<[String]>
}

protocol ApiFactsAndFiguresType{
    func getHtmlContent() -> Observable<FactsAndFiguresModel>
}

protocol ApiInitiativesServiceType:ApiFilterServiceType {
    func getInitiatives(filterObject:EventsFilterApplyModel?) -> Observable<[Event]>
    func searchFor(query:String, type:Int) -> Observable<[String]>
}
protocol ApiSettingsServicesTypes{
    func getTerms() -> Observable<TermsModel>
    func getPrivacy() -> Observable<PrivacyModel>
}

protocol ApiEnablePushNotificationServiceType {
    func getEnablePushNotificationState() -> Observable<EnablePushNotificationModel>
    func setPushNotificationStatus(isEnabled: Bool, Reminder: Int?) -> Observable<FeedbackResponseModel>
}

protocol MyDocumentServiceType {
    func getActiveDocuments(callObject:DoumentsApiCallModel) -> Observable<DocumentsModel>
    func getExpiredDocuments(callObject:DoumentsApiCallModel) -> Observable<DocumentsModel>
    func log(document:MyDocumentModel) -> Void
    
    func getDocument(id:Int) -> Observable<MyDocumentModel>
    func updateDocument(document:MyDocumentModel) -> Observable<FeedbackResponseModel>
    func shareDocument(documentID: Int,subject:String,to:String) -> Observable<FeedbackResponseModel>
}



protocol MyPaymentsServiceType {
    func getMyPayments(page:Int, paymentType : PaymentType , search : SearchObject? , filter : String? , sort : String?) ->Observable<PaymentResponseModel>
}
protocol ApiGlobalSearchType{
    func getSearchSuggestions(query: String) -> Observable<SearchSuggestions>
    func getSearchResults(query: String) -> Observable<SearchResults>
}
