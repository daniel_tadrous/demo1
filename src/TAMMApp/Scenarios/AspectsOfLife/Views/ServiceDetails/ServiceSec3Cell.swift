//
//  AolServiceFaqCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ServiceSec3Cell: UITableViewCell {
    
    var OnCellClicked: (()->Void)?
    
    @IBOutlet weak var arrow: IconLabel!
    @IBOutlet weak var label: LocalizedLabel!
    @IBAction func btnClick(_ sender: UIButton) {
       OnCellClicked?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
  
}
