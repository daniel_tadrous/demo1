//
//  Dimensions.swift
//  TAMMApp
//
//  Created by kerolos on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
class Dimensions{
    static var aspectOfLifeCellCornerRadious: CGFloat = 5
    static var journyCellCornerRadious: CGFloat = 5
    static var roundCornerRadiousForIntroGetStarted: CGFloat = 17
    static var resizedRoundCornerRadiousForIntroGetStarted: CGFloat {
        get{
            return UIFont.fontSizeMultiplier * roundCornerRadiousForIntroGetStarted
        }
    }
    
    static let introArEnMovingAnemationDurations = 0.7
    
    static let animationDuration1 = 0.5
    
    static let bounceHeight:CGFloat = 30
    static let bounceAnimationTime = 0.5
    
    
    static let talkToUsTextLimit: Int = 2000
    static let yourIdeaTextLimit: Int = 300
}

