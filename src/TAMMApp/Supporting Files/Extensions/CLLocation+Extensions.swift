//
//  CLLocation+Extensions.swift
//  TAMMApp
//
//  Created by Daniel on 6/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import CoreLocation
enum EAddressPart{
    case SubThoroughfare,Thoroughfare,Locality,AdministrativeArea
}
extension CLLocation{
    func getAddress(completion:@escaping ((String?) -> Void),parts:[EAddressPart]) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(self) { (placemarks, error) in
            if let placemark = placemarks?.first,
                let subThoroughfare = placemark.subThoroughfare,
                let thoroughfare = placemark.thoroughfare,
                let locality = placemark.locality,
                let administrativeArea = placemark.administrativeArea {
                var address = ""//subThoroughfare + " " + thoroughfare + ", " + locality + " " + administrativeArea
                for part in parts{
                    switch part{
                    case  .SubThoroughfare:
                        address = address == "" ? subThoroughfare : address + " \(subThoroughfare)"
                    case .Thoroughfare:
                        address = address == "" ? thoroughfare : address + " \(thoroughfare)"
                    case .Locality:
                        address = address == "" ? locality : address + " \(locality)"
                    case .AdministrativeArea:
                        address = address == "" ? administrativeArea : address + " \(administrativeArea)"
                    }
                }
                //placemark.addressDictionary
                
                return completion(address)
                
            }
            completion(nil)
        }
    }
}
