//
//  TammService.swift
//  TAMMApp
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

enum PopupType:String{
    case documents = "documents",phones = "phones",location = "location",conditions = "conditions", apps = "apps",fees = "fees",related_services = "related-services"
}
class ServicePopupModel: Mappable {
    var id: Int?
    var serviceId: Int?
    var content: String?
    var addressLine1: String?
    var addressLine2: String?
    var lat: Double?
    var lng: Double?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id            <- map["id"]
        serviceId         <- map["serviceId"]
        content  <- map["content"]
        addressLine1  <- map["addressLine1"]
        addressLine2  <- map["addressLine2"]
        lat  <- map["lat"]
        lng  <- map["lng"]
    }
    
}
