//
//  MediaCenterPressReleaseCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class MediaCenterPressReleaseCell: UITableViewCell {
    
    @IBOutlet weak var downloadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var downloadlabel: UILabel!
    @IBOutlet weak var pdfLabel: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var allView: UIView!
    
    @IBOutlet weak var cellView: UIView!
    var url:String!
    var fileName:String = ""
    var downloadClosure: ((String,String)->())?
    var release:PressRelease!
    
    @IBOutlet weak var sizeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        commonInit()
    }
    
    @IBAction func downloadAction(_ sender: UIButton) {
        release.isDownloading = true
        downloadIndicator.startAnimating()
        btnDownload.isEnabled = false
        downloadClosure?(url,fileName)
    
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(){
        downloadlabel.text = "\u{e024}"
        pdfLabel.setTitle("\u{e023}", for: .normal)
        selectionStyle = .none
        applyInvertColors()
    }
    
    func applyInvertColors() {
        downloadIndicator.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
        downloadlabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        pdfLabel.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        pdfLabel.setTitleColor(#colorLiteral(red: 0, green: 0.4316659272, blue: 0.526139617, alpha: 1).getAdjustedColor(), for: .normal)
        titleLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        descriptionLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        dateLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        //btnDownload.setTitleColor(, for: .normal)
        allView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1).getAdjustedColor()
        cellView.backgroundColor = UIColor.white.getAdjustedColor()
        let buttonWidth = pdfLabel.bounds.height
        pdfLabel.addRoundAllCorners(radious: buttonWidth/2, borderColor: UIColor.duckEggBlueTwo.getAdjustedColor())
        sizeLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
    }

}
