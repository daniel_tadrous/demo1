//
//  UpdateprofileW.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit


class SimpleWarningpopup : UIView {
    
    var okAction: (()->Void)?
    
    var cancelAction: (()->Void)?

    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet var contentView: RoundedView!
    
    @IBOutlet weak var mainView: RoundedView!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    
    
    @IBOutlet weak var ConfirmationBtn: LocalizedButton!
    
    @IBOutlet weak var cancelBtn: LocalizedButton!
    
    
    @IBAction func confirmationAction(_ sender: Any) {
        okAction?()
    }
    
    
    @IBAction func discardAction(_ sender: Any) {
        let height = UIApplication.shared.keyWindow!.bounds.height

        // animate from current position down out of the screen
        let superView = self.superview!
        superView.isUserInteractionEnabled = false
        UIView.animate(withDuration: Dimensions.animationDuration1, animations: { [weak self] in
            self?.center = (self?.center.applying(CGAffineTransform.init(translationX: 0, y: height)))!
            }, completion: { (finished) in
                // remove the view and put back the floating menu
                self.removeFromSuperview()
                self.displayFloatingMenu()
                superView.isUserInteractionEnabled = true
        })
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        // commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("SimpleWarningPopup", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        
        ConfirmationBtn.addRoundCorners(radious: ConfirmationBtn.frame.height/2)
        cancelBtn.addRoundCorners(radious: cancelBtn.frame.height/2)
        cancelBtn.BorderColor = UIColor.turquoiseBlue
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        ConfirmationBtn.addRoundCorners(radious: ConfirmationBtn.frame.height/2)
        cancelBtn.addRoundCorners(radious: cancelBtn.frame.height/2)
    }
}
