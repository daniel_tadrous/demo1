//
//  SideMenuItemTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 4/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class SideMenuItemTableViewCell: UITableViewCell {

    static let generalIdentifier = "SideMenuItemTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
