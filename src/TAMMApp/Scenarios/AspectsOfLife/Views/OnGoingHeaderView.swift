//
//  OnGoingHeaderView.swift
//  TAMMApp
//
//  Created by Daniel on 5/31/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class OnGoingHeaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var headerLabel: LocalizedLabel!
    var widthConstraint:NSLayoutConstraint!
    
    static func loadView() -> OnGoingHeaderView{
        let theView = Bundle.main.loadNibNamed("OnGoingHeaderView", owner: nil, options: nil)!.first as! OnGoingHeaderView
        
        theView.widthConstraint = NSLayoutConstraint(for: theView, withWidth: 375)
        theView.addConstraint(theView.widthConstraint)
        
        return theView
        
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        headerLabel.setNeedsLayout()
        self.setNeedsLayout()
        headerLabel.layoutIfNeeded()
        self.layoutIfNeeded()
        
        
    }
    
    override func didMoveToSuperview() {
        self.sizeToFit()
    }
}
