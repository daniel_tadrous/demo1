//
//  Test2ViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol ListItemType: class {
    var text : String {get set}
    var state : Bool {get set}
    
    
}

class ListItem : ListItemType{
    var text: String = ""
    
    var state: Bool = false
    
    
}


class Test2ViewController: UIViewController, TestStoryboardLodable {

    var viewModel: Test2ViewModel!
    var items: [ListItemType] = (0..<100).map({ (i) -> ListItemType  in return ListItem() })
    @IBOutlet weak var ourTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ourTableView.register(UINib(nibName: OurTableViewCell.generalIdentifier, bundle: nil), forCellReuseIdentifier: OurTableViewCell.generalIdentifier)
        ourTableView.rowHeight = UITableViewAutomaticDimension
        ourTableView.estimatedRowHeight = 124
        ourTableView.dataSource = self
        ourTableView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Test2ViewController : UITableViewDataSource {
  
    // MARK: - Table view data source
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OurTableViewCell.generalIdentifier, for: indexPath) as! OurTableViewCell
        let item = items[indexPath.row]
        if item.state == false {
            cell.hideExtra()
        } else {
            cell.showExtra()
        }
        cell.delegate = self
        return cell
    }
}

extension Test2ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        item.state = true
        let cell = tableView.cellForRow(at: indexPath) as! OurTableViewCell
        cell.showExtra()
        tableView.reloadRows(at: [indexPath], with: .automatic)

//        tableView.beginUpdates()
//        tableView.endUpdates()
        contentDidChange(cell: cell)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        item.state = false
        let cell = tableView.cellForRow(at: indexPath) as! OurTableViewCell
        cell.hideExtra()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        contentDidChange(cell: cell)
    }
}

//delegate
extension Test2ViewController: OurTableViewCellDelegate {
    func contentDidChange(cell: OurTableViewCell) {
        UIView.animate(withDuration: 1) {
            self.ourTableView.beginUpdates()
            self.ourTableView.endUpdates()
        }
    }
}
