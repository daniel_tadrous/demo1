//
//  AolDetailsViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

protocol AolDetailsViewControllerDelegate: class{
    func aspectOfLifeSubtopicDidFinish()
}
protocol AspectsOfLifeTopicsTableViewDelegate: class{
    func selectedTopic(topic:AspectOfLifeServiceTopicViewData)
}
class AolDetailsViewController: TammViewController, AolDetailsStoryboardLodable {
    
    @IBOutlet weak var aspectOfLifeLogo: UICircularImage!
    @IBOutlet weak var aspectOfLifeDescription: UILabel!
    @IBOutlet weak var myTableView: UITableView!
    
    public weak var delegate: AolDetailsViewControllerDelegate?
    public weak var selectDelegate: AspectsOfLifeTopicsTableViewDelegate?
    public var viewModel: AolDetailsViewModel?
    
    
    let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.dataSource = self
        myTableView.delegate = self
        self.title = self.viewModel?.aspectTitle.value
        setupUI()
        setupHooks()
        
        self.setNavigationBar(title:self.title!)
        self.myTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.setNavigationBar()
    }
    func setupUI(){
        
    }
    func setupHooks(){
        
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] aspectDetail in
                if(aspectDetail == nil){
                    //TODO: should do somting ????
                }else{
                    self.myTableView.reloadData()
                    self.aspectOfLifeDescription.text = aspectDetail!.aspectDescription
                    if(aspectDetail!.aspectIconUrl != nil){
                        ImageCachingUtil.loadImage(url: aspectDetail!.aspectIconUrl!, dispatchInMain: true
                            , callBack: { (url, image) in
                                self.aspectOfLifeLogo.Image = image
                                
                        })
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    var serviceTopics: [AspectOfLifeServiceTopicViewData] {
        get{
            return viewModel?.viewData.value?.aspectServiceTopics ?? []
        }
    }
    
}

extension AolDetailsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceTopics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AolDetailsTableViewCell.globalIdentifier) as! AolDetailsTableViewCell
        let item = serviceTopics[indexPath.row]
        cell.backgroundColor = UIColor.clear
        
        cell.label?.text = item.serviceTopicTitle
        
        //        if indexPath.row > 20 {
        //            if indexPath.row % 2 == 0{
        //
        //
        //        cell.label?.text =  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore." //
        //            }else {
        //                cell.label?.text = item?.serviceName
        //            }
        //        }
        //        else{
        //            cell.label?.text = item?.serviceName
        //        }
        return cell
    }
    
    
}

extension AolDetailsViewController: UITableViewDelegate{

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //TODO: navigate to AolSubTopicsViewController
        let item = serviceTopics[indexPath.row]
        self.selectDelegate?.selectedTopic(topic: item)
    }
}
