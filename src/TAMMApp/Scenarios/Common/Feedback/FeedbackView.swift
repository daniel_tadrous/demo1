//
//  FeedbackView.swift
//  TAMMApp
//
//  Created by kerolos on 6/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit



@IBDesignable class FeedbackView: UIView{

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var contentPopupView: UIView!
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.black
    
    @IBOutlet weak var titleLabel: LocalizedLabel!
    @IBOutlet weak var sadBtn: UIButton!
    
    @IBOutlet weak var neutralButton: UIButton!
    
    @IBOutlet weak var happyBtn: UIButton!
    @IBOutlet weak var commentTextView: LocalizedTextView!
    static func loadView() -> FeedbackView {
        let v = Bundle.main.loadNibNamed("FeedbackView", owner: nil, options: nil)?.first as! FeedbackView
        v.initializeButtons()
        return v
    }
    
    

    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
//    func createGradientLayer() {
//        let gradientLayer = CAGradientLayer()
//
//        gradientLayer.frame = self.bounds
//
//        gradientLayer.colors = [topColor, bottomColor]
//        gradientLayer.locations = [0, 1]
//
//        self.layer.addSublayer(gradientLayer)
//    }
    
    
    var viewModel: FeedbackViewModel!
    
    
    func initializeButtons(){
        let size = CGSize(width: 70, height: 70)
        let fontSize: CGFloat = 70
        let capWidth = 70
        let cropRect = CGRect(x:8, y:8, width:54, height:54)
        let resizeSize = CGSize(width: 70, height: 70)
        
        let sad = "|".tammImage(fontColor: #colorLiteral(red: 0.4274509804, green: 0.431372549, blue: 0.4274509804, alpha: 1), size: size, fontSize: fontSize)?.crop(rect: cropRect).resizeImage(targetSize: resizeSize)
        let sad_selected = "\u{e01a}".tammImage(fontColor: #colorLiteral(red: 0, green: 0.6705882353, blue: 0.7725490196, alpha: 1), size: size, fontSize: fontSize)?.crop(rect: cropRect).resizeImage(targetSize: resizeSize)
        
        let neutral = "{".tammImage(fontColor: #colorLiteral(red: 0.4274509804, green: 0.431372549, blue: 0.4274509804, alpha: 1), size: size, fontSize: fontSize)?.crop(rect: cropRect).resizeImage(targetSize: resizeSize)
        let neutral_selected = "{".tammImage(fontColor: #colorLiteral(red: 0, green: 0.6705882353, blue: 0.7725490196, alpha: 1), size: size, fontSize: fontSize)?.crop(rect: cropRect).resizeImage(targetSize: resizeSize) 
        
        let happy = ",".tammImage(fontColor: #colorLiteral(red: 0.4274509804, green: 0.431372549, blue: 0.4274509804, alpha: 1), size: size, fontSize: fontSize)?.crop(rect: cropRect).resizeImage(targetSize: resizeSize)
        let happy_selected = "\u{e01c}".tammImage(fontColor: #colorLiteral(red: 0, green: 0.6705882353, blue: 0.7725490196, alpha: 1), size: size, fontSize: fontSize)?.crop(rect: cropRect).resizeImage(targetSize: resizeSize)
        
//        sadButton.contentMode = .scaleToFill
//        neutralButton.contentMode = .scaleToFill
//        happyButton.contentMode = .scaleToFill
        
//        sadButton.imageView?.frame = sadButton.bounds
//        neutralButton.imageView?.frame = neutralButton.bounds
//        happyButton.imageView?.frame = happyButton.bounds
//
//        sadButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
//        neutralButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
//        happyButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        


        
        
       
        
        
        neutralButton.setImage(neutral, for: .normal)
        neutralButton.setImage(neutral_selected, for: .highlighted)
        neutralButton.setImage(neutral_selected, for: .selected)
        
        
        
       
        

            happyBtn.setImage(happy, for: .normal)
            happyBtn.setImage(happy_selected, for: .highlighted)
            happyBtn.setImage(happy_selected, for: .selected)
            
            sadBtn.setImage(sad, for: .normal)
            sadBtn.setImage(sad_selected, for: .highlighted)
            sadBtn.setImage(sad_selected, for: .selected)

        
        commentTextView.text = ""
        commentTextView.placeholder = L10n.additionalComments
        commentTextView.layer.borderWidth = 1.0
        commentTextView.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        
        contentPopupView.isUserInteractionEnabled = true
        
    }
    @IBAction func sadPressed(_ sender: Any) {
        happyBtn.isSelected = true
        neutralButton.isSelected = false
        sadBtn.isSelected = false
        viewModel.feedbackEmotion = .sad
    }
    @IBAction func neutralPressed(_ sender: Any) {
        happyBtn.isSelected = false
        neutralButton.isSelected = true
        sadBtn.isSelected = false
        viewModel.feedbackEmotion = .neutral
    }
    @IBAction func happyPressed(_ sender: Any) {
        happyBtn.isSelected = false
        neutralButton.isSelected = false
        sadBtn.isSelected = true
        viewModel.feedbackEmotion = .happy
    }
    
    @IBAction func closePressed(_ sender: Any) {
        dismissScreen()
       addFloatingButton()
    }
    @IBAction func cancelPressed(_ sender: Any) {
        dismissScreen()
        addFloatingButton()
    }
    
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        viewModel.feedbackText = commentTextView.text
        
        let (valid, message) = viewModel.isModelValid()
        if(!valid){
            self.parentContainerViewController?.displayToast(message: message ?? "Validation error")
        }else{
            viewModel.sendFeedback(completionHandler: {
                //self.parentContainerViewController?.displayToast(message: "Thank You")
                
                let vc = ThankYouViewController()
                    vc.initialize(title: L10n.thankYou, btnTitle: L10n.ok)
                    self.parentContainerViewController?.present(vc, animated: true, completion: nil)
                
//
//                    FeedBackThankYouViewController()
//                vc.initialize(title: L10n.thankYou, summary: "", details: "", refernceNumber: nil)
//                self.parentContainerViewController?.present(vc, animated: true, completion: nil)
                
                self.dismissScreen()
                
            }, errorHandler: {(error) in
                self.parentContainerViewController?.displayToast(message: "Error sending feedback")
            })
            
            
        }
    }
    @IBAction func anywherepressed(_ sender: Any) {
        dismissScreen()
    }
    
    func dismissScreen(){
        self.removeFromSuperview()
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        appDelegate?.addfloatingMenu()
    }
    
    func addFloatingButton() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addfloatingMenu()
    }
    
}
