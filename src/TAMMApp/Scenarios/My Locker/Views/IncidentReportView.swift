//
//  IncidentReportView.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import UICheckbox_Swift

import Photos
import RxSwift

protocol IncidentReportViewDelegate {
    func addMediaClicked()
    func addLocationClicked()
    func addLocationTextClicked(text: String?)
    func mediaIsRemoved(index:Int)
    func shouldRemoveVoiceFile(_: URL?) -> Bool

}
enum IncidentReportViewContactType{
    case email
    case sms
    case mobileApp
}
class IncidentReportView: UIView{

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var termsConditionsValidationLbl: LocalizedLabel!
    @IBOutlet weak var contactMethodValidation: LocalizedLabel!
    @IBOutlet weak var locationValidationLbl: LocalizedLabel!
    @IBOutlet weak var locationSeparatorLine: UIView!
    @IBOutlet weak var locationMicButton: UIButton!
    @IBOutlet weak var makePublicView: UIView!
    @IBOutlet weak var makePublicSwitch: UISwitch!
    @IBOutlet weak var termsConditionsLbl: UILabel!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var mapViewLabel: LocalizedLabel!
    @IBOutlet weak var mapViewImage: UIImageView!
    @IBOutlet weak var emailCheckBox: UICheckbox!
    @IBOutlet weak var mobileCheckBox: UICheckbox!
    @IBOutlet weak var incidentPlacetxt: UITammTextField!
    @IBOutlet weak var smsCheckBox: UICheckbox!
    @IBOutlet weak var termsConditionsCheckBox: UICheckbox!
    
    @IBOutlet weak var locationInfoView: LocationInfoView!
    @IBOutlet weak var mediaViewHolder: UIView!
    
    @IBOutlet weak var hasMediaView: HasMediaView?
    @IBOutlet weak var noMediaView: MediaBaseView?
    var mediaView:MediaBaseView!
    
    var messageType:Int?{
        didSet{
            _ = self.validateLocation()
        }
    }
    
    @IBOutlet weak var mediaViewHeightConstraint: NSLayoutConstraint!
    private var disposeBag = DisposeBag()

    @IBOutlet weak var locationTextMaskView: UIView!
    @IBAction func locationTextMaskPressed(_ sender: UITapGestureRecognizer) {
        delegate?.addLocationTextClicked(text: incidentPlacetxt.text)
    }
    var exceededLimitCheck = false{
        didSet{
            mediaView.exceededLimitCheck = exceededLimitCheck
        }
    }
    var notSupportedCheck = false
    @IBOutlet weak var addDetailsTextView: UITextView!
    private var message:String = ""
    public var delegate:IncidentReportViewDelegate?
    public var mediaList:[MediaViewModelData] = []{
        didSet{
            if(mediaList.count > 0){
                displayHasMediaView()
                // second call is to readjust the layout
                displayHasMediaView()
            }
            else{
               displayNoMediaView()
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        // commonInit()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let nibName = "IncidentReportView"
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self,options: nil).first as? UIView
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
        
    }
    private func commonInit(){
        self.locationTextMaskView.isUserInteractionEnabled = true
        self.mapViewImage.isUserInteractionEnabled = true
        self.mapViewImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addLocationClicked)))
        recordButton.delegate = self
        addDetailsTextView.delegate = self
        showMiniPlayer(false)
        showRecordButton(true)
        self.contactMethodValidation.text =  L10n.mustChooseContactMethod
        self.termsConditionsValidationLbl.text =  L10n.acceptTermsConditions
        self.locationValidationLbl.text = L10n.locationMandatory
        self.incidentPlacetxt.rx.observe(String.self, "text")
            .subscribe(onNext: { text in
                _ = self.validateLocation()
            })
            .disposed(by: disposeBag)
        locationInfoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addLocationClicked)))
        locationInfoView.isHidden = true
        displayNoMediaView()
    }
    
    private func validateLocation()->Bool{
        if ((self.incidentPlacetxt.text?.count) ?? 0 == 0) && self.messageType == MessageTypesId.incident.rawValue {
            //self.isValid = false
            self.locationValidationLbl.isHidden =  false
            self.locationValidationLbl.textAlignment = .right
            self.locationSeparatorLine.backgroundColor = UIColor.red
            return false
        }
        else{
            //self.isValid = true
            self.locationValidationLbl.isHidden =  true
            self.locationSeparatorLine.backgroundColor = UIColor.paleGrey
            return true
        }
    }
    func displayHasMediaView(){
        self.hasMediaView?.isHidden = false
        self.noMediaView?.isHidden = true
        self.hasMediaView?.delegate = self
        hasMediaView?.notSupportedCheck = notSupportedCheck
        hasMediaView?.exceededLimitCheck = exceededLimitCheck
        self.hasMediaView?.mediaList = self.mediaList

        mediaView = hasMediaView
        mediaView.sizeToFit()
        mediaViewHolder.sizeToFit()
        //self.layoutIfNeeded()

    }
    
    func displayNoMediaView(){
        self.hasMediaView?.isHidden = true
        self.noMediaView?.isHidden = false
        self.noMediaView?.delegate = self
        noMediaView?.notSupportedCheck = notSupportedCheck
        noMediaView?.exceededLimitCheck = exceededLimitCheck
    
        mediaView = noMediaView
        mediaView.sizeToFit()
        
        //self.layoutIfNeeded()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        mediaViewHolder.resizeToFitSubviews()
        mediaViewHeightConstraint.constant = mediaView.heightOfView
        self.mediaViewHolder.layoutIfNeeded()
        
        makeInfoPublicContainer.addRoundCorners(radious: 8, borderColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1))
        
        mapView.addRoundCorners(radious: 8)
        makePublicSwitch.layer.cornerRadius = 16.0;

        let fontSize: CGFloat = 16.0
        var fontName = "Roboto-Regular"
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        if isRTL {
            fontName = "Swissra-Normal"
        }
        let font1 = UIFont(name: fontName , size: fontSize)!
        
        let termsAndConditions = L10n.termsAndConditions
        let readTermsAndConditions = L10n.readTermsAndConditions(termsAndConditions: termsAndConditions) as NSString
        
        let termsAndConditionsRange = readTermsAndConditions.range(of: termsAndConditions as String)
        
        let attributedString = NSMutableAttributedString(string: readTermsAndConditions as String, attributes: [
            .font: font1,
            .foregroundColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1)
            ])
        attributedString.addAttributes([.foregroundColor: UIColor.turquoiseBlue], range: termsAndConditionsRange)
        termsConditionsLbl.attributedText = attributedString
    }
   
    @objc func addLocationClicked(){
        delegate?.addLocationClicked()
    }
    
    func setLocationText(_ location:String){
        incidentPlacetxt.text = location
    }
    
    func getLocationText() -> String?{
        return incidentPlacetxt.text
    }
    
    
    func getContactMethods() -> [IncidentReportViewContactType]{
        var contactMethods = [IncidentReportViewContactType]()
        if emailCheckBox.isSelected {
            contactMethods.append(.email)
        }
        if mobileCheckBox.isSelected {
            contactMethods.append(.mobileApp)
        }
        if smsCheckBox.isSelected {
            contactMethods.append(.sms)
        }
        return contactMethods
    }
    
    func isValid() -> Bool {
        var isValid = true
        if(!validateLocation()){
            isValid = false
        }
        if (getContactMethods().count < 1){
            self.contactMethodValidation.isHidden = false
            isValid = false
        }
        if(!termsConditionsCheckBox.isSelected){
           self.termsConditionsValidationLbl.isHidden = false
            isValid = false
        }
        return isValid
    }
    
    func getFormData()->MessageViewDataBase{
        var newMessage:IncidentMessageViewData = IncidentMessageViewData()
        newMessage.hasMedia = mediaView.HasMedia()
        newMessage.notifiedByEmail = emailCheckBox.isSelected
        newMessage.incidentLocation = incidentPlacetxt.text
        newMessage.locationCoordinates = getCoordinatesSelected()
        
        newMessage.notifiedByMobileApplication = mobileCheckBox.isSelected
        newMessage.notifiedByPhone = smsCheckBox.isSelected
        
        newMessage.voiceURI = voiceFileURL
        newMessage.comment = addDetailsTextView.text
        
        newMessage.isInformationPublic = makePublicSwitch.isOn
        
      
        return newMessage
    }
    func getErrorMessage()->String{
        return message
    }
    
    func getCoordinatesSelected() -> String?{
        
        //TODO: adjust coordinates
        return ""
    }
    
   
    
    @IBOutlet weak var recordButtonHoldingView: UIView!
    @IBOutlet weak var recordButton: RecordButton!
    @IBOutlet weak var makeInfoPublicContainer: UIView!
    @IBOutlet weak var miniPlayerContainer: XibView!
    @IBOutlet weak var pressAndHoldText: LocalizedLabel!
    private var voiceFileURL: URL?
    
    
    func setVoiceNoteAndComment(voiceUrl: URL?, comment: String){
        if let voiceUrl = voiceUrl{
            // consider the recoreder just recorded the voice
            recordButtonDidRecordAnAudioFile(url: voiceUrl)
            // comment is empty
        } else{
            addDetailsTextView.text = comment
            showRecordButton( comment.count < 1 )
        }
        
    }
    
    private var playerView: MiniPlayerView{
        return miniPlayerContainer.contentView as! MiniPlayerView
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        addDetailsTextView.layer.borderWidth = 1.5
        addDetailsTextView.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        addDetailsTextView.layer.cornerRadius = 4
        
        recordButtonHoldingView.layer.borderWidth = 1.5
        recordButtonHoldingView.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        recordButtonHoldingView.layer.cornerRadius = 31
    }
    
    @IBAction func emailCheckBoxValueChanged(_ sender: Any) {
        if(!emailCheckBox.isSelected){
            if(!mobileCheckBox.isSelected && !smsCheckBox.isSelected){
                contactMethodValidation.isHidden = false
            }
        }
        else{
            contactMethodValidation.isHidden = true
        }
    }
    
    @IBAction func mobileCheckBoxValueChanged(_ sender: Any) {
        if(!mobileCheckBox.isSelected){
            if(!emailCheckBox.isSelected && !smsCheckBox.isSelected){
                contactMethodValidation.isHidden = false
            }
        }
        else{
            contactMethodValidation.isHidden = true
        }
    }
    
    @IBAction func smsCheckBoxValueChanged(_ sender: Any) {
        if(!smsCheckBox.isSelected){
            if(!emailCheckBox.isSelected && !mobileCheckBox.isSelected){
                contactMethodValidation.isHidden = false
            }
        }
        else{
            contactMethodValidation.isHidden = true
        }
    }
    
    
    @IBAction func TermsConditionsCheckBoxValueChanged(_ sender: Any) {
        if(!termsConditionsCheckBox.isSelected){
            termsConditionsValidationLbl.isHidden =  false
        }
        else{
            termsConditionsValidationLbl.isHidden =  true
        }
    }
    
    fileprivate func enableAddDetailsTextfield(_ enabel: Bool = true){
        addDetailsTextView.isEditable = enabel
    }
    
    fileprivate func showRecordButton(_ show: Bool = true){
            recordButtonHoldingView.isHidden = !show
            pressAndHoldText.isHidden = !show

    }
    
    fileprivate func showMiniPlayer(_ show: Bool = true){
            miniPlayerContainer.isHidden = !show
    }
    
    func viewWillDisappear(){
        playerView.stop()
    }
}

extension IncidentReportView: RecordButtonDelegate{
    func recordButtonDidRecordAnAudioFile(url: URL) {
        enableAddDetailsTextfield(false)
        playerView.delegate = self
        voiceFileURL = url
        playerView.miniPlayerVoiceURL = voiceFileURL
        showMiniPlayer()
        showRecordButton(false)
    }
}

extension  IncidentReportView: MiniPlayerViewDelegate{
    func miniPlayerDidPressDelete() {
        showMiniPlayer(false)
        showRecordButton(true)
        enableAddDetailsTextfield(true)
        do{
            
            if delegate?.shouldRemoveVoiceFile(voiceFileURL) ?? true{
                try FileManager.default.removeItem(at: voiceFileURL!)
            }
            
            voiceFileURL = nil
            
            
        }
        catch let e{
            print(e)
        }
    }
}

extension IncidentReportView: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        
        showRecordButton( numberOfChars < 1 )
        
        // to indecate that the modification is allowed
        
        if ( numberOfChars > Dimensions.talkToUsTextLimit)
        {
            return false
        }
        return true
    }
}

extension IncidentReportView{
    func setInfoView(title:String,description:String) {
        locationInfoView.isHidden = false
        
        locationInfoView.details = description
        locationInfoView.title = title
    }
}
extension IncidentReportView : MediaViewDelegate{
    func addMediaClicked(){
        delegate?.addMediaClicked()
    }
    func mediaIsRemoved(index: Int) {
        mediaList.remove(at: index)
        delegate?.mediaIsRemoved(index: index)
        if(mediaList.count == 0){
            displayNoMediaView()
        }
    }
    
    func viewHasMedia() {
        mediaViewHeightConstraint.constant = mediaView.heightOfView
//        mediaViewHolder.layoutIfNeeded()
       // self.hasMediaView?.contentView.frame = CGRect(x: 0, y: 0, width: self.mediaViewHolder.frame.width, height: (self.hasMediaView?.heightOfView)!)
    }
    
    
}
