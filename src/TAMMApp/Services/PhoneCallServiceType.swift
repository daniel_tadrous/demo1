//
//  PhoneCallServiceType.swift
//  TAMMApp
//
//  Created by kerolos on 4/23/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation




protocol PhoneCallServiceType{
    
    func makeAPhoneCall(number:String) -> Bool
    
}
