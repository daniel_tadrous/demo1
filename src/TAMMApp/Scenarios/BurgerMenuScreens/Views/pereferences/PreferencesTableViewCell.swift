//
//  PreferencesTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 7/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class PreferencesTableViewCell: UITableViewCell {

    
    static let identifierNavigation = "navigationCell"
    static let identifierSelection = "selectionCell"
    static let identifierSwitch = "switchCell"
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var titleLabel: LocalizedLabel!
    @IBOutlet weak var selectedItemTitle: UILabel?
    
    @IBOutlet weak var arrowLabel: IconLabel?
    @IBOutlet weak var switchView: UISwitch?
    
    private var swichStateChanged: ((Bool)->())?
    
    func set( title: String, selectedItem: String? = nil, switchItemState: Bool?, switchStateChanged: ((Bool)->())? = nil ){
        titleLabel.text = title
        if let selectedItemTitle = selectedItemTitle, let selectedItem = selectedItem {
            selectedItemTitle.text = selectedItem
        }
        if let switchView = switchView, let switchItemState = switchItemState {
            switchView.isOn = switchItemState
            
        }
        
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        let value = sender.isOn
        if  let swichStateChanged = swichStateChanged{
            swichStateChanged(value)
        }
    }
    
}
