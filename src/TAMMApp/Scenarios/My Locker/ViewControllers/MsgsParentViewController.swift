//
//  MsgsParentViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MsgsParentViewControllerDelegate {
    func openActiveMsgs()
    func openClosed()
}

class MsgsParentViewController: ContainerViewController ,MsgsParentStoryboardLoadable  {

    var delegate:MsgsParentViewControllerDelegate?
    
    @IBOutlet weak var fullLineView: UIView!
    @IBOutlet weak var activeMsgsBtn: LocalizedSelectableButton!
    @IBOutlet weak var closedMsgsBtn: LocalizedSelectableButton!
    
    fileprivate var activeMsgsSelected = false
    fileprivate var closedMsgsSelected = false
    
    @IBOutlet weak var underLine: UIView!
    fileprivate var underLineColor = UIColor(red: 0, green: 89, blue: 113, alpha: 1)
    
    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    
    fileprivate var selectedArabicFont = UIFont(name: "Swissra-Bold",size: 18)
    fileprivate var unSelectedArabicFont = UIFont(name: "Swissra-Normal",size: 18)
    
    fileprivate var selectedFontColor = UIColor(hexString: "005971")
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activeMsgsBtn.originalColor = unSelectedFontColor
        activeMsgsBtn.selectedColor = selectedFontColor
        
        activeMsgsBtn.RTLSelecttedFont = "Swissra-Bold"
        closedMsgsBtn.RTLSelecttedFont = "Swissra-Bold"
        
        if L10n.Lang == "en" {
            activeMsgsBtn.originalFont = unSelectedFont
            activeMsgsBtn.fontProxy = unSelectedFont
            
            activeMsgsBtn.selectedFontReference = selectedFont
            
            closedMsgsBtn.originalFont = unSelectedFont
            closedMsgsBtn.fontProxy = unSelectedFont
            closedMsgsBtn.selectedFontReference = selectedFont
        }else {
            activeMsgsBtn.originalFont = unSelectedFont
            activeMsgsBtn.fontProxy = unSelectedArabicFont
            
            activeMsgsBtn.selectedFontReference = selectedArabicFont
            
            closedMsgsBtn.originalFont = unSelectedArabicFont
            closedMsgsBtn.fontProxy = unSelectedArabicFont
            closedMsgsBtn.selectedFontReference = selectedArabicFont
        }

        closedMsgsBtn.originalColor = unSelectedFontColor
        closedMsgsBtn.selectedColor = selectedFontColor
        
        self.setNavigationBar(title: L10n.MyMessages.title, willSetSearchButton: true)
        if MessagesViewModel.msgTypeStatic == MessageType.closed{
            selectButtonAction(forButton: closedMsgsBtn, isActiveMsgs: false)
        }else{
            selectButtonAction(forButton: activeMsgsBtn, isActiveMsgs: true)
        }
    }
    
    @IBAction func activeBtnAction(_ sender: LocalizedSelectableButton) {
        //        if L10n.Lang == "en" {
        self.selectButtonAction(forButton: sender, isActiveMsgs: true)
        //        }else {
        //            self.selectButtonAction(forButton: sender, isEvents: false)
        //        }
    }
    
    @IBAction func closedMsgsAction(_ sender: LocalizedSelectableButton) {
        
        self.selectButtonAction(forButton: sender, isActiveMsgs: false)

    }
    
    fileprivate func selectButtonAction(forButton button:LocalizedSelectableButton, isActiveMsgs:Bool ) {
        activeMsgsSelected = false
        closedMsgsSelected = false
        activeMsgsBtn.isButtonSelected = false
        closedMsgsBtn.isButtonSelected = false
        
        if isActiveMsgs, !activeMsgsSelected{
            if L10n.Lang == "en" {
                activeMsgsSelected = true
                button.isButtonSelected = true
            }else {
                activeMsgsSelected = false
                button.isButtonSelected = false
                closedMsgsSelected = true
                button.isButtonSelected = true
            }
            
            //moveUnderLine(to: button)
            
            delegate?.openActiveMsgs()
        }
        else if !isActiveMsgs, !closedMsgsSelected{
            
            if L10n.Lang == "en" {
                closedMsgsSelected = true
                button.isButtonSelected = true
            }else {
                closedMsgsSelected = false
                button.isButtonSelected = false
                activeMsgsSelected = true
                button.isButtonSelected = true
            }
            //moveUnderLine(to: button)
            
            delegate?.openClosed()
        }
        
    }
    
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        //         if L10n.Lang == "en" {
        if button == activeMsgsBtn {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x
        }
        else if button == closedMsgsBtn{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }
        //        }else {
        //            if button == eventsButton {
        //                underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        //                UIView.animate(withDuration: 0.35, animations: {
        //                    self.view.layoutIfNeeded()
        //                })
        //                underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        //            }
        //            else if button == publicHolidaysButton{
        //                underLine.frame.origin.x = fullLineView.frame.origin.x
        //                UIView.animate(withDuration: 0.35, animations: {
        //                    self.view.layoutIfNeeded()
        //                })
        //                underLine.frame.origin.x = fullLineView.frame.origin.x
        //            }
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        if activeMsgsSelected {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
            
            
        else if closedMsgsSelected{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    


}
