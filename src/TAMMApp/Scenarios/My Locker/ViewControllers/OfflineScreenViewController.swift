//
//  OfflineScreenViewController.swift
//  TAMMApp
//
//  Created by Daniel on 5/27/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class OfflineScreenViewController: TammViewController, OfflineScreenStoryboardLodable {
    @IBOutlet weak var wifiIconLabel: UILabel!
    
    @IBOutlet weak var talkToUsBtn: UITammResizableButton!
    
    @IBOutlet weak var label1: LocalizedLabel!
    
    @IBOutlet weak var label2: LocalizedLabel!
    
    @IBOutlet weak var label3: LocalizedLabel!
    
    weak var delegate:OfflineScreensDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ErrorView.show = false
        setUpUi()
        
    }
    func setUpUi(){
        self.wifiIconLabel.text = "\u{e013}"
        self.talkToUsBtn.addRoundCorners(radious: 17)
        self.talkToUsBtn.layer.borderWidth = 1
        self.talkToUsBtn.layer.borderColor = UIColor.turquoiseBlue.cgColor
        self.hideFloatingMenu()
    }
    @IBAction func talkToUsClickHandler(_ sender: LocalizedButton) {
        AppCoordinator.isNewSession = false
        delegate?.showNewDraft(isFromOfflineScreen: true)
    }
}
