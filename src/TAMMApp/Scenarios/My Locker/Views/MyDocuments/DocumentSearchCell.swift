//
//  DocumentSearchCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/27/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class DocumentSearchCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
