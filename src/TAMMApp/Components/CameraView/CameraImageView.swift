//
//  CameraImageView.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/7/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
protocol CameraImageViewDelegte {
    func imageIsRemoved(img:UIImage , index:Int)
}
class CameraImageView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cancelView: UICircularImage!
    public var delegate:CameraImageViewDelegte!
    @IBInspectable public var index = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    fileprivate func commonInit (){
        Bundle.main.loadNibNamed("CameraImageView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        self.cancelView.isHidden = true
        self.imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeImage)))
        self.cancelView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeImage)))
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        borderView.addRoundAllCorners(radious: 5, borderColor: UIColor.init(white: 1, alpha: 0.2))
        self.imageView.addRoundAllCorners(radious: 5)
        self.imageView.clipsToBounds = true
    }
    func addImage(image:UIImage){
        if(self.imageView.image != nil){
            self.imageView.image = image
            borderView.clipsToBounds = true
            self.cancelView.isHidden = false
            
            self.borderView.addRoundAllCorners(radious: 5, borderColor: UIColor.white)
            return
        }
        self.imageView.alpha = 0
        self.imageView.image = image
        borderView.clipsToBounds = true
        UIView.animate(withDuration: 0.5, animations: {
            self.imageView.alpha = 1
        }) { (finish) in
            self.cancelView.isHidden = false
            self.borderView.addRoundAllCorners(radious: 5, borderColor: UIColor.white)
        }
        
    }
    func clearImage(){
        self.imageView.image = nil
        self.cancelView.isHidden = true
         borderView.addRoundAllCorners(radious: 5, borderColor: UIColor.init(white: 1, alpha: 0.2))
    }
    @objc func removeImage(){
        let image = self.imageView.image
        UIView.animate(withDuration: 0.5, animations: {
            self.imageView.alpha = 0
        }) { (finish) in
            self.imageView.alpha = 1
            self.cancelView.isHidden = true
            self.borderView.addRoundAllCorners(radious: 5, borderColor: UIColor.init(white: 1, alpha: 0.2))
            self.delegate.imageIsRemoved(img:image!, index:self.index)
        }
        
    }
    
}
