//
//  ManageViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

import RxSwift

struct DocumentUrl {
    var isSelected = false
    var myUrl:URL
}

class ManageViewController: UIViewController , ManageStoryboardLoadable , UIDocumentInteractionControllerDelegate{
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var ManageTableView: UITableView!
    var allMedia:[DocumentUrl] = [DocumentUrl]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var deleteStackView: UIStackView!
    var viewModel:ManageViewModel!
    let disposeBag:DisposeBag = DisposeBag()
     let formatter = NumberFormatter()
    var selectedItems:Variable<[Int]?>  = Variable(nil)
    
    let cellIdentifier = "cellIdentifier"
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: "Manage", willSetSearchButton: true)
        // Do any additional setup after loading the view.
        selectedItems.value = [Int]()
        setupBinding()
        deleteBtn.isHidden = true
        self.ManageTableView.register(UINib(nibName: "ManageDocumentsTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        setupNumberFormatter()
    }
    
    fileprivate func setupNumberFormatter() { 
        formatter.numberStyle = .decimal
        if L10n.Lang == "ar" {
            formatter.locale = Locale(identifier: "az-Arab")
        }else {
            formatter.locale = Locale(identifier: "en")
        }
    }
    
    func setupBinding(){
        viewModel.allMedia.asObservable().subscribe(onNext: { [weak self] allSavedMedia in
            if let mediaArray = allSavedMedia {
                self?.allMedia = mediaArray
                self?.ManageTableView.reloadData()
            }
        } ).disposed(by: disposeBag)
        
        selectedItems.asObservable().subscribe(onNext: { (selectedItems) in
            if selectedItems != nil && (selectedItems?.count)! > 0 {
                self.deleteBtn.isHidden = false
            }else {
                self.deleteBtn.isHidden = true
            }
            }).disposed(by: disposeBag)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func deleteAction(_ sender: Any) {
        var urlsToDelete = [URL]()
        var selectedIndexPathes:[IndexPath] = [IndexPath]()
 
        for item in selectedItems.value! {
            urlsToDelete.append(allMedia[item].myUrl)
            selectedIndexPathes.append(IndexPath.init(row: item, section: 0))
            
        }
        
        let indiciesSorted:[Int] = selectedItems.value!.sorted().reversed()
        for item in indiciesSorted {
            print(item)
            allMedia.remove(at: item)
        }
        self.ManageTableView.deleteRows(at: selectedIndexPathes, with: .automatic)

        for url in urlsToDelete {
            deleteFile(withUrl:url )
        }
        selectedItems.value?.removeAll()
        //viewModel.getMediaData()
    }
    
}



extension ManageViewController : UITableViewDelegate, UITableViewDataSource {
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allMedia.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openUrl(withUrl: allMedia[indexPath.row].myUrl)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myDocument = allMedia[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ManageDocumentsTableViewCell
        cell.fileNameLabel.text = myDocument.myUrl.pathComponents.last
        let fileSize = myDocument.myUrl.sizePerMB()
        
       cell.descriptionLabel.text = formatter.string(from: NSNumber(value: fileSize))! + " " + L10n.megaByte
        //viewModel.getDisplayedNumber(number: fileSize)
        let tapped = UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
  
        tapped.numberOfTapsRequired = 1
        cell.clickedView.addGestureRecognizer(tapped)
        
        
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(handleLongPress))
        longPress.minimumPressDuration = 0.5
        cell.clickedView.addGestureRecognizer(longPress)

        cell.viewOverlay.isHidden = !myDocument.isSelected
        
        
//        let nameLabel = cell.viewWithTag(1) as! UILabel
//        let deleteBtn = cell.viewWithTag(2) as! UIButton
      //  nameLabel.text = allMedia[indexPath.row].pathComponents.last
       // deleteBtn.addTarget(self, action: #selector(deleteFile(_:)), for: .touchUpInside)
        return cell
        }

    @objc func handleLongPress(recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == UIGestureRecognizerState.began {
            let point = recognizer.location(in: ManageTableView as! UIView)
            let indexPath: IndexPath! = ManageTableView.indexPathForRow(at: point)
            var index = indexPath
            
            if (selectedItems.value?.isEmpty)! {
                allMedia[(index?.row)!].isSelected = true
                selectedItems.value?.append((index?.row)!)
            }else{
                if allMedia[(index?.row)!].isSelected {
                    allMedia[(index?.row)!].isSelected = false
                    let indexToRemove = selectedItems.value?.index(of: indexPath.row)
                    
                    selectedItems.value?.remove(at: indexToRemove!)
                }else {
                    allMedia[(index?.row)!].isSelected = true
                    selectedItems.value?.append((index?.row)!)
                }
            }
            ManageTableView.reloadRows(at: [index!], with: .none)
        }
        
       
    }
    
    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        
        let point = recognizer.location(in: ManageTableView as! UIView)
        let indexPath: IndexPath! = ManageTableView.indexPathForRow(at: point)
        var index = indexPath
        
        if (selectedItems.value?.isEmpty)! {
            openUrl(withUrl: allMedia[(index?.row)!].myUrl)
        }else{
            if allMedia[(index?.row)!].isSelected {
                let indexRemoved = selectedItems.value?.index(of: (index?.row)!)
                selectedItems.value?.remove(at: indexRemoved!)
            }else {
               selectedItems.value?.append((index?.row)!)
            }
            allMedia[(index?.row)!].isSelected = !allMedia[(index?.row)!].isSelected
            ManageTableView.reloadRows(at: [index!], with: .none)
        }
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        self.appDelegate.addfloatingMenu()
    }
    
//    @objc func deleteFile(_ sender:UIButton){
//        let point = sender.convert(CGPoint.zero, to: ManageTableView as UIView)
//        let indexPath: IndexPath! = ManageTableView.indexPathForRow(at: point)
//        let removedURL = allMedia[indexPath.row].myUrl
//        viewModel.deleteUrl(removedUrl:removedURL)
//    }
    
    func deleteFile(withUrl removedurl:URL){
        viewModel.deleteUrl(removedUrl: removedurl)
    }
    
    
    func openUrl(withUrl url:URL){
        let docController:UIDocumentInteractionController = UIDocumentInteractionController.init(url: url)
        docController.delegate = self
        self.appDelegate.removeFloatingMenu()
        docController.presentPreview(animated: true)
    }

    
    }

