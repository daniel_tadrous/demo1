//
//  Toast.swift
//  Coptic Planner
//
//  Created by Innuva Mac mini 4 on 3/19/17.
//  Copyright © 2017 Innuva. All rights reserved.
//

import Foundation
import UIKit
class Toast:NSObject{
    private var toastMessage:String = ""
    public var font:UIFont = UIFont.systemFont(ofSize: 15)
    init(message:String) {
        super.init()
        toastMessage = message
    }
    public func Show(){
        let keyWindow = UIApplication.shared.keyWindow
         DispatchQueue.main.async(){
        keyWindow?.endEditing(true)
        }
        let ToastLabel = UILabel()
        let attributes = [NSAttributedStringKey.font: font]
        let msg = NSMutableAttributedString(string: toastMessage, attributes: attributes)
        ToastLabel.attributedText = msg
        ToastLabel.numberOfLines = 0
        ToastLabel.textColor = UIColor.white
        ToastLabel.backgroundColor = UIColor.gray.withAlphaComponent(0.9)
        ToastLabel.textAlignment = NSTextAlignment.center
        let size  = CGSize(width: 1000  , height: 1000)
        let bottomOffset:CGFloat = 50
        let frame = msg.boundingRect(with: size, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let heightOfToast:CGFloat = frame.height + 30
        let widthOfToast:CGFloat = frame.width + 30
        ToastLabel.frame = CGRect(x: (keyWindow?.screen.bounds.width)!/2 - (widthOfToast/2), y: (keyWindow?.screen.bounds.height)! - heightOfToast - bottomOffset, width: widthOfToast, height: heightOfToast)
        ToastLabel.layer.cornerRadius = 10;
        ToastLabel.layer.masksToBounds = true;
        keyWindow?.addSubview(ToastLabel)
        UIView.animate(withDuration: 5.0, animations: {
            ToastLabel.alpha = 0.0;
        }) { (finished)->Void in
            ToastLabel.removeFromSuperview()
        }
        
    }
    
    public func Show(withTime time:Double){
        let keyWindow = UIApplication.shared.keyWindow
        DispatchQueue.main.async(){
            keyWindow?.endEditing(true)
        }
        let ToastLabel = UILabel()
        let attributes = [NSAttributedStringKey.font: font]
        let msg = NSMutableAttributedString(string: toastMessage, attributes: attributes)
        ToastLabel.attributedText = msg
        ToastLabel.numberOfLines = 0
        ToastLabel.textColor = UIColor.white
        ToastLabel.backgroundColor = UIColor.gray.withAlphaComponent(0.9)
        ToastLabel.textAlignment = NSTextAlignment.center
        let size  = CGSize(width: 1000  , height: 1000)
        let bottomOffset:CGFloat = 50
        let frame = msg.boundingRect(with: size, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let heightOfToast:CGFloat = frame.height + 30
        let widthOfToast:CGFloat = frame.width + 30
        ToastLabel.frame = CGRect(x: (keyWindow?.screen.bounds.width)!/2 - (widthOfToast/2), y: (keyWindow?.screen.bounds.height)! - heightOfToast - bottomOffset, width: widthOfToast, height: heightOfToast)
        ToastLabel.layer.cornerRadius = 10;
        ToastLabel.layer.masksToBounds = true;
        keyWindow?.addSubview(ToastLabel)
        UIView.animate(withDuration: time, animations: {
            ToastLabel.alpha = 0.0;
        }) { (finished)->Void in
            ToastLabel.removeFromSuperview()
        }
        
    }
}
