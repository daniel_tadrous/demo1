//
//  ProfileViewController.swift
//  TAMMApp
//
//  Created by kerolos on 7/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class ProfileViewModel {

    
    private let userVar: Variable<User?> = Variable( User.getUser())
    private var disposeBag = DisposeBag()

    private func setHooks(){
        
        User.userObservable.subscribe(onNext: { [unowned self] (user) in
            self.userVar.value = user
        }).disposed(by: disposeBag)
        
        
        userVar.asObservable().subscribe(onNext: { [unowned self] (user) in
            self.imageVar.value = user?.getUserImage() ??  UIImage(named: "profileimage")!
            self.fullNameVar.value = self.getTheName(user: user)
            self.emailVar.value = user?.email ?? "no@current.email"
            if user?.mobile == nil || (user?.mobile?.trimmingCharacters(in: .whitespaces).isEmpty)!{
                self.phoneVar.value =  L10n.noPhoneReg
            }
            else {
                self.phoneVar.value = (user?.mobile)!
            }
        }).disposed(by: disposeBag)
    }
    private var fullNameVar: Variable<String> = Variable("")
    public var fullName: Observable<String> {
        return fullNameVar.asObservable()
    }
    private func getTheName(user: User?) -> String{
        let isAr = L102Language.currentAppleLanguage().contains("ar")
        if(isAr){
            return getNameAR(user: user) ?? "No Current Name"
        }
        else{
            return getNameEN(user: user) ?? "No Current Name"
        }
    }
    
    private func getNameAR(user: User?) -> String?{
        var name: String? = user?.fullNameAR 
        if name == nil || (name?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            name = user?.computedFullNameAR
        }
        if name == nil || (name?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            name = user?.fullNameEN
        }
        if name == nil || (name?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            name = user?.computedFullNameEN
        }
        return name
    }
    
    
    private func getNameEN(user: User?) -> String?{
        var name: String? = user?.fullNameEN
        if name == nil || (name?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            name = user?.computedFullNameEN
        }
        if name == nil || (name?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            name = user?.fullNameAR
        }
        if name == nil || (name?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            name = user?.computedFullNameAR
        }
        return name
    }
    
    
    private var imageVar: Variable<UIImage> = Variable(UIImage(named: "profileimage")!)
    public var image: Observable<UIImage>{
        return imageVar.asObservable()
    }

    private var emailVar: Variable<String> = Variable("no@current.email")
    public var email: Observable<String>{
        return emailVar.asObservable()
    }
    
    private var phoneVar: Variable<String> = Variable("No Phone registered")
    public var phone:Observable<String>{
        return phoneVar.asObservable()
    }
    
    private var authenticationService: AuthenticationService
    
    init(authenticationService: AuthenticationService){
        self.authenticationService = authenticationService
        setHooks()

    }
    
    
    func openSmartpassAppOrWebsite() {
        self.authenticationService.openSmartpassAppOrWebsite()
    }
    
    func updateUserInfo(){
        self.authenticationService.updateUserInformation()
    }
    
    
    
}
