//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift

class ConfirmationView : UIView{
   
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mainView: UIView!

    @IBOutlet weak var bodyLabel: LocalizedLabel!
    
    @IBOutlet weak var cancelBtn: LocalizedButton!
    
    @IBOutlet weak var titleLabel: LocalizedLabel!
    
    @IBOutlet weak var okBtn: LocalizedButton!
    private var okClickHandler: (()->Void)?
    
    static var draftsUpdatedDelegate: DraftsUpdatedDelegate?
    
    var title:String?
    var message:String?
    var okBtnTitle:String?
    var cancelBtnTitle:String?
    
    @IBAction func okClickHandler(_ sender: LocalizedButton) {
        self.removeFromSuperview()
        self.displayFloatingMenu()
        self.okClickHandler?()
        removeView()
        DiscardChangesView.draftsUpdatedDelegate?.onDraftsUpdated()
    }
    
    @IBAction func cancelClickHandler(_ sender: LocalizedButton) {
        removeView()
    }
   
    func removeView() {
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    init(frame: CGRect, okClickHandler: @escaping ()->Void) {
        super.init(frame: frame)
        self.okClickHandler = okClickHandler
        commonInit()
    }
    init(frame: CGRect,title:String ,message:String,okClickHandler: @escaping ()->Void) {
        super.init(frame: frame)
        self.okClickHandler = okClickHandler
        self.title = title
        self.message = message
        commonInit()
    }
    
    init(frame: CGRect,title:String ,message:String,CancelBtnTitle:String,confirmationBtnTitle:String,okClickHandler: @escaping ()->Void) {
        super.init(frame: frame)
        self.okClickHandler = okClickHandler
        self.title = title
        self.message = message
        self.okBtnTitle = confirmationBtnTitle
        self.cancelBtnTitle = CancelBtnTitle
        
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "ConfirmationView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        setupHooks()
        setupUiData()
    }
   
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
      
        
    }
    func show() -> ConfirmationView{
        UIApplication.shared.keyWindow!.addSubview(self)
        UIApplication.shared.keyWindow!.bringSubview(toFront: self)
        let height = UIApplication.shared.keyWindow!.bounds.height
        self.center = self.center.applying(CGAffineTransform.init(translationX: 0, y: height))
        // animate the view Up into the screen
        UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
        UIView.animate(withDuration: Dimensions.animationDuration1, animations: {
            self.center = self.center.applying(CGAffineTransform.init(translationX: 0, y: -height))
        }, completion: { (finished) in
            UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
        })
        
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)

        return self
    }
    func setupUiData(){
        self.cancelBtn.layer.borderWidth = 1
        self.cancelBtn.layer.borderColor = UIColor.turquoiseBlue.cgColor
        self.cancelBtn.addRoundCorners(radious: 17)
        
        self.okBtn.addRoundCorners(radious: 17)
        if self.message != nil{
            self.bodyLabel.text = self.message
        }
        if self.title != nil{
            self.titleLabel.text = self.title
        }
        
        if self.okBtnTitle != nil{
            okBtn.setTitle(okBtnTitle, for: .normal)
        }
        if self.cancelBtnTitle != nil{
            cancelBtn.setTitle(cancelBtnTitle, for: .normal)
        }
        

        
    }

  
}
