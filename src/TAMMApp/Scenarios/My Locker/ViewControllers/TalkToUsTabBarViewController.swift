//
//  TalkToUsTabBarViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
protocol TalkToUsTabBarViewControllerDelegate: class{
    func openTalkToUsNewTab(isFromOfflineScreen:Bool)
    func openTalkToUsNew(item:String,typeId:Int,isFromOfflineScreen:Bool)
    func openTalkToUsEditMode(draftId: String, isFromOfflineScreen:Bool)
    func openDrafts(isFromOfflineScreen:Bool)
}

class TalkToUsTabBarViewController: ContainerViewController , TalkToUsTabBarStoryboardLodable{
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnNew: LocalizedSelectableButton!
    @IBOutlet weak var btnDrafts: LocalizedSelectableButton!
    static var selectedMessageTypeStatic:Int = -1
    var selectedMessageType: Int {
        get{
            return TalkToUsTabBarViewController.selectedMessageTypeStatic
        }
        set{
            TalkToUsTabBarViewController.selectedMessageTypeStatic = newValue
        }
    }
    var viewModel:TalkToUsTabBarViewModel!
    public var delegate: TalkToUsTabBarViewControllerDelegate?
    fileprivate let selectedButtonBorder:CGFloat = 4
    fileprivate let stackViewBottomBorder:CGFloat = 1
    fileprivate let unSelectedFont:UIFont = UIFont.init(name: "CircularStd-Medium", size: 18)!
    fileprivate let selectedButtonBorderColor:String = "#10596F"
    fileprivate let stackViewBottomBorderColor:String = "#CAC8D1"
    fileprivate var layer:CALayer?
    static var isFromOfflineScreenStatic:Bool = false
    var isFromOfflineScreen:Bool{
        get{
            return TalkToUsTabBarViewController.isFromOfflineScreenStatic
        }
        set{
            TalkToUsTabBarViewController.isFromOfflineScreenStatic = newValue
        }
    }
    fileprivate var btnDraftsCurrentColor:String = "#151331"
    fileprivate var btnDraftsCurrentFont:UIFont = UIFont.init(name: "CircularStd-Medium", size: 18)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnDrafts.originalFont = unSelectedFont
        btnNew.originalFont = unSelectedFont
        btnDrafts.fontProxy = unSelectedFont
        btnNew.fontProxy = unSelectedFont
        
        
        btnDrafts.RTLFont = "Swissra-Normal"
        btnDrafts.RTLSelecttedFont = "Swissra-Bold"
        
        
        btnNew.RTLFont = "Swissra-Normal"
        btnNew.RTLSelecttedFont = "Swissra-Bold"
        
        
        btnDrafts.selectedFontReference = UIFont.init(name: "Swissra-Normal-Bold", size: 18)
        btnNew.selectedFontReference = UIFont.init(name: "Swissra-Bold", size: 18)
        
        btnDrafts.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnNew.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnDrafts.selectedColor = UIColor.init(hexString: "#11596F")
        btnNew.selectedColor = UIColor.init(hexString: "#11596F")
        
        btnDrafts.originalColor = UIColor.init(hexString: "#151331")
        btnNew.originalColor = UIColor.init(hexString: "#151331")
        switch viewModel.lunchMode {
        case .NEW, .EMPTY:
            buttonIsSelected(button:  btnNew, lunchMode: viewModel.lunchMode, draftId: nil)
        case .DRAFT, .EDIT:
            buttonIsSelected(button: btnDrafts , lunchMode: viewModel.lunchMode, draftId: viewModel.lunchMode == .DRAFT ? nil : TalkToUsDraftsViewController.itemStatic.draftId )
        }
        
        if isFromOfflineScreen{
            self.navigationItem.setHidesBackButton(true, animated:true);
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addBorderToStackView()
        self.setNavigationBar(title: L10n.talk_to_us_header)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)       
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if( !appDelegate.floatingMenuIsVisible()){
            appDelegate.addfloatingMenu()
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
    @IBAction func btnNewClicked(_ sender: LocalizedSelectableButton) {
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        buttonIsSelected(button: sender, lunchMode: .NEW, draftId: nil)
    }
    @IBAction func btnDraftsClickes(_ sender: LocalizedSelectableButton) {
        TalkToUsTabBarViewModel.LunchModeStatic = .DRAFT
        buttonIsSelected(button: sender, lunchMode: .DRAFT, draftId: nil)
    }
    func btnNewClickedInEditMode(draftId: String) {
        TalkToUsTabBarViewModel.LunchModeStatic = .EDIT
        buttonIsSelected(button: btnNew, lunchMode: .EDIT , draftId: draftId)
    }
    
    func selectDraftButton() {
        buttonIsSelected(button: btnDrafts, lunchMode: .DRAFT, draftId: nil)
    }
    
    
    fileprivate func addBorderToStackView(){
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: stackView.frame.height - stackViewBottomBorder, width: stackView.frame.width, height: stackViewBottomBorder)
        
        bottomBorder.backgroundColor = UIColor.init(hexString: stackViewBottomBorderColor).cgColor
        
        stackView.layer.addSublayer(bottomBorder)
    }
    
    
    
    
    
    fileprivate func buttonIsSelected(button:LocalizedSelectableButton, lunchMode:LunchMode, draftId: String?){
        MapParentViewController.selectedPlaceStatic = nil
        if(layer != nil){
            layer?.removeFromSuperlayer()
        }
        btnDrafts.isButtonSelected = false
        btnNew.isButtonSelected = false
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: button.frame.height - selectedButtonBorder, width: button.frame.width, height: selectedButtonBorder)
        bottomBorder.backgroundColor = UIColor.init(hexString: selectedButtonBorderColor).cgColor
        
        button.layer.addSublayer(bottomBorder)

        
        button.isButtonSelected = true
        layer = bottomBorder
        switch lunchMode {
        case .DRAFT:
            delegate?.openDrafts(isFromOfflineScreen: isFromOfflineScreen)
        case .NEW:
            delegate?.openTalkToUsNew(item:viewModel.getItemName(),typeId: selectedMessageType,isFromOfflineScreen: isFromOfflineScreen)
            //delegate?.openTalkToUsNewTab()
        case .EMPTY:
            delegate?.openTalkToUsNewTab(isFromOfflineScreen: isFromOfflineScreen)
        case .EDIT:
            delegate?.openTalkToUsEditMode(draftId: draftId!,isFromOfflineScreen: isFromOfflineScreen)
            print("edit")
        }
        
    }

}
extension TalkToUsTabBarViewController:TalkToUsDraftsViewDelegate{
    func numberOfDrafts(number: Int) {
       // let string = "Drafts(" + String(number) + ")"
        //let btnDraftsString = NSAttributedString(string: string, attributes: [NSAttributedStringKey.font:btnDraftsCurrentFont ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: btnDraftsCurrentColor)])
        //btnDrafts.setAttributedTitle(btnDraftsString, for: .normal)
        //btnDrafts.setTitle(string, for: .normal)
    }
    
    
}




