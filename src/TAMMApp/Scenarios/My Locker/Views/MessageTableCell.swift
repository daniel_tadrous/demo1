//
//  TalkToUsDraftsTableCell.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
class MessageTableCell: UITableViewCell {
   
    @IBOutlet weak var orangeBg: UIView?
    @IBOutlet weak var deleteIcon: UILabel?
    
    @IBOutlet weak var archiveIcon: UILabel!
    
    
    var viewData: Any?
    var indexPath:IndexPath!
    var isEditMode: Bool!
    @IBOutlet weak var messageView: MessageView!
    var onDeleteClick: (()->Void)?
    var onArchiveClick: (()->Void)?
    var onSelectClick: ((Bool)->Void)?
    @IBAction func deleteBtnClickHandler(_ sender: UIButton) {
        onDeleteClick?()
    }
    
    @IBAction func archiveBtnClickHandler(_ sender: UIButton) {
        onArchiveClick?()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    private func commonInit(){
        self.messageView.addRoundCorners(radious: 5)
        self.orangeBg?.addRoundAllCorners(radious: 5)
        self.messageView.checkbox.onSelectStateChanged = {
            (checkbox,selected) in
            self.onSelectClick?(selected)
            
        }
        self.deleteIcon?.text = "\u{e009}"
        self.archiveIcon?.text = "\u{e012}"
    }
}
