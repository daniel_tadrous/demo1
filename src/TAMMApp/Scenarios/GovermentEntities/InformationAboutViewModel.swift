
//
//  InformationAboutViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 7/9/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

protocol EntitiesInformationServiceTimeViewDataType{
    var title: String { get }
    var label1: String { get }
    var label2: String { get }
    
}

protocol EntitiesInformationAboutViewDataType {
    var images: [URL] { get }
    var logo: URL { get }
    var header: String { get }
    var details: String { get }
    // font Icon for now
    var timeLogo: String { get }
    var officeTime: EntitiesInformationServiceTimeViewDataType { get }
    var publicTime: EntitiesInformationServiceTimeViewDataType { get }
    
}




struct EntitiesInformationServiceTimeViewData: EntitiesInformationServiceTimeViewDataType{
    var title: String
    var label1: String
    var label2: String
    
}

struct EntitiesInformationAboutViewData: EntitiesInformationAboutViewDataType{
    var images: [URL]
    var logo: URL
    var header: String
    var details: String
    // font Icon for now
    var timeLogo: String
    var officeTime: EntitiesInformationServiceTimeViewDataType
    var publicTime: EntitiesInformationServiceTimeViewDataType
    
    static func getSample() -> EntitiesInformationAboutViewData{
        return EntitiesInformationAboutViewData(images: [URL(string: "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg" )!,
                                      URL(string: "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg" )!,
                                      URL(string: "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg" )!],
                             logo: URL(string: "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg" )!,
                             header: "DED - Department of Economic Development test",
                             details: "test The media items which you uploaded for the incident are unclear, hence your case is being put on hold. Kindly provide new images/video so we can assign it to the concerned department after review.",
                             timeLogo: "v",
                             officeTime: EntitiesInformationServiceTimeViewData(title: "Office hours",
                                                             label1: "Sun - Thu",
                                                             label2: "8:00 AM - 3:00 PM"),
                             publicTime: EntitiesInformationServiceTimeViewData(title: "Public Service hours",
                                                             label1: "Sun - Thu",
                                                             label2: "8:00 AM - 2:00 PM"))
    }
    
}

protocol EntitiesInformationMapViewDataType{
    var image: URL { get }
    var lat: Double { get }
    var lng: Double { get }
    var details: String { get }
}
protocol EntitiesInformationOfficeViewDataType {
    var header: String { get }
    var details: String { get }
    var mapData: EntitiesInformationMapViewDataType { get }
    var mobile: String { get }
    var email: String { get }
    var website: String { get }
    
    var expanded: Bool? { get set}
}

struct EntitiesInformationMapViewData: EntitiesInformationMapViewDataType{
    var lat: Double
    
    var lng: Double
    
    var image: URL
    var details: String
}

struct EntitiesInformationOfficeViewData: EntitiesInformationOfficeViewDataType{
    var expanded: Bool?
        
    var header: String
    var details: String
    var mapData: EntitiesInformationMapViewDataType
    var mobile: String
    var email: String
    var website: String
    
    
    
    static func getSample() -> EntitiesInformationOfficeViewData{
        return EntitiesInformationOfficeViewData( expanded: false, header: "Head Office", details: "Al Salam Street, Baniyas Towers, Behind Abu Dhabi Municipility", mapData: EntitiesInformationMapViewData(lat: 0, lng: 0, image: URL(string:"https://image.freepik.com/free-icon/apple-logo_318-40184.jpg")!, details: "some map data to help open the map"), mobile: "012222222333", email: "www@www.www", website: "https://wwww.google.com")
    }
}


class InformationPageViewData{
    public var aboutViewData: EntitiesInformationAboutViewDataType! = EntitiesInformationAboutViewData.getSample()
    public var officeViewData: [EntitiesInformationOfficeViewDataType]! = [EntitiesInformationOfficeViewData.getSample(), EntitiesInformationOfficeViewData.getSample(), EntitiesInformationOfficeViewData.getSample()]
    init(aboutViewData: EntitiesInformationAboutViewDataType, officeViewData: [EntitiesInformationOfficeViewDataType] ){
        self.aboutViewData = aboutViewData
        self.officeViewData = officeViewData
    }
}

class EntitiesInformationViewModel{
    private var disposeBag = DisposeBag()
    private var dataVariable: Variable<InformationPageViewData?> = Variable(nil)
    private var model: GovernmentEntityInformationModel?{
        didSet{
            if let model = model{
                dataVariable.value = getViewData(model: model)
            }
        }
    }
    
    public var data: Observable<InformationPageViewData?> {
        get{
            return dataVariable.asObservable()
        }
    }
    
    
    var entitiyId: String?{
        didSet{
            getData()
        }
    }
    
    private func getData(){
        if let entitiyId = entitiyId{
            governmentInfoService.getGovernmentEntityInformation(entityId: entitiyId).subscribe(onNext: { (model) in
                self.model = model
            }, onCompleted: {
                
            }).disposed(by: disposeBag)
        }
    }
    private var governmentInfoService: GovernmentEntityInformationServiceType!
    init(governmentInfoService:GovernmentEntityInformationServiceType){
        self.governmentInfoService = governmentInfoService
    }
    
    
    func getViewData( model:GovernmentEntityInformationModel) -> InformationPageViewData?{
        let oficeHours = model.officeHours?.split(separator: ":", maxSplits: 1).map({ (str) -> String in
            str.trimmingCharacters(in: .whitespaces)
        })
        let publicHours = model.publicServiceHours?.split(separator: ":", maxSplits: 1).map({ (str) -> String in
            str.trimmingCharacters(in: .whitespaces)
        })
        
        let images = (model.images ?? []).map { (str) -> URL in
            return URL(string:str)!
        }
        
//            { (str: String) -> URL? in
//            return URL(string:str)
//        } as! [URL]
        
        let logo = URL(string: model.icon ?? "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg" )
        
        let aboutViewData = EntitiesInformationAboutViewData(images: images, logo: logo! , header: model.name ?? "", details: model.description ?? "",
                                                             timeLogo: "v",
                                                             officeTime: EntitiesInformationServiceTimeViewData(title: L10n.GovernmentEntities.officeHours,
                                                                                                                label1: oficeHours?[0] ?? "",
                                                                                                                label2: oficeHours?[1] ?? "") ,
                                                             publicTime: EntitiesInformationServiceTimeViewData(title: L10n.GovernmentEntities.publicHours,
                                                                                                                label1: publicHours?[0] ?? "",
                                                                                                                label2: publicHours?[1] ?? ""))
        let officeViewData = model.addresses?.map({ (address: GovernmentEntityInformationModel.Address) ->  EntitiesInformationOfficeViewData in
            let data = EntitiesInformationOfficeViewData(expanded: false, header: address.place! , details: address.address!,
                                                         mapData:EntitiesInformationMapViewData(lat: address.lat!, lng: address.lng!, image: URL(string:address.map!)!, details: "") ,
                                                         mobile: address.phone!, email: address.email!, website: address.website!)
            
            return data
        }) ?? []
        
        return InformationPageViewData(aboutViewData: aboutViewData, officeViewData: officeViewData)
        
    }
    
}

