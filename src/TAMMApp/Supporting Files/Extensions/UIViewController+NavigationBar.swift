//
//  UIViewController+HasFloatingButtun.swift
//  TAMMApp
//
//  Created by kerolos on 4/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

protocol HasFloatingMenu: class{
    func displayFloatingMenu()
}
extension UIViewController {
    
    
    func setNavigationBar(title:String, willSetSearchButton: Bool = true, backTitleString: String? = nil ){
        //self.navigationController?.navigationBar.frame =  CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 77)
        if willSetSearchButton {
          initializeSearchBtn()
        }
        
        var backFont:UIFont = UIFont.init(name: "CircularStd-Bold", size: 18)!
        var titleFont:UIFont = UIFont.init(name: "CircularStd-Bold", size: 22)!
        if(L102Language.currentAppleLanguage().contains("ar")){
            backFont = UIFont.init(name: "Swissra-Normal", size: 18)!
            titleFont = UIFont.init(name: "Swissra-Bold", size: 22)!
        }
        
        var backTitle = backTitleString
        
        
        if backTitle == nil {
             backTitle = "Back"
            if(L102Language.currentAppleLanguage().contains("ar")){
                backTitle = "رجوع"
            }

            self.navigationController?.navigationBar.backItem?.backBarButtonItem = UIBarButtonItem(title: backTitle, style: .plain, target: self, action:nil)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font: backFont], for: .normal)

        }
        

        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: backTitle, style: .plain, target: self, action:nil)
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font: backFont], for: .normal)
        
        
        
        
        //self.navigationController?.navigationBar.items
        self.navigationItem.title = title
        let textAttributes: [NSAttributedStringKey: Any] = [.foregroundColor:UIColor.white.getAdjustedColor() , .font: titleFont]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#13102B").getAdjustedColor()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white.getAdjustedColor()
        
        
    }
    private func initializeSearchBtn(){
        let rightButtonItem = UIBarButtonItem.init(
            title: "e",
            style: .plain,
            target: self,
            action: #selector(searchBtnClick)
        )
        self.navigationItem.rightBarButtonItem = rightButtonItem
    self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font:UIFont.init(name: "tamm", size: 27)!], for: .normal)
    }
    @objc func searchBtnClick(){
        let vc  = GlobalSearchViewController()
        self.present(vc, animated: true, completion: nil)
    }
    func setBackTitleForNextPage(backTitleString:String?, willSetSearchButton: Bool = true ){
        if willSetSearchButton {
           initializeSearchBtn()
        }
        var backFont:UIFont = UIFont.init(name: "CircularStd-Bold", size: 18)!
        var titleFont:UIFont = UIFont.init(name: "CircularStd-Bold", size: 22)!
        if(L102Language.currentAppleLanguage().contains("ar")){
            backFont = UIFont.init(name: "Swissra-Normal", size: 18)!
            titleFont = UIFont.init(name: "Swissra-Bold", size: 22)!
        }
        
        var backTitle = backTitleString
        
        
        if backTitle == nil {
            backTitle = "Back"
            if(L102Language.currentAppleLanguage().contains("ar")){
                backTitle = "رجوع"
            }
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: backTitle, style: .plain, target: self, action:nil)
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font: backFont], for: .normal)
        
    }

    func displayBlockingToast(message:String, duration: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            self.present(alert, animated: true)
        }
        // duration in seconds
        //        let duration: Double = duration
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    
    
}

extension UIResponder {
    func displayToast(message:String){
        Toast(message:message).Show()
    }
    func displayToast(message:String, Fortime time:Double){
        Toast(message:message).Show(withTime: time)
    }
    
}

