//
//  MessagesFilter.swift
//  TAMMApp
//
//  Created by Daniel on 7/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class MessagesFilter{
    var tag: String?
    var isTagged: Bool?
    var isStarred: Bool?
    var isArchived: Bool?
}
