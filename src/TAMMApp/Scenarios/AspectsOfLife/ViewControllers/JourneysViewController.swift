//
//  JourneysViewController.swift
//  TammUI
//
//  Created by Marina.Riad on 4/15/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import UIKit
import RxSwift

class JourneysViewController: TammViewController, JourneysStoryboardLodable {
    
    let disposeBag = DisposeBag()
    
    public var viewModel:MyJourneysViewModel!
    fileprivate let reuseIdentifier = "AspectsOfLife"
    @IBOutlet weak var searchText: UITammTextField!
    @IBOutlet weak var myJourneysView: AspectsOfLifeTableView!
    fileprivate let HeaderTitleFrame = CGRect(x: 15, y: 0, width: UIScreen.main.bounds.width - 15, height: 40)
    fileprivate var OnGoingLabelText: String { get { return L10n.ongoing_journeys } }
    fileprivate var JourneysLabelText: String { get { return L10n.explore_journeys } }
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.searchText.addTarget(self, action: #selector(searchTextChanged), for: .editingChanged)
        self.myJourneysView.tableView.backgroundView = UIView();
        self.myJourneysView.tableView.backgroundView?.backgroundColor = UIColor.clear
        myJourneysView.tableView.backgroundColor = UIColor.clear

        setupBinding()
        
//        self.myJourneysView.AspectsOfLifeHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: myJourneysView.HeaderHeight))
//        self.myJourneysView.OnGoingHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: myJourneysView.HeaderHeight))
        
        
        let journeysView = OnGoingHeaderView.loadView()
        journeysView.headerLabel.text = JourneysLabelText
        
        let onGoingJourneysView = OnGoingHeaderView.loadView()
        onGoingJourneysView.headerLabel.text = OnGoingLabelText
        
        
        self.myJourneysView.aspectsOfLifeHeaderView =  journeysView
        self.myJourneysView.onGoingHeaderView = onGoingJourneysView
        self.myJourneysView.aspectsOfLife = viewModel.getJourneys()
        self.myJourneysView.ongoingAspectsOfLife = viewModel.getOnGoingJourenys()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        myJourneysView.tableView.frame = myJourneysView.bounds
        myJourneysView.tableView.backgroundView?.backgroundColor = UIColor.clear
         searchText.addRoundCorners(radious: 24)
        
    }
    
    func setupBinding(){
        viewModel.journeysVar.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext:{ [unowned self] journeys in
                self.perforMUIUpdates(journeys:journeys)
        }).disposed(by:disposeBag)
    }
    
    func perforMUIUpdates(journeys:[AspectOfLifeViewData]){
        //TODO: please tell me how to perform the UI updates
        self.myJourneysView.aspectsOfLife = self.viewModel.getJourneys()
        self.myJourneysView.ongoingAspectsOfLife = self.viewModel.getOnGoingJourenys()
        self.myJourneysView.tableView.reloadData()
    }
}
