//
//  PollsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class PressReleasesViewModel {
    
    private(set) var viewData: Variable<PressReleasesModel?> = Variable(nil)
    fileprivate var apiService: ApiMyCommunityTypes!
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiMyCommunityTypes) {
        self.apiService = apiService
        requestDetails()
    }
    
  
    
    
    fileprivate func requestDetails(){
        
        apiService.getPressReleases()
            .subscribe(onNext: { [unowned self] pressRe in
                self.viewData.value = pressRe
            }).disposed(by: disposeBag)
       
    }
    func downloadPDF(url:String, name:String,onCompletion:@escaping (URL)->Void) {
        apiService.downloadPDF(url: url, forName: name).subscribe(onNext: { (downloaded) in
            if downloaded != nil{
                onCompletion(downloaded!)
            }
        }).disposed(by: disposeBag)
    }
}
