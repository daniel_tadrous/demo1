//
//  BurgerMenuCoordinator.swift
//  TAMMApp
//
//  Created by Daniel on 7/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift


enum BurgerMenuCoordinatorItem{
    case home
    case information
}

protocol BurgerMenuCoordinatorDelegate: class{
    func burgerMenuCoordinatorDidChangeLanguage()
}

class BurgerMenuCoordinator: NavigationCoordinator {
    func start() {
        initialize()
    }
    weak var delegate: BurgerMenuCoordinatorDelegate?
    var coordinators: [BurgerMenuCoordinatorItem: Coordinator] = [:]
    var navigationController: UINavigationController
    
    
    // MARK: - Properties
    var tabBarController:UITabBarController
    let container: Container
    //    var vc:UIViewController?
    //let authenticationService : AuthenticationService
    //var delegate: MyCommunityCoordinatorDelegate?
    
    init(container: Container, myNavigationController: UINavigationController, tabBarControlor:UITabBarController) {
        self.container = container
        self.navigationController = myNavigationController
        self.tabBarController = tabBarControlor
        // self.authenticationService = authenticationService
    }
//    init(container: Container, myNavigationController: UINavigationController) {
//        self.container = container
//        self.navigationController = myNavigationController
//    }
   
    private func initialize(setBarHidden: Bool = true){
        //navigationController.pushViewController(vc, animated: true)
        self.navigationController.setNavigationBarHidden(setBarHidden, animated: false)
        let navigationController = tabBarController.selectedViewController as? UINavigationController
        self.navigationController = navigationController!
    }
    
    func startGovernmentEntities(){
        coordinators = [:]
        let vc = container.resolveViewController(GovernmentEntitiesViewController.self)
        vc.delegate = self
        vc.viewModel.start.value = true
        navigationController.setViewControllers([vc], animated: true)
    }
    
//    func startMediaCenter(){
//        let vc = container.resolveViewController(NewsAndPressReleasesViewController.self)
//        vc.selectedTap = .NEWS
//        self.navigationController.pushViewController(vc, animated: true)
//    }
    func startNewsScreen(){
        let vc = container.resolveViewController(NewsViewController.self)
        vc.viewModel?.newsPage.value = 1
        //vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    private func openInformationPage(entitiyId: String){
        let coord = GovernmentEntitiesCoordinator(container: container, navigationController: navigationController, tabBarControlor: tabBarController)
        coordinators[.information] = coord
        coord.entitiyId = entitiyId
        coord.start()
        
    }

    func startPreferences(){
        coordinators = [:]
        let vc = container.resolveViewController(PreferencesViewController.self)
        vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func startProfile(previousViewController: UIViewController){
        previousViewController.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
        let vc = container.resolveViewController(ProfileViewController.self)

        vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func startPushNotificationScreen(previousViewController: UIViewController){
        previousViewController.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
        let vc = container.resolveViewController(EnablePushNotificationViewController.self)
        vc.delegate = self
        vc.viewModel.ScreenOpened.value = 1
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    func startLanguageSwitchPage(previousViewController: UIViewController){
//        previousViewController.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
        let vc = container.resolveViewController(LanguageSwitchViewController.self)
        vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    func startAccessibilityPage(previousViewController: UIViewController){
        let vc = container.resolveViewController(AccessibilityViewController.self)
        //vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func startAbout(previousViewController: UIViewController){
        previousViewController.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
        let vc = container.resolveViewController(AboutViewController.self)
        
        vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func startMediaSettings(previousViewController: UIViewController){
        previousViewController.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
        let vc = container.resolveViewController(MediaSettingsViewController.self)
        vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
}
extension BurgerMenuCoordinator: GovernmentEntitiesViewControllerDelegate{
    func openGovernmentEntityInformation(id: String) {
        openInformationPage(entitiyId: id)
    }
}
extension BurgerMenuCoordinator: PreferencesViewControllerDelegate{
    func preferencesWantsToGoToProfilePage() {
        
    }
    
    func preferencesWantsToGoToLanguageSwitchPage() {
        
    }
    
    func preferencesWantsToGoToAccessibilityPage() {
        
    }
    
    func preferencesWantsToGoToEnablePushNotificationPage() {
        
    }
    
    func preferencesWantsToGoToAboutPage() {
        
    }
    
    func preferencesWantsToGoToMediaSettings() {
        
    }
    
    
}

extension BurgerMenuCoordinator: ProfileViewControllerDelegate{
}

extension BurgerMenuCoordinator: EnablePushNotificationDelegate{
    func openSetReminderScreen() {
        
    }
}

extension BurgerMenuCoordinator: LanguageSwitchViewControllerDelegate{
    func languageSwitchViewControllerDidChangeLanguage() {
        delegate?.burgerMenuCoordinatorDidChangeLanguage()
    }
}

extension BurgerMenuCoordinator: AboutViewControllerDelegate{
    func goToPrivacyPolicy() {
        let vc = container.resolveViewController(HtmlViewController.self)
        
        vc.viewModel = PrivacyViewModel(apiService: container.resolve(ApiSettingsServicesTypes.self)!)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func goToTermsOfUse() {
        let vc = container.resolveViewController(HtmlViewController.self)
        
        vc.viewModel = TermsViewModel(apiService: container.resolve(ApiSettingsServicesTypes.self)!)
        navigationController.pushViewController(vc, animated: true)
    }
    
    
}

extension BurgerMenuCoordinator: MediaSettingsViewControllerDelegate {
    
    
    func openManageScreen() {
        let vc = container.resolveViewController(ManageParentViewController.self)
        vc.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
  
}

extension BurgerMenuCoordinator : ManageParentViewControllerDelegate {
    func openManageMedia() {
        
    }
    
    func openManageDocuments() {
        
    }
}



