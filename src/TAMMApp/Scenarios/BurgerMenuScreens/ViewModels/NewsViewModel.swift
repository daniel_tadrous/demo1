//
//  NewsViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class NewsViewModel :  SearchViewModelDelegate {

    let sortasc:String = "asc"
    let sortdesc:String = "desc"
    
    var mySearchViewModel:SeacrhViewModel?
    var newsPage:Variable<Int?> = Variable(nil)
    var searchedText:Variable<String?> = Variable(nil)
    var totalPages:Int?
    var apiService:ApiNewsServiceType
    var disposeBag = DisposeBag()
    var newsList : Variable<[NewsModel]?> = Variable(nil)
    var sortedText:Variable<String?> = Variable(nil)
    var isToGetTrending: Variable<Bool?> = Variable(nil)
    var trendingVar: Variable<[String]?> = Variable(nil)
    var queryResultVar: Variable<[String]?> = Variable(nil)
    var currentQueryVar: Variable<String?> = Variable(nil)

    init(apiService: ApiNewsServiceType) {
        self.apiService = apiService
        //super.init()
        setupBinding()
        mySearchViewModel = SeacrhViewModel()
    }
    
    
    
    private func setupBinding() {
        newsPage.asObservable().subscribe( { _ in
            self.requestNews()
        }).disposed(by: disposeBag)
        
        searchedText.asObservable().subscribe( { _ in
            self.requestNews()
        }).disposed(by: disposeBag)
        
        sortedText.asObservable().subscribe( { _ in
            self.requestNews()
        }).disposed(by: disposeBag)
        
        
        isToGetTrending.asObservable().subscribe(onNext: { (go:Bool?) in
            if go != nil{
                self.getTrending()
            }
        }).disposed(by: disposeBag)
        
        
        currentQueryVar.asObservable().subscribe(onNext: { (descriptionText) in
            if descriptionText != nil {
                self.getQueryResults(q:descriptionText!)
            }
        }).disposed(by: disposeBag)
    }
    
     func getQueryResults(q:String){
        apiService.getSearchNews(q: q, type:1)
                  .subscribe(onNext:{
                    [unowned self] popArr in
                    self.queryResultVar.value = popArr
                  }).disposed(by: disposeBag)
    }
    
    
    
    private func requestNews() {
                            if newsPage.value != nil  {
                                apiService.getAllNews(withpage: newsPage.value!, andSearchedText: searchedText.value, andSorted: sortedText.value)
                                          .asObservable()
                                    .subscribe(onNext:{ [weak self] newsResponse in
                                         self?.newsList.value = newsResponse.news
                                         self?.totalPages = newsResponse.pageCount
                                        print("\(String(describing: self?.newsList))")
                                        print("Hello from View Model")
                                        }).disposed(by: disposeBag)
                            }
    }
    
    func prepareDateToShow(date:String)->(day:String,month:String,year:String)
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000"
        
        let dateFormatterDay = DateFormatter()
        dateFormatterDay.dateFormat = "dd"
        
        let dateFormatterMonth = DateFormatter()
        dateFormatterMonth.dateFormat = "MMMM"
        
        let dateFormatterYear = DateFormatter()
        dateFormatterYear.dateFormat = "yyyy"
        
        if let date = dateFormatterGet.date(from: date){
            print(dateFormatterDay.string(from: date))
            print(dateFormatterMonth.string(from: date))
            print(dateFormatterYear.string(from: date))
            return (dateFormatterDay.string(from: date),dateFormatterMonth.string(from: date),dateFormatterYear.string(from: date))
        }
        else {
            print("There was an error decoding the string")
            return ("","","")
        }
    }
    
    func isLastNewsPage() -> Bool {
        if let allPagesCount = totalPages {
            if newsPage.value == allPagesCount {
                return true
            }else{
                //page is not last page
                return false
            }
        }else {
            // total pages is nil
            return false
        }
       
    }
    
    func sortButtonAction() {
        if sortedText.value == nil || sortedText.value == sortdesc{
            sortedText.value = sortasc
        }else {
            sortedText.value = sortdesc
        }
        newsPage.value = 1
    }
    
    func getTrending(){
        apiService.getSearchNews(q: nil, type: 0)
                  .asObservable()
                  .subscribe(onNext: { trendingItems in
                      self.trendingVar.value = trendingItems
                      print("\(trendingItems)")
                  }).disposed(by: disposeBag)
    }
}

