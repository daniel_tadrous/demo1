//
//  MainTabCoordinator.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//


import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

protocol MainTabCoordinatorDelegate: class {
    func MainTabCoordinatorDidFinish()
}
enum MainTabChildCoordinator {
    case myLocker
    case myCommunity
    case aspectsOfLife
    case menu
    case myUpdates
    case BurgerMenu
}
class MainTabCoordinator: NavigationCoordinator {
    
    
    private let authenticationService: AuthenticationService
    
    // MARK: - Properties
    var childCoordinators = [MainTabChildCoordinator: Coordinator]()
    let navigationController: UINavigationController
    let container: Container
    
    weak var tabBarViewController : MainTabViewController?
    
    //let authenticationService : AuthenticationService
    var delegate: MainTabCoordinatorDelegate?
    
    init(container: Container, navigationController: UINavigationController, authenticationService: AuthenticationService) {
        self.container = container
        self.navigationController = navigationController
        //let myLockerCoordinator = MyLockerCoordinator(container: container, myLockerNavigationController: navigationController, tabBarControlor: (navigationController.visibleViewController as? UITabBarController)!)
        //childCoordinators[.myLocker] = myLockerCoordinator
        // self.authenticationService = authenticationService
        self.authenticationService = authenticationService
        
    }
    
    // MARK: - Coordinator core
    
    func start() {
        let vc = container.resolveViewController(MainTabViewController.self)
        tabBarViewController = vc
        vc.vcDelegate = self
        let sideMenuVc = container.resolveViewController(SideMenuViewController.self)
        
        vc.sideMenuViewController = sideMenuVc
        vc.sideMenuViewController?.delegate = self
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(vc, animated: true)
       
    }
    
    
    fileprivate func languageDidChange() {
        let emptyVc = container.resolveViewController(EmptyFlipViewController.self)
        self.navigationController.popViewController(animated: false)
        self.navigationController.pushViewController(emptyVc, animated: false)
        
        self.navigationController.navigationItem.hidesBackButton = true
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { ( finished) -> Void in
            let vc = self.container.resolveViewController(MainTabViewController.self)
            self.tabBarViewController = vc
            vc.vcDelegate = self
            let sideMenuVc = self.container.resolveViewController(SideMenuViewController.self)
            
            vc.sideMenuViewController = sideMenuVc
            vc.sideMenuViewController?.delegate = self
            vc.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController.popViewController(animated: false)
            self.navigationController.pushViewController(vc, animated: true)
            
        }
    }
    
}

// MARK: - Delegate
extension MainTabCoordinator: MainTabViewControllerDelegate {
    func startMyLockerWithMyEvents() {
        let myLockerCoordinator = MyLockerCoordinator(container: container, myLockerNavigationController: navigationController, tabBarControlor: tabBarViewController! )
        childCoordinators[.myLocker] = myLockerCoordinator;
        myLockerCoordinator.delegate = self
        // authenticationCoordinator.delegate = self
        myLockerCoordinator.startWithMyEvents()
    }
    
    
    func startMyLockerAtNewDraft(isFromOfflineScreen: Bool = false) {
        let myLockerCoordinator = MyLockerCoordinator(container: container, myLockerNavigationController: navigationController, tabBarControlor: tabBarViewController! )
        childCoordinators[.myLocker] = myLockerCoordinator;
        myLockerCoordinator.delegate = self
        // authenticationCoordinator.delegate = self
        myLockerCoordinator.startNewDraft(isFromOfflineScreen:isFromOfflineScreen)
    }
    func startMyLockerAtTalkToUsLanding() {
        let myLockerCoordinator = MyLockerCoordinator(container: container, myLockerNavigationController: navigationController, tabBarControlor: tabBarViewController! )
        childCoordinators[.myLocker] = myLockerCoordinator;
        myLockerCoordinator.delegate = self
        // authenticationCoordinator.delegate = self
        tabBarViewController?.setSelected(item: .myLocker)
        myLockerCoordinator.startTalkToUs()
    }
    
    func startMyLocker(){
        let myLockerCoordinator = MyLockerCoordinator(container: container, myLockerNavigationController: navigationController, tabBarControlor: tabBarViewController! )
        childCoordinators[.myLocker] = myLockerCoordinator;
        myLockerCoordinator.delegate = self
        // authenticationCoordinator.delegate = self
        myLockerCoordinator.start()
    }
    
    func startMyLockerWithSupport(){
        let myLockerCoordinator = MyLockerCoordinator(container: container, myLockerNavigationController: navigationController, tabBarControlor: tabBarViewController! )
        childCoordinators[.myLocker] = myLockerCoordinator;
        myLockerCoordinator.delegate = self
        // authenticationCoordinator.delegate = self
        myLockerCoordinator.startWithSupport()
    }
    
    func startAspectsOfLife(){
        let aspectsOfLifeCoordinator = AspectsOfLifeCoordinator(container: container, aspectsOfLifeNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.aspectsOfLife] = aspectsOfLifeCoordinator
        aspectsOfLifeCoordinator.delegate = self
        // authenticationCoordinator.delegate = self
        aspectsOfLifeCoordinator.start()
    }
    func startMyCommunity(atItem: MyCommunityItems){
        
        var myCommunityCoordinator: MyCommunityCoordinator! = childCoordinators[.myCommunity] as? MyCommunityCoordinator
        
       // if myCommunityCoordinator == nil{
            myCommunityCoordinator = MyCommunityCoordinator(container: container, myCommunityNavigationController: navigationController, tabBarControlor: tabBarViewController!)
            childCoordinators[.myCommunity] = myCommunityCoordinator
            myCommunityCoordinator.delegate = self
       // }else{
           // myCommunityCoordinator.navigationController = navigationController
       // }
        
        // authenticationCoordinator.delegate = self
        switch atItem {
        case .Home:
            myCommunityCoordinator.start()
        case .CoCreation:
            myCommunityCoordinator.start(withItem: .CoCreation)
        default:
            break
        }
        
    }
    
}


extension MainTabCoordinator:MyLockerCoordinatorDelegate{
    func myLockerIsDismissed() {
        
    }
}

extension MainTabCoordinator: AspectsOfLifeCoordinatorDelegate{
    func aspectsOfLifeCoordinatorIsDismissed() {
        childCoordinators[.aspectsOfLife] = nil
    }
    
    
}

extension MainTabCoordinator: SideMenuViewControllerDelegate{
    
    func myEventsIsPressed() {
        let tabBar = tabBarViewController!
        tabBar.setSelected(item: .myEvents)
    }
    
    func newsNSocialPressed() {
        var myCommunityCoordinator: MyCommunityCoordinator! = childCoordinators[.myCommunity] as? MyCommunityCoordinator
        myCommunityCoordinator = MyCommunityCoordinator(container: container, myCommunityNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.myCommunity] = myCommunityCoordinator
        myCommunityCoordinator.delegate = self
        myCommunityCoordinator.start(withItem: .News)
    }
    
    func settingsPressed() {
        let burgerMenuCoordinator = BurgerMenuCoordinator(container: container, myNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.BurgerMenu] = burgerMenuCoordinator
        burgerMenuCoordinator.delegate = self
        burgerMenuCoordinator.start()
        
        // authenticationCoordinator.delegate = self
        burgerMenuCoordinator.startPreferences()
    }
    
    func governmentEntitiesPressed() {
        let burgerMenuCoordinator = BurgerMenuCoordinator(container: container, myNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.BurgerMenu] = burgerMenuCoordinator
        burgerMenuCoordinator.delegate = self
        burgerMenuCoordinator.start()
        
        // authenticationCoordinator.delegate = self
        burgerMenuCoordinator.startGovernmentEntities()
    }
    
    func sideMenuCoCreatePressed() {
        let tabBar = tabBarViewController!
        
        tabBar.setSelected(item: .myCommunity)
//
//        let comunityVc = tabBar.selectedViewController as! MyCommunityViewController

        tabBar.navigateToCoCreate()
        
    }
    
    func aspectsOfLifePressed() {
        let tabBar = tabBarViewController!
        
        tabBar.setSelected(item: .aspectsOfLife)
        
    }
    func mysupportPressed(){
        let tabBar = tabBarViewController!
        
        tabBar.setSelected(item: .incident)
    }
    func myCommunityPressed(){
        let tabBar = tabBarViewController!
        
        tabBar.setSelected(item: .myCommunity)
    }
    func myUpdatesPressed(){
        let tabBar = tabBarViewController!
        
        tabBar.setSelected(item: .myUpdates)
    }
    func logoutPressed() {
        authenticationService.logout(completionHandler: {
            DispatchQueue.main.async {
                self.delegate?.MainTabCoordinatorDidFinish()
            }
        }) { (error) in
            //error
        }
        
    }
 
	func newsAndEventsPressed() {
      // go to news Screen
        let burgerMenuCoordinator = BurgerMenuCoordinator(container: container, myNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        burgerMenuCoordinator.start()
        childCoordinators[.BurgerMenu] = burgerMenuCoordinator
        
        // authenticationCoordinator.delegate = self
        burgerMenuCoordinator.startNewsScreen()
	}

    
    func changeLanguagePressed(){
        languageDidChange()
        
    }
}
extension MainTabCoordinator: MyCommunityCoordinatorDelegate{


}

extension MainTabCoordinator:MediaCenterViewControllerDelegate{
    func openSocial() {
        Toast(message:L10n.commingSoon).Show()
    }
    
    func openNews() {
        var myCommunityCoordinator: MyCommunityCoordinator! = childCoordinators[.myCommunity] as? MyCommunityCoordinator
        myCommunityCoordinator = MyCommunityCoordinator(container: container, myCommunityNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.myCommunity] = myCommunityCoordinator
        myCommunityCoordinator.delegate = self
        myCommunityCoordinator.start(withItem: .News)
    }
    
    func openEvents() {
        print("events")
        var myCommunityCoordinator: MyCommunityCoordinator! = childCoordinators[.myCommunity] as? MyCommunityCoordinator
        myCommunityCoordinator = MyCommunityCoordinator(container: container, myCommunityNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.myCommunity] = myCommunityCoordinator
        myCommunityCoordinator.delegate = self
        myCommunityCoordinator.start(withItem: .CommunityEvents)
    }
    
    func openPressReleases() {
        var myCommunityCoordinator: MyCommunityCoordinator! = childCoordinators[.myCommunity] as? MyCommunityCoordinator
        myCommunityCoordinator = MyCommunityCoordinator(container: container, myCommunityNavigationController: navigationController, tabBarControlor: tabBarViewController!)
        childCoordinators[.myCommunity] = myCommunityCoordinator
        myCommunityCoordinator.delegate = self
        myCommunityCoordinator.start(withItem: .Press)
    }
    
    
}

extension MainTabCoordinator: BurgerMenuCoordinatorDelegate{
    func burgerMenuCoordinatorDidChangeLanguage() {
        self.languageDidChange()
    }

    
}
