//
//  MyCommunityModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class MyCommunityModel {
    
    class Poll: Mappable {
        var id: Int?
        var title: String?
        var participantsCount: Int?
        var isCurrent: Bool?
        var isPromoted: Bool?
        var closureDate: String?
        var answers: [Answer]?
        var answerId: Int?
        var link:String?
        
        required init?(map: Map) {
            
        }
        init(id:Int,answerId:Int){
            self.id = id
            self.answerId = answerId
        }
        // Mappable
        func mapping(map: Map) {
            id <- map["id"]
            title <- map["title"]
            participantsCount <- map["participantsCount"]
            isCurrent <- map["isCurrent"]
            isPromoted <- map["isPromoted"]
            closureDate <- map["closureDate"]
            answers <- map["answers"]
            answerId <- map["answerId"]
            link <- map["link"]
        }
        
        class Answer: Mappable {
            var text: String?
            var color: String?
            var result: String?
            
            required init?(map: Map) {
                
            }
            
            // Mappable
            func mapping(map: Map) {
                text <- map["text"]
                color <- map["color"]
                result <- map["result"]
            }
        }
        
    }
    
    class Survey: Mappable {
        var id: Int?
        var details: String?
       
        required init?(map: Map) {
            
        }
        // Mappable
        func mapping(map: Map) {
            id <- map["id"]
            details <- map["details"]
        }
        
    }
    class CoCreate: Mappable {
        var surveys: [Survey]?
        var polls: [Poll]?
        required init?(map: Map) {
            
        }
        // Mappable
        func mapping(map: Map) {
            surveys <- map["surveys"]
            polls <- map["polls"]
        }
        
    }
}
