//
//  EnablePushNotificationViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/7/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol EnablePushNotificationDelegate {
    func openSetReminderScreen()
}

class EnablePushNotificationViewController: TammViewController , EnablePushNotificationStoryboardLoadable {

    public var viewModel: EnablePushNotificationViewModel!
    public var delegate:EnablePushNotificationDelegate?
     let isEnablePushNotification = "isEnabled"
    
    @IBOutlet weak var enablepushNotificationSwitch: UISwitch!
    
    @IBOutlet weak var setReminderArrow: IconLabel!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    @IBOutlet weak var setReminderView: UIView!
    
    
    @IBOutlet var labels: [LocalizedLabel]!
    
    
    @IBOutlet weak var enablePushView: UIView!
    
    let disposeBag = DisposeBag()
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: L10n.Preferences.notifications, willSetSearchButton: true)
        
        // Do any additional setup after loading the view.
        addTapGesturseOnSetReminderView()
        setupUIBinding()
        setTextInNoteLabel()
        setupBinding()
        setReminderView.isHidden = false
        
        
        let documentsURL = IncidentUrl.getDirectoryForIncident()
        let docs = try? FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
      
        for item in docs!  {
            
            print("\(item.lastPathComponent)\n")
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for myLabel in labels {
            myLabel.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        }
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()

        setReminderView.backgroundColor = UIColor.white.getAdjustedColor()
        enablePushView.backgroundColor = UIColor.white.getAdjustedColor()
        setReminderArrow.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        
        navigationController?.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
    }

    
    fileprivate func setkey(withvalue value:Bool){
        defaults.set(value, forKey: isEnablePushNotification)
        defaults.synchronize()
    }
    
    fileprivate func getkey()->Bool{
        let value = defaults.bool(forKey: isEnablePushNotification)
        print(value)
        
        return value
    }

    private func setupBinding() {
        viewModel.apiResponse.asObservable().subscribe(onNext: { (response) in
            if  response != nil {
              //  if response?.isEnabled == self.getkey() {
                    self.enablepushNotificationSwitch.isOn = (response?.isEnabled)!
//                }else {
//                    self.enablepushNotificationSwitch.isOn = self.getkey()
//                }
//
            }
            }).disposed(by: disposeBag)
    }
    
   
    fileprivate func addTapGesturseOnSetReminderView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(setReminderViewClicked))
        setReminderView.addGestureRecognizer(tap)
    }

    
    @objc func setReminderViewClicked() {
       delegate?.openSetReminderScreen()
    }
    

    fileprivate func setupUIBinding() {
       // EnablepushNotificationSwitch.
        enablepushNotificationSwitch.rx.isOn.asObservable().subscribe(onNext: { isOn in
                    if isOn {
                        //enable push notification
                       self.setReminderView.isHidden = false
                        // get reminder from user defaults
                        self.viewModel.apiService.setPushNotificationStatus(isEnabled: isOn, Reminder: self.viewModel.apiResponse.value?.reminder)
                       // UIApplication.shared.registerForRemoteNotifications()
                    }else {
                       //disable push notification
                        self.setReminderView.isHidden = true
                        //set with old reminder 
                        self.viewModel.apiService.setPushNotificationStatus(isEnabled: isOn, Reminder: self.viewModel.apiResponse.value?.reminder)
                     //   UIApplication.shared.unregisterForRemoteNotifications()
                    }
            
            }).disposed(by: disposeBag)
        
        
    }
    
    func setTextInNoteLabel() {
        noteLabel.attributedText = viewModel.prepareTextInNoteLabel()
    }
}
