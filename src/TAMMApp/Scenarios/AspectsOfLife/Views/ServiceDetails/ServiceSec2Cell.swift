//
//  AolServiceFaqCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
enum ButtonTypeEnum: Int{
    
    case Process = 1,RequiredDocuments = 2, ResponsibleEntity = 4,Fees = 3,EntityHQ = 5,OtherLcation = 6,Kiosk = 7,Online = 8,MobileApp = 9,Phone = 10
}
class ServiceSec2Cell: UITableViewCell {
    
    @IBOutlet weak var iconText1: IconTextHolderView!
    @IBOutlet weak var iconText2: IconTextHolderView!
    @IBOutlet weak var iconText3: IconTextHolderView!
    @IBOutlet weak var iconText4: IconTextHolderView!
    
    @IBOutlet weak var iconText5: IconTextHolderView!
    @IBOutlet weak var iconText6: IconTextHolderView!
    @IBOutlet weak var iconText7: IconTextHolderView!
    @IBOutlet weak var iconText8: IconTextHolderView!
    @IBOutlet weak var iconText9: IconTextHolderView!
    @IBOutlet weak var iconText10: IconTextHolderView!
    
    
    @IBAction func btnClick(_ sender: UIButton) {
        let type:ButtonTypeEnum = ButtonTypeEnum(rawValue: sender.tag)!
        switch type {
        case .Process:
            OnProcessClick?()
        case .RequiredDocuments:
            OnRequiredDocumentsClick?()
        case .ResponsibleEntity:
            OnResponsibleEntityClick?()
        case .Fees:
            OnFeesClick?()
        case .EntityHQ:
            OnEntityHQClick?()
        case .OtherLcation:
            OnOtherLcationClick?()
        case .Kiosk:
            OnKioskClick?()
        case .Online:
            OnOnlineClick?()
        case .MobileApp:
            OnMobileAppClick?()
        case .Phone:
            OnPhoneClick?()
        }
    }
    
  
    var OnProcessClick: (()->Void)?
    var OnResponsibleEntityClick: (()->Void)?
    var OnRequiredDocumentsClick: (()->Void)?
    var OnFeesClick: (()->Void)?
    var OnEntityHQClick: (()->Void)?
    var OnOtherLcationClick: (()->Void)?
    var OnKioskClick: (()->Void)?
    var OnOnlineClick: (()->Void)?
    var OnMobileAppClick: (()->Void)?
    var OnPhoneClick: (()->Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.iconText1.IconText = "\u{e03d}"
        self.iconText2.IconText = "\u{e038}"
        self.iconText3.IconText = "\u{e036}"
        self.iconText4.IconText = "["
        self.iconText5.IconText = "\u{e037}"
        self.iconText6.IconText = "\u{e034}"
        self.iconText7.IconText = "\u{e03a}"
        self.iconText8.IconText = "\u{e039}"
        self.iconText9.IconText = "\u{e035}"
        self.iconText10.IconText = "\u{e018}"
        
        
    }
    
  
}
