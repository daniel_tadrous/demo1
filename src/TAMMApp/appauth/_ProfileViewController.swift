////
////  ProfileViewController.swift
////  TAMMApp
////
////  Created by Marina.Riad on 3/25/18.
////  Copyright © 2018 Marina.Riad. All rights reserved.
////
//
//import Foundation
//import UIKit
//import AppAuth
//class ProfileViewController: UIViewController {
//    @IBOutlet var image: UIImageView!
//    @IBOutlet var userInfoText: UITextView!
//    @IBOutlet var logText: UITextView!
//    let kAppAuthExampleAuthStateKey = "authState";
//    let kAppAuthExampleUserInfoKey = "userInfo";
//    var displayLog:Bool = true;
//    var userInfo:Any?
//    
//    public func setVC(userInfo:Any , displayLog:Bool){
//        self.displayLog = displayLog;
//        self.userInfo = userInfo;
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder);
//        
//    }
//    func ClearState(){
//        UserDefaults.standard.set(nil, forKey: kAppAuthExampleAuthStateKey)
//        UserDefaults.standard.set(nil, forKey: kAppAuthExampleUserInfoKey)
//        UserDefaults.standard.synchronize()
//
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad();
//        let array = self.userInfo as? [String: Any]
//        let pic  = array!["picture"] as? String
//        let url = URL(string: pic!)
//        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//        image.image = UIImage(data: data!)
//        userInfoText.text = "Success: \(String(describing: userInfo))";
//        if(displayLog){
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            logText.text = appDelegate.logText;
//        }
//        else{
//            logText.isHidden = true;
//        }
//        
//    }
//    
//    @IBAction func LogOut(_ sender: Any) {
//        self.ClearState();
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.logText = "";
//        let LogIn = self.storyboard?.instantiateViewController(withIdentifier: "LogIn") as! ViewController;
//        self.present(LogIn, animated: true, completion: nil);
//    }
//    func load_image(image_url_string:String, view:UIImageView)
//    {
//        
//        let image_url: NSURL = NSURL(string: image_url_string)!
//        let image_from_url_request: URLRequest = URLRequest(url: image_url as URL)
//        let session = URLSession.shared;
//        session.dataTask(with: image_from_url_request, completionHandler: {(data: Data!,
//                response: URLResponse!,
//                error: Error!) -> Void in
//                
//                if error == nil && data != nil {
//                    view.image = UIImage(data: data)
//                }
//                
//        })
//        
//    }
//}

