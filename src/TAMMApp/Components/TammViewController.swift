//
//  TammViewController.swift
//  TAMMApp
//
//  Created by kerolos on 5/29/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class TammViewController: SuperViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if UIColor.getInvertedStatus(){
            return UIStatusBarStyle.default
        }else{
            return UIStatusBarStyle.lightContent
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        ErrorView.show = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.barTintColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.tabBarController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        
        

        adjustPreviousPagees()
        
        
//        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.navigationController?.navigationBar.backgroundColor = UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.barTintColor =  UIColor.darkBlueGrey.getAdjustedColor()
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white.getAdjustedColor()]
        self.navigationController?.tabBarItem.badgeColor = UIColor.white.getAdjustedColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white.getAdjustedColor()
    }
    
//    fileprivate func getBackButtonLabel(view: UIView) {
//        for var view in (navigationController?.navigationBar.subviews)!{
//            print("item")
//            print(type(of:  view))
//            print(view)
//            print(view.tag)
//            if type(of: view) == UIButtonLabel{
//                return view
//            }
//            //                    let i = nil as? _UINavigationBarContentView
//        }
//    }
    
    public func adjustPreviousPagees(){
        
        
        
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font:UIFont.init(name: "tamm", size: 27)!], for: .normal)

        
        if let pvc_index = navigationController?.viewControllers.index(before: (navigationController?.viewControllers.endIndex)!){
            
            
            if let pvc = (navigationController?.viewControllers[pvc_index] ) {
                var backFont:UIFont = UIFont.init(name: "CircularStd-Bold", size: 18)!
                var titleFont:UIFont = UIFont.init(name: "CircularStd-Bold", size: 22)!
                if(L102Language.currentAppleLanguage().contains("ar")){
                    backFont = UIFont.init(name: "Swissra-Normal", size: 18)!
                    titleFont = UIFont.init(name: "Swissra-Bold", size: 22)!
                }
                
                pvc.navigationItem.backBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font: backFont], for: .normal)
                
//                getBackButtonLabel()
                navigationController?.navigationBar.backItem?.backBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.white.getAdjustedColor() , NSAttributedStringKey.font: backFont], for: .normal)

            }
        }
        
    }
    
}

class TammPageViewController: UIPageViewController {
    
    var screenName: ScreensEnum? = ScreensEnum(rawValue: String(describing: type(of: self)))
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if UIColor.getInvertedStatus(){
            return UIStatusBarStyle.default
        }else{
            return UIStatusBarStyle.lightContent
        }    }
    
}

class TammNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if UIColor.getInvertedStatus(){
            return UIStatusBarStyle.default
        }else{
            return UIStatusBarStyle.lightContent
        }    }
    
}

class TammTabBarController: UITabBarController {
    
    var screenName: ScreensEnum? = ScreensEnum(rawValue: String(describing: type(of: self)))
    var disposeBag = DisposeBag()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if UIColor.getInvertedStatus(){
            return UIStatusBarStyle.default
        }else{
            return UIStatusBarStyle.lightContent
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        InternetCheckingService.shared.hasInternet.asObservable().subscribe(onNext: {
            hasInternet in
            if hasInternet != nil{
                if hasInternet!{
                    self.disableEnableTabs(enabled: true)
                    self.enableDisableFloatingMenu(isEnabled: true)
                }
                else{
                    self.disableEnableTabs(enabled: false)
                    self.enableDisableFloatingMenu(isEnabled: false)
                }
            }
        }).disposed(by: disposeBag)
        
    }
    
    func disableEnableTabs(enabled: Bool){
        for item in self.tabBar.items!{
            item.isEnabled = enabled
        }
    }
}

//
//extension UIViewController {
//    open var preferredStatusBarStyle: UIStatusBarStyle {
//        get{
//            return .lightContent
//        }
//    }
//}

