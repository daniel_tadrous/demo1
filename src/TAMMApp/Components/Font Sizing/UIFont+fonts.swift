//
//  UIFont+fonts.swift
//  TAMMApp
//
//  Created by kerolos on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static var fontSizeMultiplier : CGFloat {
        
        get {
            let sizesArray = [10.0,11.0,12.0,13.0,14.0,15.0,16.0,18.0,19.0,20.0,21.0,22.0,23.0]
//            switch UIApplication.shared.preferredContentSizeCategory {
//            case UIContentSizeCategory.accessibilityExtraExtraExtraLarge: return 23 / 16
//            case UIContentSizeCategory.accessibilityExtraExtraLarge: return 22 / 16
//            case UIContentSizeCategory.accessibilityExtraLarge: return 21 / 16
//            case UIContentSizeCategory.accessibilityLarge: return 20 / 16
//            case UIContentSizeCategory.accessibilityMedium: return 19 / 16
//            case UIContentSizeCategory.extraExtraExtraLarge: return 19 / 16
//            case UIContentSizeCategory.extraExtraLarge: return 18 / 16
//            case UIContentSizeCategory.extraLarge: return 17 / 16
//            case UIContentSizeCategory.large: return 1.0
//            case UIContentSizeCategory.medium: return 15 / 16
//            case UIContentSizeCategory.small: return 14 / 16
//            case UIContentSizeCategory.extraSmall: return 13 / 16
//            default: return 1.0
//            }
            let multiplier =  sizesArray[UserConfigService.getFontAccessibilityMultiplier()-1]/16
            return CGFloat(multiplier)
            
        }
    }
    
//    enum FontError: Error {
//        case fontfamilyCodeNotFoundError(String)
//    }
    
    public enum CustomFontFamily: String{
        case tamm = "tamm"
        case roboto = "Roboto"
        case circularStd = "CircularStd"

        static func getFontFromCode(_ code: Int) -> String {
            switch code {
            case 0:
                return tamm.rawValue
            case 1:
                return roboto.rawValue
            case 2:
                return circularStd.rawValue
            case 3:
                return circularStd.rawValue

            default:
                //                throw FontError.fontfamilyCodeNotFoundError("fontfamilyCodeNotFoundError") // ("The font code does not ")
                return tamm.rawValue
            }
        }
        
    }
    
    
    
    public enum CustomFontStyle: String{
        
        case regular = "Regular"
        case bold = "Bold"
        case book = "Book"
        case medium = "Medium"
        
        static func getStyleFromCode(_ code: Int) -> String {
            switch code {
            case 0:
                return regular.rawValue
            case 1:
                return bold.rawValue
            case 2:
                return book.rawValue
            case 3:
                return medium.rawValue
            default:
                //                _ = 100 / ( 0 + 0) // throw FontError.fontfamilyCodeNotFoundError("fontfamilyCodeNotFoundError") // ("The font code does not ")
                return regular.rawValue
            }
        }
    }
    class func getCustomFontSize(_ customFontSize: CGFloat) -> CGFloat {
        return customFontSize * (fontSizeMultiplier )
    }
    
    class func getCustomFont(customFontFamilyCode: Int, customFontStyleCode: Int, customFontSize: CGFloat ) -> UIFont{
        //        let weight = (font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.traits)
        //            as! NSDictionary)[UIFontDescriptor.TraitKey.weight]!
        
        // Create a new font traits dictionary
        //        let attributes = [
        //            UIFontDescriptor.AttributeName.traits: [
        //                UIFontDescriptor.TraitKey.weight: weight
        //            ]
        //        ]
        
        // Create a new font descriptor
        let fontFamily = "\(CustomFontFamily.getFontFromCode(customFontFamilyCode))"
        let fontStyle = "\(CustomFontStyle.getStyleFromCode(customFontStyleCode))"
        
        var fontValue = ""
        if fontFamily == "tamm"{
            fontValue = fontFamily
        }else{
            fontValue = "\(fontFamily)-\(fontStyle)"
        }
        
        let descriptor = UIFontDescriptor(name: fontValue, size: getCustomFontSize(customFontSize)   )
        
        //            .withFamily(fontFamily)
//                    .addingAttributes(attributes)
        let fontSize = customFontSize * (fontSizeMultiplier )
        
        // Find and set a font that matches the descriptor
        return UIFont(name: fontValue, size: fontSize)!
//        return UIFont(descriptor: descriptor, size: customFontSize * fontSizeMultiplier)
    }
    
    
    
}

