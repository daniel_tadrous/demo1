//
//  RedirectOutsidePopupView.swift
//  TAMMApp
//
//  Created by marina on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import SafariServices
protocol RedirectOutsidePopupViewDelegate {
    func redirectNow()
}
class RedirectOutsidePopupView: UIView {
    @IBOutlet weak var popupView: UIView!
    @IBOutlet var contentView: UIView!
    private var timer:Timer = Timer()
    public var delegate:RedirectOutsidePopupViewDelegate!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("RedirectOutsidePopupView", owner: self, options: nil)
        contentView.frame = self.bounds
        addSubview(contentView)
        timer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (timer) in
            self.delegate.redirectNow()
            self.removeFromSuperview()
            
        }
    }
    
    @objc func timerFired(){
        // open link
        self.removeFromSuperview()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        popupView.addRoundAllCorners(radious: 8);
    }
}
