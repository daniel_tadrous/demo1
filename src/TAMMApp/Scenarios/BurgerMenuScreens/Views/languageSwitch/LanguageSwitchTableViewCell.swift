//
//  PreferencesTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 7/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class LanguageSwitchTableViewCell: UITableViewCell {

    
    static let identifierNavigation = "languageItemCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var innerBulletView: RoundedView!
    @IBOutlet weak var titleLabel: LocalizedLabel!

    
    func set( title: String, isSelected: Bool, fixedLanguage: String ){
        titleLabel.fixedLanguage = fixedLanguage
        titleLabel.text = title
        innerBulletView.isHidden = !isSelected
        
        innerBulletView.layer.cornerRadius = innerBulletView.frame.width / 2
        
    }
    
}
