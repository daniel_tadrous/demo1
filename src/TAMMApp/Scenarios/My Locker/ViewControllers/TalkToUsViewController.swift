//
//  TalkToUsViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import BSImagePicker
import Photos
import MediaPlayer
import MobileCoreServices
import ArcGIS

//import GooglePlacePicker

protocol TalkToUsSaveViewControllerDelegate : class{
    func openConfirmation(refrenceNumber:String)
    func displayErrorOverlay()
    func openMediaActionbar(actionSheet: GalleryActionSheet)
    func openCameraVC(allowed:Int)
    func openLocationPicker(_ locationDelegate: MapParentViewControllerDelegate, withSearchText: String?, mapPlace:MapPlaceObject?)
    func openDraftsScreen()
}

class TalkToUsViewController: TammViewController, TalkToUsStoryboardLodable {
    
    let cameraHandler = CameraHandler.shared
    
    
    var latitudeSelected:Double?
    var longitudeSelected:Double?
    var Address:String?
    
    private var isSaveTodraft:Bool = false
    private var pickedPlace: LocationPickerLocation?
    var isFromOfflineScreen:Bool = false
    private var pickerField = UITextField()
    var draftId : String?
    var draft: DALDrafts?
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var ScrollViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var searchText: LocalizedTextField!
    static var coveringWindow: UIWindow!
    @IBOutlet weak var incidentView: IncidentReportView!
    @IBOutlet weak var messageType: UITextField!
    @IBOutlet weak var messageTypeTouchableView: UIView!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var mainScrollview: UIScrollView!
    
    @IBOutlet weak var validationLbl: LocalizedLabel!
    
    @IBOutlet weak var searchTextLine: UIView!
    public weak var delegate:TalkToUsSaveViewControllerDelegate?
    //public var delegateDrafts: OfflineScreenDelegate?
    private var isValid:Bool = true
    
    var exceededLimitCheck = false
    var notSupportedCheck = false
    
    var selectedMessageId:Int = -1
    //    @IBAction func touchUpInsideMessageType(_ sender: UIView) {
    //        self.openPickerView()
    //    }
    private var disposeBag = DisposeBag()
    var viewModel:TalkToUsViewModel!
    private var textFromMic:Bool = false
    public weak var talkToUsOverlayView:TalkToUsOverlayView!
    private var typesDisplayViewData: MessageTypesViewDataType?
    private let debounceTimeInSec: Double = 0.5
    private weak var pickerView: PickerView?
    private var mediaActionSheet:GalleryActionSheet?
    private var internetService:InternetCheckingService!
    private var isSaved = false
    private var isAddingData = false
    private var NewMessageInitial:IncidentMessageViewData!
    private var searchtext:String = ""
    private let maximumSearchTextLength = 60
    
    @IBAction func btnClearClicked(_ sender: Any) {
        self.searchText.text = ""
        self.btnClear.isHidden = true
    }
    fileprivate func getCurrentViewMessageObject() -> IncidentMessageViewData {
        
        var NewMessage = self.incidentView.getFormData() as! IncidentMessageViewData
        NewMessage.messageTypeId = self.selectedMessageId
        NewMessage.title = self.searchtext
        if let place = self.pickedPlace{
            NewMessage.incidentLocation = "\(place.locationAddress)"
        }
        NewMessage.media = self.viewModel.getMediaList()
        if longitudeSelected != nil {
            NewMessage.long = longitudeSelected!
        }
        
        if latitudeSelected != nil {
            NewMessage.lat = latitudeSelected!
        }
        
        return NewMessage
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //save to drafts click
        let NewMessage:IncidentMessageViewData = getCurrentViewMessageObject()
        
        if self.isDirty(NewMessage: NewMessage, NewMessageInitial: NewMessageInitial) && !isSaved && !isAddingData{
            let discardChangesView = DiscardChangesView(frame: UIScreen.main.bounds,okClickHandler:{
                
                _ = self.draft?.delete()
                
                self.viewModel.saveMessageToDrafts(NewMessage: NewMessage)
                
                self.removeFileWithURL(self.NewMessageInitial.voiceURI)
                
                self.onDiscardPopupActionClicked()
                TalkToUsViewController.coveringWindow.removeFromSuperview()
                TalkToUsViewController.coveringWindow = nil
            }, cancelClickHandler:{
                if self.NewMessageInitial.voiceURI != NewMessage.voiceURI{
                    self.removeFileWithURL(NewMessage.voiceURI)
                }
                self.onDiscardPopupActionClicked()
                TalkToUsViewController.coveringWindow.removeFromSuperview()
                
                TalkToUsViewController.coveringWindow = nil
            })
            discardChangesView.tag = 1000
            
            TalkToUsViewController.coveringWindow = UIWindow(frame: UIScreen.main.bounds)
            TalkToUsViewController.coveringWindow.windowLevel = UIWindowLevelAlert + 1
            TalkToUsViewController.coveringWindow.isHidden = false
            TalkToUsViewController.coveringWindow.makeKeyAndVisible()
            TalkToUsViewController.coveringWindow.addSubview(discardChangesView)
            TalkToUsViewController.coveringWindow.bringSubview(toFront: discardChangesView)
            TalkToUsViewController.coveringWindow.backgroundColor = UIColor.clear
        }
        
        incidentView.viewWillDisappear()
        
    }
    
    private func removeFileWithURL(_ url: URL?){
        if let url = url {
            try? FileManager.default.removeItem(at: url)
        }
    }
    
    private func onDiscardPopupActionClicked(){
        self.isSaved = true
        self.viewModel.title = ""
        self.searchText.text = L10n.searchEllipsized
        self.selectedMessageId = -1
        
        self.incidentView.emailCheckBox.isSelected = false
        self.incidentView.mobileCheckBox.isSelected = false
        self.incidentView.smsCheckBox.isSelected = false
        // to do
        //self.incidentView.mediaView.isSelected = !false
        self.incidentView.incidentPlacetxt.text = ""
        self.incidentView.makePublicSwitch.isOn = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isAddingData = false
        let mediaList = (viewModel?.getMediaList())!
        
        incidentView.notSupportedCheck = self.viewModel.notSupportedCheck
        if(mediaList.count >= 0){
          //  incidentView.mediaList = mediaList
        }
        self.mainScrollview.resizeToFitContent()
    }
    
    private func initializeNewInitialMessage(){
        NewMessageInitial = self.incidentView.getFormData() as! IncidentMessageViewData
        NewMessageInitial.messageTypeId = self.selectedMessageId
        NewMessageInitial.title = draft == nil ? "" : self.searchtext
        if let place = self.pickedPlace{
            NewMessageInitial.incidentLocation = "\(place)"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ErrorView.show = !isFromOfflineScreen
        //self.setupBinding()
        searchText.textColor = UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1)
        messageType.rightViewMode = .always
        let fontImage = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: messageType.frame.height));
        fontImage.text = "q"
        fontImage.font = UIFont.init(name: "tamm", size: 30)
        fontImage.textAlignment = .center
        messageType.rightView = fontImage
        fontImage.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        //setNavigationBar(title: L10n.talk_to_us_header)
        
        incidentView.delegate = self
        
       
                self.searchText.rx.observe(String.self, "text")
                    .subscribe(onNext: { text in
                      if(text == ""){
                            self.validationLbl.text = L10n.messageTitleMandatory
                            self.validationLbl.isHidden =  false
                            self.isValid = false
                            self.searchTextLine.backgroundColor = UIColor.red
                        }
                        else{
                            self.btnClear.isHidden =  false
                            self.searchtext = text!
                            self.searchText.textColor = UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1)
                            self.isValid = true
                            self.validationLbl.isHidden =  true
                            self.searchTextLine.backgroundColor = UIColor.paleGrey
                        }
                        
                        if((self.searchText.text?.count) ?? 0 > self.maximumSearchTextLength){
                            self.isValid = false
                            self.validationLbl.isHidden =  false
                            self.validationLbl.textAlignment = .right
                            self.validationLbl.text = String(self.searchText.text?.count ?? 0 ) + "/" + String(self.maximumSearchTextLength)
                            self.searchTextLine.backgroundColor = UIColor.red
                        }
                    })
                    .disposed(by: disposeBag)
        self.validationLbl.isHidden =  true
        self.searchTextLine.backgroundColor = UIColor.paleGrey
        self.messageType.rx.observe(String.self, "text")
            .subscribe(onNext: { text in
                
                self.viewModel.messagesTypesVar.value?.types = self.viewModel.staticMessagesTypes?.types
                
                
                for type in (self.viewModel.messagesTypesVar.value?.types)!{
                    if(type.text == self.messageType.text){
                        self.incidentView.messageType = Int(type.id!)
                        break
                    }
                }
                //self.getCurrentViewMessageObject().messageTypeId
            })
            .disposed(by: disposeBag)
        self.setupBinding()
        
        internetService =  InternetCheckingService.shared
        internetService.hasInternet.asObservable().subscribe(onNext: { [unowned self]
            hasInternet in
            if hasInternet != nil{
                if hasInternet!{
                    print("Has Internet")
                    self.btnSend.setTitle(L10n.send, for: .normal)
                    self.checkDraftAddress()
                    //self.checkDraftAddress()
                }
                else{
                    print("no Internet")
                    self.btnSend.setTitle(L10n.save_to_drafts, for: .normal)
                }
            }
        }).disposed(by: disposeBag)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnSend.addRoundCorners(radious: 25)
        btnClear.addRoundAllCorners(radious: btnClear.frame.width / 2)
        // let lastHeight = self.btnSend.frame.origin.y + self.btnSend.frame.height
        
        //        if(lastHeight > UIScreen.main.bounds.height){
        //            self.ScrollViewHeightConstraint.constant = lastHeight - UIScreen.main.bounds.height + 200
        //        }
        self.mainScrollview.resizeToFitContent()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.mainScrollview.resizeToFitContent()
    }
    
    
    fileprivate func checkDraftAddress(){
 
        if draft != nil && draft?.location != nil   {
            
                if (draft?.location?.contains(L10n.offlineAddress))! ||  draft?.location == ""{
                    // send Request to update new Address
                    updateAddress()
                    
                }
        }else {
            
            if (incidentView.incidentPlacetxt.text?.contains(L10n.offlineAddress))! {
                updateAddress()
            }
        }
    }
    
    
    fileprivate func updateAddress() {
        ArcGisUtil.reverseGeocode(point: getLocationSelected()!, completionHandler: { [weak self]  address in
            
            if let addressSelected = address {
                //self.draft?.location = addressSelected
                    self?.incidentView.incidentPlacetxt.text = addressSelected
                    self?.incidentView.setInfoView(title: L10n.address, description: addressSelected)
                
                if self?.draft != nil {
                    self?.updateMsgToDrafts(withAdress: addressSelected)
                }
            }
            
        })
    }
    
    
    fileprivate func setupBinding(){
        viewModel.exceededLimitCheck.asDriver().drive(onNext: { [unowned self] (val) in
            self.incidentView.exceededLimitCheck = val!
        }).disposed(by: disposeBag)
        viewModel.onMediaChange = {
            self.incidentView.mediaList = self.viewModel.getMediaList()
        }
        if(!viewModel.title.isEmpty){
            searchText.text = viewModel.title
            btnClear.isHidden = false
        }
        else{
            
            btnClear.isHidden = true
        }
        
        //edit mode
        if self.draftId != nil{
            self.draft = viewModel.getDraftById(draftId: draftId!)
            
            if(!(self.draft?.draftTitle?.isEmpty)!){
                self.searchText.text = self.draft?.draftTitle
                btnClear.isHidden = false
            }
            else{
                btnClear.isHidden = true
            }
            self.selectedMessageId = (self.draft?.typeId)!
            
            self.incidentView.emailCheckBox.isSelected = (self.draft?.notifiedByEmail)!
            self.incidentView.mobileCheckBox.isSelected = (self.draft?.notifiedByMobileApplication)!
            self.incidentView.smsCheckBox.isSelected = (self.draft?.notifiedByPhone)!
            // to do
            // self.incidentView.NoMediaCheckBox.isSelected = !(self.draft?.hasMedia)!
            self.incidentView.incidentPlacetxt.text = self.draft?.location
            self.incidentView.makePublicSwitch.isOn = (self.draft?.isInformationPublic)!
            
            
            let medialist: [MediaViewModelData] = (draft?.mediaUris)!
            
            //medialist.append(MediaViewModelData(name: url.lastPathComponent, resolution: "", fileSize: sizeString, url: url))
            
            
            self.viewModel.setMediaList(medialist)
            self.incidentView.mediaList = self.viewModel.getMediaList()
            
            let url = URL(string:(draft?.voiceUri ?? nil)! )
            self.incidentView.setVoiceNoteAndComment(voiceUrl: url ?? nil, comment: (draft?.comments) ?? "")
            self.mainScrollview.resizeToFitContent()
  
         
        self.initializeNewInitialMessage()
         
            if self.draft?.long != 0 && self.draft?.lat != 0{
                incidentView.setInfoView(title: L10n.offlineAddress, description: L10n.fullOfflineAddress(point1: "\(self.draft?.lat)!)", point2: "\(self.draft?.long)!)"))
                incidentView.mapViewImage.image = UIImage(named: "locationImage")
                incidentView.setLocationText(L10n.fullOfflineAddress(point1: "\(self.draft?.lat)!)", point2: "\(self.draft?.long)!)"))
                incidentView.incidentPlacetxt.text = L10n.fullOfflineAddress(point1: "\(self.draft?.lat)!)", point2: "\(self.draft?.long)!)")
                self.longitudeSelected = (draft?.long)!
                self.latitudeSelected = (draft?.lat)!
                //self.checkDraftAddress()
            }
            
            if self.draft?.location != nil && self.draft?.location != "" {
                
                incidentView.mapViewImage.image = UIImage(named: "locationImage")
                incidentView.setInfoView(title: L10n.address, description: (self.draft?.location!)!)
                incidentView.setLocationText((self.draft?.location!)!)
                incidentView.incidentPlacetxt.text = (self.draft?.location!)!
            }
            
        }
            
        viewModel.messagesTypes.observeOn(MainScheduler.instance).subscribe(onNext:{
            [unowned self] typesDisplayData in
            self.typesDisplayViewData = typesDisplayData
            self.selectedMessageId = self.selectedMessageId == -1 ?(typesDisplayData?.selectedId) ?? 0 : self.selectedMessageId
            self.initializeNewInitialMessage()
            if (self.selectedMessageId != -1){
               
                // getting the selected item
                let text = self.typesDisplayViewData?.types?.reduce(nil, { (result, type) -> String? in
                    if self.selectedMessageId == type.messageId {
                        return type.messageName
                    }else{
                        return result
                    }
                })
                self.messageType.text = text
            }
        }).disposed(by: disposeBag)
        setupSpeechBinding()
        self.mainScrollview.resizeToFitContent()
    }
    
    @IBAction func btnSendClicked(_ sender: Any) {

        if viewModel.getisWifiOnlyStatus() && self.isValid {
            //switch in settings isOn
            if !WifiInternetService.isWifiConnected() && self.internetService.hasInternet.value!{
                // wifi is not connected
                let vc = ConfirmationMessageViewController()
                vc.initialize(title: L10n.upload_Over_WIFI, message: L10n.youHaveChosenToUpload, CancelBtnTitle: L10n.uploadMediaOverNetwork, confirmationBtnTitle: L10n.saveToDrafts, okClickHandler: { self.chaneIsSaveToDraftValue(); self.uploadData()
                } , saveToDraftsHandler: { self.uploadData() })
                
                self.present(vc, animated: true, completion: nil)
            }else{
                uploadData()
            }
        }else {
            //switch in settings isOff
            uploadData()
        }
   

    }
    
    private func chaneIsSaveToDraftValue() {
        isSaveTodraft = true
    }
    
    private func uploadData() {
        
        if(self.isValid && self.incidentView.isValid()){
            isSaved = true
            var NewMessage:IncidentMessageViewData = getCurrentViewMessageObject()

            if self.internetService.hasInternet.value! {
                //ToDo rework add media
                
                if !isSaveTodraft {
                    NewMessage.media = []
                    viewModel.sendMessage(message: NewMessage).observeOn(MainScheduler.instance).subscribe(onNext:{
                        [unowned self] text in
                        
                        _ = self.draft?.delete()
                        self.delegate?.openConfirmation(refrenceNumber: text)
                        
                        } ,onError:{ _ in
                            self.hideFloatingMenu()
                            self.delegate?.displayErrorOverlay()
                    }).disposed(by: disposeBag)
                }else {
                    _ = self.draft?.delete()
                    self.viewModel.saveMessageToDrafts(NewMessage: NewMessage)
                    delegate?.openDraftsScreen()
                }
              
            }else{
                _ = self.draft?.delete()
                self.viewModel.saveMessageToDrafts(NewMessage: NewMessage)
                if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                    myDelegate.getAppCoordinator().showOfflineScreen()
                }
            }
        }
        else{
            self.searchText.text = self.searchText.text
        }
    }
    
    
    private func updateMsgToDrafts(withAdress: String) {
        //self.draft?.location = withAdress
        self.draft?.updateAddress(newAddress: withAdress)
        //self.viewModel.saveMessageToDrafts(NewMessage: getCurrentViewMessageObject())
        
        NewMessageInitial.incidentLocation = withAdress
       
        
        
    }
    
    
    private func isDirty(NewMessage:IncidentMessageViewData, NewMessageInitial: IncidentMessageViewData )->Bool{
        
        let mediaset1 = NSSet(array:NewMessage.media ?? [String]())
        let mediaset2 = NSSet(array:NewMessageInitial.media ?? [String]())
        
        
        if !(NewMessage.hasMedia == NewMessageInitial.hasMedia
            && NewMessage.incidentLocation == NewMessageInitial.incidentLocation
            && NewMessage.messageTypeId == NewMessageInitial.messageTypeId
            && NewMessage.notifiedByEmail == NewMessageInitial.notifiedByEmail
            && NewMessage.notifiedByMobileApplication == NewMessageInitial.notifiedByMobileApplication
            && NewMessage.notifiedByPhone == NewMessageInitial.notifiedByPhone
            && NewMessage.title == NewMessageInitial.title
            && mediaset1.isSubset(of: mediaset2 as! Set<AnyHashable>) && mediaset2.isSubset(of: mediaset1 as! Set<AnyHashable>)
            && NewMessage.comment == NewMessageInitial.comment
            && NewMessage.voiceURI == NewMessageInitial.voiceURI
            
            && NewMessage.locationCoordinates == NewMessageInitial.locationCoordinates
            
            && NewMessage.isInformationPublic == NewMessageInitial.isInformationPublic
            
            
            ){
            return true
        }
        
        return false
    }
    
    var overlayMicText: String?{
        didSet{
            if self.talkToUsOverlayView != nil{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.talkToUsOverlayView.searchTxt.text = self.overlayMicText
                    self.viewModel.currentQuery = self.overlayMicText
                })
                
            }
        }
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.mainScrollview.resizeToFitContent()
    }
    func setupSpeechBinding(){
        
        
        
        // triggering the mic recognision when item is pressed
        let tapGesture = UITapGestureRecognizer()
        micButton.isUserInteractionEnabled = true
        micButton.addGestureRecognizer(tapGesture)
        
        tapGesture.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: { recognizer in
            
            print("mic button tapped")
            if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: self.viewModel.speechDuration)
            
            // assigning the mic text to the text field
            
            self.viewModel.startSpeechService().observeOn(MainScheduler.instance).subscribe(onNext:{
                [unowned self] text in
                if text != nil {
                    self.searchText.text = text
                    self.btnClear.isHidden = false
                }
                }, onCompleted: {
                    [unowned self] in
                    //self.openOverlay()
            }).disposed(by: self.disposeBag)
            }
            // we may use an overlay for the service
        }).disposed(by: disposeBag)
        
        
        
        // tab on the dropdown label
        let tapGestureDD = UITapGestureRecognizer()
        messageTypeTouchableView.isUserInteractionEnabled = true
        messageTypeTouchableView.addGestureRecognizer(tapGestureDD)
        
        tapGestureDD.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: {
            [unowned self ] recognizer in
            self.openPickerView()
        }).disposed(by: disposeBag)
        
        
        // tab on the dropdown label
        let tapGestureResigne = UITapGestureRecognizer()
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGestureResigne)
        
        tapGestureResigne.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: {
            [unowned self ] recognizer in
            self.view.endEditing(true)
            //            self.pickerField.resignFirstResponder()
        }).disposed(by: disposeBag)
        
        
        incidentView.locationMicButton.rx.tap.debounce(0.3, scheduler: MainScheduler.instance).bind(onNext:{
            print("mic button tapped")
            if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: self.viewModel.speechDuration)
            let observable = self.viewModel.startSpeechService().observeOn(MainScheduler.instance)
            observable.subscribe({ [unowned self] event in
                switch event{
                    
                case .next(let text):
                    self.incidentView.setLocationText(text)
                case .error(let e):
                    break
                case .completed:
                    self.pickPlace(text: self.incidentView.getLocationText())
                    self.incidentView.setLocationText("")
                }
                
            }).disposed(by: self.disposeBag)
            }
        }).disposed(by: disposeBag)
        
        
        
        
    }
    
    
    private func openPickerView(){
        let picker = PickerView()
       // self.viewModel.messagesTypesVar.value?.types
        picker.items = ((self.typesDisplayViewData?.types) ?? []).map({ (type) -> PickerItemType in
            return PickerItem.init(id: type.messageId, displayName: type.messageName)
        })
        var selectedIndex = -1
        let itemCount = (self.typesDisplayViewData?.types?.count ?? 0)
        for i in 0..<itemCount {
            if self.typesDisplayViewData?.types?[i].messageId == selectedMessageId {
                selectedIndex = i
                break
            }
        }
        if selectedIndex != -1{
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        pickerField.inputView = picker
        pickerField.removeFromSuperview()
        picker.viewDelegate = self
        pickerField.frame = CGRect.zero
        pickerField.clipsToBounds = true
        self.view.addSubview(pickerField)
        pickerField.becomeFirstResponder()
        
        
    }
    
    
    private func dismissOverlay(){
        // animate up and out of screen
        let height =  self.talkToUsOverlayView.bounds.height
        let currCenter =  self.talkToUsOverlayView.center
        self.talkToUsOverlayView.endEditing(true)
        
        UIView.animate(withDuration: Dimensions.animationDuration1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.talkToUsOverlayView.center = currCenter.applying(CGAffineTransform(translationX: 0, y: -height) )
        }) { (finished) in
            self.talkToUsOverlayView.removeFromSuperview()
        }
    }
    
    
}

extension TalkToUsViewController : PickerViewDelegate{
    
    func itemSelected(item: PickerItemType, pickerView: PickerView) {
        //        self.pickerView?.removeFromSuperview()
        let item2 = MessageTypeViewData.init(messageName: item.displayName, messageId: item.id as! Int)
        messageType.text = item2.messageName
        self.selectedMessageId = item2.messageId
    }
    
    func clickOutsidePicker() {
        //        self.pickerView?.removeFromSuperview()
    }
    
    
}

extension TalkToUsViewController:  CameraHandlerDelegate{
    func videoIsSelected(url: URL) {
        
    }
    
    func imageIsSelected(url: URL) {
        
    }
    
}

extension TalkToUsViewController: GalleryActionSheetDelegate{
    func openDocuments() {
        DispatchQueue.main.async {
            self.isAddingData = true
            let docPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeText)], in: UIDocumentPickerMode.import)
            docPicker.delegate = self
            
            self.present(docPicker, animated: true, completion: nil)
        }
    }
    
    func openSoundFiles() {
        let status = MPMediaLibrary.authorizationStatus()
        switch status {
        
        case .notDetermined:
            MPMediaLibrary.requestAuthorization() { status in
                if status == .authorized {
                    self.isAddingData = true
                    DispatchQueue.main.async {
                        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.anyAudio)
                        mediaPicker.allowsPickingMultipleItems = true
                        mediaPicker.delegate = self
                        self.present(mediaPicker, animated: true, completion: nil)
                    }
                }
            }
        case .denied:
            DispatchQueue.main.async {
                let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: "Sound permission denied", onComplete: {
                })
                UIApplication.shared.keyWindow!.addSubview(permessionView)
                UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
                
            }
        case .restricted:
            break
        case .authorized:
            DispatchQueue.main.async {
                self.isAddingData = true
                let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.anyAudio)
                mediaPicker.allowsPickingMultipleItems = true
                mediaPicker.delegate = self
                
                self.present(mediaPicker, animated: true, completion: nil)
            }
        }
        //let vc =
    }
    
    func openCamera() {
        isAddingData = true
        self.delegate?.openCameraVC(allowed: viewModel.availableAssets)
        
    }
    
    func openGallery() {
        if GalleryPermission.isOpenGalleryAvailable() {
                    isAddingData = true
                    let vc = BSImagePickerViewController()
                    vc.maxNumberOfSelections = viewModel.availableAssets
                    self.hideFloatingMenu()
                    bs_presentImagePickerController(vc, animated: true,
                                                    select: { (asset: PHAsset) -> Void in
                                                        print("Selected: \(asset)")
                    }, deselect: { (asset: PHAsset) -> Void in
                        print("Deselected: \(asset)")
                    }, cancel: { (assets: [PHAsset]) -> Void in
                        DispatchQueue.main.async{
                            self.displayFloatingMenu()
                        }
                        self.isAddingData = false
                        print("Cancel: \(assets)")
                    }, finish: { (assets: [PHAsset]) -> Void in
                        DispatchQueue.main.async{
                            self.displayFloatingMenu()
                            self.viewModel.setRawMedia(rawMedia: assets)
                            self.incidentView.mediaList = self.viewModel.getMediaList()
                            self.mainScrollview.resizeToFitContent()
                        }
                        
                        self.isAddingData = false
                        print("Finish: \(assets)")
                    }, completion: nil)
                }
        }
        
        
         
    
}

extension TalkToUsViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
}

extension TalkToUsViewController
{
    
    func pickPlace(text: String?) {
        isAddingData = true
        
        delegate?.openLocationPicker(self, withSearchText: text , mapPlace:getMapPlace())

    }
    
    func getLocationSelected() -> AGSPoint? {
        if latitudeSelected != nil && longitudeSelected != nil {
            return AGSPoint(x: latitudeSelected!, y: longitudeSelected!, spatialReference: AGSSpatialReference(wkid: 3857))
            
        }
        return nil
    }
    
    func getMapPlace() -> MapPlaceObject? {
        let loc2D = getLocationSelected()
        if loc2D != nil {
            let mapPlace = MapPlaceObject()
            mapPlace.mapPoint = loc2D
            mapPlace.addressLocation = incidentView.incidentPlacetxt.text
            
            return mapPlace
        }
        return nil
    }
    
//    func getLocationName() -> String? {
//        var NewMessage = self.incidentView.getFormData() as! IncidentMessageViewData
//        if let place = self.pickedPlace{
//            return "\(place.locationAddress)"
//        }
//        return nil
//    }
    
}

extension TalkToUsViewController: IncidentReportViewDelegate{
    func addLocationTextClicked(text: String?) {
        pickPlace(text: text ?? "")
    }
    
    func shouldRemoveVoiceFile(_ url: URL?) -> Bool {
        if NewMessageInitial.voiceURI == url {
            // do not delete
            return false
        } else{
            return true
        }
    }
    
    func addMediaClicked() {
        self.view.endEditing(true)
        self.mediaActionSheet = GalleryActionSheet(vc:self)
        self.mediaActionSheet?.delegate = self
        self.hideFloatingMenu()
        self.delegate?.openMediaActionbar(actionSheet: self.mediaActionSheet!)
    }
    
    func addLocationClicked() {
        pickPlace(text: nil)
    }
    
    func mediaIsRemoved(index:Int){
        self.viewModel.removeFromMediaList(index: index)
        self.mainScrollview.resizeToFitContent()
    }
    
    
}

extension TalkToUsViewController: MPMediaPickerControllerDelegate{
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true, completion: nil)
        self.isAddingData = false
    }
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        viewModel.setsoundMedia(rawMedia: mediaItemCollection.items,closure:{
            mediaPicker.dismiss(animated: true, completion: nil)
        })
        self.incidentView.mediaList = self.viewModel.getMediaList()
        self.isAddingData = false
    }
}

extension TalkToUsViewController:UIDocumentMenuDelegate{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
    }
    
    public func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController){
        documentMenu.dismiss(animated: true, completion: nil)
    }
}

struct PickerItem : PickerItemType{
    var id: AnyHashable
    var displayName: String
}
//extension UIViewController{
//    func bringDicard
//}

extension TalkToUsViewController: MapParentViewControllerDelegate{
    func locationPickerDidSelectLocation(_ viewController : MapParentViewController, _ location: LocationPickerLocation) {
        // Dismiss the place picker, as it cannot dismiss itself.
        isAddingData = false
        pickedPlace = location
        incidentView.setLocationText(location.locationAddress)
    
        longitudeSelected = location.location.longitude
        latitudeSelected = location.location.latitude
        incidentView.mapViewImage.image = UIImage(named: "locationImage")
        incidentView.setInfoView(title: location.subregion, description: location.locationAddress)
        if location.subregion == "" && location.locationAddress == "" {
            
            incidentView.setInfoView(title: L10n.offlineAddress, description: L10n.fullOfflineAddress(point1: "\(location.location.latitude)", point2: "\(location.location.longitude)"))
            incidentView.incidentPlacetxt.text = L10n.fullOfflineAddress(point1: "\(location.location.latitude)", point2: "\(location.location.longitude)")
        }
        incidentView.mapViewLabel.isHidden = true
    }
    
    func locationPickerDidCancel(_ viewController : MapParentViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        isAddingData = false
        print("No place selected")
    }
    func openOfflineMap() {
        ///
    }
    func openOnlineMap(){
        ///
    }
    
}


extension TalkToUsViewController:UIDocumentPickerDelegate{
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController){
        controller.dismiss(animated: true, completion: nil)
        self.isAddingData = false
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]){
        viewModel.setDocumentsList(urls: urls, closure: {
            self.incidentView.mediaList = self.viewModel.getMediaList()
            self.isAddingData = false
 //           controller.dismiss(animated: true, completion: nil)
        })
    }
}
//
//extension PHAsset {
//
//    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
//        if self.mediaType == .image {
//            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
//            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
//                return true
//            }
//            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
//                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
//            })
//        } else if self.mediaType == .video {
//            let options: PHVideoRequestOptions = PHVideoRequestOptions()
//            options.version = .original
//            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
//                if let urlAsset = asset as? AVURLAsset {
//                    let localVideoUrl: URL = urlAsset.url as URL
//                    completionHandler(localVideoUrl)
//                } else {
//                    completionHandler(nil)
//                }
//            })
//        }
//    }
//}


