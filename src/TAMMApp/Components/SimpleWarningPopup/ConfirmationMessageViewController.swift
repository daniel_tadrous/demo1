//
//  ConfirmationMessageViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class ConfirmationMessageViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        self.appDelegate.removeFloatingMenu()
    }
    
    @IBOutlet weak var cancelLbl: LocalizedLabel!
    @IBOutlet weak var okLbl: LocalizedLabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var attributedLabel: UILabel!
    @IBOutlet weak var bodyLabel: LocalizedLabel!
    
    @IBOutlet weak var cancelBtn: LocalizedButton!
    
    @IBOutlet weak var titleLabel: LocalizedLabel!
    
    @IBOutlet weak var okBtn: LocalizedButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var apiService:ApiSupportServicesType?
    
    private var okClickHandler: (()->Void)?
    
    private var uploadAnyWayHandler: (()->Void)?
    
    var isFirstTimeMap:Bool?
    
    var titleStr:String?
    var message:String?
    var okBtnTitle:String?
    var cancelBtnTitle:String?
    let disposeBag = DisposeBag()
    
    @IBAction func okClickHandler(_ sender: LocalizedButton) {

        apiService?.downloadFile(url: "https://arcgis.sdi.abudhabi.ae/portal/sharing/rest/content/items/214e71049df5427aba282fffb36094d2/data", forName: "map", fileFormat: "vtpk").asObservable().subscribe(onNext: { [weak self] url in
 
        }).disposed(by: disposeBag)
        dismiss(animated: true) {
            self.displayFloatingMenu()
            self.okClickHandler?()
            DiscardChangesView.draftsUpdatedDelegate?.onDraftsUpdated()
        }
        
    }
    
    @IBAction func cancelClickHandler(_ sender: LocalizedButton) {
        self.uploadAnyWayHandler?()
        dismiss(animated: true, completion: {
            DispatchQueue.main.async {
                self.appDelegate.addfloatingMenu()
            }
        })
        
    }
    
    
    
    
    
    func initialize(okClickHandler: @escaping ()->Void) {
        self.okClickHandler = okClickHandler
        commonIntializer()
    }
    func initialize(title:String ,message:String,okClickHandler: @escaping ()->Void) {
        self.okClickHandler = okClickHandler
        self.title = title
        self.message = message
        commonIntializer()
    }
    
    func initialize(title:String ,isFirstTimeMap:Bool,CancelBtnTitle:String,confirmationBtnTitle:String,okClickHandler: @escaping ()->Void, apiService:ApiSupportServicesType) {
        self.okClickHandler = okClickHandler
        self.title = title
        self.isFirstTimeMap = isFirstTimeMap
        self.okBtnTitle = confirmationBtnTitle
        self.cancelBtnTitle = CancelBtnTitle
        self.apiService = apiService
        commonIntializer()
    }
    
    
    func initialize(title:String ,message:String,CancelBtnTitle:String,confirmationBtnTitle:String,okClickHandler: @escaping ()->Void) {
        self.okClickHandler = okClickHandler
        self.title = title
        self.message = message
        self.okBtnTitle = confirmationBtnTitle
        self.cancelBtnTitle = CancelBtnTitle
        commonIntializer()
    }
    
    func initialize(title:String ,message:String,CancelBtnTitle:String,confirmationBtnTitle:String,okClickHandler: @escaping ()->Void,saveToDraftsHandler: @escaping ()->Void) {
        self.okClickHandler = okClickHandler
        self.uploadAnyWayHandler = saveToDraftsHandler
        self.title = title
        self.message = message
        self.okBtnTitle = confirmationBtnTitle
        self.cancelBtnTitle = CancelBtnTitle
        commonIntializer()
    }
    
    
    
    private func commonIntializer(){
        self.modalPresentationStyle = .overFullScreen
    }
    
    private func commonInit(){
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        self.cancelBtn.layer.borderWidth = 1
        self.cancelBtn.layer.borderColor = UIColor.turquoiseBlue.cgColor
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
        setupUiData()
        
    }
    
    
    
    func setupUiData(){
        
        if self.message != nil{
            self.bodyLabel.text = self.message
        }
        
        if self.isFirstTimeMap != nil {
            self.attributedLabel.attributedText = getMessage()
            self.bodyLabel.text = ""
        }
        
        if self.title != nil{
            self.titleLabel.text = self.title
        }
        
        if self.okBtnTitle != nil{
            okLbl.text = okBtnTitle
        }
        if self.cancelBtnTitle != nil{
            cancelLbl.text = cancelBtnTitle
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.okBtn.addRoundCorners(radious: (self.okBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
    }
    
    func getMessage()->NSMutableAttributedString {
        var currentFont:UIFont?
        var currentBoldFont:UIFont?
        
        let regularFont = UIFont(name: "Roboto-Regular", size: UIFont.fontSizeMultiplier * 16.0)!
        let boldFont = UIFont(name: "Roboto-Bold", size: UIFont.fontSizeMultiplier * 20.0)!
        
        let regularArabicFont = UIFont(name: "Swissra-Normal", size: UIFont.fontSizeMultiplier * 16.0)!
        let regularArabicBoldFont = UIFont(name: "Swissra-Bold", size: UIFont.fontSizeMultiplier * 20.0)!
        
        if L10n.Lang == "en" {
            currentFont = regularFont
            currentBoldFont = boldFont
        }else {
            currentFont = regularArabicFont
            currentBoldFont = regularArabicBoldFont
        }
        
        let str1 = L10n.offlineMapsAreUsedTo
        let str2 = L10n.mapSize
        var str3 = L10n.doYouWish
        if isFirstTimeMap! {
            str3 = str3 + L10n.ifNo
        }
        
        //let fullStr = str1 + str2 + str3
        let firstPart = NSMutableAttributedString(string: str1, attributes: [.font:  currentFont!, .foregroundColor:UIColor.darkBlueGrey.getAdjustedColor()])
        
        let secondPart = NSMutableAttributedString(string: str2, attributes: [.font:  currentBoldFont!, .foregroundColor:UIColor.turquoiseBlue.getAdjustedColor()])
        let thirdPart = NSMutableAttributedString(string: str3, attributes: [.font:  currentFont!, .foregroundColor:UIColor.darkBlueGrey.getAdjustedColor()])
        
        firstPart.append(secondPart)
        firstPart.append(thirdPart)
        
        return firstPart
    }
    
}
