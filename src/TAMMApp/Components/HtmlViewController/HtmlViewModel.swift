//
//  HtmlViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class HtmlViewModel {
    
    
    var content:Variable<String?> = Variable(nil)
    var title: String = ""
}
