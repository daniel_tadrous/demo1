//
//  TammAol.swift
//  TAMMApp
//
//  Created by kerolos on 4/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class JourneyModel: Mappable {
    var id: Int?
    var title: String?
    var description: String?
    var icon: String?
    var aspectType: Int?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id              <- map["id"]
        title           <- map["title"]
        description     <- map["description"]
        icon            <- map["icon"]
        aspectType      <- map["aspectType"]
        
    }
    
}
