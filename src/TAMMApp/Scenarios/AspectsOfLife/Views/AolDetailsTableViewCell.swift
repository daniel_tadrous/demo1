//
//  AolDetailsTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 4/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AolDetailsTableViewCell: UITableViewCell {

    public static let globalIdentifier = "AolDetailsTableViewCell"
    @IBOutlet weak var label: LocalizedLabel!
    @IBOutlet weak var shadowedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowedView.layer.shadowColor = UIColor.lightGray.cgColor;
        shadowedView.layer.shadowOpacity=0.5;
        shadowedView.layer.shadowRadius=2.0;
        shadowedView.layer.shadowOffset = CGSize(width:0, height:0);
        shadowedView.layer.masksToBounds = false;
        shadowedView.layer.cornerRadius = 5
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
