//
//  ServiceCategoryEntityModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceCategoryEntityModel : Mappable{
    var id:String?
    var title:String?
    var description:String?
    var services:[ServiceModel]?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        services <- map["services"]
    }
}
