//
//  PressReleasesViewController.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class PressReleasesViewController: TammViewController,PressReleasesStoryboardLoadable {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchFilterView: SearchFilterSort!
    let disposeBag = DisposeBag()
    let cellId = "MediaCenterPressReleaseCell"
    var viewModel: PressReleasesViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        viewModel.viewData.asDriver().drive(onNext: { [unowned self] (pressReleases) in
          
            if pressReleases != nil{
                self.tableView.reloadData()
            }
            
        }).disposed(by: disposeBag)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyInvertColors()
    }
    
    fileprivate func applyInvertColors() {
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        self.searchFilterView.sortBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        self.searchFilterView.filterBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        
        self.searchFilterView.searchView.searchField.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        self.searchFilterView.searchView.view.backgroundColor =  UIColor.white.getAdjustedColor()
        self.searchFilterView.searchView.searchIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        if searchFilterView.searchView.searchField.text == "" {
            searchFilterView.searchView.searchField.text = "Search"
        }
        self.searchFilterView.searchView.searchField.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
    }


}
extension PressReleasesViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.viewData.value?.pressReleases?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MediaCenterPressReleaseCell
        let release = viewModel.viewData.value!.pressReleases![indexPath.row]
        cell.release = release
        cell.titleLabel.text = release.title
        cell.dateLabel.text = release.getFormattedDate()
        cell.sizeLabel.text = release.size
        cell.descriptionLabel.text = release.excerpt
        cell.url = release.url
        cell.fileName = String(release.title!)
        cell.downloadClosure = {url,fileName in
            self.downloadFile(url: url, name: fileName, onCompletion: {
                release.isDownloading = false
                tableView.reloadRows(at: [indexPath], with: .none)
            })
        }
        if release.isDownloading{
            cell.downloadIndicator.startAnimating()
            cell.btnDownload.isEnabled = false
        }else{
            cell.downloadIndicator.stopAnimating()
            cell.btnDownload.isEnabled = true
        }
        cell.applyInvertColors()
        return cell
    }
    func downloadFile(url: String, name:String,onCompletion: @escaping ()->Void){
        viewModel.downloadPDF(url: url, name: name, onCompletion: { isDownloaded in
                onCompletion()

            
            self.openUrl(withUrl: isDownloaded)
        })
        
    }
    
    func openUrl(withUrl url:URL){
        
        let docController:UIDocumentInteractionController = UIDocumentInteractionController.init(url: url)
        docController.delegate = self
        hideFloatingMenu()
        docController.presentPreview(animated: true)
    }
    
}


extension PressReleasesViewController : UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        displayFloatingMenu()
    }
}

