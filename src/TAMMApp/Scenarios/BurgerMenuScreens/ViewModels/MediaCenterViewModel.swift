//
//  MediaCenterViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class MediaCenterViewModel {
    private var apiService: ApiMediaCenterServiceType!
    private(set) var mediaReleases: Variable<MediaCenterModel?> = Variable(nil)
   
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService:ApiMediaCenterServiceType) {
        self.apiService = apiService
    }
    
    func getMyEvents() {
        apiService.getMediaReleases().subscribe(onNext: {  [unowned self] (media) in
            self.mediaReleases.value = media
        })
            .disposed(by: disposeBag)
    }
    
    func downloadPDF(url:String, name:String,onCompletion:@escaping (URL)->Void) {
        apiService.downloadPDF(url: url, forName: name).subscribe(onNext: { (downloaded) in
            if downloaded != nil{
                onCompletion(downloaded!)
                
            }
        }).disposed(by: disposeBag)
    }
    
    func getAttributedText(event:Event, days:String, month:String)->NSMutableAttributedString {
        var startDay:NSMutableAttributedString
//        var startMonth:NSAttributedString
//        var sep:NSAttributedString
        let dateString = days+" "+event.getFormattedStartMonth(monthFormatOfthree: false)
        if L10n.Lang == "en" {
            startDay = NSMutableAttributedString(string: dateString, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white , NSAttributedStringKey.font : UIFont.init(name: "CircularStd-Bold", size: 20)!])

            startDay.addAttribute(NSAttributedStringKey.font, value: UIFont(  name: "CircularStd-Bold",  size:  16)!, range: NSRange( location: days.count, length: month.count+1))

        }else {

            startDay = NSMutableAttributedString(string: dateString, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white , NSAttributedStringKey.font : UIFont.init(name: "Swissra-Bold", size:  20)!])
            
            startDay.addAttribute(NSAttributedStringKey.font, value: UIFont(  name: "Swissra-Bold",  size: 16)!, range: NSRange( location: days.count, length: month.count+1))
        }
        return startDay
    }
    
    
    
    
    
    
}
