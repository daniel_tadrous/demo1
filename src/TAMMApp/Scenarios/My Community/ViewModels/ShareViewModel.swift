//
//  ShareViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/21/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

class ShareViewModel:NSObject, IShare {
    
    func share(viewController: UIViewController,text:String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let textToShare:[String] = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.modalPresentationStyle = .overFullScreen
        activityViewController.popoverPresentationController?.sourceView = viewController.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop,UIActivityType.saveToCameraRoll ,UIActivityType.assignToContact,UIActivityType.message,UIActivityType.addToReadingList,UIActivityType.copyToPasteboard,UIActivityType.postToFlickr,UIActivityType.postToTencentWeibo,UIActivityType.openInIBooks,UIActivityType.postToVimeo,UIActivityType.print, UIActivityType.init("com.apple.reminders.RemindersEditorExtension"),UIActivityType.init("com.apple.mobilenotes.SharingExtension"),UIActivityType.init("com.google.Drive.ShareExtension"),UIActivityType.init("com.apple.CloudDocsUI.AddToiCloudDrive")]
        
        activityViewController.completionWithItemsHandler = {(_, _, _, _) in
            appDelegate.addfloatingMenu()
        }
        //activityViewController.tag = 1000
        // present the view controller
        
        appDelegate.removeFloatingMenu()
        viewController.present(activityViewController, animated: true, completion: nil)
        
    }    
}
