//
//  ServiceSec3CellNoButton.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/23/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit


class ServiceSec3CellNoButton : UITableViewCell {
    
    var OnCellClicked: (()->Void)?
    
    @IBOutlet weak var serviceMainLabel: LocalizedLabel!
    
    @IBOutlet weak var secondLabel: LocalizedLabel!
    

    @IBAction func btnAction(_ sender: Any) {
        OnCellClicked?()
    }
    
    
}
