//
//  LocationInfoView.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/31/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class LocationInfoView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var describtionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var locationPoint: RoundedView!
    @IBOutlet weak var container: UIView!
    
    var title: String!{
        didSet{
            titleLabel.text = title
        }
    }
    
    
    var details: String!{
        didSet{
            describtionLabel.text = details
        }
    }
    
    
    var widthConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        let viewFileName: String = "LocationInfoView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(container)
        scrollView.addRoundCorners(radious: 10)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}
