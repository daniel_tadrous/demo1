//
//  CaseDetailsViewController.swift
//  TAMMApp
//
//  Created by Daniel on 8/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import Swinject


enum CaseDetailCellID: String{
    case AddingAttachmentCell,EntityCell,AttachementTableViewCell,CaseDetailTableViewCell,CaseDetailTableViewCell2, CommentsTableViewCell, CaseRejectedTableViewCell,CaseClosedTableViewCell
}
class CaseDetailsViewController: TammViewController,CaseDetailsStoryboardLoadable {

    let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let outputFormat = "dd MMM 'at' HH:mm a"
    
    @IBOutlet weak var refNumLbl: UILabel!
    @IBOutlet weak var incidentHeaderPlaceholderView: UIView!
    var incidentHeaderViewVC: IncidentHeaderViewController!
    
    var viewModel:CaseDetailsViewModel!
    let disposeBag = DisposeBag()
    var playerUrl: URL?
    var container: Container?
    var answers: [Int:Int] = [:]
    var approveSubmitted:Bool = false
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(title: L10n.Status_tracker, willSetSearchButton: true)
        initializeHeader()
        
        tableView.register(UINib(nibName: CaseDetailCellID.AttachementTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CaseDetailCellID.AttachementTableViewCell.rawValue)
        
       tableView.register(UINib(nibName: CaseDetailCellID.CaseDetailTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CaseDetailCellID.CaseDetailTableViewCell.rawValue)
        
        tableView.register(UINib(nibName: CaseDetailCellID.CaseDetailTableViewCell2.rawValue, bundle: nil), forCellReuseIdentifier: CaseDetailCellID.CaseDetailTableViewCell2.rawValue)
         tableView.register(UINib(nibName: CaseDetailCellID.CommentsTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CaseDetailCellID.CommentsTableViewCell.rawValue)
         tableView.register(UINib(nibName: CaseDetailCellID.CaseRejectedTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CaseDetailCellID.CaseRejectedTableViewCell.rawValue)
        tableView.register(UINib(nibName: CaseDetailCellID.CaseClosedTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CaseDetailCellID.CaseClosedTableViewCell.rawValue)
        setupHooks()
        
    }

    private func initializeHeader(){
        incidentHeaderViewVC = IncidentHeaderViewController()
        //childVC.delegate = self
        addChildViewController(incidentHeaderViewVC)
        //Or, you could add auto layout constraint instead of relying on AutoResizing contraints
        incidentHeaderViewVC.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        incidentHeaderViewVC.view.frame = incidentHeaderPlaceholderView.bounds
        
        incidentHeaderPlaceholderView.addSubview(incidentHeaderViewVC.view)
        incidentHeaderViewVC.didMove(toParentViewController: self)
    }
    private func setupHooks(){
        self.viewModel.viewData.asDriver().drive(onNext: { [unowned self] (message) in
            guard let mess = message else {
                return
            }
            self.refNumLbl.text = L10n.refrenceNumber(refNum: mess.caseId!)
            self.incidentHeaderViewVC.setDates(datesStr: mess.statusDates)
            self.incidentHeaderViewVC.setState(caseDetail: (mess.status?.value)!)
            
            self.tableView.reloadData()
            
        }).disposed(by: disposeBag)
        
        self.viewModel.questions.asDriver().drive(onNext: { (questions) in
            if questions != nil{
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .top, animated: false)
                
            }
        }).disposed(by: disposeBag)
        self.viewModel.isSubmitted.asDriver().drive(onNext: { (isSubmitted) in
            if isSubmitted != nil && isSubmitted!{
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .top, animated: false)
                
            }
        }).disposed(by: disposeBag)
    }

}

extension CaseDetailsViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.viewData.value == nil{
            return 0
        }else{
            switch section{
            case 0:
                let status = self.viewModel.viewData.value!.status!.value
                switch status{
                case .CLOSED, .REJECTED:
                    return 0
                case .RESOLVED:
                    if let isApproved = viewModel.isApproved.value{
                        if isApproved{
                            return 0
                        }
                    }
                    if self.approveSubmitted{
                        return 0 
                    }
                    return 1
                default:
                    return 1
                }
                case 1:
                    let status = self.viewModel.viewData.value!.status!.value
                    switch status{
                    case .RESOLVED, .REJECTED:
                        return 1
                    default:
                        return 0
                    }
            case 3:
                let details = self.viewModel.viewData.value!.caseDetails
                    if details?.text?.isEmpty ?? true && details?.url?.isEmpty ?? true {
                        return 0
                    }
                    return 1
                
                default:
                    return 1
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mes = self.viewModel.viewData.value!
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.AddingAttachmentCell.rawValue) as! AddingAttachmentCell
            
            cell.setCaseType(type: (mes.status?.value)!)
            cell.addCommentHandler = {
                
                let vc = CommentTalkToUsViewController()
                vc.initialize( okClickHandler: { comment in
                    self.viewModel.msgId = mes.id
                    self.viewModel.msgCommentAdded.value = comment
                    mes.additionalComments.append(self.viewModel.createAdditionalComment(comment: comment))
                    //mes.additionalComments.append(comment)
                    self.tableView.reloadData()
                })
                
                self.present(vc, animated: true, completion: nil)
            }
            
            cell.addAttachmenetHandler = {
                
                let vc = AttachementsPopupViewController()
                vc.initialize( okClickHandler: { comment in
                    comment.export(completionHandler: { (url, error) in
                        if url != nil {
                            
                            self.viewModel.msgId = mes.id
                            self.viewModel.mediaURLAdded.value = url
                            let newAttachmenet = self.viewModel.createAdditionalAttachement(id: mes.attachments.count, name: (url?.lastPathComponent)!, size: (Double((url?.fileSizeInKB)!/1024)), url: (url?.path)!)
                            mes.attachments.append(newAttachmenet)
                            
                            self.tableView.reloadData()
                        }else {
                            
                        }
                            //error saving file
                        })
                   
                }, container: self.container!)
                
                self.present(vc, animated: true, completion: nil)
            }
            
            return cell
        case 1:
            if mes.status!.value == .REJECTED{
                let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.CaseRejectedTableViewCell.rawValue) as! CaseRejectedTableViewCell
                cell.bodyLbl.text = mes.statusMessage
                cell.callClick = {
                    
                }
                cell.emailClick = {
                    
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.CaseClosedTableViewCell.rawValue) as! CaseClosedTableViewCell
                cell.bodyLbl.text = mes.statusMessage
                cell.approveClick = {
                    self.viewModel.isApproved.value = true
                }
                cell.rejectClick = {
                    self.viewModel.isApproved.value = false
                    tableView.reloadData()
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
                if self.viewModel.isApproved.value != nil {
                    cell.feedBackView.isHidden = false
                    if self.viewModel.isApproved.value!{
                        self.incidentHeaderViewVC.setState(caseDetail: CaseDetailEnum.CLOSED)
                        cell.questionsStackView.isHidden = false
                        cell.buttonsHolderView.isHidden = false
                        cell.commentViewHolder.isHidden = true
                        cell.rightLbl.textColor = UIColor.white
                        cell.rightBgLbl.text = "l"
                        cell.xLbl.textColor = UIColor.grapefruit
                        cell.xBgLbl.text = "s"
                        for view in (cell.questionsStackView?.arrangedSubviews)!{
                            view.isHidden = true
                            cell.questionsStackView?.removeArrangedSubview(view)
                        }
                        
                        for index in 0..<(viewModel.questions.value?.count ?? 0) {
                            let q = viewModel.questions.value![index]
                            let label = LocalizedLabel()
                            label.font = UIFont(name: "CircularStd-Book", size: 26)
                            label.originalFont = UIFont(name: "CircularStd-Book", size: 22)
                            label.applyFontScalling = true
                            label.RTLFont = "Swissra-Medium"
                            label.textColor = UIColor.darkBlueGrey
                            label.text = q.title
                            label.numberOfLines = 0
                            label.lineBreakMode = .byWordWrapping
                            cell.questionsStackView?.addArrangedSubview(label)
                            for ans in q.answers{
                            let button = PollButton(frame: CGRect(x: 0, y: 0, width: cell.questionsStackView.frame.width, height: 50), fontSize: 18)
                            button.setTitle(ans.text, for: .normal)
                            ans.qIndex = index
                            button.answer = ans
                            button.isSelected(isSelected: self.answers[index] != nil && self.answers[index] == ans.id)
                                button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answerSubmitted(_:))))
                                cell.questionsStackView?.addArrangedSubview(button)
                                
                            }
                        }
                    }else{
                        self.incidentHeaderViewVC.setState(caseDetail: (mes.status?.value)!)
                        cell.questionsStackView.isHidden = true
                        cell.buttonsHolderView.isHidden = false
                        cell.commentViewHolder.isHidden = false
                        
                        cell.rightLbl.textColor = UIColor.lichen
                        cell.rightBgLbl.text = "s"
                        cell.xLbl.textColor = UIColor.white
                        cell.xBgLbl.text = "l"
                    }
                }else{
                    if self.viewModel.isSubmitted.value != nil && self.viewModel.isSubmitted.value!{
                        cell.buttonsHolderView.isHidden = true
                    }else{
                        cell.buttonsHolderView.isHidden = false
                    }
                    cell.feedBackView.isHidden = true
                    cell.rightLbl.textColor = UIColor.lichen
                    cell.rightBgLbl.text = "s"
                    cell.xLbl.textColor = UIColor.grapefruit
                    cell.xBgLbl.text = "s"
                }
                cell.submitClick = {
                    if self.viewModel.isApproved.value!{
                        if self.viewModel.questions.value!.count == self.answers.count || self.answers.count == 0 {
                            self.viewModel.answers.value = self.answers.map({ (k,v) -> Int in
                                return v
                            })
                            
                            self.viewModel.isApproved.value = nil
                            self.viewModel.comment.value = nil
                            self.viewModel.questions.value = nil
                            self.viewModel.answers.value = nil
                            self.approveSubmitted = true
                        }else{
                            Toast.init(message: L10n.answer_all).Show()
                            return
                        }
                    }else{
                        if !cell.commentTextView.text.isEmpty{
                            self.viewModel.comment.value = cell.commentTextView.text
                        
                            self.viewModel.isApproved.value = nil
                            self.viewModel.comment.value = nil
                            self.viewModel.questions.value = nil
                                self.viewModel.answers.value = nil
                            self.approveSubmitted = false
                        }else{
                            Toast.init(message: L10n.commentNotEmpty).Show()
                            return
                        }
                        
                    }
                    let vc = FeedBackThankYouViewController1()
                    vc.initialize(title: L10n.thankYou, summary: L10n.messageRecievedSummary, details: "", refernceNumber: nil)
                    
                    self.present(vc, animated: true, completion: nil)
                }
                cell.cancelClick = {
                    self.viewModel.isApproved.value = nil
                    self.viewModel.comment.value = nil
                    self.viewModel.questions.value = nil
                    self.viewModel.answers.value = nil
                    self.answers = [:]
                    tableView.reloadData()
                    tableView.scrollToRow(at: indexPath, at: .middle, animated: false)
                }
                cell.cancelSubmitbtns.forEach { (btn) in
                    btn.addRoundCorners(radious: btn.frame.height/2)
                }
                return cell
            }
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.EntityCell.rawValue) as! EntityCell
            //case type
            cell.caseTypeSubLbl.text = mes.caseTitle
            cell.caseTypeLbl.text = mes.type?.name ?? ""
            
            //entity
            cell.entityHolderView?.isHidden = mes.entity?.name?.isEmpty ?? true
            if !(cell.entityHolderView?.isHidden)!{
                cell.entityImg?.sd_setImage(with: URL(string: (mes.entity?.icon)!), completed: nil)
                cell.entityNameLbl?.text = mes.entity?.name
            }
            //closure date
            cell.closureDateHolderView.isHidden = mes.expectedClosureDate?.isEmpty ?? true
            cell.closureDateLbl?.text = mes.expectedClosureDate?.getDate(format: inputFormat)?.getFormattedString(format: outputFormat)
            
            //submitted date
            cell.submittedOnLbl.text = mes.createDate?.getDate(format: inputFormat)?.getFormattedString(format: outputFormat)
            //location
            if mes.location?.name != nil{
                cell.locationHolderView.isHidden = false
                cell.addressLine1Lbl.text = mes.location?.name
                cell.addressLine2Lbl.text = mes.location?.description
            }else{
                cell.locationHolderView.isHidden = true
            }
            return cell
        case 3:
            if !(mes.caseDetails?.text?.isEmpty ?? true) {
                let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.CaseDetailTableViewCell.rawValue) as! CaseDetailTableViewCell
                cell.bodyLbl.text = mes.caseDetails?.text
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.CaseDetailTableViewCell2.rawValue) as! CaseDetailTableViewCell
                if cell.playerView?.miniPlayerVoiceURL == nil {
                    if mes.isCaseDetailsRecordDownloaded(){
                        cell.playerView?.miniPlayerVoiceURL = mes.caseDetailsRecordFullUrl
                    }else{
                        cell.progressIndicator?.startAnimating()
                        self.viewModel.downloadCaseDetailsAudio().asObservable().subscribe(onNext: { (url) in
                            if url != nil{
                                cell.progressIndicator?.stopAnimating()
                                cell.playerView?.miniPlayerVoiceURL = url
                            }
                        }).disposed(by: self.disposeBag)
                    }
                }
                
                
                return cell
            }
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.CommentsTableViewCell.rawValue) as! CommentsTableViewCell
            cell.setData(comments: mes.additionalComments)
            
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: CaseDetailCellID.AttachementTableViewCell.rawValue) as! AttachementTableViewCell
            cell.setData(attachments: mes.attachments,click: {
                mattachment,completion in
                // || mattachment.url?.range(of: "/Incident") != nil to be removed when upload is complete
                
                if mattachment.url?.range(of: "/Incident") != nil {
                    self.openUrl(withUrl:URL(fileURLWithPath: mattachment.url!) )
                    completion()
                }
                
                else if mattachment.isAttachmentDownloaded()  {
                    
                    self.openUrl(withUrl: mattachment.attachmentFullUrl)
                    completion()
                }else{
                    
                    self.viewModel.downloadAttachment( attachment: mattachment, onComplete: { (fileUrl) in
                        self.openUrl(withUrl: fileUrl)
                        completion()
                    })
                }
            })
            
            return cell
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func openUrl(withUrl url:URL){
        let docController:UIDocumentInteractionController = UIDocumentInteractionController.init(url: url)
        docController.delegate = self
        hideFloatingMenu()
        docController.presentPreview(animated: true)
    }
    
    @objc func answerSubmitted(_ sender: Any){
        let pollBtn = (sender as! UITapGestureRecognizer).view as! PollButton
        self.answers[pollBtn.answer!.qIndex!] = pollBtn.answer!.id!
        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: UITableViewRowAnimation.none)
    }
    
}
extension CaseDetailsViewController : UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        displayFloatingMenu()
    }
}
