//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift

class PollResultView : UIView{
 
    @IBOutlet weak var container: UIView!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var resultLabel: LocalizedLabel!
    
    @IBOutlet weak var resultTextBtn: LocalizedButton!
    
    var answer: MyCommunityModel.Poll.Answer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    init(frame: CGRect,answer: MyCommunityModel.Poll.Answer){
        super.init(frame: frame)
        self.answer = answer
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "PollResultView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        setupUiData()
    }
   
   
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
 
 
    func setupUiData(){
        self.resultTextBtn.setTitle(answer.text, for: .normal)
        self.resultLabel.text = "\(answer.result ?? "0")%"
        let color = UIColor(hexString: answer.color!)
        self.progressBar.progressTintColor = color
        self.progressBar.progress =  Float(Float(answer.result!)!/100)
        self.container.layer.borderWidth = 1
        self.container.layer.borderColor = color.cgColor
    }
}
