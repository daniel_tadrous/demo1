//
//  TalkToUsDraftsTableCell.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
class TalkToUsDraftsTableCell: UITableViewCell {
   
    @IBOutlet weak var orangeBg: UIView?
    @IBOutlet weak var deleteIcon: UILabel?
    var viewData: Any?
    var rowIndex:Int!
    var isEditMode: Bool!
    @IBOutlet weak var draftView: DraftView!
    var onDeleteClick: (()->Void)?
    @IBAction func deleteBtnClickHandler(_ sender: UIButton) {
        onDeleteClick?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    private func commonInit(){
        self.draftView.addRoundCorners(radious: 5)
        self.orangeBg?.addRoundAllCorners(radious: 5)
    }
}
