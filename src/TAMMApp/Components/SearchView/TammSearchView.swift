//
//  TammSearchView.swift
//  TAMMApp
//
//  Created by Daniel on 7/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

@IBDesignable class TammSearchView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var searchIconLabel: UILabel!
    
    @IBInspectable
    var PlaceholderTextContent: String? {
        didSet {
            searchField.placeholder = PlaceholderTextContent
        }
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var searchField: LocalizedTextField!

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
       // commonInit()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let nibName = "TammSearchView"
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self,options: nil).first as? UIView
    }
    private func commonInit(){
        self.searchField.placeholder = self.PlaceholderTextContent ?? ""
        self.addRoundCorners(radious: 30)
        self.contentView.addRoundCorners(radious: 30)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    public func applyColorTheme(){
        commonInit()
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        innerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        searchField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
        
        searchIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        
    }

}
