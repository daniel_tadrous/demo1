//
//  ApiTest.swift
//  TAMMAppTests
//
//  Created by kerolos on 4/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import XCTest
import RxSwift
import ObjectMapper

@testable import TAMMApp

class ApiTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAspectsOfLifeCall() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectation = XCTestExpectation(description: "call is done")

        let service = ApiServiceAspectsOfLife()
        let aspectsOfLife = service.getAspectsOfLife()
        aspectsOfLife.subscribe(onNext:{ a in
            print (a.toJSON())
            XCTAssert(a[0].AspectOfLifeID == 0)
            
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
