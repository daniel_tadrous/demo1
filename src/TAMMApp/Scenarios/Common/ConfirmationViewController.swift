//
//  IncidentConfirmationViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

enum ConfirmationType{
    case collaborate
}

protocol ConfirmationViewControllerDelegate: class{
    func confirmationDidFinish(type: ConfirmationType)
}
class ConfirmationViewController: TammViewController , CommonStoryboardLodable{
    
    @IBOutlet weak var titleLabel: LocalizedLabel!
    @IBOutlet weak var summaryLabel: LocalizedLabel!
    @IBOutlet weak var detailsLabel: LocalizedLabel!
    @IBOutlet weak var refrenceNoLbl: UILabel!

    @IBOutlet weak var goToLabel: LocalizedLabel!

    @IBOutlet weak var gotoIconLabel: UILabel!
    @IBOutlet var goToIconGestuerRecognizer: UITapGestureRecognizer!
    @IBOutlet var goToLabelGestureRecognizer: UITapGestureRecognizer!
    public weak var delegate: ConfirmationViewControllerDelegate?
    public var viewModel:ConfirmationViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideFloatingMenu()
//        goToMyLockerLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToMyLocker)))
//        myLockerIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToMyLocker)))
        refrenceNoLbl.addRoundCorners(radious: 25, borderColor: UIColor.turquoiseBlue)
        refrenceNoLbl.layer.cornerRadius = 25
        if #available(iOS 11.0, *) {
            if(L102Language.currentAppleLanguage().contains("ar")){
                refrenceNoLbl.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                refrenceNoLbl.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
        } else {
            // Fallback on earlier versions
        }
        
        refrenceNoLbl.clipsToBounds = true
        refrenceNoLbl.setNeedsLayout()
        
        
        
        if viewModel.reference == nil {
            refrenceNoLbl.isHidden = true
        } else{
            refrenceNoLbl.text = viewModel.reference
        }
        
        titleLabel.text = viewModel.title
        summaryLabel.text = viewModel.summary
        goToLabel.text = viewModel.gotoLabel
        gotoIconLabel.text = viewModel.iconLetter
        
        
    }

    @IBAction func goTOIconOrLabelPressed(_ sender: UITapGestureRecognizer) {
        self.displayFloatingMenu()
        delegate?.confirmationDidFinish(type: viewModel.type)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setNavigationBar(title: L10n.confirmation, willSetSearchButton: false)
        
    }
    
}
