//
//  SideMenuViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import SideMenu
protocol SideMenuViewControllerDelegate: class{
    func logoutPressed()
    func mysupportPressed()
    func myCommunityPressed()
    func myUpdatesPressed()
    func aspectsOfLifePressed()
    func newsNSocialPressed()
    func sideMenuCoCreatePressed()
    func governmentEntitiesPressed()
    func changeLanguagePressed()
    func newsAndEventsPressed()
    func settingsPressed()
    func myEventsIsPressed()
}


class SideMenuViewController: TammViewController, SideMenuStoryboardLodable {
    
    
    
    //@IBOutlet weak var aspectsOfLifeLabel: UILabel!
    @IBOutlet weak var abuDhabiGovLabel: UILabel!
    @IBOutlet weak var separtor1: UIView!
    //@IBOutlet weak var myUpdatesLabel: UILabel!
    @IBOutlet weak var myEventsLabel: UILabel!
    //@IBOutlet weak var myCommunityLabel: UILabel!
    @IBOutlet weak var mySupportLabel: UILabel!
    @IBOutlet weak var separator2: UIView!
    @IBOutlet weak var coCreateLabel: UILabel!
    @IBOutlet weak var newsNSocialLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var separator3: UIView!
    @IBOutlet weak var logoutIcon: UIView!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var aboutTammLabel: LocalizedLabel!
    @IBOutlet weak var aboutAbuDhabiLabel: LocalizedLabel!
    
    @IBOutlet weak var separator4: UIView!
    
    @IBOutlet weak var arrow: UILabel!
    @IBOutlet weak var separator5: UIView!
    
    @IBOutlet weak var logoutLabel: UILabel!
    var logout:Bool = false
    weak var delegate: SideMenuViewControllerDelegate?
    
    private func dissmissSideMenu(){
        if(L102Language.currentAppleLanguage().contains("ar")){
            SideMenuManager.default.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
        }
        else{
            SideMenuManager.default.menuRightNavigationController?.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        logout = false
        var tabGesture = UITapGestureRecognizer(target: self, action: #selector(handelLogoutTab))
        logoutLabel.isUserInteractionEnabled = true
        logoutLabel.addGestureRecognizer(tabGesture)
        
        if L10n.Lang == "ar" {
            arrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        
        abuDhabiGovLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openEntities )))
//        myUpdatesLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openUpdates )))
        
        myEventsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openEvents )))
        
//        myCommunityLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openMyCommunity )))
        
        aboutAbuDhabiLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openAboutAbuDhabi )))
        aboutTammLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openAboutTamm)))
        
        mySupportLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openMySupport )))
        
        coCreateLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openCoCreate )))
        
        newsNSocialLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openNewsNSocial )))
        
        settingsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector( openSettings )))
        
        languageLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeLanguage)))
        
        logoutIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handelLogoutTab)))
        tabGesture = UITapGestureRecognizer(target: self, action: #selector( handelAspectsOfLifeTab ))
        //aspectsOfLifeLabel.isUserInteractionEnabled = true
        //aspectsOfLifeLabel.addGestureRecognizer(tabGesture)
        if L102Language.currentAppleLanguage().contains("en") {
            languageLbl.font = UIFont(name: "Swissra-Bold", size: 17)
        }
        else{
            languageLbl.font = UIFont(name: "CircularStd-Bold", size: 17)
        }
        // Do any additional setup after loading the view.
    }
    @objc func changeLanguage(){
        dissmissSideMenu()
        AppDelegate.changeLang(navigationController: self.navigationController)
    }
    @objc func openEntities(){
        dissmissSideMenu()
        self.delegate?.governmentEntitiesPressed()
    }
    
    @objc func openUpdates(){
        dissmissSideMenu()
        self.delegate?.myUpdatesPressed()
    }
    
    @objc func openEvents(){
        dissmissSideMenu()
        self.delegate?.myEventsIsPressed()
        
    }
    
    @objc func openMyCommunity(){
        dissmissSideMenu()
        self.delegate?.myCommunityPressed()
    }
    
    @objc func openMySupport(){
        dissmissSideMenu()
        self.delegate?.mysupportPressed()
    }
    
    @objc func openCoCreate(){
        dissmissSideMenu()
        self.delegate?.sideMenuCoCreatePressed()
    }
    
    @objc func openNewsNSocial(){
        dissmissSideMenu()
        self.delegate?.newsNSocialPressed()
    }
    @objc func openNews(){
        dissmissSideMenu()
        self.delegate?.newsAndEventsPressed()
        self.displayToast(message: "News & Events Clicked")
    }
    
    
    @objc func openSettings(){
        dissmissSideMenu()
        self.delegate?.settingsPressed()
//        self.displayToast(message: L10n.commingSoon)
    }
    
    @objc func handelAspectsOfLifeTab(){
        dissmissSideMenu()
        self.delegate?.aspectsOfLifePressed()
        
    }
    
    @objc func openAboutTamm(){
        self.displayToast(message: L10n.commingSoon)
    }
    
    @objc func openAboutAbuDhabi(){
        self.displayToast(message: L10n.commingSoon)
    }
    
    @objc func handelLogoutTab(){
        print("logout pressed")
        dissmissSideMenu()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.removeFloatingMenu()
        self.logout = true
        self.delegate?.logoutPressed()
        
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if(!logout){
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addfloatingMenu()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.backgroundColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1)
        view.isOpaque = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.removeFloatingMenu()
        
        
        
        
    }
    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
        self.separtor1.frame = CGRect(origin: self.separtor1.frame.origin, size: CGSize(width: self.separtor1.frame.width , height: 1))
        self.separator3.frame = CGRect(origin: self.separator3.frame.origin, size: CGSize(width: self.separator3.frame.width , height: 1))
        self.separator2.frame = CGRect(origin: self.separator2.frame.origin, size: CGSize(width: self.separator2.frame.width , height: 1))
        self.separator4.frame = CGRect(origin: self.separator4.frame.origin, size: CGSize(width: self.separator4.frame.width , height: 1))
        self.separator5.frame = CGRect(origin: self.separator5.frame.origin, size: CGSize(width: self.separator5.frame.width , height: 1))
        self.view.bringSubview(toFront: separtor1)
        self.view.bringSubview(toFront: separator3)
        self.view.bringSubview(toFront: separator2)
        self.view.bringSubview(toFront: separator4)
        self.view.bringSubview(toFront: separator5)
    }
    override func viewDidAppear(_ animated: Bool) {
        view.backgroundColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1)
        view.isOpaque = true

        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
