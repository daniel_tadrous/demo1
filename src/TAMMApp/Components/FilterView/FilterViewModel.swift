//
//  FilterViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
import EventKit

protocol filterViewModelDelegate {
    var apiServiceObject: ApiFilterServiceType{get}
}

class FilterViewModel: ShareViewModel{
    
    private(set) var currentLocationViewData: Variable<CLLocation?> = Variable(nil)
    private(set) var categoriesViewData: Variable<[String]?> = Variable([])
    private(set) var locationsViewData: Variable<[String]?> = Variable(nil)
    private(set) var locationsQuery: Variable<String?> = Variable(nil)
    fileprivate var disposeBag = DisposeBag()
    fileprivate var isGetLocations:Bool!
    
    private var apiService: ApiFilterServiceType!
    fileprivate var locationService:LocationServiceType!
    init(apiService: ApiFilterServiceType,isGetLocations:Bool,locationService:LocationServiceType) {
        super.init()
        self.apiService = apiService
        self.isGetLocations = isGetLocations
        self.locationService = locationService
        self.setupBinding()
        getCategories()
    }
    
    fileprivate func getCategories(){
            apiService.getFilterCategories()
                .subscribe(onNext: { [unowned self] cats in
                    for cat in cats{
                        self.categoriesViewData.value?.append(cat.title!)
                    }
                }).disposed(by: disposeBag)
    }
    
    fileprivate func setupBinding(){
        if(isGetLocations){
            locationsQuery.asObservable().subscribe(onNext:{ [unowned self] locations in
                self.getLocations()
            }).disposed(by:disposeBag)
        }
    }
    
    fileprivate func getLocations(){
        if  (locationsQuery.value != nil) && !(locationsQuery.value?.isEmpty)!
        {
            apiService.getFilterLocations(q: locationsQuery.value!)
                .subscribe(onNext: { [unowned self] locations in
                    self.locationsViewData.value = locations
                }).disposed(by: disposeBag)
        }else{
            self.locationsViewData.value  = nil
        }
    }
    
}

extension FilterViewModel: LocationServiceTypeDelegate{
    func startLocationService(){
        self.locationService.start(delegate: self)
    }
    func onLocationUpdated(location: CLLocation) {
        self.currentLocationViewData.value = location
    }
}
