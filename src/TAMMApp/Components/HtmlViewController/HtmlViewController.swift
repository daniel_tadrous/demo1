//
//  HtmlViewController.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift



class HtmlViewController: TammViewController,HtmlStoryboardLoadable{

  //  @IBOutlet weak var htmlLabel: UILabel!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var factsAndFiguresWebView: UIWebView!
    static var viewModelStatic:HtmlViewModel!
    public var viewModel:HtmlViewModel!{
        get{
            return HtmlViewController.viewModelStatic
        }
        set{
            HtmlViewController.viewModelStatic = newValue
        }
    }
    fileprivate var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewModel.title
        factsAndFiguresWebView.clipsToBounds = true
        factsAndFiguresWebView.layer.cornerRadius = 5
        parentView.addRoundCorners(radious: 5)
        parentView.layer.shadowOpacity = 0.1
        
        setupUIBinding()
    }


    
    fileprivate func setupUIBinding(){
        
        viewModel.content.asObservable().subscribe(onNext: { response in
            if response != nil {
                self.factsAndFiguresWebView.loadHTMLString(response!, baseURL: nil)
            }
            
        }).disposed(by: disposeBag)
    }
    
    
}
