//
//  User.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit
import RxSwift

class User: NSObject, Mappable, NSCoding{

    private static var userVar: Variable<User?> = Variable(User.getUser())
    public static var userObservable: Observable<User?> {
        return userVar.asObservable()
    }
    private static func notifyTheSubscribers(user: User?){
        User.userVar.value = user
    }
    
    static let userKey:String = "UserInfo"
    var uuID: String?
    var userType: String?
    var idn: String?
    var firstNameEN :String?
    var firstNameAR :String?
    var lastNameEN :String?
    var lastNameAR :String?
    var fullNameEN :String?
    var fullNameAR :String?
    
    var computedFullNameEN: String{
        return (firstNameEN ?? "") + " " + (lastNameEN ?? "")
    }
    var computedFullNameAR: String{
        return (firstNameAR ?? "") + " " + (lastNameAR ?? "")
    }

    var photo:String?
    var email :String?
    var sub:String?
    var mobile: String?
    
    required init?(map: Map) {
        email = (map.JSON["email"] as? String) ?? (map.JSON["emailNotVerified"] as? String)
        mobile = (map.JSON["mobile"] as? String) ?? (map.JSON["mobileNotVerified"] as? String)
    }
    
    // Mappable
    func mapping(map: Map) {
        uuID            <- map["uuid"]
        userType         <- map["userType"]
        idn  <- map["idn"]
        firstNameEN         <- map["firstnameEN"]
        firstNameAR  <- map["firstnameAR"]
        lastNameEN   <- map["lastnameEN"]
        lastNameAR  <- map["lastnameAR"]
        fullNameEN            <- map["fullnameEN"]
        fullNameAR         <- map["fullnameAR"]
        photo  <- map["photo"]
        sub   <- map["sub"]
    }
    
    required init(coder decoder: NSCoder) {
        self.uuID = decoder.decodeObject(forKey: "uuID") as? String ?? ""
        self.userType = decoder.decodeObject(forKey: "userType") as? String ?? ""
        self.idn = decoder.decodeObject(forKey: "idn") as? String ?? ""
        self.firstNameEN = decoder.decodeObject(forKey: "firstNameEN") as? String ?? ""
        self.firstNameAR = decoder.decodeObject(forKey: "firstNameAR") as? String ?? ""
        self.lastNameEN = decoder.decodeObject(forKey: "lastNameEN") as? String ?? ""
        self.lastNameAR = decoder.decodeObject(forKey: "lastNameAR") as? String ?? ""
        self.fullNameEN = decoder.decodeObject(forKey: "fullNameEN") as? String ?? ""
        self.fullNameAR = decoder.decodeObject(forKey: "fullNameAR") as? String ?? ""
        self.email = decoder.decodeObject(forKey: "email") as? String ?? ""
        self.sub = decoder.decodeObject(forKey: "sub") as? String ?? ""
        
        self.photo = decoder.decodeObject(forKey: "photo") as? String ?? ""
        
        self.mobile = decoder.decodeObject(forKey: "mobile") as? String ?? ""
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(uuID, forKey: "uuID")
        aCoder.encode(userType, forKey: "userType")
        aCoder.encode(idn, forKey: "idn")
        aCoder.encode(firstNameEN, forKey: "firstNameEN")
        aCoder.encode(firstNameAR, forKey: "firstNameAR")
        aCoder.encode(lastNameEN, forKey: "lastNameEN")
        aCoder.encode(lastNameAR, forKey: "lastNameAR")
        aCoder.encode(fullNameEN, forKey: "fullNameEN")
        aCoder.encode(fullNameAR, forKey: "fullNameAR")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(sub, forKey: "sub")
        
        aCoder.encode(photo, forKey: "photo")
        
        aCoder.encode(mobile, forKey: "mobile")
        
    }
    
    class func getUser()->User?{

        guard let archivedUserInfo = UserDefaults.standard.object(forKey: User.userKey) as? Data else{
            return nil
        }
        
        guard let userInfo = NSKeyedUnarchiver.unarchiveObject(with: archivedUserInfo) as? User else{
            return nil
        }
        return userInfo;
    }

    func save(){
        let archivedUserInfo = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(archivedUserInfo, forKey: User.userKey)
        
        UserDefaults.standard.synchronize()

        User.notifyTheSubscribers(user: self)

    }
    
    class func clear(){
        UserDefaults.standard.set(nil, forKey: User.userKey)
        
        UserDefaults.standard.synchronize()
    }
    
    public func getUserImage() -> UIImage? {
        return type(of:self).getUserImageFromBase64String(string: self.photo)
    }
    
    static func getUserImageFromBase64String(string: String?) -> UIImage?{
        let encodedImageData = string ?? ""
        if let imageData = Data(base64Encoded: encodedImageData , options: []){
            let image = UIImage(data: imageData)
            return image
        }
        return UIImage(named: "profile-image")
    }
    
}
