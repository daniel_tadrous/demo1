//
//  MyServicesViewController.swift
//  TammUI
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import UIKit
import RxSwift

protocol AspectsOfLifeListViewControllerSelectionDelegate: class {
    func didSelectAspect(itemId: String, title:String)
}

class AspectsOfLifeListViewController:TammViewController, UITextFieldDelegate, AspectsOfLifeListDetailsStoryboardLodable{
    public var viewModel:MyServicesViewModel!
    public weak var delegate: AspectsOfLifeListViewControllerSelectionDelegate?
    
    fileprivate let reuseIdentifier = "AspectsOfLife"
    @IBOutlet weak var searchText: UITammTextField!
    @IBOutlet weak var myServicesView: AspectsOfLifeTableView!
    @IBOutlet weak var myServicesViewBottomConstrain: NSLayoutConstraint!
    var loadMoreButton: UITammResizableButton!
    fileprivate var filteredServices:[AspectOfLifeViewData] = []
    fileprivate var initialServicesWillBeDisplayed:[AspectOfLifeViewData] = []
    fileprivate var lastInitialServicesIndex = 0
    fileprivate var allServices:[AspectOfLifeViewData] = []
    fileprivate let minCells = 5
    fileprivate let heightOfShowMoreButton:CGFloat = 50
    
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchText.addTarget(self, action: #selector(searchTextChanged), for: .editingChanged)
        self.myServicesView.tableView.backgroundView = UIView();
        self.myServicesView.tableView.backgroundView?.backgroundColor = UIColor.clear
        myServicesView.tableView.backgroundColor = UIColor.clear
        searchText.delegate = self
//        viewModel = MyServicesViewModel()
        
        myServicesView.delegate = self
        setupBinding()
        
        //self.myServicesView.addTableConstraints()
    }
    private func setupUI(){
        let startOfTable = myServicesView.frame.origin.y
        let EndOfScreen = UIScreen.main.bounds.height
        let heightOfTableSupposedToBe = EndOfScreen - startOfTable - heightOfShowMoreButton;
        let heightOfCells = allServices.count * myServicesView.defaultCellHeight
        
        if(CGFloat(heightOfCells) > heightOfTableSupposedToBe || allServices.count < minCells){
            // add show more buttons and
            var initialServicesWillBeDisplayed:[AspectOfLifeViewData] = []
            var currentHeight = 0
            for service in allServices{
                currentHeight += myServicesView.defaultCellHeight
                if(CGFloat(currentHeight) <= heightOfTableSupposedToBe || lastInitialServicesIndex < minCells ){
                    initialServicesWillBeDisplayed.append(service)
                    lastInitialServicesIndex += 1
                }
                else{
                    break;
                }
            }
            myServicesView.aspectsOfLife = initialServicesWillBeDisplayed
            if(allServices.count > 5){
                self.addLoadMore()
            }
            
        }
        else{
            myServicesView.aspectsOfLife = allServices
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchText.resignFirstResponder()
        return false
    }
    fileprivate func initiateLoadMoreButton(){
        loadMoreButton = UITammResizableButton()
        loadMoreButton.applyFontScalling = true
        loadMoreButton.frame = CGRect(x: 15, y: 0, width: UIScreen.main.bounds.width - 30, height: heightOfShowMoreButton )
        loadMoreButton.BorderColor = UIColor.init(hexString: "#00A1BD")
        loadMoreButton.ignoreWidthConstrints = true
        loadMoreButton.referenceHeigh = heightOfShowMoreButton
        loadMoreButton.applyRoundCorners = true
        loadMoreButton.cornerRadiusToHeightRatio = 0.5
        loadMoreButton.RTLFont = "Swissra-Bold"
        loadMoreButton.backgroundColor = UIColor.init(hexString: "#DFF4F7")
        loadMoreButton.addTarget(self, action: #selector(loadMoreBtnClicked), for: .touchUpInside)
        loadMoreButton.setTitle(L10n.show_all, for: .normal)
        loadMoreButton.setTitleColor(UIColor.init(hexString: "#004F66"), for: .normal)
        
        loadMoreButton.titleLabel?.font = UIFont.init(name: "CircularStd-Bold", size: 16)
        loadMoreButton.applyDimensions()
        
    }
    fileprivate func addLoadMore(){
        if(loadMoreButton == nil){
            initiateLoadMoreButton()
        }
        loadMoreButton.applyDimensions()

        var f = loadMoreButton.frame
        let f2 = CGRect(origin: f.origin, size: CGSize(width: f.width, height: loadMoreButton.currentHeight)) //) b.height = loadMoreButton.currentHeight
        loadMoreButton.frame = f2
        loadMoreButton.setNeedsLayout()

        loadMoreButton.layoutIfNeeded()
        myServicesView.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: loadMoreButton.currentHeight +  100))
        myServicesView.tableView.tableFooterView?.addSubview(loadMoreButton)
    }
    fileprivate func removeLoadMore(){
        myServicesView.tableView.tableFooterView = nil
        
    }
    @objc func searchTextChanged(){
        if(searchText.text != nil && !(searchText.text?.isEmpty)!){
            filteredServices = allServices.filter({( service : AspectOfLifeViewData) -> Bool in
                let text = searchText.text ?? ""
                return (service.AspectOfLifeHeader.lowercased().contains(text.lowercased()) || (service.AspectOfLifeDescription.lowercased().contains(text.lowercased())))
            })
            self.myServicesView.changeTable(items: filteredServices)
            self.removeLoadMore()
        }
        else{
            if(loadMoreButton != nil){
                self.myServicesView.changeTable(items: initialServicesWillBeDisplayed)
                addLoadMore()
            }
            else{
                self.myServicesView.changeTable(items: allServices)
            }
        }
    }
    @objc func loadMoreBtnClicked(){
        var restOfServices:[AspectOfLifeViewData] = []
        var index = lastInitialServicesIndex
        while(index < allServices.count){
            restOfServices.append(allServices[index])
            index += 1
        }
        self.removeLoadMore()
        myServicesView.displayMoreItems(items: restOfServices)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        myServicesView.tableView.frame = myServicesView.bounds
        myServicesView.tableView.backgroundView?.backgroundColor = UIColor.clear
        searchText.addRoundCorners(radious: 24)
        if(loadMoreButton != nil){
            loadMoreButton.applyCornersIfActive()
        }
        
    }
    
    private func setupBinding(){
        _ = viewModel.getMyServices()
        allServices = viewModel.services.value
        viewModel.services.asObservable().subscribeOn(MainScheduler.instance)
            .subscribe(onNext:{ [unowned self] ser in
            self.allServices = ser
            if(self.allServices.count > 0){
                self.setupUI()
                self.myServicesView.tableView.reloadData()
                if(self.loadMoreButton != nil){
                    self.loadMoreButton.applyCornersIfActive()
                }
            }
            }).disposed(by:disposeBag)
        
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if myServicesView.tableView.tableFooterView != nil{
            addLoadMore()
        }
    }
    
}

extension AspectsOfLifeListViewController : AspectsOfLifeTableViewDelegate{
    func selectedAspectsAtIndex(index: Int) {
        let itemId = allServices[index].AspectOfLifeID
        let title  = allServices[index].AspectOfLifeHeader
        delegate?.didSelectAspect(itemId: "\(itemId)", title:title)
    }
    
    
}
