//
//  ContainerViewController.swift
//  TAMMApp
//
//  Created by Daniel on 9/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ContainerViewController: TammViewController {
    @IBOutlet weak var containerView: UIView!

    var selectedViewController:UIViewController!

}
