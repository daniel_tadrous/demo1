//
//  MyLockerViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MyLockerViewControllerDelegate {
    func openMyDocuments();
    func openMySupport();
    func openMyEvents()
    func openMyPayments()
    func openMySubscriptions();
}

enum MyLockerItems:Int {
    case MyPayments = 0
    case MySubscription = 1
    case MyEvents = 2
    case MySupport = 3
    case MyDocuments = 4
}
class MyLockerViewController: SuperCollectionViewController, UICollectionViewDelegateFlowLayout, MyLockerStoryboardLodable {
    
    static var storyboardName: String = "MyLocker"
    
    public var delegate:MyLockerViewControllerDelegate?
    public var viewModel:MyLockerViewModel?
    public var draggableButton:UICircularImage!
    fileprivate let reuseIdentifier = "MyLockerCell"
    fileprivate let numberOfSections = 1
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate let intentRatio: CGFloat = 0.041
    private var sectionInsets:UIEdgeInsets?
    private var screenWidth:CGFloat?
    //private var floatingMenu:FloatingMenuView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MyLockerViewModel();
        screenWidth = UIScreen.main.bounds.width;
        sectionInsets = UIEdgeInsets(top: 20.0, left: screenWidth! * intentRatio, bottom: 20.0, right: screenWidth! * intentRatio)
        self.collectionView?.setNeedsLayout()
        self.collectionView?.layoutIfNeeded()
        //floatingMenu = FloatingMenuView(frame: UIScreen.main.bounds)
        //self.view.addSubview(floatingMenu)
        self.setNavigationBar(title: L10n.myLocker)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView?.reloadData()
        collectionView?.backgroundColor = #colorLiteral(red: 0.9499574304, green: 0.9501777291, blue: 0.9562802911, alpha: 1).getAdjustedColor()
        
        self.view.backgroundColor = #colorLiteral(red: 0.9499574304, green: 0.9501777291, blue: 0.9562802911, alpha: 1).getAdjustedColor()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // floatingMenu.bringSubview(toFront: floatingMenu.draggableButton)
        //if(draggableButton == nil){
    //draggableButton = self.initiateDraggableButton()
       // }
        //self.setDraggableButton(draggable: draggableButton)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = (sectionInsets?.left)! * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    //3
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MyLockerCollectionViewCell
        cell.backgroundColor = UIColor.white.getAdjustedColor()
        cell.isSelected = true
        cell.lblName.text = viewModel?.getMyLockerSectionName(index: indexPath.row)
        cell.lblName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
        
        cell.icon.FontImage = (viewModel?.getMyLockerSectionIcon(index: indexPath.row))!
        cell.icon.FontImageColor = UIColor.init(hexString: (viewModel?.getMyLockerSectionIconColor(index: indexPath.row))!)
        cell.icon.backgroundColor = UIColor.init(hexString: (viewModel?.getMyLockerSectionIconBackgroundColor(index: indexPath.row))!)
        cell.layer.cornerRadius = 10
        
        // drop shadow
        cell.layer.shadowColor = UIColor.lightGray.getAdjustedColor().cgColor;
        cell.layer.shadowOpacity=0.5;
        cell.layer.shadowRadius=2.0;
        cell.layer.shadowOffset = CGSize(width:0, height:0);
        cell.layer.masksToBounds = false;
        // Configure the cell
        return cell
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets!
    }
    
    //4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets!.left
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case MyLockerItems.MySupport.rawValue:
            delegate?.openMySupport()
        case MyLockerItems.MyEvents.rawValue:
            delegate?.openMyEvents()
        case MyLockerItems.MyDocuments.rawValue:
            delegate?.openMyDocuments()
        case MyLockerItems.MyPayments.rawValue:
             delegate?.openMyPayments()
        case MyLockerItems.MySubscription.rawValue:
            delegate?.openMySubscriptions()
        default:
            Toast(message:L10n.commingSoon).Show()

        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    
    
}

