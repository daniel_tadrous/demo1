//
//  ArcGisUtil.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import UIKit
import ArcGIS

class ArcGisUtil {
   // private static
    
    private static var geocodeParameters: AGSGeocodeParameters!
    private static var reverseGeocodeParameters: AGSReverseGeocodeParameters!
    private static var locatorTask: AGSLocatorTask!
    private static var locatorTaskOperation: AGSCancelable?
    private static let country =  "ARE"
    private static let lang = L102Language.currentAppleLanguage().contains("ar") ? "AR": "EN"

    private static var agsPoint: AGSPoint!
    
    class func reverseGeocode(point:AGSPoint, completionHandler:@escaping (String?)->() ) {
        
        //initialize geocode params
        self.geocodeParameters = AGSGeocodeParameters()
        //        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["*"])
        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["Match_addr"])
        //        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["LongLabel"])
        // will return alot of results it was 75 but I will try 50 for a while
        self.geocodeParameters.minScore = 50
        self.geocodeParameters.outputLanguageCode = lang
        self.geocodeParameters.countryCode = country
        
        //initialize reverse geocode params
        self.reverseGeocodeParameters = AGSReverseGeocodeParameters()
        self.reverseGeocodeParameters.maxResults = 1
        self.reverseGeocodeParameters.resultAttributeNames.append(contentsOf: ["*"])
        self.reverseGeocodeParameters.outputLanguageCode = lang
        //        self.reverseGeocodeParameters.countryCode = country
        
        self.locatorTask = AGSLocatorTask(url: URL(string: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer")!)
        
        //normalize the point
        let normalizedPoint = AGSGeometryEngine.normalizeCentralMeridian(of: point) as! AGSPoint
        
        //cancel all previous operations
        self.locatorTaskOperation?.cancel()
        
        
        //create a graphic and add to the overlay
        
        self.agsPoint = normalizedPoint
        
        //perform reverse geocode
        self.locatorTaskOperation = self.locatorTask.reverseGeocode(withLocation: self.agsPoint, parameters: self.reverseGeocodeParameters!) { (results: [AGSGeocodeResult]?, error: Error?) -> Void in
            //var data : NSMutableDictionary = [String:AnyObject]() as! NSMutableDictionary
            print ("reverse geo code done")
            
            if let error = error as NSError? , error.code != NSUserCancelledError {
                //print error instead alerting to avoid disturbing the flow
                print(error.localizedDescription)
                completionHandler(nil)
                return
            }
            else {
                //if a result is found extract the required attributes
                //assign the attributes to the graphic
                //and show the callout
                
                if let results = results , results.count > 0 {
                    
                    //data.addEntries(from: results.first?.attributes ?? [:])
                    completionHandler(results[0].label)
                    return
                }
                else {
                    completionHandler(nil)
                    return
                    
                }
            }
           
        }
    }
    
    
    
}
