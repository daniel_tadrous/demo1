//
//  NewsTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var newsImage: UIImageView!
    
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var newsDayLabel: LocalizedLabel!
    
    @IBOutlet weak var newsMonthLabel: LocalizedLabel!
    
    @IBOutlet weak var newsYearLabel: LocalizedLabel!

    @IBOutlet weak var newsTitleLabel: LocalizedLabel!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsCategoryLabel: UILabel!
    @IBOutlet weak var newsCategoryFrame: UIView!
    
    @IBOutlet weak var newsCellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.addRoundCorners(radious: 17)
        
        newsImage.addRoundCorners(radious: 17)
        newsCategoryFrame.layer.cornerRadius = 5
        
        newsCategoryFrame.layer.borderWidth = 1
        cellView.addRoundCorners(radious: 17)
        if L10n.Lang == "en" {
            dateStackView.alignment = .bottom
        }else {
            dateStackView.alignment = .center
        }
        applyInvertColors()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func applyInvertColors() {
        newsCellView.backgroundColor = #colorLiteral(red: 0.9562581182, green: 0.9564779401, blue: 0.9625678658, alpha: 1).getAdjustedColor()
        containerView.backgroundColor = UIColor.white.getAdjustedColor()
        newsDayLabel.textColor = UIColor.white.getAdjustedColor()
        newsMonthLabel.textColor = UIColor.white.getAdjustedColor()
        newsYearLabel.textColor = UIColor.white.getAdjustedColor()
        newsTitleLabel.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        newsCategoryLabel.textColor = #colorLiteral(red: 0, green: 0.4316659272, blue: 0.526139617, alpha: 1).getAdjustedColor()
        newsCategoryFrame.layer.borderColor = UIColor.petrol.getAdjustedColor().cgColor
        newsCategoryFrame.backgroundColor = UIColor.white.getAdjustedColor()
        containerView.layer.borderColor = UIColor.petrol.getAdjustedColor().cgColor
    }

}
