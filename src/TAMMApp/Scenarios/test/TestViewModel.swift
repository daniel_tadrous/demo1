//
//  TestViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol TestViewModelDelegate: class {
    func testIsDone()
    func goToTest2Page()
}

class TestViewModel {
    
    weak var delegate: TestViewModelDelegate?
    
    func viewTest2(){
        delegate?.goToTest2Page()
    }
}

