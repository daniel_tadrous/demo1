//
//  AolServiceWithFaqsViewController.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage
enum Identifier : String{
    case CellSec0,CellSec1Ex,CellSec1Col,CellSec2,CellSec3,CellSec4,CellSec5Ex,CellSec5Col,CellSec6,CellSec7,SectionHeader,ViewMore,CellSec3ButtonFree
}
class AolServiceWithFaqsViewController: TammViewController, AolServiceWithFaqsStoryboardLodable {
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var serviceWithFaqsTableView: UITableView!
    public var viewModel: AolServiceWithFaqsViewModel?
    public var responsibleEntityViewModel: ResponsibleEntityViewModel?
    public var processViewModel: ProcessViewModel?
    private var isViewMorePressed :Bool = false
    var disposeBag = DisposeBag()
    var relatedJourneys:[JourneyModel]{
        get{
           return self.service?.relatedJourneys ?? []
        }
    }
    var faqs:[AspectOfLifeServiceFaqsModel]{
        get{
            return self.service?.faqs ?? []
        }
    }
    var service:ServiceModel?{
        get{
            return self.viewModel?.viewData.value
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serviceWithFaqsTableView.register(UINib(nibName: "AspectsOfLifeTableCellView", bundle: nil), forCellReuseIdentifier: Identifier.CellSec4.rawValue)
        self.serviceWithFaqsTableView.register(UINib(nibName: "ShowMoreCell", bundle: nil), forCellReuseIdentifier: Identifier.ViewMore.rawValue)
        self.serviceWithFaqsTableView.register(UINib(nibName: "SectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: Identifier.SectionHeader.rawValue)
        setupHooks()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupHooks(){
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] serviceDetails in
                if(serviceDetails == nil){
                    //TODO: should do somting ????
                }else{
                    self.serviceWithFaqsTableView.reloadData()
                    //self.title = serviceDetails?.subTopicServiceTitle
                    self.setNavigationBar(title: (self.viewModel?.serviceTitle.value!)!, willSetSearchButton: true)
                   
                    self.disposeBag = DisposeBag()
                }
            })
            .disposed(by: disposeBag)
    }
}

extension AolServiceWithFaqsViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 4,5:
            return 70
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
  
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view  = tableView.dequeueReusableHeaderFooterView(withIdentifier: Identifier.SectionHeader.rawValue)
        let label = view?.viewWithTag(1) as! UILabel
        label.text = L10n.getServiceSectionHeader(section: section)
        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel?.viewData.value == nil {
            return 0
        }
        switch section {
            case 3:
                return 3
            case 4:
                return self.relatedJourneys.count
            case 5:
                if isViewMorePressed {
                    return self.faqs.count
                }else {
                    return (Int(self.faqs.count/2)+1)
                }
            default:
                return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = (self.viewModel?.viewData.value!)!
        var cell: UITableViewCell!
        switch indexPath.section{
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec0.rawValue)
            let cellCasted = cell as! AolServiceFaqCell
            cellCasted.sec0TitleLabel?.text = self.viewModel?.viewData.value?.subTopicServiceTitle
            cellCasted.sec0DescLabel?.text = self.viewModel?.viewData.value?.subTopicServiceDescription
            if(item.icon != nil){
                ImageCachingUtil.loadImage(url: URL(string: item.icon!)!, dispatchInMain: true
                    , callBack: { (url, image) in
                            cellCasted.sec0Icon?.CircularFontImage = false
                            cellCasted.sec0Icon?.Image = image
                })
            }
            cellCasted.OnAlertBtnClick = {
                Toast.init(message: L10n.commingSoon).Show()
            }
            cellCasted.OnShareBtnClick = {
                Toast.init(message: L10n.commingSoon).Show()
            }
        case 1:
            if self.service?.isServcieCodeCollapsed ?? true {
                cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec1Col.rawValue)
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec1Ex.rawValue)
                
            }
            let cellCasted = cell as! ServiceCodeCell
            cellCasted.bodyLabel.attributedText = self.service?.details?.getAttributedString()
            cellCasted.titleLabel.text = "\(L10n.serviceCode) :\(self.service?.serviceCode ?? "")"
            cellCasted.OnReadMoreClick = {
                self.service?.isServcieCodeCollapsed = false
                tableView.reloadRows(at: [indexPath], with: .none)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            cellCasted.OnReadLessClick = {
                self.service?.isServcieCodeCollapsed = true
                tableView.reloadRows(at: [indexPath], with: .none)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            cellCasted.OnViewConditionsBtnClick = {
                self.openPopup(popupType: .conditions)
            }
            cell.layoutSubviews()
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec2.rawValue)
            let cellCasted = cell as! ServiceSec2Cell
            
            cellCasted.OnProcessClick = {
                self.openProcessView()
            }
            cellCasted.OnRequiredDocumentsClick = {
                self.openPopup(popupType: .documents)
            }
            cellCasted.OnFeesClick = {
                self.openPopup(popupType: .fees)
            }
            cellCasted.OnResponsibleEntityClick = {
                self.openResponsibilityPopUp()
            }
            cellCasted.OnEntityHQClick = {
                Toast.init(message: L10n.commingSoon).Show()
            }
            cellCasted.OnKioskClick = {
                Toast.init(message: L10n.commingSoon).Show()
            }
            cellCasted.OnMobileAppClick = {
                self.openPopup(popupType: .apps)
            }
            cellCasted.OnPhoneClick = {
                self.openPopup(popupType: .phones)
            }
            cellCasted.OnOnlineClick = {
                Toast.init(message: L10n.commingSoon).Show()
            }
            cellCasted.OnOtherLcationClick = {
                Toast.init(message: L10n.commingSoon).Show()
            }
            
        case 3:
            
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec3ButtonFree.rawValue)
                let cellCasted = cell as! ServiceSec3CellNoButton
                cellCasted.serviceMainLabel.text = L10n.getServiceSection3RowText(row: indexPath.row)
                cellCasted.secondLabel.text = "Contract Issuance"
                cellCasted.OnCellClicked = {
                    Toast.init(message: L10n.commingSoon).Show()
                }
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec3.rawValue)
                let cellCasted = cell as! ServiceSec3Cell
               cellCasted.label.text = L10n.getServiceSection3RowText(row: indexPath.row)
                cellCasted.OnCellClicked = {
                    if indexPath.row == 1{
                        self.openPopup(popupType: .related_services)
                    }else{
                        Toast.init(message: L10n.commingSoon).Show()
                    }
                }
            }
            
        case 4:
            let journey = self.relatedJourneys[indexPath.row]
            cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec4.rawValue)
            let cellCasted = cell as! AspectsOfLifeTableCell
            cellCasted.header.text = journey.AspectOfLifeHeader
            cellCasted.descriptionTxt.text = journey.AspectOfLifeDescription
            ImageCachingUtil.loadImage(url: journey.AspectOfLifeImageUrl, dispatchInMain: true
                , callBack: { (url, image) in
                        cellCasted.icon.CircularFontImage = false
                        cellCasted.icon.Image = image
            })
        case 5:
            if !isViewMorePressed && indexPath.row == (Int(self.faqs.count/2)) {
                // show View More Cell
               let myCell = tableView.dequeueReusableCell(withIdentifier: Identifier.ViewMore.rawValue) as! ShowMoreCell
                myCell.cellButtomdistance.constant = CGFloat(0)
                myCell.cellRightDistance.constant = CGFloat(0)
                myCell.cellLeftDistance.constant = CGFloat(0)
                myCell.showMoreBtn.setTitle(L10n.viewAll,for: .normal)
                myCell.showMoreBtn.addTarget(self, action: #selector(showMoreNewsPressed(_:)), for: .touchUpInside)
                return myCell
            }else{
                let serviceFaq  = self.faqs[indexPath.row]
                if serviceFaq.isCollapsed{
                    cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec5Col.rawValue)
                }else{
                    cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec5Ex.rawValue)
                    let cellCasted = cell as! AolServiceFaqCell
                    cellCasted.sec2DescLabel?.text = serviceFaq.aProp
                    
                }
                let cellCasted = cell as! AolServiceFaqCell
                cellCasted.sec2TitleLabel?.text = serviceFaq.qProp
                cellCasted.OnCollapseBtnClick = {
                    self.faqs[indexPath.row].isCollapsed = !serviceFaq.isCollapsed
                    self.serviceWithFaqsTableView.reloadRows(at: [indexPath], with: .automatic)
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
            
        case 6:
            cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec6.rawValue)
            let cellCasted = cell as! InformationHelpfulCell
            cellCasted.viewModel = self.viewModel
            cellCasted.reloadSection = {
                tableView.reloadRows(at: [indexPath], with: .automatic)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            if (self.viewModel?.isInformationHelpfulCellExpanded)!{
                cellCasted.textView.placeholder = L10n.additionalComments
                cellCasted.show()
            }else{
                cellCasted.hide()
                
            }
            cellCasted.rightBtn.addRoundAllCorners(radious: 22)
            cellCasted.crossBtn.addRoundAllCorners(radious: 22)
            cellCasted.setBtnsColors()
        case 7:
            cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec7.rawValue)
            let cellCasted = cell as! AolServiceFaqCell
            
            cellCasted.OnApplyBtnClick = {
                
            }
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: Identifier.CellSec7.rawValue) as! AolServiceFaqCell
        }
        
        return cell
    }
   
    @objc func showMoreNewsPressed(_ sender:UIButton) {
        isViewMorePressed = true
        serviceWithFaqsTableView.reloadData()
    }

}

extension AolServiceWithFaqsViewController {
    private func openPopup(popupType: PopupType){
        self.viewModel?.popupType.value = popupType
        var popup: ServicePopup!
        if popupType != .location{
            popup = ServicePopup(title: L10n.getServicePopupHeader(type: popupType), viewModel: self.viewModel!)
            
        }else{
            popup = ServicePopup(title: L10n.getServicePopupHeader(type: popupType), isMap:true, viewModel: self.viewModel!)
            
        }
        
        
        let height = UIApplication.shared.keyWindow!.bounds.height
        popup.center = popup.center.applying(CGAffineTransform.init(translationX: 0, y: height))
        // animate the view Up into the screen
        
        UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
        UIView.animate(withDuration: Dimensions.animationDuration1, animations: {
            popup.center = popup.center.applying(CGAffineTransform.init(translationX: 0, y: -height))
        }, completion: { (finished) in
            UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
        })
        
        
        
    }
    
    fileprivate func openProcessView() {
        if self.service != nil {
            let processView = ProcessView(frame: UIScreen.main.bounds, id: (self.service?.subTopicServiceId)!, viewModel: self.processViewModel!)
            UIApplication.shared.keyWindow!.addSubview(processView)
            //UIApplication.shared.keyWindow!.bringSubview(toFront: processView)
            //            processView.animateUpIntoScreen()
            // put the view down out of the csreen
            let height = UIApplication.shared.keyWindow!.bounds.height
            processView.center = processView.center.applying(CGAffineTransform.init(translationX: 0, y: height))
            // animate the view Up into the screen
            
            UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
            UIView.animate(withDuration: Dimensions.animationDuration1, animations: {
                processView.center = processView.center.applying(CGAffineTransform.init(translationX: 0, y: -height))
            }, completion: { (finished) in
                UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
            })
        }
    }
    
    fileprivate func openResponsibilityPopUp() {
        if self.service != nil {
            let responsibilityView = ResponsibilityView(frame:UIScreen.main.bounds, isOnlineService:   (self.service?.subTopicServiceIsOnlineService)!, id: (self.service?.subTopicServiceId)!, viewModel: self.responsibleEntityViewModel!)
            UIApplication.shared.keyWindow!.addSubview(responsibilityView)
            //            UIApplication.shared.keyWindow!.bringSubview(toFront: responsibilityView)
            let height = UIApplication.shared.keyWindow!.bounds.height
            responsibilityView.center = responsibilityView.center.applying(CGAffineTransform.init(translationX: 0, y: height))
            // animate the view Up into the screen
            
            UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
            UIView.animate(withDuration: Dimensions.animationDuration1, animations: {
                responsibilityView.center = responsibilityView.center.applying(CGAffineTransform.init(translationX: 0, y: -height))
            }, completion: { (finished) in
                UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
            })
            
            
        }
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.serviceWithFaqsTableView.reloadData()
    }
}
