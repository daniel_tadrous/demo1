//
//  HasMediaView.swift
//  TAMMApp
//
//  Created by marina on 6/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import Photos

protocol MediaViewDelegate {
    func addMediaClicked()
    func mediaIsRemoved(index:Int)
    func viewHasMedia()
}
@IBDesignable class HasMediaView: MediaBaseView{

    let cellID = "AttachmentCollectionViewCell"
    let cellHeight = 200
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addMediaTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaListHeightConstraint: NSLayoutConstraint!
    fileprivate let resuseIdentifier =  "Media"
    @IBOutlet weak var errorStack: UIStackView!
    @IBOutlet var contentView: UIView!

    
    @IBOutlet weak var contentStack: UIStackView!
    fileprivate func updateLayoutAfterListUpdate() {
        if(mediaList.count == 3){
            addMediaView.isHidden = true
            
        }else{
            addMediaView.isHidden = false
            
        }
        
        notSupportedLabel.isHidden = !notSupportedCheck
        exceedLimitLabel.isHidden = !exceededLimitCheck
        
        self.mediaListHeightConstraint.constant = CGFloat(ceil(Double(mediaList.count)/2.0) * 200.0)
       self.collectionView.reloadData()
        
    }
    
    public var mediaList:[Media] =  []{
        didSet{
            updateLayoutAfterListUpdate()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        collectionView.register(UINib(nibName: cellID, bundle: nil), forCellWithReuseIdentifier: cellID)
    }
    
    func loadViewFromNib() -> UIView? {
        let nibName = "HasMediaView"
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self,options: nil).first as? UIView
    }
    fileprivate func commonInit(){
        collectionView.isScrollEnabled = false
        notSupportedLabel.isHidden = true
        exceedLimitLabel.isHidden = true
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
   
    override func HasMedia() -> Bool {
        return true
    }
    
}
extension HasMediaView: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.mediaList[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! AttachmentCollectionViewCell
        cell.setData(attachment: item, click: { media, clck in
            
        })
        cell.onDeleteClick = {
            self.mediaList.remove(at: indexPath.row)
            self.delegate?.mediaIsRemoved(index: indexPath.row)
            self.collectionView.layoutIfNeeded()
            self.mediaListHeightConstraint.constant = self.collectionView.contentSize.height
            self.heightOfView =   self.contentStack.frame.origin.y + self.mediaListHeightConstraint.constant + 40
            self.delegate?.viewHasMedia()
        }
        self.mediaListHeightConstraint.constant = collectionView.contentSize.height
       
        heightOfView = contentStack.frame.origin.y +   self.mediaListHeightConstraint.constant + 40
    
        self.delegate?.viewHasMedia()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width-90.0)/2, height: 200.0)
    }
    
}
