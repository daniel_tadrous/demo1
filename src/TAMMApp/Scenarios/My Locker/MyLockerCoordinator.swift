//
//  MyLockerCoordinator.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import Photos
import RxSwift

protocol MyLockerCoordinatorDelegate {
    func myLockerIsDismissed();
}
class MyLockerCoordinator: NavigationCoordinator {
    var navigationController: UINavigationController
    
    
    // MARK: - Properties
    var tabBarController:UITabBarController?
    let container: Container
    var vc:UIViewController?
    //let authenticationService : AuthenticationService
    var delegate: MyLockerCoordinatorDelegate?
    weak var locationDelegate: MapParentViewControllerDelegate?
    
    init(container: Container, myLockerNavigationController: UINavigationController, tabBarControlor:UITabBarController) {
        self.container = container
        self.navigationController = myLockerNavigationController
        self.tabBarController = tabBarControlor
        // self.authenticationService = authenticationService
    }
    init(container: Container, myLockerNavigationController: UINavigationController) {
        self.container = container
        self.navigationController = myLockerNavigationController
    }
    // MARK: - Coordinator core
    
    func start() {
        if(self.vc != nil){
            tabBarController?.selectedViewController = self.vc
        }
        else{
            let vc = container.resolveViewController(MyLockerViewController.self)
            //self.navigationController = vc.navigationController!
            vc.delegate = self;
            vc.viewModel = MyLockerViewModel();
            //navigationController.pushViewController(vc, animated: true)
            self.navigationController.setNavigationBarHidden(true, animated: false)
            let navigationController = tabBarController?.selectedViewController as? UINavigationController
            self.navigationController = navigationController!
            navigationController?.setViewControllers([vc], animated: false)
        }
    }
    
    func startWithSupport(){
        if(self.vc != nil){
            tabBarController?.selectedViewController = self.vc
        }
        else{
            let vc = container.resolveViewController(MySupportMainViewController.self)
            //self.navigationController = vc.navigationController!
            vc.delegate = self;
            //navigationController.pushViewController(vc, animated: true)
            self.navigationController.setNavigationBarHidden(true, animated: false)
            let navigationController = tabBarController?.selectedViewController as? UINavigationController
            self.navigationController = navigationController!
            navigationController?.setViewControllers([vc], animated: false)
            
        }
    }
    
    func startWithMyEvents(){
        if(self.vc != nil){
            tabBarController?.selectedViewController = self.vc
        }
        else{
            let vc = container.resolveViewController(MyEventsViewController.self)
            vc.getData()
            self.navigationController.setNavigationBarHidden(true, animated: false)
            let navigationController = tabBarController?.selectedViewController as? UINavigationController
            self.navigationController = navigationController!
            navigationController?.setViewControllers([vc], animated: false)
            
        }
    }
    
    
    func startNewDraft(isFromOfflineScreen: Bool = false) {
        if(self.vc != nil){
            tabBarController?.selectedViewController = self.vc
        }
        else{
            let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
            //self.navigationController = vc.navigationController!
            vc.delegate = self;
            vc.viewModel.title = ""
            vc.viewModel.lunchMode = .NEW
            vc.isFromOfflineScreen = isFromOfflineScreen
            //navigationController.pushViewController(vc, animated: true)
            self.navigationController.setNavigationBarHidden(true, animated: false)
            let navigationController = tabBarController?.selectedViewController as? UINavigationController
            self.navigationController = navigationController!
            navigationController?.setViewControllers([vc], animated: false)
        }
    }
    
    func startTalkToUs() {
        if(self.vc != nil){
            tabBarController?.selectedViewController = self.vc
        }
        else{
            let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
            vc.delegate = self
            vc.delegate = self
            vc.viewModel.title = ""
            vc.viewModel.lunchMode = .NEW
            vc.selectedMessageType = 3
            self.navigationController.setNavigationBarHidden(true, animated: false)
            let navigationController = tabBarController?.selectedViewController as? UINavigationController
            self.navigationController = navigationController!
            navigationController?.setViewControllers([vc], animated: false)

        }
    }
     func openTalkToUs(){
        let vc = container.resolveViewController(TalkToUsLandingViewController.self)
        vc.delegate = self;
        self.navigationController.pushViewController(vc, animated: true)

        
    }
    
    private func openFAQ(){
        let vc = container.resolveViewController(FAQViewController.self)
        //self.navigationController = vc.navigationController!
        vc.viewModel.pageId.value = 1
        
        self.navigationController.pushViewController(vc, animated: true)
        
        
    }
    
    private func displayConfirmation(refrenceNumber: String){
        let vc = container.resolveViewController(IncidentConfirmationViewController.self)
        vc.delegate = self
        vc.viewModel.refrenceNumber = refrenceNumber
        vc.hidesBottomBarWhenPushed = true
        self.navigationController.setViewControllers([vc], animated: true)
    }
    
    private func goToMain(){
        let vc = container.resolveViewController(MyLockerViewController.self)
        //self.navigationController = vc.navigationController!
        vc.delegate = self;
        vc.viewModel = MyLockerViewModel();
        //navigationController.pushViewController(vc, animated: true)
        tabBarController?.tabBar.isHidden = false
        self.navigationController.setViewControllers([vc], animated: true)
    }
    private func openCheckStatus(){
        
       // let vc = container.resolveViewController(MsgsParentViewController.self)
        let vc = container.resolveViewController(MsgsParentViewController.self)
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    private func openTalkToUsLanding(){
        let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
        vc.delegate = self
        vc.viewModel.title = ""
        vc.viewModel.lunchMode = .NEW
        self.navigationController.pushViewController(vc, animated: true)
    }
    private func openTalkToUsMain(item:String){
        let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
        vc.viewModel.title = item
        vc.delegate = self
        vc.viewModel.lunchMode = .NEW
        self.navigationController.pushViewController(vc, animated: true)
    }
    private func openTalkToUsMain(type:Int,isFromOfflineScreen:Bool = false){
        let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
        vc.isFromOfflineScreen = isFromOfflineScreen
        vc.viewModel.title = ""
        vc.selectedMessageType = type
        vc.delegate = self
        vc.viewModel.lunchMode = .NEW
        self.navigationController.pushViewController(vc, animated: true)
    }
    private func openTalkToUsMain(item:TalkToUsItemViewDataType){
        let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
        vc.viewModel.item = item
        vc.delegate = self
        vc.viewModel.lunchMode = .NEW
       self.navigationController.pushViewController(vc, animated: true)
    }
    
    fileprivate func embedViewController(child: UIViewController , To parent:TalkToUsTabBarViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    private var navigationBarOriginalState: Bool!
    

    
}

extension MyLockerCoordinator: MyLockerViewControllerDelegate{
    func openMyDocuments() {
        let vC = container.resolveViewController(MyDocumentViewController.self)
        vC.delegate = self
        self.navigationController.pushViewController(vC, animated: true)
    }
    
    func openMySupport() {
        let vc = container.resolveViewController(MySupportMainViewController.self)
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    func openMyEvents() {
        let vC = container.resolveViewController(MyEventsViewController.self)
        //vC.delegate = self
        vC.viewModel.getMyEvents()
        vC.getData()
        self.navigationController.pushViewController(vC, animated: true)
    }
    

    func openMyPayments() {
        let vC = container.resolveViewController(MyPaymentsParentViewController.self)
        vC.delegate = self
        //vC.viewModel.getMyEvents()
        //vC.getData()
        self.navigationController.pushViewController(vC, animated: true)
    }
    func openMySubscriptions() {
        let vC = container.resolveViewController(MySubscriptionsParentViewController.self)
        vC.delegate = self
        self.navigationController.pushViewController(vC, animated: true)
    }
    
}

extension MyLockerCoordinator: MySupportMainViewControllerDelegate{
    func talkToUsClicked() {
        openTalkToUsLanding()
    }
    
    func checkStatusClicked() {
        openCheckStatus()
    }
    
    func faqClicked() {
        openFAQ()
    }
    
    
}

extension MyLockerCoordinator:TalkToUsTabBarViewControllerDelegate{
    func openTalkToUsNewTab( isFromOfflineScreen:Bool = false){
        let parentVC = self.navigationController.topViewController as? TalkToUsTabBarViewController
        vc = container.resolveViewController(TalkToUsLandingViewController.self)
        (vc as! TalkToUsLandingViewController).isFromOfflineScreen = isFromOfflineScreen
        //(vc as! TalkToUsLandingViewController).viewModel.title = item
        (vc as! TalkToUsLandingViewController).delegate = self
        self.embedViewController(child: (vc as! TalkToUsLandingViewController), To: parentVC!)
    }
    func openTalkToUsNew(item:String,typeId:Int, isFromOfflineScreen:Bool = false){
        let parentVC = self.navigationController.topViewController as? TalkToUsTabBarViewController
        vc = container.resolveViewController(TalkToUsViewController.self)
        (vc as! TalkToUsViewController).isFromOfflineScreen = isFromOfflineScreen
        (vc as! TalkToUsViewController).selectedMessageId = typeId
        (vc as! TalkToUsViewController).viewModel.title = item
        (vc as! TalkToUsViewController).delegate = self
        self.embedViewController(child: (vc as! TalkToUsViewController), To: parentVC!)
    }
    func openTalkToUsEditMode(draftId: String, isFromOfflineScreen:Bool = false){
        let parentVC = self.navigationController.topViewController as? TalkToUsTabBarViewController
        vc = container.resolveViewController(TalkToUsViewController.self)
        (vc as! TalkToUsViewController).isFromOfflineScreen = isFromOfflineScreen
        (vc as! TalkToUsViewController).draftId = draftId
        (vc as! TalkToUsViewController).delegate = self
        self.embedViewController(child: (vc as! TalkToUsViewController), To: parentVC!)
    }
    func openDrafts(isFromOfflineScreen:Bool = false){
        let parentVC = self.navigationController.topViewController as? TalkToUsTabBarViewController
        let vc = container.resolveViewController(TalkToUsDraftsViewController.self)
        vc.isFromOfflineScreen = isFromOfflineScreen
        vc.delegate = parentVC!
        self.embedViewController(child: vc, To: parentVC!)
    }
}

extension MyLockerCoordinator: TalkToUsViewControllerDelegate{
    func talkToUsMessageTypeSelected(id: Int,isFromOfflineScreen:Bool = false) {
        openTalkToUsMain(type: id, isFromOfflineScreen:isFromOfflineScreen)
    }
    
    func checkStatusClickedInTalkToUs() {
        openCheckStatus()
    }
    func talkToUsItemIsSelected(item:TalkToUsItemViewDataType){
        openTalkToUsMain(item:item)
    }
    
    func talkToUsItemIsSelected(item:String){
        openTalkToUsMain(item:item)
    }

}

extension MyLockerCoordinator: IncidentConfirmationViewControllerDelegate{
    func goToMyLocker() {
        
        self.goToMain()
    }
    
    
}
extension MyLockerCoordinator: TalkToUsSaveViewControllerDelegate{
    func openLocationPicker(_ locationDelegate: MapParentViewControllerDelegate, withSearchText: String?, mapPlace: MapPlaceObject?) {
        self.locationDelegate = locationDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.removeFloatingMenu()
        //        self.navigationController.setNavigationBarHidden(false, animated: false)
        //        self.tabBarController?.navigationController?.setNavigationBarHidden(true, animated: false)
        let vc = container.resolveViewController(MapParentViewController.self)
        vc.hidesBottomBarWhenPushed = true
        vc.delegate = self
        vc.initialTextSearch = withSearchText
        
        if mapPlace != nil {
            vc.selectedPlace = mapPlace
        }
        
        
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    
   
    func openCameraVC(allowed:Int) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.removeFloatingMenu()
        self.navigationController.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.navigationController?.setNavigationBarHidden(true, animated: false)
        let vc = container.resolveViewController(CameraViewController.self)
        vc.delegate =  self
        vc.numberOfImages = CGFloat(allowed)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    
    func openMediaActionbar(actionSheet: GalleryActionSheet){
        actionSheet.show(inView: (self.tabBarController?.view) ?? (self.vc?.view)!)
    }
    
    func displayErrorOverlay() {
        let errorView = ErrorView(frame:(self.tabBarController?.view.bounds)!)
        self.tabBarController?.view.addSubview(errorView)
        // vc.view.addSubview(errorView)
    }
    
    func openConfirmation(refrenceNumber: String) {
        tabBarController?.tabBar.isHidden = true
        self.displayConfirmation(refrenceNumber: refrenceNumber)
    }
    
    func openDraftsScreen(){
        
        let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
        vc.viewModel.title = ""
        vc.viewModel.lunchMode = .DRAFT
        vc.delegate = self
        vc.isFromOfflineScreen = false
        self.navigationController.viewControllers = [vc]
        
        
    }
    
    
}

protocol OfflineScreenDelegate: class{
   
    func openPendingDrafts()
}
extension MyLockerCoordinator: OfflineScreenDelegate{
   
    func openPendingDrafts(){
        let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
        vc.viewModel.title = ""
        vc.viewModel.lunchMode = .DRAFT
        vc.delegate = self
        vc.isFromOfflineScreen = false
        self.navigationController.viewControllers = [vc]
    }
}

extension MyLockerCoordinator: CameraViewControllerDelegate{
    func cameraIsDismissed(assests: [PHAsset]) {
        let assets:[PHAsset] =  assests
        self.navigationController.popViewController(animated: false)
        self.navigationController.setNavigationBarHidden(false, animated: true)
        self.navigationController.hidesBottomBarWhenPushed = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.addfloatingMenu()
        let vc  = self.navigationController.visibleViewController as? TalkToUsTabBarViewController
        let selectedVC =  vc?.selectedViewController as? TalkToUsViewController
        selectedVC?.viewModel.setRawMedia(rawMedia: assets)
    }
    
    
}

extension MyLockerCoordinator: MapParentViewControllerDelegate{
    func locationPickerDidSelectLocation(_ viewController : MapParentViewController, _ location: LocationPickerLocation) {

        self.navigationController.popViewController(animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.addfloatingMenu()
        locationDelegate?.locationPickerDidSelectLocation(viewController, location)
    }
    
    func locationPickerDidCancel(_ viewController : MapParentViewController) {

        self.navigationController.popViewController(animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.addfloatingMenu()
        locationDelegate?.locationPickerDidCancel(viewController)
        
    }
    
    fileprivate func embedViewController(child: UIViewController , To parent:MapParentViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    
    
    func openOfflineMap() {
//        let childVCont = container.resolveViewController(OfflineMapViewController.self)
//        childVCont.parentVC = parentVC
//        embedViewController(child: childVCont, To: parentVC)
    }
    
    
    func openOnlineMap() {
//        let childVCont = container.resolveViewController(LocationPickerEsriMapViewController.self)
//        childVCont.parentVC = parentVC
//         embedViewController(child: childVCont, To: parentVC)
    }
    
    
}

extension MyLockerCoordinator: MyEventsParentViewControllerDelegate{
    
    fileprivate func embedViewController(child: UIViewController , To parent:MyEventsParentViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    func openEvents(parentVC:MyEventsParentViewController) {
        let childVCont = container.resolveViewController(MyEventsViewController.self)
        embedViewController(child: childVCont, To: parentVC)
        childVCont.viewModel.getMyEvents()
    }
    
    func openPublicHolidays(parentVC:MyEventsParentViewController) {
        print("go to Public")
    }
    
    
}

extension MyLockerCoordinator : MessagesDelegate{
    func openCaseDetails(id:Int) {
        let vc = container.resolveViewController(CaseDetailsViewController.self)
        //vc.delegate = self
        vc.viewModel.id.value = id
        vc.container = container
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    
}


extension MyLockerCoordinator : MsgsParentViewControllerDelegate {
    func openActiveMsgs() {
        
    }
    func openClosed() {
        
    }
    
}

extension MyLockerCoordinator: MyDocumentViewControllerDelegate{
    func openActive() {
//        let childVCont = container.resolveViewController(ActiveDocumentsViewController.self)
//        childVCont.viewModel.getActiveDocuments()
//
//        childVCont.documentsType = .Active
//        childVCont.setupHooks()
//        embedViewController(child: childVCont, To: parentVC!)
    }
    
    func openExpired() {
//        let childVCont = container.resolveViewController(ActiveDocumentsViewController.self)
//        childVCont.viewModel.getExpiredDocuments()
//        childVCont.documentsType = .Expired
//        childVCont.setupHooks()
//        embedViewController(child: childVCont, To: parentVC!)
    }
    
    
    fileprivate func embedViewController(child: UIViewController , To parent:MyDocumentViewController){
        
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    
}


extension MyLockerCoordinator:MyPaymentsParentDelegate{
    
    fileprivate func embedViewController(child: UIViewController , To parent:MyPaymentsParentViewController){
      
        child.willMove(toParentViewController: parent)
        
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
        
    }
    
    func openUnpaid() {
//        let childVCont = container.resolveViewController(MyPaymentViewController.self)
//        childVCont.viewModel.paymentType = PaymentType.pending
//        childVCont.viewModel.page.value = 1
//        embedViewController(child: childVCont, To: vc)
    }
    
    func openPaid() {
//        let childVCont = container.resolveViewController(MyPaymentViewController.self)
//        childVCont.viewModel.paymentType = PaymentType.paid
//        childVCont.viewModel.page.value = 1
//        embedViewController(child: childVCont, To: vc)
    }
    
}

extension MyLockerCoordinator: MySubsciptionsViewControllerDelegate{
    func openActiveSubsciptions() {
        
    }
    
    func openExpiredSubsciptions() {
        
    }
    
   
}
