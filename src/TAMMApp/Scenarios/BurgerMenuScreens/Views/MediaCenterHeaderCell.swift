//
//  MediaCenterHeaderCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class MediaCenterHeaderCell: UITableViewHeaderFooterView {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var viewAllButton: LocalizedButton!
    @IBOutlet weak var viewAllLabel: UILabel!
    @IBOutlet var containerView: MediaCenterHeaderCell!
    var onClick:(()->Void)?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    @IBAction func viewAllButtonAction(_ sender: LocalizedButton) {
        onClick?()
    }
    
    func commonInit() {
        self.backgroundColor = UIColor.paleGreyTwo
        let height = viewAllButton.layer.bounds.height
        viewAllButton.addRoundCorners(radious: height/2, borderColor:UIColor.turquoiseBlue)
        //viewAllButton.setTitle(L10n.viewAll, for: .normal)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.viewAllButton.layer.cornerRadius = (self.viewAllButton.frame.height)/2
    }
}


