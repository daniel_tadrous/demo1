//
//  PollsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class PollsViewModel: ShareViewModel{
    private(set) var viewData: Variable<[MyCommunityModel.Poll]?> = Variable(nil)
    private(set) var start: Variable<Bool?> = Variable(nil)
    private(set) var answersViewData:Variable<[MyCommunityModel.Poll.Answer]?> = Variable(nil)
    private(set) var poll: Variable<MyCommunityModel.Poll?> = Variable(nil)
    fileprivate var apiService: ApiMyCommunityTypes!
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiMyCommunityTypes) {
        super.init()
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        start.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        poll.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.submitPoll()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if start.value != nil && start.value!{
            apiService.getPolls()
                .subscribe(onNext: { [unowned self] polls in
                    self.viewData.value = polls
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func submitPoll(){
        if poll.value != nil{
            apiService.submitPoll(poll: poll.value!)
                .subscribe(onNext: { [unowned self] answers in
                    self.answersViewData.value = answers
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
}
