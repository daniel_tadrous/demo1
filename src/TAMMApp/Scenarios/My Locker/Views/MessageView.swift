//
//  MessageView.swift
//  TAMMApp
//
//  Created by Daniel on 5/31/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import UIKit
import Foundation
import UICheckbox_Swift

class MessageView : UIView{
   
    @IBOutlet weak var checkbox: UICheckbox!
    
    @IBOutlet weak var refNumLabel: LocalizedLabel!
    
    @IBOutlet weak var tagLabel: LocalizedLabel!
    @IBOutlet weak var tagViewHolder: UIView!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var extensionView: UIView!
    
    @IBOutlet weak var descLabel: LocalizedLabel!
    @IBOutlet weak var messageTypeValLabel: LocalizedLabel!
    
    @IBOutlet weak var receivedByValLabel: LocalizedLabel!
    var starClickHandlerClosure:(()->Void)?
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    @IBOutlet var contentView: UIView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "MessageView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        addSubview(contentView)
        //contentView.frame = self.frame
        
        setConstraints(view: self, childView: contentView)
        self.refNumLabel.addRoundCorners(radious: 15)
        self.refNumLabel.layer.borderColor = UIColor.turquoiseBlue.cgColor
        self.tagLabel.addRoundCorners(radious: 15)
        self.tagLabel.layer.borderColor = UIColor.turquoiseBlue.cgColor
        
        self.starLabel.isEnabled = true
        self.starLabel.isUserInteractionEnabled = true
        self.starLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(starClickHandler)))
    }
    @objc func starClickHandler(){
        starClickHandlerClosure?()
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
}
