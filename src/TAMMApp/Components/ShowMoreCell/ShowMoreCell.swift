//
//  ShowMoreCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ShowMoreCell: UITableViewCell {

    @IBOutlet weak var showMoreBtn: LocalizedButton!
    @IBOutlet weak var cellButtomdistance: NSLayoutConstraint!

    @IBOutlet weak var cellRightDistance: NSLayoutConstraint!
    
    @IBOutlet weak var cellLeftDistance: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        showMoreBtn.addRoundCorners(radious: CGFloat(28.5))
//        showMoreBtn.layer.borderWidth = 2
//        showMoreBtn.layer.borderColor = UIColor.petrol.cgColor
        applyInvertColors()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func applyInvertColors() {
        showMoreBtn.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        showMoreBtn.BorderColor = #colorLiteral(red: 0, green: 0.4316659272, blue: 0.526139617, alpha: 1).getAdjustedColor()
        showMoreBtn.setTitleColor(#colorLiteral(red: 0, green: 0.4316659272, blue: 0.526139617, alpha: 1).getAdjustedColor(), for: .normal)
    }
}
