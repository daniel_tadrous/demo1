//
//  AolSubTopicCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AolSubTopicCell: UITableViewCell {
    @IBOutlet weak var collapseBtn: UIButton!
    
    @IBOutlet weak var detailsBtn: UITammButton?
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var onCollapseButtonTapped : (() -> Void)? = nil
    var onDetailsButtonTapped : (() -> Void)? = nil
    
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var smallerContentView: UIView!
    @IBAction func collapseBtnClickHandler(_ sender: UIButton) {
        if let onCollapseButtonTapped = self.onCollapseButtonTapped {
            onCollapseButtonTapped()
        }
    }
    @IBAction func detailsBtnClickHandler(_ sender: UIButton) {
        if let onDetailsButtonTapped = self.onDetailsButtonTapped {
            onDetailsButtonTapped()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        intitializeSmallerContentView()
        initiateDetailsBtn()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    private func intitializeSmallerContentView(){
        smallerContentView.applyRoundCorners()
    }
    private func initiateDetailsBtn(){
        self.detailsBtn?.roundCorners(radius: 17 * UIFont.fontSizeMultiplier)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        initiateDetailsBtn()
    }
    
}
extension UIView{
    
    func applyRoundCorners(){
        self.layer.shadowColor = UIColor.lightGray.cgColor;
        self.layer.shadowOpacity=0.5;
        self.layer.shadowRadius=2.0;
        self.layer.shadowOffset = CGSize(width:0, height:0);
        self.layer.masksToBounds = false;
        self.layer.cornerRadius = 5
    }
    
    func makeItRound(){
//        self.layer.shadowColor = UIColor.lightGray.cgColor;
//        self.layer.shadowOpacity=0.5;
//        self.layer.shadowRadius=2.0;
//        self.layer.shadowOffset = CGSize(width:0, height:0);
//        self.layer.masksToBounds = false;
        self.layer.cornerRadius = self.frame.width/2
    }
}
