//
//  ProcessModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper



class ProcessModel: Mappable {
    var id: String?
    var serviceId: String?
    var processSteps: [ProcessStep] = []
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id            <- map["id"]
        serviceId         <- map["serviceId"]
        processSteps  <- map["processSteps"]
    }
    
    class ProcessStep: Mappable {
        var index: Int?
        var serviceProcessId: String?
        var title: String?
        var detailsFormat: EDetailsFormat?
        var detailsHeaders: [String]?
        var details: [String]?
        
        
        required init?(map: Map) {
            
        }
        
        // Mappable
        func mapping(map: Map) {
            index <- map["index"]
            serviceProcessId <- map["serviceProcessId"]
            title <- map["title"]
            detailsFormat <- map["detailsFormat"]
            detailsHeaders <- map["detailsHeaders"]
            details <- map["details"]
        }
        public enum EDetailsFormat: Int {
            case TEXT = 0,BULLET, TABLE
        }
    }
}
