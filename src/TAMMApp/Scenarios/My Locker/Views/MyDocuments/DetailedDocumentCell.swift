//
//  DetailedDocumentCellTableViewCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class DetailedDocumentCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.gray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
