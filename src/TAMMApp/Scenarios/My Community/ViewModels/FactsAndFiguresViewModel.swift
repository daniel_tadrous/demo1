//
//  FactsAndFiguresViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class FactsAndFiguresViewModel {
    
    private var apiService: ApiFactsAndFiguresType!
    var pageOpened:Variable<Bool?> = Variable(nil)
    var content:Variable<String?> = Variable(nil)
    fileprivate var disposeBag = DisposeBag()
    
    
    init(apiService:ApiFactsAndFiguresType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate  func setupBinding(){
        
        pageOpened.asObservable().subscribe(onNext:{ value in
            self.loadpage()
        } ).disposed(by: disposeBag)
        
    }
    
    fileprivate func loadpage() {
        apiService.getHtmlContent().asObservable().subscribe(onNext:{ response in
            self.content.value = response.content
            print(response.content ?? "welocme")
        } ).disposed(by: disposeBag)
        
    }
}
