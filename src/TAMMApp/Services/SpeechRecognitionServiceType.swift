//
//  SpeechRecognitionServiceType.swift
//  TAMMApp
//
//  Created by kerolos on 4/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

//
//protocol SpeechRecognitionServiceTypeAvailabilityDelegate: class {
//}

protocol SpeechRecognitionServiceTypeDelegate: class {
    func recognizerAvailable(isAvailable:Bool)
    
    func textRecived(text:String)
    func recognitionIsDone(_ error: Error?)
}


protocol SpeechRecognitionServiceType{
    
    //    enum Locals : String {
    //        case arabic: "ar-AR"
    //    }
    
    //    weak var availabilityDelegate: SpeechRecognitionServiceTypeAvailabilityDelegate? {set get}
    
    // should inform of the recognizer availability at start
    func start(delegate: SpeechRecognitionServiceTypeDelegate, duration: Double)
    
    func stop()
    
    
}

