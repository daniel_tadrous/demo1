//
//  DocumentCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class DocumentCell: UITableViewCell {
    
    @IBOutlet weak var typeLabel: LocalizedLabel!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    @IBOutlet weak var expiredOnLabel: LocalizedLabel!
    @IBOutlet weak var expiryDateLabel: LocalizedLabel!
    @IBOutlet weak var idLabel: LocalizedLabel!
    @IBOutlet weak var label1Title: LocalizedLabel!
    @IBOutlet weak var detailesLabel1: LocalizedLabel!
    @IBOutlet weak var horizontalStackView: UIStackView!
    @IBOutlet weak var VerticalStackView: UIStackView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var editLabel: UILabel!
    
    
    var okClosure:((_ :String)->Void)!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commonInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func commonInit(){
        self.backgroundColor = UIColor.paleGreyTwo
        horizontalStackView.isHidden = true
        editLabel.text = "\u{e045}"
    }
    @IBAction func editAction(_ sender: UIButton) {
        editDocument()
    }
    
    func addRow(title:String, details:String) {
        
        let stack2 = horizontalStackView.copyView() as! UIStackView
        stack2.isHidden = false
        VerticalStackView.addArrangedSubview(stack2)
        (stack2.arrangedSubviews[0] as! UILabel).text = title
        (stack2.arrangedSubviews[1] as! UILabel).text = details
    }
    
    func setAsExpiredCell(){
        editLabel.text = ""
        editButton.isEnabled = false
    }
    
    func editDocument() {
        let editOverlay = EditFieldView(frame: UIScreen.main.bounds,currentName:titleLabel.text!)
        editOverlay.closeClosure = closeAction
        editOverlay.okClosure = okAction(_:)
        UIApplication.shared.keyWindow!.addSubview(editOverlay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: editOverlay)
    }
    func closeAction() -> Void {
        
    }
    func okAction(_ nickname:String) -> Void {
        okClosure(nickname)
    }
    
}

extension UIView
{
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}
