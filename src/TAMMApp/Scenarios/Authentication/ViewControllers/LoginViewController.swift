//
//  LogInViewController.swift
//  TammUI
//
//  Created by Marina.Riad on 4/4/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol LoginViewControllerDelegate {
    func authenticationViewControllerDismissed()
    func login()-> Observable<Bool>
    func reload()
}
class LoginViewController:TammViewController, AuthenticationStoryboardLodable{
    var justCameFromIntro = false
    
    
    @IBOutlet weak var smartPassLabel: LocalizedLabel!
    @IBOutlet weak var lockLabel: UILabel!
    
    @IBOutlet weak var EnglishBtn: UITammResizableButton!
    
    @IBOutlet weak var tammImage: UIImageView!
    @IBOutlet weak var continueAsGuestButton: LocalizedButton!
    @IBOutlet weak var arabicEnglishSpaacingConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectALanguageLabel: LocalizedLabel!
    @IBOutlet weak var languageSwitchView: UIView!
    @IBOutlet weak var LoginOrRegisterBtn: UITammButton!
    @IBOutlet weak var ArabicBtn: UITammResizableButton!
    var delegate:LoginViewControllerDelegate!
    private var disposeBag = DisposeBag()
    var viewModel: AuthenticationViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        _ = LoginOrRegisterBtn.rx.tap
            .debounce(0.3, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] in
                _ = self.delegate?.login()
            })
        lockLabel.text = "\u{e03f}"
        
        
        self.hideFloatingMenu()
        self.removeSideMenu()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.applyRoundCorners()
        
        if(justCameFromIntro){
            languageSwitchView.isHidden = true
            selectALanguageLabel.isHidden = true
        }
        self.arabicEnglishSpaacingConstraint.constant = 0
        
        self.tammImage.alpha = 0
        self.languageSwitchView.alpha = 0
        self.selectALanguageLabel.alpha = 0
        self.continueAsGuestButton.alpha = 0
        self.LoginOrRegisterBtn.alpha = 0
        adjustButtonColor()
        
        var str = "Smart"
        let smartAttr: [NSAttributedStringKey: Any] = [.font: UIFont(name: "CircularStd-Bold", size: 21)!]
        let passAttr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: UIFont(name: "CircularStd-Book", size: 21)!]
        
        let smartStr = NSMutableAttributedString(string:str, attributes: smartAttr)
        str = "Pass"
        let passStr = NSMutableAttributedString(string:str, attributes: passAttr)
        let result = NSMutableAttributedString(attributedString: smartStr)
        result.append(passStr)
        smartPassLabel.attributedText = result
        
        self.view.layoutIfNeeded()
        
    }
    private func adjustButtonColor(){
        EnglishBtn.setTitleColor(.white, for: .normal)
        EnglishBtn.setTitleColor( #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .highlighted)
        
        EnglishBtn.BorderColor = .white
        
        ArabicBtn.setTitleColor(.white , for: .normal)
        ArabicBtn.setTitleColor( #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .highlighted)

        ArabicBtn.BorderColor = .white
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        if isRTL{
            ArabicBtn.setTitleColor(#colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .normal)
//            ArabicBtn.titleLabel?.textColor = #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1)
            
            ArabicBtn.BorderColor = #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1)
        } else{
            EnglishBtn.setTitleColor(#colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .normal)
//            EnglishBtn.titleLabel?.textColor = #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1)
            
            EnglishBtn.BorderColor = #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1)
        }
        applyRoundCorners()

        EnglishBtn.setNeedsLayout()
        ArabicBtn.setNeedsLayout()
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        EnglishBtn.layoutIfNeeded()
        ArabicBtn.layoutIfNeeded()
        
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        applyRoundCorners()
        
//        LoginOrRegisterBtn.roundCorners(radius: 20)
    }
    @IBAction func btnArabicClicked(_ sender: Any) {
        if L102Language.currentAppleLanguage().contains("en") {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            self.delegate.reload()
            
        }
    }
    @IBAction func btnContinueAsGuest(_ sender: Any) {
        self.delegate.authenticationViewControllerDismissed()
    }
    
    @IBAction func btnEnglishClicked(_ sender: Any) {
        if L102Language.currentAppleLanguage().contains("ar"){
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            self.delegate.reload()
            
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.arabicEnglishSpaacingConstraint.constant = 0
        
        self.LoginOrRegisterBtn.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        self.continueAsGuestButton.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        UIView.animate(withDuration: Dimensions.animationDuration1, delay: Dimensions.animationDuration1, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.tammImage.alpha = 1
            
        }) { (finished) in
            self.view.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: Dimensions.animationDuration1, delay: 2 * Dimensions.animationDuration1, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.LoginOrRegisterBtn.transform = CGAffineTransform.identity
            self.continueAsGuestButton.transform = CGAffineTransform.identity
            self.arabicEnglishSpaacingConstraint.constant = 25
            
            self.languageSwitchView.alpha = 1
            self.selectALanguageLabel.alpha = 1
            self.continueAsGuestButton.alpha = 1
            self.LoginOrRegisterBtn.alpha = 1
            self.view.layoutIfNeeded()
            
            
        }) { (finished) in
            self.view.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: Dimensions.animationDuration1, delay: 3 * Dimensions.animationDuration1, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            
            
        }) { (finished) in
        }
        adjustButtonColor()
        
    }
    
    fileprivate func applyRoundCorners() {
        EnglishBtn.roundCorners( radius: EnglishBtn.currentHeight / 2)
        ArabicBtn.roundCorners(radius: ArabicBtn.currentHeight / 2)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        applyRoundCorners()
    }
    @IBAction func buttonDown(_ sender: UITammResizableButton) {
        
        sender.addRoundCorners(radious: sender.currentHeight / 2, borderColor: #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1))
    }
    @IBAction func touchUpOutside(_ sender: UITammResizableButton) {
        adjustButtonColor()
    }
}


