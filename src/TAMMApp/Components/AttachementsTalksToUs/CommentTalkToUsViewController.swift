//
//  CommentTalkToUsViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class CommentTalkToUsViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var mainView: RoundedView!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var additionalComentsTextView: RoundedTextView!

    @IBOutlet weak var submitBtn: LocalizedButton!
    
    @IBOutlet weak var cancelBtn: LocalizedButton!
    
    @IBOutlet weak var closeLabel: UILabel!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    private var okClickHandler: ((_ comment:String)->Void)?

//    var titleStr:String?
//    var message:String?
//    var okBtnTitle:String?
//    var cancelBtnTitle:String?
    
    @IBAction func cancelClickHandler(_ sender: LocalizedButton) {
        dismiss(animated: true) {self.appDelegate.addfloatingMenu()}
    }
    
    

    @IBAction func submitBtnAction(_ sender: Any) {
        if additionalComentsTextView.text != "" && additionalComentsTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            okClickHandler?(additionalComentsTextView.text)
            dismiss(animated: true) {self.appDelegate.addfloatingMenu()}
        }else {
            // write comment to submit
             Toast(message:L10n.pleaseWriteComment).Show()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
        self.appDelegate.removeFloatingMenu()
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeIconClicked))
        closeView.addGestureRecognizer(tap)
        additionalComentsTextView.placeholder = L10n.additionalComments
        applyInvertedColors()
    }
    
    
    fileprivate func applyInvertedColors() {
        
        backgroundView.backgroundColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1).getAdjustedColor()
        mainView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        closeView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        closeLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        titleLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        additionalComentsTextView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        additionalComentsTextView.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        additionalComentsTextView.placeholderTextColor = #colorLiteral(red: 0.66777426, green: 0.6667010188, blue: 0.6666694283, alpha: 1).getAdjustedColor()
        additionalComentsTextView.placeholderTextColor = #colorLiteral(red: 0.66777426, green: 0.6667010188, blue: 0.6666694283, alpha: 1).getAdjustedColor()
        submitBtn.backgroundColor = #colorLiteral(red: 1, green: 0.8666666667, blue: 0, alpha: 1).getAdjustedColor()
        submitBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        cancelBtn.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        cancelBtn.setTitleColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor(), for: .normal)
        
    }
    
    @objc func closeIconClicked() {
        dismiss(animated: true, completion: {
            
            self.appDelegate.addfloatingMenu()
        })
    }

    
    func initialize(okClickHandler: @escaping (_ comment:String)->Void) {
        self.okClickHandler = okClickHandler
        commonIntializer()
    }

    
 
    
    private func commonIntializer(){
        self.modalPresentationStyle = .overFullScreen
    }
    
    private func commonInit(){
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey.getAdjustedColor())
        self.cancelBtn.layer.borderWidth = 1
        self.cancelBtn.layer.borderColor = UIColor.turquoiseBlue.getAdjustedColor().cgColor
        self.submitBtn.addRoundCorners(radious: (self.submitBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
       // setupUiData()
        
    }
    
    
//
//    func setupUiData(){
//
//        if self.message != nil{
//            self.bodyLabel.text = self.message
//        }
//        if self.title != nil{
//            self.titleLabel.text = self.title
//        }
//
//        if self.okBtnTitle != nil{
//            okBtn.setTitle(okBtnTitle, for: .normal)
//        }
//        if self.cancelBtnTitle != nil{
//            cancelBtn.setTitle(cancelBtnTitle, for: .normal)
//        }
//
//
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.submitBtn.addRoundCorners(radious: (self.submitBtn.frame.height)/2)
        self.cancelBtn.addRoundCorners(radious: (self.cancelBtn.frame.height)/2)
    }
    

}
