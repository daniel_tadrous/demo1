//
//  UIColor+Extension.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
extension UIColor {
    
    @nonobjc class var petrolTwo: UIColor {
        return UIColor(red: 3.0 / 255.0, green: 91.0 / 255.0, blue: 115.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var petrol: UIColor {
        return UIColor(red: 0.0, green: 89.0 / 255.0, blue: 113.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var sunflowerYellow: UIColor {
        return UIColor(red: 1.0, green: 221.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var duckEggBlue: UIColor {
        return UIColor(red: 217.0 / 255.0, green: 243.0 / 255.0, blue: 247.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var offWhite: UIColor {
        return UIColor(red: 1.0, green: 251.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var red: UIColor {
        return UIColor(red: 208.0 / 255.0, green: 0.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var paleGrey: UIColor {
        return UIColor(red: 232.0 / 255.0, green: 231.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var cloudyBlue: UIColor {
        return UIColor(red: 204.0 / 255.0, green: 221.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkBlueGrey: UIColor {
        return UIColor(red: 22.0 / 255.0, green: 17.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var coolGrey: UIColor {
        return UIColor(red: 161.0 / 255.0, green: 159.0 / 255.0, blue: 175.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var turquoiseBlue: UIColor {
        return UIColor(red: 0.0, green: 171.0 / 255.0, blue: 197.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var black50: UIColor {
        return UIColor(white: 0.0, alpha: 0.5)
    }
    @nonobjc class var paleGreyTwo: UIColor {
        return UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var duckEggBlueTwo: UIColor {
        return UIColor(red: 229.0 / 255.0, green: 247.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkGreyBlue: UIColor {
        return UIColor(red: 60.0 / 255.0, green: 51.0 / 255.0, blue: 84.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var tomato: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 67.0 / 255.0, blue: 54.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var darkIndigo10: UIColor {
        return UIColor(red: 11.0 / 255.0, green: 34.0 / 255.0, blue: 62.0 / 255.0, alpha: 0.1)
    }
    @nonobjc class var redTwo: UIColor {
        return UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
    }
    @nonobjc class var white12: UIColor {
        return UIColor(white: 1.0, alpha: 0.12)
    }
    @nonobjc class var greyish: UIColor {
        return UIColor(white: 183.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var squash: UIColor {
        return UIColor(red: 247.0/255.0, green: 148.0/255.0, blue: 29.0/255.0, alpha: 1.0)
    }
    @nonobjc class var lichen: UIColor {
        return UIColor(red: 135.0/255.0, green: 192.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    }
    @nonobjc class var grapefruit: UIColor {
        return UIColor(red: 255.0/255.0, green: 96.0/255.0, blue: 96.0/255.0, alpha: 1.0)
    }
    
    @nonobjc class var darkishGreen: UIColor {
        return UIColor(red: 34.0 / 255.0, green: 105.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var whiteTwo: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var silver: UIColor {
        return UIColor(red: 208.0 / 255.0, green: 207.0 / 255.0, blue: 215.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var greyPurple: UIColor {
        return UIColor(red: 115.0 / 255.0, green: 112.0 / 255.0, blue: 135.0 / 255.0, alpha: 1.0)
    }
    
    
    
    func getAdjustedColor()->UIColor {
        if UIColor.getInvertedStatus() {
            return self.inverted
        }else {
            return self
        }
    }
    
    
    public static func getInvertedStatus()->Bool {
        let defaults = UserDefaults.standard
        let invertedStatus = defaults.object(forKey: UserDefaultskeys.isInverted)
        if invertedStatus != nil {
            return invertedStatus as!Bool
        }else{
            return false
        }
    }
    
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIGraphicsGetCurrentContext()?.fillEllipse(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func resizeImage(targetSize: CGSize) -> UIImage{
        return resizeImage(image: self, targetSize: targetSize)
    }
    
    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
