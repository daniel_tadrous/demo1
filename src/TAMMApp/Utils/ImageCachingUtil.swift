//
//  ImageCachingUtil.swift
//  TAMMApp
//
//  Created by kerolos on 4/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SDWebImage

class ImageCachingUtil{
    
    static func loadImage( url: URL, dispatchInMain: Bool = false, callBack: @escaping (URL, UIImage?) -> () ){
        
        let manager = SDWebImageManager.shared()
//        SDWebImageOptions(rawValue: 0)
        manager.loadImage(with: url, options: SDWebImageOptions.retryFailed , progress: { (receivedSize: Int, expectedSize: Int, url: URL?) in
            
        }) { (image: UIImage?, data: Data?, error: Error?, cacheType: SDImageCacheType, finished: Bool , url: URL?) in
            
            if (image != nil) {
                print("image loaded successfully with url \(String(describing: url))")
                
                if dispatchInMain{
                    DispatchQueue.main.async {
                        callBack(url!, image)
                    }
                }
                else{
                    callBack(url!, image)
                }
            }
            else{
                print("could not load image with url \(String(describing: url))")
                print(error ?? "no error")
            }
            
            
        }
    }
    
    // x image URL(string:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAS1BMVEX///8AAACXl5dnZ2cTExPS0tLBwcGkpKTU1NTp6eni4uIsLCwjIyMNDQ3Hx8cSEhL39/exsbFaWlpISEiGhoZwcHAbGxuNjY1+fn4TsIpCAAAD2klEQVR4nO2b23abMBBFIYEkbtNLWift/39pMeDWxiDNoLlZPfspttbSzI4RaCTRNAAAAAD433ho/zJ+bq/Z+V3XVyLiYKIkYm+iJWJuoibSfny3F3mU7bPvTp2+yXaaQ0NkNDH20BFp+g9rDyWRZh4fx0/iPW+hJDLx3r6amWiKvA9dm5koivwe78JWJtIiLw8DT+Of/TdLE2mRy/5MTTRFTE1URSxNdEUMTZ4eB57l+lv+Y6Z5l+HzRIqbX/hscnBLaR+3l+pk8sMrob2sjLmTyd15rN48+s7A4+JJLMLqXXAe6b++ysWhBS4gcRc8tl8UTVSn8Vcch0CKJmYi0yqHnomZyOGzrondpaVsYieibGIoomtiKaJqoljqrqBoolyPLDmbyK9wG4vMJj/lF7itRUYTBQ97kcFEw8NBpDlMHr3sONGu2TfpO4cdUwZUkVPZGNqEKDIV8lYP4j1c7Em289Zic7W7OH3x8lqJiPzUSKVm9xBRuf1CpDSww6CDiFmHXnEhEi0uRKLF9RKRLh/cRMSBSDSYIulCW7gMZ8EbdOlCm1OGS5cPPNKFNqsMd72kp0J7K9d06xJPkfkMw0au6dYbHEXOma7nmm69xa9m/5fpWq7p1hXcpihzpl23mmu6tSQuGdaC2pBiv5ZrurUkLh1ahxcJruSabi2Jy4DU4VV6N7mmW0vicqB0uEiO9bEkLgtCh8yfgGbiIMIeFCQTe5EdtymKibnIRlK0x0rCxFpkMyXSgz5hYlyzJxIiTb0MN9eSIsl0SJNhO5P9IukCJJYIwSSKR27Q5Ux2e5jX7BmT3b+HfYW44yJxeSDmYZv4TFEIME2cJo2UQccyCTyNZ5mELqwYJsFLXXKC4RcfiCkyLkG/rTdCkpybguMeYjZN1s3NczM0kyjvcQOR8g5rubSEB7vbPrv07VccrweiOF5TFHG8Jo3iUAadxjTeY5/9PgqrPLWUurUsPtzNclBm0Kkt0BlPUfSWTG1FFBexA4mUbSvEubQKN3qq2XrDZujuDmvZnq7nwEA9Rzh0DtX41OwKx5zE8Tp4Jo7XUUBxvA5nikMedKRHOd3D8Ww8aXJF/j1wpFyCZAGSaV3i+wJOoiTMti5wfp9d7kUY79f3xF5N8haJGxci0eJCJFpciESLC5FC8D77FhCJRjUi4oOOiO/77IJUcyVAJBrONbsc1UxRIGLWoVdciESLC5FocSESLS5q9mhAJBrViKBmL6SaKwEi0UDNbtahV1yIRIsLkWhxIRItLkSixUXNHo3qRNprxjaL7yCy5Dzo7l7kDESiiQAAAAAgNn8A12ElUecFm88AAAAASUVORK5CYII=")!
}
