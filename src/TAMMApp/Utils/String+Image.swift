//
//  String+Image.swift
//  TAMMApp
//
//  Created by kerolos on 6/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
//import UIKit
//class LetterImageGenerator: NSObject {
//
//    var backgroundColor: UIColor = .lightGray
//    var textColor: UIColor
//
//    func imageWith(name: String?) -> UIImage? {
//        let frame = CGRect(x: 0, y: 0, width: 50, height: 50)
//        let nameLabel = UILabel(frame: frame)
//        nameLabel.textAlignment = .center
//        nameLabel.backgroundColor = .lightGray
//        nameLabel.textColor = .white
//        nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
//        var initials = ""
//        if let initialsArray = name?.components(separatedBy: " ") {
//            if let firstWord = initialsArray.first {
//                if let firstLetter = firstWord.characters.first {
//                    initials += String(firstLetter).capitalized
//                }
//            }
//            if initialsArray.count > 1, let lastWord = initialsArray.last {
//                if let lastLetter = lastWord.characters.first {
//                    initials += String(lastLetter).capitalized
//                }
//            }
//        } else {
//            return nil
//        }
//        nameLabel.text = initials
//        UIGraphicsBeginImageContext(frame.size)
//        if let currentContext = UIGraphicsGetCurrentContext() {
//            nameLabel.layer.render(in: currentContext)
//            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
//            return nameImage
//        }
//        return nil
//    }
//}


import UIKit

extension String {
    func tammImage( fontColor: UIColor = UIColor.black, size: CGSize = CGSize(width: 40, height: 40), fontSize: CGFloat = 40 , backgroundColor: UIColor  = UIColor.clear) -> UIImage? {

        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        backgroundColor.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(rect)
        
        
        // Setup the font specific variables
        let textColor: UIColor = fontColor
        let textFont: UIFont = UIFont(name: "tamm", size: fontSize)!
        
        let textFontAttributes: [NSAttributedStringKey:Any] = [
            NSAttributedStringKey.font: textFont,
            NSAttributedStringKey.foregroundColor: textColor
            ]
        
        var drawRect = CGRect(x: 0, y: 0, width: fontSize, height: fontSize)
        
        
        drawRect.origin = CGPoint(x: ( size.width -  fontSize) / 2 , y: ( size.height -  fontSize) / 2)
        
        (self as AnyObject).draw(in: drawRect, withAttributes: textFontAttributes) //) UIFont("tamm", ofSize: 40)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension UIImage {
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension String {
    enum regexes:String {
        case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        case password = "^(?=.*?[a-z])(?=.*?[0-9]).{6,12}$"
    }
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
    
    func validate(regex: String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }
    
    
}

extension String{
    func isOfExtension(types:[String]) -> Bool {
        for type in types {
            if self.contains(type){
                return true
            }
        }
        return false
    }
}
