//
//  UIView+Extensions.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


extension UIView{
    func addDashedBorder(){
        
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.black.cgColor
//        yourViewBorder.lineDashPattern = [2, 2]
//        yourViewBorder.frame = self.bounds
//        yourViewBorder.fillColor = nil
//        yourViewBorder.path = UIBezierPath(rect: self.bounds).cgPath
//        self.layer.addSublayer(yourViewBorder)

                self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(patternImage: UIImage(named: "dash")!).cgColor
        
//        let color = UIColor.red.cgColor
//
//        let shapeLayer:CAShapeLayer = CAShapeLayer()
//        let frameSize = self.frame.size
//        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
//
//        shapeLayer.bounds = shapeRect
//        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        shapeLayer.strokeColor = color
//        shapeLayer.lineWidth = 2
//        shapeLayer.lineJoin = kCALineJoinRound
//        shapeLayer.lineDashPattern = [6,3]
//        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
//
//        self.layer.addSublayer(shapeLayer)
    }
    func resizeToFitSubviews()  {
        var h:Float = 0
        
        for v in self.subviews {
            //let fw:Float = Float(v.frame.origin.x + v.frame.size.width)
            let fh:Float = Float(v.frame.origin.y + v.frame.size.height)
           // w = max(w, fw)
            h = max(fh, h)
        }
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: CGFloat(h))
    }
//    func resizeToFitContent(){
//        var contentRect = CGRect.zero
//        for view in self.subviews {
//            contentRect = contentRect.union(view.frame)
//        }
//        self.frame = CGRect(origin: self.frame.origin, size: contentRect.size)
//    }
    
    func addRoundCorners(radious:CGFloat){
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radious
            if(L102Language.currentAppleLanguage().contains("ar")){
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
        } else {
            // Fallback on earlier versions
            self.layer.cornerRadius = radious
        }
    }
    func addRoundAllCorners(radious:CGFloat){
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radious
            self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner, .layerMinXMinYCorner]
         
        } else {
            // Fallback on earlier versions
            self.layer.cornerRadius = radious
        }
    }
    func addRoundCorners(radious:CGFloat, borderColor:UIColor){
        self.addRoundCorners(radious: radious)
        self.layer.borderWidth = 1
        self.layer.borderColor = borderColor.cgColor
    }
    func addRoundAllCorners(radious:CGFloat, borderColor:UIColor){
        self.addRoundAllCorners(radious: radious)
        self.layer.borderWidth = 1
        self.layer.borderColor = borderColor.cgColor
    }
    func addRoundAllCorners(radious:CGFloat, borderColor:UIColor , borderWidth:CGFloat){
        self.addRoundAllCorners(radious: radious)
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    func addRoundCorners(radious:CGFloat, shadowColor:UIColor){
        self.addRoundCorners(radious: radious)
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5)//0.5
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 1//1
    }
    func addRoundAllCorners(radious:CGFloat, shadowColor:UIColor){
        self.addRoundAllCorners(radious: radious)
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5)//0.5
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 1//1
    }
    
    func clearSubViews(){
        for v in self.subviews{
            v.removeFromSuperview()
        }
    }
    
    func displayFloatingMenu(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(!appDelegate.floatingMenuIsVisible()){
            appDelegate.addfloatingMenu()
        }
    }
    func hideFloatingMenu(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.floatingMenuIsVisible()){
            appDelegate.removeFloatingMenu()
        }
    }
    static func setFillingConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    
    
    func tintImage(image: UIImage, color: UIColor) -> UIImage {
        let size = image.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()
        image.draw(at: CGPoint.zero, blendMode: CGBlendMode.normal, alpha: 1.0)
        
        context!.setFillColor(color.cgColor)
        context!.setBlendMode(CGBlendMode.sourceIn)
        context!.setAlpha(1.0)
        
        let rect = CGRect(
            x:
            CGPoint.zero.x,
            y:
            CGPoint.zero.y,
            width:
            image.size.width,
            height:
            image.size.height)
        UIGraphicsGetCurrentContext()!.fill(rect)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return tintedImage!
    }
    
}

extension UITextField{
    
    func tintClearImage(tintedClearImageOrNil:UIImage?) -> UIImage? {
        var tintedClearImage = tintedClearImageOrNil
        for view in subviews {
            if view is UIButton {
                let button = view as! UIButton
                if let uiImage = button.image(for: .highlighted) {
                    if tintedClearImage == nil {
                        tintedClearImage = tintImage(image: uiImage, color: tintColor)
                    }
                    button.setImage(tintedClearImage, for: .normal)
                    button.setImage(tintedClearImage, for: .highlighted)
                }
            }
        }
        return tintedClearImage
    }




    
    
    
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}


