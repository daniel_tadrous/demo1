//
//  InitiativeViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import SDWebImage


class InitiativeViewController: TammViewController,InitiativesStoryboardLoadable {
    let expandedIdentifier = "EventCellExpended"
    public var viewModel:InitiativeViewModel!
    private var disposeBag = DisposeBag()
    
     var eventFilterModel = EventsFilterApplyModel()
    
    
    @IBOutlet weak var searchView: TammSearchView!
    @IBOutlet weak var eventsTableView: UITableView!
    
    var events: [Event]{
        get{
            return viewModel.initiatives.value ?? []
        }
    }
    
    
    private var textFromMic:Bool =  false
    private let debounceTimeInSec: Double = 0.5
    
    @IBAction func OnFilterClickHandler(_ sender: UIButton) {
        let filterView = FilterView(frame: UIScreen.main.bounds, delegateViewModel: viewModel,filterViewDelegate:self,enableSearchLocation:true)
        UIApplication.shared.keyWindow!.addSubview(filterView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: filterView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(title: L10n.initiatives, willSetSearchButton: true)
        self.searchView.addRoundCorners(radious: 30)
        self.searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClick)))
        
        searchView.PlaceholderTextContent = L10n.Initiatives.searchPlaceHolder
        
        setupHooks()
    }
    
    func setupHooks(){
        viewModel!.initiatives.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] events in
                if(events == nil){
                    //TODO: should do somting ????
                }else{
                    self.eventsTableView.reloadData()
                    
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    @objc func searchClick(){
        
        let searchViewOverLay = SearchOverlayView(frame: UIScreen.main.bounds, currentViewModel: self.viewModel!, text: self.searchView.searchField.text!, currentSearchModel: self.viewModel!)
        searchViewOverLay.delegate = self
        UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay)
        print("Search View Clicked")
    }
    
}

extension InitiativeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event: Event = events[indexPath.row]
        var cell: EventCell

        cell = tableView.dequeueReusableCell(withIdentifier: expandedIdentifier) as! EventCell
        cell.descrLb?.text = event.description
        cell.shareClickHandlerClosure = {
            // set up activity view controller
            self.viewModel.shareViewModel.share(viewController: self, text: event.link!)
        }
        
        cell.findOutMoreClickHandlerClosure = {
            Toast(message:L10n.commingSoon).Show()
        }
        
        

        if !event.isExpanded{
            cell.bottomConstraint.priority = .defaultHigh
        }
        
        if event.isImageLoaded {
            let constraint = cell.imageAspectRatioConstraint.constraintWithMultiplier(event.imageAspectRatio)
            cell.img.removeConstraint(cell.imageAspectRatioConstraint)
            cell.imageAspectRatioConstraint = constraint
            cell.img.addConstraint(cell.imageAspectRatioConstraint)
        }
        
        ImageCachingUtil.loadImage(url: URL(string: (event.image)!)!, dispatchInMain: true
            , callBack: { (url, image) in
                
                let const = cell.imageAspectRatioConstraint.constraintWithMultiplier(image!.size.width/image!.size.height)
                event.imageAspectRatio = image!.size.width/image!.size.height
                cell.img.removeConstraint(cell.imageAspectRatioConstraint)
                cell.imageAspectRatioConstraint = const
                cell.img.addConstraint(const)
                if !event.isImageLoaded{
                    event.isImageLoaded = true
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                cell.img.image = image
        })
        
        cell.categoryLb.text = event.category?.title?.uppercased()
        cell.dateLb.text = event.completionDate?.getDate(format: "yyyy-MM-dd'T'HH:mm:ss.SSS")?.getFormattedString(format:"MMM, YYYY").capitalizingFirstLetter()
        cell.titleLb.text = event.title
        cell.containerView.addRoundCorners(radious: 17)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event: Event = events[indexPath.row]
        event.isExpanded = !event.isExpanded
        //let animation = event.isExpanded ? UITableViewRowAnimation.bottom : UITableViewRowAnimation.top
        let cell = tableView.cellForRow(at: indexPath) as! EventCell
        
        
        if event.isExpanded{
            cell.expand()
        }else{
            cell.collapse()
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 36
    }
    
    
}

extension InitiativeViewController:SearchOverlayViewDelegate{
    
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchView.searchField.text = message.name
        self.searchView.PlaceholderTextContent = message.name
        self.viewModel?.searchedText.value = message.name
    }
    func backPressed(){
        self.searchView.searchField.text = ""
    }
    
}

extension InitiativeViewController:FilterViewDelegate{
    var filterModel: EventsFilterApplyModel {
        get {
            return eventFilterModel
        }set{
            eventFilterModel = newValue
        }
    }
    
    func applyFilter() {
        print(filterModel)
        viewModel.filterObject.value = filterModel
        viewModel.getInitiatives()
    }
    
    
    
}
