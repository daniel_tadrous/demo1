//
//  SortView.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol SortViewDelegate {
    func applySortClicked(applySortOn:String)
}

class SortView : UIView , UITableViewDelegate, UITableViewDataSource{
    
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var filterTableView: UITableView!
    
    var delegate:SortViewDelegate?
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var filterByLabel: LocalizedLabel!
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var closeLabel: UILabel!
    
    @IBOutlet weak var roundedView: RoundedView!
    
    @IBOutlet weak var applyFilterBtn: LocalizedButton!
    
    let cellIdentifier = "FilterCell"
    var hasAll = true
    var tableHeight:CGFloat = CGFloat(0)
    var isSelected:[Bool] = [Bool]()
    var isLoaded = false
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    var sortOptions: [String]!
    var selectedOption:String!
    
    @IBAction func applyFilterAction(_ sender: Any) {
        delegate?.applySortClicked(applySortOn: selectedOption!)
        closeView()
    }
    
    ///init methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    
    init(frame: CGRect,sortOptions:[String],selectedOption:String){
        super.init(frame: frame)
        assert(sortOptions.contains(selectedOption))
        self.sortOptions = sortOptions
        self.selectedOption = selectedOption
        setSortSelection()
        commonInit()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func setupViews(){
        self.filterTableView.reloadData()
    }
    
    private func commonInit(){
        let viewFileName: String = "SortView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        filterByLabel.text = L10n.MyMessages.sortBy
        applyFilterBtn.setTitle(L10n.MyMessages.applySort, for: .normal)
        self.filterTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        self.filterTableView.delegate = self
        self.filterTableView.dataSource = self
        applyColors()
    }
    
    

    fileprivate func applyColors() {
        closeLabel.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        backgroundView.backgroundColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1).getAdjustedColor()
        roundedView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        backView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        filterByLabel.textColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        applyFilterBtn.BorderColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor()
        applyFilterBtn.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        applyFilterBtn.setTitleColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor(), for: .normal)
    }
    
    
    
    
    
    //sort methods
    func setSortSelection() {
        
        for item in sortOptions! {
            
            if item == selectedOption {
                isSelected.append(true)
            }else {
                isSelected.append(false)
            }
        }
        
    }
    
    
    
    @objc func cancelBtnClicked(){
        closeView()
    }
    
    func closeView() {
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return sortOptions!.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = filterTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! FilterTableViewCell
        cell.filterTitleLabel.text = sortOptions?[indexPath.row]
        cell.applyInvertColors()
        cell.backgroundColor = UIColor.white.getAdjustedColor()
        if isSelected[indexPath.row] {
            cell.selectedRoundedView.backgroundColor = UIColor.darkBlueGrey.getAdjustedColor()
            selectedOption = cell.filterTitleLabel.text
            if L10n.Lang == "en" {
                cell.filterTitleLabel.font = UIFont(name: "CircularStd-Bold", size: UIFont.fontSizeMultiplier * 16)
            }else {
                cell.filterTitleLabel.font = UIFont(name: "Swissra-Bold", size: UIFont.fontSizeMultiplier * 16)
            }
            
        }else{
            cell.selectedRoundedView.backgroundColor = UIColor.white.getAdjustedColor()
            if L10n.Lang == "en" {
                cell.filterTitleLabel.font = UIFont(name: "CircularStd-Medium", size: UIFont.fontSizeMultiplier * 16)
            }else {
                cell.filterTitleLabel.font = UIFont(name: "Swissra-Normal", size: UIFont.fontSizeMultiplier * 16)
            }
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let  rowHeight = UITableViewAutomaticDimension
        //        tableHeight += rowHeight
        //        if indexPath.row == (filterOptions?.count)!-1 && !isLoaded {
        //            isLoaded = true
        //            tableViewHeight.constant = tableHeight
        //            filterTableView.reloadData()
        //        }
        
        return rowHeight
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let  rowHeight = UITableViewAutomaticDimension
        
        return rowHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        isSelected.removeAll()
            for index in 0..<tableView.numberOfRows(inSection: 0) {
                if indexPath.row == index {
                    isSelected.append(true)
                }else{
                    isSelected.append(false)
                }
            }
            self.filterTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isLoaded  {
            tableViewHeight.constant = CGFloat(tableView.numberOfRows(inSection: 0) * 80)
            //filterTableView.contentSize.height +
            isLoaded = true
            filterTableView.reloadData()
        }
        
    }
    

    
    
}
