//
//  FilterTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    

    
    @IBOutlet weak var filterTitleLabel: LocalizedLabel!
    
    @IBOutlet weak var selectedRoundedView: RoundedView!
    
    @IBOutlet weak var borderRoundedView: RoundedView!
    
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        applyInvertColors()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func applyInvertColors(){
        //self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        self.selectedRoundedView.backgroundColor = #colorLiteral(red: 0.9562581182, green: 0.9564779401, blue: 0.9625678658, alpha: 1).getAdjustedColor()
        self.borderRoundedView.BorderColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        self.borderRoundedView.backgroundColor = #colorLiteral(red: 0.9562581182, green: 0.9564779401, blue: 0.9625678658, alpha: 1).getAdjustedColor()
        self.bottomView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1).getAdjustedColor()
        self.filterTitleLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
    }
    
}
