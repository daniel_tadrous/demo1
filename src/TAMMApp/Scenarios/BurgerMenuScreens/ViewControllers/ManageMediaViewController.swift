//
//  ManageMediaViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage

class ManageMediaViewController: TammViewController ,ManageMediaStoryboardLoadable{

    var viewModel:ManageMediaViewModel!

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var manageMediaCollectionView: UICollectionView!
    var allMedia:[MediaUrl] = [MediaUrl]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var deleteStackView: UIStackView!
    let disposeBag:DisposeBag = DisposeBag()
    
    var selectedItems:Variable<[Int]?>  = Variable(nil)
    
    let cellIdentifier = "MediaCell"
    

    @IBAction func deleteAction(_ sender: Any) {
        var urlsToDelete = [URL]()
        var selectedIndexPathes:[IndexPath] = [IndexPath]()
        for item in selectedItems.value! {
            urlsToDelete.append(allMedia[item].myUrl)
            selectedIndexPathes.append(IndexPath.init(row: item, section: 0))
        }
        let indiciesSorted:[Int] = selectedItems.value!.sorted().reversed()
        for item in indiciesSorted {
            print(item)
            allMedia.remove(at: item)
        }
        
        
        self.manageMediaCollectionView.performBatchUpdates({
            self.manageMediaCollectionView.deleteItems(at: selectedIndexPathes)
        }) { (finished) in
            self.manageMediaCollectionView.reloadItems(at: self.manageMediaCollectionView.indexPathsForVisibleItems)
        }
        
        
       // self.manageMediaCollectionView.deleteItems(at: selectedIndexPathes)
        for url in urlsToDelete {
            viewModel.deleteUrl(removedUrl: url)
        }
        selectedItems.value?.removeAll()
        manageMediaCollectionView.reloadData()
        //viewModel.getMediaData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manageMediaCollectionView.register(UINib(nibName: "ManageMediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        // Do any additional setup after loading the view.
        selectedItems.value = [Int]()
        setupBinding()
    }

    func setupBinding() {
        
        viewModel.allMedia.asObservable().subscribe(onNext: { [weak self] allSavedMedia in
            if let mediaArray = allSavedMedia {
                self?.allMedia = mediaArray
                self?.manageMediaCollectionView.reloadData()
            }
        } ).disposed(by: disposeBag)
 
        selectedItems.asObservable().subscribe(onNext: { (selectedItems) in
            if selectedItems != nil && (selectedItems?.count)! > 0 {
                self.deleteBtn.isHidden = false
            }else {
                self.deleteBtn.isHidden = true
            }
        }).disposed(by: disposeBag)
    }
    


}
extension ManageMediaViewController :UICollectionViewDelegate , UICollectionViewDataSource {

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell =   collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ManageMediaCollectionViewCell
      
      let mediaItem = allMedia[indexPath.row]
      
        cell.viewOverlay.isHidden = !mediaItem.isSelected
        
        print(indexPath.row)
        let tapped = UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        tapped.numberOfTapsRequired = 1
        cell.clickedView.addGestureRecognizer(tapped)
        
        
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(handleLongPress))
        longPress.minimumPressDuration = 0.5
 //       longPress.allowableMovement = 15
        cell.clickedView.addGestureRecognizer(longPress)
        
        switch mediaItem.mediaType {
        case .audio:
            cell.imgae.image = nil
            cell.durationLabel.text = mediaItem.myUrl.getMediaTime()
            cell.playLabel.text = "\u{e020}"
            cell.videoIconLabel.text = ""
            
        case .video:
            cell.imgae.image = mediaItem.myUrl.createThumbnailOfVideoFromFileURL()
            cell.durationLabel.text =  mediaItem.myUrl.getMediaTime()
            cell.videoIconLabel.text = "\u{e020}"
            cell.playLabel.text = ""

        case.image:
            cell.imgae.sd_setImage(with: URL(string: mediaItem.myUrl.absoluteString), placeholderImage: nil)
            cell.videoIconLabel.text = ""
            cell.durationLabel.text =  ""
            cell.playLabel.text = ""


        }
        
       return cell
    }

    @objc func handleLongPress(recognizer: UILongPressGestureRecognizer) {
        
        if recognizer.state == UIGestureRecognizerState.began {
            let point = recognizer.location(in: manageMediaCollectionView as! UIView)
            
            print("recognizer View \(point)")
            
            let indexPath: IndexPath! = manageMediaCollectionView.indexPathForItem(at: point)
            
            
            var index = indexPath
            
            if (selectedItems.value?.isEmpty)! {
                allMedia[(index?.row)!].isSelected = true
                
                selectedItems.value?.append((index?.row)!)
            }else{
                if allMedia[(index?.row)!].isSelected {
                    allMedia[(index?.row)!].isSelected = false
                    let indexToRemove = selectedItems.value?.index(of: indexPath.row)
                    
                    selectedItems.value?.remove(at: indexToRemove!)
                }else {
                    allMedia[(index?.row)!].isSelected = true
                    selectedItems.value?.append((index?.row)!)
                }
            }
            manageMediaCollectionView.reloadItems(at: [index!])
        }
       
    }
    
    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        


        
        let point = recognizer.location(in: manageMediaCollectionView as! UIView)
        let indexPath: IndexPath! = manageMediaCollectionView.indexPathForItem(at: point)
        var index = indexPath
        
        if (selectedItems.value?.isEmpty)! {
            openUrl(withUrl: allMedia[(index?.row)!].myUrl)
        }else{
            if allMedia[(index?.row)!].isSelected {
                let indexRemoved = selectedItems.value?.index(of: (index?.row)!)
                selectedItems.value?.remove(at: indexRemoved!)
            }else {
                selectedItems.value?.append((index?.row)!)
            }
            allMedia[(index?.row)!].isSelected = !allMedia[(index?.row)!].isSelected
            manageMediaCollectionView.reloadItems(at: [index!])
        }
    }
    
    
  
}


extension ManageMediaViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width - 32
        return CGSize(width: width/2, height: width/2)
    }
}



extension ManageMediaViewController :UIDocumentInteractionControllerDelegate{
    
    func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
        self.appDelegate.addfloatingMenu()
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    func openUrl(withUrl url:URL){
        let docController:UIDocumentInteractionController = UIDocumentInteractionController.init(url: url)
        docController.delegate = self
        self.appDelegate.removeFloatingMenu()
        docController.presentPreview(animated: true)
    }
}

