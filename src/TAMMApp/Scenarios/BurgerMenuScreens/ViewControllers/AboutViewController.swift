//
//  AboutViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
protocol AboutViewControllerDelegate{
    func goToPrivacyPolicy()
    func goToTermsOfUse()
}
class AboutViewController: TammViewController,PerfernecesAboutStoryboardLoadable {

    @IBOutlet weak var AboutTableView: UITableView!
    
    var viewModel:AboutViewModel!
    let aboutCell = "AboutCell"
    var delegate: AboutViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar(title: "About", willSetSearchButton: true)
        
        AboutTableView.delegate = self
        AboutTableView.dataSource = self
        
        let bundle = Bundle(for: type(of: self))
        AboutTableView.register(UINib(nibName: aboutCell, bundle: bundle), forCellReuseIdentifier: aboutCell)
        // Do any additional setup after loading the view.
    }

}

extension AboutViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: aboutCell) as! AboutCell
        
        cell.goToPrivacyPolicyClosure = goToPrivacyPolicy
        cell.gotoTermsOfUseClosure = goToTermsOfUse
        
        return cell
    }
    
    func goToPrivacyPolicy() -> Void{
        delegate?.goToPrivacyPolicy()
    }
    
    func goToTermsOfUse() -> Void{
        delegate?.goToTermsOfUse()
    }

}
