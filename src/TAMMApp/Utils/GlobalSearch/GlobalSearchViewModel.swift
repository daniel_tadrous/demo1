//
//  EventsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
import EventKit

protocol GlobalSearchViewModelProtocol {
    func setLocationQueryValue(q:String?)
    func setIsToGetCategories()
    func startLocationService()
    func getCategoriesVar()->Variable<[Event.Category]?>
    func getLocationsVar()->Variable<[String]?>
    func getCurrentLocation()->Variable<CLLocation?>
}
class GlobalSearchViewModel{
    
    private let speechDuration: Double = 10.0
    private var speechDoneSubject: PublishSubject<String?>?
    private var speechRecognitionService: SpeechRecognitionServiceType!

    func startSpeechService() -> Observable<String?>{
        speechRecognitionService.start(delegate: self, duration: speechDuration)
        speechDoneSubject = PublishSubject<String?>()
        
        return (speechDoneSubject!)
    }
    
    private(set) var currentQueryVar : Variable<String?> = Variable(nil)
    private(set) var suggestionQueryVar : Variable<String?> = Variable(nil)
    private(set) var suggestion : Variable<SearchSuggestions?> = Variable(nil)
    private(set) var result : Variable<SearchResults?> = Variable(nil)
    private var apiService: ApiGlobalSearchType!
    
    private var disposeBag = DisposeBag()
    
    init(apiService: ApiGlobalSearchType,speechRecognitionService: SpeechRecognitionServiceType) {
        self.apiService = apiService
        self.speechRecognitionService = speechRecognitionService
        self.setupBinding()
    }
    
    private func setupBinding(){
        currentQueryVar.asDriver().drive(onNext: {[unowned self] (q) in
            if let q = q{
                if q.count >= 3{
                    self.requestDetails()
                }else{
                    self.suggestion.value = nil
                }
            }else{
                self.suggestion.value = nil
            }
        }).disposed(by: disposeBag)
        suggestionQueryVar.asDriver().drive(onNext: {[unowned self] (q) in
            if q != nil{
                self.requestResults()
            }else{
                self.result.value = nil
            }
        }).disposed(by: disposeBag)
    }
    
    private func requestDetails(){
        self.apiService.getSearchSuggestions(query: self.currentQueryVar.value!).asObservable().subscribe(onNext: { [unowned self] (res) in
            self.suggestion.value = res
        }).disposed(by: disposeBag)
    }
    private func requestResults(){
        self.apiService.getSearchResults(query: self.suggestionQueryVar.value!).asObservable().subscribe(onNext: { [unowned self] (res) in
            self.result.value = res
        }).disposed(by: disposeBag)
    }
}

extension GlobalSearchViewModel: SpeechRecognitionServiceTypeDelegate{
    func recognizerAvailable(isAvailable: Bool) {
        // indecates that the recognizer is available and most likely will start
        // should inform the subscribers with the change
        // disable the recording for some time
        if( isAvailable){
            speechDoneSubject?.onNext("")
            
        }
        else{
            speechDoneSubject?.onError("Service is not currently available" as Error)
        }
    }
    
    func textRecived(text: String) {
        speechDoneSubject?.onNext(text)
    }
    
    func recognitionIsDone(_ error: Error?) {
        speechDoneSubject?.onCompleted()
    }
}
