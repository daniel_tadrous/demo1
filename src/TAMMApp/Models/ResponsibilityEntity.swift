//
//  ResponsibilityEntity.swift
//  TAMMApp
//
//  Created by Daniel on 5/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class ResponsibleEntity: Mappable {
    var id: String?
    var name: String?
    var icon: String?
    var description: String?
    var address: Address?
    var officeHours: Hours?
    var publicServiceHours : Hours?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id            <- map["id"]
        name         <- map["name"]
        description  <- map["description"]
        icon         <- map["icon"]
        address  <- map["address"]
        icon   <- map["icon"]
        publicServiceHours  <- map["publicServiceHours"]
        officeHours  <- map["officeHours"]
    }
    class Hours: Mappable {
        var id: String?
        var entityId: String?
        var days: String?
        var times: String?
        required init?(map: Map) {
            
        }
        
        // Mappable
        func mapping(map: Map) {
            id <- map["id"]
            entityId <- map["entityId"]
            days <- map["days"]
            times <- map["times"]
        }
    }
    
    
  
}
