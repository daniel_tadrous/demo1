//
//  IntroLanguageSwitchViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 5/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation



class IntroLanguageSwitchViewModel{
    
    var userConfiguration: UserConfigServiceType
    
    init(userConfiguration: UserConfigServiceType) {
        self.userConfiguration = userConfiguration
    }
    
    func switchToLanguage(_ lang: TammSupportedLanguages){
        userConfiguration.changeLanguage(lang)
    }
    
}
