//
//  SearchWithoutTrending.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import RxSwift


protocol SearchWithoutTrendingDelegate {
    //implemented by View Controller
    func eventIsSelected(message:TalkToUsItemViewDataType)
    func backPressed()
}

class SearchWithoutTrending: UIView, UITammStackViewDelegate {
    
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTxt: LocalizedTextField!
    
 
    @IBOutlet weak var micBtn: UIButton!
 
    @IBOutlet var view: UIView!

    
    public var delegate:SearchWithoutTrendingDelegate!
    public let disposeBag = DisposeBag()
    var currentSearchModel:SearchModelWithoutTrending!
    
    private var text:String!
    
    func itemIsSelected(message: TalkToUsItemViewDataType) {
        delegate.eventIsSelected(message: message)
        self.removeFromSuperview()
    }
    
    
    

    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    init(frame: CGRect,text:String,currentSearchModel:SearchModelWithoutTrending) {
        super.init(frame:frame)
        self.currentSearchModel = currentSearchModel
        self.text = text
        
        commonInit()
        setupHooks()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("SearchWithoutTrendingView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds

        
        backBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        searchTxt.attributedPlaceholder = NSAttributedString(string: L10n.searchEllipsized, attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 155/255, green: 155/255, blue: 155/255, alpha: 1) , NSAttributedStringKey.font : UIFont.init(name: "CircularStd-Book", size: 16)!])
        searchTxt.delegate = self
    }
    
    func displayBlockingToast(message:String, duration: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        //        let alert = UILabel(frame: CGRect(x: UIScreen.main.bounds.width/4, y: UIScreen.main.bounds.height/4, width: UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.height/2))
        //alert.center = CGPoint(x: 160, y: 285)
        //        alert.textAlignment = .center
        //        alert.text = message
        //        alert.font.withSize(30)
        //        alert.backgroundColor = UIColor.white
        //        alert.addRoundCorners(radious: 20)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            //            UIApplication.shared.keyWindow!.addSubview(alert)
            //            UIApplication.shared.keyWindow!.bringSubview(toFront: alert)
            alert.show()
            //self.view.displayToast(message: message, Fortime: self.currentSearchModel.speechDuration)
        }
        // duration in seconds
        //        let duration: Double = duration
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            //  alert.removeFromSuperview()
            //alert.dismiss(animated: true, completion: nil)
            alert.dismiss(animated: true, completion: nil)
            let txt = self.searchTxt.text
            if txt != nil && txt != ""{
                self.searchTxt.becomeFirstResponder()
                self.searchTxt.text = txt
            }
        }
        
        
        
        
    }
    
    @IBAction func micBtnClicked(_ sender: Any) {
        if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: currentSearchModel.speechDuration)
            //currentSearchModel.startSpeechService()
            
            self.currentSearchModel.startSpeechService().asObservable().subscribe(onNext: { (str:String?) in
                self.searchTxt.text = str
               
            }).disposed(by: disposeBag)
        }
        
    }
    
    
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //
    //        //delegate?.suggestionIsSelected(message: self.searchTxt.text!)
    //        return true
    //    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.backPressed()
    }
    
    

    func setupHooks(){
        //searchTxt.addTarget(self, action: #selector(searchTextChanged), for: .editingChanged)
        searchTxt.text = self.text
        

    }

    

}

extension SearchWithoutTrending : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text!.isEmpty || textField.text!.trimmingCharacters(in: .whitespaces) == "" {
            Toast(message:"Please Type to search").Show()
        }else{
            
            delegate.eventIsSelected(message: PoppularItemViewData(name: textField.text!) )
            self.removeFromSuperview()
        }
        
        return true
    }
}

