//
//  iconLabel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class IconLabel:UILabel{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if(L102Language.currentAppleLanguage().contains("ar")){
              //self.transform = CGAffineTransform(scaleX: -transform.a, y: transform.d);
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        else{
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
        }
    }
}

