//
//  PHAsset+Extension.swift
//  TAMMApp
//
//  Created by marina on 6/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Photos
extension PHAsset {
    
    func export( completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
         let imgInfo = self.getFileSizeAndSizeFormatedAndName()
            let destination = IncidentUrl.getDirectoryForIncident().appendingPathComponent(imgInfo.2!)
        
                self.getURL(completionHandler: { imgUrl in
                    let imageData = try? Data(contentsOf: imgUrl!)
                    do {
                        try imageData?.write(to: destination)
                        completionHandler(destination,nil)
                    } catch {
                        print(error.localizedDescription)
                        completionHandler(nil,error)
                    }
            })
    }
    
func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
    if self.mediaType == .image {
        let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
        options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
            return true
        }
        self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
            completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
        })
    } else if self.mediaType == .video {
        let options: PHVideoRequestOptions = PHVideoRequestOptions()
        options.version = .original
        PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
            if let urlAsset = asset as? AVURLAsset {
                let localVideoUrl: URL = urlAsset.url as URL
                completionHandler(localVideoUrl)
            } else {
                completionHandler(nil)
            }
        })
    }
}
    func getHumanReadableSize(_ fileSize : UInt64) -> String{
        var convertedValue: Double = Double(fileSize)
        var multiplyFactor = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB", "PB",  "EB",  "ZB", "YB"]
        let rtlTokens = ["بايت", "كيلوبايت", "ميغابايت", "جيجابايت", "تيرابايت", "PB",  "EB",  "ZB", "YB"]
        while convertedValue > 1024 {
            convertedValue /= 1024
            multiplyFactor += 1
        }
        if(L102Language.currentAppleLanguage().contains("ar")){
            return String(format: "%4.2f %@", convertedValue, rtlTokens[multiplyFactor])
        }
        let ret = String(format: "%4.2f %@", convertedValue, tokens[multiplyFactor])
        return ret
    }
    
    func getFileSizeAndSizeFormatedAndName() -> (Int64?, String?, String?){
        let resources = PHAssetResource.assetResources(for: self) // your PHAsset
        
        var sizeOnDisk: Int64? = 0
        var fileName: String? = "FileName.NAV"
        
        if let resource = resources.first {
            let unsignedInt64 = resource.value(forKey: "fileSize") as? CLong
            sizeOnDisk = Int64(bitPattern: UInt64(unsignedInt64!))
            
            print (resource.originalFilename) // .value(forKey: "filename") )
            
            fileName = resource.originalFilename
        }
        
        return  (sizeOnDisk,self.getHumanReadableSize(UInt64(sizeOnDisk!)), fileName)
    }
    
}
