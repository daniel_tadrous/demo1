//
//  AuthenticationService.swift
//  TAMMApp
//
//  Created by kerolos on 3/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol AuthenticationServiceState: class {
    
    var presenter:UIViewController { get }
}

protocol AuthenticationService: class {
    var isAuthenticated : Bool {get}
    var authenticatedObservable: Observable<Bool> {get}
    func authenticate(authenticationServiceState: AuthenticationServiceState) ->  Observable<Bool>
    func logout(completionHandler: @escaping () -> Void, errorHanler:@escaping (Error) -> Void)
    func openSmartpassAppOrWebsite()
    func refreshToken(onComplete: @escaping (UIBackgroundFetchResult) -> Void)
    func updateUserInformation()

}
