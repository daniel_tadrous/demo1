//
//  ApplicationCoordinator.swift
//  TAMMApp
//
//  Created by Daniel on 9/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift
import Photos

enum ScreensEnum:String{
    case AspectsOfLifeViewController,MyCommunityViewController, MyLockerViewController, HtmlViewController, LanguageSwitchViewController,GovernmentEntitiesViewController, MediaCenterViewController, EntityServicesViewController, NewsViewController, PreferencesViewController, AccessibilityViewController, ProfileViewController, EnablePushNotificationViewController, AboutViewController, MediaSettingsViewController, ManageParentViewController, ManageMediaViewController, IntroScreenViewController, IntroLanguageSwitchViewController, AolDetailsViewController, AoIDetailsParentViewController, JourneysViewController, AspectsOfLifeListViewController, AolServiceWithFaqsViewController, AolSubTopicsViewController, LoginViewController, TalkToUsLandingViewController, LocationPickerEsriMapViewController, OfflineScreenViewController, MySupportMainViewController, FAQViewController, CheckStatusViewController, TalkToUsViewController, TalkToUsTabBarViewController, TalkToUsDraftsViewController, MessagesViewController, MyEventsParentViewController, MyEventsViewController, CaseDetailsViewController, MsgsParentViewController, MyDocumentViewController, ActiveDocumentsViewController, MyPaymentsParentViewController, MyPaymentViewController, MySubscriptionsParentViewController, MySubscriptionsChildViewController, PollsViewController, EventsViewController, CoCreateViewController, EventsParentViewController, NewsAndPressReleasesViewController, PublicHolidaysViewController, FactsAndFiguresViewController, InitiativeViewController, PressReleasesViewController, TopTappedViewController, ConfirmationViewController, IntroPagingViewController, MainTabViewController, CameraViewController, IncidentConfirmationViewController, SetReminderViewController,MapParentViewController
}
class ApplicationCoordinator: Coordinator{
    private let window: UIWindow
    let container: Container
    let navigationController : UINavigationController
    static var shared: ApplicationCoordinator!
    static var isToOpenUserProfile: Bool = false
    var tabBarViewController : MainTabViewController?
    var tabNavigationController: UINavigationController!{
        get{
            if let vc = self.tabBarViewController?.selectedViewController{
                return (vc as! UINavigationController)
            }else{
                self.tabBarViewController?.setSelected(item: .myUpdates)
                return (self.tabBarViewController?.selectedViewController as? UINavigationController)
            }
        }
    }
    //services
    private var authenticationService: AuthenticationService
    private var internetService:InternetCheckingService
    private var userConfigService: UserConfigServiceType
    
    private let disposeBag = DisposeBag()
    private var isFirstLaunch: Bool
    {
        get{
            return userConfigService.isFirstLaunch
        }
        set{
            userConfigService.isFirstLaunch = newValue
        }
    }
    //public static var isNewSession: Bool = true
    private var currentScreen: ScreensEnum!
    static var locationDelegate: MapParentViewControllerDelegate?
    init(window: UIWindow, container: Container) {
        self.window = window
        self.container = container
        self.navigationController = UINavigationController()
        self.window.rootViewController = self.navigationController
        self.authenticationService = self.container.resolve(AuthenticationService.self)!
        self.internetService =  InternetCheckingService.shared
        
        self.userConfigService = container.resolve(UserConfigServiceType.self)!
        internetService.hasInternet.asObservable().subscribe(onNext: {
           [unowned self] hasInternet in
            if hasInternet != nil{
                if hasInternet!{
                    print("Has Internet")
                    if self.currentScreen == ScreensEnum.OfflineScreenViewController{
                        self.load(screenEnum: .LoginViewController)
                    }
                }
                else{
                    print("no Internet")
                    if self.currentScreen == ScreensEnum.LoginViewController{
                        self.load(screenEnum: .OfflineScreenViewController)
                    }
                }
            }
        }).disposed(by: disposeBag)
        ApplicationCoordinator.shared = self
    }
    
    func startLogin() -> Bool  {
        authenticationService = self.container.resolve(AuthenticationService.self)!
        if  !authenticationService.isAuthenticated {
                _ = self.login()
                return true
        } else{
            AppCoordinator.isNewSession = false
            load(screenEnum: .MainTabViewController)
            
        }
        return false
    }
    func start() {
        if isFirstLaunch{
            isFirstLaunch = false
            load(screenEnum: .IntroLanguageSwitchViewController, false)
        }else{
            if !authenticationService.isAuthenticated {
                if !internetService.hasInternet.value!{
                    load(screenEnum: .OfflineScreenViewController)
                }else{
                    load(screenEnum: .LoginViewController)
                }
            } else{
                load(screenEnum: .MainTabViewController)
            }
        }
    }
    func showNavigationBar(_ show:Bool = true){
        self.navigationController.navigationBar.isHidden = !show
    }
    func embedViewController(child: UIViewController , To parent: ContainerViewController){
        child.willMove(toParentViewController: parent)
        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
        parent.selectedViewController = child
        parent.containerView.clearSubViews()
        parent.containerView.addSubview(child.view)
        parent.addChildViewController(child)
        child.didMove(toParentViewController: parent)
    }
    func load(screenEnum: ScreensEnum, _ animated: Bool = true){
        currentScreen = screenEnum
        switch screenEnum {
        case .IntroPagingViewController:
            let vc = container.resolveViewController(IntroPagingViewController.self)
            vc.coordinationDelegate = self
            vc.allViewControllers = getAllIntroViewControllers()
            showNavigationBar(false)
            navigationController.pushViewController(vc, animated: animated)
        case .IntroLanguageSwitchViewController:
            let vc = container.resolveViewController(IntroLanguageSwitchViewController.self)
            vc.delegate = self
            showNavigationBar(false)
            navigationController.pushViewController(vc, animated: animated)
        case .MainTabViewController:
            showNavigationBar(false)
            let vc = container.resolveViewController(MainTabViewController.self)
            tabBarViewController = vc
            //tabBarViewController?.setSelected(item: .myUpdates)
            vc.vcDelegate = self
            let sideMenuVc = container.resolveViewController(SideMenuViewController.self)
            vc.sideMenuViewController = sideMenuVc
            vc.sideMenuViewController?.delegate = self
            navigationController.pushViewController(vc, animated: animated)
            if InternetCheckingService.shared.hasInternet.value ?? false {
                if DALDrafts.getAll().count > 0{
                    let offlineDraftsPopUpView = OfflineDraftsPopUpView(frame: UIScreen.main.bounds,okClickHandler: self.showPendingDraftsScreen)
                    offlineDraftsPopUpView.tag = 1000
                    UIApplication.shared.keyWindow!.addSubview(offlineDraftsPopUpView)
                    UIApplication.shared.keyWindow!.bringSubview(toFront: offlineDraftsPopUpView)
                }
                
                // show offline map popup
                if !UserDefaultsKeys.getIsOfflinePopupAppeared() {
                    if !UserDefaultsKeys.getIsOfflinePopupAppeared() {
                        let apiSuportService = SupportServices()
                        let vC = ConfirmationMessageViewController()
                        
                        vC.initialize(title: L10n.downloadOfflineMap, isFirstTimeMap:true, CancelBtnTitle: L10n.cancel, confirmationBtnTitle: L10n.ok, okClickHandler: {}, apiService: apiSuportService)
                        
                        vc.present(vC, animated: true, completion: nil)
                    }
                    UserDefaultsKeys.changeisofflinePopupApearedValue()
                }
            }
        case .OfflineScreenViewController:
            let vc = container.resolveViewController(OfflineScreenViewController.self)
            vc.delegate = self
            showNavigationBar(false)
            self.navigationController.pushViewController(vc, animated: animated)
        case .LoginViewController:
            let vc = container.resolveViewController(LoginViewController.self)
            vc.delegate = self
            showNavigationBar(false)
            self.navigationController.pushViewController(vc, animated: animated)
        case .MyLockerViewController:
            self.tabBarViewController?.forceAnimateToTab(toIndex: 1)
            let vc = container.resolveViewController(MyLockerViewController.self)
            vc.delegate = self
            vc.viewModel = MyLockerViewModel();
            showNavigationBar(false)
            tabNavigationController.setViewControllers([vc], animated: animated)
        case .MyCommunityViewController:
            self.tabBarViewController?.forceAnimateToTab(toIndex: 3)
            let vc = container.resolveViewController(MyCommunityViewController.self)
            vc.delegate = self
            vc.viewModel = MyCommunityViewModel();
            showNavigationBar(false)
            tabNavigationController.setViewControllers([vc], animated: animated)
        case .PollsViewController:
            let vc = container.resolveViewController(PollsViewController.self)
            vc.viewModel.start.value = true
            tabNavigationController.pushViewController(vc, animated: animated)
        case .EventsParentViewController:
            let vc = container.resolveViewController(EventsParentViewController.self)
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .InitiativeViewController:
            let vc = container.resolveViewController(InitiativeViewController.self)
            vc.viewModel.getInitiatives()
            tabNavigationController.pushViewController(vc, animated: animated)
        case .FactsAndFiguresViewController:
            let vc = container.resolveViewController(FactsAndFiguresViewController.self)
            vc.viewModel.pageOpened.value = true
            tabNavigationController.pushViewController(vc, animated: animated)
        case .CoCreateViewController:
            let vc = container.resolveViewController(CoCreateViewController.self)
            vc.viewModel.start.value = true
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .NewsAndPressReleasesViewController:
            let vc = container.resolveViewController(NewsAndPressReleasesViewController.self)
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .MyEventsViewController:
            let vc = container.resolveViewController(MyEventsViewController.self)
            vc.getData()
            tabNavigationController.pushViewController(vc, animated: animated)
        case .TalkToUsTabBarViewController:
            self.tabBarViewController?.forceAnimateToTab(toIndex: MainTabItems.myLocker.rawValue)
            let vc = container.resolveViewController(TalkToUsTabBarViewController.self)
            vc.delegate = self
            if TalkToUsTabBarViewController.isFromOfflineScreenStatic{
                showNavigationBar(false)
                tabNavigationController.setViewControllers([vc], animated: animated)
            }else{
                tabNavigationController.pushViewController(vc, animated: animated)
            }
        case .MySupportMainViewController:
            let vc = container.resolveViewController(MySupportMainViewController.self)
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .AspectsOfLifeViewController:
            showNavigationBar(false)
            self.tabBarViewController?.forceAnimateToTab(toIndex: 2)
            let vc = container.resolveViewController(AspectsOfLifeViewController.self)
            vc.delegate = self
            tabNavigationController.setViewControllers([vc], animated: animated)
        case .MyPaymentsParentViewController:
            let vC = container.resolveViewController(MyPaymentsParentViewController.self)
            vC.delegate = self
            tabNavigationController.pushViewController(vC, animated: animated)
        case .MySubscriptionsParentViewController:
            let vC = container.resolveViewController(MySubscriptionsParentViewController.self)
            vC.delegate = self
            tabNavigationController.pushViewController(vC, animated: animated)
        case .MyDocumentViewController:
            let vC = container.resolveViewController(MyDocumentViewController.self)
            vC.delegate = self
            tabNavigationController.pushViewController(vC, animated: animated)
        case .AspectsOfLifeListViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVC = container.resolveViewController(AspectsOfLifeListViewController.self)
            childVC.delegate = self
            embedViewController(child: childVC, To: parentVC!)
        case .JourneysViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVC = container.resolveViewController(JourneysViewController.self)
            embedViewController(child: childVC, To: parentVC!)
        case .MsgsParentViewController:
            let vc = container.resolveViewController(MsgsParentViewController.self)
            vc.delegate = self
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .CameraViewController:
            let vc = container.resolveViewController(CameraViewController.self)
            vc.delegate =  self
            vc.hidesBottomBarWhenPushed = true
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .IncidentConfirmationViewController:
            let vc = container.resolveViewController(IncidentConfirmationViewController.self)
            vc.delegate = self
            vc.hidesBottomBarWhenPushed = true
            self.tabNavigationController.setViewControllers([vc], animated: animated)
        case .MessagesViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVC = container.resolveViewController(MessagesViewController.self)
            childVC.delegate = self
            childVC.viewModel.page.value = 1
            embedViewController(child: childVC, To: parentVC!)
        case .CaseDetailsViewController:
            let vc = container.resolveViewController(CaseDetailsViewController.self)
            vc.viewModel.id.value = CaseDetailsViewModel.idStatic
            vc.container = container
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .MySubscriptionsChildViewController:
            let childVCont = container.resolveViewController(MySubscriptionsChildViewController.self)
            let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
            embedViewController(child: childVCont, To: parentVC!)
        case .FAQViewController:
            let vc = container.resolveViewController(FAQViewController.self)
            vc.viewModel.pageId.value = 1
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .LocationPickerEsriMapViewController:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.removeFloatingMenu()
            let vc = container.resolveViewController(LocationPickerEsriMapViewController.self)
            vc.hidesBottomBarWhenPushed = true
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .EventsViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVCont = container.resolveViewController(EventsViewController.self)
            childVCont.viewModel.eventsFilterApplyModel.value = EventsFilterApplyModel()
            embedViewController(child: childVCont, To: parentVC!)
        case .PublicHolidaysViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVCont = container.resolveViewController(PublicHolidaysViewController.self)
            childVCont.viewModel.getMyEvents()
            childVCont.setupHooks()
            embedViewController(child: childVCont, To: parentVC!)
        case .AoIDetailsParentViewController:
            let parentVC = container.resolveViewController(AoIDetailsParentViewController.self)
            parentVC.delegate = self
            self.tabNavigationController.pushViewController(parentVC, animated: animated)
        case .TalkToUsLandingViewController:
            let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
            let vc = container.resolveViewController(TalkToUsLandingViewController.self)
            vc.delegate = self
            self.embedViewController(child: vc, To: parentVC!)
        case .NewsViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVCont = container.resolveViewController(NewsViewController.self)
            childVCont.viewModel?.newsPage.value = 1
            embedViewController(child: childVCont, To: parentVC!)
        case .PressReleasesViewController:
            let parentVC = tabNavigationController.topViewController as? ContainerViewController
            let childVCont = container.resolveViewController(PressReleasesViewController.self)
            embedViewController(child: childVCont, To: parentVC!)
        case .AolSubTopicsViewController:
            let vc = container.resolveViewController(AolSubTopicsViewController.self)
            vc.viewModel?.topicId.value = AolTopicDetailsViewModel.topicIdStatic
            vc.viewModel?.topicTitle.value = AolTopicDetailsViewModel.topicTitleStatic
            vc.serviceSelectedDelegate = self
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .AolServiceWithFaqsViewController:
            let vc = container.resolveViewController(AolServiceWithFaqsViewController.self)
            vc.viewModel?.serviceId.value = AolServiceWithFaqsViewModel.serviceIdStatic
            vc.viewModel?.serviceTitle.value = AolServiceWithFaqsViewModel.serviceTitleStatic
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .PreferencesViewController:
            let vc = container.resolveViewController(PreferencesViewController.self)
            vc.delegate = self
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .GovernmentEntitiesViewController:
            let vc = container.resolveViewController(GovernmentEntitiesViewController.self)
            vc.delegate = self
            vc.viewModel.start.value = true
            tabNavigationController.setViewControllers([vc], animated: animated)
        case .ProfileViewController:
            let previousViewController = tabNavigationController.topViewController
            previousViewController?.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
            let vc = container.resolveViewController(ProfileViewController.self)
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .EnablePushNotificationViewController:
            let previousViewController = tabNavigationController.topViewController
            previousViewController?.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
            let vc = container.resolveViewController(EnablePushNotificationViewController.self)
            vc.delegate = self
            vc.viewModel.ScreenOpened.value = 1
            tabNavigationController.pushViewController(vc, animated: animated)
        case .LanguageSwitchViewController:
            let vc = container.resolveViewController(LanguageSwitchViewController.self)
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .AccessibilityViewController:
            let vc = container.resolveViewController(AccessibilityViewController.self)
            tabNavigationController.pushViewController(vc, animated: animated)
        case .AboutViewController:
           let previousViewController = tabNavigationController.topViewController
           previousViewController?.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
            let vc = container.resolveViewController(AboutViewController.self)
            
           vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .MediaSettingsViewController:
           let previousViewController = tabNavigationController.topViewController
           previousViewController?.setBackTitleForNextPage(backTitleString: L10n.Preferences.settingsTitle, willSetSearchButton: true)
            let vc = container.resolveViewController(MediaSettingsViewController.self)
           vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .SetReminderViewController :
            let previousViewController = tabNavigationController.topViewController as? EnablePushNotificationViewController
            previousViewController?.setBackTitleForNextPage(backTitleString: L10n.Preferences.notifications, willSetSearchButton: true)
            let vc = container.resolveViewController(SetReminderViewController.self)
            vc.remiderAfter = previousViewController?.viewModel.apiResponse.value?.reminder
            tabNavigationController.pushViewController(vc, animated: animated)
        case .HtmlViewController:
            let vc = container.resolveViewController(HtmlViewController.self)
            tabNavigationController.pushViewController(vc, animated: animated)
        case .ManageParentViewController:
            let vc = container.resolveViewController(ManageParentViewController.self)
            vc.delegate = self
            tabNavigationController.pushViewController(vc, animated: animated)
        case .TopTappedViewController:
            let vc = container.resolveViewController(TopTappedViewController.self)
            vc.viewModel = TopTappedViewModel.getEntitiesInformationAndServices()
            vc.delegate = self
            self.tabNavigationController.pushViewController(vc, animated: animated)
        case .MapParentViewController:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.removeFloatingMenu()
            let vc = container.resolveViewController(MapParentViewController.self)
            vc.hidesBottomBarWhenPushed = true
            vc.delegate = self
            self.tabNavigationController.pushViewController(vc, animated: animated)
        default:
            print("Welcome")
        }
    }
}


extension ApplicationCoordinator : IntroLanguageSwitchViewControllerDelegate, PagingViewControllerDelegate{
    func introLanguageSelectionIsDone() {
       load(screenEnum: .IntroPagingViewController)
    }
    func pageViewControllerDidFinish() {
        if !internetService.hasInternet.value!{
            load(screenEnum: .OfflineScreenViewController)
        }else{
            load(screenEnum: .LoginViewController)
        }
    }
    func getAllIntroViewControllers() -> [IntroScreenViewController] {
        var controllers = [IntroScreenViewController]()
        for i in 0..<4{
            let vc = container.resolveViewController(IntroScreenViewController.self)
            vc.viewModel.pageNumber = i
            vc.viewModel.pageCount = 4
            controllers.append(vc)
        }
        return controllers
    }
}
extension ApplicationCoordinator: MainTabViewControllerDelegate{
   
    func startMyLocker() {
        load(screenEnum: .MyLockerViewController,false)
    }
    
    func startMyLockerWithSupport() {
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .MySupportMainViewController,false)
    }
    
    func startMyLockerWithMyEvents() {
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .MyEventsViewController,false)
    }
    
    func startMyLockerAtNewDraft(isFromOfflineScreen: Bool) {
        
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  isFromOfflineScreen
        TalkToUsTabBarViewController.selectedMessageTypeStatic = -1
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController,false)
    }
    func startTalkToUsWithIncident() {
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  false
        TalkToUsTabBarViewController.selectedMessageTypeStatic = 3
       // self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController,true)
    }
    func startAspectsOfLife() {
        AspectsOfLifeViewController.lunchMode = .SERVICES
        load(screenEnum: .AspectsOfLifeViewController,false)
    }
    
    func startMyCommunity(atItem: MyCommunityItems) {
        switch atItem {
        case .Home:
            load(screenEnum: .MyCommunityViewController,false)
        case .CoCreation:
            load(screenEnum: .CoCreateViewController,false)
            break
        case .CommunityEvents:
            load(screenEnum: .EventsParentViewController)
        case .News:
            NewsAndPressReleasesViewController.selectedTapStatic = .NEWS
            load(screenEnum: .NewsAndPressReleasesViewController)
        case .Press:
            NewsAndPressReleasesViewController.selectedTapStatic = .PRESS
            load(screenEnum: .NewsAndPressReleasesViewController)
            
        case .FactsAndFigures:
            load(screenEnum: .FactsAndFiguresViewController)
        case .Initiative:
            load(screenEnum: .InitiativeViewController)
        case .AboutAbudhabi:
            break
        case .Polls:
            load(screenEnum: .PollsViewController)
        }
    }
}

extension ApplicationCoordinator: SideMenuViewControllerDelegate{
    func myEventsIsPressed() {
        tabBarViewController!.setSelected(item: .myEvents)
    }
    
    func newsNSocialPressed() {
        NewsAndPressReleasesViewController.selectedTapStatic = .NEWS
        load(screenEnum: .NewsAndPressReleasesViewController)
    }
    
    func settingsPressed() {
        load(screenEnum: .PreferencesViewController)
    }
    
    func governmentEntitiesPressed() {
      load(screenEnum: .GovernmentEntitiesViewController)
    }
    
    func sideMenuCoCreatePressed() {
        tabBarViewController?.forceAnimateToTab(toIndex: MainTabItems.myCommunity.rawValue)
        load(screenEnum: .CoCreateViewController,false)
        
    }
    
    func aspectsOfLifePressed() {
        tabBarViewController!.setSelected(item: .aspectsOfLife)
    }
    func mysupportPressed(){
        tabBarViewController!.setSelected(item: .incident)
    }
    func myCommunityPressed(){
        tabBarViewController!.setSelected(item: .myCommunity)
    }
    func myUpdatesPressed(){
        tabBarViewController!.setSelected(item: .myUpdates)
    }
    func logoutPressed() {
        authenticationService.logout(completionHandler: {
            DispatchQueue.main.async {
                self.tabNavigationController.setViewControllers([self.tabNavigationController.topViewController!], animated: false)
                self.start()
            }
        }) { (error) in
            //error
        }
        
    }
    
    func newsAndEventsPressed() {
        // go to news Screen
        NewsAndPressReleasesViewController.selectedTapStatic = .NEWS
        load(screenEnum: .NewsAndPressReleasesViewController,false)
        
    }
    
    
    func changeLanguagePressed(){
        let emptyVc = container.resolveViewController(EmptyFlipViewController.self)
        self.tabNavigationController.popViewController(animated: false)
        self.tabNavigationController.pushViewController(emptyVc, animated: false)
        
        self.tabNavigationController.navigationItem.hidesBackButton = true
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { ( finished) -> Void in
            let vc = self.container.resolveViewController(MainTabViewController.self)
            self.tabBarViewController = vc
            vc.vcDelegate = self
            let sideMenuVc = self.container.resolveViewController(SideMenuViewController.self)
            
            vc.sideMenuViewController = sideMenuVc
            vc.sideMenuViewController?.delegate = self
            vc.navigationController?.setNavigationBarHidden(true, animated: false)
            self.tabNavigationController.popViewController(animated: false)
            self.tabNavigationController.pushViewController(vc, animated: true)
        }
        
    }
    
}
extension ApplicationCoordinator: OfflineScreensDelegate{
    func showOfflineScreen(){
        AppCoordinator.isNewSession = true
        load(screenEnum: .OfflineScreenViewController)
    }
    func showPendingDraftsScreen(){
        TalkToUsTabBarViewModel.LunchModeStatic = .DRAFT
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  false
        TalkToUsTabBarViewController.selectedMessageTypeStatic = -1
        showNavigationBar(false)
        //self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController, true)
    }
    
    func showNewDraft(isFromOfflineScreen: Bool = false){
        showNavigationBar(false)
        load(screenEnum: .MainTabViewController,false)
        tabBarViewController?.setMyLockerSelectedAtNewDraft(isFromOfflineScreen:isFromOfflineScreen)
    }
}

extension ApplicationCoordinator: LoginViewControllerDelegate,AuthenticationServiceState, AuthenticationCoordinatorDelegate{
    func authenticationCoordinatorDidFinish() {
       // ApplicationCoordinator.isNewSession = false
        if !ApplicationCoordinator.isToOpenUserProfile{
            load(screenEnum: .MainTabViewController)
        }else{
            load(screenEnum: .MainTabViewController,false)
            load(screenEnum: .PreferencesViewController,false)
            load(screenEnum: .ProfileViewController,true)
        }
    }
    var presenter: UIViewController {
        return navigationController.visibleViewController!
    }
    func authenticationViewControllerDismissed() {
        self.authenticationCoordinatorDidFinish()
    }
    
    func login() -> Observable<Bool> {
        let authed = authenticationService.authenticate(authenticationServiceState: self)
        _ = authed.subscribe(onNext: {[weak self] b in
            if b{
                self?.authenticationCoordinatorDidFinish()
            }
            
        })
        
        return authed
    }
    
    func reload() {
        AppDelegate.reset()
    }
    
    
}

extension ApplicationCoordinator: MyLockerViewControllerDelegate{
    func openMyDocuments() {
        load(screenEnum: .MyDocumentViewController)
    }
    
    func openMySupport() {
        load(screenEnum: .MySupportMainViewController)
    }
    func openMyEvents() {
       load(screenEnum: .MyEventsViewController)
    }
    
    
    func openMyPayments() {
        load(screenEnum: .MyPaymentsParentViewController)
    }
    func openMySubscriptions() {
        load(screenEnum: .MySubscriptionsParentViewController)
    }
    
}
extension ApplicationCoordinator: MyCommunityViewControllerDelegate{
    func openPolls(){
        load(screenEnum: .PollsViewController,true)
    }
    func openEvents(){
        load(screenEnum: .EventsParentViewController)
    }
    
    func openInitiative(){
        load(screenEnum: .InitiativeViewController)
    }
    
    func openFactsAndFigures(){
        load(screenEnum: .FactsAndFiguresViewController)
    }
    
    func openCoCreate(){
        load(screenEnum: .CoCreateViewController)
    }
    func openNewsAndPress(type:NewsAndPressReleasesEnum = .NEWS) {
        switch type {
        case .NEWS:
            NewsAndPressReleasesViewController.selectedTapStatic = .NEWS
            load(screenEnum: .NewsAndPressReleasesViewController)
        case .PRESS:
            NewsAndPressReleasesViewController.selectedTapStatic = .PRESS
            load(screenEnum: .NewsAndPressReleasesViewController)
        }
    }
    
}
extension ApplicationCoordinator: EventsParentViewControllerDelegate{
    func openEventsTab() {
        load(screenEnum: .EventsViewController)
    }
    
    func openPublicHolidays() {
        load(screenEnum: .PublicHolidaysViewController)
    }
}
extension ApplicationCoordinator: NewsAndPressReleasesViewControllerDelegate{
    func openNews() {
        load(screenEnum: .NewsViewController)
    }
    
    func openPressReleases() {
        load(screenEnum: .PressReleasesViewController)
    }
}
extension ApplicationCoordinator: CoCreateViewControllerDelegate{
    func coCreateDoPresentCollaborateConfirmation() {
        // view model and data will be set in the helper factory
        let vc = ConfirmationFactory.create(container: container, type: .collaborate, referenceNumber: nil)
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        self.navigationController.setViewControllers([vc], animated: true)
    }
    
    func openPollsFromCoCreate() {
        load(screenEnum: .PollsViewController)
    }
}
extension ApplicationCoordinator: TalkToUsTabBarViewControllerDelegate{
    func openTalkToUsNewTab( isFromOfflineScreen:Bool = false){
        TalkToUsLandingViewController.isFromOfflineScreenStatic = isFromOfflineScreen
        load(screenEnum: .TalkToUsLandingViewController)
    }
    func openTalkToUsNew(item:String,typeId:Int, isFromOfflineScreen:Bool = false){
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let vc = container.resolveViewController(TalkToUsViewController.self)
        vc.isFromOfflineScreen = isFromOfflineScreen
        vc.selectedMessageId = typeId
        vc.viewModel.title = item
        vc.delegate = self
        self.embedViewController(child: vc, To: parentVC!)
    }
    func openTalkToUsEditMode(draftId: String, isFromOfflineScreen:Bool = false){
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let vc = container.resolveViewController(TalkToUsViewController.self)
        vc.isFromOfflineScreen = isFromOfflineScreen
        vc.draftId = draftId
        vc.delegate = self
        self.embedViewController(child: vc, To: parentVC!)
    }
    func openDrafts(isFromOfflineScreen:Bool = false){
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let vc = container.resolveViewController(TalkToUsDraftsViewController.self)
        vc.isFromOfflineScreen = isFromOfflineScreen
        vc.delegate = parentVC! as? TalkToUsDraftsViewDelegate
        self.embedViewController(child: vc, To: parentVC!)
    }
}
extension ApplicationCoordinator: MySupportMainViewControllerDelegate{
    func talkToUsClicked() {
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  false
        TalkToUsTabBarViewController.selectedMessageTypeStatic = -1
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController, true)
    }
    
    func checkStatusClicked() {
        load(screenEnum: .MsgsParentViewController)
    }
    
    func faqClicked() {
        load(screenEnum: .FAQViewController)
    }
    
    
}
extension ApplicationCoordinator: AspectsOfLifeViewControllerDelegate{
    func openAspectsOfLifeList() {
       load(screenEnum: .AspectsOfLifeListViewController)
    }
    
    func openJourneys() {
        load(screenEnum: .JourneysViewController)
    }
}
extension ApplicationCoordinator: MyDocumentViewControllerDelegate{
    func openActive() {
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let childVCont = container.resolveViewController(ActiveDocumentsViewController.self)
        childVCont.viewModel.getActiveDocuments()
        childVCont.documentsType = .Active
        childVCont.setupHooks()
        embedViewController(child: childVCont, To: parentVC!)
    }
    
    func openExpired() {
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let childVCont = container.resolveViewController(ActiveDocumentsViewController.self)
        childVCont.viewModel.getExpiredDocuments()
        childVCont.documentsType = .Expired
        childVCont.setupHooks()
        embedViewController(child: childVCont, To: parentVC!)
    }
}
extension ApplicationCoordinator: MyPaymentsParentDelegate{
    func openUnpaid() {
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let childVCont = container.resolveViewController(MyPaymentViewController.self)
        childVCont.viewModel.paymentType = PaymentType.pending
        childVCont.viewModel.page.value = 1
        embedViewController(child: childVCont, To: parentVC!)
    }
    
    func openPaid() {
        let parentVC = self.tabNavigationController.topViewController as? ContainerViewController
        let childVCont = container.resolveViewController(MyPaymentViewController.self)
        childVCont.viewModel.paymentType = PaymentType.paid
        childVCont.viewModel.page.value = 1
        embedViewController(child: childVCont, To: parentVC!)
    }
}
extension ApplicationCoordinator: MySubsciptionsViewControllerDelegate{
    func openActiveSubsciptions() {
        load(screenEnum: .MySubscriptionsChildViewController)
    }
    
    func openExpiredSubsciptions() {
        load(screenEnum: .MySubscriptionsChildViewController)
    }
}
extension ApplicationCoordinator: ConfirmationViewControllerDelegate{
    func confirmationDidFinish(type: ConfirmationType) {
        load(screenEnum: .MyCommunityViewController)
    }
    
    
}
extension ApplicationCoordinator: AspectsOfLifeListViewControllerSelectionDelegate{
    func didSelectAspect(itemId: String, title: String) {
        AoIDetailsParentViewModel.titleStatic = title
        AoIDetailsParentViewModel.idStatic = itemId
        load(screenEnum: .AoIDetailsParentViewController)
        
    }
}
extension ApplicationCoordinator: AoIDetailsParentViewControllerDelegate{
    func openAspectsOfLifeDetails(id: String , title:String) {
        let parentVC = tabNavigationController.topViewController as? ContainerViewController
        let childVC = container.resolveViewController(AolDetailsViewController.self)
        childVC.viewModel?.aspectId.value = id
        childVC.viewModel?.aspectTitle.value = title
        childVC.delegate = self
        childVC.selectDelegate = self
        self.embedViewController(child: childVC, To: parentVC!)
    }
    
    func openJourneysInDetails(){
        load(screenEnum: .JourneysViewController)
    }
}
extension ApplicationCoordinator: AolDetailsViewControllerDelegate,AspectsOfLifeTopicsTableViewDelegate{
    func aspectOfLifeSubtopicDidFinish() {
        
    }
    
    func selectedTopic(topic: AspectOfLifeServiceTopicViewData) {
        AolTopicDetailsViewModel.topicIdStatic = topic.serviceTopicId
        AolTopicDetailsViewModel.topicTitleStatic = topic.serviceTopicTitle
        load(screenEnum: .AolSubTopicsViewController)
    }
}
extension ApplicationCoordinator: TalkToUsViewControllerDelegate{
    func checkStatusClickedInTalkToUs() {
        load(screenEnum: .MsgsParentViewController, true)
    }
    
    func talkToUsItemIsSelected(item: TalkToUsItemViewDataType) {
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = item
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  false
        TalkToUsTabBarViewController.selectedMessageTypeStatic = -1
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController, true)
    }
    
    func talkToUsItemIsSelected(item: String) {
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        TalkToUsTabBarViewModel.titleStatic = item
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  false
        TalkToUsTabBarViewController.selectedMessageTypeStatic = -1
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController, true)
    }
    
    func talkToUsMessageTypeSelected(id: Int, isFromOfflineScreen: Bool) {
        TalkToUsTabBarViewModel.LunchModeStatic = .NEW
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  isFromOfflineScreen
        TalkToUsTabBarViewController.selectedMessageTypeStatic = id
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        
        load(screenEnum: .TalkToUsTabBarViewController, true)
    }
}
extension ApplicationCoordinator: TalkToUsSaveViewControllerDelegate{
    func openLocationPicker(_ locationDelegate: MapParentViewControllerDelegate, withSearchText: String?, mapPlace: MapPlaceObject?) {
        ApplicationCoordinator.locationDelegate = locationDelegate
        MapParentViewController.initialTextSearchStatic = withSearchText
        if mapPlace != nil {
            MapParentViewController.selectedPlaceStatic = mapPlace
        }
        load(screenEnum: .MapParentViewController)
    }
    
    
    func openCameraVC(allowed:Int) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.removeFloatingMenu()
        showNavigationBar(false)
        CameraViewController.numberOfImagesStatic = CGFloat(allowed)
        load(screenEnum: .CameraViewController)
    }
    
    
    func openMediaActionbar(actionSheet: GalleryActionSheet){
        actionSheet.show(inView: (self.tabBarViewController?.view)!)
    }
    
    func displayErrorOverlay() {
        let errorView = ErrorView(frame:(self.tabBarViewController?.view.bounds)!)
        self.tabBarViewController?.view.addSubview(errorView)
        // vc.view.addSubview(errorView)
    }
    
    func openConfirmation(refrenceNumber: String) {
        tabBarViewController?.tabBar.isHidden = true
        
        IncidentConfirmationViewModel.refrenceNumberStatic = refrenceNumber
        load(screenEnum: .IncidentConfirmationViewController)
    }
    
    func openDraftsScreen(){
        
        TalkToUsTabBarViewModel.LunchModeStatic = .DRAFT
        TalkToUsTabBarViewModel.titleStatic = ""
        TalkToUsTabBarViewModel.itemStatic = nil
        TalkToUsTabBarViewController.isFromOfflineScreenStatic =  false
        TalkToUsTabBarViewController.selectedMessageTypeStatic = -1
        self.tabBarViewController?.animateToTab(toIndex: MainTabItems.myLocker.rawValue)
        load(screenEnum: .TalkToUsTabBarViewController, true)
    }
}
extension ApplicationCoordinator: MapParentViewControllerDelegate{
    func locationPickerDidSelectLocation(_ viewController : MapParentViewController, _ location: LocationPickerLocation) {
        
        self.tabNavigationController.popViewController(animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.addfloatingMenu()
        ApplicationCoordinator.locationDelegate?.locationPickerDidSelectLocation(viewController, location)
    }
    
    func locationPickerDidCancel(_ viewController : MapParentViewController) {
        
        self.tabNavigationController.popViewController(animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.addfloatingMenu()
        ApplicationCoordinator.locationDelegate?.locationPickerDidCancel(viewController)
        
    }

    func openOfflineMap() {
        let parentVC = tabNavigationController.topViewController as! ContainerViewController
        let childVCont = container.resolveViewController(OfflineMapViewController.self)
        childVCont.parentVC = parentVC as! MapParentViewController
        embedViewController(child: childVCont, To: parentVC)
    }
    
    
    func openOnlineMap() {
        let parentVC = tabNavigationController.topViewController as! ContainerViewController
        let childVCont = container.resolveViewController(LocationPickerEsriMapViewController.self)
        childVCont.parentVC = parentVC  as! MapParentViewController
        embedViewController(child: childVCont, To: parentVC)
    }
}
extension ApplicationCoordinator: AspectsOfLifeServiceTableViewDelegate{
    func selectedService(service: AspectOfLifeSubTopicServiceViewData, subTopicTitle: String) {
        AolServiceWithFaqsViewModel.serviceIdStatic = service.subTopicServiceId
        AolServiceWithFaqsViewModel.serviceTitleStatic = subTopicTitle
        load(screenEnum: .AolServiceWithFaqsViewController)
    }
}
extension ApplicationCoordinator: MsgsParentViewControllerDelegate{
    func openActiveMsgs() {
        MessagesViewModel.msgTypeStatic = MessageType.open
        load(screenEnum: .MessagesViewController)
    }
    
    func openClosed() {
        MessagesViewModel.msgTypeStatic = MessageType.closed
        load(screenEnum: .MessagesViewController)
    }
}
extension ApplicationCoordinator: CameraViewControllerDelegate{
    func cameraIsDismissed(assests: [PHAsset]) {
        let assets:[PHAsset] =  assests
        self.tabNavigationController.popViewController(animated: false)
        showNavigationBar(true)
        self.tabNavigationController.hidesBottomBarWhenPushed = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.addfloatingMenu()
        let vc  = self.tabNavigationController.visibleViewController as? TalkToUsTabBarViewController
        let selectedVC =  vc?.selectedViewController as? TalkToUsViewController
        selectedVC?.viewModel.setRawMedia(rawMedia: assets)
    }
}
extension ApplicationCoordinator:IncidentConfirmationViewControllerDelegate{
    func goToMyLocker() {
        load(screenEnum: .MyLockerViewController, true)
    }
}
extension ApplicationCoordinator: MessagesDelegate{
    func openCaseDetails(id: Int) {
        CaseDetailsViewModel.idStatic = id
        load(screenEnum: .CaseDetailsViewController)
    }
}
extension ApplicationCoordinator: PreferencesViewControllerDelegate{
    func preferencesWantsToGoToProfilePage() {
        load(screenEnum: .ProfileViewController)
    }
    
    func preferencesWantsToGoToLanguageSwitchPage() {
        load(screenEnum: .LanguageSwitchViewController)
    }
    
    func preferencesWantsToGoToAccessibilityPage() {
        load(screenEnum: .AccessibilityViewController)
    }
    
    func preferencesWantsToGoToEnablePushNotificationPage() {
        load(screenEnum: .EnablePushNotificationViewController)
    }
    
    func preferencesWantsToGoToAboutPage() {
        load(screenEnum: .AboutViewController)
    }
    
    func preferencesWantsToGoToMediaSettings() {
        load(screenEnum: .MediaSettingsViewController)
    }
}
extension ApplicationCoordinator: ProfileViewControllerDelegate{
    
}
extension ApplicationCoordinator: EnablePushNotificationDelegate{
    func openSetReminderScreen() {
        load(screenEnum: .SetReminderViewController)
    }
}
extension ApplicationCoordinator: LanguageSwitchViewControllerDelegate{
    func languageSwitchViewControllerDidChangeLanguage() {
        //To Do change lang
    }
}
extension ApplicationCoordinator: AboutViewControllerDelegate{
    func goToPrivacyPolicy() {
        HtmlViewController.viewModelStatic = PrivacyViewModel(apiService: container.resolve(ApiSettingsServicesTypes.self)!)
        load(screenEnum: .HtmlViewController)
    }
    
    func goToTermsOfUse() {
        HtmlViewController.viewModelStatic = TermsViewModel(apiService: container.resolve(ApiSettingsServicesTypes.self)!)
        load(screenEnum: .HtmlViewController)
    }
}
extension ApplicationCoordinator: MediaSettingsViewControllerDelegate{
    func openManageScreen() {
       load(screenEnum: .ManageParentViewController)
    }
}
extension ApplicationCoordinator: ManageParentViewControllerDelegate{
    func openManageMedia() {
        let parentVC = tabNavigationController.topViewController as! ContainerViewController
        let childVCont = container.resolveViewController(ManageMediaViewController.self)
        embedViewController(child: childVCont, To: parentVC)
        childVCont.viewModel.getMediaData()
    }
    
    func openManageDocuments() {
        let parentVC = tabNavigationController.topViewController as! ContainerViewController
        let childVCont = container.resolveViewController(ManageViewController.self)
        embedViewController(child: childVCont, To: parentVC)
        childVCont.viewModel.getMediaData()
    }
    
    
}
extension ApplicationCoordinator: GovernmentEntitiesViewControllerDelegate{
    func openGovernmentEntityInformation(id: String) {
        TopTappedViewModel.idStatic = id
        load(screenEnum: .TopTappedViewController)
    }
}
extension ApplicationCoordinator: TopTappedViewControllerDelegate,EntitiesInformationViewControllerDelegate,EntityServicesViewControllerDelegate{
    func openFirstTab() {
        let parentVC = tabNavigationController.topViewController as! ContainerViewController
        let childVC = container.resolveViewController(EntitiesInformationViewController.self)
        childVC.delegate = self
        childVC.viewModel.entitiyId = TopTappedViewModel.idStatic
        embedViewController(child: childVC, To: parentVC)
    }
    
    func openSecondTab() {
        let parentVC = tabNavigationController.topViewController as! ContainerViewController
        let childVC = container.resolveViewController(EntityServicesViewController.self)
        childVC.viewModel?.serviceCategoryId.value = TopTappedViewModel.idStatic
        childVC.delegate = self
        embedViewController(child: childVC, To: parentVC)
    }
}

