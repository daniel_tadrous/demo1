//
//  AttachementTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var iconLbl: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let outputFormat = L10n.Lang == "en" ? "dd MMM 'at' HH:mm a" : "dd MMM 'في' HH:mm a"
    
    let iconStr = "\u{e047}"
    override func awakeFromNib() {
        super.awakeFromNib()
        iconLbl.text = iconStr
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var comments:[MsgAdditionalComments] = []
    
    func setData(comments:[MsgAdditionalComments]){
        
        self.comments = comments
        _ = self.stackView.arrangedSubviews.map { (v) -> UIView in
            v.isHidden = true
            
            return v
        }
        
        _ = self.comments.map { (com) -> MsgAdditionalComments in
            
            let dateLabel = getInitializedLabelBold(frame: CGRect(x: 0, y: 0, width: self.stackView.frame.width, height: 20))
            let label = getInitializedLabel(frame: CGRect(x: 0, y: 0, width: self.stackView.frame.width, height: 20))
            if com.date != nil {
                dateLabel.text = com.date?.getDate(format: inputFormat)?.getFormattedString(format: outputFormat)
            }
            if com.text != nil {
                label.text = com.text!
            }
   
            
            self.stackView.addArrangedSubview(dateLabel)
            
            self.stackView.addArrangedSubview(label)
            return com
        }
        
    }
    
    
    
  
    
    private func getInitializedLabel(frame: CGRect)-> LocalizedLabel{
        let label: LocalizedLabel = LocalizedLabel(frame: frame)
        label.font = L10n.Lang == "en" ? UIFont(name: "Roboto-Regular", size: 14) : UIFont(name: "Swissra-Normal", size: 14)
        label.applyFontScalling = true
        label.originalFont = UIFont(name: "Roboto-Regular", size: 14)
        label.RTLFont = "Swissra-Normal"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = UIColor.clear
        return label
    }
    private func getInitializedLabelBold(frame: CGRect)-> LocalizedLabel{
        let label: LocalizedLabel = LocalizedLabel(frame: frame)
        label.font = L10n.Lang == "en" ? UIFont(name: "CircularStd-Bold", size: 14) : UIFont(name: "Swissra-Bold", size: 14)
        label.applyFontScalling = true
        label.originalFont = UIFont(name: "CircularStd-Bold", size: 14)
        label.RTLFont = "Swissra-Bold"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = UIColor.clear
        return label
    }
    
}

