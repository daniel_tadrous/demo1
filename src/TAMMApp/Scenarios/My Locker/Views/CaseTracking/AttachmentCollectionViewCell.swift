//
//  AttachmentCollectionViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AttachmentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let outputFormat = L10n.Lang == "en" ? "dd MMM 'at' HH:mm a" : "dd MMM 'في' HH:mm a"
    
    var onClick:(()->Void)?
    var onDeleteClick:(()->Void)?
    @IBOutlet weak var iconLbl: UILabel!
    
    @IBOutlet weak var xLbl: UILabel!
    @IBOutlet weak var xBtn: UIButton!
    
    @IBOutlet weak var fileTitleLbl: UILabel!
    
    @IBOutlet weak var fileSizeLabl: UILabel!
    
    @IBAction func fileClick(_ sender: UIButton) {
        self.onClick?()
    }
    @IBAction func xClick(_ sender: UIButton) {
        onDeleteClick?()
    }
    
    @IBOutlet weak var dateLbl: UILabel!
    var click:((MsgAttachment,@escaping ()->Void)->Void)?
    var mediaClick:((Media,@escaping ()->Void)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(attachment: MsgAttachment,click:((MsgAttachment,@escaping ()->Void)->Void)?) {
        self.click = click
        switch attachment.type{
        case .VIDEO:
            self.iconLbl.text = "\u{e020}"
        case .AUDIO:
            self.iconLbl.text = "\u{e04d}"
        case .IMAGE:
            self.iconLbl.text = "\u{e041}"
        case .TXT:
            self.iconLbl.text = "\u{e046}"
        }
        self.fileTitleLbl.text = attachment.name
        self.fileSizeLabl.text  = attachment.size
        self.dateLbl.text = attachment.date?.getDate(format: inputFormat)?.getFormattedString(format: outputFormat)
        
        self.onClick = {
            self.progressIndicator.startAnimating()
            self.click?(attachment,{
                self.progressIndicator.stopAnimating()
            })
        }
    }
    func setData(attachment: Media,click:((Media,@escaping ()->Void)->Void)?) {
        self.mediaClick = click
        switch attachment.type{
        case .VIDEO:
            self.iconLbl.text = "\u{e020}"
        case .AUDIO:
            self.iconLbl.text = "\u{e04d}"
        case .IMAGE:
            self.iconLbl.text = "\u{e041}"
        case .TXT:
            self.iconLbl.text = "\u{e046}"
        }
        self.fileTitleLbl.text = attachment.name
        self.fileSizeLabl.text  = attachment.fileSize
        self.onClick = {
//            self.progressIndicator.startAnimating()
//            self.mediaClick?(attachment,{
//                self.progressIndicator.stopAnimating()
//            })
        }
    }
}
