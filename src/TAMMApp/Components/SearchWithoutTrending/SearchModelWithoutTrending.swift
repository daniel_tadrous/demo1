//
//  SearchModelWithoutTrending.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class SearchModelWithoutTrending :  SpeechRecognitionServiceTypeDelegate {
    
    
    var speechDoneSubject: PublishSubject<String?>?
    var speechRecognitionService: CapturedSpeechToTextService = CapturedSpeechToTextService()
    let speechDuration:Double = 10
    
    func textRecived(text: String) {
        //        micRecognizedVar.value = text
        speechDoneSubject?.onNext(text)
    }
    
    
    func isServiceAvailable() -> Bool{
        return CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable()
    }
    
    
    func recognitionIsDone(_ error: Error?) {
        // should inform the ui with the status and reset the availablility
        //        micRecodingIsEnabledVar.value = true
        //        micRecordingErrorVar.value = "\(String(describing: error))"
        speechDoneSubject?.onCompleted()
    }
    
    
    
    func startSpeechService() -> Observable<String?>{
        speechRecognitionService.start(delegate: self, duration: speechDuration)
        speechDoneSubject = PublishSubject<String?>()
        //        micRecognizedVar.value = ""
        return (speechDoneSubject!)
    }
    
    
    func recognizerAvailable(isAvailable: Bool) {
        // indecates that the recognizer is available and most likely will start
        // should inform the subscribers with the change
        // disable the recording for some time
        if( isAvailable){
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onNext("")
            
        }
        else{
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onError("Service is not currently available" as! Error)
        }
    }
    
    
    
}
