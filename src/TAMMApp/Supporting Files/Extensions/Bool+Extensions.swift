//
//  Bool+Extensions.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

extension Bool{
    public var IntValue:Int{
        get{
            if(self){
                return 1
            }
            return 0
        }
    }
}
