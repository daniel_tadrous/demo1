//
//  PaymentTypeModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentTypeModel:Mappable {
    
    var text: String?
    var url: String?
    
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        text <- map["text"]
        url <- map["url"]
    }
    
}
