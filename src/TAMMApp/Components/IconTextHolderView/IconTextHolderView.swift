//
//  IconTextHolderView.swift
//  TAMMApp
//
//  Created by Daniel on 7/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class IconTextHolderView: RoundedView{

    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var iconLabel: LocalizedLabel!
    @IBOutlet weak var label: LocalizedLabel!
    @IBOutlet var contentView: UIView!
    @IBInspectable
    var LabelText: String = "" { didSet {
        
        if L10n.Lang == "en"{
            label.text = LabelText
        }
        
        } }
    @IBInspectable
    var ArLabelText: String = "" { didSet {
        
        if L10n.Lang == "ar" && ArLabelText != ""  {
            label.text = ArLabelText
        }
        } }
    @IBInspectable
    var LabelTextSize: Float = 16 { didSet {
        label.font = UIFont(name: "CircularStd-Bold", size: CGFloat(LabelTextSize))
        label.originalFont = label.font
        } }
    
    @IBInspectable
    var IconText: String = "" { didSet { iconLabel.text = IconText } }
    @IBInspectable
    var IconTextSize: Float = 36 { didSet {
       iconLabel.font = iconLabel.font.withSize( CGFloat(IconTextSize))
        iconLabel.originalFont  = iconLabel.font
        } }
    @IBInspectable
    var LabelColor: UIColor = UIColor.darkBlueGrey { didSet { label.textColor = LabelColor } }
    @IBInspectable
    var circleColor: UIColor = UIColor.clear { didSet {
        circleView.backgroundColor = circleColor
        circleView.addRoundAllCorners(radious: circleView.frame.width/2)
        
        } }
    @IBInspectable
    var IconColor: UIColor = UIColor.darkBlueGrey { didSet { iconLabel.textColor = IconColor } }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        commonInit()
    }
    
    func loadViewFromNib() -> UIView? {
        let nibName = "IconTextHolderView"
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self,options: nil).first as? UIView
    }
    private func commonInit(){
       //iconLabel.font = iconLabel.font.withSize( CGFloat(IconTextSize))
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
//    override func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        xibSetup()
//        commonInit()
//        contentView?.prepareForInterfaceBuilder()
//    }
}
