//
//  TermsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class TermsViewModel: HtmlViewModel{
    fileprivate var disposeBag = DisposeBag()
    private var apiService: ApiSettingsServicesTypes!
    
    init(apiService:ApiSettingsServicesTypes) {
        super.init()
        self.title = L10n.terms
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate  func setupBinding(){
        self.loadpage()
    }
    
    fileprivate func loadpage() {
        apiService.getTerms().asObservable().subscribe(onNext:{ response in
            self.content.value = response.termsOfUse
            
        } ).disposed(by: disposeBag)
        
    }
}
