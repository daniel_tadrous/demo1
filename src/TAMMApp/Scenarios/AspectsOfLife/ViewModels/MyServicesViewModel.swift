//
//  MyServicesViewModel.swift
//  TammUI
//
//  Created by Marina.Riad on 4/11/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//
import RxSwift

class MyServicesViewModel{
    private(set) var services:Variable<[AspectOfLifeViewData]> = Variable([])
    private var disposeBag = DisposeBag()
    
    
//    init() {
//        self.fillServices()
//        self.fillServices()
//    }
    
    let apiService: ApiServiceAspectsOfLifeType!
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
           // fillServices()
           // fillServices()
    }
        
//      // used for UI Demos
//        fileprivate func fillServices(){
//            // call services
//            let fixedUrl = URL(string:"https://cdn.iconscout.com/public/images/icon/free/png-512/iphonex-iphone-x-apple-event-device-mobile-3c5c3e68547a213a-512x512.png")!
//
//            services.value.append(MyServices(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 1, AspectOfLifeHeader: "Moving & Leaving", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.MovingNLeavingIconString, AspectOfLifeImageColor:String.MovingNLeavingBackgroundColorString ))
//
//            services.value.append(MyServices(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 2, AspectOfLifeHeader: "Housing & Property", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.HousingNPropertyIconString, AspectOfLifeImageColor:String.HousingNPropertyBackgroundColorString ))
//
//            services.value.append(MyServices(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 3, AspectOfLifeHeader: "Visa & EmiratesID", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.VisaNEmiratesIDIconString, AspectOfLifeImageColor:String.VisaNEmiratesIDBackgroundColorString ))
//
//            services.value.append(MyServices(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 4, AspectOfLifeHeader: "Work & Employment", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.WorkNEmploymentIconString, AspectOfLifeImageColor:String.WorkNEmploymentBackgroundColorString ))
//
//            services.value.append(MyServices(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 5, AspectOfLifeHeader: "Health & Safety", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.HealthNSafetyIconString, AspectOfLifeImageColor:String.HealthNSafetyBackgroundColorIconString ))
//        }
//
    
    func getMyServices(){
        // will call the api then returns the services
        apiService.getAspectsOfLife().subscribe(onNext: { arr in
//            print(self.apiService)
            var arr1 = arr + arr + arr
            arr1 = arr1 + arr1 + arr1 + arr1 + arr1 + arr1
            self.services.value = arr1

        }).disposed(by: disposeBag)
        
        //return services
    }
    
}
protocol AspectOfLifeViewData {
    var AspectOfLifeID:Int { get }
    var AspectOfLifeHeader:String { get }
    var AspectOfLifeDescription:String { get }
    var AspectOfLifeImage:String { get }
    var AspectOfLifeImageColor:String { get }
    var AspectOfLifeImageUrl: URL { get }
}

extension JourneyModel: AspectOfLifeViewData{
//    var id: String?
//    var title: String?
//    var description: String?
//    var icon: URL?
//    var aspectType: Int?
//
    var AspectOfLifeID: Int {
        get{
            return id!
        }
    }
    
    var AspectOfLifeHeader: String {
        get{
            return title ?? "Title Not Found"
        }
    }
    
    var AspectOfLifeDescription: String {
        return description ?? "Description Not Found"
    }
    
    var AspectOfLifeImage: String {
        return self.icon!
    }
    
    var AspectOfLifeImageUrl: URL {
        return URL(string: self.icon!)!
    }
    
    var AspectOfLifeImageColor: String {
        return String.HealthNSafetyBackgroundColorIconString
    }
    
    
}


struct MyServices:AspectOfLifeViewData{
    var AspectOfLifeImageUrl: URL
    
    
    var AspectOfLifeID:Int
    
    var AspectOfLifeHeader: String
    
    var AspectOfLifeDescription: String
    
    var AspectOfLifeImage: String
    
    var AspectOfLifeImageColor: String
}

extension String{
    public static var MovingNLeavingIconString: String{
        return "J"
    }
    public static var HousingNPropertyIconString: String{
        return "C"
    }
    public static var VisaNEmiratesIDIconString: String{
        return "0"
    }
    public static var WorkNEmploymentIconString: String{
        return "1"
    }
    public static var HealthNSafetyIconString: String{
        return "D"
    }
    public static var EducationNTrainingIconString: String{
        return "x"
    }
    
    // Background Color
    public static var MovingNLeavingBackgroundColorString: String{
        return "#00A1BD"
    }
    public static var HousingNPropertyBackgroundColorString: String{
        return "#F6891B"
    }
    public static var VisaNEmiratesIDBackgroundColorString: String{
        return "#7CB879"
    }
    public static var WorkNEmploymentBackgroundColorString: String{
        return "#004F66"
    }
    public static var HealthNSafetyBackgroundColorIconString: String{
        return "#EC8570"
    }
    public static var EducationNTrainingBackgroundColorIconString: String{
        return "#FDB014"
    }
}

