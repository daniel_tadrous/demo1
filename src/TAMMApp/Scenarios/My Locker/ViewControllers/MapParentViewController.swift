//
//  MapParentViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import RxSwift
import UIKit
import ArcGIS


class MapPlaceObject {
    var mapPoint:AGSPoint?
    var addressLocation:String?
}

protocol MapParentViewControllerDelegate: class {
    func locationPickerDidSelectLocation(_ : MapParentViewController,  _: LocationPickerLocation)
    func locationPickerDidCancel(_ : MapParentViewController)
    func openOfflineMap()
    func openOnlineMap()
}

class MapParentViewController: ContainerViewController , MapParentStoryboardLodable{
    static var selectedPlaceStatic :MapPlaceObject?
    
    var selectedPlace:MapPlaceObject?{
        get{
            return MapParentViewController.selectedPlaceStatic
        }
        set{
            MapParentViewController.selectedPlaceStatic = newValue
        }
    }
    weak var delegate:MapParentViewControllerDelegate?
    static var initialTextSearchStatic: String?
    public var initialTextSearch: String?{
        get{
            return MapParentViewController.initialTextSearchStatic
        }
        set{
            MapParentViewController.initialTextSearchStatic = newValue
        }
    }
    var magnifierOffset: CGPoint!
    weak var timer : Timer?
    let disposeBag = DisposeBag()
    var firstTimeEntry : Bool = true
    override func viewDidLoad() {
        
        super.viewDidLoad()
        addInternetListener()
        let img = UIImage(named: "ArcGIS.bundle/Magnifier.png")!
        self.magnifierOffset = CGPoint(x: 0, y: -img.size.height)
        // Do any additional setup after loading the view.
    }

    fileprivate func addInternetListener(){
        
        InternetCheckingService.shared.hasInternet.asObservable().subscribe(onNext: { [unowned self]
            hasInternet in
            if hasInternet != nil{
                if hasInternet!{
//                    if self.firstTimeEntry {
                        self.delegate?.openOnlineMap()
                        self.firstTimeEntry = false
//                    }else {
                       // self.startTimer()
//                    }
                    
                }
                else{
//                    if self.firstTimeEntry {
                        self.delegate?.openOfflineMap()
                        self.firstTimeEntry = false
//                    }else {
//                        self.startTimer()
//                    }
                }
                
            }
        }).disposed(by: disposeBag)
    }
    
    
    
    //method returns a graphic object for the specified point and attributes
    func graphicForPoint(_ point: AGSPoint, attributes:[String:AnyObject]?) -> AGSGraphic {
        let markerImage = UIImage.init(color: .darkBlueGrey, size: CGSize(width: 20, height: 20 ))! // #imageLiteral(resourceName: "About") // UIImage(named: "RedMarker")!
        let symbol = AGSPictureMarkerSymbol(image: markerImage)
        symbol.leaderOffsetY = 0 // markerImage.size.height/2
        symbol.offsetY = 0 //markerImage.size.height/2
        let graphic = AGSGraphic(geometry: point, symbol: symbol, attributes: attributes)
        return graphic
    }
    
    func updateSelectedlocation(adress : String? , mapPoint:AGSPoint) {
        if selectedPlace == nil {
            selectedPlace = MapPlaceObject()
        }
        selectedPlace?.addressLocation = adress
        selectedPlace?.mapPoint = mapPoint
    }
    
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 3.0,
                                     target: self,
                                     selector: #selector(isLocarionRightRequest(timer:)),
                                     userInfo: nil,
                                     repeats: false)
    }
    
    // Timer expects @objc selector
    @objc func isLocarionRightRequest(timer: Timer!) {
        timer.invalidate()
        if InternetCheckingService.shared.hasInternet.value!{
                self.delegate?.openOnlineMap()
            }
            else{
                self.delegate?.openOfflineMap()
            }
    }
    
}
