//
//  AolServiceFaqCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AolServiceFaqCell: UITableViewCell {
    
    @IBOutlet weak var sec0TitleLabel: LocalizedLabel?
    @IBOutlet weak var sec0DescLabel: LocalizedLabel?
    @IBOutlet weak var sec0AlertBtn: UIButton?
    @IBOutlet weak var sec0ShareBtn: UIButton?
    @IBOutlet weak var sec0Icon: UICircularImage?
    @IBOutlet weak var sec0ContainerView: UIView?
    @IBOutlet weak var sec1ProcessBtn: UIButton?
    
    @IBOutlet weak var sec2CollapseBtn: UIButton?
    @IBOutlet weak var sec2TitleLabel: UILabel?
    @IBOutlet weak var sec2DescLabel: UILabel?
    @IBOutlet weak var sec2ContainerView: UIView?
    
    var OnAlertBtnClick: (()->Void)?
    var OnShareBtnClick: (()->Void)?
    var OnCollapseBtnClick: (()->Void)?
    var OnApplyBtnClick: (()->Void)?
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.sec0ContainerView?.applyRoundCorners()
        self.sec2ContainerView?.applyRoundCorners()
        self.sec0ShareBtn?.setTitle("\u{e03c}", for: .normal)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func AlertBtnClickHandler(_ sender: UIButton) {
        OnAlertBtnClick?()
    }
    
    @IBAction func ShareBtnClickHandler(_ sender: UIButton) {
        OnShareBtnClick?()
    }
    @IBAction func CollapseBtnClickHandler(_ sender: UIButton) {
        OnCollapseBtnClick?()
    }
    
    @IBAction func ApplyBtnClickHandler(_ sender: UIButton) {
        OnApplyBtnClick?()
    }
   
}
