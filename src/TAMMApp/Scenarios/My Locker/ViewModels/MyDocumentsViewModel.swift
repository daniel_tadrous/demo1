//
//  MyDocumentsViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import EventKit

class MyDocumentsViewModel {
    private var apiService:MyDocumentServiceType!
    private(set) var documents: Variable<DocumentsModel?> = Variable(nil)
    private(set) var searchedDocuments: Variable<DocumentsModel?> = Variable(nil)
    fileprivate var disposeBag = DisposeBag()
    var pageCount:Variable<Int> = Variable(0)
    var documentType:DocumentType?
    var detailedDocument:Variable<MyDocumentModel?> = Variable(nil)
    var documentsCallObject = DoumentsApiCallModel()
    
    //speach recognition
    var speechDoneSubject: PublishSubject<String?>?
    var speechRecognitionService: CapturedSpeechToTextService = CapturedSpeechToTextService()
    let speechDuration:Double = 10
    
    init(apiService:MyDocumentServiceType) {
        self.apiService = apiService
        setupBinding()
        
    }
    //speech recognition
    
    func setupBinding() {
        pageCount.asObservable().subscribe(onNext: { (count) in
            if let type = self.documentType{
                if type == .Active{
                    self.documentsCallObject.page = count
                    self.getActiveDocuments()
                }else{
                    self.documentsCallObject.page = count
                    self.getExpiredDocuments()
                }
            }
        })
        
    }
    
    func getActiveDocuments(){
        apiService.getActiveDocuments(callObject: documentsCallObject).asObservable().subscribe(onNext: { (docs) in
            print("\(docs)")
            if self.documents.value == nil{
                self.documents.value = docs
            }
            else{
                self.documents.value?.documents.append(contentsOf: docs.documents)
                let docsRes = self.documents.value
                self.documents.value = nil
                self.documents.value = docsRes
            }
        }).disposed(by: disposeBag)
    }
    
    func getExpiredDocuments(){
        apiService.getExpiredDocuments(callObject: documentsCallObject).asObservable().subscribe(onNext: { (docs) in
            print("\(docs)")
            if self.documents.value == nil{
                self.documents.value = docs
            }
            else{
                self.documents.value?.documents.append(contentsOf: docs.documents)
                let docsRes = self.documents.value
                self.documents.value = nil
                self.documents.value = docsRes
            }
        }).disposed(by: disposeBag)
    }
    
    func searchExpiredDocuments(callObject:DoumentsApiCallModel){
        apiService.getExpiredDocuments(callObject: callObject).asObservable().subscribe(onNext: { (docs) in
            print("\(docs)")
            if self.searchedDocuments.value == nil{
                self.searchedDocuments.value = docs
            }
            else{
                for doc in docs.documents{
                    var found = false
                    for sDoc in (self.searchedDocuments.value?.documents)!{
                        if sDoc.Id == doc.Id{
                            found = true
                        }
                    }
                    if !found{
                        self.searchedDocuments.value?.documents.append(doc)
                    }
                }
            }
            let docsRes = self.searchedDocuments.value
            self.searchedDocuments.value = nil
            self.searchedDocuments.value = docsRes
        }).disposed(by: disposeBag)
    }
    
    func searchActiveDocuments(callObject:DoumentsApiCallModel){
        apiService.getActiveDocuments(callObject: callObject).asObservable().subscribe(onNext: { (docs) in
            print("\(docs)")
            if self.searchedDocuments.value == nil{
                self.searchedDocuments.value = docs
            }
            else{
                for doc in docs.documents{
                    var found = false
                    for sDoc in (self.searchedDocuments.value?.documents)!{
                        if sDoc.Id == doc.Id{
                            found = true
                        }
                    }
                    if !found{
                        self.searchedDocuments.value?.documents.append(doc)
                    }
                }
                self.searchedDocuments.value = nil
            }
            let docsRes = self.searchedDocuments.value
            self.searchedDocuments.value = nil
            self.searchedDocuments.value = docsRes
        }).disposed(by: disposeBag)
    }
    
    func getDocument(id:Int) {
        apiService.getDocument(id: id).asObservable().subscribe(onNext: { (doc) in
            self.detailedDocument.value = doc
        }).disposed(by: disposeBag)
    }
    
    func updateDocument(document:MyDocumentModel) {
        apiService.updateDocument(document: document)
    }
    
    func shareDocument(id:Int,to:String,subject:String,closure:@escaping ()->Void) {
        apiService.shareDocument(documentID: id, subject: subject, to: to).asObservable().subscribe(onNext: { (res) in
            closure()
        })
    }
}

extension MyDocumentsViewModel: SpeechRecognitionServiceTypeDelegate{
    
    func textRecived(text: String) {
        //        micRecognizedVar.value = text
        speechDoneSubject?.onNext(text)
    }
    
    
    func isServiceAvailable() -> Bool{
        return CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable()
    }
    
    
    func recognitionIsDone(_ error: Error?) {
        // should inform the ui with the status and reset the availablility
        //        micRecodingIsEnabledVar.value = true
        //        micRecordingErrorVar.value = "\(String(describing: error))"
        speechDoneSubject?.onCompleted()
    }
    
    
    
    func startSpeechService() -> Observable<String?>{
        speechRecognitionService.start(delegate: self, duration: speechDuration)
        speechDoneSubject = PublishSubject<String?>()
        //        micRecognizedVar.value = ""
        return (speechDoneSubject!)
    }
    
    
    func recognizerAvailable(isAvailable: Bool) {
        // indecates that the recognizer is available and most likely will start
        // should inform the subscribers with the change
        // disable the recording for some time
        if( isAvailable){
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onNext("")
            
        }
        else{
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onError("Service is not currently available" as! Error)
        }
    }
    
}
