//
//  TestCoordinator.swift
//  TAMMApp
//
//  Created by kerolos on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

protocol TestCoordinatorDelegate: class {
    func testCoordinatorDidFinish()
}


class TestCoordinator: NavigationCoordinator{
    
    let navigationController: UINavigationController
    let container: Container
    weak var delegate: AuthenticationCoordinatorDelegate?
    
    init(container: Container, navigationController: UINavigationController) {
        self.container = container
        self.navigationController = navigationController
    }
    
    
    func start() {
        let vc = container.resolveViewController(TestViewController.self)
        // view model is initialized by the container
        
        
        vc.viewModel.delegate = self
        vc.navigationItem.hidesBackButton = true
        navigationController.setBackButton()
        navigationController.pushViewController(vc, animated: true)
    }
    
    func test2() {
        let vc = container.resolveViewController(Test2ViewController.self)
        // view model is initialized by the container
        
        
        vc.viewModel.delegate = self
        vc.navigationItem.hidesBackButton = false
        navigationController.setBackButton()
        navigationController.pushViewController(vc, animated: true)
    }
    
    
}

extension TestCoordinator : TestViewModelDelegate{
    func testIsDone(){
        
    }
    func goToTest2Page(){
        test2()
    }
}

extension TestCoordinator : Test2ViewModelDelegate{
    func test2IsDone(){
        self.navigationController.popViewController(animated: true)
    }

}
