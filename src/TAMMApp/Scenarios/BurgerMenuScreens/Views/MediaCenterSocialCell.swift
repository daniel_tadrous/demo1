//
//  MediaCenterSocialCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import WebKit

class MediaCenterSocialCell: UITableViewCell {

//    @IBOutlet weak var webView1: UIWebView!
//    @IBOutlet weak var webView2: UIWebView!
    @IBOutlet weak var webView1: UILabel!
    @IBOutlet weak var webView2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func commonInit() {
        selectionStyle = .none
        webView1.addRoundCorners(radious: 17)
        webView2.addRoundCorners(radious: 17)
    }
}

