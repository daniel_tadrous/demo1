//
//  MyPaymentsViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MyPaymentsParentDelegate {
    func openUnpaid()
    func openPaid()
}


class MyPaymentsParentViewController: ContainerViewController , MyPaymentsParentStoryboardLoadable {

    @IBOutlet weak var pendingButton: LocalizedSelectableButton!
    
    @IBOutlet weak var paidButton: LocalizedSelectableButton!
    
    @IBOutlet weak var fullLineView: UIView!
    
    @IBOutlet weak var underLineView: UIView!
    
    var delegate:MyPaymentsParentDelegate?
    
    fileprivate var pendingSelected = false
    fileprivate var paidSelected = false
    fileprivate var underLineColor = UIColor(red: 0, green: 89, blue: 113, alpha: 1)
    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    fileprivate var selectedFontColor = UIColor(hexString: "005971")
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138")
    
    @IBAction func pendingBtnAction(_ sender: LocalizedSelectableButton) {
        selectButtonAction(forButton: sender, isMedia: true)
    }
    
    
    @IBAction func paidBtnAction(_ sender: LocalizedSelectableButton) {
        selectButtonAction(forButton: sender, isMedia: false)
    }
    
    fileprivate func setupFont() {
        if L10n.Lang == "ar" {
            selectedFont = UIFont(name: "Swissra-Bold",size: 18)
            unSelectedFont = UIFont(name: "Swissra-Normal",size: 18)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont()
        
        pendingButton.RTLSelecttedFont = "Swissra-Bold"
        paidButton.RTLSelecttedFont = "Swissra-Bold"
        
        
        pendingButton.originalFont = unSelectedFont
        pendingButton.fontProxy = unSelectedFont
        pendingButton.selectedFontReference = selectedFont
        pendingButton.originalColor = unSelectedFontColor
        pendingButton.selectedColor = selectedFontColor
        
        paidButton.originalFont = unSelectedFont
        paidButton.fontProxy = unSelectedFont
        paidButton.selectedFontReference = selectedFont
        paidButton.originalColor = unSelectedFontColor
        paidButton.selectedColor = selectedFontColor
        
        selectButtonAction(forButton: pendingButton, isMedia: true)
        
        setNavigationBar(title: L10n.payments, willSetSearchButton: true)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let myselectedFontColor = UIColor(hexString: "005971").getAdjustedColor()
        let myunSelectedFontColor = UIColor(hexString: "161138").getAdjustedColor()
        pendingButton.selectedColor = myselectedFontColor
        paidButton.selectedColor = myselectedFontColor
        pendingButton.originalColor = myunSelectedFontColor
        paidButton.originalColor = myunSelectedFontColor
        if pendingSelected {
            pendingButton.setTitleColor(myselectedFontColor, for: .normal)
            paidButton.setTitleColor(myunSelectedFontColor, for: .normal)
        }else {
            pendingButton.setTitleColor(myunSelectedFontColor, for: .normal)
            paidButton.setTitleColor(myselectedFontColor, for: .normal)
        }
        
        
         self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        fullLineView.backgroundColor = #colorLiteral(red: 0.8506266475, green: 0.8481912017, blue: 0.8733561635, alpha: 1).getAdjustedColor()
        underLineView.backgroundColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
    }
    
    
    fileprivate func selectButtonAction(forButton button:LocalizedSelectableButton, isMedia:Bool ) {
        pendingSelected = false
        paidSelected = false
        pendingButton.isButtonSelected = false
        paidButton.isButtonSelected = false
        
        if isMedia, !pendingSelected{
            pendingSelected = true
            //moveUnderLine(to: button)
            button.isButtonSelected = true
            delegate?.openUnpaid()
        }
        else if !isMedia, !paidSelected{
            paidSelected = true
            //moveUnderLine(to: button)
            button.isButtonSelected = true
            // goto public holidays
            delegate?.openPaid()
        }
        
    }
    
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        if button == pendingButton {
            underLineView.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
            underLineView.frame.origin.x = fullLineView.frame.origin.x
        }
            
            
        else if button == paidButton{
            underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }
        
    }
    
    
    override func viewDidLayoutSubviews() {
        if pendingSelected {
            if L10n.Lang == "en" {
                underLineView.frame.origin.x = fullLineView.frame.origin.x
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }else {
                underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
            
        else if paidSelected{
            if L10n.Lang == "en" {
                underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }else {
                underLineView.frame.origin.x = fullLineView.frame.origin.x
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            
        }
    }
    
    
    
    

}
