//
//  UIViewController+Extensions.swift
//  iOSSampleApp
//
//  Created by Igor Kulman on 16/11/2017.
//  Copyright © 2017 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
//import CRToast


extension UIViewController {
    func displayFloatingMenu(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(!appDelegate.floatingMenuIsVisible() && InternetCheckingService.shared.hasInternet.value!){
            appDelegate.addfloatingMenu()
        }
    }
    func hideFloatingMenu(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.floatingMenuIsVisible()){
            appDelegate.removeFloatingMenu()
        }
    }
    func enableDisableFloatingMenu(isEnabled: Bool){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.enableDisableFloatingMenu(isEnabled: isEnabled)
    }
    func removeSideMenu(){
        SideMenuManager.default.menuLeftNavigationController = nil
        SideMenuManager.default.menuRightNavigationController = nil
    }
    
}
