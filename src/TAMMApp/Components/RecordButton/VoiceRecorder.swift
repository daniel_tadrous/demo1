//
//  VoiceRecorder.swift
//  AVFoundation Recorder
//
//  Created by kerolos on 6/26/18.
//  Copyright © 2018 Gene De Lisa. All rights reserved.
//

import Foundation
import AVFoundation
import Photos


protocol VoiceRecorderDelegate: class {
    func voiceRecorderDidFinishRecording(url: URL?, success: Bool )
}

class VoiceRecorder: NSObject{
    
    weak var delegate: VoiceRecorderDelegate?
    
    private var recorder: AVAudioRecorder!
    
//    private var meterTimer: Timer!
    
    private var soundFileURL: URL!
    
    public override init(){
        
    }
    
    public func requestPermissions() -> Bool{
        return shouldStartRecordingElseRequestPermissions()
    }
    
    public func record(){
        record1()
    }
    
    public func stop(){
        recorder?.stop()
        
        //        meterTimer.invalidate()
        
    }
    
    
    fileprivate func deactivateSession() {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(false)
        } catch {
            print("could not make session inactive")
            print(error.localizedDescription)
        }
    }
    
    
//    @objc func updateAudioMeter(_ timer: Timer) {
//        
//        if let recorder = self.recorder {
//            if recorder.isRecording {
//                let min = Int(recorder.currentTime / 60)
//                let sec = Int(recorder.currentTime.truncatingRemainder(dividingBy: 60))
//                let s = String(format: "%02d:%02d", min, sec)
//                recorder.updateMeters()
//                // if you want to draw some graphics...
//                //var apc0 = recorder.averagePowerForChannel(0)
//                //var peak0 = recorder.peakPowerForChannel(0)
//            }
//        }
//    }
    
    private func shouldStartRecordingElseRequestPermissions() -> Bool{
        
        var shouldRecord1 = true
        
        switch AVAudioSession.sharedInstance().recordPermission() {
        case .denied:
            print("permission already denied by the user")
            shouldRecord1 = false
            micServiceNotPermitted()
        case .granted:
            print("permission already granted by the user")
            shouldRecord1 =  true
        case .undetermined:
            print("permission will be requested from the user")
            AVAudioSession.sharedInstance().requestRecordPermission { (granted) in
                if (granted) {
                    print("requestRecordPermission Permission granted");
                }
                else {
                    print("requestRecordPermission Permission denied");
                }
            }
            shouldRecord1 = false
        default:
            print("UNHANDELED CASE SHOULD VISIT RecordButton AND FIX THIS")
            shouldRecord1 = false
        }
        
        var shouldRecord2 = true
        
//        switch PHPhotoLibrary.authorizationStatus() {
//        case .authorized:
//            // The user has previously granted access to the photo library.
//            shouldRecord2 = true
//
//        case .notDetermined:
//            // The user has not yet been presented with the option to grant photo library access so request access.
//            PHPhotoLibrary.requestAuthorization({ status in
//                print("Assets permission granted. = \(status)")
//            })
//            shouldRecord2 = false
//
//        case .denied:
//            // The user has previously denied access.
//            shouldRecord2 = false
//        case .restricted:
//            // The user doesn't have the authority to request access e.g. parental restriction.
//            shouldRecord2 = false
//        default:
//            shouldRecord2 = false
//        }
        
        return shouldRecord1 && shouldRecord2
    }
    
    private func record1() {
        
        print("\(#function)")

        if recorder == nil {
            print("recording. recorder nil")
            recordWithPermission(true)
            return
        }
       
    }
    
    fileprivate func micServiceNotPermitted() {
        DispatchQueue.main.async {
            let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.micPermissionDenied, onComplete: {
            })
            UIApplication.shared.keyWindow!.addSubview(permessionView)
            UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
            
        }
    }

    
    private func setupRecorder() {
        print("\(#function)")
        
        let format = DateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        let currentFileName = "recording-\(format.string(from: Date())).m4a"
        print(currentFileName)
        
       // let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let documentsDirectory =  IncidentUrl.getDirectoryForIncident()
        self.soundFileURL = documentsDirectory.appendingPathComponent(currentFileName)
        print("writing to soundfile url: '\(soundFileURL!)'")
        
        if FileManager.default.fileExists(atPath: soundFileURL.absoluteString) {
            // probably won't happen. want to do something about it?
            print("soundfile \(soundFileURL.absoluteString) exists")
        }
        
        let recordSettings: [String: Any] = [
            AVFormatIDKey: kAudioFormatAppleLossless,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
            AVEncoderBitRateKey: 32000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey: 44100.0
        ]
        
        
        do {
            recorder = try AVAudioRecorder(url: soundFileURL, settings: recordSettings)
            recorder.delegate = self
            recorder.isMeteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
        } catch {
            recorder = nil
            print(error.localizedDescription)
        }
        
    }
    
    func recordWithPermission(_ setup: Bool) {
        print("\(#function)")
        
        AVAudioSession.sharedInstance().requestRecordPermission {
            [unowned self] granted in
            if granted {
                
                DispatchQueue.main.async {
                    print("Permission to record granted")
                    self.setSessionPlayAndRecord()
                    if setup {
                        self.setupRecorder()
                    }
                    self.recorder.record()
                    
//                    self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1,
//                                                           target: self,
//                                                           selector: #selector(self.updateAudioMeter(_:)),
//                                                           userInfo: nil,
//                                                           repeats: true)
                }
            } else {
                print("Permission to record not granted")
            }
        }
        
        if AVAudioSession.sharedInstance().recordPermission() == .denied {
            print("permission denied")
            micServiceNotPermitted()
        }
    }
    

    func setSessionPlayAndRecord() {
        print("\(#function)")
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
}



// MARK: AVAudioRecorderDelegate
extension VoiceRecorder: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder,
                                         successfully flag: Bool) {
        
        print("\(#function)")
        
        print("finished recording \(flag)")
        deactivateSession()

        delegate?.voiceRecorderDidFinishRecording(url: flag ? recorder.url : nil,  success: flag)
        // iOS8 and later
//        let alert = UIAlertController(title: "Recorder",
//                                      message: "Finished Recording",
//                                      preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Keep", style: .default) {[unowned self] _ in
//            print("keep was tapped")
//            self.recorder = nil
//        })
//        alert.addAction(UIAlertAction(title: "Delete", style: .default) {[unowned self] _ in
//            print("delete was tapped")
//            self.recorder.deleteRecording()
//        })
//
//        self.present(alert, animated: true, completion: nil)
        
        print("keep recording")
        self.recorder = nil
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder,
                                          error: Error?) {
        print("\(#function)")
        deactivateSession()

        if let e = error {
            delegate?.voiceRecorderDidFinishRecording(url: nil, success: false)
            print("\(e.localizedDescription)")
        }
    }
    
}


