//
//  PrefrencesViewModel.swift
//  TAMMApp
//
//  Created by kero1 on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation


enum PreferencesType{
    case navigationType
    case switchType
    case selectionType
}
enum PreferencesItem: String{
    case profile = "Profile"
    case language = "Language"
    case notifications = "Notifications"
    case mediaSettings = "Media Settings"
    case enableLocationServices = "Enable Location Services"
    case syncCalendar = "Sync Calendar"
    case about = "About"
    case tellAFriend = "Tell a Friend"
    case appFeatures = "App features"
    case accessibility = "Accessibility"
    case downloadOfflineMaps = "Download offline Maps"
    
}
enum PreferencesLanguageSelection: String{
    case english = "English"
    case arabic = "Arabic"
}

class PreferencesViewModel{
    
    
    let userConfigService: UserConfigServiceType

    init(userConfigService: UserConfigServiceType){
        self.userConfigService = userConfigService
    }
    
    struct PreferencesViewData {
        var type: PreferencesType
        var item: PreferencesItem
    }
    
    
    
    public private(set) var items: [[PreferencesViewModel.PreferencesViewData]] = [[
        PreferencesViewData(type: .navigationType, item: .profile),
        PreferencesViewData(type: .selectionType , item: .language),],[
        PreferencesViewData(type: .navigationType, item: .notifications),
        PreferencesViewData(type: .navigationType, item: .mediaSettings),
        PreferencesViewData(type: .switchType, item: .enableLocationServices),
        PreferencesViewData(type: .switchType, item: .syncCalendar),PreferencesViewData(type: .navigationType, item: .downloadOfflineMaps),],[
        PreferencesViewData(type: .navigationType, item: .about),
        PreferencesViewData(type: .navigationType, item: .tellAFriend),
        PreferencesViewData(type: .navigationType, item: .appFeatures),],[
        PreferencesViewData(type: .navigationType, item: .accessibility),
        
    ]]
    
    public func getLanguageLitral() -> String{
        return userConfigService.getCurrentLanguageLitral()
    }
    
    public func getCalenderSyncStatus() -> Bool{
        return userConfigService.getCalanderSyncStatus()
    }
    
    public func getLocationServiceStatus() -> Bool{
        return userConfigService.getLocationServicesStatus()
    }
    
    public func setCalanderSyncStatus(isOn:Bool) {
        print(isOn)
        userConfigService.setCalanderSyncStatus(isOn:isOn)
    }
    
    

    
    
   
    
}

