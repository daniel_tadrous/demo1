//
//  UserJourny.swift
//  TAMMApp
//
//  Created by kerolos on 4/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class UserJourneyModel: Mappable{
    var id: Int?
    var userId: Int?
    var journey: JourneyModel?    
    var completionPercentage: Int?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id                      <- map["id"]
        userId                  <- map["userId"]
        journey                 <- map["journey"]
        completionPercentage    <- map["completionPercentage"]
        
    }
}
