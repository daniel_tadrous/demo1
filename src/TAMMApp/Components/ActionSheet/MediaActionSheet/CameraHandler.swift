//
//  ViewController.swift
//  cameraTest
//
//  Created by kerolos on 5/2/18.
//  Copyright © 2018 kerolos. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices
import AssetsLibrary
import Photos

protocol CameraHandlerDelegate: class {
    func videoIsSelected(url: URL)
    func imageIsSelected(url: URL)
}
// this should be wraped in a service
class CameraHandler: NSObject{
    
    
    static let shared = CameraHandler()
    
    weak var delegate: CameraHandlerDelegate?
    fileprivate var currentVC: UIViewController!
    
    //MARK: Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    
    @objc func camera()
    {
        actionSheetPresented?.dismiss(cancel:false)

        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            
            
            myPickerController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
//            imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
            myPickerController.videoMaximumDuration = 10.0
            
            
//            myPickerController.cameraOverlayView
            currentVC.present(myPickerController, animated: true, completion: nil)
            
            
            
        }
        
    }
    
    @objc func photoLibrary()
    {
        actionSheetPresented?.dismiss(cancel:false)

        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            currentVC.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func showActionSheet(vc: UIViewController) {
        //currentVC = vc

        //self.actionSheetPresented = actionSheet
    }
    
    weak var actionSheetPresented: CustomizableActionSheet?
    
    
    @objc func dismissActionSheetPresented(){
        actionSheetPresented?.dismiss(cancel:true)
    }
    
    
}


extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
       
        let mediaType:AnyObject? = info[UIImagePickerControllerMediaType] as AnyObject

        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imagePickedBlock?(image)
        }else if let type:AnyObject = mediaType {
            if type is String {
                let stringType = type as! String
                if stringType == kUTTypeMovie as String {
                    let urlOfVideo = info[UIImagePickerControllerMediaURL] as? URL
                    if let url = urlOfVideo {
                        
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url as URL)
                        }) { saved, error in
                            if saved {
                                print("no errors happened")
                            } else{
                                print("Error saving video = \(error)")
                            }
                        }
                    }
                }
            }
        }

        
        else{
            print("Something went wrong")
        }
        currentVC.dismiss(animated: true, completion: nil)
    }
    
}


