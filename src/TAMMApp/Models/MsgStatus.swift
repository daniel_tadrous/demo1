//
//  ActiveMsgModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class MsgStatus:Mappable {
    var id:Int?
    var name:String?
    
    var value: CaseDetailEnum{
        
        get{
            return CaseDetailEnum.fromString(rawValue:CaseDetailEnum(rawValue: id ?? 0)!.toString() )
        }
    }
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        
    }
}












