//
//  ActiveDocumentsViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class ActiveDocumentsViewController: TammViewController,ActiveDocumentsStoryboardLoadable {
    
    @IBOutlet weak var documentsTableView: UITableView!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var noDocumentsView: UIView!
    @IBOutlet weak var youDontLabel: LocalizedLabel!
    @IBOutlet weak var atTheMomentLabel: LocalizedLabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchField: LocalizedTextField!
    
    private var disposeBag = DisposeBag()
    var parentV:MyDocumentViewController!
    
    var pageCount:Int{
        get{
            return viewModel.pageCount.value
        }set{
            viewModel.pageCount.value = newValue
        }
    }
    var documents:[MyDocumentModel]{
        get{
            return viewModel.documents.value?.documents ?? []
        }
    }
    var numberOfPages:Int{
        get{
            return viewModel.documents.value?.pageCount ?? 0
        }
    }
    
    
    
    let showMoreCellIdentifier:String = "showmoreCell"
    let cellIdentifier = "DocumentCell"
    
    
    
    var viewModel:MyDocumentsViewModel!
    var documentsType:DocumentType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        documentsTableView.dataSource = self
        documentsTableView.delegate = self
        view.backgroundColor = UIColor.paleGreyTwo
        
        documentsTableView.register(UINib(nibName: "ShowMoreCell", bundle: nil), forCellReuseIdentifier: showMoreCellIdentifier)
        documentsTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClicked)))
        
    }
    
    func setupHooks(){
        viewModel.documentType = documentsType
        viewModel.documents.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] docRes in
                if(docRes == nil || docRes?.documents.count == 0){
                    // to add something here
                }else{
                    self.noDocumentsView.isHidden = true
                    print(docRes!.documents.count)
                    self.documentsTableView.reloadData()
                    print(self.documents)
                }
            })
            .disposed(by: disposeBag)
    }
    
    @objc func searchClicked(){
        
        let searchOverlay = DocumentsSearchOverlay(frame: UIScreen.main.bounds,model:viewModel,type:documentsType)
        searchOverlay.searchingClosure = finishedSearching(_:_:)
        UIApplication.shared.keyWindow!.addSubview(searchOverlay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchOverlay)
    }
    
    func finishedSearching(_ docs:DocumentsModel,_ text:String){
        viewModel.documents.value = docs
        searchField.text = text
    }
    
}
extension ActiveDocumentsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pageCount < numberOfPages-1 {
            return documents.count + 1
        }
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < documents.count{
            var cell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! DocumentCell
            let document = documents[indexPath.row]
            
            cell.typeLabel.text = document.type
            cell.titleLabel.text = document.name
            cell.expiryDateLabel.text = document.expidationDate?.getDate(format: "yyyy-MM-dd'T'HH:mm:ss.SSS")?.getFormattedString(format: "dd MMM, YYYY")
            if documentsType == .Active{
                cell.expiredOnLabel.text = L10n.MyDocsStrings.expiresOn
                cell.okClosure = {(nickname) in
                    cell.titleLabel.text = nickname
                    document.name = nickname
                    self.viewModel.updateDocument(document: document)
                }
            }else{
                cell.setAsExpiredCell()
            }
            cell.idLabel.text = document.idNumber
            cell.VerticalStackView.arrangedSubviews.map { (v) -> UIView in
                v.isHidden = true
                cell.VerticalStackView.removeArrangedSubview(v)
                return v
            }
            if let count = document.fields?.count{
                for i in 0..<count{
                    cell.addRow(title: document.fields![i].key!, details: document.fields![i].value!)
                }
            }
            ImageCachingUtil.loadImage(url: URL(string: (document.icon)!)!, dispatchInMain: true
                , callBack: { (url, image) in
                    cell.logoImage?.image = image
            })
            return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: showMoreCellIdentifier) as! ShowMoreCell
            
            cell.showMoreBtn.addTarget(self, action: #selector(showMorePressed), for: .touchUpInside)
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let id = documents[indexPath.row].Id{
            let detailedOverLay = DocumentsDetails(frame: UIScreen.main.bounds,model:viewModel,type:documentsType,id:id)
            UIApplication.shared.keyWindow!.addSubview(detailedOverLay)
            UIApplication.shared.keyWindow!.bringSubview(toFront: detailedOverLay)
            viewModel.getDocument(id: id)
        }
        
    }
    @objc func showMorePressed() {
        pageCount = pageCount + 1
    }
}
