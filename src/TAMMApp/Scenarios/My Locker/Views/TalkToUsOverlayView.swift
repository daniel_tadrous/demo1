//
//  TalkToUsOverlayView.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
protocol TalkToUsOverlayViewDelegate {
    func suggestionIsSelected(message:TalkToUsItemViewDataType)
    func suggestionIsSelected(message:String)
    func searchIsDone(text:String)
    func typeIsSelected(typeId:Int)
    func getMessageTypes()->Observable<MessageTypesViewDataType?>
}

class TalkToUsOverlayView: UIView , UITextFieldDelegate {
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTxt: LocalizedTextField!
    
    @IBOutlet weak var searchResultsTableView: UITableView!
    @IBOutlet weak var micBtn: UIButton!

    @IBOutlet var view: UIView!
    let footerViewIdentifier = "footer_view"
    let defaultReuseIdentifier = "cell"
    
    private let debounceTimeInSec: Double = 0.5
    var items : [QueryResultItemItemViewDataType]?
    public var delegate:TalkToUsOverlayViewDelegate!
    public let disposeBag = DisposeBag()
    let service = ApiMessageServices()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
   
    private func commonInit(){
        Bundle.main.loadNibNamed("TalkToUsOverlayView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
         self.searchResultsTableView.register(UINib(nibName: "TalkToUsOverLayFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: footerViewIdentifier)
         self.searchResultsTableView.register(UINib(nibName: "SearchResultCell", bundle: nil), forCellReuseIdentifier: defaultReuseIdentifier)
        self.searchResultsTableView.delegate = self
        self.searchResultsTableView.dataSource = self
        self.searchTxt.delegate = self
        
        
        //tab outside
//        let tapGestureResigne = UITapGestureRecognizer()
//        self.isUserInteractionEnabled = true
//        self.addGestureRecognizer(tapGestureResigne)
//        
//        tapGestureResigne.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: {
//            [unowned self ] recognizer in
//            self.endEditing(true)
//            
//        }).disposed(by: disposeBag)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.searchTxt.delegate = self
        backBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        searchTxt.attributedPlaceholder = NSAttributedString(string: L10n.searchEllipsized, attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 155/255, green: 155/255, blue: 155/255, alpha: 1) , NSAttributedStringKey.font : UIFont.init(name: "CircularStd-Book", size: 16)!])
        searchTxt.becomeFirstResponder()
    }
    
    @IBAction func micBtnClicked(_ sender: Any) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        delegate?.suggestionIsSelected(message: textField.text!)
        
        return true
    }
    
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        print("textFieldShouldReturn")
//        //delegate?.suggestionIsSelected(message: )
//        //delegate?.searchIsDone(text: self.searchTxt.text!)
//       // delegate?.suggestionIsSelected(message: self.searchTxt.text!)
//        delegate?.searchIsDone(text: textField.text!)
//        return true
//    }
    @IBAction func backBtnClicked(_ sender: Any) {
//        self.removeFromSuperview()
        print("textFieldShouldReturn")
        delegate?.searchIsDone(text: self.searchTxt.text!)
    }
    
    
    func itemIsSelected(message: TalkToUsItemViewDataType) {
        delegate.suggestionIsSelected(message: message)
    }
    
   
}
extension TalkToUsOverlayView: TalkToUsOverLayFooterViewDelegate{
    func OnSubmit(messageTypeId: Int) {
        delegate?.typeIsSelected(typeId: messageTypeId)
    }
}
extension TalkToUsOverlayView: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(500)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: footerViewIdentifier) as! TalkToUsOverLayFooterView
        view.delegate = self
        view.fillPickerView(messagesTypes: delegate.getMessageTypes())
        return view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifier)
        let l1 = cell?.viewWithTag(1) as! UILabel
        l1.text = item.name
        let l2 = cell?.viewWithTag(2) as! UILabel
        l2.text = item.name
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(78)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items![indexPath.row]
        self.searchTxt.text = item.name
    }
}
