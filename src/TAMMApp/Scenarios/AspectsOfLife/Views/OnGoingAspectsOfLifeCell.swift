//
//  OnGoingAspectsOfLifeCell.swift
//  TammUI
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit
class OnGoingAspectsOfLifeCell: UITableViewCell {
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var headerText: LocalizedLabel!
    @IBOutlet weak var icon: UICircularImage!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var completenceText: LocalizedLabel!
    var viewData: Any?
    private var currentProgress: Float?{
        didSet{
            do{
                setProgressPrivate(value: currentProgress!)
            }
            catch let error{
               print(error)
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        commonInit()
    }
    
    override func awakeFromNib() {
        commonInit()
    }
    
    private func commonInit(){
//        Bundle.main.loadNibNamed("OnGoingAspectsOfLifeCellView", owner: self, options: nil)
//        addSubview(view)
//        view.frame = self.bounds
//        UIView.setFillingConstraints(view: self, childView: view)
//        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.backgroundColor = UIColor.clear
        self.icon.FontImageColor = .white
        //self.headerText.adjustsFontSizeToFitWidth = true
        self.viewContent.layer.shadowColor = UIColor.lightGray.cgColor;
        self.viewContent.layer.shadowOpacity=0.5;
        self.viewContent.layer.shadowRadius=2.0;
        self.viewContent.layer.shadowOffset = CGSize(width:0, height:0);
        self.viewContent.layer.masksToBounds = false;
        self.viewContent.addRoundCorners(radious: Dimensions.journyCellCornerRadious)
    }
    public func setProgress(value:Float){
        currentProgress = value
//        setProgressPrivate(value: value)
    }
    
    private func setProgressPrivate(value:Float){
        progressView.progress = value / 100
//        let fontScale = UIFont.fontSizeMultiplier
        let fontSize18: CGFloat = 18 //* fontScale
        let fontSize14: CGFloat = 14 //* fontScale
        let fontSize11: CGFloat = 10 //* fontScale
        
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        if isRTL {
            var mystring = NSMutableAttributedString(string: String(format:" %.0f", value) , attributes: [NSAttributedStringKey.font:UIFont.init(name: "Swissra-Bold", size: fontSize18)!])
        mystring.append(NSAttributedString(string: "%", attributes: [NSAttributedStringKey.font:UIFont.init(name: "CircularStd-Bold", size: fontSize14)!]))
        
        let part2 = NSMutableAttributedString(string: " \(L10n.complete)", attributes: [NSAttributedStringKey.font:UIFont.init(name: "Swissra-Bold", size: fontSize11)!])
            part2.append(mystring)
            
            completenceText.attributedText = part2

        }
        else{
            let mystring = NSMutableAttributedString(string: String(format:"%.0f", value) , attributes: [NSAttributedStringKey.font:UIFont.init(name: "CircularStd-Bold", size: fontSize18)!])
            mystring.append(NSAttributedString(string: "%", attributes: [NSAttributedStringKey.font:UIFont.init(name: "CircularStd-Bold", size: fontSize14)!]))
            
            mystring.append(NSAttributedString(string: " \(L10n.complete)", attributes: [NSAttributedStringKey.font:UIFont.init(name: "CircularStd-Medium", size: fontSize11 )!]))
            completenceText.attributedText = mystring

        }
        
        progressView.subviews.forEach { subview in
            subview.layer.masksToBounds = true
            subview.layer.cornerRadius = 4
        }
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setProgressPrivate(value: currentProgress!)
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard  let currentProgress = self.currentProgress else {
            return
        }
        setProgressPrivate(value: currentProgress)
    }
    
    
}
