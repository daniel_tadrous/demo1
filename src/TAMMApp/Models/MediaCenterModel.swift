//
//  MediaCenterModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

import ObjectMapper

class MediaCenterModel:Mappable{
    
    var news:[News]?
    var events:[Event]?
    var social:[String]?
    var pressReleases:[PressRelease]?
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        news <- map["news"]
        events <- map["events"]
        social <- map["social"]
        pressReleases <- map["pressReleases"]
        
    }
    
}

class PressRelease: Mappable {
    var id:Int?
    var title:String?
    var date:String?
    var excerpt:String?
    var url:String?
    var size:String?
    var isDownloading = false
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        date <- map["date"]
        excerpt <- map["excerpt"]
        url <- map["url"]
        size <- map["size"]
    }
    
    func getFormattedDate()->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let outputFormat = "MMM dd yyyy"
        
        var res = ""
        let formattedDate = self.date?.getDate(format: inputFormat)
        res = "\(formattedDate?.getFormattedString(format: outputFormat) ?? "")"
        
        return res
    }
}

class News: Mappable {
    var id:Int?
    var title:String?
    var category:String?
    var date:String?
    var image:String?
    var url:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        category <- map["category"]
        date <- map["date"]
        image <- map["image"]
        url <- map["url"]
    }
    func getFormattedDate()->String{
        
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let outputFormat = "dd MMMM yyyy"
        
        var res = ""
        let formattedDate = self.date?.getDate(format: inputFormat)
        res = "\(formattedDate?.getFormattedString(format: outputFormat) ?? "")"
        
        return res
    }
}
