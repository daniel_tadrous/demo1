//
//  TalkToUsOverlayView.swift
//  TAMMApp
//
//  Created by Daniel on 4/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
protocol EventsSearchOverlayViewDelegate {
    func eventIsSelected(message:TalkToUsItemViewDataType)
    func backPressed()
}
class EventsSearchOverlayView: UIView, UITammStackViewDelegate {
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTxt: LocalizedTextField!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var micBtn: UIButton!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var view: UIView!
    @IBOutlet weak var didYouMeanLbl: UILabel!
    @IBOutlet weak var suggestionsStackView: UITammStackView!
    @IBOutlet weak var trendingStack: UITammStackView!
    @IBOutlet weak var trendingStackHeightConstraint: NSLayoutConstraint!
    public var delegate:EventsSearchOverlayViewDelegate!
    public let disposeBag = DisposeBag()
    public var searchItems:[TalkToUsItemViewDataType] = [] {
        didSet{
            self.adjustStackView(view: suggestionsStackView, items: searchItems, heightConstraint: stackViewHeightConstraint)
        }
    }
    public var trendingItems:[TalkToUsItemViewDataType] = [] {
        didSet{
            self.adjustStackView(view: trendingStack, items: trendingItems, heightConstraint: trendingStackHeightConstraint)
        }
    }
    var eventsViewModel:EventsViewModel!
    @IBOutlet weak var refreshBtn: UIButton!
    private var text:String!
    @IBAction func refreshClickHandler(_ sender: UIButton) {
        self.eventsViewModel.isToGetTrending.value = false
        self.eventsViewModel.isToGetTrending.value = true
    }
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    init(frame: CGRect,eventsViewModel:EventsViewModel,text:String) {
        super.init(frame:frame)
        self.eventsViewModel = eventsViewModel
        self.text = text
        commonInit()
        setupHooks()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("EventsSearchOverlayView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        
        didYouMeanLbl.addCharacterSpacing(space: 2.3)
        self.didYouMeanLbl.isHidden = true
        
        backBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        searchTxt.attributedPlaceholder = NSAttributedString(string: L10n.searchEllipsized, attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 155/255, green: 155/255, blue: 155/255, alpha: 1) , NSAttributedStringKey.font : UIFont.init(name: "CircularStd-Book", size: 16)!])
    }
    func displayBlockingToast(message:String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            alert.show()
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 10) {
            
            alert.dismiss(animated: true, completion: nil)
            let txt = self.searchTxt.text
            if txt != nil && txt != ""{
                self.searchTxt.becomeFirstResponder()
                self.searchTxt.text = txt
            }
            
        }
    }

   
    
    @IBAction func micBtnClicked(_ sender: Any) {
        if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
        self.displayBlockingToast(message: L10n.recognizingVoice)
        self.eventsViewModel.startSpeechService().asObservable().subscribe(onNext: { (str:String?) in
            self.searchTxt.text = str
            self.searchTextChanged()
        }).disposed(by: disposeBag)
        }
    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        
//        //delegate?.suggestionIsSelected(message: self.searchTxt.text!)
//        return true
//    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.backPressed()
    }
    
    
    func itemIsSelected(message: TalkToUsItemViewDataType) {
        delegate.eventIsSelected(message: message)
        self.removeFromSuperview()
    }
    func setupHooks(){
        searchTxt.addTarget(self, action: #selector(searchTextChanged), for: .editingChanged)
        searchTxt.text = self.text
        self.eventsViewModel.isToGetTrending.value = true
        eventsViewModel.trendingVar.asObservable().subscribe(onNext: { (ar:[String]?) in
            if ar != nil{
                self.trendingItems =   (ar?.map({ (s:String) -> TalkToUsItemViewDataType in
                    return PoppularItemViewData(name: s)
                }))!
            }else{
                
            }
        }).disposed(by: disposeBag)
        eventsViewModel.queryResultVar.asObservable().subscribe(onNext: { (ar:[String]?) in
            if ar != nil{
                self.searchItems =   (ar?.map({ (s:String) -> TalkToUsItemViewDataType in
                    return PoppularItemViewData(name: s)
                }))!
            }else{
                
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func searchTextChanged() {
        if (searchTxt.text?.isEmpty)! || (searchTxt.text?.count ?? 0) < 3{
            searchItems = []
        }else{
            self.eventsViewModel.currentQueryVar.value = self.searchTxt.text
        }
    }
    private func adjustStackView(view:UITammStackView , items:[TalkToUsItemViewDataType] , heightConstraint:NSLayoutConstraint){
        view.items = items
        if view.tag == 1{
            if(items.count == 0){
                self.didYouMeanLbl.isHidden = true
            }
            else{
                self.didYouMeanLbl.isHidden = false
            }
        }
        view.layoutSubviews()
        view.resizeToFitSubviews()
        view.delegate = self
        heightConstraint.constant = view.frame.height
        self.stackView.layoutSubviews()
        self.stackView.setNeedsLayout()
        self.stackView.setNeedsUpdateConstraints()
    }
}

extension EventsSearchOverlayView : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate.eventIsSelected(message: PoppularItemViewData(name: textField.text!) )
        self.removeFromSuperview()
        return true
    }
}
