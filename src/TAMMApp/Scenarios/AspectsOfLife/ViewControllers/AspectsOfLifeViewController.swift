//
//  AspectsOfLifeViewController.swift
//  TammUI
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

protocol AspectsOfLifeViewControllerDelegate: class{
    func openAspectsOfLifeList()
    func openJourneys()
}
enum AspectsOfLifeViewControllerEnum{
    case SERVICES, JOURNEYS
}
class AspectsOfLifeViewController: ContainerViewController, AspectsOfLifeStoryboardLodable{
    
    static var lunchMode: AspectsOfLifeViewControllerEnum = .SERVICES
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnServices: LocalizedSelectableButton!
    @IBOutlet weak var btnJourneys: LocalizedSelectableButton!
    public weak var delegate: AspectsOfLifeViewControllerDelegate?
    public var draggableButton:UICircularImage!
    fileprivate let selectedButtonBorder:CGFloat = 4
    fileprivate let stackViewBottomBorder:CGFloat = 1
    fileprivate let unSelectedFont:UIFont = UIFont.init(name: "CircularStd-Medium", size: 18)!
    fileprivate let selectedButtonBorderColor:String = "#10596F"
    fileprivate let stackViewBottomBorderColor:String = "#CAC8D1"
    fileprivate var layer:CALayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(title: L10n.servicesTitle)
        
        btnJourneys.originalFont = unSelectedFont
        btnServices.originalFont = unSelectedFont
        btnJourneys.fontProxy = unSelectedFont
        btnServices.fontProxy = unSelectedFont

        btnJourneys.RTLFont = "Swissra-Normal"
        btnJourneys.RTLSelecttedFont = "Swissra-Bold"
        
        btnServices.RTLFont = "Swissra-Normal"
        btnServices.RTLSelecttedFont = "Swissra-Bold"
        
        btnJourneys.selectedFontReference = UIFont.init(name: "Swissra-Normal-Bold", size: 18)
        btnServices.selectedFontReference = UIFont.init(name: "Swissra-Bold", size: 18)
        
        btnJourneys.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnServices.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnJourneys.selectedColor = UIColor.init(hexString: "#11596F")
        btnServices.selectedColor = UIColor.init(hexString: "#11596F")
        
        btnJourneys.originalColor = UIColor.init(hexString: "#151331")
        btnServices.originalColor = UIColor.init(hexString: "#151331")
        
        if AspectsOfLifeViewController.lunchMode == .SERVICES{
            buttonIsSelected(button: btnServices, openJournyes: false)
        }else{
            buttonIsSelected(button: btnJourneys, openJournyes: true)
        }
      

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addBorderToStackView()
        
    }
    
    func rightButtonAction(sender: UIBarButtonItem){
        
    }

    
    @IBAction func btnServicesClicked(_ sender: LocalizedSelectableButton) {
        AspectsOfLifeViewController.lunchMode = .SERVICES
        buttonIsSelected(button: sender, openJournyes: false)
    }
    @IBAction func btnJourneysClickes(_ sender: LocalizedSelectableButton) {
        AspectsOfLifeViewController.lunchMode = .JOURNEYS
        buttonIsSelected(button: sender, openJournyes: true)
    }
    
    fileprivate func addBorderToStackView(){

        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: stackView.frame.height - stackViewBottomBorder, width: stackView.frame.width, height: stackViewBottomBorder)
        
        bottomBorder.backgroundColor = UIColor.init(hexString: stackViewBottomBorderColor).cgColor
        
        stackView.layer.addSublayer(bottomBorder)
    }
    fileprivate func buttonIsSelected(button:LocalizedSelectableButton, openJournyes:Bool){
        if(layer != nil){
            layer?.removeFromSuperlayer()
        }
        
        btnJourneys.isButtonSelected = false
        btnServices.isButtonSelected = false
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: button.frame.height - selectedButtonBorder, width: button.frame.width, height: selectedButtonBorder)
        bottomBorder.backgroundColor = UIColor.init(hexString: selectedButtonBorderColor).cgColor
        
        button.layer.addSublayer(bottomBorder)
        button.isButtonSelected = true
        layer = bottomBorder
        if(openJournyes){
            delegate?.openJourneys()
        }
        else{
            delegate?.openAspectsOfLifeList()
        }
        
    }


}
