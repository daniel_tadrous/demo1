//
//  FAQViewController.swift
//  TAMMApp
//
//  Created by Daniel on 6/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import SafariServices

protocol ISubmitCollaborate {
    func submitCollaborate(collaborate: CollaborateModel, onSuccess: @escaping ()->() )
    func updateCollaborateCell()
}
protocol CoCreateViewControllerDelegate: class{
    func openPollsFromCoCreate()
    func coCreateDoPresentCollaborateConfirmation()
}
class CoCreateViewController: TammViewController, CoCreateStoryboardLodable {
    
    @IBOutlet weak var cocreateTableView: UITableView!
    public var viewModel:CoCreateViewModel!
    let headerViewIdentifier = "co_create_id"
    let collapsedIdentifier = "PollCellCollapsed"
    let expandedAnswersIdentifier = "PollCellExpandedAnswers"
    let expandedResultsIdentifier = "PollCellExpandedResults"
    let participateCellIdentifier = "ParticipateCell"
    let collaborateCellIdentifier = "CollaborateCell"
    var categories: [String] = [String]()
    var disposeBag = DisposeBag()
    var popUpView:ConfirmationView?
    weak var delegate: CoCreateViewControllerDelegate?
    
    var polls: [MyCommunityModel.Poll]? {
        get{
            return viewModel?.viewData.value?.polls
        }
        set{
            viewModel?.viewData.value?.polls = newValue
        }
    }
    var surveys: [MyCommunityModel.Survey]? {
        get{
            return viewModel?.viewData.value?.surveys
        }
        set{
            viewModel?.viewData.value?.surveys = newValue
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(title: L10n.MyCommunity.getCommunityItemText(item: .CoCreation), willSetSearchButton: true)
        // Do any additional setup after loading the view.
        let bundle = Bundle(for: type(of: self))
        self.cocreateTableView.register(UINib(nibName: "CoCreateSectionHeaderView", bundle: bundle), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        setupHooks()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupHooks(){
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] polls in
                if(polls == nil){
                    //TODO: should do somting ????
                }else{
                    
                    self.cocreateTableView.reloadData()
                    
                }
            })
            .disposed(by: disposeBag)
    }
    var submitCollaborationDisposeBag: DisposeBag?
    
}

extension CoCreateViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 76
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0,1:
            return 70
        default:
            return 10
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.paleGreyTwo
        switch section {
        case 0,1:
            let btn = LocalizedButton()
            btn.backgroundColor = UIColor.duckEggBlueTwo
            btn.setTitleColor(UIColor.petrol, for: .normal)
            btn.setTitle(section == 0 ? L10n.MyCommunity.CoCreate.viewAllSurveys : L10n.MyCommunity.CoCreate.viewAllPolls, for: .normal)
            btn.applyFontScalling = true
            btn.RTLFont = "Swissra-Bold"
            btn.originalFont = UIFont(name: L10n.Lang == "en" ? "CircularStd-Bold" : "Swissra-Bold", size: 16)
            btn.titleLabel?.font = UIFont(name: L10n.Lang == "en" ? "CircularStd-Bold" : "Swissra-Bold", size: 16)
            btn.layer.borderColor = UIColor.turquoiseBlue.cgColor
            btn.layer.borderWidth = 1
            btn.addRoundCorners(radious: 25)
            btn.tag = section
            btn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(footerClicked(_:))))
            btn.frame = CGRect(x: 15, y: 10, width: UIScreen.main.bounds.width-30, height: 50)
            footerView.addSubview(btn)
        default:
            break
        }
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier)
        let label  = headerView?.viewWithTag(1) as! UILabel
        label.text = L10n.MyCommunity.CoCreate.getSectionHeader(section: section)
        return headerView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.surveys?.count ?? 0
        case 1:
            return self.polls?.count ?? 0
        default:
            return 1
        }
    }
    //        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //            return UITableViewAutomaticDimension
    //        }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var genCell: UITableViewCell!
        switch indexPath.section{
        case 0:
            let survey = self.surveys?[indexPath.row]
            genCell = tableView.dequeueReusableCell(withIdentifier: participateCellIdentifier)
            let label = genCell.viewWithTag(1) as! UILabel
            label.text = survey?.details
            let button = genCell.viewWithTag(3) as! UIButton
            button.addRoundCorners(radious: 20)
            let button2 = genCell.viewWithTag(2) as! UIButton
            button2.addRoundCorners(radious: 20)
            button2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(participateClicked(_:))))
            let view = genCell.viewWithTag(4) as! UIView
            view.addRoundCorners(radious: 5)
        case 1:
            
            let poll  = self.polls?[indexPath.row]
            if (poll?.isPromoted)!{
                if poll?.answerId != -1{
                    genCell = tableView.dequeueReusableCell(withIdentifier: expandedResultsIdentifier)
                    let cell  = genCell as! PollCell
                    cell.shareDimmerBtn?.backgroundColor = UIColor.clear
                    for view in (cell.stackViewContainer?.arrangedSubviews)!{
                        view.isHidden = true
                        cell.stackViewContainer?.removeArrangedSubview(view)
                    }
                    for answer in (poll?.answers)!{
                        let button = PollResultView(frame: CGRect(), answer: answer)
                        cell.stackViewContainer?.addArrangedSubview(button)
                    }
                }else{
                    genCell = tableView.dequeueReusableCell(withIdentifier: expandedAnswersIdentifier) as! PollCell
                    let cell  = genCell as! PollCell
                    for view in (cell.stackViewContainer?.arrangedSubviews)!{
                        view.isHidden = true
                        cell.stackViewContainer?.removeArrangedSubview(view)
                    }
                    for index in 0..<(poll?.answers?.count)!{
                        let answer = poll?.answers?[index]
                        let button = PollButton()
                        button.setTitle(answer?.text, for: .normal)
                        button.poll = MyCommunityModel.Poll(id: (poll?.id)!, answerId: index)
                        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answerSubmitted(_:))))
                        cell.stackViewContainer?.addArrangedSubview(button)
                    }
                    
                }
            }else{
                genCell = tableView.dequeueReusableCell(withIdentifier: collapsedIdentifier) as! PollCell
                
            }
            let cell  = genCell as! PollCell
            cell.colorizeFooter(isCurrent: (poll?.isCurrent)!)
            cell.setClosureDate(poll: poll!)
            let formater = NumberFormatter()
            formater.groupingSeparator = ","
            formater.numberStyle = .decimal
            cell.participantNumLabel.text = formater.string(from: NSNumber(value: (poll?.participantsCount)!))
            cell.titleLable.text = poll?.title
            cell.OnShareBtnClick = {
                self.viewModel.share(viewController: self, text: (poll?.title)!)
            }
            
            cell.OnCollapseExpandBtnClick = {
                poll?.isPromoted = !(poll?.isPromoted)!
                let animation = (poll?.isPromoted)! ? UITableViewRowAnimation.bottom : UITableViewRowAnimation.top
                self.cocreateTableView.reloadRows(at: [indexPath], with: animation)
            }
            
            
            
            
        case 2:
            genCell = tableView.dequeueReusableCell(withIdentifier: collaborateCellIdentifier)
            let cell = genCell as! CollaborateCell
            cell.delegate = self
            cell.emailTF.text = self.viewModel.getUserEmail()
        default:
            break
        }
        
        
        return genCell
    }
    @objc func answerSubmitted(_ sender: Any){
        let pollBtn = (sender as! UITapGestureRecognizer).view as! PollButton
        viewModel.poll.value = pollBtn.poll
        var index:Int = 0
        for poll in self.polls!{
            
            if poll.id == pollBtn.poll?.id{
                poll.answerId = pollBtn.poll?.answerId
                self.cocreateTableView.reloadRows(at: [IndexPath(row: index, section: 1)], with: UITableViewRowAnimation.automatic)
                break
            }
            index = index + 1
        }
        
    }
    @objc func participateClicked(_ sender: Any){
        //Toast.init(message: "Participate is clicked").Show()

        let vc = ConfirmationMessageViewController()
        vc.initialize(title: L10n.warning, message: L10n.youAreAboutToLeaveTAMM, CancelBtnTitle: L10n.cancel, confirmationBtnTitle: L10n.ok, okClickHandler: {
            let svc = SFSafariViewController(url: URL(string: "https://www.abudhabi.ae/Tamm/about.html")!)
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.removeFloatingMenu()
            //warningPopup.
            self.present(svc, animated: true, completion: nil)
        })
        
        self.present(vc, animated: true, completion: nil)
        
//        popUpView?.okBtn.addRoundCorners(radious: (popUpView?.okBtn.frame.height)!/2)
//        popUpView?.cancelBtn.addRoundCorners(radious: (popUpView?.cancelBtn.frame.height)!/2)

     
    }
    @objc func footerClicked(_ sender: Any){
        let btn = (sender as! UITapGestureRecognizer).view as! UIButton
        if btn.tag == 0 {
            Toast.init(message: L10n.commingSoon).Show()
        }else{
            self.delegate?.openPollsFromCoCreate()
        }
    }
    
}

extension CoCreateViewController: ISubmitCollaborate{
    
    func updateCollaborateCell() {
        // let cell  = self.cocreateTableView.cellForRow(at: IndexPath(row: 0, section: 2))
        //cell?.frame = CGRect(x: (cell?.frame.origin.x)!, y: (cell?.frame.origin.y)!, width: (cell?.frame.width)!, height: (cell?.frame.height)! + 100)
        //tableView(self.cocreateTableView, heightForRowAt: IndexPath(row: 0, section: 2))
        self.cocreateTableView.beginUpdates()
        self.cocreateTableView.endUpdates()
    }
    
    func submitCollaborate(collaborate: CollaborateModel, onSuccess: @escaping ()->() ) {
        self.viewModel.collaborate.value = collaborate
        let dispasable = self.viewModel.submitCollaborationStatus.subscribeOn(MainScheduler.instance).subscribe(onNext: { (item) in
            
        }, onError: { (e) in
            print(e)
        }, onCompleted: { [unowned self] in
            onSuccess()
            self.submitCollaborationDisposeBag = nil
            
   

            let vc = FeedBackThankYouViewController()
            vc.initialize(title: L10n.thankYou, summary: L10n.messageRecievedSummary, details: L10n.messageRecievedDetails, refernceNumber: nil)

            self.present(vc, animated: true, completion: nil)
            
            //self.delegate?.coCreateDoPresentCollaborateConfirmation()
            //Toast.init(message: "Thank you for your collaboration").Show()
//            self.navigationController?.popViewController(animated: true)
        }, onDisposed: {
            
        })
        submitCollaborationDisposeBag = DisposeBag()
        dispasable.disposed(by: submitCollaborationDisposeBag!)
        
    }
    
    
}

extension CoCreateViewController:RedirectOutsidePopupViewDelegate{
    func redirectNow() {
        let svc = SFSafariViewController(url: URL(string: "https://www.abudhabi.ae/Tamm/about.html")!)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.removeFloatingMenu()
        present(svc, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.addfloatingMenu()
    }
    
    
    
}
