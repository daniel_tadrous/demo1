//
//  MySubscriptionsParentViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 9/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MySubsciptionsViewControllerDelegate {
    func openActiveSubsciptions()
    func openExpiredSubsciptions()
}

enum SubscriptionType {
    case Active
    case Expired
}

class MySubscriptionsParentViewController: ContainerViewController, MySubscriptionsParentStoryboardLoadable{
    @IBOutlet weak var activeButton: LocalizedSelectableButton!
    @IBOutlet weak var expiredButton: LocalizedSelectableButton!
    @IBOutlet weak var underLine: UIView!
    @IBOutlet weak var fullLineView: UIView!
    @IBOutlet var contentView: UIView!
    
    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    
    fileprivate var selectedArabicFont = UIFont(name: "Swissra-Bold",size: 18)
    fileprivate var unSelectedArabicFont = UIFont(name: "Swissra-Normal",size: 18)
    
    fileprivate var selectedFontColor = UIColor(hexString: "005971")
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138")
    
    var delegate:MySubsciptionsViewControllerDelegate?
    
    var screenSelected:SubscriptionType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = L10n.MySubscriptionsStrings.mySubscriptions
        
        activeButton.originalColor = unSelectedFontColor
        activeButton.selectedColor = selectedFontColor
        
        activeButton.RTLSelecttedFont = "Swissra-Bold"
        expiredButton.RTLSelecttedFont = "Swissra-Bold"
        
        if L10n.Lang == "en" {
            activeButton.originalFont = unSelectedFont
            activeButton.fontProxy = unSelectedFont
            
            activeButton.selectedFontReference = selectedFont
            
            expiredButton.originalFont = unSelectedFont
            expiredButton.fontProxy = unSelectedFont
            expiredButton.selectedFontReference = selectedFont
        }else {
            activeButton.originalFont = unSelectedFont
            activeButton.fontProxy = unSelectedArabicFont
            
            activeButton.selectedFontReference = selectedArabicFont
            
            expiredButton.originalFont = unSelectedArabicFont
            expiredButton.fontProxy = unSelectedArabicFont
            expiredButton.selectedFontReference = selectedArabicFont
        }
        
        expiredButton.originalColor = unSelectedFontColor
        expiredButton.selectedColor = selectedFontColor
        
        showScreen(.Active,forButton:activeButton)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setViewsColors()
    }
    
    func setViewsColors() {
        var underLineColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        var fullLineColor = #colorLiteral(red: 0.8506266475, green: 0.8481912017, blue: 0.8733561635, alpha: 1).getAdjustedColor()
        var contentViewColor = #colorLiteral(red: 0.9562498927, green: 0.9562721848, blue: 0.9562601447, alpha: 1).getAdjustedColor()
        var containerViewColor = #colorLiteral(red: 0.9562581182, green: 0.9564779401, blue: 0.9625678658, alpha: 1).getAdjustedColor()
        contentView.backgroundColor = contentViewColor
        containerView.backgroundColor = contentViewColor
        underLine.backgroundColor = underLineColor
        fullLineView.backgroundColor = fullLineColor
        if screenSelected == .Active{
            activeButton.isButtonSelected = true
            expiredButton.isButtonSelected = false
        }
        else if screenSelected == .Expired{
            activeButton.isButtonSelected = false
            expiredButton.isButtonSelected = true
        }
    }
    
    @IBAction func activeButtonAction(_ sender: LocalizedSelectableButton) {
        
        showScreen(.Active, forButton:sender)
    }
    
    @IBAction func expiredButtonAction(_ sender: LocalizedSelectableButton) {
        showScreen(.Expired, forButton:sender)
    }
    
    func showScreen( _ screenName:SubscriptionType,forButton button:LocalizedSelectableButton){
        screenSelected = screenName
        if screenName == .Active{
            activeButton.isButtonSelected = true
            expiredButton.isButtonSelected = false
            delegate?.openActiveSubsciptions()
        }
        else if screenName == .Expired{
            activeButton.isButtonSelected = false
            expiredButton.isButtonSelected = true
            delegate?.openExpiredSubsciptions()
        }
    }
    
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        if button == activeButton {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x
        }
        else if button == expiredButton{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        if screenSelected == .Active {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
            
            
        else if screenSelected == .Expired{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }

}
