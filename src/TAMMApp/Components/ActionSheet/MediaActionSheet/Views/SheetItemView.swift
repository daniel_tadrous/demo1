//
//  ItemView.swift
//  cameraTest
//
//  Created by kerolos on 5/2/18.
//  Copyright © 2018 kerolos. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class SheetItemView : UIView{
    
    @IBOutlet weak var fontIconLabel: UILabel!
    
    @IBOutlet weak var titelLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBInspectable
    public var lowerViewCornerRadious: CGFloat = 10
    
    @IBOutlet weak var upperMask: UIView!
    @IBOutlet weak var lowerMask: UIView!
    
    
    var isUpperMaskHidden: Bool{
        get{
            return upperMask.isHidden
        }
        set{
            upperMask.isHidden = newValue
        }
    }
    var isLowerMaskHidden: Bool{
        get{
            return lowerMask.isHidden
        }
        set{
            lowerMask.isHidden = newValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit(){
//        roundCorners(view: mainView, radius: lowerViewCornerRadious)
    }
    
    
    func roundCorners(view: UIView, radius: CGFloat) {
        //        self.layer.borderColor = BorderColor?.cgColor
        //        view.layer.borderWidth = 1
        if #available(iOS 11.0, *) {
            if(L102Language.currentAppleLanguage().contains("ar")){
                view.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                view.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            view.layer.cornerRadius = radius
        } else {
            // Fallback on earlier versions
            view.layer.cornerRadius = radius
        }
        
    }
    
    //
    //    override func layerWillDraw(_ layer: CALayer) {
    //
    //        super.layerWillDraw(layer)
    //
    //    }
    
    override func layoutSubviews() {
        roundCorners(view: mainView, radius: lowerViewCornerRadious)
        super.layoutSubviews()
    }
}
