    //
    //  FAQViewController.swift
    //  TAMMApp
    //
    //  Created by kerolos on 4/24/18.
    //  Copyright © 2018 Igor Kulman. All rights reserved.
    //
    
    import UIKit
    import RxSwift
    
    class FAQViewController: TammViewController, FAQStoryboardLodable {
        @IBOutlet weak var faqsTableView: UITableView!
        
        @IBOutlet weak var searchView: UIView!
        
        @IBOutlet weak var searchText: UITextField!
        var loadMoreButton: LocalizedButton!
        fileprivate let heightOfShowMoreButton:CGFloat = 50
        let filterBtnSize : CGFloat = 40
        public var viewModel:FAQViewModel!
        var faqsWithCategories: [String:[AspectOfLifeServiceFaqsModel]] = [:]
        let headerViewIdentifier = "header_view"
        var categories: [String] = [String]()
        var disposeBag = DisposeBag()
        var faqs: [AspectOfLifeServiceFaqsModel]? {
            get{
                return viewModel?.viewData.value
            }
            set{
                viewModel?.viewData.value = newValue
            }
        }
        var aspectsOfLife: [JourneyModel]? {
            get{
                return viewModel?.viewDataAspectOfLife.value
            }
            set{
                viewModel?.viewDataAspectOfLife.value = newValue
            }
        }
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setNavigationBar(title: L10n.faqs, willSetSearchButton: true)
            // Do any additional setup after loading the view.
            self.searchView.addRoundCorners(radious: 17)
           let bundle = Bundle(for: type(of: self))
            self.faqsTableView.register(UINib(nibName: "FaqSectionHeaderView", bundle: bundle), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
            setupHooks()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        func setupHooks(){
            self.searchText.placeholder = L10n.search_placeholder
            
            viewModel!.viewData.asObservable()
                .subscribeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] supportFaqs in
                    if(supportFaqs == nil){
                        //TODO: should do somting ????
                    }else{
                        
                        for faq in self.faqs!{
                            if self.faqsWithCategories[faq.relatedToName!] == nil{
                                self.faqsWithCategories[faq.relatedToName!] = []
                                self.categories.append(faq.relatedToName!)
                            }
                            self.faqsWithCategories[faq.relatedToName!]?.append(faq)
                        }
                        if self.faqs!.count == 0{
                            self.removeLoadMore()
                        }else{
                            self.addLoadMore()
                        }
                        self.faqsTableView.reloadData()
                        
                    }
                })
                .disposed(by: disposeBag)
        }
        
        @IBAction func micSearchClickHandler(_ sender: UIButton) {
            if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: self.viewModel.speechDuration)
            self.viewModel.startSpeechService()
            self.viewModel.recognizedTextVar.asObservable().subscribeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] text in
                    if(text == nil){
                        
                    }else{
                        self.searchText.text = text
                        
                    }
                })
                .disposed(by: disposeBag)
                
            }
        }
    }
    
    extension FAQViewController : UITableViewDelegate, UITableViewDataSource{
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return self.categories.count
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            
            return 50
        }
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 10
        }
        
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let footerView = UIView()
            footerView.backgroundColor = UIColor.paleGreyTwo
            return footerView
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier)
            
            
            //let label = LocalizedLabel()
//            label.textColor = UIColor.darkBlueGrey
//            label.originalFont = UIFont(name: "CircularStd-Bold", size: 20)
//            label.RTLFont = "Swissra-Bold"
//            label.applyFontScalling = true
//            label.font = label.originalFont
            let label  = headerView?.viewWithTag(1) as! UILabel
            label.text = self.categories[section]
            //label.backgroundColor = UIColor.clear
            //if section == 0{
//            if L10n.Lang == "en"{
//                label.frame =  CGRect(x: 17, y: 0, width: tableView.frame.width - (self.filterBtnSize + 34), height: view.frame.height-5)
//            }else{
//                label.frame =  CGRect(x: (self.filterBtnSize + 17), y: 0, width: tableView.frame.width - (self.filterBtnSize + 34), height: view.frame.height-5)
//            }
            // }else{
            //    label.frame =  CGRect(x: 17, y: 0, width: tableView.frame.width-34, height: view.frame.height-5)
            // }
            //view.addSubview(label)
            
            
            //if section == 0 {
//            let filterLabel = UILabel()
//            filterLabel.textColor = UIColor.darkBlueGrey
//            filterLabel.font =  UIFont(name: "tamm", size: self.filterBtnSize)!
//            filterLabel.text = "7"
//            filterLabel.backgroundColor = UIColor.clear
//            print(Locale.current)
//            if L10n.Lang == "en"{
//                filterLabel.frame = CGRect(x: tableView.frame.width-(self.filterBtnSize + 17), y: 0, width: self.filterBtnSize, height: view.frame.height-5)
//            }else{
//                filterLabel.frame = CGRect(x: 17, y: 0, width: self.filterBtnSize, height: view.frame.height-5)
//            }
            let filterLabel = headerView?.viewWithTag(2) as! UILabel
            filterLabel.isUserInteractionEnabled = true
            filterLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterClicked)))
//            filterLabel.tag = 7
                filterLabel.isHidden = section != 0
//            view.addSubview(filterLabel)
            // }
            
            
            return headerView
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return self.faqsWithCategories[self.categories[section]]?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var cell: AolServiceFaqCell!
            
            let serviceFaq  = (self.faqsWithCategories[self.categories[indexPath.section]]?[indexPath.row])!
            if serviceFaq.isCollapsed{
                cell = tableView.dequeueReusableCell(withIdentifier: "AolServiceFaqCellSec2Collapsed") as! AolServiceFaqCell
                
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: "AolServiceFaqCellSec2") as! AolServiceFaqCell
                cell.sec2DescLabel?.text = serviceFaq.aProp
                
            }
            cell.sec2TitleLabel?.text = serviceFaq.qProp
            cell.OnCollapseBtnClick = {
                serviceFaq.isCollapsed = !serviceFaq.isCollapsed
                self.faqsTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
            return cell
        }
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let indexPaths = self.faqsTableView.indexPathsForVisibleRows
            let firstVisibleIndexPath = indexPaths?.count != 0 ? indexPaths?[0] : self.faqsTableView.indexPathsForRows(in: CGRect(x: 15, y: 200, width: self.faqsTableView.frame.width, height: 300))?[0]
            let sectionIndex = (firstVisibleIndexPath?.section)!
            for sec in 0..<self.categories.count{
                if self.faqsTableView.headerView(forSection: sec) != nil{
                    let subViews = (self.faqsTableView.headerView(forSection: sec)?.subviews)!
                    for view in subViews{
                        if view.tag == 2{
                            view.isHidden = sec != sectionIndex
                            break
                        }
                    }
                }
            }
        }
        @objc func filterClicked(){
            let responsibilityView = FaqFilterView(frame: UIScreen.main.bounds, viewModel: viewModel) { (faqFilter:FaqFilterModel ) in
                self.faqsWithCategories = [:]
                self.categories = [String]()
                self.viewModel.faqFilter = faqFilter
                self.viewModel.pageId.value = 1
            }
            UIApplication.shared.keyWindow!.addSubview(responsibilityView)
            UIApplication.shared.keyWindow!.bringSubview(toFront: responsibilityView)
            
        }
        
    }
    
    extension FAQViewController{
        
        fileprivate func initiateLoadMoreButton(){
            loadMoreButton = LocalizedButton()
            loadMoreButton.frame = CGRect(x: 20, y: 0, width: UIScreen.main.bounds.width - 40, height: heightOfShowMoreButton)
            loadMoreButton.backgroundColor = UIColor.init(hexString: "#DFF4F7")
            loadMoreButton.addTarget(self, action: #selector(loadMoreBtnClicked), for: .touchUpInside)
            loadMoreButton.RTLFont = "Swissra-Bold"
            loadMoreButton.applyFontScalling = true
            loadMoreButton.setTitle(L10n.load_more, for: .normal)
            loadMoreButton.setTitleColor(UIColor.petrol, for: .normal)
            
            loadMoreButton.titleLabel?.font = UIFont.init(name: "CircularStd-Bold", size: 16)
            loadMoreButton.originalFont = UIFont.init(name: "CircularStd-Bold", size: 16)
            loadMoreButton.layer.borderWidth = 1
            loadMoreButton.layer.borderColor = UIColor.turquoiseBlue.cgColor
            loadMoreButton.addRoundCorners(radious: 17)
            
        }
        fileprivate func addLoadMore(){
            if(loadMoreButton == nil){
                initiateLoadMoreButton()
            }
            self.faqsTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: heightOfShowMoreButton +  100))
            self.faqsTableView.tableFooterView?.addSubview(loadMoreButton)
        }
        fileprivate func removeLoadMore(){
            self.faqsTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
            
        }
        @objc func loadMoreBtnClicked(){
            
            self.viewModel.pageId.value = self.viewModel.pageId.value! + 1
            //self.faqsTableView.reloadData()
            
        }
    }
    
