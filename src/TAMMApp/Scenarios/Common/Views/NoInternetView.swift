//
//  FaqFilterView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift
import CoreLocation

class NoInternetView : UIView{
 
   
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    var message:String = ""
    var onRetryClickHandler:(()->Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        setupHooks()
    }
    init(frame: CGRect,message:String,onRetryClickHandler:@escaping ()->Void) {
        super.init(frame: frame)
        self.onRetryClickHandler = onRetryClickHandler
        self.message = message
        commonInit()
        setupHooks()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "NoInternetView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        setupHooks()
        setupUiData()
    }
   
    
    @objc func cancelBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
      
       
    }
    
   
   
    func setupUiData(){
        
      
        
    }
    @IBAction func OnRetryClick(_ sender: LocalizedButton) {
        onRetryClickHandler?()
        cancelBtnClicked()
    }
    
}

