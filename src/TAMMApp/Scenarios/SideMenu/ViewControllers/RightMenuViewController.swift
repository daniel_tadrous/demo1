//
//  RightMenuViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class RightMenuViewController: UITableViewController {

    var viewModel = SideMenuViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupUI(){
        tableView.register(UINib(nibName: SideMenuItemTableViewCell.generalIdentifier, bundle: nil), forCellReuseIdentifier: SideMenuItemTableViewCell.generalIdentifier)
        
        viewModel.sections.asObservable().subscribeOn(MainScheduler.instance).subscribe(onNext:{
            _ in
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.sections.value.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.sections.value[section].count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.viewModel.sections.value[indexPath.section][indexPath.row]
        
        let cellToReturn : UITableViewCell
        if item.type == .text{
            let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuItemTableViewCell.generalIdentifier) as! SideMenuItemTableViewCell
            
            cell.textLabel?.text = item.text
            cell.textLabel?.font = fontsUsed[item.type]
            cellToReturn = cell
        }
        else if item.type == .switchProfile {
            let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuItemTableViewCell.generalIdentifier) as! SideMenuItemTableViewCell
            
            let switchProfileItem = item as! SideMenuSwitchProfileItem
            cell.textLabel?.text = switchProfileItem.text
            cell.textLabel?.font = fontsUsed[item.type]
            
            cellToReturn = cell

        }
        else if item.type == .logout {
            let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuItemTableViewCell.generalIdentifier) as! SideMenuItemTableViewCell
            
            let switchProfileItem = item as! SideMenuSwitchProfileItem
            cell.textLabel?.text = switchProfileItem.text
            cell.textLabel?.font = fontsUsed[item.type]
            
            
            cellToReturn = cell
            
        }
        else {
            cellToReturn = tableView.dequeueReusableCell(withIdentifier: SideMenuItemTableViewCell.generalIdentifier) as! SideMenuItemTableViewCell

        }
        return cellToReturn
    }
    
    
    private let fontsUsed :[SideMenuItemTypes:UIFont] = [.text: UIFont(name: "Circular Std", size: 20.0)! ,
                                                         .switchProfile : UIFont(name: "Circular Std", size: 20.0 )!,
                                                         .logout: UIFont(name: "Circular Std", size: 20.0 )!]
    

}
