//
//  MessageModel.swift
//  TAMMApp
//
//  Created by Daniel on 7/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class Contact:Mappable{
    
    var email: String?
    var phone: String?
   
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        email <- map["email"]
        phone <- map["phone"]
    }
}
