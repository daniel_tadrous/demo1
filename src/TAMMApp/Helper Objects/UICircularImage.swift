//
//  UICircularImage.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
enum CircularImageType{
    case FontImage
    case Image
}
@IBDesignable
class UICircularImage: UIView{
    private var _useFontImage = false
    private var _image:UIImageView?
    private var _fontImage:UILabel?
    var getImageView: UIImageView {
        get{
            return _image!
        }
    }
    public var EnableDragging:Bool = false
    @IBInspectable public var CircularFontImage: Bool = false {
        didSet {
            _useFontImage = CircularFontImage
            _image?.isHidden = _useFontImage
            _fontImage?.isHidden = !_useFontImage
            
        }
    }
    @IBInspectable public var FontImage: String = "" {
        didSet {
            _fontImage?.text = FontImage;
            _fontImage?.textAlignment = .center;
            
        }
    }
    @IBInspectable public var FontSize:Int = 60{
        didSet {
            _fontImage?.font = UIFont(name: FontName!, size: CGFloat(FontSize))
            
            _fontImage?.adjustsFontSizeToFitWidth = true
            
        }
    }
    
    @IBInspectable public var FontName: String? {
        didSet {
            _fontImage?.font = UIFont(name: FontName!, size: CGFloat(FontSize))
            
            _fontImage?.adjustsFontSizeToFitWidth = true
            
        }
    }
    @IBInspectable public var FontImageColor: UIColor = .clear {
        didSet {
            _fontImage?.textColor = FontImageColor
            
        }
    }
    @IBInspectable public var Image: UIImage?{
        didSet {
            _image?.image = Image;
            
        }
    }
    override var bounds: CGRect{
        didSet {
            self._fontImage?.frame = CGRect(origin: (self._fontImage?.frame.origin)!, size: self.frame.size)
            self._image?.frame = CGRect(origin: (self._image?.frame.origin)!, size: self.frame.size)
            self.layer.cornerRadius = self.frame.width/2
            self.clipsToBounds = true
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self._image?.setNeedsLayout()
        self._image?.layoutIfNeeded()
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self._image?.setNeedsLayout()
        self._image?.layoutIfNeeded()
        self.initialize()
    }
    
    private func initialize(){
        _image = UIImageView(frame: self.bounds)
        _fontImage = UILabel(frame: self.bounds)
        self.addSubview(_image!)
        self.addSubview(_fontImage!)
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(EnableDragging){
            if let touch = touches.first {
                let position = touch.location(in: self.superview)
            
                self.center = CGPoint(x: position.x, y: position.y)
            
            }
        }
    }
}

