//
//  TalkToUsOverLayFooterView.swift
//  TAMMApp
//
//  Created by Daniel on 6/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
protocol TalkToUsOverLayFooterViewDelegate {
    func OnSubmit(messageTypeId: Int)
}
class TalkToUsOverLayFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var arrowLbl: UILabel!
    @IBOutlet weak var messageType: UITextField!
    @IBOutlet weak var messageTypeTouchableView: UIView!
    private var disposeBag = DisposeBag()
    private let debounceTimeInSec: Double = 0.5
    var delegate: TalkToUsOverLayFooterViewDelegate?
    private var typesDisplayViewData: MessageTypesViewDataType?
    private var selectedMessageId:Int = -1
    
    private var pickerField = UITextField()
   
    @IBAction func onNextClickHandler(_ sender: UIButton) {
        delegate?.OnSubmit(messageTypeId: self.selectedMessageId)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//         //commonInit()
//    }
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        //commonInit()
//    }
    
    func commonInit(){
        self.arrowLbl.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        let tapGestureDD = UITapGestureRecognizer()
        messageTypeTouchableView.isUserInteractionEnabled = true
        messageTypeTouchableView.addGestureRecognizer(tapGestureDD)
        
        tapGestureDD.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: {
            [unowned self ] recognizer in
            self.openPickerView()
        }).disposed(by: disposeBag)
        //tab outside
        let tapGestureResigne = UITapGestureRecognizer()
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGestureResigne)
        
        tapGestureResigne.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: {
            [unowned self ] recognizer in
            self.endEditing(true)
           
        }).disposed(by: disposeBag)
        
    }
    func fillPickerView(messagesTypes:Observable<MessageTypesViewDataType?>){
        
        messagesTypes.observeOn(MainScheduler.instance).subscribe(onNext:{
            [unowned self] typesDisplayData in
            self.typesDisplayViewData = typesDisplayData
            self.selectedMessageId = self.selectedMessageId == -1 ?(typesDisplayData?.selectedId) ?? 0 : self.selectedMessageId
            if (self.selectedMessageId != -1){
                print(self.selectedMessageId)
                // getting the selected item
                let text = self.typesDisplayViewData?.types?.reduce(nil, { (result, type) -> String? in
                    if self.selectedMessageId == type.messageId {
                        return type.messageName
                    }else{
                        return result
                    }
                })
                self.messageType.text = text
            }
        }).disposed(by: disposeBag)
    }
    private func openPickerView(){
        let picker = PickerView()
        picker.items = ((self.typesDisplayViewData?.types) ?? []).map({ (type) -> PickerItemType in
            return PickerItem.init(id: type.messageId, displayName: type.messageName)
        })
        var selectedIndex = -1
        let itemCount = (self.typesDisplayViewData?.types?.count ?? 0)
        for i in 0..<itemCount {
            if self.typesDisplayViewData?.types?[i].messageId == selectedMessageId {
                selectedIndex = i
                break
            }
        }
        if selectedIndex != -1{
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        pickerField.inputView = picker
        pickerField.removeFromSuperview()
        picker.viewDelegate = self
        pickerField.frame = CGRect.zero
        pickerField.clipsToBounds = true
        self.addSubview(pickerField)
        pickerField.becomeFirstResponder()
        
        
    }
}
extension TalkToUsOverLayFooterView : PickerViewDelegate{
    
    func itemSelected(item: PickerItemType, pickerView: PickerView) {
        //        self.pickerView?.removeFromSuperview()
        let item2 = MessageTypeViewData.init(messageName: item.displayName, messageId: item.id as! Int)
        messageType.text = item2.messageName
        self.selectedMessageId = item2.messageId
    }
    
    func clickOutsidePicker() {
        //        self.pickerView?.removeFromSuperview()
    }
    
    
}
