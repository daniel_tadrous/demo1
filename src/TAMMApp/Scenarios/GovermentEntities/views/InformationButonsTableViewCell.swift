//
//  InformationButonsTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class InformationButonsTableViewCell: UITableViewCell {

    public static let namedNibName = "InformationButonsTableViewCell"

    
    @IBOutlet weak var leftContent: UIView!
    @IBOutlet weak var rightContent: UIView!

    @IBOutlet weak var leftIconLabel: StyledLabel!
    @IBOutlet weak var leftLabel: LocalizedLabel!
    
    @IBOutlet weak var rightIconLabel: StyledLabel!
    @IBOutlet weak var rightLabel: LocalizedLabel!
    
    
    @objc var leftPressedEvent: (() -> ())?
    @objc var rightPressedEvent: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupClick()
    }

    func setupClick(){
        leftContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getter: self.leftPressedEvent) ))
        rightContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getter: self.rightPressedEvent) ))
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    fileprivate func roundCorners(_ view: UIView) {
        let h = view.frame.height
        view.layer.cornerRadius = h / 10
        
        if #available(iOS 11.0, *) {
            view.layer.maskedCorners =
                [
                    //                .layerMinXMinYCorner,
                    .layerMinXMaxYCorner,
                    .layerMaxXMaxYCorner,
                    .layerMinXMinYCorner
            ]
        } else {
            // Fallback on earlier versions
        }
        view.layer.cornerRadius = h / 8
    }
    
    private func updateLayout(){
        // roundCorners for content views
        applyShadow(leftContent)
        applyShadow(rightContent)
        
        roundCorners(leftContent)
        roundCorners(rightContent)
        
        leftIconLabel.layer.cornerRadius = leftIconLabel.frame.height / 2
        rightIconLabel.layer.cornerRadius = rightIconLabel.frame.height / 2
        
    }
    
    private func applyShadow(_ view: UIView){
        view.layer.shadowColor = UIColor.lightGray.cgColor;
        view.layer.shadowOpacity=0.5;
        view.layer.shadowRadius=2.0;
        view.layer.shadowOffset = CGSize(width:0, height:0);
        view.layer.masksToBounds = false;
    }
    
    private func setIcon(_ label: UILabel, _ fontIcon:String?, _ color: UIColor, _ textColor: UIColor ){
        label.text = fontIcon
        label.textColor = textColor
        label.backgroundColor = color
    }
    
    func setLeftContent(text: String, fontIcon:String?, _ color: UIColor, _ textColor: UIColor, completed: (() -> ())? = nil ){
        setIcon(leftIconLabel, fontIcon, color, textColor)
        leftLabel.text = text
        leftContent.isHidden = false
        
        if let  completed = completed{
            leftPressedEvent = completed
        }
    }
    
    func setRightContent(text: String, fontIcon:String?, _ color: UIColor, _ textColor: UIColor, completed: (() -> ())? = nil ){
        setIcon(rightIconLabel, fontIcon, color, textColor)
        rightLabel.text = text
        rightContent.isHidden = false
        if let  completed = completed{
            rightPressedEvent = completed
        }
    }
    
    func clearSetting(){
        leftContent.isHidden = true
        rightContent.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayout()
    }
    
    
}
