//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ResponsibilityView : UIView{
    
    
    var onViewMapClick: (()->Void)?
    var onViewPageClick: (()->Void)?
    
    @IBOutlet weak var addressLabel: LocalizedLabel?
    @IBOutlet weak var serviceHours: LocalizedLabel?
    @IBOutlet weak var serviceDays: LocalizedLabel?
    @IBOutlet weak var hoursLabel: LocalizedLabel?
    @IBOutlet weak var daysLabel: LocalizedLabel?
    @IBOutlet weak var resEntityDesLabel: LocalizedLabel!
    @IBOutlet weak var responsibleEntityTitle: LocalizedLabel!
    @IBOutlet weak var responsibleEntityImage: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    var isOnlineService: Bool!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    public var viewModel:  ResponsibleEntityViewModel?
    var disposeBag = DisposeBag()
    
    var responsibileEntity: ResponsibleEntity?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    convenience init(frame: CGRect, isOnlineService: Bool, id: String, viewModel: ResponsibleEntityViewModel) {
        self.init(frame: frame)
        self.isOnlineService = isOnlineService
        self.viewModel = viewModel
        self.viewModel?.responsibleEntityId.value = id
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = isOnlineService ? "ResponsibilityViewOnline" : "ResponsibilityViewOffline"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
       setupHooks()

    }
    
    @objc func cancelBtnClicked(){
        let height = UIApplication.shared.keyWindow!.bounds.height

        // animate from current position down out of the screen
        let superView = self.superview!
        superView.isUserInteractionEnabled = false
        UIView.animate(withDuration: Dimensions.animationDuration1, animations: { [weak self] in
            self?.center = (self?.center.applying(CGAffineTransform.init(translationX: 0, y: height)))!
            }, completion: { (finished) in
                // remove the view and put back the floating menu
                self.removeFromSuperview()
                self.displayFloatingMenu()
                superView.isUserInteractionEnabled = true
        })

    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    @IBAction func ViewMapClickHandler(_ sender: UIButton) {
        onViewMapClick?()
        cancelBtnClicked()
    }
    @IBAction func ViewPageClickHandler(_ sender: UIButton) {
        onViewPageClick?()
        cancelBtnClicked()
    }
    
    
    func setupHooks(){
        
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] responsibleEnt in
                if(responsibleEnt == nil){
                    //TODO: should do somting ????
                }else{
                    
                    self.responsibileEntity = responsibleEnt
                    self.setupUiData()
                    // self.disposeBag = DisposeBag()
                }
            })
            .disposed(by: disposeBag)
    }
    
    func setupUiData(){
        self.responsibleEntityTitle.text = self.responsibileEntity?.name
        self.resEntityDesLabel.text = self.responsibileEntity?.description
        let url = URL(string: (self.responsibileEntity?.icon!)!)
        if url != nil{
            self.responsibleEntityImage.sd_setImage(with: url, completed: nil)
        }
        self.addressLabel?.text = self.responsibileEntity?.address?.description!
        self.hoursLabel?.text = self.responsibileEntity?.officeHours?.times!
        self.daysLabel?.text = self.responsibileEntity?.officeHours?.days!
        self.serviceDays?.text = self.responsibileEntity?.publicServiceHours?.days!
        self.serviceHours?.text = self.responsibileEntity?.publicServiceHours?.times!
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.scrollView.resizeToFitContent()
    }
}
