//
//  MyServicesTableCell.swift
//  TammUI
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

class AspectsOfLifeTableCell:UITableViewCell{
    
    
    @IBOutlet weak var contentCellView: UIView!
    @IBOutlet var view: UIView!
    @IBOutlet weak var icon: UICircularImage!
    @IBOutlet weak var header: LocalizedLabel!
    @IBOutlet weak var descriptionTxt: LocalizedLabel!
    var viewData: Any?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        commonInit()
    }
    
    
    override func awakeFromNib() {
        commonInit()
    }
    
    private func commonInit(){
//        Bundle.main.loadNibNamed("AspectsOfLifeTableCellView", owner: self, options: nil)
//        let view = self
//        addSubview(view)
//        setConstraints(view: self, childView: view)
//        view.frame = self.bounds

        
        self.autoresizingMask = [.flexibleWidth]
        self.backgroundColor = UIColor.clear
        self.icon.FontImageColor = .white
        //self.header.adjustsFontSizeToFitWidth = true
        self.contentCellView.layer.shadowColor = UIColor.lightGray.cgColor;
        self.contentCellView.layer.shadowOpacity=0.5;
        self.contentCellView.layer.shadowRadius=2.0;
        self.contentCellView.layer.shadowOffset = CGSize(width:0, height:0);
        self.contentCellView.layer.masksToBounds = false;
        self.contentCellView.addRoundCorners(radious: Dimensions.aspectOfLifeCellCornerRadious)

        
    }
    

}
