//
//  DocumentEmailView.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/26/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class DocumentEmailView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var toField: LocalizedTextField!
    @IBOutlet weak var subjectField: LocalizedTextField!
    @IBOutlet weak var errorLabel: LocalizedLabel!
    @IBOutlet weak var cancelButton: LocalizedButton!
    @IBOutlet weak var sendButton: LocalizedButton!
    
    var closeClosure:(()->Void)!
    var sendClosure:((_ :String,_ :String)->Void)!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("DocumentEmailView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        errorLabel.text = ""
        toField.becomeFirstResponder()
        
    }
    
    @IBAction func cancelAction(_ sender: LocalizedButton) {
        self.removeFromSuperview()
        closeClosure()
    }
    @IBAction func okAction(_ sender: LocalizedButton) {
        
        errorLabel.text = ""
        
        if toField.validateEmail() && (subjectField.text?.count)! > 0{
            sendClosure(toField.text!,subjectField.text!)
            self.removeFromSuperview()
        }else if !toField.validateEmail(){
            errorLabel.text = L10n.emailIsNotValid
        }else if (subjectField.text?.count)! == 0 {
            errorLabel.text = L10n.subjectFieldIsNotEmpty
        }
        
    }
    
}
