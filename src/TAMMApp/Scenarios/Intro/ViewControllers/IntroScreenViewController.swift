//
//  LastIntroScreenViewController.swift
//  TAMMApp
//
//  Created by kerolos on 5/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

protocol IntroScreenViewControllerDelegate: class {
    func dismissIntro()
}

class IntroScreenViewController: TammViewController, IntroScreensStoryboardLoadable{
    weak var delegate: IntroScreenViewControllerDelegate?
    var viewModel: IntroScreenViewModel!
    
    
    @IBOutlet weak var skip: LocalizedButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: LocalizedLabel!
    @IBOutlet weak var getStartedButton: UITammResizableButton!
    
    @IBOutlet weak var getStartedLabel: LocalizedLabel!
    @IBOutlet weak var stackView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getStartedButton.titleLabel?.text = L10n.get_started
        getStartedLabel.text = L10n.get_started

        getStartedButton.isHidden = !viewModel.viewData.showGetStartedButton
        getStartedLabel.isHidden = !viewModel.viewData.showGetStartedButton

        
        stackView.isHidden = viewModel.viewData.showGetStartedButton
        
        imageView.image = viewModel.viewData.image
        titleLabel.text = viewModel.viewData.title
        textLabel.text = viewModel.viewData.text
        
        for i in 0..<viewModel.pageCount{
            if(i != viewModel.pageNumber){
               stackView.addArrangedSubview(UIImageView(image: #imageLiteral(resourceName: "dot_intro")))
            }else{
               stackView.addArrangedSubview(UIImageView(image: #imageLiteral(resourceName: "dash_intro")))
            }
        }
        
    }
    
    @IBAction func skipPressed(_ sender: LocalizedButton) {
        delegate?.dismissIntro()
    }
    
    @IBAction func getStartedPressed(_ sender: Any) {
        delegate?.dismissIntro()
    }
    
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        getStartedButton.roundCorners(radius: Dimensions.resizedRoundCornerRadiousForIntroGetStarted)
        textLabel.setNeedsLayout()
        titleLabel.setNeedsLayout()
        getStartedButton.setNeedsLayout()
        textLabel.layoutIfNeeded()
        titleLabel.layoutIfNeeded()
        getStartedButton.layoutIfNeeded()
    }
    
    
    func dismissIntroPressed(){
        delegate?.dismissIntro()
    }
}
