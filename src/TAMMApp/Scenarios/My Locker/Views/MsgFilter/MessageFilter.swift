//
//  MessageFilter.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol MessageFilterDelegate {
    //implemented by View Controller
    func applyFilterClicked(applyFilterOn:String)
}


class MessageFilterOverLay: UIView {

    var selectedOption:String?
    var filterOptions:[MsgStatus]?
    var delegate:MessageFilterDelegate?
    
    var categories: [PickerItem] = [PickerItem(id: 0,displayName: L10n.Individual), PickerItem(id: 1,displayName: L10n.Business), PickerItem(id: 2,displayName: L10n.Tourist)]
    
    var aols: [PickerItem] = [PickerItem]()
    
    var selectedCategoryId = -1
    var selectedAolId = -1
    
    
    @IBOutlet weak var categoryArrowLabel: UILabel!

    
    @IBOutlet weak var categoryTextField: UITextField!

    
    @IBOutlet weak var categoriesView: UIView!

    
    
    
    private var viewModel:GovernmentEntityViewModel!
    private let cellIdentifier = "FilterTableCell"
    
    @IBOutlet weak var applyFiltersBtn: LocalizedButton!
    var disposeBag = DisposeBag()
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    var onApplyFilterClick: ((String,String)->Void)?
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    init(frame: CGRect,availableStatuses:[MsgStatus], selectedOption:String?){
        super.init(frame: frame)
        self.filterOptions = availableStatuses
        if selectedOption == nil || selectedOption == ""{
            self.selectedOption = L10n.MyMessages.all
        }else{
            self.selectedOption = selectedOption
        }
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "MessageFilterView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        
        setupUiData()
    }
    
    
    @objc func cancelBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    

    
    func setupUiData(){
        //category drop down
        self.categoryArrowLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.categoriesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCategoryDropDownClick)))
        self.categoriesView.addRoundAllCorners(radious: 5)
        self.categoriesView.layer.borderWidth = 1
        self.categoriesView.layer.borderColor = UIColor.cloudyBlue.cgColor
        self.categoryTextField.attributedPlaceholder =  NSAttributedString(string: selectedOption!,  attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkBlueGrey])
        
        //aspects of life drop down

        //apply btn
        self.applyFiltersBtn.addRoundCorners(radious: 23)
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
    }
    
    
    @IBAction func ApplyFilterClickHandler(_ sender: LocalizedButton) {
        
        self.delegate?.applyFilterClicked(applyFilterOn: categoryTextField.text!)
        self.cancelBtnClicked()
    }
    
    
    // Mark:  categories
    @objc func onCategoryDropDownClick(){
        self.openCategoryPickerView()
    }
    
    
    private func preparePickerList() -> [PickerItem] {
        var pickerItems:[PickerItem] = [PickerItem]()
        pickerItems.append(PickerItem(id: -1, displayName: L10n.MyMessages.all))
        for item in filterOptions! {
            pickerItems.append(PickerItem(id: item.id!, displayName: item.name!))
        }
        
        return pickerItems
    }
    
    private func openCategoryPickerView(){
        let picker = PickerView()
        picker.items = preparePickerList()
        var selectedIndex:Int?
        for item in picker.items {
            if item.displayName == selectedOption {
                
                selectedIndex = item.id as? Int
            }
        }
        
        if selectedIndex != nil {
        
            picker.selectRow(selectedIndex!, inComponent: 0, animated: false)
       }
       
        
        self.categoryTextField.inputView = picker
        picker.viewDelegate = self
        self.categoryTextField.becomeFirstResponder()
    }
    
    
 
    
    
  
    
    
    
    //dismiss keyboard
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
}
extension MessageFilterOverLay: PickerViewDelegate{
    func itemSelected(item: PickerItemType, pickerView: PickerView) {
            self.categoryTextField.text = item.displayName
            self.selectedCategoryId = item.id as! Int
    }
    
    func clickOutsidePicker() {
        self.categoryTextField.resignFirstResponder()
    }
    
    
}
