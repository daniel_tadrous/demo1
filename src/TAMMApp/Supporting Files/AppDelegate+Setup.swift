//
//  AppDelegate+Setup.swift
//  iOSSampleApp
//
//  Created by Igor Kulman on 03/10/2017.
//  Copyright © 2017 Igor Kulman. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
//import GoogleMaps
//import GooglePlacePicker

extension AppDelegate {


    static func setupDependencies() {
       
        // services


        container.autoregister(AuthenticationService.self, initializer: SmartPassOpenIDAuthenticationService.init) .inObjectScope(ObjectScope.container)
        
        container.autoregister(ApiServiceAspectsOfLifeType.self, initializer: ApiServiceAspectsOfLife.init)
        
        container.autoregister(ApiSupportServicesType.self, initializer: SupportServices.init)
        
        container.autoregister(UserConfigServiceType.self, initializer: UserConfigService.init)
        
        container.autoregister(EmailServiceType.self, initializer: EmailService.init)
        
        container.autoregister(PhoneCallServiceType.self, initializer: PhoneCallService.init)
        
        container.autoregister(ApiMessageServicesTypes.self, initializer: ApiMessageServices.init)
        
        container.autoregister(SpeechRecognitionServiceType.self, initializer: CapturedSpeechToTextService.init)

        container.autoregister(ApiMyCommunityTypes.self, initializer: ApiMyCommunity.init)
        
        container.autoregister(FeedbackServiceType.self, initializer: FeedbackService.init)
        
        container.autoregister(ApiEntityServices.self, initializer: ApiEntityServicesType.init)
        
        container.autoregister(ApiNewsServiceType.self, initializer: ApiNewsService.init)
        container.autoregister(GovernmentEntityInformationServiceType.self, initializer: GovernmentEntityInformationService.init)
      
        container.autoregister(ApiMyEventsServiceType.self, initializer: MyEventsServices.init)
        
        container.autoregister(ApiMediaCenterServiceType.self, initializer: MediaCenterServices.init)
        
        container.autoregister(ApiFactsAndFiguresType.self, initializer: ApiFactsAndFigures.init)
        
        container.autoregister(LocationServiceType.self, initializer: LocationService.init)
        
        container.autoregister(ApiInitiativesServiceType.self, initializer: InitiativeServices.init)
        container.autoregister(ApiSettingsServicesTypes.self, initializer:ApiSettings.init)
        
        container.autoregister(ApiEnablePushNotificationServiceType.self, initializer:ApiEnablePushNotificationService.init)
        container.autoregister(MyDocumentServiceType.self, initializer:MyDocumentService.init)
        
        container.autoregister(MyPaymentsServiceType.self, initializer:MyPaymentService.init)
        container.autoregister(ApiGlobalSearchType.self, initializer:ApiGeneral.init)
        
        // viewmodels
        container.autoregister(AuthenticationViewModel.self, initializer: AuthenticationViewModel.init)

        container.autoregister(AolDetailsViewModel.self, initializer: AolDetailsViewModel.init)
        
         container.autoregister(AoIDetailsParentViewModel.self, initializer: AoIDetailsParentViewModel.init)
        
        container.autoregister(AolTopicDetailsViewModel.self, initializer: AolTopicDetailsViewModel.init)
        // needs ApiServiceAspectsOfLifeType for initialization 
        container.autoregister(MyServicesViewModel.self, initializer: MyServicesViewModel.init)
        
        container.autoregister(MyJourneysViewModel.self, initializer: MyJourneysViewModel.init)
        
        container.autoregister(MySupportMainViewModel.self, initializer: MySupportMainViewModel.init)
        
        container.autoregister(TalkToUsLandingViewModel.self, initializer: TalkToUsLandingViewModel.init)
        
        container.autoregister(TalkToUsViewModel.self, initializer: TalkToUsViewModel.init)
        
        container.autoregister(TalkToUsDraftsViewModel.self, initializer: TalkToUsDraftsViewModel.init)
        
        container.autoregister(TalkToUsTabBarViewModel.self, initializer: TalkToUsTabBarViewModel.init)
        
        container.autoregister(IncidentConfirmationViewModel.self, initializer: IncidentConfirmationViewModel.init)
        
        container.autoregister(AolServiceWithFaqsViewModel.self, initializer: AolServiceWithFaqsViewModel.init)
        
        container.autoregister(ResponsibleEntityViewModel.self, initializer: ResponsibleEntityViewModel.init)
        container.autoregister(ProcessViewModel.self, initializer: ProcessViewModel.init)
        
        container.autoregister(IntroScreenViewModel.self, initializer: IntroScreenViewModel.init)
        container.autoregister(FAQViewModel.self, initializer: FAQViewModel.init)
        container.autoregister(PollsViewModel.self, initializer: PollsViewModel.init)
        container.autoregister(CoCreateViewModel.self, initializer: CoCreateViewModel.init)
        container.autoregister(EventsViewModel.self, initializer: EventsViewModel.init)
        container.autoregister(IntroLanguageSwitchViewModel.self, initializer: IntroLanguageSwitchViewModel.init)
        
        container.autoregister(FeedbackViewModel.self, initializer: FeedbackViewModel.init)
        container.autoregister(MessagesViewModel.self, initializer: MessagesViewModel.init)
        container.autoregister(GovernmentEntityViewModel.self, initializer: GovernmentEntityViewModel.init)
        
        
        container.autoregister(MyEventsViewModel.self, initializer: MyEventsViewModel.init)
        container.autoregister(PublicHolidaysViewModel.self, initializer: PublicHolidaysViewModel.init)
        container.autoregister(EntitiesInformationViewModel.self, initializer: EntitiesInformationViewModel.init)
        container.autoregister(MediaCenterViewModel.self, initializer: MediaCenterViewModel.init)
        
        container.autoregister(EntitiesDetailsViewModel.self, initializer: EntitiesDetailsViewModel.init)
        
        container.autoregister(NewsViewModel.self, initializer: NewsViewModel.init)
        
        container.autoregister(PreferencesViewModel.self, initializer: PreferencesViewModel.init)
        
        container.autoregister(ProfileViewModel.self, initializer: ProfileViewModel.init)
        container.autoregister(FactsAndFiguresViewModel.self, initializer: FactsAndFiguresViewModel.init)
        
        container.autoregister(LanguageSwitchViewModel.self, initializer: LanguageSwitchViewModel.init)

        container.autoregister(InitiativeViewModel.self, initializer: InitiativeViewModel.init)

        container.autoregister(AccessibilityViewModel.self, initializer: AccessibilityViewModel.init)
        
        container.autoregister(AboutViewModel.self, initializer: AboutViewModel.init)

        container.autoregister(EnablePushNotificationViewModel.self, initializer: EnablePushNotificationViewModel.init)
        
        container.autoregister(SetReminderViewModel.self, initializer: SetReminderViewModel.init)
        

        container.autoregister(MediaSettingsViewModel.self, initializer: MediaSettingsViewModel.init)

        container.autoregister(PressReleasesViewModel.self, initializer: PressReleasesViewModel.init)

        container.autoregister(ManageViewModel.self, initializer: ManageViewModel.init)
        

        container.autoregister(CaseDetailsViewModel.self, initializer: CaseDetailsViewModel.init)
        container.autoregister(MyDocumentsViewModel.self, initializer: MyDocumentsViewModel.init)
        container.autoregister(ManageMediaViewModel.self, initializer: ManageMediaViewModel.init)
        
        container.autoregister(MyPaymentViewModel.self, initializer: MyPaymentViewModel.init)
        
        // view controllers
        container.storyboardInitCompleted(LoginViewController.self) { r, c
            in c.viewModel = r.resolve(AuthenticationViewModel.self)
        }
        

        container.storyboardInitCompleted(AolDetailsViewController.self) { r, c
            in c.viewModel = r.resolve(AolDetailsViewModel.self)
        }
        
        container.storyboardInitCompleted(AoIDetailsParentViewController.self) { r, c
            in c.viewModel = r.resolve(AoIDetailsParentViewModel.self)
        }
        
        container.storyboardInitCompleted(AolSubTopicsViewController.self) { r, c
            in c.viewModel = r.resolve(AolTopicDetailsViewModel.self)
        }
        container.storyboardInitCompleted(AolServiceWithFaqsViewController.self) { r, c
            in c.viewModel = r.resolve(AolServiceWithFaqsViewModel.self)
            c.responsibleEntityViewModel = r.resolve(ResponsibleEntityViewModel.self)
            c.processViewModel = r.resolve(ProcessViewModel.self)
        }
        container.storyboardInitCompleted(MyLockerViewController.self) { r, c
            in
//            c.viewModel = r.resolve(MyServicesViewModel.self)
        }
        container.storyboardInitCompleted(SideMenuViewController.self) { r, c
            in
            //            c.viewModel = r.resolve(MyServicesViewModel.self)
        }
        
        container.storyboardInitCompleted(JourneysViewController.self) { (r, c) in
            c.viewModel = r.resolve(MyJourneysViewModel.self)
        }
        
        container.storyboardInitCompleted(AspectsOfLifeListViewController.self) { r, c in
            c.viewModel = r.resolve(MyServicesViewModel.self)
        }
        
        container.storyboardInitCompleted(MySupportMainViewController.self) { r, c in
            c.viewModel = r.resolve(MySupportMainViewModel.self)
        }
        
        container.storyboardInitCompleted(TalkToUsLandingViewController.self) { r, c in
            c.viewModel = r.resolve(TalkToUsLandingViewModel.self)
        }
        
        container.storyboardInitCompleted(TalkToUsViewController.self) { r, c in
            c.viewModel = r.resolve(TalkToUsViewModel.self)
        }
        
        container.storyboardInitCompleted(TalkToUsDraftsViewController.self) { r, c in
            c.viewModel = r.resolve(TalkToUsDraftsViewModel.self)
        }
        
        container.storyboardInitCompleted(TalkToUsTabBarViewController.self) { r, c in
            c.viewModel = r.resolve(TalkToUsTabBarViewModel.self)
        }
        
        container.storyboardInitCompleted(IncidentConfirmationViewController.self) { r, c in
            c.viewModel = r.resolve(IncidentConfirmationViewModel.self)
        }
        
        container.storyboardInitCompleted(FAQViewController.self) { r, c in
            c.viewModel = r.resolve(FAQViewModel.self)
        }
        container.storyboardInitCompleted(PollsViewController.self) { r, c in
            c.viewModel = r.resolve(PollsViewModel.self)
        }
        container.storyboardInitCompleted(EventsViewController.self) { r, c in
            c.viewModel = r.resolve(EventsViewModel.self)
        }
        container.storyboardInitCompleted(CoCreateViewController.self) { r, c in
            c.viewModel = r.resolve(CoCreateViewModel.self)
        }
        container.storyboardInitCompleted(CheckStatusViewController.self) { r, c in
//            c.viewModel = r.resolve(MySupportMainViewModel.self)
        }
        
        container.storyboardInitCompleted(IntroScreenViewController.self) { r, c in
            c.viewModel = r.resolve(IntroScreenViewModel.self)
        }
        
        container.storyboardInitCompleted(IntroPagingViewController.self) { r, c in
        }
        container.storyboardInitCompleted(IntroLanguageSwitchViewController.self) { (r, c) in
            c.viewModel = r.resolve(IntroLanguageSwitchViewModel.self)
        }
        
        container.storyboardInitCompleted(OfflineScreenViewController.self) { r, c
            in
        }
        
        container.storyboardInitCompleted(EmptyLoadingScreenViewController.self) { r, c
            in
        }
        
        
        container.storyboardInitCompleted(LocationPickerEsriMapViewController.self) { r, c
            in
            c.micRecognizerService = r.resolve(SpeechRecognitionServiceType.self)
        }
        
        container.storyboardInitCompleted(MessagesViewController.self) { (r, c) in
            c.viewModel = r.resolve(MessagesViewModel.self)
        }
        container.storyboardInitCompleted(GovernmentEntitiesViewController.self) { r, c
            in c.viewModel = r.resolve(GovernmentEntityViewModel.self)
        }
        
        
        container.storyboardInitCompleted(MyEventsViewController.self) { r, c
            in c.viewModel = r.resolve(MyEventsViewModel.self)
        }
        container.storyboardInitCompleted(PublicHolidaysViewController.self) { r, c
            in c.viewModel = r.resolve(PublicHolidaysViewModel.self)
        }
        
        container.storyboardInitCompleted(EntitiesInformationViewController.self) { r, c
            in c.viewModel = r.resolve(EntitiesInformationViewModel.self)
        }
        container.storyboardInitCompleted(MediaCenterViewController.self) { r, c
            in c.viewModel = r.resolve(MediaCenterViewModel.self)
        }
        
        
        container.storyboardInitCompleted(EntityServicesViewController.self) {r , c in
              c.viewModel = r.resolve(EntitiesDetailsViewModel.self)
        }
        
        container.storyboardInitCompleted(NewsViewController.self) {r , c in
            c.viewModel = r.resolve(NewsViewModel.self)
        }
        
        
        
        
    container.storyboardInitCompleted(PreferencesViewController.self) {r , c in
            c.viewModel = r.resolve(PreferencesViewModel.self)
    }
        
        
        container.storyboardInitCompleted(ProfileViewController.self) {r , c in
            c.viewModel = r.resolve(ProfileViewModel.self)
        }
        
        container.storyboardInitCompleted(FactsAndFiguresViewController.self) {r , c in
            c.viewModel = r.resolve(FactsAndFiguresViewModel.self)
        }

        container.storyboardInitCompleted(LanguageSwitchViewController.self) {r , c in
            c.viewModel = r.resolve(LanguageSwitchViewModel.self)
        }

        container.storyboardInitCompleted(InitiativeViewController.self) {r , c in
            c.viewModel = r.resolve(InitiativeViewModel.self)
        }
        container.storyboardInitCompleted(AccessibilityViewController.self) {r , c in
            c.viewModel = r.resolve(AccessibilityViewModel.self)
        }
        container.storyboardInitCompleted(AboutViewController.self) {r , c in
            c.viewModel = r.resolve(AboutViewModel.self)
        }
        
        container.storyboardInitCompleted(EnablePushNotificationViewController.self) {r , c in
            c.viewModel = r.resolve(EnablePushNotificationViewModel.self)
        }
        container.storyboardInitCompleted(SetReminderViewController.self) {r , c in
            c.viewModel = r.resolve(SetReminderViewModel.self)
        }

        container.storyboardInitCompleted(MediaSettingsViewController.self) {r , c in
            c.viewModel = r.resolve(MediaSettingsViewModel.self)
        }
        
        container.storyboardInitCompleted(PressReleasesViewController.self) { r, c
            in c.viewModel = r.resolve(PressReleasesViewModel.self)
        }
        container.storyboardInitCompleted(HtmlViewController.self) { r, c
            in
        }
        
        container.storyboardInitCompleted(ManageViewController.self) { r, c
            in c.viewModel = r.resolve(ManageViewModel.self)
        }
        
        container.storyboardInitCompleted(MsgsParentViewController.self) {r , c in
           
        }
        container.storyboardInitCompleted(CaseDetailsViewController.self) { r, c
            in
            c.viewModel = r.resolve(CaseDetailsViewModel.self)
        }
        container.storyboardInitCompleted(ActiveDocumentsViewController.self) { r, c
            in c.viewModel = r.resolve(MyDocumentsViewModel.self)
        }
        
        container.storyboardInitCompleted(ManageMediaViewController.self) { r, c
            in c.viewModel = r.resolve(ManageMediaViewModel.self)
        }
        
        container.storyboardInitCompleted(MyPaymentsParentViewController.self) { r, c
            in 
        }
        
        container.storyboardInitCompleted(MapParentViewController.self) { r, c
            in
        }
        
        container.storyboardInitCompleted(OfflineMapViewController.self) { r, c
            in
        }
        
        container.storyboardInitCompleted(MyPaymentViewController.self) { (r, c) in
            c.viewModel = r.resolve(MyPaymentViewModel.self)
        }
        
        
        #if DEBUG
            if ProcessInfo().arguments.contains("testMode") {
               
            }
        #endif
    }
    
//    func setupGoogleMaps(){
//        let APIKey = "AIzaSyCdatx0U2EF7qo2u5DfFRhrhQeBbzSaleA"
//        GMSServices.provideAPIKey(APIKey)
//        GMSPlacesClient.provideAPIKey(APIKey)
//    }
    
}
