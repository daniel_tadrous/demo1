//
//  MessageType.swift
//  TAMMApp
//
//  Created by kerolos on 4/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageTypeModel: Mappable{
    
    
    var id: Int?
    var text: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id                      <- map["id"]
        text                    <- map["text"]
    }
    
}
