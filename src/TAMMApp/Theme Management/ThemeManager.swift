//
//  ThemeManager.swift
//  TAMMApp
//
//  Created by Marina.Riad on 3/27/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import Foundation
import UIKit
enum ThemeType:String {
    case Blue = "BlueTheme"
    case Green = "GreenTheme"
}

enum ThemeProperty:String {
    case DefaultButtonFontColor = "DefaultButtonFontColor"
    
    case InvertedButtonFontColor = "InvertedButtonFontColor"
    
    case DefaultBackgroundColor = "DefaultBackgroundColor"
    
    case InvertedBackgroundColor = "InvertedBackgroundColor"
    
    case DefaultButtonColor = "DefaultButtonColor"
    
    case InvertedButtonColor = "InvertedButtonColor"
    
    case DefaultLabelFontColor = "DefaultLabelFontColor"
    
    case InvertedLabelFontColor = "InvertedLabelFontColor"
    
    case DefaultButtonFontSize = "DefaultButtonFontSize"
    
    case DefaultLabelFontSize = "DefaultLabelFontSize"

}

class ThemeManager {
    static var currentDisplayedTheme:Theme?
    
    private let defaultThemeType:ThemeType = .Blue;
    
    private let userDefaultThemeKey:String = "Theme";
    
    
    
    // change the currentDisplayedTheme
    func applyTheme(themeType:ThemeType){
        var myDict: NSDictionary?
        if let path = Bundle.main.path(forResource: themeType.rawValue, ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        if let dict = myDict {
            ThemeManager.currentDisplayedTheme = BasicTheme(values:dict, themeType: themeType);
        }
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(ThemeManager.currentDisplayedTheme, forKey: self.userDefaultThemeKey)
        userDefaults.synchronize()
    }
    
    func applyStoredTheme(){
        let userDefaults = UserDefaults.standard
        if let theme = userDefaults.value(forKey:self.userDefaultThemeKey) {
            ThemeManager.currentDisplayedTheme = theme as? Theme
        }
        else {
            // not found so apply the default
            self.applyDefaultTheme();
        }

    }
    
    func applyDefaultTheme(){
        self.applyTheme(themeType: self.defaultThemeType)
    }
    
    func invertColor(inverted:Bool){
        ThemeManager.currentDisplayedTheme?.invertColor(inverted: inverted)
    }
    
    func changeFontSize(value:Double){
        ThemeManager.currentDisplayedTheme?.changeFontSize(value: value)
    }
}

protocol Theme {
    var themeType:ThemeType{ get  }
    
    var buttonColor:UIColor{ get  }
    
    var backgroundColor:UIColor{ get }
    
    var buttonFontColor:UIColor{ get }
    
    var labelFontColor:UIColor{ get }
    
    var buttonFontSize:Double{ get }
    
    var labelFontSize:Double{ get }
    
    var defaultBackgroundColor:UIColor{ get }
    
    var invertedBackgroundColor:UIColor{ get }
    
    var defaultButtonBackgroundColor:UIColor{ get }
    
    var invertedButtonBackgroundColor:UIColor{ get }
    
    var defaultbButtonFontColor:UIColor{ get }
    
    var invertedButtonFontColor:UIColor{ get }
    
    var defaultLabelFontColor:UIColor{ get }
    
    var invertedLabelFontColor:UIColor{ get }
    
    var defaultButtonFontSize:Double{ get }
    
    var defaultLabelFontSize:Double{ get }
    
    func invertColor(inverted:Bool);
    
    func changeFontSize(value:Double);
}


class BasicTheme:Theme {
    
    var themeType: ThemeType
    /// Current
    
    var labelFontSize: Double
    
    var labelFontColor: UIColor
    
    var buttonFontColor: UIColor
    
    var buttonColor: UIColor
    
    var backgroundColor: UIColor
    
    var buttonFontSize: Double
    
    /// References
    var defaultBackgroundColor: UIColor
    
    var invertedBackgroundColor: UIColor
    
    var defaultButtonBackgroundColor: UIColor
    
    var invertedButtonBackgroundColor: UIColor
    
    var defaultbButtonFontColor: UIColor
    
    var invertedButtonFontColor: UIColor
    
    var defaultLabelFontColor: UIColor
    
    var invertedLabelFontColor: UIColor
    
    var defaultButtonFontSize: Double
    
    var defaultLabelFontSize: Double
    
    init(values:NSDictionary,themeType:ThemeType) {
        
        self.themeType = themeType
        
        defaultBackgroundColor = UIColor(hexString: (values[ThemeProperty.DefaultBackgroundColor.rawValue] as? String)!)
        
        invertedBackgroundColor = UIColor(hexString: (values[ThemeProperty.InvertedBackgroundColor.rawValue] as? String)!)
        
        defaultButtonBackgroundColor = UIColor(hexString: (values[ThemeProperty.DefaultButtonColor.rawValue] as? String)!)
        
        invertedButtonBackgroundColor = UIColor(hexString: (values[ThemeProperty.InvertedButtonColor.rawValue] as? String)!)
        
        defaultbButtonFontColor = UIColor(hexString: (values[ThemeProperty.DefaultButtonFontColor.rawValue] as? String)!)
        
        invertedButtonFontColor = UIColor(hexString: (values[ThemeProperty.InvertedButtonFontColor.rawValue] as? String)!)
        
        defaultLabelFontColor = UIColor(hexString: (values[ThemeProperty.DefaultLabelFontColor.rawValue] as? String)!)
        
        invertedLabelFontColor = UIColor(hexString: (values[ThemeProperty.InvertedLabelFontColor.rawValue] as? String)!)
        
        defaultLabelFontSize = (values[ThemeProperty.DefaultLabelFontSize.rawValue] as? Double)!
        
        defaultButtonFontSize = (values[ThemeProperty.DefaultButtonFontSize.rawValue] as? Double)!
        
        // setup the theme
        
        backgroundColor = defaultBackgroundColor
        buttonColor = defaultButtonBackgroundColor
        buttonFontColor = defaultbButtonFontColor
        labelFontColor = defaultLabelFontColor
        labelFontSize = defaultLabelFontSize
        buttonFontSize = defaultButtonFontSize
        
    }
    func invertColor(inverted:Bool){
        if(inverted){
            backgroundColor = invertedBackgroundColor;
            buttonColor = invertedButtonBackgroundColor;
            buttonFontColor = invertedButtonFontColor
            labelFontColor = invertedLabelFontColor
        }
        else{
            backgroundColor = defaultBackgroundColor
            buttonColor = defaultButtonBackgroundColor
            buttonFontColor = defaultbButtonFontColor
            labelFontColor = defaultLabelFontColor
        }
    }
    
    func changeFontSize(value: Double) {
        buttonFontSize += value;
        labelFontSize += value;
    }
}



extension UIColor{
    open class var labelFontColor: UIColor{
        return (ThemeManager.currentDisplayedTheme?.labelFontColor)!
    }
    open class var buttonFontColor: UIColor{
        return (ThemeManager.currentDisplayedTheme?.buttonFontColor)!
    }
    
    open class var buttonColor: UIColor{
        return (ThemeManager.currentDisplayedTheme?.buttonColor)!
    }
    
    open class var backgroundColor: UIColor{
        return (ThemeManager.currentDisplayedTheme?.backgroundColor)!
    }
    convenience init(hexString: String) {
        
        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner          = Scanner(string: hexString as String)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:1)
    }


}

extension UIFont{
    open var ButtonFontSize: CGFloat{
        return CGFloat((ThemeManager.currentDisplayedTheme?.buttonFontSize)!)
    }
    
    open var LabelFontSize: CGFloat{
        return CGFloat((ThemeManager.currentDisplayedTheme?.labelFontSize)!)
    }
}


