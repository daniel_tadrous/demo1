//
//  InitiativeViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import RxSwift
import EventKit

class InitiativeViewModel:SeacrhViewModel , SearchViewModelDelegate {
    
    private var apiService: ApiInitiativesServiceType!
    
    private var locationService: LocationServiceType!
    var shareViewModel = ShareViewModel()
    
    var filterObject = Variable(EventsFilterApplyModel())
    
    var searchedText:Variable<String?> = Variable(nil)
    var mySearchViewModel:SeacrhViewModel?
    var isToGetTrending: Variable<Bool?> = Variable(nil)
    var trendingVar: Variable<[String]?> = Variable(nil)
    var queryResultVar: Variable<[String]?> = Variable(nil)
    var currentQueryVar: Variable<String?> = Variable(nil)
    
    private(set) var initiatives: Variable<[Event]?> = Variable(nil)
    
    private(set) var currentLocationViewData: Variable<CLLocation?> = Variable(nil)
    
    private(set) var categoriesViewData: Variable<[Event.Category]?> = Variable(nil)
    private(set) var locationsViewData: Variable<[String]?> = Variable(nil)
    private(set) var eventsFilterApplyModel: Variable<EventsFilterApplyModel?> = Variable(nil)
    private(set) var isToGetCategories: Variable<Bool?> = Variable(nil)
    private(set) var locationsQuery: Variable<String?> = Variable(nil)
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService:ApiInitiativesServiceType,locationService: LocationServiceType) {
        super.init()
        self.apiService = apiService
        self.locationService = locationService
        setupBinding()
    }
    
    func setupBinding() {
        
        eventsFilterApplyModel.asObservable()
            .subscribe(onNext: { [unowned self] events in
                self.getInitiatives()
            })
            .disposed(by:disposeBag)
        
        isToGetCategories.asObservable().subscribe(onNext:{ [unowned self] cats in
            self.getCategories()
        }).disposed(by:disposeBag)
        
        locationsQuery.asObservable().subscribe(onNext:{ [unowned self] locations in
            self.getLocations()
        }).disposed(by:disposeBag)
        
        
        isToGetTrending.asObservable().subscribe(onNext: { (go:Bool?) in
            if go != nil{
                self.getTrending()
            }
        }).disposed(by: disposeBag)
        
        currentQueryVar.asObservable().subscribe(onNext: { (descriptionText) in
            if descriptionText != nil {
                self.getQueryResults(q:descriptionText!)
            }
        }).disposed(by: disposeBag)
        
        searchedText.asObservable().subscribe(onNext: { [unowned self] (object) in
            self.filterObject.value.q = self.searchedText.value ?? ""
            self.getInitiatives()
        })
            .disposed(by: disposeBag)
        
    }
    
    fileprivate func getCategories(){
        if isToGetCategories.value != nil && isToGetCategories.value!{
            apiService.getFilterCategories()
                .subscribe(onNext: { [unowned self] cats in
                    self.categoriesViewData.value = cats
                }).disposed(by: disposeBag)
        }else{
            self.categoriesViewData.value  = nil
        }
    }
    fileprivate func getLocations(){
        if  (locationsQuery.value != nil) && !(locationsQuery.value?.isEmpty)!
        {
            apiService.getFilterLocations(q: locationsQuery.value!)
                .subscribe(onNext: { [unowned self] locations in
                    self.locationsViewData.value = locations
                }).disposed(by: disposeBag)
        }else{
            self.locationsViewData.value  = nil
        }
    }
    
    func getQueryResults(q: String) {
        apiService.searchFor(query: q, type: 1).asObservable().subscribe(onNext: { items in
            self.queryResultVar.value = items
            print("\(items)")
        }).disposed(by: disposeBag)
    }
    
    func getTrending() {
        apiService.searchFor(query: "", type: 0).asObservable().subscribe(onNext: { trendingItems in
            self.trendingVar.value = trendingItems
            print("\(trendingItems)")
        }).disposed(by: disposeBag)
    }
    
    func getInitiatives() {
        apiService.getInitiatives(filterObject:filterObject.value).subscribe(onNext: { (eventsRes) in
            self.initiatives.value = eventsRes
        })
            .disposed(by: disposeBag)
    }
    
    func findOutMore() {
        
        Toast(message: L10n.commingSoon).Show()
    }
}
extension InitiativeViewModel:filterViewModelDelegate{
    var apiServiceObject: ApiFilterServiceType {
        return apiService
    }
    
}
extension InitiativeViewModel: LocationServiceTypeDelegate{
    
    func onLocationUpdated(location: CLLocation) {
        self.currentLocationViewData.value = location
    }
}
extension InitiativeViewModel :FilterEventsViewModelProtocol{
    func setIsToGetCategories() {
        self.isToGetCategories.value = true
    }
    
    
    func setLocationQueryValue(q: String?) {
        self.locationsQuery.value = q
    }
    
    func getCategoriesVar() -> Variable<[Event.Category]?> {
        return self.categoriesViewData
    }
    
    func getLocationsVar() -> Variable<[String]?> {
        return self.locationsViewData
    }
    func startLocationService(){
        self.locationService.start(delegate: self)
    }
    func getCurrentLocation() -> Variable<CLLocation?> {
        return self.currentLocationViewData
    }
    
    
}
