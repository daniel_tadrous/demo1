//
//  MediaCenterEventsCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class MediaCenterEventsCell: UITableViewCell {
    
    @IBOutlet var eventViews: [MediaCenterEventsView]!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit() {
        selectionStyle = .none
        eventViews[0].addRoundCorners(radious: 10)
        eventViews[1].addRoundCorners(radious: 10)
        self.backgroundColor = UIColor.paleGreyTwo
      
    }

}
