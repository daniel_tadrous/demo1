//
//  SetReminderViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/7/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class SetReminderViewModel {
    
    var apiService:ApiEnablePushNotificationServiceType
    
    var remiderValue: Variable<Int?> = Variable(nil)
    
    let disposeBag = DisposeBag()
    
    init(apiService: ApiEnablePushNotificationServiceType) {
        self.apiService = apiService
        setUpBinding()
    }
    
    func setUpBinding() {
        remiderValue.asObservable().subscribe({ _ in
            self.setReminder()
        }).disposed(by: disposeBag)
    }
    
    fileprivate func setReminder() {
        if remiderValue.value != nil {
            // subscribe on response to do what 
             apiService.setPushNotificationStatus(isEnabled: true, Reminder: remiderValue.value!)
        }
       
    }
    
    
}
