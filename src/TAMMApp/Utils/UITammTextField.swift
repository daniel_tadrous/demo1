//
//  UITammTextField.swift
//  TammUI
//
//  Created by Marina.Riad on 4/11/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import UIKit
@IBDesignable
class UITammTextField: LocalizedTextField {
    fileprivate var _fontImage:UILabel!
    fileprivate var padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5);
    
    
    
    
    @IBInspectable public var FontImage: String = "" {
        didSet {
            _fontImage.text = FontImage
            self.updateView()
            
        }
    }
    @IBInspectable public var FontName: String? {
        didSet {
            _fontImage.font = UIFont(name: FontName ?? "System", size: CGFloat(FontSize))
            _fontImage.adjustsFontSizeToFitWidth = true
            self.updateView()
            
        }
    }
    @IBInspectable public var FontImageColor: UIColor = UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1) {
        didSet {
            _fontImage.textColor = FontImageColor
            
        }
    }
    @IBInspectable public var FontSize:Int = 30{
        didSet {
            _fontImage?.font = UIFont(name: FontName!, size: CGFloat(FontSize))
            
            _fontImage?.adjustsFontSizeToFitWidth = true
            self.updateView()
            
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        _fontImage = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: self.frame.height));
        //_fontImage.backgroundColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        _fontImage = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: self.frame.height));
        _fontImage.textAlignment = .center
    }
    
    fileprivate func updateView(){
        if(FontImage != ""){
            if(L102Language.currentAppleLanguage().contains("ar")){
                leftViewMode = UITextFieldViewMode.always
                leftView = _fontImage
                rightViewMode = UITextFieldViewMode.never
                rightView = nil
            }
            else{
                rightViewMode = UITextFieldViewMode.always
                leftViewMode = UITextFieldViewMode.never
                leftView = nil
                rightView = _fontImage
            }
        } else {
            leftViewMode = UITextFieldViewMode.never
            rightViewMode = UITextFieldViewMode.never
            rightView = nil
            leftView = nil
        }
        
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        if(L102Language.currentAppleLanguage().contains("ar")){
            var textRect = super.leftViewRect(forBounds: bounds)
            textRect.origin.y = 0
            return textRect
        }
        return CGRect.zero
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
         if(L102Language.currentAppleLanguage().contains("en")){
            var textRect = super.rightViewRect(forBounds: bounds)
            textRect.origin.y = 0
            return textRect
        }
        return CGRect.zero
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 20)
        }
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 20)
        }
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 20)
        }
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    func roundCorners() {
        if #available(iOS 11.0, *) {
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.init(hexString: "#E5E3E8").cgColor
            if(L102Language.currentAppleLanguage().contains("ar")){
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            self.layer.cornerRadius = 25
        } else {
            // Fallback on earlier versions
        }
        
    }
}

