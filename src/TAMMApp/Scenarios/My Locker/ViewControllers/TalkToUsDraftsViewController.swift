//
//  TalkToUsDraftsViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
protocol TalkToUsDraftsViewDelegate {
    func numberOfDrafts(number:Int)
}
protocol DraftsUpdatedDelegate {
    func onDraftsUpdated()
}
class TalkToUsDraftsViewController: TammViewController, TalkToUsDraftsStoryboardLodable, UITableViewDelegate , UITableViewDataSource, DraftsUpdatedDelegate{
    
    func onDraftsUpdated() {
        self.drafts = viewModel.getDrafts()
        self.tableView.reloadData()
    }
    
    var isFromOfflineScreen:Bool = false
    var viewModel:TalkToUsDraftsViewModel!
    //weak var delegate: AspectsOfLifeTableViewDelegate?
    public  var delegate:TalkToUsDraftsViewDelegate?
    public var drafts:[DraftViewModelData] = []
    fileprivate let defaultReuseIdentifier = "Drafts"
    fileprivate let defaultReuseIdentifierEdit = "Drafts_Edit"
    public let defaultCellHeight = 118
    static var itemStatic: DraftViewModelData!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ErrorView.show = !isFromOfflineScreen
        self.tableView?.allowsSelection = true
        self.tableView?.tableFooterView = UIView()
        self.drafts = viewModel.getDrafts()
        // self.tableView?.register(AspectsOfLifeTableCell.self, forCellReuseIdentifier: defaultReuseIdentifier)
        DiscardChangesView.draftsUpdatedDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(self.drafts.count > 0){
            delegate?.numberOfDrafts(number: self.drafts.count)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drafts.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(defaultCellHeight)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return initiateDefaultCell(indexPath: indexPath)
    }
    
    fileprivate func initiateDefaultCell(indexPath:IndexPath)->TalkToUsDraftsTableCell{
        let item = drafts[indexPath.row]
        var cell: TalkToUsDraftsTableCell
        if item.isOpened{
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifierEdit, for: indexPath) as! TalkToUsDraftsTableCell
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(openCloseGesture(_:)))
            
            swipeRight.direction = L10n.Lang == "en" ? .right : .left
            cell.addGestureRecognizer(swipeRight)
            cell.deleteIcon?.text = "\u{e009}"
            cell.onDeleteClick = {
                let confirmDeleteView = ConfirmationView(frame: UIScreen.main.bounds,okClickHandler:{
                    let item = self.drafts[indexPath.row]
                    self.viewModel.deleteDraftById(id: item.draftId!)
                    self.drafts.remove(at: indexPath.row)
                    self.tableView.reloadData()
                })
                UIApplication.shared.keyWindow!.addSubview(confirmDeleteView)
                UIApplication.shared.keyWindow!.bringSubview(toFront: confirmDeleteView)
            }
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifier, for: indexPath) as! TalkToUsDraftsTableCell
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(openCloseGesture(_:)))
            swipeLeft.direction =  L10n.Lang == "en" ? .left : .right
            cell.addGestureRecognizer(swipeLeft)
        }
        cell.draftView.typelbl.text = item.type
        cell.draftView.descriptionlbl.text = item.message.title
        cell.rowIndex = indexPath.row
        let calendar = NSCalendar.current
        var timeStr = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: L10n.Lang)
        if calendar.isDateInToday(item.time){
            dateFormatterGet.dateFormat = "HH:mm a"
            dateFormatterGet.timeStyle = .short
            timeStr = dateFormatterGet.string(from: item.time)
        }else{
            dateFormatterGet.dateFormat = "MMM d"
            timeStr = dateFormatterGet.string(from: item.time)
        }
        cell.draftView.timelbl.text = timeStr
        if item.message.hasMedia! {
            cell.draftView.attachementlbl.isHidden = false
        }else{
            cell.draftView.attachementlbl.isHidden = true
        }
        return cell
    }
    
    
    @objc func openCloseGesture(_ sender: Any){
        let index = ((sender as! UISwipeGestureRecognizer).view as! TalkToUsDraftsTableCell).rowIndex
        self.drafts[index!].isOpened = !self.drafts[index!].isOpened
        var animation:UITableViewRowAnimation
        if L10n.Lang == "en"{
            animation = self.drafts[index!].isOpened ? .left : .right
        }else{
            animation = self.drafts[index!].isOpened ? .right : .left
        }
        self.tableView.reloadRows(at: [IndexPath(row: index!, section: 0)], with: animation)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        TalkToUsDraftsViewController.itemStatic = drafts[indexPath.row]
        let parentViewController = self.parent as? TalkToUsTabBarViewController
        parentViewController?.btnNewClickedInEditMode(draftId: TalkToUsDraftsViewController.itemStatic.draftId!)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DiscardChangesView.draftsUpdatedDelegate = nil
    }
}
