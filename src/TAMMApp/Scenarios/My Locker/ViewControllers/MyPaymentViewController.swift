//
//  MyPaymentViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

enum PaymentType : String {
    case pending = "pending"
    case paid = "paid"
}

class MyPaymentViewController: TammViewController ,  MyPaymentStoryboardLoadable{
    
    var viewModel:MyPaymentViewModel!
    
    @IBOutlet weak var searchFilterSortStackView: UIStackView!
    //Views In payment View
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var paymentTableview: UITableView!
    @IBOutlet weak var searchFilterSortView: SearchFilterSort!
    var paymentResponse : PaymentResponseModel?
    var paymentsToShow = [PaymentTypeModel]()
    // Views In Nopayment View
    @IBOutlet weak var noPaymentView: UIView!
    @IBOutlet weak var iconLabel: IconLabel!
    @IBOutlet weak var circleView: RoundedView!
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    private let disposeBag = DisposeBag()
    
    private var firstTimeLoading:FirstTimeLoadingView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentView.isHidden = true
        noPaymentView.isHidden = true
        checkIsFirstTime()
        setupViews()
        addTapGesturseOnSearchView()
        setFilterAction()
        setSortAction()
        setupBinding()
       // paymentView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    
    fileprivate func checkIsFirstTime() {
        
        if viewModel.getisFirstTimeStatus() == true {
            // show first Time loading and set User Defaults
            firstTimeLoading = FirstTimeLoadingView(frame: UIScreen.main.bounds, text: "Please Wait While Loading Your Data")
            UIApplication.shared.keyWindow!.addSubview(firstTimeLoading!)
            UIApplication.shared.keyWindow!.bringSubview(toFront: firstTimeLoading!)
            viewModel.setIsFirstTimeStatus(value: false)
        }else {
            // do nothing
            
        }
    }
    
    
    fileprivate func addTapGesturseOnSearchView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(searchViewClicked))
        searchFilterSortView.searchView.addGestureRecognizer(tap)
    }
    
    fileprivate func setSortAction() {
        searchFilterSortView.sortAction = {
            
            let sortViewOverLay = SortView(frame: UIScreen.main.bounds, sortOptions: self.viewModel.getSortedOptions(), selectedOption: self.viewModel.getSortedText())
            sortViewOverLay.delegate = self
            UIApplication.shared.keyWindow!.addSubview(sortViewOverLay)
            UIApplication.shared.keyWindow!.bringSubview(toFront: sortViewOverLay)
        }
    }
    
    fileprivate func setFilterAction() {
        searchFilterSortView.filterAction = {
            Toast(message:"Filter").Show()
        }
    }
    
    @objc func searchViewClicked() {
        
//        let searchViewOverLay = SearchWithoutTrending(frame: UIScreen.main.bounds, text: self.searchFilterSortView.searchView.searchField.text!, currentSearchModel: self.viewModel.searchModel)
//        searchViewOverLay.delegate = self
//        UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
//        UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay)
        print("Search View Clicked")
        Toast(message:"Search").Show()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViewColors()
    }
    
    fileprivate func setupViewColors() {
        
        self.searchFilterSortView.sortBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        self.searchFilterSortView.filterBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        
        self.searchFilterSortView.searchView.searchField.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        self.searchFilterSortView.searchView.view.backgroundColor =  UIColor.white.getAdjustedColor()
        self.searchFilterSortView.searchView.searchIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        if searchFilterSortView.searchView.searchField.text == "" {
            searchFilterSortView.searchView.searchField.text = "Search"
        }
        self.searchFilterSortView.searchView.searchField.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()

        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        circleView.backgroundColor = UIColor.silver.getAdjustedColor()
        descriptionLabel.textColor = UIColor.greyPurple.getAdjustedColor()
        iconLabel.textColor = UIColor.greyPurple.getAdjustedColor()
    }
    
    func setupViews() {
        if viewModel.paymentType == PaymentType.paid {
            descriptionLabel.text = L10n.youDontHaveAnyPayment
            searchFilterSortStackView.isHidden = true
        } else {
            searchFilterSortView.searchView.isHidden = true
        }
    }

    fileprivate func setupBinding() {
        viewModel.paymentResponse.asObservable().subscribe(onNext: { paymentResponse in
            // remove loading Screen
            if let response = paymentResponse {
                self.firstTimeLoading?.dismiss()
                self.paymentResponse = response
                
                if self.paymentResponse?.payments?.count == 0 ||  response.payments == nil {
                    // show nothing to show
                    
//                    self.noPaymentView.isHidden = false
//                    self.paymentView.isHidden = true
                    
                    self.noPaymentView.isHidden = true
                    self.paymentView.isHidden = false
                    
                }else {
                    // show payment view
                    
                    self.noPaymentView.isHidden = true
                    self.paymentView.isHidden = false
                }
            }
            
        }).disposed(by: disposeBag)
    }

}


extension MyPaymentViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        return cell!
    }

}


extension MyPaymentViewController : SortViewDelegate {
    func applySortClicked(applySortOn:String) {
        paymentsToShow.removeAll()
        viewModel.sortByText.value = applySortOn
        viewModel.page.value = 1
    }
}
