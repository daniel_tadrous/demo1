//
//  MessagesViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import RxSwift

class CaseDetailsViewModel {
    static var idStatic:Int?
    var id : Variable<Int?> = Variable(nil)
    
    var msgId:Int?
    
    var mediaURLAdded : Variable<URL?> = Variable(nil)
    
    var msgCommentAdded : Variable<String?> = Variable(nil)
    
    private(set) var viewData: Variable<MessageModel?> = Variable(nil)
   
    fileprivate var apiService: ApiSupportServicesType!
    
    fileprivate var disposeBag = DisposeBag()
    var isApproved:Variable<Bool?> = Variable(nil)
    
    var isSubmitted:Variable<Bool?> = Variable(nil)
    
    var comment:Variable<String?> = Variable(nil)
    
    var answers:Variable<[Int]?> = Variable(nil)
    
    init(apiService: ApiSupportServicesType) {
        self.apiService = apiService
        setupBinding()
    }
    
    
   
    
    fileprivate func setupBinding(){
      
        id.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        
        
        msgCommentAdded.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.updateMsgComment()
            })
            .disposed(by:disposeBag)
        
        mediaURLAdded.asObservable().subscribe(onNext: { [unowned self] mediaURl in
            self.uploadMedia()
        }).disposed(by:disposeBag)
        
        isApproved.asObservable().subscribe(onNext:{ [unowned self] approved in
            self.approveCase()
        }).disposed(by: disposeBag)
        
        answers.asObservable().subscribe(onNext:{ [unowned self] answers in
            self.submitApproveCase()
        }).disposed(by: disposeBag)
        comment.asObservable().subscribe(onNext:{ [unowned self] comment in
            self.submitRejectCase()
        }).disposed(by: disposeBag)
        
    }
    
    fileprivate func updateMsgComment() {
        if msgCommentAdded.value != nil && msgId != nil {
            apiService.putMessage(msgId: String(msgId!), comment: msgCommentAdded.value!).subscribe(onNext: {
                response in
                print(response)
            } ).disposed(by: disposeBag)
        }
       
    }
    
    
    fileprivate func uploadMedia() {
         if mediaURLAdded.value != nil && msgId != nil {
            apiService.putMessage(msgId: String(msgId!), mediaURL: mediaURLAdded.value!).subscribe(onNext: {
                response in
                print(response)
            } ).disposed(by: disposeBag)
        }
    }
    
    fileprivate func requestDetails(){
        if id.value != nil{
            apiService.getMessageDetails(id: id.value!)
                .subscribe(onNext: { [unowned self] details in
                    self.viewData.value = details
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
    func downloadCaseDetailsAudio()-> Observable<URL?>  {
        
        return apiService.downloadFile(url: (self.viewData.value?.caseDetails?.url)!, forName: (self.viewData.value?.caseDetailsRecordFileName)!, fileFormat: "mp3")
    }
    func downloadAttachment(attachment: MsgAttachment,onComplete:@escaping (URL)->Void){
        let parts = attachment.url?.split(separator: ".")
        let format  = String(parts![parts!.count-1])
        apiService.downloadFile(url: attachment.url!, forName: attachment.attachmentFileName, fileFormat: format).asObservable().subscribe(onNext: { (fileUrl) in
            if fileUrl != nil{
                onComplete(fileUrl!)
            }
        }).disposed(by: disposeBag)
    }
    
    func getcurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let result = formatter.string(from: date)
        return result
    }
    
    func createAdditionalComment(comment:String) ->MsgAdditionalComments {
     let newObject = """
        {
        "text": "\(comment)",
        "date": "\(getcurrentDate())"
        }
        """
        return MsgAdditionalComments(JSONString: newObject)!
    }
    
    func createAdditionalAttachement(id:Int, name:String,size:Double,url:String) ->MsgAttachment {
        let newObject = """
        {
        "id": "\(id)",
        "name": "\(name)",
        "size": "\(size)MB",
        "url": "\(url)",
        "date": "\(getcurrentDate())"
        }
        """
        
        return MsgAttachment(JSONString: newObject)!
    }
    

    let questions: Variable<[MessageModel.QuestionModel]?> = Variable(nil)
    
    fileprivate func approveCase(){
        if isApproved.value != nil && isApproved.value!{
            apiService.getApproveCaseQuestions(id: id.value!)
                .subscribe(onNext: { [unowned self] questions in
                    self.questions.value = questions
                }).disposed(by: disposeBag)
        }
    }
    fileprivate func submitApproveCase(){
        if let answers = answers.value{
            apiService.approveCase(id: id.value!, answers: answers)
                .subscribe(onNext: { [unowned self] _ in
                    self.isSubmitted.value = true
                }).disposed(by: disposeBag)
        }
    }
    fileprivate func submitRejectCase(){
        if let comment = comment.value {
            apiService.rejectCase(id: id.value!, ratingId: 0, comment: comment)
                .subscribe(onNext: { [unowned self] _ in
                    self.isSubmitted.value = true
                }).disposed(by: disposeBag)
        }
    }
}

