//
//  IncidentConfirmationViewModel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
class IncidentConfirmationViewModel {
    static var refrenceNumberStatic :String!
    var refrenceNumber:String!{
        set{
            IncidentConfirmationViewModel.refrenceNumberStatic = newValue
        }
        get{
            return IncidentConfirmationViewModel.refrenceNumberStatic
        }
    }
}
