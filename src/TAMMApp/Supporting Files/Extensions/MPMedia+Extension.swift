//
//  MPMedia+Extension.swift
//  TAMMApp
//
//  Created by Daniel on 8/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import MediaPlayer
import AVFoundation
enum ExportError: Error {
    case unableToCreateExporter
}
extension MPMediaItem{
    func export( completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
        let asset = AVURLAsset(url: self.assetURL!)
        guard let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else {
            completionHandler(nil, ExportError.unableToCreateExporter)
            return
        }
        
        let fileURL = URL(fileURLWithPath: IncidentUrl.getDirectoryForIncident().absoluteString)
            .appendingPathComponent("\(UUID().uuidString)")
            .appendingPathExtension("m4a")
        
        exporter.outputURL = fileURL
        exporter.outputFileType = AVFileType.m4a
        
        exporter.exportAsynchronously {
            if exporter.status == .completed {
                completionHandler(fileURL, nil)
            } else {
                completionHandler(nil, exporter.error)
            }
        }
    }
}
