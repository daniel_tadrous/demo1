//
//  MediaFormats.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class MediaFormats {
    
    //images
    static let jpg = "jpg"
    static let png = "png"
    static let gif = "gif"
    static let jpeg = "jpeg"
    static let bmp = "bmp"
    static let ps = "ps"
    static let tiff = "tiff"
    
    static let imgFormats = [jpg,png,gif,jpeg,bmp,ps,tiff]
    
    //audio
    static let m4a = "m4a"
    static let mp3 = "mp3"
    static let wav = "wav"
    
    static let audioFormats = [m4a,mp3,wav,mov,mp4,mpg]
    
    //video
    static let mp4 = "mp4"
    static let mpg = "mpg"
    static let mpeg = "mpeg"
    static let mov = "mov"
   
    static let vedioFormats = [mp4,mpg,mpeg,mov, mp3]
    //docs
    static let pptx = "pptx"
    static let xls = "xls"
    static let doc = "doc"
    static let txt = "txt"
    static let csv = "csv"
    static let url = "url"
    static let xml = "xml"
    static let pdf = "pdf"
    static let htm = "htm"
    static let html = "html"
    static let ppt = "ppt"
    static let docx = "docx"
    static let xlsx = "xlsx"
    static let msg = "msg"
    static let rtf = "rtf"
    
    static let docsFormat = [pptx,xls,doc,txt,csv,url,xml,pdf,htm,html,ppt,docx,xlsx,msg,rtf]
    
    
}
