//
//  AolTopicDetailsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
protocol AspectOfLifeTopicDetailsViewData{
    var topicId: String {get}
    var topicTitle: String {get}
    var topicDescription: String {get}
    var topicSubTopics: [AspectOfLifeSubTopicViewData] {get set}
}
protocol AspectOfLifeSubTopicViewData {
    var subTopicId: String {get}
    var subTopicTitle: String {get}
    var subTopicDescription: String {get}
    var subTopicServices: [AspectOfLifeSubTopicServiceViewData] {get set}
    
}
protocol AspectOfLifeFaqViewData {
    var faqIdProp: String {get}
    var relatedToProp: String {get}
    var relatedToIdProp: String {get}
    var qProp: String {get}
    var aProp: String {get}
    var isCollapsed: Bool {set get}
}
protocol AspectOfLifeSubTopicServiceViewData {
    var subTopicServiceId: String {get}
    var subTopicServiceTitle: String {get}
    var subTopicServiceDescription: String {get}
    var isCollapsed: Bool {get set}
    var serviceFaqs: [AspectOfLifeFaqViewData]? {get set}
    var subTopicServiceIcon: String {get set}
    var subTopicServiceIsOnlineService: Bool {get set}
}

extension AspectOfLifeServiceTopicDetailModel: AspectOfLifeTopicDetailsViewData{
    var topicDescription: String {
        get{
            return description ?? "No Description"
        }
    }
    
    var topicId: String {
        get{
            return id ?? "-1"
        }
    }
    
    var topicTitle: String {
        get{
            return title ?? "No Title"
        }
    }
    
    var topicSubTopics: [AspectOfLifeSubTopicViewData] {
        get{
            return subTopics ?? []
        }
        set{
            subTopics = (newValue as! [AspectOfLifeSubTopicModel])
        }
    }
    
    
}
extension AspectOfLifeSubTopicModel: AspectOfLifeSubTopicViewData{
    var subTopicId: String {
        get{
            return id ?? "-1"
        }
    }
    
    var subTopicTitle: String {
        get{
            return title ?? "No Title"
        }
    }
    
    var subTopicDescription: String {
        get{
            return self.description ?? "NO Description"
        }
    }
    
    var subTopicServices: [AspectOfLifeSubTopicServiceViewData] {
        get{
            return services ?? []
        }
        set{
            services = (newValue as! [ServiceModel])
        }
    }
}
extension ServiceModel: AspectOfLifeSubTopicServiceViewData{
    var subTopicServiceIcon: String {
        get {
            return icon ?? ""
        }
        set{
            icon = newValue
        }
    }
    
    var subTopicServiceIsOnlineService: Bool {
        get {
            return isOnlineService ?? false
        }
        set{
            isOnlineService = newValue
        }
    }
    
    var serviceFaqs: [AspectOfLifeFaqViewData]? {
        get {
            return faqs
        }
        set{
            faqs = newValue as! [AspectOfLifeServiceFaqsModel]
        }
    }
    
    
    var isCollapsed: Bool {
        get {
            return isServiceCollapsed
        }
        set {
            isServiceCollapsed = newValue
        }
    }
    
  
    
    var subTopicServiceId: String {
        get{
            return id ?? "-1"
        }
    }
    
    var subTopicServiceTitle: String {
        get{
            return title ?? "No Title"
        }
    }
    
    var subTopicServiceDescription: String {
        get{
            return self.description ?? "NO Description"
        }
    }
}

extension AspectOfLifeServiceFaqsModel : AspectOfLifeFaqViewData{
    
    var isCollapsed: Bool {
        get{
            return self.collapsed
        }
        set{
            self.collapsed = newValue
        }
    }
    
    var faqIdProp: String {
        get{
            return self.id ?? "NO faqId"
        }
    }
    
    var relatedToProp: String {
        get{
            return self.relatedTo ?? "NO RelatedTo"
        }
    }
    
    var relatedToIdProp: String {
        get{
            return self.relatedToId ?? "NO RelatedToId"
        }
    }
    
    var qProp: String {
        get{
            return self.q ?? "NO Question"
        }
    }
    
    var aProp: String {
        get{
            return self.a ?? "NO Answer"
        }
    }
    
    
}

class AolTopicDetailsViewModel{
    
//    // Just set aspectId.value and the api will be called
    
    static var topicIdStatic:String?
    private(set) var topicId: Variable<String?> = Variable(nil)
//    private(set) var aspectTitle: Variable<String?> = Variable(nil)
    private(set) var viewData: Variable<AspectOfLifeTopicDetailsViewData?> = Variable(nil)
    
    fileprivate var apiService: ApiServiceAspectsOfLifeType
    
    fileprivate var disposeBag = DisposeBag()
    static var topicTitleStatic: String?
    private(set) var topicTitle: Variable<String?> = Variable(nil)
    
    private(set) var topicDescription: Variable<String?> = Variable(nil)
    
    
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        topicId.asObservable()
            .subscribe(onNext:{ [unowned self] topicId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if topicId.value != nil && !topicId.value!.isEmpty{
            apiService.getAspectOfLifeTopicDetails(id: topicId.value!)
                .subscribe(onNext:{ [unowned self] topicDetail in
                    self.viewData.value = topicDetail
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
}
