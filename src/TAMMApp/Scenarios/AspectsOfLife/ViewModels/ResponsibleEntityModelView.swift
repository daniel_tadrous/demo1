//
//  ResponsibleEntityModelView.swift
//  TAMMApp
//
//  Created by Daniel on 5/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class ResponsibleEntityViewModel{
    private(set) var viewData: Variable<ResponsibleEntity?> = Variable(nil)
      private(set) var responsibleEntityId: Variable<String?> = Variable(nil)
    fileprivate var apiService: ApiServiceAspectsOfLifeType
    
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        responsibleEntityId.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if responsibleEntityId.value != nil && !responsibleEntityId.value!.isEmpty{
            apiService.getResponsibleEntity(id: responsibleEntityId.value!)
                .subscribe(onNext: { [unowned self] responsibleEntity in
                    self.viewData.value = responsibleEntity
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
}
