//
//  SuperViewController.swift
//  TAMMApp
//
//  Created by Daniel on 9/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

class SuperViewController: UIViewController{
     var screenName: ScreensEnum? = ScreensEnum(rawValue: String(describing: type(of: self)).replacingOccurrences(of: "TAMMApp.", with: ""))
}
