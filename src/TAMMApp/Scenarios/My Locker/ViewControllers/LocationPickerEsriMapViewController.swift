//
//  LocationPickerReduced.swift
//  TAMMApp
//
//  Created by kerolos on 6/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import UIKit
import ArcGIS

enum SuggestionType {
    case poi
    case populatedPlace
}
struct LocationPickerLocation{
    var locationAddress: String
    var location: CLLocationCoordinate2D
    var country: String
    var region: String
    var subregion: String
    
    var image:UIImage?
    
    init (locationAddress: String,location: CLLocationCoordinate2D,country: String, region: String,subregion: String){
        self.locationAddress = locationAddress
        self.location = location
        self.country = country
        self.region = region
        self.subregion = subregion
    }
}

class LocationPickerEsriMapViewController: TammViewController, LocationPickerStoryboardLodable {
    
    weak var parentVC:MapParentViewController!
    var locationSelected:CLLocationCoordinate2D?
    
    static let abudhabiLocation = AGSPoint(clLocationCoordinate2D: CLLocationCoordinate2D(latitude: CLLocationDegrees(24.4539), longitude: CLLocationDegrees(54.3773)))
    
    let isRTL = L102Language.currentAppleLanguage().contains("ar")
    
    var micRecognizerService: SpeechRecognitionServiceType!
    
    private let country =  "ARE"
    private let lang = L102Language.currentAppleLanguage().contains("ar") ? "AR": "EN"
    
    @IBOutlet var mapView: AGSMapView!
    @IBOutlet var tableView:UITableView!
    //@IBOutlet
    //    var preferredSearchLocationTextField:UITextField! = UITextField()
    @IBOutlet var poiTextField:LocalizedTextField!
    @IBOutlet var tableViewHeightConstraint:NSLayoutConstraint!
    //    @IBOutlet var extentSearchButton:UIButton!
    @IBOutlet weak var reCenter: UIButton!
    
    fileprivate func recenterAction(displayCallout: Bool ) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                // do nothing
                print("access will be requested")
            case .restricted:
                displayToast(message: L10n.locationPermissionRistricted)
            case .denied:
                print("No access")
                DispatchQueue.main.async {
                    let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.locationPermissionNeeded, onComplete: {
                        self.recenterAction(displayCallout: displayCallout)
                    })
                    permessionView.shouldDisplayFloatingMenuWhenDismissed = false
                    UIApplication.shared.keyWindow!.addSubview(permessionView)
                    UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
                    
                }
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                panToCurrentLocation(displayCallout: displayCallout)
            }
        } else {
            print("Location services are not enabled")
            panToCurrentLocation(displayCallout: displayCallout)
        }
    }
    
    @IBAction func recenterPressed(_ sender: UIButton) {
        recenterAction(displayCallout: true)
        
    }
    private func panToCurrentLocation(displayCallout: Bool){
        recenterToLocation(){ [weak self] in
            if displayCallout{
                self?.reverseGeocode(point:  (self?.mapView.locationDisplay.mapLocation)!)
            }
        }
    }
    
    @IBAction private func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    private var geocodeParameters: AGSGeocodeParameters!
    private var reverseGeocodeParameters: AGSReverseGeocodeParameters!
    private var graphicsOverlay: AGSGraphicsOverlay!
    
    private var isTableViewVisible = true
    private var isTableViewAnimating = false
    
    
    private var locatorTask: AGSLocatorTask!
    private var locatorTaskOperation: AGSCancelable?
    
    
    private var suggestResults:[AGSSuggestResult]!
    private var suggestRequestOperation:AGSCancelable?
    private var selectedSuggestResult:AGSSuggestResult!
//    private var preferredSearchLocation:AGSPoint!
    //    private var selectedTextField:UITextField!
    
    private var isUsingCurrentLocation = false
    private let tableViewHeight:CGFloat = 220
    private var longPressedAndMoving = false
    private var magnifierOffset: CGPoint!
    

    //public weak var delegate: LocationPickerEsriMapViewControllerDelegate?
    public static var initialTextSearchStatic: String?
    public var initialTextSearch: String?{
        get{
            return LocationPickerEsriMapViewController.initialTextSearchStatic
        }
        set{
            LocationPickerEsriMapViewController.initialTextSearchStatic = newValue
        }
    }
    
    private let speechDuration = 10.0
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
                    if self.graphicsOverlay != nil {
                        self.graphicsOverlay.graphics.removeAllObjects()
                    }
                    self.initialize()
                    
                    if let initialTextSearch = self.initialTextSearch {
                        self.poiTextField.becomeFirstResponder()
                        self.poiTextField.text = initialTextSearch
                        self.fetchSuggestions(initialTextSearch, suggestionType: .poi, textField:self.poiTextField)
                    }
        
                }
    
    
    func roundTableViewCorners(){
        let maskPath = UIBezierPath(roundedRect: tableView.bounds,
                                    byRoundingCorners: [.topLeft, .topRight, .bottomRight, .bottomLeft],
                                    cornerRadii: CGSize(width: 10.0, height: 0.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        tableView.layer.mask = maskLayer
    }

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    private var filterText: [String]{
        return ConstantStrings.suggestionIncludeOneOrMoreFilter
    }
    
    override func viewDidAppear(_ animated: Bool) {

        self.setNavigationBar(title: L10n.selectLocationTitle, willSetSearchButton: false)

    }
    
    fileprivate func recenterToLocation(_ completion: (() -> ())? = nil  ) {
        //start location display
        
        self.mapView.locationDisplay.autoPanMode = .recenter
        self.mapView.locationDisplay.start { [weak self] (error: Error?) -> Void in
            if error == nil {
                //if the location display starts, update the preferred search location
                //textfield's text
                
                
                completion?()
                
            }
        }
    }
    
    private func initialize(){
        
        
        // UI stuff
        reCenter.layer.cornerRadius = reCenter.bounds.width / 2
        poiTextField.addRoundCorners(radious: poiTextField.bounds.height / 2, borderColor: #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1) )
        
       
        // decide which map to use
        //TODO confirm the decision
        self.mapView.map = AGSMap(basemap: AGSBasemap.streets() )
        //TODO: set language of the map (don't know how yet)
        
        //logic to show the extent search button
        self.mapView.viewpointChangedHandler = { [weak self] () -> Void in
            // we can do somting when the viwpoint changes
        }
        
        //instantiate the graphicsOverlay and add to the map view
        self.graphicsOverlay = AGSGraphicsOverlay()
        self.mapView.graphicsOverlays.add(self.graphicsOverlay)
        
        self.mapView.touchDelegate = self
        
        // if we want to add a layer or more to the app we can add those here
        //Create an instance of a tiled map service layer
        //        var tiledLayer = AGSTiledMapServiceLayer(URL: URL(string: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"))
        //        //Add it to the map view
        //        self.mapView.addMapLayer(tiledLayer, withName: "Basemap Tiled Layer")
        
        // the delegate to be used for actions like mapDidLoad AGSMapViewLayerDelegate
        //        self.mapView.layerDelegate = self
        
      
       
        
        //initialize geocode params
        self.geocodeParameters = AGSGeocodeParameters()
        //        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["*"])
        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["Match_addr"])
        //        self.geocodeParameters.resultAttributeNames.append(contentsOf: ["LongLabel"])
        // will return alot of results it was 75 but I will try 50 for a while
        self.geocodeParameters.minScore = 50
        self.geocodeParameters.outputLanguageCode = lang
        self.geocodeParameters.countryCode = country
        
        //initialize reverse geocode params
        self.reverseGeocodeParameters = AGSReverseGeocodeParameters()
        self.reverseGeocodeParameters.maxResults = 1
        self.reverseGeocodeParameters.resultAttributeNames.append(contentsOf: ["*"])
        self.reverseGeocodeParameters.outputLanguageCode = lang
        //        self.reverseGeocodeParameters.countryCode = country
        
        //register for keyboard notification in order to toggle overlay view on and off
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        //enable magnifier for better experience while using tap n hold to add a location
        self.mapView.interactionOptions.isMagnifierEnabled = true
        
        //the total amount by which we will need to offset the callout along y-axis
        //to show it correctly centered on the pushpin's head in the magnifier
        let img = UIImage(named: "ArcGIS.bundle/Magnifier.png")!
        self.magnifierOffset = CGPoint(x: 0, y: -img.size.height)
        
        
        
        
        self.locatorTask = AGSLocatorTask(url: URL(string: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer")!)
        
        
        poiTextField.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        
        setUpMicButton()
        // center to current user location whenever it is available
        if parentVC?.selectedPlace == nil {
            recenterToLocation()
            recenterAction(displayCallout: false)
        }else {
           goToSelectedLocation()
        }
        
    }
    
    func setUpMicButton(){
        let button = UIButton(type: .custom)
        let img = "V".tammImage()
        button.setImage( img , for: .normal)
        let b = poiTextField.bounds
        button.frame = CGRect(x: CGFloat(b.width - 25), y: 0, width: b.height, height: b.height)
        button.addTarget(self, action: #selector(self.micButtonTouched), for: .touchUpInside)
        
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 19, height: 20))
        
        poiTextField.rightViewMode = .always
        poiTextField.leftViewMode = .always

        if isRTL{
            button.imageEdgeInsets = UIEdgeInsetsMake(0, 16, 0, 0)
        } else{
            button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 16)
        }
        poiTextField.rightView = button
        
        poiTextField.leftView = paddingView
    }
    
    @objc func micButtonTouched(){
        micRecognizerService.start(delegate: self, duration: self.speechDuration)
    }
    
    @objc func keyboardWillShow(){
        
    }
    
    @objc func keyboardWillHide(){
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //method to toggle the suggestions table view on and off
    private func animateTableView(expand:Bool) {
//        if (expand != self.isTableViewVisible) &&
//            !self.isTableViewAnimating
//        {
        DispatchQueue.main.async {
            self.isTableViewAnimating = true
            self.tableViewHeightConstraint.constant = expand ? min ( self.tableViewHeight, CGFloat((self.tableView.dataSource?.tableView(self.tableView, numberOfRowsInSection: 0))! * 28))  : 0


            self.view.layoutIfNeeded()
            self.roundTableViewCorners()

        }
        
//            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
//                self?.view.layoutIfNeeded()
//                }, completion: { [weak self] (finished) -> Void in
//                    self?.isTableViewAnimating = false
//                    self?.isTableViewVisible = expand
//            })
//        }
    }
    
    
    //method returns a graphic object for the specified point and attributes
    private func graphicForPoint(_ point: AGSPoint, attributes:[String:AnyObject]?) -> AGSGraphic {
        let markerImage = UIImage.init(color: .darkBlueGrey, size: CGSize(width: 20, height: 20 ))! // #imageLiteral(resourceName: "About") // UIImage(named: "RedMarker")!
        let symbol = AGSPictureMarkerSymbol(image: markerImage)
        symbol.leaderOffsetY = 0 // markerImage.size.height/2
        symbol.offsetY = 0 //markerImage.size.height/2
        let graphic = AGSGraphic(geometry: point, symbol: symbol, attributes: attributes)
        return graphic
    }
    
    
    private func showAlert(_ msg: String){
        self.displayToast(message: msg)
    }
    
    
    
    
    //method to show the callout for the provided graphic, with tap location details
    private func showCalloutForGraphic(_ graphic:AGSGraphic, tapLocation:AGSPoint, animated:Bool, offset:Bool) {
        print(graphic.attributes)
        
        
        let customView = OurCalloutView.loadView()
        self.mapView.callout.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //customView.bounds = CGRect(x: 0, y: 0, width: 300, height: 300)
        self.mapView.callout.customView = customView
        customView.frame =  CGRect(origin: CGPoint.zero, size: CGSize(width: customView.frame.width, height: customView.frame.height))
        
        customView.title =  graphic.attributes["PlaceName"] as? String ?? ""
        customView.details = graphic.attributes["LongLabel"] as? String ?? ""
        
        var validLocation = true
        if !containsAnyOfFilterText( customView.details){
            if (customView.title.isEmpty){
                customView.title = customView.details
            }
            customView.details = L10n.serviceNotAvailableForLocation
            customView.setDetailsTextColor(color:.red)
            validLocation = false
            customView.setButtonText(string: L10n.cancel)
        }
        
        if validLocation {
            parentVC.updateSelectedlocation(adress: customView.details, mapPoint: tapLocation)
        }
        print ("call out should display" )
        print (customView.title)
        print (customView.details)
        
        customView.buttonClickedAction = {
            [weak self] in
            let lat = tapLocation.x
            let long = tapLocation.y
            
            if validLocation{
                var location = LocationPickerLocation(locationAddress: graphic.attributes["LongLabel"] as? String ?? "", location: CLLocationCoordinate2D(latitude: lat, longitude: long), country: graphic.attributes["CountryCode"] as? String ?? "", region: graphic.attributes["Region"] as? String ?? "", subregion: graphic.attributes["Subregion"] as? String ?? "")
//                let lat = tapLocation.toCLLocationCoordinate2D().latitude
//                let long = tapLocation.toCLLocationCoordinate2D().longitude
                
                
                
                graphic.isVisible = false
                
                
                //zoom to point
//                let envelope = AGSEnvelope(xMin: tapLocation.x - 3, yMin: tapLocation.y - 3, xMax: tapLocation.x + 3, yMax: tapLocation.y + 3, spatialReference: AGSSpatialReference(wkid: 3857))
//
//                let viewPoint = AGSViewpoint(targetExtent: envelope)
//                self?.mapView.setViewpoint(viewPoint)
                //take screenshpt
                let viewpoint = AGSViewpoint(latitude: lat, longitude: long, scale: 10000)
                self?.mapView.setViewpoint(viewpoint, duration: 0, curve: AGSAnimationCurve.linear) { (finished) -> Void in
                    if (!finished){
                        print("User interrupted Animation")
                    }
                    else{
                        self?.mapView.exportImage(completion: { (image, error) in
                            if let er = error{
                                print("nil image")
                            }
                            else{
                                location.image = image
                                self?.parentVC?.delegate?.locationPickerDidSelectLocation((self?.parentVC!)!,  location)
                            }
                        })
                    }
                }
                
                
            } else{
                print("cancel pressed")
                self?.mapView.callout.dismiss()
            }
        }
        
        if !offset {
            self.mapView.callout.show(for: graphic, tapLocation: tapLocation, animated: animated)
        }
        else {
            self.mapView.callout.show(at: tapLocation, screenOffset: self.magnifierOffset, rotateOffsetWithMap: false, animated: animated)
        }
    }
    
    
//    func contains(anyOf: [String], in text: String) -> [String] {
//
//        do {
//            let regex = try NSRegularExpression(pattern: regex)
//            let nsString = text as NSString
//            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
//            return results.map { nsString.substring(with: $0.range)}
//        } catch let error {
//            print("invalid regex: \(error.localizedDescription)")
//            return []
//        }
//    }
    
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func goToSelectedLocation(){

        if parentVC.selectedPlace != nil {

            
            let point = parentVC.selectedPlace?.mapPoint!
            
//            let centerPoint = parentVC.selectedPlace?.mapPoint!
//            let rightPoint = AGSPoint(x: (centerPoint?.x)!+1, y: (centerPoint?.y)!-1,  spatialReference: AGSSpatialReference(wkid: 4326))
//            let vp = AGSViewpoint(targetExtent: AGSEnvelope(min: centerPoint!, max: rightPoint))
//            self.mapView.setViewpoint(vp, duration: 3, completion: nil)

//            mapView.setViewpointCenter(point!, completion: nil)
            //mapView.setViewpointCenter(point!, scale: 0.5, completion: nil)
            let graphic = parentVC.graphicForPoint(point!, attributes: [String:AnyObject]())
            self.graphicsOverlay.graphics.removeAllObjects()
            graphic.attributes.setValue(parentVC.selectedPlace?.addressLocation, forKey: "LongLabel")

            self.graphicsOverlay.graphics.add(graphic)
           // showCalloutForGraphic(graphic, tapLocation: point!, animated: true, offset: false)
            reverseGeocodeAndDisplayCallout(point!)
            mapView.setViewpointCenter(point!, scale: 10000, completion: nil)
            
        }
    }
    
}




// used for reverse gro coding and poping out callout when a place graphic is pressed 

extension LocationPickerEsriMapViewController: AGSGeoViewTouchDelegate{
    
    
    
    
    private func reverseGeocode(point:AGSPoint) {
        //clear the search bar text to give feedback that the graphic
        //is based on the tap and not search
        self.poiTextField.text = ""
        
        //remove all previous graphics
        self.graphicsOverlay.graphics.removeAllObjects()
        
        //normalize the point
        let normalizedPoint = AGSGeometryEngine.normalizeCentralMeridian(of: point) as! AGSPoint
        
        //cancel all previous operations
        self.locatorTaskOperation?.cancel()
        
        
        //create a graphic and add to the overlay
        let graphic = self.graphicForPoint(normalizedPoint, attributes: [String:AnyObject]())
        self.graphicsOverlay.graphics.add(graphic)
        
        // stop displaying current location
        self.mapView.locationDisplay.stop()
        
        //perform reverse geocode
        self.locatorTaskOperation = self.locatorTask.reverseGeocode(withLocation: normalizedPoint, parameters: self.reverseGeocodeParameters) { [weak self] (results: [AGSGeocodeResult]?, error: Error?) -> Void in
            
            print ("reverse geo code done")
            
            if let error = error as NSError? , error.code != NSUserCancelledError {
                //print error instead alerting to avoid disturbing the flow
                print(error.localizedDescription)
            }
            else {
                //if a result is found extract the required attributes
                //assign the attributes to the graphic
                //and show the callout
                if let results = results , results.count > 0 {
                    graphic.attributes.addEntries(from: results.first?.attributes ?? [:])
                    
                    self?.showCalloutForGraphic(graphic, tapLocation: normalizedPoint, animated: false, offset: self!.longPressedAndMoving)
                    
                    return
                }
                else {
                    //no result was found
                    //using print in log instead of alert to
                    //avoid breaking the flow
                    print("No address found")
                    self?.showAlert("No address found")
                    //dismiss the callout if already visible
                    self?.mapView.callout.dismiss()
                    
                }
            }
            //in case of error or no results, remove the graphics
            self?.graphicsOverlay.graphics.remove(graphic)
        }
    }
    
    
    
    fileprivate func displayCalloutIfGraphicIsTouched(_ screenPoint: CGPoint, _ mapPoint: AGSPoint) {
        self.mapView.callout.dismiss()
        
        self.mapView.identify(self.graphicsOverlay, screenPoint: screenPoint, tolerance: 12, returnPopupsOnly: false) { [weak self] (result: AGSIdentifyGraphicsOverlayResult)  in
            
            if let error = result.error{
                self?.showAlert(error.localizedDescription)
            } else if result.graphics.count > 0{
                let graphic = result.graphics.first!
                
                    self?.showCalloutForGraphic(graphic, tapLocation: mapPoint, animated: true, offset: false)
                
                
            }
        }
    }
    
    fileprivate func reverseGeocodeAndDisplayCallout(_ mapPoint: AGSPoint) {
        //on long press perform reverse geocode
        self.reverseGeocode(point: mapPoint)
    }
    
    
    func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        hideKeyboard()
        displayCalloutIfGraphicIsTouched(screenPoint, mapPoint)
    }
    
    
    func geoView(_ geoView: AGSGeoView, didLongPressAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        self.longPressedAndMoving = true
        hideKeyboard()
        reverseGeocodeAndDisplayCallout(mapPoint)
    }
    
    func geoView(_ geoView: AGSGeoView, didMoveLongPressToScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        //perform geocode for the updated location
        hideKeyboard()
        reverseGeocodeAndDisplayCallout(mapPoint)
    }
    
    func geoView(_ geoView: AGSGeoView, didEndLongPressAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
        self.longPressedAndMoving = false
        hideKeyboard()
        //the callout right now will be at an offset
        //update the callout to show on top of the graphic
        displayCalloutIfGraphicIsTouched(screenPoint, mapPoint)
        
    }
}



extension LocationPickerEsriMapViewController: UITextFieldDelegate{
    //MARK: - UITextFieldDelegate
    
    
    //method to zoom to an array of graphics
    func zoomToGraphics(_ graphics:[AGSGraphic]) {
        if graphics.count > 0 {
            let multipoint = AGSMultipointBuilder(spatialReference: graphics[0].geometry!.spatialReference)
            for graphic in graphics {
                multipoint.points.add(graphic.geometry as! AGSPoint)
            }
            self.mapView.setViewpoint(AGSViewpoint(targetExtent: multipoint.extent)) {(finished:Bool) -> Void in
               print(AGSViewpoint(targetExtent: multipoint.extent).targetScale)
            }
        }
    }
    
    
    func handleGeocodeResultsForPOIs(_ geocodeResults:[AGSGeocodeResult]?, areExtentBased:Bool) {
        if let results = geocodeResults , results.count > 0 {
            
            //show the graphics on the map
            let results2 = results.filter { (graphicItem) -> Bool in
                return containsAnyOfFilterText( graphicItem.attributes!["LongLabel"] as? String ?? "")

            }
            for result in results2 {
                let graphic = self.graphicForPoint(result.displayLocation!, attributes: result.attributes as [String : AnyObject]?)
                
                self.graphicsOverlay.graphics.add(graphic)
                self.mapView.locationDisplay.stop()
            }
            
            //extent search button display logic
            self.zoomToGraphics(self.graphicsOverlay.graphics as AnyObject as! [AGSGraphic])
            
        }
        else {
            //show alert for no results
            self.showAlert("No results found")
        }
    }
    
    
    private func geocodePOIs(_ poi:String, location:AGSPoint?, extent:AGSGeometry?) {
        //hide callout if already visible
        self.mapView.callout.dismiss()
        
        //remove all previous graphics
        self.graphicsOverlay.graphics.removeAllObjects()
        
        //parameters for geocoding POIs
        let params = AGSGeocodeParameters()
        params.preferredSearchLocation = location
        params.searchArea = extent
        params.outputLanguageCode = lang
        params.countryCode = country
        
        params.outputSpatialReference = self.mapView.spatialReference
        params.resultAttributeNames.append(contentsOf: ["*"])
        
        //cancel all previous operations
        self.locatorTaskOperation?.cancel()
        
        
        //geocode using the search text and params
        self.locatorTaskOperation = self.locatorTask.geocode(withSearchText: poi, parameters: params) { [weak self] (results:[AGSGeocodeResult]?, error:Error?) -> Void in
            if let error = error {
                print(error.localizedDescription)
                self?.showAlert("error finding address")
                
            }
            else {
                self?.handleGeocodeResultsForPOIs(results, areExtentBased: (extent != nil))
            }
        }
    }
    
    private func geocodeUsingSuggestResult(_ suggestResult:AGSSuggestResult, completion: @escaping () -> Void) {
        //create geocode params
        let params = AGSGeocodeParameters()
        params.outputSpatialReference = self.mapView.spatialReference
        params.outputLanguageCode = lang
        params.countryCode = country
        
        self.locatorTaskOperation?.cancel()
        
        //geocode with selected suggest result
        self.locatorTaskOperation = self.locatorTask.geocode(with: suggestResult, parameters: params) { [weak self] (result: [AGSGeocodeResult]?, error: Error?) -> Void in
            if let error = error {
                print(error.localizedDescription)
            }
            else {
                if let result = result , result.count > 0 {
//                    self?.preferredSearchLocation = result[0].displayLocation
                    completion()
                }
                else {
                    print("No location found for the suggest result")
                }
            }
        }
    }
    
    private func search() {
        //validation
        guard let poi = self.poiTextField.text , !poi.isEmpty else {
            self.showAlert("Point of interest required")
            return
        }
        
        //cancel previous requests
        self.suggestRequestOperation?.cancel()
        self.suggestResults = []
        //hide the table view
        self.animateTableView(expand: false)
        
        //check if a suggestion is present
        if self.selectedSuggestResult != nil {
            //since a suggestion is selected, check if it was already geocoded to a location
            //if no, then goecode the suggestion
            //else use the geocoded location, to find the POIs
            self.geocodeUsingSuggestResult(self.selectedSuggestResult, completion: { [weak self] () -> Void in
                //find the POIs wrt location
                self?.geocodePOIs(poi, location: nil, extent: nil)
            })

        }
        else {
            self.geocodePOIs(poi, location: nil, extent: nil)
        }
    }
    
    
    
    fileprivate func containsAnyOfFilterText(_ label: String) -> Bool{
        print(label)
        let label = label.lowercased()
        for filterItem in self.filterText{
            let stringMatch = label.contains(filterItem)
            if stringMatch {
                return true
            }
        }
        return false
    }
    
    private func fetchSuggestions(_ string:String, suggestionType:SuggestionType, textField:UITextField) {
        //cancel previous requests
        self.suggestRequestOperation?.cancel()
        
        //initialize suggest parameters
        let suggestParameters = AGSSuggestParameters()
        let flag:Bool = (suggestionType == SuggestionType.poi)
        suggestParameters.categories = flag ? ["POI"] : ["Populated Place"]
        suggestParameters.preferredSearchLocation =  LocationPickerEsriMapViewController.abudhabiLocation  // flag ? nil : self.mapView.locationDisplay.mapLocation
        suggestParameters.countryCode = country
//        suggestParameters.outputLanguageCode = lang
        
        
        //get suggestions
        self.suggestRequestOperation = self.locatorTask.suggest(withSearchText: string, parameters: suggestParameters) { (result: [AGSSuggestResult]?, error: Error?) -> Void in
            if string == textField.text { //check if the search string has not changed in the meanwhile
                if let error = error {
                    print(error.localizedDescription)
                }
                else {
                    //update the suggest results and reload the table
                    self.suggestResults = result?.filter({(item: AGSSuggestResult) -> Bool in
                        
                        let label = item.label
                        
                        
                        return self.containsAnyOfFilterText(label)
                    })
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        //        self.selectedTextField = self.poiTextField
        
        if newString.count > 0 {
            self.fetchSuggestions(newString, suggestionType: .poi, textField:self.poiTextField)
        }
        else{
            self.suggestResults = []
            animateTableView(expand: false)
        }
        
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        self.animateTableView(expand: false)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.search()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.animateTableView(expand: false)
    }
    
}


extension LocationPickerEsriMapViewController: UITableViewDataSource{
    //MARK: - UITableViewDataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if let count = self.suggestResults?.count {
            rows = count
        }
        self.animateTableView(expand: rows > 0)
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestCell")!
        
        let rowNumber = (indexPath as NSIndexPath).row
        let suggestResult = self.suggestResults[rowNumber]
        
        cell.textLabel?.text = suggestResult.label
        cell.imageView?.image = nil
        return cell
    }
}

extension LocationPickerEsriMapViewController: UITableViewDelegate{
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let suggestResult = self.suggestResults[(indexPath as NSIndexPath).row]
        self.selectedSuggestResult = suggestResult

        self.poiTextField.text = suggestResult.label
//
//        self.animateTableView(expand: false)
        
        search()
        poiTextField.resignFirstResponder()
    }
}

extension LocationPickerEsriMapViewController: SpeechRecognitionServiceTypeDelegate{
    func recognizerAvailable(isAvailable: Bool) {
        if isAvailable{
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: self.speechDuration)
        }
        else{
            self.displayToast(message: "could not start recognition")
        }
    }
    
    func textRecived(text: String) {
        poiTextField.text = text
        if text.count > 2 {
            self.fetchSuggestions(text, suggestionType: .poi, textField:self.poiTextField)
        }
    }
    
    func recognitionIsDone(_ error: Error?) {
        poiTextField.becomeFirstResponder()
        print("recognition done")
        
    }
    
    
}

