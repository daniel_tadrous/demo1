//
//  IconButton.swift
//  TAMMApp
//
//  Created by kerolos on 5/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

class IconButton:UIButton{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.titleLabel?.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
        else{
            self.titleLabel?.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
        }
    }
}

