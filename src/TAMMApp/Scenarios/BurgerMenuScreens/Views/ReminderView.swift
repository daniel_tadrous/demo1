//
//  ReminderTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ReminderView: UIView {

    @IBOutlet weak var grayBottomView: UIView!
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var timeIntervalLabel: LocalizedLabel!
    
    @IBOutlet weak var RoundedView: RoundedView!
    
    @IBOutlet weak var selectedView: RoundedView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        // commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("ReminderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    
}
