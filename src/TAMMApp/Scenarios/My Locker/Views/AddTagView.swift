//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift

class AddTagView : UIView{
   
    @IBOutlet weak var tagTF: LocalizedTextField!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mainView: UIView!

    
    
    @IBOutlet weak var cancelBtn: LocalizedButton!
    
   
    
    @IBOutlet weak var okBtn: LocalizedButton!
    private var okClickHandler: ((String)->Void)?
    
    @IBAction func okClickHandler(_ sender: LocalizedButton) {
        self.removeFromSuperview()
        self.displayFloatingMenu()
        self.okClickHandler?(self.tagTF.text!)
        self.tagTF.text = ""
    }
    
    @IBAction func cancelClickHandler(_ sender: LocalizedButton) {
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    init(frame: CGRect, okClickHandler: @escaping (String)->Void) {
        super.init(frame: frame)
        self.okClickHandler = okClickHandler
        commonInit()
    }
 
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "AddTagView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        setupHooks()
        setupUiData()
    }
   
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
      
        
    }
    
    func setupUiData(){
        self.cancelBtn.layer.borderWidth = 1
        self.cancelBtn.layer.borderColor = UIColor.turquoiseBlue.cgColor
        self.cancelBtn.addRoundCorners(radious: 17)
        self.okBtn.addRoundCorners(radious: 17)
    }
    
    
}
