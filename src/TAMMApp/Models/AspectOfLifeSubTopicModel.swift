//
//  SubTopic.swift
//  TAMMApp
//
//  Created by kerolos on 4/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class AspectOfLifeSubTopicModel: Mappable {
    var id: String?
    var title: String?
    var description: String?
    var services: [ServiceModel]?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id              <- map["id"]
        title           <- map["title"]
        description     <- map["description"]
        services        <- map["services"]
    }
    
}
