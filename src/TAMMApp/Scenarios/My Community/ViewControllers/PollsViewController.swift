    //
    //  FAQViewController.swift
    //  TAMMApp
    //
    //  Created by Daniel on 6/25/18.
    //  Copyright © 2018 Igor Kulman. All rights reserved.
    //
    
    import UIKit
    import RxSwift
    
    class PollsViewController: TammViewController, PollsStoryboardLodable {
        
        @IBOutlet weak var pollsTableView: UITableView!
        public var viewModel:PollsViewModel!
        var faqsWithCategories: [String:[AspectOfLifeServiceFaqsModel]] = [:]
        let headerViewIdentifier = "poll_header_view"
        let collapsedIdentifier = "PollCellCollapsed"
        let expandedAnswersIdentifier = "PollCellExpandedAnswers"
        let expandedResultsIdentifier = "PollCellExpandedResults"
        var categories: [String] = [String]()
        var disposeBag = DisposeBag()
        var sortedPolls: [Int:[MyCommunityModel.Poll]] = [0:[MyCommunityModel.Poll](),1:[MyCommunityModel.Poll]()]
        var polls: [MyCommunityModel.Poll]? {
            get{
                return viewModel?.viewData.value
            }
            set{
                viewModel?.viewData.value = newValue
            }
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setNavigationBar(title: L10n.MyCommunity.getCommunityItemText(item: .Polls), willSetSearchButton: true)
            // Do any additional setup after loading the view.
            let bundle = Bundle(for: type(of: self))
            self.pollsTableView.register(UINib(nibName: "PollsSectionHeaderView", bundle: bundle), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
            setupHooks()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        func setupHooks(){
            viewModel!.viewData.asObservable()
                .subscribeOn(MainScheduler.instance)
                .subscribe(onNext: { [unowned self] polls in
                    if(polls == nil){
                        //TODO: should do somting ????
                    }else{
                        self.sortedPolls = [0:[MyCommunityModel.Poll](),1:[MyCommunityModel.Poll]()]
                        for poll in self.polls!{
                            if poll.isCurrent!{
                                self.sortedPolls[0]?.append(poll)
                            }else{
                                self.sortedPolls[1]?.append(poll)
                            }
                        }
                        
                        self.pollsTableView.reloadData()
                        
                    }
                })
                .disposed(by: disposeBag)
        }
        
    }
    
    extension PollsViewController : UITableViewDelegate, UITableViewDataSource{
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 2
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            
            return 70
        }
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 10
        }
        
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let footerView = UIView()
            footerView.backgroundColor = UIColor.paleGreyTwo
            return footerView
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier)
            let label  = headerView?.viewWithTag(1) as! UILabel
            label.text = L10n.MyCommunity.Polls.getSectionHeader(section: section)
            return headerView
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return self.sortedPolls[section]?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var cell: PollCell!
            
            let poll  = self.sortedPolls[indexPath.section]?[indexPath.row]
            if (poll?.isPromoted)!{
                if (poll?.isCurrent)!{
                    if poll?.answerId != -1{
                        cell = tableView.dequeueReusableCell(withIdentifier: expandedResultsIdentifier) as! PollCell
                        cell.shareDimmerBtn?.backgroundColor = UIColor.clear
                        for view in (cell.stackViewContainer?.arrangedSubviews)!{
                            view.isHidden = true
                            cell.stackViewContainer?.removeArrangedSubview(view)
                        }
                        for answer in (poll?.answers)!{
                            let button = PollResultView(frame: CGRect(), answer: answer)
                            cell.stackViewContainer?.addArrangedSubview(button)
                        }
                    }else{
                        cell = tableView.dequeueReusableCell(withIdentifier: expandedAnswersIdentifier) as! PollCell
                        for view in (cell.stackViewContainer?.arrangedSubviews)!{
                            view.isHidden = true
                            cell.stackViewContainer?.removeArrangedSubview(view)
                        }
                        for index in 0..<(poll?.answers?.count)!{
                            let answer = poll?.answers?[index]
                            let button = PollButton()
                            button.setTitle(answer?.text, for: .normal)
                            button.poll = MyCommunityModel.Poll(id: (poll?.id)!, answerId: index)
                            button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answerSubmitted(_:))))
                            cell.stackViewContainer?.addArrangedSubview(button)
                        }

                    }
                }else{
                    cell = tableView.dequeueReusableCell(withIdentifier: expandedResultsIdentifier) as! PollCell
                    cell.shareDimmerBtn?.backgroundColor = UIColor.clear
                    for view in (cell.stackViewContainer?.arrangedSubviews)!{
                        view.isHidden = true
                        cell.stackViewContainer?.removeArrangedSubview(view)
                    }
                    for answer in (poll?.answers)!{
                        let button = PollResultView(frame: CGRect(), answer: answer)
                        cell.stackViewContainer?.addArrangedSubview(button)
                    }
                }
            }else{
                cell = tableView.dequeueReusableCell(withIdentifier: collapsedIdentifier) as! PollCell
            }
            cell.colorizeFooter(isCurrent: (poll?.isCurrent)!)
            cell.setClosureDate(poll: poll!)
            let formater = NumberFormatter()
            formater.groupingSeparator = ","
            formater.numberStyle = .decimal
            cell.participantNumLabel.text = formater.string(from: NSNumber(value: (poll?.participantsCount)!))
            cell.titleLable.text = poll?.title
            cell.OnShareBtnClick = {
                self.viewModel.share(viewController: self, text: (poll?.link)!)
            }
            
            cell.OnCollapseExpandBtnClick = {
                poll?.isPromoted = !(poll?.isPromoted)!
               // let animation = (poll?.isPromoted)! ? UITableViewRowAnimation.bottom : UITableViewRowAnimation.top
                self.pollsTableView.reloadRows(at: [indexPath], with: .automatic)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
            return cell
        }
        @objc func answerSubmitted(_ sender: Any){
            let pollBtn = (sender as! UITapGestureRecognizer).view as! PollButton
            viewModel.poll.value = pollBtn.poll
            var index:Int = 0
            for poll in self.sortedPolls[0]!{
                
                if poll.id == pollBtn.poll?.id{
                    poll.answerId = pollBtn.poll?.answerId
                    self.pollsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.automatic)
                    break
                }
                index = index + 1
            }
            
        }
        
    }
    
    
