//
//  PressReleasesModel.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

import ObjectMapper

class PressReleasesModel:Mappable{
    
    var pageCount:Int?
    var pressReleases:[PressRelease]?
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        pageCount <- map["pageCount"]
        pressReleases <- map["pressReleases"]
        
    }
    
}

