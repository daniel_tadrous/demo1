//
//  MyCommunityViewController.swift
//  TAMMApp
//
//  Created by Daniel Tadrous on 6/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MyCommunityViewControllerDelegate {
    func openPolls()
    func openEvents()
    func openCoCreate()
    func openNewsAndPress(type:NewsAndPressReleasesEnum)
    func openFactsAndFigures()
    func openInitiative()
}

class MyCommunityViewController:SuperCollectionViewController, UICollectionViewDelegateFlowLayout, MyCommunityStoryboardLodable {
    
    static var storyboardName: String = "MyCommunity"
    
    public var delegate:MyCommunityViewControllerDelegate?
    public var viewModel:MyCommunityViewModel?
    public var draggableButton:UICircularImage!
    fileprivate let reuseIdentifier = "MyCommunityCell"
    fileprivate let numberOfSections = 1
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate let intentRatio: CGFloat = 0.041
    private var sectionInsets:UIEdgeInsets?
    private var screenWidth:CGFloat?
    //private var floatingMenu:FloatingMenuView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MyCommunityViewModel();
        screenWidth = UIScreen.main.bounds.width;
        sectionInsets = UIEdgeInsets(top: 20.0, left: screenWidth! * intentRatio, bottom: 20.0, right: screenWidth! * intentRatio)
        self.collectionView?.setNeedsLayout()
        self.collectionView?.layoutIfNeeded()
        //floatingMenu = FloatingMenuView(frame: UIScreen.main.bounds)
        //self.view.addSubview(floatingMenu)
        self.setNavigationBar(title: L10n.MyCommunity.myCommunity)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.backgroundColor = #colorLiteral(red: 0.9499574304, green: 0.9501777291, blue: 0.9562802911, alpha: 1).getAdjustedColor()
        collectionView?.reloadData()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // floatingMenu.bringSubview(toFront: floatingMenu.draggableButton)
        //if(draggableButton == nil){
        //draggableButton = self.initiateDraggableButton()
        // }
        //self.setDraggableButton(draggable: draggableButton)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = (sectionInsets?.left)! * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        let tempLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: widthPerItem-10, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 2
        tempLabel.text = viewModel?.getMyCommunitySectionName(index: 0)
        tempLabel.font = UIFont(name: "CircularStd-Bold", size: 18)
        tempLabel.sizeToFit()
       
        return CGSize(width: widthPerItem, height: widthPerItem + tempLabel.frame.height/2)
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getMyCommunitySectionCount() ?? 0
    }
    
    //3
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MyCommunityCollectionViewCell
        
        cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        cell.lblName.textColor = #colorLiteral(red: 0.1137974337, green: 0.09944240004, blue: 0.2515189052, alpha: 1).getAdjustedColor()
        cell.isSelected = true
        cell.lblName.text = viewModel?.getMyCommunitySectionName(index: indexPath.row)
        cell.icon.FontImage = (viewModel?.getMyCommunitySectionIcon(index: indexPath.row))!
        cell.icon.FontImageColor = UIColor.init(hexString: (viewModel?.getMyCommunitySectionIconColor(index: indexPath.row))!)
        cell.icon.backgroundColor = UIColor.init(hexString: (viewModel?.getMyCommunitySectionIconBackgroundColor(index: indexPath.row))!)
        cell.layer.cornerRadius = 10
//        if indexPath.row == 4{
//            cell.icon.FontSize = 30
//        }
        
        
        // drop shadow
        cell.layer.shadowColor = UIColor.lightGray.cgColor;
        cell.layer.shadowOpacity=0.5;
        cell.layer.shadowRadius=2.0;
        cell.layer.shadowOffset = CGSize(width:0, height:0);
        cell.layer.masksToBounds = false;
        // Configure the cell
        return cell
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets!
    }
    
    //4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets!.left
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = viewModel?.getMyCommunitySectionType( index: indexPath.row ) ?? .Home  //MyCommunityItems.getItemByIndex(index: indexPath.row)
        
        switch item {
        case .FactsAndFigures:
            delegate?.openFactsAndFigures()
        case .CommunityEvents:
            delegate?.openEvents()
        case .Initiative:
            delegate?.openInitiative()
        case .AboutAbudhabi:
            Toast(message:L10n.commingSoon).Show()
        case .CoCreation:
            openCoCreate()
        case .Polls:
            delegate?.openPolls()
        case .News:
            delegate?.openNewsAndPress(type: .NEWS)
        case .Press:
            delegate?.openNewsAndPress(type: .PRESS)
        default:
            break
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    public func openCoCreate(){
        delegate?.openCoCreate()
    }
    
}

