//
//  ConfirmationFactory.swift
//  TAMMApp
//
//  Created by kerolos on 7/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Swinject

class ConfirmationFactory{
    

    
    class func create(container: Container, type: ConfirmationType, referenceNumber: String?) -> ConfirmationViewController {
        switch type {
        case .collaborate:
            let vc = container.resolveViewController(ConfirmationViewController.self)
            
            vc.viewModel = ConfirmationViewModel()
            vc.viewModel.initialize(type: type, title: L10n.thankYou, summary: L10n.messageRecievedSummary, reference: referenceNumber, details: L10n.messageRecievedDetails, iconLetter: "u", gotoLabel: L10n.goToMyComunity)
            
            
            return vc
            
//        default:
            
        }
    }
    
    
}
