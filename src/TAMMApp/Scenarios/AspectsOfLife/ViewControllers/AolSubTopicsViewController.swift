//
//  AolSubTopicsViewController.swift
//  TAMMApp
//
//  Created by Daniel on 5/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol AspectsOfLifeServiceTableViewDelegate {
    func selectedService(service: AspectOfLifeSubTopicServiceViewData, subTopicTitle: String)
}

class AolSubTopicsViewController: TammViewController, AolSubTopicsStoryboardLodable,UITableViewDelegate, UITableViewDataSource {
    
    public var viewModel: AolTopicDetailsViewModel?
    var disposeBag = DisposeBag()
    var serviceSelectedDelegate: AspectsOfLifeServiceTableViewDelegate?
    var subTopics: [AspectOfLifeSubTopicViewData] {
        get{
            return viewModel?.viewData.value?.topicSubTopics ?? []
        }
        set{
            viewModel?.viewData.value?.topicSubTopics = newValue
        }
    }
    public weak var delegate: AspectsOfLifeTopicsTableViewDelegate?
    
    @IBOutlet weak var subTopicsTableView: UITableView!
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label:LocalizedLabel = LocalizedLabel()
        label.frame = CGRect(x: label.frame.origin.x, y: label.frame.origin.y, width: label.frame.width, height: 35)
        label.textColor = UIColor.darkBlueGrey
        label.font = UIFont(name: "CircularStd-Bold", size: 22)
        label.originalFont = UIFont(name: "CircularStd-Bold", size: 22)
        label.RTLFont = "Swissra-Bold"
        label.applyFontScalling = true
        label.text = "  " + subTopics[section].subTopicTitle
        label.backgroundColor = UIColor.paleGreyTwo
        return label
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return subTopics.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subTopics[section].subTopicServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var service : AspectOfLifeSubTopicServiceViewData = subTopics[indexPath.section].subTopicServices[indexPath.row]
        let cell:AolSubTopicCell
        if service.isCollapsed{
            cell = tableView.dequeueReusableCell(withIdentifier: "AolSubTopicCell2", for: indexPath) as! AolSubTopicCell
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "AolSubTopicCell", for: indexPath) as! AolSubTopicCell
            cell.descriptionLabel.text = service.subTopicServiceDescription
        }
        
        cell.title.text = service.subTopicServiceTitle
        
        cell.onCollapseButtonTapped = {
            self.collapseCell(collapse: !service.isCollapsed, index: indexPath, cell: cell)
            
            self.subTopicsTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        
        }
        cell.onDetailsButtonTapped = {
            self.serviceSelectedDelegate?.selectedService(service: service, subTopicTitle: self.subTopics[indexPath.section].subTopicTitle)
        }
        self.collapseCell(collapse: service.isCollapsed, index: indexPath, cell: cell)
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHooks()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupHooks(){
        viewModel!.viewData.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] topicDetails in
                if(topicDetails == nil){
                    //TODO: should do somting ????
                }else{
                    self.subTopicsTableView.reloadData()
                    //self.title = topicDetails?.topicTitle
                    self.setNavigationBar(title: (topicDetails?.topicTitle)!, willSetSearchButton: true)
                    self.disposeBag = DisposeBag()
                }
            })
            .disposed(by: disposeBag)
    }
    
  
    
    func collapseCell(collapse: Bool,index:IndexPath,cell: AolSubTopicCell){
        if collapse{
            self.subTopics[index.section].subTopicServices[index.row].isCollapsed = true
            cell.collapseBtn.setTitle("O", for: .normal)
        }else{
            self.subTopics[index.section].subTopicServices[index.row].isCollapsed = false;
            cell.collapseBtn.setTitle("I", for: .normal)
            
        }
    }
}

