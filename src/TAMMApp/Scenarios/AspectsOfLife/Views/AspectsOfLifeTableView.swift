//
//  AspectsOfLifeTableView.swift
//  TammUI
//
//  Created by Daniel on 4/11/18.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit
protocol AspectsOfLifeTableViewDelegate: class{
    func selectedAspectsAtIndex(index:Int)
}
class AspectsOfLifeTableView:UIView, UITableViewDelegate, UITableViewDataSource{

    weak var delegate: AspectsOfLifeTableViewDelegate?
    public var aspectsOfLife:[AspectOfLifeViewData] = []
    public var ongoingAspectsOfLife:[JourneyAspectOfLifeViewData] = []{
        didSet {
            if(ongoingAspectsOfLife.count > 0){
                willDisplayonGoing = true
                numberOfSections = 2
                let nibName = "OnGoingAspectsOfLifeCellView"
                let nib = UINib(nibName: nibName, bundle: nil)
                self.tableView?.register( nib, forCellReuseIdentifier: onGoingReuseIdentifier)
            }
        }
    }
    fileprivate var numberOfSections = 1
    fileprivate var willDisplayonGoing:Bool = false
    public var tableView:UITableView!
    fileprivate let defaultReuseIdentifier = "AspectsOfLife"
    fileprivate let onGoingReuseIdentifier = "OnGoingAspectsOfLife"
    public var onGoingHeaderView:OnGoingHeaderView!
    public var aspectsOfLifeHeaderView:OnGoingHeaderView!
    public let defaultCellHeight = 108
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    fileprivate func commonInit(){
        

        
        tableView = Bundle.main.loadNibNamed("AspectsOfLifeTableViewView", owner: self, options: nil)?.first as! UITableView //UITableView(frame: CGRect.zero, style: .grouped)
        tableView.frame = self.bounds
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 108.0 // screenWidth *  384.0 / 108.0
        
        self.addSubview(tableView)
        UIView.setFillingConstraints(view:self, childView:self.tableView)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.allowsSelection = true
        let nibName = "AspectsOfLifeTableCellView"
        let nib = UINib(nibName: nibName, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: defaultReuseIdentifier)

        self.tableView.separatorStyle = .none
        self.tableView.separatorInset = UIEdgeInsetsMake(0, tableView.bounds.width, 0, 0)
        self.tableView.separatorColor = .clear
        
    }
//    func addTableConstraints(){
//        self.addConstraint(NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -100))
//        self.addConstraint(NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
//        self.addConstraint(NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0))
//        self.addConstraint(NSLayoutConstraint(item: tableView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0))
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(willDisplayonGoing){
            var headerView: OnGoingHeaderView?
            if(section == 1){
                
                headerView = aspectsOfLifeHeaderView
            }
            else{

                headerView = onGoingHeaderView
            }
            headerView?.widthConstraint.constant = tableView.bounds.width
            headerView?.headerLabel.setNeedsLayout()
            headerView?.setNeedsLayout()
            headerView?.headerLabel.layoutIfNeeded()
            headerView?.layoutIfNeeded()
            let size =
//                headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
//                headerView?.systemLayoutSizeFitting(UIScreen.main.bounds, withHorizontalFittingPriority: UILayoutPriority.defaultHigh, verticalFittingPriority: UILayoutPriority.defaultLow)
                headerView?.systemLayoutSizeFitting(UILayoutFittingCompressedSize)

//            tableView.estimatedSectionHeaderHeight =  CGFloat(HeaderHeight)
//            return  UITableViewAutomaticDimension // CGFloat(HeaderHeight)
            return (size?.height) ?? UITableViewAutomaticDimension
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(willDisplayonGoing){
            if(section == 1){
                return aspectsOfLifeHeaderView
            }
            else{
                return onGoingHeaderView
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(willDisplayonGoing){
            if(section == 1){
                return aspectsOfLife.count
            }
            else{
                return ongoingAspectsOfLife.count
            }
        }
        return aspectsOfLife.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        cell.separatorInset = UIEdgeInsetsMake(0, self.bounds.width, 0, 0)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if(willDisplayonGoing){
//            if(indexPath.section == 1){
//                return CGFloat(defaultCellHeight)
//            }
//            else{
//                return CGFloat(onGoingCellHeight)
//            }
//        }
//        return UITableViewAutomaticDimension
//        
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(willDisplayonGoing){
            if(indexPath.section == 1){
                return initiateDefaultCell(indexPath: indexPath)
            }
            else{
                return initiateOnGoingCell(indexPath: indexPath)
            }
        }
        return initiateDefaultCell(indexPath: indexPath)
    }
    
    fileprivate func initiateDefaultCell(indexPath:IndexPath)->AspectsOfLifeTableCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifier, for: indexPath) as! AspectsOfLifeTableCell
        let item = aspectsOfLife[indexPath.row]
        cell.viewData = item
        cell.icon.CircularFontImage = false
        ImageCachingUtil.loadImage(url: item.AspectOfLifeImageUrl, dispatchInMain: true
            , callBack: { (url, image) in
                guard let item = cell.viewData as? AspectOfLifeViewData else {return}
                if item.AspectOfLifeImageUrl == url{
                    cell.icon.Image = image
                }
        })
        //        cell.icon.FontImage = aspectsOfLife[indexPath.row].AspectOfLifeImage
        //        cell.icon.backgroundColor = UIColor.init(hexString: aspectsOfLife[indexPath.row].AspectOfLifeImageColor)
        
        cell.header.text = item.AspectOfLifeHeader
        cell.descriptionTxt.text = item.AspectOfLifeDescription
        return cell
    }
    
    fileprivate func initiateOnGoingCell(indexPath:IndexPath)->UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: onGoingReuseIdentifier, for: indexPath) as! OnGoingAspectsOfLifeCell
        let item = ongoingAspectsOfLife[indexPath.row]
        cell.viewData = item
        cell.icon.CircularFontImage = false
        ImageCachingUtil.loadImage(url: item.AspectOfLifeImageUrl, dispatchInMain: true
            , callBack: { (url, image) in
                guard let item = cell.viewData as? JourneyAspectOfLifeViewData else {return}
                if item.AspectOfLifeImageUrl == url{
                    cell.icon.Image = image
                }
        })
        
        //cell.icon.FontImage = item.AspectOfLifeImage
        //cell.icon.backgroundColor = UIColor.init(hexString: item.AspectOfLifeImageColor)
        cell.headerText.text = item.AspectOfLifeHeader
        cell.setProgress(value: Float(item.completionPercentage))
        return cell
    }
    
    func changeTable(items:[AspectOfLifeViewData]){
        aspectsOfLife = items
        tableView.reloadData()
    }
    
    func displayMoreItems(items:[AspectOfLifeViewData]){
        var indexPaths:[IndexPath] = []
        var index = 0
        for _ in items
        {
            indexPaths.append(IndexPath(row: aspectsOfLife.count + index, section: 0))
            index += 1
        }
        aspectsOfLife.append(contentsOf: items)
        tableView.beginUpdates()
        tableView.insertRows(at: indexPaths, with: .automatic)
        tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !willDisplayonGoing{
            tableView.deselectRow(at: indexPath, animated: true)
            delegate?.selectedAspectsAtIndex(index: indexPath.row)
        }else{
            tableView.deselectRow(at: indexPath, animated: true)
            //TODO: do desired action
        }
        
        
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        tableView.beginUpdates()
        tableView.reloadData()
        
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    
}


