//
//  AspectsOfLifeViewController.swift
//  TammUI
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Daniel. All rights reserved.
//

import UIKit

protocol TopTappedViewControllerDelegate: class{
    func openFirstTab()
    func openSecondTab()
}
class TopTappedViewController: ContainerViewController, TopTappedStoryboardLoadable{
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnServices: LocalizedSelectableButton!
    @IBOutlet weak var btnJourneys: LocalizedSelectableButton!
    public weak var delegate: TopTappedViewControllerDelegate?
    
    public var viewModel: TopTappedViewModel!
    
    
    fileprivate let selectedButtonBorder:CGFloat = 4
    fileprivate let stackViewBottomBorder:CGFloat = 1
    fileprivate let unSelectedFont:UIFont = UIFont.init(name: "CircularStd-Medium", size: 18)!
    fileprivate let selectedButtonBorderColor:String = "#10596F"
    fileprivate let stackViewBottomBorderColor:String = "#CAC8D1"
    fileprivate var layer:CALayer?
    
    
    private var embeddedViewController: UIViewController?
    @objc var cview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "Services"
        self.cview = containerView

        
        
        
        btnJourneys.originalFont = unSelectedFont
        btnServices.originalFont = unSelectedFont
        btnJourneys.fontProxy = unSelectedFont
        btnServices.fontProxy = unSelectedFont
        
        
        btnJourneys.RTLFont = "Swissra-Normal"
        btnJourneys.RTLSelecttedFont = "Swissra-Bold"
        
        
        btnServices.RTLFont = "Swissra-Normal"
        btnServices.RTLSelecttedFont = "Swissra-Bold"
        
        
        btnJourneys.selectedFontReference = UIFont.init(name: "Swissra-Normal-Bold", size: 18)
        btnServices.selectedFontReference = UIFont.init(name: "Swissra-Bold", size: 18)
        
        btnJourneys.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnServices.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnJourneys.selectedColor = UIColor.init(hexString: "#11596F")
        btnServices.selectedColor = UIColor.init(hexString: "#11596F")
        
        btnJourneys.originalColor = UIColor.init(hexString: "#151331")
        btnServices.originalColor = UIColor.init(hexString: "#151331")
        
        btnJourneys.originalColor = UIColor.init(hexString: "#151331")

        
        
        self.setNavigationBar(title: viewModel.pageHeader)

        registerContentViewFrameObserver()
        
    }
    
    private var firstTimeLaunch = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // set first tab title
//        btnServices.setTitle(viewModel.firstTabTitle, for: [.normal, .selected, .highlighted, .focused])
        btnServices.titleLabel?.text = viewModel.firstTabTitle
        // set second tab title
//        btnJourneys.setTitle(viewModel.secondTabTitle, for: [.normal, .selected, .highlighted, .focused])
        btnJourneys.titleLabel?.text = viewModel.secondTabTitle
        btnServices.updateFontAndColor()
        btnJourneys.updateFontAndColor()
        
        if(firstTimeLaunch){
            buttonIsSelected(button: btnServices, openJournyes: false)
            firstTimeLaunch = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addBorderToStackView()
//        self.setNavigationBar(title: L10n.servicesTitle)
        
        adjustEmbeddedControllerSize()
    }
    
    func rightButtonAction(sender: UIBarButtonItem){
        
    }
    
    
    @IBAction func btnServicesClicked(_ sender: LocalizedSelectableButton) {
        buttonIsSelected(button: sender, openJournyes: false)
    }
    @IBAction func btnJourneysClickes(_ sender: LocalizedSelectableButton) {
        buttonIsSelected(button: sender, openJournyes: true)
    }
    
    fileprivate func addBorderToStackView(){
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: stackView.frame.height - stackViewBottomBorder, width: stackView.frame.width, height: stackViewBottomBorder)
        
        bottomBorder.backgroundColor = UIColor.init(hexString: stackViewBottomBorderColor).cgColor
        
        stackView.layer.addSublayer(bottomBorder)
    }
    fileprivate func buttonIsSelected(button:LocalizedSelectableButton, openJournyes:Bool){
        if(layer != nil){
            layer?.removeFromSuperlayer()
        }
        
        btnJourneys.isButtonSelected = false
        btnServices.isButtonSelected = false
        
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: button.frame.height - selectedButtonBorder, width: button.frame.width, height: selectedButtonBorder)
        bottomBorder.backgroundColor = UIColor.init(hexString: selectedButtonBorderColor).cgColor
        
        button.layer.addSublayer(bottomBorder)
        
        button.isButtonSelected = true
        layer = bottomBorder
        if(openJournyes){
            delegate?.openSecondTab()
            self.setNavigationBar(title: viewModel.secondHeader)

        }
        else{
            delegate?.openFirstTab()
            self.setNavigationBar(title: viewModel.firstHeader)
        }
        
    }
    
    func removeEmbeddedController(){
        if let embeddedViewController = embeddedViewController{
            embeddedViewController.willMove(toParentViewController: nil)
            self.selectedViewController = nil
            embeddedViewController.view.removeFromSuperview()
            embeddedViewController.didMove(toParentViewController: nil)
            
        }
    }
    fileprivate func adjustEmbeddedControllerSize() {
        if let embeddedViewController = embeddedViewController{

            embeddedViewController.view.frame = self.containerView.bounds
//            CGRect(origin: embeddedViewController.view.frame.origin, size: self.containerView.frame.size)
        }
    }
    
    
    
    func embeddViewController(controller: UIViewController){
        
        controller.willMove(toParentViewController: self)
        
        embeddedViewController = controller
        adjustEmbeddedControllerSize()

        self.selectedViewController = controller
        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.containerView.addSubview(controller.view)
        self.addChildViewController(controller)

        controller.didMove(toParentViewController: self)
//        adjustEmbeddedControllerSize()

    }
    
    
    let containerViewFrameKeyPath = #keyPath(cview.frame)
    
    fileprivate func registerContentViewFrameObserver() {
        //        print(fontProxy?.fontName)
        self.addObserver(self, forKeyPath: #keyPath(cview.frame), options: [.old, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(cview.frame) {
            adjustEmbeddedControllerSize()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: #keyPath(cview.frame) )
    }
    
}
