//
//  FloatingMenuView.swift
//  TAMMApp
//
//  Created by kerolos on 4/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
//import Macaw

enum MenuItem{
    case chat
    case message
    case text
    case incident
    case faq
    case close
}

protocol FloadingMenuViewDelegate: class {
    func didSelectMenueItem(item: MenuItem)
}

class PassThroughView: UIView {
    
    var allowedSubviews : [UIView]? = []
   
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in allowedSubviews ?? [] {
            
            if self.subviews.contains(subview) && !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}

class FloatingMenuView: PassThroughView {
    
    var contentView: UIView!
    var contentView2: UIView!
    private static var isEnabled:Bool = true
    private static var floatingMenuView: FloatingMenuView?
    var draggableContentView: PassThroughView!
    
    var floatingMenu : NewFloatingMenuView!
    var draggableButton: UICircularImage!
    weak var delegate : FloadingMenuViewDelegate?
    var currentLocation: CGPoint?
    var panGesture: UIPanGestureRecognizer?
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    var heightOfTabbar: CGFloat = 0 {
        didSet{
            if oldValue != heightOfTabbar{
                let b = self.bounds
                
                let b2 = CGRect(x: b.minX, y: b.minY, width: b.width, height: b.height - heightOfTabbar)
                
                contentView2.frame = b2
                draggableContentView.frame = b2
            }
        }
    }
    
    private func commonInit(){
        FloatingMenuView.floatingMenuView = self
        let b = self.bounds
        contentView = UIView(frame: b)
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(stopOpenMenuAnimation)))

        let b2 = CGRect(x: b.minX, y: b.minY, width: b.width, height: b.height - heightOfTabbar)

        contentView2 = UIView(frame: b2)
        contentView.clipsToBounds = true
        floatingMenu = NewFloatingMenuView.loadView()
        
        
        initiateDraggableButton()
        setDraggableButton()
        FloatingMenuView.isEnabled ? (self.alpha = 1) : (self.alpha = 0.3)
      
    }
    @objc func bringToFront(){
        self.bringSubview(toFront: draggableButton)
    }

    static func enableDisable(isEnabled: Bool){
        FloatingMenuView.isEnabled = isEnabled
        FloatingMenuView.isEnabled ? (FloatingMenuView.floatingMenuView?.alpha = 1) : (FloatingMenuView.floatingMenuView?.alpha = 0.3)
    }
    
    
    func initiateDraggableButton(){
        let b = self.bounds
        let b2 = CGRect(x: b.minX, y: b.minY, width: b.width, height: b.height - heightOfTabbar)
        
        draggableContentView = PassThroughView(frame: b2)
        self.addSubview( draggableContentView )
        draggableContentView.clipsToBounds = true
        
    
        
        var panGesture = UIPanGestureRecognizer()
        var tapGesture = UITapGestureRecognizer()
        let origin = loadCurrentPosition()!
        
        let initialPosition =  CGPoint(x: Double(UIScreen.main.bounds.width - 80), y:Double(UIScreen.main.bounds.height) - 30)
        
        draggableButton = UICircularImage(frame:CGRect(x: origin.x, y: initialPosition.y, width: 60.0, height: 60.0))
        draggableButton.alpha = 0
        UIView.animate(withDuration: Dimensions.animationDuration1, delay: Dimensions.animationDuration1, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.draggableButton.center = CGPoint(x:origin.x + self.draggableButton.bounds.width / 2, y: origin.y - Dimensions.bounceHeight + self.draggableButton.bounds.height / 2 )
            
            self.draggableButton.alpha = 1
        }) { (finished) in
             UIView.animate(withDuration: Dimensions.bounceAnimationTime, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.draggableButton.center = CGPoint(x:origin.x + self.draggableButton.bounds.width / 2, y: origin.y + self.draggableButton.bounds.height / 2 )
             }) { (finished) in
            self.draggableButton.center = CGPoint(x:origin.x + self.draggableButton.bounds.width / 2, y: origin.y + self.draggableButton.bounds.height / 2 )
            }
        }
        

        
        
        allowedSubviews? = [ //fanMenu ,
                            draggableContentView,
                            contentView,
                             draggableButton]
        currentLocation = draggableButton.frame.origin
        draggableButton.EnableDragging = true
        draggableButton.CircularFontImage = true
        draggableButton.FontImage = "p"
        draggableButton.FontName = "tamm"
        draggableButton.FontSize = 40
        draggableButton.backgroundColor = UIColor.init(hexString: "#034D63")
        draggableButton.FontImageColor = UIColor.white
        // shadow #B8B8C1
        self.draggableContentView.addSubview(draggableButton)
        self.draggableContentView.allowedSubviews = [draggableButton]
        draggableButton.isUserInteractionEnabled = true;
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView(_:)))
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(displayFloatingMenu(_:)))
        draggableButton.isUserInteractionEnabled = true
        draggableButton.addGestureRecognizer(tapGesture)
        draggableButton.addGestureRecognizer(panGesture)
    }
    
    func setDraggableButton(){
        draggableButton.layer.cornerRadius = draggableButton.frame.width/2
        draggableButton.clipsToBounds = true
        draggableButton.layer.shadowColor = UIColor.init(hexString: "#B8B8C1").cgColor
        draggableButton.layer.shadowRadius = 5
        draggableButton.layer.shadowOffset = CGSize(width:0, height:0);
        draggableButton.layer.masksToBounds = false;
    }
    

    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        self.bringSubview(toFront: self.draggableButton)
        let translation = sender.translation(in: self)
        let oldPosition = self.draggableButton.frame.origin
        self.draggableButton.center = CGPoint(x: self.draggableButton.center.x + translation.x, y: self.draggableButton.center.y + translation.y)
        if(self.draggableButton.frame.origin.x < 0 || self.draggableButton.frame.origin.x + self.draggableButton.frame.width > self.draggableContentView.bounds.width){
            self.draggableButton.frame = CGRect(origin: CGPoint(x:oldPosition.x , y:self.draggableButton.frame.origin.y ), size: self.draggableButton.frame.size)
            
        }
        if(self.draggableButton.frame.origin.y < 0 || self.draggableButton.frame.origin.y + self.draggableButton.frame.height > self.draggableContentView.bounds.height){
            self.draggableButton.frame = CGRect(origin: CGPoint(x:self.draggableButton.frame.origin.x , y:oldPosition.y ), size: self.draggableButton.frame.size)
        }
        sender.setTranslation(CGPoint.zero, in: self)
        currentLocation = draggableButton.frame.origin
        saveCurrentPosition()

    }
    
    @objc func displayFloatingMenu(_ sender:UITapGestureRecognizer){
        if FloatingMenuView.isEnabled{
            self.bringSubview(toFront: self.draggableButton)
            startOpenMenuAnimation()
        }
    }
    
    fileprivate func startOpenMenuAnimation(){
        floatingMenu.removeFromSuperview()
        contentView2.removeFromSuperview()
        contentView.removeFromSuperview()

        let b = self.bounds
        let b2 = CGRect(x: b.minX, y: b.minY, width: b.width, height: b.height - heightOfTabbar)
        contentView.frame = b
        contentView2.frame = b2
        floatingMenu.frame = b2
        
        self.addSubview(contentView)
        
        self.contentView.addSubview(contentView2)
        
        self.contentView2.addSubview(floatingMenu)
        contentView2.clipsToBounds = true
        
        

        self.backgroundColor = .clear

        let height = floatingMenu.getMenuHeight()
        
        floatingMenu.center = floatingMenu.center.applying(CGAffineTransform.init(translationX: 0, y: height))
    
        floatingMenu.delegate = self
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.4,  animations: {
//            self.draggableButton.center = self.contentView.getCloseButtonCenter()
            self.floatingMenu.center = self.floatingMenu.center.applying(CGAffineTransform.init(translationX: 0, y: -height))
            self.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6326519692)

            self.layoutIfNeeded()
        }, completion:  {_ in
//            self.draggableButton.isHidden = true
            UIView.animate(withDuration: 0.1, animations: {
            }, completion: nil)
 
        })
    }
    
    @objc fileprivate func stopOpenMenuAnimation(){
//        self.draggableButton.isHidden = false
        contentView.frame = self.bounds
        let height = floatingMenu.getMenuHeight()

        
        UIView.animate(withDuration: 0.1,  animations: {
//            self.draggableButton.frame = CGRect(origin: self.currentLocation!, size: self.draggableButton.frame.size)
            self.backgroundColor = .clear
            self.floatingMenu.center = self.floatingMenu.center.applying(CGAffineTransform.init(translationX: 0, y: height))
            self.layoutIfNeeded()
            
        }, completion: {
            finished in
                self.floatingMenu.removeFromSuperview()
                self.contentView.removeFromSuperview()
        })
    }
    
    fileprivate func saveCurrentPosition(){
        // for production usage consider using the OS Keychain instead
        let archivedOrigin = NSKeyedArchiver.archivedData(withRootObject: currentLocation)
        UserDefaults.standard.set(archivedOrigin, forKey: "MenuLocation")
        
        UserDefaults.standard.synchronize()
    }
    
    fileprivate func loadCurrentPosition() -> CGPoint?{
        // loads OIDAuthState from NSUSerDefaults
        guard let archivedOrigin = UserDefaults.standard.object(forKey: "MenuLocation") as? Data else{
            return CGPoint(x: Double(UIScreen.main.bounds.width - 80), y:Double(UIScreen.main.bounds.height - 130) - 30)
        }
        
        guard let Origin = NSKeyedUnarchiver.unarchiveObject(with: archivedOrigin) as? CGPoint else{
            return CGPoint(x: Double(UIScreen.main.bounds.width - 80), y:Double(UIScreen.main.bounds.height - 130) - 30)
        }
        
        return CGPoint( x: max(0,Origin.x), y: max(0, Origin.y) )
    }
    
}

extension FloatingMenuView: NewFloatingMenuViewDelegate{
    func didSelectItemWithId(_ actionId: NewFloatingMenuItems) {
        switch actionId {
        case .chat:
            _ = Toast(message: L10n.commingSoon).Show()
            break;
        case .feedback:
            stopOpenMenuAnimation()
            //_ = Toast(message: "Feedback is Clicked").Show()
             let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.openFeedbackView()
            break;
        case .incident:
            AppDelegate.appCoordinator.startTalkToUsWithIncident()
            stopOpenMenuAnimation()
            break;
        case .message:
            _ = Toast(message: L10n.commingSoon).Show()
            break;
        case .quickPay:
            _ = Toast(message: L10n.commingSoon).Show()
            break;
        default:
            stopOpenMenuAnimation()
            break;
            
        }
        
    }
}

extension AppDelegate{
    func openFeedbackView(){
        let v = FeedbackView.loadView()
        v.frame = (AppDelegate.window?.rootViewController?.view.bounds)!
        v.viewModel = AppDelegate.container.resolve(FeedbackViewModel.self)!
        AppDelegate.window?.rootViewController?.view.addSubview( v )
        self.removeFloatingMenu()
    }
}
