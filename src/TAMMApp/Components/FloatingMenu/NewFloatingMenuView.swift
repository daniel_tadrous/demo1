//
//  NewFloatingMenuView.swift
//  TAMMApp
//
//  Created by kerolos on 5/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

enum NewFloatingMenuItems{
    case incident
    case message
    case feedback
    case chat
    case quickPay
    case close
}

protocol NewFloatingMenuViewDelegate: class{
    func didSelectItemWithId(_: NewFloatingMenuItems)
}

class NewFloatingCircleView: UIView{
    private func applyCircleCorners(){
        self.layer.cornerRadius = self.frame.width / 2
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.applyCircleCorners()
    }
}

class NewFloatingMenuView: UIView {
    
    @IBOutlet weak var closeLabel: StyledLabel!
    @IBOutlet weak var quickPayView: UIImageView!
    @IBOutlet weak var chatView: UIImageView!
    @IBOutlet weak var feedbackView: UIImageView!
    @IBOutlet weak var messageView: UIImageView!
    @IBOutlet weak var incidentView: UIImageView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var menuView: UIView!
    
    @IBAction func outsideClickHandler(_ sender: UIButton) {
        closeClicked()
    }
    weak var delegate: NewFloatingMenuViewDelegate?
    var actionViews: [UIView]!
    
    
    static func loadView() -> NewFloatingMenuView {
        return Bundle.main.loadNibNamed("NewFloatingMenuView", owner: nil, options: nil)?.first as! NewFloatingMenuView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    private func commonInit(){
        
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        if (isRTL){
            quickPayView.image = #imageLiteral(resourceName: "QuickPay_ar")
            chatView.image = #imageLiteral(resourceName: "Chat_ar")
            feedbackView.image = #imageLiteral(resourceName: "Feedback_ar")
            messageView.image = #imageLiteral(resourceName: "Message_ar")
            incidentView.image = #imageLiteral(resourceName: "Incident_ar")
        } else{
            quickPayView.image = #imageLiteral(resourceName: "QuickPay")
            chatView.image = #imageLiteral(resourceName: "Chat")
            feedbackView.image = #imageLiteral(resourceName: "Feedback")
            messageView.image = #imageLiteral(resourceName: "Message")
            incidentView.image = #imageLiteral(resourceName: "Incident")
        }
        
        
        
        actionViews =
            [closeLabel,
             quickPayView,
             chatView,
             feedbackView,
             messageView,
             incidentView]
        
        let selectors: [Selector] = [
        #selector(closeClicked),
        #selector(quickPayClicked),
        #selector(chatClicked),
        #selector(feedbackClicked),
        #selector(messageClicked),
        #selector(incidentClicked)
        ]
        
//        for  v in actionViews{
//            let gesture = UITapGestureRecognizer(target: self, action: #selector(buttonClicked(recognizer:)) )
//            v.addGestureRecognizer(gesture)
//        }
        for var x in 0..<actionViews.count{
            
            let gesture = UITapGestureRecognizer(target: self, action: selectors[x])
            actionViews[x].isUserInteractionEnabled = true
            actionViews[x].addGestureRecognizer(gesture)
        }
        
    }
    @objc func closeClicked(){
        delegate?.didSelectItemWithId(.close)
    }
    
    @objc func quickPayClicked(){
        delegate?.didSelectItemWithId(.quickPay)
    }
    
    @objc func chatClicked(){
        delegate?.didSelectItemWithId(.chat)
    }
    
    @objc func feedbackClicked(){
        delegate?.didSelectItemWithId(.feedback)
    }
    
    @objc func messageClicked(){
        delegate?.didSelectItemWithId(.message)
    }
    
    @objc func incidentClicked(){
        delegate?.didSelectItemWithId(.incident)
    }
//    @objc func buttonClicked(recognizer: UITapGestureRecognizer){
//        guard let crrView = recognizer.view else{ return }
//
//        switch crrView{
//        case closeLabel: delegate?.didSelectItemWithId(.close)
//        case quickPayView: delegate?.didSelectItemWithId(.quickPay)
//        case chatView: delegate?.didSelectItemWithId(.chat)
//        case feedbackView: delegate?.didSelectItemWithId(.feedback)
//        case messageView: delegate?.didSelectItemWithId(.message)
//        case incidentView: delegate?.didSelectItemWithId(.incident)
//
//        default:
//            delegate?.didSelectItemWithId(.close)
//        }
//    }
    
    func getCloseButtonCenter() -> CGPoint{
        return CGPoint(x: menuView.frame.origin.x + closeLabel.center.x, y:  menuView.frame.origin.y + closeLabel.center.y)
    }
    
    func getMenuHeight() -> CGFloat{
        return menuView.bounds.maxY
    }
    
}

class NewFloatingMenuViewDemo : UIView{
    
    var view: NewFloatingMenuView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.view = NewFloatingMenuView.loadView()
        self.view.frame = self.bounds
        self.addSubview(self.view);    // adding the top level view to the view hierarchy
    }
}

class NewFloatingMenuViewDemoViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.view.frame = self.bounds
        self.view = NewFloatingMenuView.loadView()
        
    }
}


