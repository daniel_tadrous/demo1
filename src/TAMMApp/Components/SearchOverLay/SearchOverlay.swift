//
//  SearchOverlay.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift



protocol SearchOverlayViewDelegate {
    //implemented by View Controller
    func eventIsSelected(message:TalkToUsItemViewDataType)
    func backPressed()
}





class SearchOverlayView: UIView, UITammStackViewDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTxt: LocalizedTextField!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var micBtn: UIButton!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var view: UIView!
    @IBOutlet weak var didYouMeanLbl: UILabel!
    @IBOutlet weak var suggestionsStackView: UITammStackView!
    @IBOutlet weak var trendingStack: UITammStackView!
    @IBOutlet weak var trendingStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var refreshBtn: UIButton!
    
    public var delegate:SearchOverlayViewDelegate!
    public let disposeBag = DisposeBag()
    var currentViewModel:SearchViewModelDelegate!
    var currentSearchModel:SeacrhViewModel!
    
    private var text:String!
    
    public var searchItems:[TalkToUsItemViewDataType] = [] {
        didSet{
            self.adjustStackView(view: suggestionsStackView, items: searchItems, heightConstraint: stackViewHeightConstraint)
        }
    }
    
    public var trendingItems:[TalkToUsItemViewDataType] = [] {
        didSet{
            self.adjustStackView(view: trendingStack, items: trendingItems, heightConstraint: trendingStackHeightConstraint)
        }
    }
    
    
    
    @IBAction func refreshClickHandler(_ sender: UIButton) {
        self.currentViewModel.isToGetTrending.value = false
        self.currentViewModel.isToGetTrending.value = true
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    init(frame: CGRect,currentViewModel:SearchViewModelDelegate,text:String,currentSearchModel:SeacrhViewModel) {
        super.init(frame:frame)
        self.currentViewModel = currentViewModel
        self.currentSearchModel = currentSearchModel
        self.text = text
        commonInit()
        setupHooks()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("EventsSearchOverlayView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        
        didYouMeanLbl.addCharacterSpacing(space: 2.3)
        self.didYouMeanLbl.isHidden = true
        
        backBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        searchTxt.attributedPlaceholder = NSAttributedString(string: L10n.searchEllipsized, attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 155/255, green: 155/255, blue: 155/255, alpha: 1) , NSAttributedStringKey.font : UIFont.init(name: "CircularStd-Book", size: 16)!])
    }
    
    func displayBlockingToast(message:String, duration: Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
//        let alert = UILabel(frame: CGRect(x: UIScreen.main.bounds.width/4, y: UIScreen.main.bounds.height/4, width: UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.height/2))
        //alert.center = CGPoint(x: 160, y: 285)
//        alert.textAlignment = .center
//        alert.text = message
//        alert.font.withSize(30)
//        alert.backgroundColor = UIColor.white
//        alert.addRoundCorners(radious: 20)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
//            UIApplication.shared.keyWindow!.addSubview(alert)
//            UIApplication.shared.keyWindow!.bringSubview(toFront: alert)
              alert.show()
            //self.view.displayToast(message: message, Fortime: self.currentSearchModel.speechDuration)
        }
        // duration in seconds
        //        let duration: Double = duration
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
          //  alert.removeFromSuperview()
            //alert.dismiss(animated: true, completion: nil)
            alert.dismiss(animated: true, completion: nil)
            let txt = self.searchTxt.text
            if txt != nil && txt != ""{
                self.searchTxt.becomeFirstResponder()
                self.searchTxt.text = txt
            }
        }
        
        

        
    }
    
    @IBAction func micBtnClicked(_ sender: Any) {
        if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: currentSearchModel.speechDuration)
            //currentSearchModel.startSpeechService()
            
            self.currentSearchModel.startSpeechService().asObservable().subscribe(onNext: { (str:String?) in
                self.searchTxt.text = str
                self.searchTextChanged()
            }).disposed(by: disposeBag)
        }
        
    }
    
    
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //
    //        //delegate?.suggestionIsSelected(message: self.searchTxt.text!)
    //        return true
    //    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.removeFromSuperview()
        delegate?.backPressed()
    }
    
    
    func itemIsSelected(message: TalkToUsItemViewDataType) {
        delegate.eventIsSelected(message: message)
        self.removeFromSuperview()
    }
    func setupHooks(){
        searchTxt.addTarget(self, action: #selector(searchTextChanged), for: .editingChanged)
        searchTxt.text = self.text
        self.currentViewModel.isToGetTrending.value = true
        currentViewModel.trendingVar.asObservable().subscribe(onNext: { (ar:[String]?) in
            if ar != nil{
                self.trendingItems =   (ar?.map({ (s:String) -> TalkToUsItemViewDataType in
                    return PoppularItemViewData(name: s)
                }))!
            }else{
                
            }
        }).disposed(by: disposeBag)
        currentViewModel.queryResultVar.asObservable().subscribe(onNext: { (ar:[String]?) in
            if ar != nil{
                self.searchItems =   (ar?.map({ (s:String) -> TalkToUsItemViewDataType in
                    return PoppularItemViewData(name: s)
                }))!
            }else{
                
            }
        }).disposed(by: disposeBag)
    }
    
    @objc func searchTextChanged() {
        if (searchTxt.text?.isEmpty)! || (searchTxt.text?.count ?? 0) < 3{
            searchItems = []
        }else{
            self.currentViewModel.currentQueryVar.value = self.searchTxt.text
        }
    }
    private func adjustStackView(view:UITammStackView , items:[TalkToUsItemViewDataType] , heightConstraint:NSLayoutConstraint){
        view.items = items
        if view.tag == 1{
            if(items.count == 0){
                self.didYouMeanLbl.isHidden = true
            }
            else{
                self.didYouMeanLbl.isHidden = false
            }
        }
        view.layoutSubviews()
        view.resizeToFitSubviews()
        view.delegate = self
        heightConstraint.constant = view.frame.height
        self.stackView.layoutSubviews()
        self.stackView.setNeedsLayout()
        self.stackView.setNeedsUpdateConstraints()
    }
}

extension SearchOverlayView : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate.eventIsSelected(message: PoppularItemViewData(name: textField.text!) )
        self.removeFromSuperview()
        return true
    }
}


public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindowLevelAlert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
