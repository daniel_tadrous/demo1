//
//  AoIDetailsParentViewModel.swift
//  TAMMApp
//
//  Created by marina on 6/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
class AoIDetailsParentViewModel {
    static var titleStatic:String?
    static var idStatic:String?
    var title:String?{
        get{
            return AoIDetailsParentViewModel.titleStatic
        }
        set{
            AoIDetailsParentViewModel.titleStatic = newValue
        }
    }
    var id:String?{
        get{
            return AoIDetailsParentViewModel.idStatic
        }
        set{
            AoIDetailsParentViewModel.idStatic = newValue
        }
    }
}
