//
//  AolServiceFaqCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import UICheckbox_Swift

class CollaborateCell: UITableViewCell {
    
    
    @IBOutlet weak var shareYourIdeaBtn: LocalizedButton!
    var delegate: ISubmitCollaborate?
    @IBOutlet weak var phoneTF: LocalizedTextField!
    
    @IBOutlet weak var emailTF: LocalizedTextField!
    var collaborate:CollaborateModel = CollaborateModel()
    
    @IBOutlet weak var phoneStackview: UIStackView!
    
    
    @IBAction func shareIdeaBtnClickHandler(_ sender: LocalizedButton) {
        collaborate.email = emailTF.text
        collaborate.detailsText = addDetailsTextView.text
        collaborate.phone = phoneTF.text
        //todo add audio
        if !self.isValidForm(){
            return
        }
        
        
        self.delegate?.submitCollaborate(collaborate: collaborate, onSuccess: { [weak self] in
            self?.collaborate = CollaborateModel()
            //self?.emailTF.text = ""
            self?.addDetailsTextView.text = ""
            self?.phoneTF.text = ""
            self?.checkbox.isSelected = false
            self?.miniPlayerDidPressDelete()
            //Toast.init(message: "Thank you for sharing your idea").Show()
            
        })
        
 
        
    }
    func isValidForm()->Bool{
        
        if !(collaborate.email?.isValidEmail())!{
            self.emailValidation.isHidden = false
            self.conatainerView.resizeToFitSubviews()
            self.delegate?.updateCollaborateCell()
            return false
        }else{
            self.emailValidation.isHidden = true
            self.conatainerView.resizeToFitSubviews()
            self.delegate?.updateCollaborateCell()
        }
        
        if voiceFileURL == nil{
            if (collaborate.detailsText?.isEmpty)!{
                self.detailsValidation.isHidden = false
                self.conatainerView.resizeToFitSubviews()
                self.delegate?.updateCollaborateCell()
                return false
            }else{
                self.detailsValidation.isHidden = true
                self.conatainerView.resizeToFitSubviews()
                self.delegate?.updateCollaborateCell()
            }
        }else{
            self.detailsValidation.isHidden = true
            self.conatainerView.resizeToFitSubviews()
            self.delegate?.updateCollaborateCell()
        }
        
        if checkbox.isSelected{
            if !(collaborate.phone?.isValidNumber())!{
                self.phoneValidation.isHidden = false
                self.conatainerView.resizeToFitSubviews()
                self.delegate?.updateCollaborateCell()
                return false
            }else{
                self.phoneValidation.isHidden = true
                self.conatainerView.resizeToFitSubviews()
                self.delegate?.updateCollaborateCell()
            }
        }
        
        return true
    }
    @IBOutlet weak var conatainerView: UIView!
    @IBOutlet weak var recordButtonHoldingView: UIView!
    @IBOutlet weak var recordButton: RecordButton!
    //@IBOutlet weak var makeInfoPublicContainer: UIView!
    @IBOutlet weak var miniPlayerContainer: XibView!
    @IBOutlet weak var pressAndHoldText: LocalizedLabel!
    @IBOutlet weak var addDetailsTextView: UITextView!
    private var voiceFileURL: URL?
    private var playerView: MiniPlayerView{
        return miniPlayerContainer.contentView as! MiniPlayerView
    }
    
    
    
    @IBOutlet weak var checkbox: UICheckbox!
    
    @IBOutlet weak var checkboxLabel: LocalizedLabel!
    
    @IBOutlet weak var emailValidation: LocalizedLabel!
    @IBOutlet weak var detailsValidation: LocalizedLabel!
    
    @IBOutlet weak var phoneNumLabel: LocalizedLabel!
    @IBOutlet weak var phoneValidation: LocalizedLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    func commonInit(){
        recordButton.delegate = self
        addDetailsTextView.delegate = self
        showMiniPlayer(false)
        showRecordButton(true)
        self.shareYourIdeaBtn.addRoundCorners(radious: 17)
        self.conatainerView.addRoundCorners(radious: 5)
        
        checkbox.onSelectStateChanged = { (checkb , isSelected) in
            var font = UIFont(name: L10n.Lang == "en" ? "Roboto-Regular" : "Swissra-Normal", size: 16)
            if isSelected{
                font = UIFont(name: L10n.Lang == "en" ? "Roboto-Bold" : "Swissra-Bold", size: 16)
                self.phoneValidation.isHidden = true
                self.phoneStackview.isHidden = false
            }else{
                self.phoneStackview.isHidden = true
            }
            self.conatainerView.resizeToFitSubviews()
            self.checkboxLabel.font = font
            self.delegate?.updateCollaborateCell()
        }
        
        
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        addDetailsTextView.layer.borderWidth = 1.5
        addDetailsTextView.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        addDetailsTextView.layer.cornerRadius = 4
        
        recordButtonHoldingView.layer.borderWidth = 1.5
        recordButtonHoldingView.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        recordButtonHoldingView.layer.cornerRadius = 31
    }
    fileprivate func enableAddDetailsTextfield(_ enabel: Bool = true){
        addDetailsTextView.isEditable = enabel
    }
    
    fileprivate func showRecordButton(_ show: Bool = true){
        recordButtonHoldingView.isHidden = !show
        pressAndHoldText.isHidden = !show
        
    }
    
    fileprivate func showMiniPlayer(_ show: Bool = true){
        miniPlayerContainer.isHidden = !show
    }
}

extension CollaborateCell: RecordButtonDelegate{
    func recordButtonDidRecordAnAudioFile(url: URL) {
        enableAddDetailsTextfield(false)
        playerView.delegate = self
        voiceFileURL = url
        playerView.miniPlayerVoiceURL = voiceFileURL
        showMiniPlayer()
        showRecordButton(false)
    }
}

extension  CollaborateCell: MiniPlayerViewDelegate{
    func miniPlayerDidPressDelete() {
        showMiniPlayer(false)
        showRecordButton(true)
        enableAddDetailsTextfield(true)
        if voiceFileURL != nil{
            do{
                try FileManager.default.removeItem(at: voiceFileURL!)
                voiceFileURL = nil
            }
            catch let e{
                print(e)
            }
        }
    }
}

extension CollaborateCell: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        
        showRecordButton( numberOfChars < 1 )
        
        // to indecate that the modification is allowed
        
        if ( numberOfChars > Dimensions.yourIdeaTextLimit)
        {
            return false
        }
        return true
    }
}

