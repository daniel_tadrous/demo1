//
//  MySubscriptionsChildViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 9/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class MySubscriptionsChildViewController:  TammViewController,MySubscriptionsChildStoryboardLoadable {
    
    @IBOutlet weak var subscriptionsTableView: UITableView!
    @IBOutlet weak var nosubscriptionsView: UIView!
    @IBOutlet weak var youDontLabel: LocalizedLabel!
    @IBOutlet weak var atTheMomentLabel: LocalizedLabel!
    @IBOutlet weak var noSubscriptionsRoundedView: RoundedView!
    @IBOutlet weak var noSubscriptionsLogoLabel: UILabel!
    
    private var disposeBag = DisposeBag()
    var parentV:MySubscriptionsParentViewController!
    var subscriptionType:SubscriptionType!
    let subscriptionCellIdentifier = "MySubscriptionCell"
    let showMoreCellIdentifier:String = "ShowMoreCell"
    
    var pageCount:Int{
        get{
            return 1 //viewModel.pageCount.value
        }set{
            //viewModel.pageCount.value = newValue
        }
    }
    var subscriptions:[MyDocumentModel]{
        get{
            return []//viewModel.documents.value?.documents ?? []
        }
    }
    var numberOfPages:Int{
        get{
            return 1//viewModel.documents.value?.pageCount ?? 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        subscriptionsTableView.dataSource = self
        subscriptionsTableView.delegate = self
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setViewsColors()
        subscriptionsTableView.reloadData()
        subscriptionsTableView.register(UINib(nibName: subscriptionCellIdentifier, bundle: nil), forCellReuseIdentifier: subscriptionCellIdentifier)
        subscriptionsTableView.register(UINib(nibName: showMoreCellIdentifier, bundle: nil), forCellReuseIdentifier: showMoreCellIdentifier)
    }
    func setViewsColors() {
        let viewBackgroundColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1).getAdjustedColor()
        let labelTextColor = #colorLiteral(red: 0.5259655118, green: 0.5167995691, blue: 0.5816957951, alpha: 1).getAdjustedColor()
        let noDocsRoundedviewBackgroundColor = #colorLiteral(red: 0.8506266475, green: 0.8481912017, blue: 0.8733561635, alpha: 1).getAdjustedColor()
        subscriptionsTableView.backgroundColor = viewBackgroundColor
        nosubscriptionsView.backgroundColor = viewBackgroundColor
        view.backgroundColor = viewBackgroundColor
        youDontLabel.textColor = labelTextColor
        atTheMomentLabel.textColor = labelTextColor
        noSubscriptionsLogoLabel.textColor = labelTextColor
        noSubscriptionsRoundedView.backgroundColor = noDocsRoundedviewBackgroundColor
    }
}

extension MySubscriptionsChildViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        nosubscriptionsView.isHidden = true
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: subscriptionCellIdentifier) as! MySubscriptionCell
        cell.setViewColors()
        return cell
    }
    
    
}
