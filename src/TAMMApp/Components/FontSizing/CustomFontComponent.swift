//
//  OurLabel.swift
//  TAMMApp
//
//  Created by kerolos on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


protocol CustomFontComponent: class {
    
    var fontProxy: UIFont? { get set }
//    var customFontFamilyCode: Int { get }
//    var customFontStyleCode: Int { get }
//    var customFontSize: CGFloat { get }
    var originalFont: UIFont? { get }

//    var customFontForCurrentConfig: UIFont{ get }
    var applyFontScalling: Bool { get }
    
    var RTLFont: String { get }
    
    var textAlignment: NSTextAlignment{ get }
    
    var lineBreakModeProxy: NSLineBreakMode?{ get }
    
    var attributedTextProxy: NSAttributedString? { get set }
    
    var textProxy: String? { get }
    
    var attributedPlaceholderProxy: NSAttributedString? { get set}
    
    var placeholderProxy: String? { get }
    
    var placeholderTextColor: UIColor? { get }
    
}
extension CustomFontComponent where Self : UIView { //} || Self.Base == UITextField.self {
    
    var attributedPlaceholderProxy: NSAttributedString?{
        get {
            return nil
        }
        set {
        }
    }
    
    var placeholderProxy: String? { get {
            return nil
        }
    }
    
    var placeholderTextColor: UIColor? {
        return nil
    }
}

extension CustomFontComponent where Self : UITextField { //} || Self.Base == UITextField.self {
    
    var attributedPlaceholderProxy: NSAttributedString?{
        get {
            return attributedPlaceholder
        }
        set {
            attributedPlaceholder = newValue
        }
    }
    
    var placeholderProxy: String? { get {
            return placeholder
        }
    }
    
    var placeholderTextColor: UIColor? {
        return nil
    }
}

extension CustomFontComponent {

//    var customFontForCurrentConfig: UIFont {
//        get {
//            return UIFont.getCustomFont(customFontFamilyCode: customFontFamilyCode, customFontStyleCode: customFontStyleCode, customFontSize: customFontSize)
//        }
//    }
    
//    func updateFont(){
//        fontProxy = customFontForCurrentConfig
//    }
    

    func adjustFont() {
        //            print("old:", change?[.oldKey])
        //            print("new:", change?[.newKey])
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        adjustFont(RTL: isRTL)
    }
    
    func adjustFont(RTL:Bool){
        let fontSize = (applyFontScalling ? UIFont.getCustomFontSize((originalFont?.pointSize)!) : originalFont?.pointSize)!
        if(RTL){
            let paragraphStyle = NSMutableParagraphStyle()
            let arFont =  UIFont(name: RTLFont, size: fontSize ) ?? UIFont(name: (originalFont?.fontName)!, size: fontSize )
            
            // *** set LineSpacing property in points ***
            paragraphStyle.lineSpacing = fontSize * 0.1 + 2.0
            paragraphStyle.alignment = self.textAlignment
            //            paragraphStyle.maximumLineHeight = fontSize + 10
            paragraphStyle.lineBreakMode = self.lineBreakModeProxy ?? NSLineBreakMode.byTruncatingTail
            
            paragraphStyle.lineHeightMultiple = 1.2 //= UIBaselineAdjustment.alignCenters
            
            //            self.insets = UIEdgeInsets.init(top: 10, left: 0, bottom: 10, right: 0)
            
            var fontAttributes = [
                NSAttributedStringKey.font : arFont,
                NSAttributedStringKey.paragraphStyle: paragraphStyle,
                NSAttributedStringKey.expansion: -0.1
                ] as [NSAttributedStringKey : Any]
            
            self.attributedTextProxy = NSAttributedString(
                string: self.textProxy ?? "",
                attributes: fontAttributes) //string: self.text ?? "",
            //            self.attributedText?. (.font, at: , effectiveRange: )
            
            if let color = placeholderTextColor{
                fontAttributes[ NSAttributedStringKey.foregroundColor] = color
            }
            
            if !(placeholderProxy?.isEmpty ?? true) {
                attributedPlaceholderProxy = NSAttributedString(
                    string: self.placeholderProxy ?? "",
                    
                    attributes: fontAttributes)
            }
            self.fontProxy = UIFont(name: RTLFont, size: fontSize )
        }else{
            fontProxy = UIFont(name: (originalFont?.fontName)!, size: fontSize )
            
            // UIFont.getCustomFont(customFontFamilyCode: originalFont?.fontName, customFontStyleCode: customFontStyleCode, customFontSize: fontSize)
        }
    }
    
    func adjustFont(RTL:Bool, fontReference: UIFont, arFontFamily: String){
        let fontSize = (applyFontScalling ? UIFont.getCustomFontSize((fontReference.pointSize)) : originalFont?.pointSize)!
        if(RTL){
            fontProxy = UIFont(name: arFontFamily, size: fontSize )
        }else{
            fontProxy = UIFont(name: (fontReference.fontName), size: fontSize )
            // UIFont.getCustomFont(customFontFamilyCode: originalFont?.fontName, customFontStyleCode: customFontStyleCode, customFontSize: fontSize)
        }
    }
    
}


