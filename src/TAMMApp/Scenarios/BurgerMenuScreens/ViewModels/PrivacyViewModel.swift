//
//  PrivacyViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class PrivacyViewModel: HtmlViewModel{
    fileprivate var disposeBag = DisposeBag()
    private var apiService: ApiSettingsServicesTypes!
    
    init(apiService:ApiSettingsServicesTypes) {
        super.init()
        self.title = L10n.privacy
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate  func setupBinding(){
        self.loadpage()
    }
    
    fileprivate func loadpage() {
        apiService.getPrivacy().asObservable().subscribe(onNext:{ response in
            self.content.value = response.privacyPolicy
            
        } ).disposed(by: disposeBag)
        
    }
}
