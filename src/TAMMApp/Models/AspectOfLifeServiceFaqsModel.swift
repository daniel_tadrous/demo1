//
//  AspectOfLifeServiceFaqsModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class AspectOfLifeServiceFaqsModel: Mappable {
    var id: String?
    var relatedTo: String?
    var relatedToName: String?
    var relatedToId: String?
    var q: String?
    var a: String?
    var collapsed: Bool = true
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id              <- map["id"]
        relatedTo           <- map["relatedTo"]
        relatedToName     <- map["relatedToName"]
        relatedToId        <- map["relatedToId"]
        q     <- map["q"]
        a        <- map["a"]
    }
}
