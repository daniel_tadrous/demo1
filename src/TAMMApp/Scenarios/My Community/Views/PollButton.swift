//
//  PollButton.swift
//  TAMMApp
//
//  Created by Daniel on 6/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

class PollButton : LocalizedButton{
    var poll : MyCommunityModel.Poll?
    var answer: MessageModel.AnswerModel?
    init(frame: CGRect,fontSize:CGFloat){
        super.init(frame: frame)
        self.fontSize = fontSize
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    var fontSize = CGFloat(16.0)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        self.applyFontScalling = true
        self.RTLFont = "Swissra-Bold"
        self.addRoundCorners(radious: 19)
        self.backgroundColor = UIColor.duckEggBlue
        self.layer.borderColor = UIColor.turquoiseBlue.cgColor
        self.layer.borderWidth = 1
        self.setTitleColor(UIColor.petrol, for: .normal)
        self.titleLabel?.font = UIFont(name: L10n.Lang == "en" ? "CircularStd-Bold" : "Swissra-Bold", size: fontSize)
        self.originalFont = self.titleLabel?.font
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: 40)
    }
    func isSelected(isSelected: Bool){
        if isSelected{
            self.backgroundColor = UIColor.turquoiseBlue
        }else{
            self.backgroundColor = UIColor.duckEggBlue
        }
    }
}
