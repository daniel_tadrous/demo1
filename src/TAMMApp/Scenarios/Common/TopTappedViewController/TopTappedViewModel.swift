//
//  TopTappedViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 7/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation


class TopTappedViewModel{
    
    public private(set) var firstTabTitle: String
    public private(set) var secondTabTitle: String
    
    public private(set) var firstHeader: String
    public private(set) var secondHeader: String
    
    public private(set) var pageHeader: String
    
    static var idStatic: String?
    
    init(pageHeader: String, first: String, second: String, firstHeader: String, secondHeader: String) {
        self.pageHeader = pageHeader
        self.firstTabTitle = first
        self.secondTabTitle = second
        self.firstHeader = firstHeader
        self.secondHeader = secondHeader
    }
    
    
    static func getEntitiesInformationAndServices() -> TopTappedViewModel{
        let first = L10n.GovernmentEntities.informationTitle
        let second = L10n.GovernmentEntities.servicesTitle
        
        let entityDetails = L10n.GovernmentEntities.entityDetails
        let firstHeader  = entityDetails
        let secondHeader = entityDetails

        return TopTappedViewModel(pageHeader: L10n.GovernmentEntities.entityDetails, first: first, second: second, firstHeader: firstHeader, secondHeader: secondHeader)
    }
    
}
