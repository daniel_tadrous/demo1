//
//  Address.swift
//  TAMMApp
//
//  Created by Daniel on 7/31/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class Address: Mappable {
    var description: String?
    var lat: Double?
    var lng: Double?
    var name: String?
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        description <- map["description"]
        lat <- map["lat"]
        lng <- map["lng"]
        name <- map["name"]
    }
}
