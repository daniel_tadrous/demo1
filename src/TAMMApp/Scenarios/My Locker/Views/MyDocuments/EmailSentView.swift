//
//  EmailSentView.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class EmailSentView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var emailLogo: UILabel!
    

    @IBAction func okAction(_ sender: LocalizedButton) {
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("EmailSentView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        emailLogo.text = "\u{e04f}"
    }
}
