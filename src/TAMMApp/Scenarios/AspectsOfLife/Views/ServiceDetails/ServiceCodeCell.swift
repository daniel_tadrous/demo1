//
//  AolServiceFaqCell.swift
//  TAMMApp
//
//  Created by Daniel on 5/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ServiceCodeCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var titleLabel: LocalizedLabel!
    
    @IBOutlet weak var bodyLabel: LocalizedLabel!
    
    @IBOutlet weak var collapseExpandBtn: UIButton!
    
    @IBOutlet weak var readMore: LocalizedButton?
    @IBOutlet weak var viewServiceBtn: LocalizedButton!
    @IBAction func viewServiceConditionsClick(_ sender: LocalizedButton) {
        OnViewConditionsBtnClick?()
    }
    
    @IBAction func readMoreClick(_ sender: LocalizedButton) {
        OnReadMoreClick?()
    }
    @IBAction func readLessClick(_ sender: UIButton) {
        OnReadLessClick?()
    }
    
    var OnReadMoreClick: (()->Void)?
    var OnReadLessClick: (()->Void)?
    var OnViewConditionsBtnClick: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.addRoundCorners(radious: 5)
        if L10n.Lang == "ar" {
            readMore?.setTitle(L10n.readMore, for: .normal)
        }
        
    }
   
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.viewServiceBtn.layer.cornerRadius = self.viewServiceBtn.frame.height/2
        self.readMore?.layer.cornerRadius = (self.readMore?.frame.height)!/2
    }
   
  
}
