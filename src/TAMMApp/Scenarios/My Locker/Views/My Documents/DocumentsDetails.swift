//
//  DocumentsDetails.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class DocumentsDetails: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var typeLabel: LocalizedLabel!
    @IBOutlet weak var closeLabel: UILabel!
    @IBOutlet weak var idLabel: LocalizedLabel!
    @IBOutlet weak var expiresOnLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var detailsTable: UITableView!
    
    var shareClosure:(()->Void)!
    var editClosure:(()->Void)!
    var closeClosure:(()->Void)!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        Bundle.main.loadNibNamed("DocumentDetails", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        shareLabel.text = "\u{e003}"
        detailsTable.delegate = self
        detailsTable.dataSource = self
    }
    func editDocument() {
        editClosure()
    }
    func close() {
        closeClosure()
    }
    func shareDocument() {
        shareClosure()
    }
}

extension DocumentsDetails: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(
    }


}
