//
//  EventsParentViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol NewsAndPressReleasesViewControllerDelegate {
    func openNews()
    func openPressReleases()
}
enum NewsAndPressReleasesEnum{
    case NEWS,PRESS
}
class NewsAndPressReleasesViewController:  ContainerViewController, NewsAndPressReleasesStoryboardLoadable {
    
    var delegate:NewsAndPressReleasesViewControllerDelegate?
    
    @IBOutlet weak var newsButton: LocalizedButton!
    @IBOutlet weak var pressReleasesButton: LocalizedButton!
    @IBOutlet weak var fullUnderLine: UIView!
    
    static var selectedTapStatic :NewsAndPressReleasesEnum = .NEWS
    var selectedTap:NewsAndPressReleasesEnum{
        get{
            return NewsAndPressReleasesViewController.selectedTapStatic
        }
        set{
            NewsAndPressReleasesViewController.selectedTapStatic = newValue
        }
    }

    
    @IBOutlet weak var underLineNews: UIView!
    @IBOutlet weak var underLinePressReleases: UIView!
    
    fileprivate var selectedFont:UIFont! {
        get{
        return L10n.Lang == "en" ? UIFont(name: "CircularStd-Bold",size: UIFont.getCustomFontSize(20)) : UIFont(name: "Swissra-Bold",size: UIFont.getCustomFontSize(20))
        }
    }
    fileprivate var unSelectedFont:UIFont! {
        get{
            return L10n.Lang == "en" ? UIFont(name: "CircularStd-Book",size: UIFont.getCustomFontSize(20)) : UIFont(name: "Swissra-Normal",size: UIFont.getCustomFontSize(20))
            
        }
    }
    
    fileprivate var selectedFontColor = UIColor.petrol.getAdjustedColor()
    fileprivate var unSelectedFontColor = UIColor.darkBlueGrey.getAdjustedColor()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(title: L10n.news, willSetSearchButton: true)
        selectButtonAction(type: selectedTap)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        applyInvertColors()
    }
    
    fileprivate func applyInvertColors() {
        selectedFontColor = UIColor.petrol.getAdjustedColor()
        unSelectedFontColor = UIColor.darkBlueGrey.getAdjustedColor()
        selectButtonAction(type: selectedTap)
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        fullUnderLine.backgroundColor = #colorLiteral(red: 0.8506266475, green: 0.8481912017, blue: 0.8733561635, alpha: 1).getAdjustedColor()
        underLineNews.backgroundColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        underLinePressReleases.backgroundColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
//        underLineView.backgroundColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
    }
    
    
    @IBAction func newsButtonAction(_ sender: LocalizedButton) {
        self.selectedTap = .NEWS
        selectButtonAction(type: selectedTap)
    }
    
    @IBAction func pressButtonAction(_ sender: LocalizedButton) {
        self.selectedTap = .PRESS
        selectButtonAction(type: selectedTap)
    }
    
    fileprivate func selectButtonAction(type: NewsAndPressReleasesEnum) {
        switch type {
        case .NEWS:
            self.newsButton.titleLabel?.font = selectedFont
            self.newsButton.setTitleColor(selectedFontColor, for: .normal)
            self.pressReleasesButton.titleLabel?.font = unSelectedFont
            self.pressReleasesButton.setTitleColor(unSelectedFontColor, for: .normal)
            self.underLineNews.isHidden = false
            self.underLinePressReleases.isHidden = true
            
            //self.setNavigationBar(title: L10n.news, willSetSearchButton: true)
            self.delegate?.openNews()
        case .PRESS:
            self.pressReleasesButton.titleLabel?.font = selectedFont
            self.pressReleasesButton.setTitleColor(selectedFontColor, for: .normal)
            self.newsButton.titleLabel?.font = unSelectedFont
            self.newsButton.setTitleColor(unSelectedFontColor, for: .normal)
            self.underLineNews.isHidden = true
            self.underLinePressReleases.isHidden = false
            //self.setNavigationBar(title: L10n.news, willSetSearchButton: true)
            self.delegate?.openPressReleases()
        }
        
    }
    

    
}
