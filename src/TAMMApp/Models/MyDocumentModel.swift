//
//  MyDocumentModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class DocumentsModel: Mappable {
    
    var pageCount:Int?
    var documents:[MyDocumentModel] = []
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        pageCount <- map["pageCount"]
        documents <- map["documents"]
    }
    
    
}

class MyDocumentModel:Mappable{
    var Id:Int?
    var type:String?
    var name:String?
    var icon:String?
    var idNumber:String?
    var expidationDate:String?
    var fields:[DocumentField]?
    var sections:[DocumentSection]?
    
    var isLoaded = false
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        Id <- map["id"]
        type <- map["type"]
        name <- map["name"]
        icon <- map["icon"]
        idNumber <- map["idNumber"]
        expidationDate <- map["expidationDate"]
        fields <- map["fields"]
        sections <- map["sections"]
        
        
    }
    class DocumentField: Mappable {
        
        var key:String?
        var value:String?
        
        required init?(map: Map) {
        }
        
        func mapping(map: Map) {
            key <- map["key"]
            value <- map["value"]
        }
    }
    class DocumentSection: Mappable {
        var name:String?
        var fields:[DocumentField]?
        
        var isExpanded = false
        required init?(map: Map) {
        }
        
        func mapping(map: Map) {
            name <- map["name"]
            fields <- map["fields"]
        }
    }
}
class DoumentsApiCallModel {
    
    var lang:Int = 0
    var page:Int = 1
    var q:String = ""
    var filter:String = ""
    var from:String = ""
    var to:String = ""
}
