//
//  CaseDetailTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class CaseDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var progressIndicator: UIActivityIndicatorView?
    @IBOutlet weak var bodyLbl: LocalizedLabel!
    
    @IBOutlet weak var miniPlayer: XibView?
    var playerView: MiniPlayerView?{
        return miniPlayer?.contentView as? MiniPlayerView
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
