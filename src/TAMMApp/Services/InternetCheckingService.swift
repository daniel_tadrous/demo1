//
//  NetworkCheckingService.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import PlainPing
import Alamofire


class InternetCheckingService{
    private static var instance: InternetCheckingService!
    static var shared:InternetCheckingService {
        get{
            if instance == nil{
                instance = InternetCheckingService()
            }
            return instance
        }
    }
    public var hasInternet:Variable<Bool?> = Variable(NetworkReachabilityManager()?.isReachable)
    fileprivate let pingURL = "www.google.com"
    fileprivate weak var timer: Timer?
    
    func start(){
        
        if timer == nil || timer?.isValid == false{
            timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { [weak self] _ in
                PlainPing.ping((self?.pingURL)!, withTimeout: 3.0, completionBlock: { (timeElapsed:Double?, error:Error?) in
                    if let latency = timeElapsed {
                        print("latency (ms): \(latency)")
                        if(self?.hasInternet.value == nil || !(self?.hasInternet.value!)! ){
                            self?.hasInternet.value = true
                        }
                    }
                    
                    if let error = error {
                        print("error: \(error.localizedDescription)")
                        if(self?.hasInternet.value == nil || (self?.hasInternet.value!)!){
                            self?.hasInternet.value = false
                        }
                    }
                })
            }
        }
    }
}
