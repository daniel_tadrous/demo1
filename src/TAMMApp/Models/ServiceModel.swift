//
//  TammService.swift
//  TAMMApp
//
//  Created by Daniel on 4/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper


class ServiceModel: Mappable {
    var id: String?
    var title: String?
    var icon: String?
    var isOnlineService: Bool?
    var description: String?
    var isSubscribed: Bool?
    var faqs:[AspectOfLifeServiceFaqsModel] = []
    var serviceCode:String?
    var details:String?
    var relatedJourneys:[JourneyModel] = []
    var isServiceCollapsed: Bool = true
    var isServcieCodeCollapsed: Bool = true
    
    required init?(map: Map) {
       
    }
    
    // Mappable
    func mapping(map: Map) {
        id            <- map["id"]
        title         <- map["title"]
        description  <- map["description"]
        faqs         <- map["faqs"]
        isSubscribed  <- map["isSubscribed"]
        icon   <- map["icon"]
        isOnlineService  <- map["isOnlineService"] 
        serviceCode  <- map["serviceCode"]
        details  <- map["details"]
        relatedJourneys  <- map["relatedJourneys"]
    }
    
}
