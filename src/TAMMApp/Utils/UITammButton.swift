//
//  UITammButton.swift
//  TammUI
//
//  Created by Marina.Riad on 4/4/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//

import UIKit
class UITammButton: LocalizedButton{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func roundCorners(radius: CGFloat) {
        self.layer.borderColor = BorderColor.cgColor
        self.layer.borderWidth = 1
        if #available(iOS 11.0, *) {
            if(L102Language.currentAppleLanguage().contains("ar")){
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                self.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            self.layer.cornerRadius = radius
        } else {
            // Fallback on earlier versions
            self.layer.cornerRadius = radius
        }
        
    }
    
}

