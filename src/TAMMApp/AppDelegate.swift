///
//  AppDelegate.swift
//  iOSSampleApp
//
//  Created by Igor Kulman on 03/10/2017.
//  Copyright © 2017 Igor Kulman. All rights reserved.
//

import UIKit
import Swinject
import AppAuth
import Firebase
import IQKeyboardManager
import RxSwift
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    static var isDownloadRunning = false
    private(set) var isTraitChanged:Variable<Bool?> = Variable(false)
    static var window: UIWindow?
    static var vcs: [UIViewController] = []
    static let container = Container()
    var currentAuthorizationFlow:OIDAuthorizationFlowSession?
    var floatingMenu = FloatingMenuView(frame: UIScreen.main.bounds)
    static var appCoordinator: ApplicationCoordinator!
    private var menuIsVisible:Bool = false
    let smartPassKey = "smartPassIntent"
    
    var shouldTriggerLoginAfterIntro = false
    
    func application(_: UIApplication, willFinishLaunchingWithOptions _: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {
        InternetCheckingService.shared.start()
        AppDelegate.setupDependencies()
//        setupGoogleMaps()
        L012Localizer.DoTheSwizzling()
//        _ = DBManager.instance.createDatabase()
        return true
    }
   
    
    func applicationWillTerminate(_ application: UIApplication) {
        if AppDelegate.isDownloadRunning {
            self.sendNotification(isDownloadComplete: false)
        }
        
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions _: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        AppDelegate.window = UIWindow()
        // will lift up any text editing field so that the written text is visible
        // it is a shame iOS does not provide this simple function by defauly
        IQKeyboardManager.shared().isEnabled = true
        
    
        
        AppDelegate.appCoordinator = ApplicationCoordinator(window: AppDelegate.window!, container: AppDelegate.container)
        ApplicationCoordinator.shared.start()
       // window?.addSubview(floatingMenu)
        
        AppDelegate.window?.makeKeyAndVisible()
        FirebaseApp.configure()
        
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        UNUserNotificationCenter.current().requestAuthorization(options:
            [[.alert, .sound]],
                                                                completionHandler: { (granted, error) in
                                                                    // Handle Error
        })
        UNUserNotificationCenter.current().delegate = self
        
        return true
    }
    var heightOfTabbar : CGFloat = 0{
        didSet{
            if (menuIsVisible){
                if oldValue != heightOfTabbar{
                    floatingMenu.heightOfTabbar = heightOfTabbar
                }
            }
        }
    }
    
   
    
    func addfloatingMenu()  {
        let b = UIScreen.main.bounds
        let floatingMenu = FloatingMenuView(frame: b )
        self.floatingMenu.removeFromSuperview()
        self.floatingMenu = floatingMenu
        floatingMenu.heightOfTabbar = heightOfTabbar
        floatingMenu.clipsToBounds = true
       // window?.insertSubview(floatingMenu, at: (window?.subviews.count)!-2 > 0 ?(window?.subviews.count)!-2 : 0)
       // window?.addSubview(floatingMenu)
     
        var isAdded = false
        for view in (AppDelegate.window?.subviews)!{
            if view.tag == 1000{
                AppDelegate.window?.insertSubview(floatingMenu, belowSubview: view)
                isAdded = true
                break
            }
        }
        if !isAdded{
            AppDelegate.window?.addSubview(floatingMenu)
        }
        self.menuIsVisible = true
    }
    func floatingMenuIsVisible()->Bool{
        return menuIsVisible
    }
    func removeFloatingMenu()  {
        self.floatingMenu.removeFromSuperview()
        self.menuIsVisible = false
    }
    func enableDisableFloatingMenu(isEnabled: Bool)  {
        FloatingMenuView.enableDisable(isEnabled: isEnabled)
        
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApp = options[UIApplicationOpenURLOptionsKey.sourceApplication]
        if(sourceApp != nil){
            let sourceAppId = sourceApp as! String
            if(sourceAppId == (Bundle.main.object(forInfoDictionaryKey: smartPassKey) as? String)!){
                let loginTriggered = AppDelegate.appCoordinator.startLogin()
                
                if(!loginTriggered){
                    shouldTriggerLoginAfterIntro = true
                }
                return true
            }
        }
        if let authorizationFlow = self.currentAuthorizationFlow, authorizationFlow.resumeAuthorizationFlow(with: url) {
            self.currentAuthorizationFlow = nil
            return true
        }
        return false
    }
    
    func getAppCoordinator()->ApplicationCoordinator{
        return AppDelegate.appCoordinator
    }
}
extension AppDelegate{
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       //log.debug("smartpass","smart pass is called")
       LogToFile(log: "smart pass app is called")
        
        SmartPassOpenIDAuthenticationService().refreshToken(onComplete:completionHandler)
        
    }
    func LogToFile(log: String){
        let file = "tamm.txt" //this is the file. we will write to and read from it
        do {
            let dir: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last! as URL
            let url = dir.appendingPathComponent(file)
            try "\(log) \(Date().getFormattedString(format: "yyyy-MM-dd HH:mm:ss")) \n".appendLineToURL(fileURL: url as URL)
        }
        catch {
            print("Could not write to file")
        }
        
        
    }
}
extension AppDelegate{
    
    static func reload(){
        let emptyVc = AppDelegate.container.resolveViewController(EmptyFlipViewController.self)
        var screens: [ScreensEnum] = []
        var vcs: [UIViewController] = []
        vcs.append(contentsOf: appCoordinator.navigationController.viewControllers)
        if appCoordinator.tabNavigationController != nil{
            vcs.append(contentsOf: appCoordinator.tabNavigationController.viewControllers)
        }
        for vc in vcs{
            
            if let sc = ScreensEnum(rawValue: String(describing: type(of: vc)).replacingOccurrences(of: "TAMMApp.", with: "")){
                screens.append(sc)
            }
        }
        appCoordinator.navigationController.setViewControllers([emptyVc], animated: false)




        appCoordinator.navigationController.navigationItem.hidesBackButton = true
        let mainwindow = window!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { ( finished) -> Void in
            
            for i in 0..<screens.count{
                appCoordinator.load(screenEnum: screens[i], i == screens.count - 1)
            }
            //appCoordinator.load(screenEnum: .MyLockerViewController,false)
            //appCoordinator.load(screenEnum: .TalkToUsTabBarViewController)
        }
    }
    
    
    
    static func reset() {
        reload()
    }
    static func changeLang(navigationController: UINavigationController?){
        vcs = navigationController?.viewControllers ?? []
        if L102Language.currentAppleLanguage() == "en" {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        reset()
    }
    
    
}


extension AppDelegate : UNUserNotificationCenterDelegate{
    
    func sendNotification(isDownloadComplete:Bool) {
        
        let content = UNMutableNotificationContent()
        content.title = L10n.downloadOfflineMap
        if isDownloadComplete {
            content.body = L10n.downloadCompleted
        }else {
            content.body = L10n.downloadFailed
        }
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2,
                                                        repeats: false)
        
        let requestIdentifier = "DownloadMapNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier,
                                            content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request,
                                               withCompletionHandler: { (error) in
                                                // Handle error
        })
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
  
    
}
