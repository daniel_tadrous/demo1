//
//  ManageParentViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


protocol ManageParentViewControllerDelegate {
    func openManageMedia()
    func openManageDocuments()
}

class ManageParentViewController: ContainerViewController , ManageParentStoryboardLoadable {

    @IBOutlet weak var mediaButton: LocalizedSelectableButton!
    
    @IBOutlet weak var documentsButton: LocalizedSelectableButton!
    
    @IBOutlet weak var fullLineView: UIView!
    
    @IBOutlet weak var underLineView: UIView!
    
    var delegate:ManageParentViewControllerDelegate?
    
    fileprivate var manageSelected = false
    fileprivate var documentsSelected = false
    fileprivate var underLineColor = UIColor(red: 0, green: 89, blue: 113, alpha: 1)
    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    fileprivate var selectedFontColor = UIColor(hexString: "005971")
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138")
    
    @IBAction func mediaBtnAction(_ sender: LocalizedSelectableButton) {
         selectButtonAction(forButton: sender, isMedia: true)
    }
    
    
    @IBAction func documentsBtnAction(_ sender: LocalizedSelectableButton) {
        selectButtonAction(forButton: sender, isMedia: false)
    }
    
    fileprivate func setupFont() {
        if L10n.Lang == "ar" {
             selectedFont = UIFont(name: "Swissra-Bold",size: 18)
             unSelectedFont = UIFont(name: "Swissra-Normal",size: 18)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFont()
        
        mediaButton.RTLSelecttedFont = "Swissra-Bold"
        documentsButton.RTLSelecttedFont = "Swissra-Bold"
        
        
        mediaButton.originalFont = unSelectedFont
        mediaButton.fontProxy = unSelectedFont
        mediaButton.selectedFontReference = selectedFont
        mediaButton.originalColor = unSelectedFontColor
        mediaButton.selectedColor = selectedFontColor
        
        documentsButton.originalFont = unSelectedFont
        documentsButton.fontProxy = unSelectedFont
        documentsButton.selectedFontReference = selectedFont
        documentsButton.originalColor = unSelectedFontColor
        documentsButton.selectedColor = selectedFontColor

        selectButtonAction(forButton: mediaButton, isMedia: true)
         setNavigationBar(title: L10n.manageMedia, willSetSearchButton: true)
        
    }
    
    

    
    fileprivate func selectButtonAction(forButton button:LocalizedSelectableButton, isMedia:Bool ) {
        manageSelected = false
        documentsSelected = false
        mediaButton.isButtonSelected = false
        documentsButton.isButtonSelected = false

        if isMedia, !manageSelected{
            manageSelected = true
            //moveUnderLine(to: button)
            button.isButtonSelected = true
            delegate?.openManageMedia()
        }
        else if !isMedia, !documentsSelected{
            documentsSelected = true
            //moveUnderLine(to: button)
            button.isButtonSelected = true
            // goto public holidays
            delegate?.openManageDocuments()
        }
        
    }
    
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        if button == mediaButton {
            underLineView.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
            underLineView.frame.origin.x = fullLineView.frame.origin.x
        }
            
            
        else if button == documentsButton{
            underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }
        
    }
    

    override func viewDidLayoutSubviews() {
        if manageSelected {
            if L10n.Lang == "en" {
                underLineView.frame.origin.x = fullLineView.frame.origin.x
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }else {
                underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
   
        else if documentsSelected{
            if L10n.Lang == "en" {
                underLineView.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }else {
                underLineView.frame.origin.x = fullLineView.frame.origin.x
                UIView.animate(withDuration: 0.35, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            
        }
    }
    


    
   
    
    
    
    
    
}
