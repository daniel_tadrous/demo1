//
//  IShare.swift
//  TAMMApp
//
//  Created by Daniel on 6/21/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

protocol IShare {
    func share(viewController:UIViewController,text:String)
}
