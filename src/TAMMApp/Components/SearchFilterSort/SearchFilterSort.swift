//
//  SearchFilterSort.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class SearchFilterSort: UIView {

    var sortAction: (()->Void)?
    
    var filterAction: (()->Void)?
    
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var sortBtn: UIButton!
    
    @IBOutlet weak var searchView: TammSearchView!

    @IBOutlet weak var filterBtn: UIButton!
    
    
    @IBAction func filterClicked(_ sender: Any) {
        filterAction?()
    }
    
    
    @IBAction func sortClicked(_ sender: Any) {
        sortAction?()
    }
  
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        // commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("SearchFilterSort", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        sortBtn.setTitle("\u{e008}", for: .normal)
        searchView.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        searchView.setContentCompressionResistancePriority(UILayoutPriority.defaultLow, for: .horizontal)
        searchView.PlaceholderTextContent = L10n.searchEllipsized
        searchView.addRoundCorners(radious: 24)
        searchView.addRoundCorners(radious: 24)
        
    }
    
}
