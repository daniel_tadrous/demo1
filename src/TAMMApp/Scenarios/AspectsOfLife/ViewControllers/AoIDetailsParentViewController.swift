//
//  AoIDetailsParentViewController.swift
//  TAMMApp
//
//  Created by Daniel on 6/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//


import UIKit

protocol AoIDetailsParentViewControllerDelegate: class{
    func openAspectsOfLifeDetails(id: String , title:String)
    func openJourneysInDetails()
}
class AoIDetailsParentViewController: ContainerViewController, AolDetailsParentStoryboardLodable{
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnServices: LocalizedSelectableButton!
    @IBOutlet weak var btnJourneys: LocalizedSelectableButton!
    public weak var delegate: AoIDetailsParentViewControllerDelegate?
    public var draggableButton:UICircularImage!
    public var viewModel:AoIDetailsParentViewModel!
    fileprivate let selectedButtonBorder:CGFloat = 4
    fileprivate let stackViewBottomBorder:CGFloat = 1
    fileprivate let unSelectedFont:UIFont = UIFont.init(name: "CircularStd-Medium", size: 18)!
    fileprivate let selectedButtonBorderColor:String = "#10596F"
    fileprivate let stackViewBottomBorderColor:String = "#CAC8D1"
    fileprivate var layer:CALayer?
    fileprivate var isJourenys:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "Services"
        
        btnJourneys.originalFont = unSelectedFont
        btnServices.originalFont = unSelectedFont
        btnJourneys.fontProxy = unSelectedFont
        btnServices.fontProxy = unSelectedFont
        
        
        btnJourneys.RTLFont = "Swissra-Normal"
        btnJourneys.RTLSelecttedFont = "Swissra-Bold"
        
        
        btnServices.RTLFont = "Swissra-Normal"
        btnServices.RTLSelecttedFont = "Swissra-Bold"
        
        
        btnJourneys.selectedFontReference = UIFont.init(name: "Swissra-Normal-Bold", size: 18)
        btnServices.selectedFontReference = UIFont.init(name: "Swissra-Bold", size: 18)
        
        btnJourneys.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnServices.selectedFontReference = UIFont.init(name: "CircularStd-Bold", size: 18)
        btnJourneys.selectedColor = UIColor.init(hexString: "#11596F")
        btnServices.selectedColor = UIColor.init(hexString: "#11596F")
        
        btnJourneys.originalColor = UIColor.init(hexString: "#151331")
        btnServices.originalColor = UIColor.init(hexString: "#151331")
        
        
        buttonIsSelected(button: btnServices, openJournyes: false)
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addBorderToStackView()
        if(isJourenys){
            self.setNavigationBar(title: L10n.services)
        }
        else{
            self.setNavigationBar(title: viewModel.title!)
        }
        
        
    }
    
    func rightButtonAction(sender: UIBarButtonItem){
        
    }
    
    
    @IBAction func btnServicesClicked(_ sender: LocalizedSelectableButton) {
        buttonIsSelected(button: sender, openJournyes: false)
    }
    @IBAction func btnJourneysClickes(_ sender: LocalizedSelectableButton) {
        buttonIsSelected(button: sender, openJournyes: true)
    }
    
    fileprivate func addBorderToStackView(){
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: stackView.frame.height - stackViewBottomBorder, width: stackView.frame.width, height: stackViewBottomBorder)
        
        bottomBorder.backgroundColor = UIColor.init(hexString: stackViewBottomBorderColor).cgColor
        
        stackView.layer.addSublayer(bottomBorder)
    }
    fileprivate func buttonIsSelected(button:LocalizedSelectableButton, openJournyes:Bool){
        if(layer != nil){
            layer?.removeFromSuperlayer()
        }
        //        let btnServiceString = NSAttributedString(string: btnServices.title(for: .normal)!, attributes: [NSAttributedStringKey.font:unSelectedFont ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#151331")])
        //        btnServices.setAttributedTitle(btnServiceString, for: .normal)
        //        let btnJourneysString = NSAttributedString(string: btnJourneys.title(for: .normal)!, attributes: [NSAttributedStringKey.font:unSelectedFont ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#151331")])
        //
        //        btnJourneys.setAttributedTitle(btnJourneysString, for: .normal)
        //
        
        btnJourneys.isButtonSelected = false
        btnServices.isButtonSelected = false
        //
        //        btnJourneys.titleLabel?.textColor = UIColor.init(hexString: "#151331")
        //        btnServices.titleLabel?.textColor = UIColor.init(hexString: "#151331")
        //
        
        
        let bottomBorder:CALayer = CALayer.init()
        bottomBorder.frame = CGRect(x: 0, y: button.frame.height - selectedButtonBorder, width: button.frame.width, height: selectedButtonBorder)
        bottomBorder.backgroundColor = UIColor.init(hexString: selectedButtonBorderColor).cgColor
        
        button.layer.addSublayer(bottomBorder)
        //        let btnString = NSAttributedString(string: button.title(for: .normal)!, attributes: [NSAttributedStringKey.font:UIFont.init(name: "CircularStd-Bold", size: 18)! ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#11596F")])
        //        button.setAttributedTitle(btnString, for: .normal)
        //        button.titleLabel?.textColor = UIColor.init(hexString: "#11596F")
        button.isButtonSelected = true
        layer = bottomBorder
        isJourenys = openJournyes
        if(openJournyes){
            delegate?.openJourneysInDetails()
        }
        else{
            delegate?.openAspectsOfLifeDetails(id: viewModel.id! , title:viewModel.title!)
        }
        
    }
    
    
}

