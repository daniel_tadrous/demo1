////
////  GovernmentEntitiesCoordinator.swift
////  TAMMApp
////
////  Created by kerolos on 7/12/18.
////  Copyright © 2018 Igor Kulman. All rights reserved.
////

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

class GovernmentEntitiesCoordinator: NavigationCoordinator {
    var navigationController: UINavigationController
    
    var tabBarControlor:UITabBarController
    
    var container: Container
    
    var topTappedController: TopTappedViewController
    
    
    var entitiyId: String?
    init(container: Container, navigationController: UINavigationController
        , tabBarControlor:UITabBarController
        ) {
        self.container = container
        self.navigationController = navigationController
        
//        self.navigationController.setNavigationBarHidden(true, animated: false)
        self.tabBarControlor = tabBarControlor
//        self.tabBarController = tabBarControlor
        // self.authenticationService = authenticationService
        
        self.topTappedController = container.resolveViewController(TopTappedViewController.self)
        self.topTappedController.viewModel = TopTappedViewModel.getEntitiesInformationAndServices()
        self.topTappedController.delegate = self
        
    
    }
    
    func start() {
        self.navigationController.pushViewController(topTappedController, animated: false)
    }
    
    private func openInformationPage(){
//        let parentVC = topTappedController
        let childVC = container.resolveViewController(EntitiesInformationViewController.self)
        childVC.delegate = self
        childVC.viewModel.entitiyId = self.entitiyId
        topTappedController.removeEmbeddedController()
        topTappedController.embeddViewController(controller: childVC)
        
    }
    func openEntityDetails(id: String) {
        let detailedvc = container.resolveViewController(EntityServicesViewController.self)
        detailedvc.viewModel?.serviceCategoryId.value = id
        detailedvc.delegate = self
        topTappedController.removeEmbeddedController()
        topTappedController.embeddViewController(controller: detailedvc)
    }
    
    
    
    private func openServicesPage(){
        openEntityDetails(id: entitiyId!)
    }
    
//    fileprivate func embedViewController(child: UIViewController , To parent:TopTappedViewController){
//
//        child.willMove(toParentViewController: parent)
//        child.view.frame = CGRect(origin: child.view.frame.origin, size: parent.containerView.frame.size)
//        parent.selectedViewController = child
//        parent.containerView.addSubview(child.view)
//        parent.addChildViewController(child)
//        child.didMove(toParentViewController: parent)
//
//    }
}

extension GovernmentEntitiesCoordinator: TopTappedViewControllerDelegate{
    func openFirstTab() {
        
    }
    
    func openSecondTab() {
        
    }
}

extension GovernmentEntitiesCoordinator: EntitiesInformationViewControllerDelegate{}

extension GovernmentEntitiesCoordinator: EntityServicesViewControllerDelegate{}

