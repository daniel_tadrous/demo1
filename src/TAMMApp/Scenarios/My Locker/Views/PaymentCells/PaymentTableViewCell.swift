//
//  PaymentAdweyaTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainRoundedView: RoundedView!
    
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var arrowLabel: UILabel!
    
    @IBOutlet weak var amountLabel: LocalizedLabel!
    
    @IBOutlet weak var moneyLabel: UILabel!
    
    
    // views In stack
    
    @IBOutlet weak var viewContainImage: UIView!
    @IBOutlet weak var imageViewInStack: UIImageView!
    
    @IBOutlet weak var dueOnLabel: UILabel!
    
    @IBOutlet weak var firstLabel: LocalizedLabel!
    
    @IBOutlet weak var secondLabel: LocalizedLabel!
    
    @IBOutlet weak var thirdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setAdweyaCell(cell:PaymentTableViewCell){
        cell.viewContainImage.isHidden = true
        
    }
}
