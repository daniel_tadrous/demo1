//
//  ManageMediaCollectionViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class ManageMediaCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewOverlay: RoundedView!
    
    @IBOutlet weak var clickedView: RoundedView!
    
    @IBOutlet weak var playLabel: UILabel!
    
    @IBOutlet weak var durationLabel: LocalizedLabel!
    
    @IBOutlet weak var imgae: UIImageView!
    
    @IBOutlet weak var videoIconLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization
 
                
    }

}
