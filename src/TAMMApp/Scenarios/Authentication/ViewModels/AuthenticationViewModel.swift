//
//  AuthenticationViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 3/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class AuthenticationViewModel {

    // Decided not to use the viewModel for the authentication
    // all work will be thruough the AuthenticationCoordinator
    // since we just need the status from the service
    // and we are using the service anyway to decide the user authentication status
    // we may use it if we need any data to be displayed on the page but typically the view controller does not need to display anything but the Authorize button
    let authenticationService: AuthenticationService!
    
    init(authenticationService: AuthenticationService) {
        self.authenticationService = authenticationService
        
    }
    
//    func login(){
//        authenticationService.authorize()
//    }
//    
    
}
