//
//  NewsViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift



class NewsViewController: TammViewController , NewsStoryboardLodable{
    
    @IBOutlet weak var searchFilterSortView: SearchFilterSort!
    @IBOutlet weak var newsTableView: UITableView!
    
    let cellIdentifier:String = "NewsCell"
    let showMoreCellIdentifier:String = "showmoreCell"
    public var viewModel: NewsViewModel?
    var disposeBag = DisposeBag()
    var newsListToShow = [NewsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupBinding()
        registerXib()
        addTapGesturseOnSearchView()
        setupNavigationBar()
        setupLanguagePlaceolderSearchView()
        setupCustomView()
        searchFilterSortView.searchView.searchField.text = L10n.search
       
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViewColors()
    }
    
    fileprivate func setupViewColors() {
        
        self.searchFilterSortView.sortBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        self.searchFilterSortView.filterBtn.setTitleColor(#colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        
        self.searchFilterSortView.searchView.searchField.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        self.searchFilterSortView.searchView.view.backgroundColor =  UIColor.white.getAdjustedColor()
        self.searchFilterSortView.searchView.searchIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        if searchFilterSortView.searchView.searchField.text == "" {
            searchFilterSortView.searchView.searchField.text = "Search"
        }
        self.searchFilterSortView.searchView.searchField.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
//        circleView.backgroundColor = UIColor.silver.getAdjustedColor()
//        descriptionLabel.textColor = UIColor.greyPurple.getAdjustedColor()
//        iconLabel.textColor = UIColor.greyPurple.getAdjustedColor()
    }
    
    func setupCustomView() {
        
        searchFilterSortView.filterAction = {
            self.displayToast(message: L10n.commingSoon)
        }
        
        searchFilterSortView.sortAction = {
            self.newsListToShow.removeAll()
            self.viewModel?.sortButtonAction()
        }
    }
    
    fileprivate func setupLanguagePlaceolderSearchView() {
        searchFilterSortView.searchView.PlaceholderTextContent = L10n.search
    }
    
    fileprivate func setupNavigationBar() {
        setNavigationBar(title: L10n.news, willSetSearchButton: true)
        
    }
    
    fileprivate func addTapGesturseOnSearchView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(searchViewClicked))
        searchFilterSortView.searchView.addGestureRecognizer(tap)
    }

    @objc func searchViewClicked() {
        let searchViewOverLay = SearchOverlayView(frame: UIScreen.main.bounds, currentViewModel: self.viewModel!, text: self.searchFilterSortView.searchView.searchField.text!, currentSearchModel: (self.viewModel?.mySearchViewModel)!)
          searchViewOverLay.delegate = self
          UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
          UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay)
           print("Search View Clicked")
    }
    
    fileprivate func setupBinding(){
        viewModel!.newsList.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] newsList in
                if let currentNews = newsList {
                    self.newsListToShow += currentNews
                    self.newsTableView.reloadData();
                    print("Table Updated \(self.newsListToShow.count)")
                }else {
                    // To do someThing when list is nil
                }
            }).disposed(by: disposeBag)
    }
    
    fileprivate func registerXib() {
        self.newsTableView.register(UINib(nibName: "NewsTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        self.newsTableView.register(UINib(nibName: "ShowMoreCell", bundle: nil), forCellReuseIdentifier: showMoreCellIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NewsViewController : UITableViewDelegate , UITableViewDataSource {
    
    fileprivate func getNumberOfRows()->Int {
        if newsListToShow.count == 0 {
            return 0
        }else if (viewModel?.isLastNewsPage())!{
            return newsListToShow.count
        }
        return newsListToShow.count+1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       if indexPath.row <= newsListToShow.count-1   {
            let newsInstance = newsListToShow[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! NewsTableViewCell
        
        if let month = (newsInstance.date?.getDate(format: "yyyy-MM-dd'T'HH:mm:ss.000")?.getFormattedString(format: "MMMM"))
        {
            cell.newsMonthLabel.text = " " + month
        }
        
        if let day = (newsInstance.date?.getDate(format: "yyyy-MM-dd'T'HH:mm:ss.000")?.getFormattedString(format: "dd"))
        {
            cell.newsDayLabel.text =  day
        }
        
        if let year = (newsInstance.date?.getDate(format: "yyyy-MM-dd'T'HH:mm:ss.000")?.getFormattedString(format: "yyyy"))
        {
            cell.newsYearLabel.text = " " + year
        }
        
         cell.newsImage.sd_setImage(with: URL(string: newsInstance.imageUrl!)!)
         {           (newsImage, e, c, u) in
            
            if let newsImage = newsImage{
                    let const = cell.imageAspectRatioConstraint.constraintWithMultiplier(newsImage.size.width / newsImage.size.height)
                    cell.newsImage.removeConstraint(cell.imageAspectRatioConstraint)
                    cell.imageAspectRatioConstraint = const
                    cell.newsImage.addConstraint(const)
                    cell.newsImage.image = newsImage
                    cell.imageHeightConstraint.priority = .defaultHigh
            
                    if !newsInstance.isLoaded && indexPath.row < 3
                    {
                        newsInstance.isLoaded = true
                        // tableView.beginUpdates()
                        // tableView.reloadRows(at: [indexPath], with: .none)
                        //  tableView.endUpdates()
                        tableView.reloadData()
                    }
            }
        }

        
        cell.newsCategoryLabel.text = newsInstance.category?.uppercased()
        cell.newsTitleLabel.text = newsInstance.title
        cell.addRoundCorners(radious: 17)
        cell.applyInvertColors()
        cell.backgroundColor = #colorLiteral(red: 0.9562581182, green: 0.9564779401, blue: 0.9625678658, alpha: 1).getAdjustedColor()
        return cell
       }else {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: showMoreCellIdentifier) as! ShowMoreCell

            cell.showMoreBtn.addTarget(self, action: #selector(showMoreNewsPressed(_:)), for: .touchUpInside)
            cell.backgroundColor = #colorLiteral(red: 0.9562581182, green: 0.9564779401, blue: 0.9625678658, alpha: 1).getAdjustedColor()
            cell.applyInvertColors()
            return cell

        }
  
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 200
//    }
    
    @objc func showMoreNewsPressed(_ sender:UIButton){
        
        viewModel?.newsPage.value = (viewModel?.newsPage.value)! + 1
    }
    
}

extension NewsViewController : SearchOverlayViewDelegate{
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchFilterSortView.searchView.searchField.text = message.name
        //self.searchView.PlaceholderTextContent = message.name
        print("Welcome Again After OverLay \(message.name)")
        self.newsListToShow.removeAll()
        self.viewModel?.searchedText.value = message.name
    }
    func backPressed(){
        self.searchFilterSortView.searchView.searchField.text = L10n.search
    }
}





