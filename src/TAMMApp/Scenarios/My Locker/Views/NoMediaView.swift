//
//  NoMediaView.swift
//  TAMMApp
//
//  Created by marina on 6/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import UICheckbox_Swift
class MediaBaseView:UIView{
    var delegate:MediaViewDelegate?
    public var heightOfView:CGFloat = 366
    @IBOutlet weak var notSupportedLabel: LocalizedLabel!
    @IBOutlet weak var exceedLimitLabel: LocalizedLabel!
     @IBOutlet weak var addMediaBtn: UIButton?
    @IBAction func addMediaClick(_ sender: Any){
        delegate?.addMediaClicked()
    }
    var exceededLimitCheck = false{
        didSet{
            exceedLimitLabel.isHidden = !exceededLimitCheck
        }
    }
    var notSupportedCheck = false{
        didSet{
            notSupportedLabel.isHidden = !notSupportedCheck
        }
    }
    
    func HasMedia()->Bool{
        return false
    }
    
    internal var addMedialayer:CALayer?
    @IBOutlet weak var addMediaView: UIView!
    
    fileprivate func updateBoarderAndUI() {
        self.layoutIfNeeded()
        if self.addMedialayer != nil{
            self.addMedialayer?.removeFromSuperlayer()
        }
        let layer = CAShapeLayer()
        layer.strokeColor = UIColor.init(red: 204/255, green: 221/255, blue: 226/255, alpha: 1).cgColor
        layer.lineDashPattern = [5, 2]
        layer.frame = addMediaView.bounds
        layer.fillColor = nil
        layer.path = UIBezierPath(rect: addMediaView.bounds).cgPath
        self.addMedialayer = layer
        addMediaView.layer.addSublayer(layer)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        updateBoarderAndUI()
        
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateBoarderAndUI()
    }
    
}

