//
//  HomeCoordinator.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import SafariServices
import Swinject
import UIKit
import RxSwift

protocol MainTabCoordinatorDelegate: class {
    func mainTabCoordinatorDidFinish()
}

class MainTabCoordinator: NavigationCoordinator {
    
    // MARK: - Properties
    
    let navigationController: UINavigationController
    let container: Container
    //let authenticationService : AuthenticationService
    weak var delegate: MainTabCoordinatorDelegate?
    
    init(container: Container, navigationController: UINavigationController) {
        self.container = container
        self.navigationController = navigationController
       // self.authenticationService = authenticationService
    }
    
    // MARK: - Coordinator core
    
    func start() {
        let vc = container.resolveViewController(MainTabViewController.self)
        vc.vcDelegate = self
        vc.navigationItem.hidesBackButton = true
        navigationController.pushViewController(vc, animated: true)
    }
    
}

// MARK: - Delegate
extension MainTabCoordinator: MainTabViewControllerDelegate {
    func mainTabViewControllerDismissed() {
        delegate?.mainTabCoordinatorDidFinish()
    }

    
}


