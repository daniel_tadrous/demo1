//
//  AccessibilityViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 8/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit


class AccessibilityViewModel{
    
    let invertedKey = "isInverted"
    
    let userConfigService: UserConfigServiceType
    
    init(userConfigService: UserConfigServiceType){
        self.userConfigService = userConfigService
        
    }
   
    
    func getFontAccessibilitySize() -> Int {
        return UserConfigService.getFontAccessibilityMultiplier()
    }
    func setFontAccessibilitySize(size: CGFloat) {
        userConfigService.setFontAccessibilitySize(size: size)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isTraitChanged.value = true
    }
    
    func getInvertedStatus()->Bool {
        let defaults = UserDefaults.standard
        let invertedStatus = defaults.object(forKey: invertedKey)
        if invertedStatus != nil {
            return invertedStatus as!Bool
        }else{
            return false
        }
    }
    
    func setInvertedStatus(withValue value:Bool){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: invertedKey)
        defaults.synchronize()
    }
    
}

