//
//  UIImage+Inversion.swift
//  TAMMApp
//
//  Created by Kerolos on 8/30/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

extension UIImage {

    
    func getAdjustedImage()->UIImage {
        if UIColor.getInvertedStatus() {
            return self.inverted
        }else {
            return self
        }
    }
    
    
    var inverted : UIImage{
//        let theImage = self //this takes the image currently loaded on our UIImageView
//
//        let filter = CIFilter(name: "CIColorInvert") //this creates a CIFilter with the attribute color invert
//
//        filter?.setValue(CIImage(image: theImage), forKey: kCIInputImageKey) //this applies our filter to our UIImage
////        UIImage(cii)
//        let newImage = UIImage(ciImage: (filter?.outputImage)!, scale: theImage.scale, orientation: theImage.imageOrientation ) //this takes our inverted image and stores it as a new UIImageßß
//        return newImage //this shows our new inverted image on our UIImageView on our View
//        return makeImageNegative(self)!
        
        return negativeImage()!
    }
    
    func makeImageNegative(_ image: UIImage?) -> UIImage? {
        UIGraphicsBeginImageContext((image?.size)!)
        UIGraphicsGetCurrentContext()!.setBlendMode(CGBlendMode.copy)
        image?.draw(in: CGRect(x: 0, y: 0, width: image?.size.width ?? 0.0, height: image?.size.height ?? 0.0))
        UIGraphicsGetCurrentContext()!.setBlendMode(CGBlendMode.difference)
        UIGraphicsGetCurrentContext()?.setFillColor(UIColor.white.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: image?.size.width ?? 0.0, height: image?.size.height ?? 0.0))
        let returnImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return returnImage
    }
    
    func negativeImage() -> UIImage? {
        let size: CGSize = self.size
        let width = Int(size.width)
        let height = Int(size.height)
        let colourSpace = CGColorSpaceCreateDeviceRGB()
        
        let memoryBounds = calloc(width * height * 4, 1)!
        
        let memoryPool =  memoryBounds.assumingMemoryBound(to: UInt8.self) // UnsafeMutablePointer<UInt8>.  .allocate(capacity: width * height * 4 )
        //memoryBounds?.assumingMemoryBound(to: UInt8.self) as [Int8] // as? UnsafePointer
        
//        let memoryPool =  UnsafeRawPointer<UInt8>( as? [UInt8]
        let context = CGContext(data: memoryPool, width: width, height: height, bitsPerComponent: 8, bytesPerRow: width * 4, space: colourSpace, bitmapInfo: CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue)
        
        context?.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height)))

        for y in 0..<height {
            var linePointer = memoryPool.advanced(by: y * width * 4)
            for x in 0..<width {
                var r: Int
                var g: Int
                var b: Int
                
//                var or = linePointer.advanced(by: 0).pointee
//                var og = linePointer.advanced(by: 1).pointee
//                var ob = linePointer.advanced(by: 2).pointee
//                var oa = linePointer.advanced(by: 3).pointee
                
                
                if linePointer.advanced(by: 3).pointee != 0 {
                    r = ( Int(linePointer.advanced(by: 0).pointee) * 255  / Int(linePointer.advanced(by: 3).pointee))
                    g = ( Int(linePointer.advanced(by: 1).pointee) * 255  / Int(linePointer.advanced(by: 3).pointee))
                    b = ( Int(linePointer.advanced(by: 2).pointee) * 255  / Int(linePointer.advanced(by: 3).pointee))
                } else {
                        b = 0
                        g = b
                        r = g
                    }
                    r = 255 - r
                    g = 255 - g
                    b = 255 - b
                linePointer.advanced(by:0).pointee = UInt8(r * Int(linePointer.advanced(by:3).pointee) / 255)
                linePointer.advanced(by:1).pointee = UInt8(g * Int(linePointer.advanced(by:3).pointee) / 255)
                linePointer.advanced(by:2).pointee = UInt8(b * Int(linePointer.advanced(by:3).pointee) / 255)
                linePointer += 4
            }
        }

        
        let cgImage = context?.makeImage()
        let returnImage = UIImage(cgImage: cgImage!)
        
        
//        CGImageRelease(cgImage!)
//        CGContextRelease(context!)
        free(memoryPool)
        
        
        return returnImage
    }


}
