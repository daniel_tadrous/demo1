//
//  TalkToUsViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
protocol TalkToUsViewControllerDelegate: class {
    func checkStatusClickedInTalkToUs()
    func talkToUsItemIsSelected(item:TalkToUsItemViewDataType)
    func talkToUsItemIsSelected(item:String)
    func talkToUsMessageTypeSelected(id:Int,isFromOfflineScreen:Bool)
}

class TalkToUsLandingViewController: TammViewController, UITammStackViewDelegate, UITextFieldDelegate, TalkToUsLandingStoryboardLodable {

    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblSearch: UILabel!
    static var isFromOfflineScreenStatic:Bool = false
    var isFromOfflineScreen:Bool{
        set{
            TalkToUsLandingViewController.isFromOfflineScreenStatic = newValue
        }
        get{
            return TalkToUsLandingViewController.isFromOfflineScreenStatic
        }
    }
    
    @IBOutlet weak var trendingNowLbl: UILabel!
    
    @IBOutlet weak var trendingView: UITammStackView!
    @IBOutlet weak var trendingViewHightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var micBtn: UIButton!
    
    @IBOutlet weak var suggestionsLbl: UILabel!
    
    @IBOutlet weak var suggestedView: UITammStackView!
    @IBOutlet weak var suggestedHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var checkStatusContentView: UIView!
    @IBOutlet weak var checkStatusBtn: UITammButton!

    @IBOutlet weak var checkStatusIcon: UILabel!
    var viewModel: TalkToUsLandingViewModel!
    private let disposeBag = DisposeBag()
    public weak var talkToUsOverlayView:TalkToUsOverlayView!
    var delegate:TalkToUsViewControllerDelegate?
    private var textFromMic:Bool =  false
    private let debounceTimeInSec: Double = 0.5
    //public var searchView:TalkToUsSearchView!
    //public var trendingNow:UIlabel!
    
    
    var items:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ErrorView.show = !isFromOfflineScreen
        self.setupBindings()
        self.setupSpeechBinding()
        self.lblSearch.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchTxtFocused)))
        self.lblSearch.isUserInteractionEnabled = true
        self.checkStatusIcon.text = "\u{e00d}"
        if(L102Language.currentAppleLanguage().contains("en")){
            self.trendingNowLbl.addCharacterSpacing(space: 2.3)
            self.suggestionsLbl.addCharacterSpacing(space: 2.3)
        }
        self.checkStatusContentView.addRoundCorners(radious: 8, borderColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 0.6))
//        self.checkStatusBtn.roundCorners(radius: 20)
        setNavigationBar(title: L10n.talk_to_us_header)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func adjustStackView(view:UITammStackView , items:[TalkToUsItemViewDataType] , heightConstraint:NSLayoutConstraint){
        view.items = items
        view.layoutSubviews()
        view.resizeToFitSubviews()
        view.delegate = self
        heightConstraint.constant = view.frame.height
    }
    
    func itemIsSelected(message: TalkToUsItemViewDataType) {
        delegate?.talkToUsItemIsSelected(item: message)
    }
    


    @objc func searchTxtFocused() {
        self.overlayMicText = ""
        self.openOverlay()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblSearch.text = L10n.searchEllipsized
        self.overlayMicText = ""
        self.textFromMic = false
    }
    fileprivate func openOverlay(){
        // open overlay
        print("open Overlay called")

        let view = TalkToUsOverlayView(frame: UIScreen.main.bounds)
        self.talkToUsOverlayView = view
        view.delegate = self
        view.searchTxt.rx.text.debounce(0.5, scheduler: MainScheduler.instance)
            .asObservable()
            .subscribe(onNext: { text in
                print("fetching suggestions for: \"\(view.searchTxt.text)\" ")

                self.viewModel.currentQuery = view.searchTxt.text
            })
            .disposed(by: view.disposeBag)
        
        if(textFromMic){
            print("assigning text in condition text from Mic: \"\(self.lblSearch.text)\" to serch text")
            self.overlayMicText = self.lblSearch.text
       }
        
        UIView.transition(with: view, duration: 2.0, options: .transitionFlipFromBottom, animations: {
            self.navigationController?.view.addSubview(view)
        }, completion: nil)
//        viewModel.micRecognized.observeOn(MainScheduler.instance).subscribe(onNext:{
//            [unowned self] text in
//            if text != nil {
//                print("assigning text: \"\(text)\" to serch text")
//                self.overlayMicText = text
//            }
//        }).disposed(by: view.disposeBag)
        
        let tapGesture = UITapGestureRecognizer()
        self.talkToUsOverlayView.micBtn.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: { recognizer in
            if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            print("mic button tapped")
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: self.viewModel.speechDuration)
            self.viewModel.startSpeechService().subscribe(
                onNext: {
                    [unowned self] text in
                    if text != nil {
                        self.overlayMicText = text
                    }
            }, onCompleted: {
                [unowned self] in
                // just to activate did set
                let txt = self.overlayMicText
                self.overlayMicText = txt
            }).disposed(by: self.disposeBag)
                
            }
            
            // we may use an overlay for the service
        }).disposed(by: view.disposeBag)
        let txt = self.overlayMicText
        self.overlayMicText = txt

    }
    
    var overlayMicText: String?{
        didSet{
            if self.talkToUsOverlayView != nil{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                
                    self.talkToUsOverlayView.searchTxt.text = self.overlayMicText
                    self.viewModel.currentQuery = self.overlayMicText
                    
                })
            }
        }
    }
    
    
    @IBAction func refreshSuggestions(_ sender: Any) {
       viewModel.reloadPopular()
    }
    
    @IBAction func checkStatusBtnClicked(_ sender: Any) {
        delegate?.checkStatusClickedInTalkToUs()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    fileprivate func updateView(){
        self.view.layoutSubviews()
        let lastHeight = self.checkStatusContentView.frame.origin.y + self.checkStatusContentView.frame.height
        
        if(lastHeight > UIScreen.main.bounds.height){
            self.scrollContentViewHeightConstraint.constant = lastHeight - UIScreen.main.bounds.height + 100
        }
    }
    private func setPopularArray(_ pops: [PoppularItemViewDataType]?){
        self.adjustStackView(view: self.suggestedView, items:pops ?? [], heightConstraint: self.suggestedHeightConstraint)
        self.updateView()
    }
    private func setTrendingArray(_ trends: [TrendingItemViewDataType]? ){
        self.adjustStackView(view: self.trendingView, items: trends ?? [], heightConstraint: self.trendingViewHightConstraint)
        self.updateView()
    }
    private func setupBindings(){
        viewModel.popular.observeOn(MainScheduler.instance).subscribe(onNext:{
            [unowned self] pops in
            
            self.setPopularArray(pops)
            
        }).disposed(by: disposeBag)
        
        viewModel.trending.observeOn(MainScheduler.instance).subscribe(onNext:{
            [unowned self] trends in
            
            self.setTrendingArray(trends)
            
        }).disposed(by: disposeBag)
        
    viewModel.queryResult.observeOn(MainScheduler.instance).subscribe(onNext:{
            [unowned self] query in
        if( self.talkToUsOverlayView != nil){
            self.talkToUsOverlayView.items = query!
            self.talkToUsOverlayView.searchResultsTableView.reloadData()
        }
            
        }).disposed(by: disposeBag)
        
    }
    
    func setupSpeechBinding(){
        
                
        // triggering the mic recognision when item is pressed
        let tapGesture = UITapGestureRecognizer()
        micBtn.isUserInteractionEnabled = true
        micBtn.addGestureRecognizer(tapGesture)
        
        tapGesture.rx.event.debounce(self.debounceTimeInSec, scheduler: MainScheduler.instance).bind(onNext: { recognizer in
            
            print("mic button tapped")
            if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice, duration: self.viewModel.speechDuration)
            self.viewModel.startSpeechService().observeOn(MainScheduler.instance).subscribe(onNext:{
                
                // assigning the mic text to the text field
                [unowned self] text in
                if text != nil {
                    self.lblSearch.text = text
                    self.textFromMic = true
                }
            }, onCompleted: {
              [unowned self] in
                self.openOverlay()
            }).disposed(by: self.disposeBag)
            }
            // we may use an overlay for the service
        }).disposed(by: disposeBag)
        
    }
    
    
    private func dismissOverlay(){
        // animate up and out of screen
//        let height =  self.talkToUsOverlayView.bounds.height
//        let currCenter =  self.talkToUsOverlayView.center
//        self.talkToUsOverlayView.endEditing(true)
//        UIView.animate(withDuration: Dimensions.animationDuration1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//            self.talkToUsOverlayView.center = currCenter.applying(CGAffineTransform(translationX: 0, y: -height) )
//        }) { (finished) in
            self.talkToUsOverlayView.removeFromSuperview()
    //    }
    }

}

extension TalkToUsLandingViewController : TalkToUsOverlayViewDelegate{
    func typeIsSelected(typeId: Int) {
        dismissOverlay()
        delegate?.talkToUsMessageTypeSelected(id: typeId,isFromOfflineScreen: self.isFromOfflineScreen)
    }
    
    func getMessageTypes() -> Observable<MessageTypesViewDataType?> {
        return viewModel.messagesTypes
    }
    
    func suggestionIsSelected(message: TalkToUsItemViewDataType) {
        dismissOverlay()
        delegate?.talkToUsItemIsSelected(item: message)
    }
    func suggestionIsSelected(message:String){
        dismissOverlay()
        delegate?.talkToUsItemIsSelected(item: message)
    }
    func searchIsDone(text: String) {
        dismissOverlay()
        self.lblSearch.text = L10n.searchEllipsized
//        delegate?.talkToUsItemIsSelected(item: text)
    }

}


