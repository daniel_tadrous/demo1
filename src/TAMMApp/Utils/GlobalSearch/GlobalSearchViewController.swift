//
//  GlobalSearchViewController.swift
//  TAMMApp
//
//  Created by Daniel on 9/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
enum GlobalSearchMode{
    case suggestion, result
}
class GlobalSearchViewController: UIViewController {
    var mode = GlobalSearchMode.suggestion
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTxt: LocalizedTextField!
    @IBOutlet weak var micBtn: UIButton!
    let suggestionCellId = "SuggestionTableViewCell"
    let resultCellId = "ResultTableViewCell"
    let suggestionHeaderId = "DidYouMeanHeaderView"
    let resultHeaderId = "ReasultHeaderView"
    var viewModel: GlobalSearchViewModel = GlobalSearchViewModel(apiService: ApiGeneral(), speechRecognitionService: CapturedSpeechToTextService())
    @IBOutlet weak var tableView: UITableView!
    private var disposeBag = DisposeBag()
    
    public var suggestionItems:[SearchSuggestions.SearchSuggestion] = [] {
        didSet{
            self.tableView.reloadData()
        }
    }
    public var resultItems:[SearchResults.SearchResult] = [] {
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: suggestionCellId, bundle: nil), forCellReuseIdentifier: suggestionCellId)
        tableView.register(UINib(nibName: resultCellId, bundle: nil), forCellReuseIdentifier: resultCellId)
        tableView.register(UINib(nibName: suggestionHeaderId, bundle: nil), forHeaderFooterViewReuseIdentifier: suggestionHeaderId)
        tableView.register(UINib(nibName: resultHeaderId, bundle: nil), forHeaderFooterViewReuseIdentifier: resultHeaderId)
        setupHooks()
        
    }
    func setupHooks(){
        searchTxt.addTarget(self, action: #selector(searchTextChanged), for: .editingChanged)
        
        self.viewModel.suggestion.asDriver().drive(onNext: { [unowned self](suggestions) in
            if let suggestions = suggestions {
                self.suggestionItems = suggestions.Results
            }else{
                self.suggestionItems = []
            }
        }).disposed(by: disposeBag)
        self.viewModel.result.asDriver().drive(onNext: { [unowned self](results) in
            if let results = results {
                self.resultItems = results.Results
            }else{
                self.resultItems = []
            }
        }).disposed(by: disposeBag)
    }
    @IBAction func micBtnClicked(_ sender: Any) {
        if CapturedSpeechToTextService.isCapturedSpeechToTextServiceAvailable() {
            self.displayBlockingToast(message: L10n.recognizingVoice)
            self.viewModel.startSpeechService().asObservable().subscribe(onNext: { (str:String?) in
                self.searchTxt.text = str
                self.searchTextChanged()
            }).disposed(by: disposeBag)
        }
    }
    
    @IBAction func backBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
    
    @objc func searchTextChanged() {
        switch mode {
        case .suggestion:
            self.viewModel.currentQueryVar.value = self.searchTxt.text
        case .result:
            mode = .suggestion
            resultItems = []
            self.viewModel.currentQueryVar.value = self.searchTxt.text
        }
        
    }
    
    func displayBlockingToast(message:String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            alert.show()
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 10) {
            
            alert.dismiss(animated: true, completion: nil)
            let txt = self.searchTxt.text
            if txt != nil && txt != ""{
                self.searchTxt.becomeFirstResponder()
                self.searchTxt.text = txt
            }
            
        }
    }
}
extension GlobalSearchViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch mode {
        case .suggestion:
            let item = suggestionItems[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: suggestionCellId) as! SuggestionTableViewCell
            cell.setupCell(item)
            return cell
        case .result:
            let item = resultItems[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: resultCellId) as! ResultTableViewCell
            cell.setupCell(item)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch mode {
            case .suggestion:
                return suggestionItems.count
            case .result:
                return resultItems.count
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch mode {
        case .suggestion:
            self.mode = .result
            let item = suggestionItems[indexPath.row]
            suggestionItems = []
            tableView.reloadData()
            self.searchTxt.text = item.Term
            self.viewModel.suggestionQueryVar.value = item.Term
        case .result:
            let item = resultItems[indexPath.row]
            //todo handle navigation
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch mode {
        
        case .suggestion:
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: suggestionHeaderId)
            return view
        case .result:
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: resultHeaderId)
            let label = view?.viewWithTag(1) as! UILabel
            label.text = L10n.getGlobalSearchResultHeader(num: "\(resultItems.count)", total: "\(viewModel.result.value?.Count)")
            return view
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch mode {
        
        case .suggestion:
            if suggestionItems.count == 0{
                return 0
            }
        case .result:
            if resultItems.count == 0{
                return 0
            }
        }
        return 45
    }
}
