//
//  LocalizedSelectableButton.swift
//  TAMMApp
//
//  Created by kerolos on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

class LocalizedSelectableButton: UIButton, CustomFontComponent {    
    
    var attributedTextProxy: NSAttributedString?{
        get{
            return  titleLabel?.attributedText
        }
        set{
             titleLabel?.attributedText = newValue
        }
    }
    
    var textProxy: String?{
        return titleLabel?.text
    }
    
    
    var lineBreakModeProxy: NSLineBreakMode?{
        
        return titleLabel?.lineBreakMode
    }
    
    var textAlignment: NSTextAlignment{
        return titleLabel?.textAlignment ?? NSTextAlignment.natural
    }
    
    var fontProxy: UIFont?{
        get{
            return self.titleLabel?.font
        }
        set {
            self.titleLabel?.font = newValue
        }
    }
    
    @IBInspectable
    var applyFontScalling: Bool = false
    
    @IBInspectable var RTLFont:String = "Swissra-Normal"
    @IBInspectable var RTLSelecttedFont:String = "Swissra-Normal"

    @IBInspectable var originalColor:UIColor!
    @IBInspectable var selectedColor:UIColor!

    
    // properties currently not used
    @IBInspectable
    var isButtonSelected: Bool = false {
        didSet{
            updateFontAndColor()
        }
    }
    
    var selectedFontReference: UIFont!{
        didSet{
            updateFontAndColor()
        }
    }

    
    var originalFont: UIFont?{
        didSet{
            updateFontAndColor()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        print(fontProxy?.fontName)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        print(fontProxy?.fontName)
        commonInit()
    }
    
    private func commonInit(){
        self.originalFont = fontProxy
        self.selectedFontReference = fontProxy
        self.originalColor = self.titleLabel?.textColor
        self.selectedColor = self.titleLabel?.textColor
        registerTextObserver()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
//        self.adjustAlignment(RTL: isRTL)
        //        self.adjustFont(RTL: isRTL)
    }
    
    fileprivate func adjustAlignment(RTL:Bool){
        if(RTL){
            if(self.titleLabel?.textAlignment != .center){
                self.titleLabel?.textAlignment = .right
            }
        }
        else{
            if(self.titleLabel?.textAlignment != .center){
                self.titleLabel?.textAlignment = .left
            }
        }
    }
    
    func updateFontAndColor(){
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        let font = isButtonSelected ? selectedFontReference : originalFont
        let arFamilyStyleName = isButtonSelected ? RTLSelecttedFont : RTLFont
        adjustFont(RTL:isRTL, fontReference: font!, arFontFamily: arFamilyStyleName)
        
        self.setTitleColor( isButtonSelected ? selectedColor : originalColor, for: .normal)
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateFontAndColor()
    }
    

    var disposeBag = DisposeBag()
    fileprivate func registerTextObserver() {
        //        print(fontProxy?.fontName)
        (UIApplication.shared.delegate as! AppDelegate).isTraitChanged.asObservable().subscribe(onNext: { [unowned self] (_) in
            self.adjustFont()
            
        }).disposed(by: disposeBag)
        self.addObserver(self, forKeyPath: #keyPath(titleLabel.text), options: [.old, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(titleLabel.text) {
            adjustFont()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: #keyPath(titleLabel.text) )
    }
  
    
}
