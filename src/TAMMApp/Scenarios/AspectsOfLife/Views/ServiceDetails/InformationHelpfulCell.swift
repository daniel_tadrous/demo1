//
//  InformationHelpful.swift
//  TAMMApp
//
//  Created by Daniel on 7/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

class InformationHelpfulCell : UITableViewCell{
    
    public var viewModel: AolServiceWithFaqsViewModel?
   
    @IBOutlet weak var rightBtn: LocalizedButton!
    
    @IBOutlet weak var crossBtn: LocalizedButton!
    @IBOutlet weak var textView: RoundedTextView!
    
    @IBOutlet weak var stackView: UIStackView!
    var reloadSection:(()->Void)?
    @IBAction func yesClicked(_ sender: LocalizedButton) {
        self.viewModel?.isYesSelected = true
        if !(self.viewModel?.isInformationHelpfulCellExpanded)!{
            show()
            reloadSection?()
        }else{
            setBtnsColors()
        }
        
    }
    
    @IBAction func noClicked(_ sender: LocalizedButton) {
        self.viewModel?.isYesSelected = false
        if !(self.viewModel?.isInformationHelpfulCellExpanded)!{
            show()
            reloadSection?()
        }else{
            setBtnsColors()
        }
    }
    @IBAction func submitClicked(_ sender: UIButton) {
        self.viewModel?.submitFeedback(comment: self.textView.text, isYesSelected: (self.viewModel?.isYesSelected)!)
        self.textView.text = ""
        self.textView.placeholder = L10n.additionalComments
        self.textView.placeHolderVariable.isHidden = false
        self.viewModel?.isYesSelected = nil
        hide()
        reloadSection?()
    }
    
    @IBAction func cancelClicked(_ sender: UIButton) {
        
        //self.textView.text = ""
        self.textView.placeholder = L10n.additionalComments
        self.viewModel?.isYesSelected = nil
        hide()
        reloadSection?()
    }
    
    func hide(){
        self.viewModel?.isInformationHelpfulCellExpanded = false
        for v in self.stackView.arrangedSubviews{
            v.isHidden = true
        }
    }
    
    func show(){
        self.viewModel?.isInformationHelpfulCellExpanded = true
        for v in self.stackView.arrangedSubviews{
            v.isHidden = false
        }
    }
    func setBtnsColors(){
        if self.viewModel?.isYesSelected != nil{
            if (self.viewModel?.isYesSelected!)!{
                rightBtn.backgroundColor = UIColor.lichen
                rightBtn.setTitleColor(UIColor.white, for: .normal)
                crossBtn.backgroundColor = UIColor.white
                crossBtn.setTitleColor(UIColor.grapefruit, for: .normal)
            }else{
                crossBtn.backgroundColor = UIColor.grapefruit
                crossBtn.setTitleColor(UIColor.white, for: .normal)
                
                rightBtn.backgroundColor = UIColor.white
                rightBtn.setTitleColor(UIColor.lichen, for: .normal)
            }
        }else{
            rightBtn.backgroundColor = UIColor.white
            rightBtn.setTitleColor(UIColor.lichen, for: .normal)
            crossBtn.backgroundColor = UIColor.white
            crossBtn.setTitleColor(UIColor.grapefruit, for: .normal)
        }
    }
    
}
