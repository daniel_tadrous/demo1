//
//  EventsParentViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol EventsParentViewControllerDelegate {
    func openEventsTab()
    func openPublicHolidays()
}

class EventsParentViewController:  ContainerViewController, EventsParentStoryboardLoadable {
    
    var delegate:EventsParentViewControllerDelegate?
    
    @IBOutlet weak var fullLineView: UIView!
    @IBOutlet weak var eventsButton: LocalizedSelectableButton!
    @IBOutlet weak var publicHolidaysButton: LocalizedSelectableButton!
    
    
    fileprivate var eventsSelected = false
    fileprivate var publicHolidaysSelected = false
    
    @IBOutlet weak var underLine: UIView!
    fileprivate var underLineColor = UIColor(red: 0, green: 89, blue: 113, alpha: 1).getAdjustedColor()
    
    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    
    fileprivate var selectedArabicFont = UIFont(name: "Swissra-Bold",size: 18)
    fileprivate var unSelectedArabicFont = UIFont(name: "Swissra-Normal",size: 18)
    
    fileprivate var selectedFontColor = UIColor(hexString: "005971").getAdjustedColor()
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138").getAdjustedColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        eventsButton.originalColor = unSelectedFontColor
        eventsButton.selectedColor = selectedFontColor
        
        eventsButton.RTLSelecttedFont = "Swissra-Bold"
        publicHolidaysButton.RTLSelecttedFont = "Swissra-Bold"
        
        if L10n.Lang == "en" {
            eventsButton.originalFont = unSelectedFont
            eventsButton.fontProxy = unSelectedFont
            
            eventsButton.selectedFontReference = selectedFont
            
            publicHolidaysButton.originalFont = unSelectedFont
            publicHolidaysButton.fontProxy = unSelectedFont
            publicHolidaysButton.selectedFontReference = selectedFont
        }else {
            eventsButton.originalFont = unSelectedFont
            eventsButton.fontProxy = unSelectedArabicFont
            
            eventsButton.selectedFontReference = selectedArabicFont
            
            publicHolidaysButton.originalFont = unSelectedArabicFont
            publicHolidaysButton.fontProxy = unSelectedArabicFont
            publicHolidaysButton.selectedFontReference = selectedArabicFont
        }
       
        
        
        
        
        publicHolidaysButton.originalColor = unSelectedFontColor
        publicHolidaysButton.selectedColor = selectedFontColor
        
        self.title = L10n.communityEvents
        
        selectButtonAction(forButton: eventsButton, isEvents: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyInvertColors()
    }
    
    
    fileprivate func applyInvertColors() {
        let myselectedFontColor = UIColor(hexString: "005971").getAdjustedColor()
        let myunSelectedFontColor = UIColor(hexString: "161138").getAdjustedColor()
        eventsButton.selectedColor = myselectedFontColor
        publicHolidaysButton.selectedColor = myselectedFontColor
        eventsButton.originalColor = myunSelectedFontColor
        publicHolidaysButton.originalColor = myunSelectedFontColor
        if eventsSelected {
            eventsButton.setTitleColor(myselectedFontColor, for: .normal)
            publicHolidaysButton.setTitleColor(myunSelectedFontColor, for: .normal)
        }else {
            eventsButton.setTitleColor(myunSelectedFontColor, for: .normal)
            publicHolidaysButton.setTitleColor(myselectedFontColor, for: .normal)
        }
        
        
        self.view.backgroundColor = UIColor.paleGreyTwo.getAdjustedColor()
        fullLineView.backgroundColor = #colorLiteral(red: 0.8506266475, green: 0.8481912017, blue: 0.8733561635, alpha: 1).getAdjustedColor()
        underLine.backgroundColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
    }
    
    @IBAction func eventsButtonAction(_ sender: LocalizedSelectableButton) {
//        if L10n.Lang == "en" {
            self.selectButtonAction(forButton: sender, isEvents: true)
//        }else {
//            self.selectButtonAction(forButton: sender, isEvents: false)
//        }
    }
    
    @IBAction func publicHolidaysButtonAction(_ sender: LocalizedSelectableButton) {
      
            self.selectButtonAction(forButton: sender, isEvents: false)
       
    
    }
    
    fileprivate func selectButtonAction(forButton button:LocalizedSelectableButton, isEvents:Bool ) {
        eventsSelected = false
        publicHolidaysSelected = false
        eventsButton.isButtonSelected = false
        publicHolidaysButton.isButtonSelected = false
        
        if isEvents, !eventsSelected{
            if L10n.Lang == "en" {
                eventsSelected = true
                button.isButtonSelected = true
            }else {
                eventsSelected = false
                button.isButtonSelected = false
                publicHolidaysSelected = true
                button.isButtonSelected = true
            }
            
            //moveUnderLine(to: button)
            
            delegate?.openEventsTab()
        }
        else if !isEvents, !publicHolidaysSelected{
            
            if L10n.Lang == "en" {
                publicHolidaysSelected = true
                button.isButtonSelected = true
            }else {
                publicHolidaysSelected = false
                button.isButtonSelected = false
                eventsSelected = true
                button.isButtonSelected = true
            }
            //moveUnderLine(to: button)
            
            delegate?.openPublicHolidays()
        }
        
    }
    
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        if button == eventsButton {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x
        }
        else if button == publicHolidaysButton{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }

    }
    
    override func viewDidLayoutSubviews() {
        if eventsSelected {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
            
            
        else if publicHolidaysSelected{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
}
