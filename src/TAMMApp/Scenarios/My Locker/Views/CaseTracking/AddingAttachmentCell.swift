//
//  EntityCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AddingAttachmentCell: UITableViewCell {

    @IBOutlet weak var requestForInfoExtraText: UIView?
    @IBOutlet weak var addAttachmentBtn: LocalizedButton!
    
    @IBOutlet weak var addCommentBtn: LocalizedButton!
    
    var addAttachmenetHandler: (()->Void)?
    
    var addCommentHandler: (()->Void)?
    
    
    @IBAction func addAttachmentClick(_ sender: LocalizedButton) {
        addAttachmenetHandler?()
    }
    
    @IBAction func addCommentClick(_ sender: LocalizedButton) {
        addCommentHandler?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addCommentBtn.contentMode = .center
        self.addCommentBtn.titleLabel?.textAlignment = .center
        self.addAttachmentBtn.contentMode = .center
        self.addAttachmentBtn.titleLabel?.textAlignment = .center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCaseType(type:CaseDetailEnum){
        self.requestForInfoExtraText?.isHidden = type != .REQUEST_FOR_INFORMATION
      
    }

}
