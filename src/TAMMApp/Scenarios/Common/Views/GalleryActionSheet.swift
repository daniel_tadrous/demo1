//
//  GalleryActionSheet.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/7/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
protocol GalleryActionSheetDelegate {
    func openCamera()
    func openGallery()
    func openDocuments()
    func openSoundFiles()
}
class GalleryActionSheet{
    fileprivate var items = [CustomizableActionSheetItem]()
    fileprivate var actionSheet:CustomizableActionSheet!
    public var delegate:GalleryActionSheetDelegate!
    init(vc:UIViewController) {
        self.items = [CustomizableActionSheetItem]()
        
        // Setup custom view
        //camera
        if let sampleView = UINib(nibName: "SheetItemView", bundle: nil).instantiate(withOwner: vc, options: nil)[0] as? SheetItemView
        {
            let sampleViewItem = CustomizableActionSheetItem()
            sampleViewItem.type = .view
            sampleViewItem.view = sampleView
            sampleViewItem.height = 62
            sampleViewItem.downMargin = 1
            sampleView.isUpperMaskHidden = true
            sampleView.isLowerMaskHidden = false
            sampleView.titelLabel.text = L10n.open_camera
            sampleView.titelLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.text = "#"
            sampleView.fontIconLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.font = UIFont.init(name: "tamm", size: 30)
            sampleView.fontIconLabel.textAlignment = .center
            
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.cameraItemClicked))
            sampleView.addGestureRecognizer(gesture)
            // used if the action is a button
            //            sampleViewItem.selectAction = { _ in
            //                self.camera()
            //            }
            items.append(sampleViewItem)
        }
        
        //sound file
        if let sampleView = UINib(nibName: "SheetItemView", bundle: nil).instantiate(withOwner: vc, options: nil)[0] as? SheetItemView
        {
            let sampleViewItem = CustomizableActionSheetItem()
            sampleViewItem.type = .view
            sampleViewItem.view = sampleView
            sampleViewItem.height = 62
            sampleViewItem.downMargin = 1
            sampleView.isUpperMaskHidden = true
            sampleView.isLowerMaskHidden = false
            sampleView.titelLabel.text = L10n.chooseSoundFile
            sampleView.titelLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.text = "l"
            sampleView.fontIconLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.font = UIFont.init(name: "tamm", size: 30)
            sampleView.fontIconLabel.textAlignment = .center
            
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.soundItemClicked))
            sampleView.addGestureRecognizer(gesture)
            // used if the action is a button
            //            sampleViewItem.selectAction = { _ in
            //                self.camera()
            //            }
            items.append(sampleViewItem)
        }
        
        // choose a document
        if let sampleView = UINib(nibName: "SheetItemView", bundle: nil).instantiate(withOwner: vc, options: nil)[0] as? SheetItemView
        {
            let sampleViewItem = CustomizableActionSheetItem()
            sampleViewItem.type = .view
            sampleViewItem.view = sampleView
            sampleViewItem.height = 62
            sampleViewItem.downMargin = 1
            sampleView.isUpperMaskHidden = true
            sampleView.isLowerMaskHidden = false
            sampleView.titelLabel.text = L10n.chooseDocument
            sampleView.titelLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.text = "\u{e023}"
            sampleView.fontIconLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.font = UIFont.init(name: "tamm", size: 30)
            sampleView.fontIconLabel.textAlignment = .center
            
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.documentItemClicked))
            sampleView.addGestureRecognizer(gesture)
            // used if the action is a button
            //            sampleViewItem.selectAction = { _ in
            //                self.camera()
            //            }
            items.append(sampleViewItem)
        }
        // choose from Library
        if let sampleView = UINib(nibName: "SheetItemView", bundle: nil).instantiate(withOwner: vc, options: nil)[0] as? SheetItemView
        {
            let sampleViewItem = CustomizableActionSheetItem()
            sampleViewItem.type = .view
            sampleViewItem.view = sampleView
            sampleViewItem.height = 62
            sampleViewItem.downMargin = 10
            sampleView.isUpperMaskHidden = false
            sampleView.isLowerMaskHidden = true
            sampleView.titelLabel.text = L10n.upload_from_library
            sampleView.titelLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.text = ":"
            sampleView.fontIconLabel.textColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1)
            sampleView.fontIconLabel.font = UIFont.init(name: "tamm", size: 30)
            sampleView.fontIconLabel.textAlignment = .center
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.galleryItemClicked))
            sampleView.addGestureRecognizer(gesture)
            //            // used if the action is a button
            //            sampleViewItem.selectAction = { _ in
            //                self.photoLibrary()
            //            }
            items.append(sampleViewItem)
        }
        if let sampleView = UINib(nibName: "SheetCancelView", bundle: nil).instantiate(withOwner: vc, options: nil)[0] as? SheetCancelView
        {
            let closeItem = CustomizableActionSheetItem()
            closeItem.type = .view
            closeItem.view = sampleView
            closeItem.downMargin = 20
            sampleView.mainView.backgroundColor = #colorLiteral(red: 0.999055326, green: 0.8675454259, blue: 0.01394770201, alpha: 1)
            
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.cancelItemClicked))
            sampleView.addGestureRecognizer(gesture)
            
            items.append(closeItem)
        }
        self.actionSheet = CustomizableActionSheet()
    }
    
    func show(inView view:UIView){
        self.actionSheet.showInView(view, items: items)
    }
    
    @objc func cameraItemClicked(){
        self.actionSheet.dismiss(cancel:false)
        self.delegate.openCamera()
    }
    
    @objc func galleryItemClicked(){
        self.actionSheet.dismiss(cancel:false)
        self.delegate.openGallery()
    }
    
    @objc func soundItemClicked(){
        self.actionSheet.dismiss(cancel:false)
        self.delegate.openSoundFiles()
    }
    
    @objc func documentItemClicked(){
        self.actionSheet.dismiss(cancel:false)
        self.delegate.openDocuments()
    }
    
    @objc func cancelItemClicked(){
        
        self.actionSheet.dismiss(cancel:true)
    }
}
