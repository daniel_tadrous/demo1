//
//  ManageDocumentsTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit



class ManageDocumentsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageIcon: UIImageView!
    
    @IBOutlet weak var fileNameLabel: LocalizedLabel!
    
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    
    @IBOutlet weak var viewOverlay: UIView!

    @IBOutlet weak var clickedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewOverlay.isHidden = true
        self.addRoundCorners(radious: 5)
        viewOverlay.addRoundCorners(radious: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
