//
//  MyEventsViewModel.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import EventKit

class MyEventsViewModel : NSObject, SearchViewModelDelegate {
    
    private var apiService: ApiMyEventsServiceType!
    private var locationService: LocationServiceType!
    private(set) var events: Variable<[Event]?> = Variable(nil)
    
    let ascendingOrder = "asc"
    let descendingOrder = "desc"
    var sortDirection = Variable("")
    var filterObject = Variable(EventsFilterApplyModel())
    
    var searchedText:Variable<String?> = Variable(nil)
    var mySearchViewModel:SeacrhViewModel = SeacrhViewModel()
    var isToGetTrending: Variable<Bool?> = Variable(nil)
    var trendingVar: Variable<[String]?> = Variable(nil)
    var queryResultVar: Variable<[String]?> = Variable(nil)
    var currentQueryVar: Variable<String?> = Variable(nil)
    
    private(set) var currentLocationViewData: Variable<CLLocation?> = Variable(nil)
    
    fileprivate var disposeBag = DisposeBag()
    
    
    
    private(set) var categoriesViewData: Variable<[Event.Category]?> = Variable(nil)
    private(set) var locationsViewData: Variable<[String]?> = Variable(nil)
    private(set) var eventsFilterApplyModel: Variable<EventsFilterApplyModel?> = Variable(nil)
    private(set) var isToGetCategories: Variable<Bool?> = Variable(nil)
    private(set) var locationsQuery: Variable<String?> = Variable(nil)
    
    
    init(apiService:ApiMyEventsServiceType,locationService: LocationServiceType) {
        super.init()
        self.apiService = apiService
        self.locationService = locationService
        setupBinding()
        
    }
    
    func setupBinding() {
        
        eventsFilterApplyModel.asObservable()
            .subscribe(onNext:{ [unowned self] events in
                self.getMyEvents()
            })
            .disposed(by:disposeBag)
        isToGetCategories.asObservable().subscribe(onNext:{ [unowned self] cats in
            self.getCategories()
        }).disposed(by:disposeBag)
        
        locationsQuery.asObservable().subscribe(onNext:{ [unowned self] locations in
            self.getLocations()
        }).disposed(by:disposeBag)
        
        
        
        
        sortDirection.asObservable().subscribe(onNext: { [unowned self] (object) in
            self.getMyEvents()
        })
            .disposed(by: disposeBag)
        
        isToGetTrending.asObservable().subscribe(onNext: { (go:Bool?) in
            if go != nil{
                self.getTrending()
            }
        }).disposed(by: disposeBag)
        
        currentQueryVar.asObservable().subscribe(onNext: { (descriptionText) in
            if descriptionText != nil {
                self.getQueryResults(q:descriptionText!)
            }
        }).disposed(by: disposeBag)
        
        searchedText.asObservable().subscribe(onNext: { [unowned self] (object) in
            self.filterObject.value.q = self.searchedText.value ?? ""
            self.getMyEvents()
        })
            .disposed(by: disposeBag)
        
    }
    fileprivate func getCategories(){
        if isToGetCategories.value != nil && isToGetCategories.value!{
            apiService.getFilterCategories()
                .subscribe(onNext: { [unowned self] cats in
                    self.categoriesViewData.value = cats
                }).disposed(by: disposeBag)
        }else{
            self.categoriesViewData.value  = nil
        }
    }
    fileprivate func getLocations(){
        if  (locationsQuery.value != nil) && !(locationsQuery.value?.isEmpty)!
        {
            apiService.getFilterLocations(q: locationsQuery.value!)
                .subscribe(onNext: { [unowned self] locations in
                    self.locationsViewData.value = locations
                }).disposed(by: disposeBag)
        }else{
            self.locationsViewData.value  = nil
        }
    }
    
    func getQueryResults(q: String) {
        apiService.searchFor(query: q, type: 1).asObservable().subscribe(onNext: { items in
            self.queryResultVar.value = items
            print("\(items)")
        }).disposed(by: disposeBag)
    }
    
    func getTrending() {
        apiService.searchFor(query: "", type: 0).asObservable().subscribe(onNext: { trendingItems in
            self.trendingVar.value = trendingItems
            print("\(trendingItems)")
        }).disposed(by: disposeBag)
    }
    
    func getMyEvents() {
        apiService.getMyEvents(filterObject: filterObject.value).subscribe(onNext: { [unowned self] (eventsRes) in
            self.events.value = eventsRes
        })
            .disposed(by: disposeBag)
    }
    
    func addToCalendar(event:Event){
        let eventStore : EKEventStore = EKEventStore()
        let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        eventStore.requestAccess(to: .event) { (granted, error) in
            if (granted) && (error == nil) {
                let eventC:EKEvent = EKEvent(eventStore: eventStore)
                eventC.title = event.title
                eventC.startDate = event.start?.getDate(format: inputFormat)
                eventC.endDate = event.end?.getDate(format: inputFormat)
                eventC.notes = event.description
                eventC.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(eventC, span: .thisEvent)
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                print("Saved Event")
                DispatchQueue.main.async {
                    Toast(message: L10n.MyCommunity.Events.eventAddedSuccessfully).Show()
                }
            }else{
                DispatchQueue.main.async {
                    let permessionView = GoToSettingsView(frame: UIScreen.main.bounds, message: L10n.MyCommunity.Events.permessionDenied, onComplete: {
                        self.addToCalendar(event: event)
                    })
                    UIApplication.shared.keyWindow!.addSubview(permessionView)
                    UIApplication.shared.keyWindow!.bringSubview(toFront: permessionView)
                }
            }
        }
    }
    
    func buyTickets() {
        Toast(message: L10n.commingSoon).Show()
    }
    
    func sort() {
        switch sortDirection.value {
        case ascendingOrder:
            filterObject.value.sort = descendingOrder
            sortDirection.value = descendingOrder
        default:
            filterObject.value.sort = ascendingOrder
            sortDirection.value = ascendingOrder
        }
    }
}

extension MyEventsViewModel:filterViewModelDelegate{
    var apiServiceObject: ApiFilterServiceType {
        return apiService
    }
    
}

extension MyEventsViewModel: LocationServiceTypeDelegate{
    
    func onLocationUpdated(location: CLLocation) {
        self.currentLocationViewData.value = location
    }
}
extension MyEventsViewModel :FilterEventsViewModelProtocol{
    func setIsToGetCategories() {
        self.isToGetCategories.value = true
    }
    
    
    func setLocationQueryValue(q: String?) {
        self.locationsQuery.value = q
    }
    
    func getCategoriesVar() -> Variable<[Event.Category]?> {
        return self.categoriesViewData
    }
    
    func getLocationsVar() -> Variable<[String]?> {
        return self.locationsViewData
    }
    func startLocationService(){
        self.locationService.start(delegate: self)
    }
    func getCurrentLocation() -> Variable<CLLocation?> {
        return self.currentLocationViewData
    }
    
    
}
















