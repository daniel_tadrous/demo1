
//
//  AppCoordinator.swift
//  TammApp
//
//  Created by Igor Kulman on 03/10/2017.
//  Copyright © 2017 Igor Kulman. All rights reserved.
//

import Foundation
import Swinject
import UIKit
import RxSwift
import RxCocoa

enum AppChildCoordinator {
    case setup
    case feed
    case authentication
    case main
    case test
    case intro
}

class AppCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private let window: UIWindow
    let container: Container
    private var childCoordinators = [AppChildCoordinator: Coordinator]()
    
    private var authenticationService: AuthenticationService
    
    private var internetService:InternetCheckingService
    
    private let navigationController: UINavigationController
    private var userConfigService: UserConfigServiceType
    private let disposeBag = DisposeBag()
    public static var isNewSession: Bool = true
    // MARK: - Coordinator core
    
    // will use the service after development is done
    private var isFirstLaunch: Bool
    {
        get{
            return userConfigService.isFirstLaunch
        }
        set{
            userConfigService.isFirstLaunch = newValue
        }
    }
    
    private var justCameFromIntro: Bool = false
    
    
    init(window: UIWindow, container: Container) {
        self.window = window
        self.container = container
        navigationController = UINavigationController()
        
        let emptyVc = container.resolveViewController(EmptyLoadingScreenViewController.self)
        
        navigationController.pushViewController(emptyVc, animated: false)
        emptyVc.navigationController?.setNavigationBarHidden(true, animated: false)
        
        authenticationService = self.container.resolve(AuthenticationService.self)!
        internetService =  InternetCheckingService.shared
        
        self.userConfigService = container.resolve(UserConfigServiceType.self)!
        internetService.hasInternet.asObservable().subscribe(onNext: {
            hasInternet in
            if hasInternet != nil{
                if hasInternet!{
                    print("Has Internet")
                    
                }
                else{
                    print("no Internet")
                }
                if !self.isFirstLaunch && AppCoordinator.isNewSession{
                    self.start()
                }
            }
        }).disposed(by: disposeBag)
        
        // run time check for the platform
        if #available(iOS 11.0, *) {
            navigationController.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        navigationController.view.backgroundColor = UIColor.white
        
        self.window.rootViewController = navigationController
        if isFirstLaunch{
            startIntroCoordinator()
            return
        }
    }
    
    func start() {
        childCoordinators = [:]
        if !internetService.hasInternet.value!{
            self.showOfflineScreen()
            return
        }
        
        authenticationService = self.container.resolve(AuthenticationService.self)!
        if  !authenticationService.isAuthenticated {
            AppCoordinator.isNewSession = true
            self.showAuthentication()
        } else{
            AppCoordinator.isNewSession = false
            self.showMainTab()
        }
        
        //        showTestCoordinator()
    }
    
    func startIntroCoordinator(){
        justCameFromIntro = true
        
        let introCoordinator = IntroViewCoordinator(container: container, navigationController: navigationController)
        childCoordinators[.intro] = introCoordinator
        introCoordinator.delegate = self
        introCoordinator.start()
        
    }
    
    // will indicate if the login is triggered or not
    func startLogin() -> Bool  {
        authenticationService = self.container.resolve(AuthenticationService.self)!
        if  !authenticationService.isAuthenticated {
            let authenticationCoordinator = childCoordinators[.authentication] as? AuthenticationCoordinator
            if(authenticationCoordinator != nil){
                _ = authenticationCoordinator?.login()
                return true
            }
        } else{
            AppCoordinator.isNewSession = false
            self.showMainTab()
        }
        return false
    }
    
    private func showAuthentication(){
        let authenticationCoordinator = AuthenticationCoordinator(container: container, navigationController: navigationController, authenticationService: self.authenticationService, justCameFromIntro: justCameFromIntro)
        justCameFromIntro = false
        childCoordinators[.authentication] = authenticationCoordinator
        authenticationCoordinator.delegate = self
        authenticationCoordinator.start()
    }
    
    private func showAuthenticationAndStart(){
        let authenticationCoordinator = AuthenticationCoordinator(container: container, navigationController: navigationController, authenticationService: self.authenticationService, justCameFromIntro: self.justCameFromIntro)
        childCoordinators[.authentication] = authenticationCoordinator
        authenticationCoordinator.delegate = self
        authenticationCoordinator.start()
        _ = authenticationCoordinator.login()
    }
    
    
  
    
    
    private func showMainTab(){
        let homeCoordinator = MainTabCoordinator(container: container, navigationController: navigationController, authenticationService:self.authenticationService);
        childCoordinators[.main] = homeCoordinator
        homeCoordinator.delegate = self
        homeCoordinator.start()
        
    }
    
    // currently hosting the floating menu
    private func showTestCoordinator(){
    }
    
}

// MARK: - Delegate

extension AppCoordinator: AuthenticationCoordinatorDelegate {
    func authenticationCoordinatorDidFinish() {
        AppCoordinator.isNewSession = false
        self.showMainTab()
    }
}

extension AppCoordinator: MainTabCoordinatorDelegate {
    func MainTabCoordinatorDidFinish() {
        self.navigationController.viewControllers = [self.navigationController.topViewController!]
        start()
    }
    
    
}
extension AppCoordinator: IntroViewCoordinatorDelegate{
    func introViewCoordinatorDidFinish() {
        isFirstLaunch = false
        self.start()
    }
}
protocol OfflineScreensDelegate : class {
    func showOfflineScreen()
    func showPendingDraftsScreen()
    func showNewDraft(isFromOfflineScreen: Bool)
}

extension AppCoordinator: OfflineScreensDelegate{
    func showOfflineScreen(){
        AppCoordinator.isNewSession = true
        let vc = container.resolveViewController(OfflineScreenViewController.self)
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    func showPendingDraftsScreen(){
        let coordinator = childCoordinators[.main] as! MainTabCoordinator
        coordinator.tabBarViewController?.setMyLockerSelected(onComplete: {
            let mylocker = coordinator.childCoordinators[.myLocker] as! MyLockerCoordinator
            mylocker.openPendingDrafts()
            
            if InternetCheckingService.shared.hasInternet.value ?? false {
                if DALDrafts.getAll().count > 0{
                    let offlineDraftsPopUpView = OfflineDraftsPopUpView(frame: UIScreen.main.bounds,okClickHandler: self.showPendingDraftsScreen)
                    offlineDraftsPopUpView.tag = 1000
                    UIApplication.shared.keyWindow!.addSubview(offlineDraftsPopUpView)
                    UIApplication.shared.keyWindow!.bringSubview(toFront: offlineDraftsPopUpView)
                }
            }
        })
    }
    
    func showNewDraft(isFromOfflineScreen: Bool = false){
        let homeCoordinator = MainTabCoordinator(container: container, navigationController: navigationController, authenticationService:self.authenticationService);
        childCoordinators[.main] = homeCoordinator
        homeCoordinator.delegate = self
        homeCoordinator.start()
        homeCoordinator.tabBarViewController?.setMyLockerSelectedAtNewDraft(isFromOfflineScreen:isFromOfflineScreen)
    }
    func getChildrenCoordinators()->[AppChildCoordinator: Coordinator]{
        
        return childCoordinators
    }
}


