//
//  LocalizedTextField.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class LocalizedTextField: UITextField, CustomFontComponent {
    var disposeBag = DisposeBag()
    @IBInspectable
    var placeholderTextColor: UIColor?{
        didSet{
            adjustFont()
        }
    }
    
    var attributedTextProxy: NSAttributedString?{
        get{
            return attributedText
        }
        set{
            attributedText = newValue
        }
    }
    
    var textProxy: String?{
        return text
    }
    
    var lineBreakModeProxy: NSLineBreakMode?{
        
        return NSLineBreakMode.byWordWrapping
    }
    
    var fontProxy: UIFont?{
        get{
            return self.font
        }
        set {
            self.font = newValue
        }
    }
    
    @IBInspectable
    var applyFontScalling: Bool = false
    
    @IBInspectable var RTLFont:String = "Swissra-Normal"
//    // properties currently not used 
//    @IBInspectable
//    var customFontFamilyCode: Int = 0
//    @IBInspectable
//    var customFontStyleCode: Int = 0
//    @IBInspectable
//    var customFontSize: CGFloat = 16
    
    var originalFont: UIFont?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        self.adjustAlignment(RTL: isRTL)
//        self.adjustFont(RTL: isRTL)
    }

    
    fileprivate func adjustAlignment(RTL:Bool){
        if(RTL){
            if(self.textAlignment != .center){
                self.textAlignment = .right
            }
        }
        else{
            if(self.textAlignment != .center){
                self.textAlignment = .left
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        adjustFont()
        
    }
    
    fileprivate func registerTextObserver() {
        //        print(fontProxy?.fontName)
        (UIApplication.shared.delegate as! AppDelegate).isTraitChanged.asObservable().subscribe(onNext: { [unowned self] (_) in
            self.adjustFont()
            
        }).disposed(by: disposeBag)
        self.addObserver(self, forKeyPath: #keyPath(text), options: [.old, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(text) {
            adjustFont()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: #keyPath(text) )
    }
    
   

    override var description: String{
        get {
            return super.description + " text=\(String(describing: self.text))"
        }
    }
    

    
}
extension LocalizedTextField{
    func validateEmail() -> Bool{
        for text in (self.text?.components(separatedBy: "; "))! {
            if (text.validate(regex: String.regexes.email.rawValue)) {
            }
            else{
                return false
            }
        }
        return true
    }
}

