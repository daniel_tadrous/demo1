//
//  PagingViewController.swift
//  TAMMApp
//
//  Created by kerolos on 5/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

protocol PagingViewControllerDelegate: class{
    func pageViewControllerDidFinish()
}


class IntroPagingViewController : UIPageViewController{
    
    weak var coordinationDelegate: PagingViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
    }
    
    
    private var currentViewController: Int = 0
    private var allViewControllers = [UIViewController()]
    
    func getControllerAllViewControllers() -> [UIViewController] {
        let storyboardName = "IntroScreens"
        let sb = UIStoryboard.init(name: storyboardName, bundle: nil)
        var controllers = [UIViewController]()
        
        let names = ["screen 1", "screen 2", "screen 3", "screen 4"]
        for name in names{
            controllers.append(sb.instantiateViewController(withIdentifier: name) )
        }
        
        // should cast the last view controller to the 
        let lastController = controllers.last as! LastIntroScreenViewController
        lastController.delegate = self
        
        return controllers
    }
    
}

extension IntroPagingViewController: LastIntroScreenViewControllerDelegate{
    func dismissIntro() {
        coordinationDelegate?.pageViewControllerDidFinish()
    }
}


extension IntroPagingViewController: UIPageViewControllerDelegate {
    
}

extension IntroPagingViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = allViewControllers.index(of: viewController) else {
            return nil
        }

        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = allViewControllers.count

        guard orderedViewControllersCount != nextIndex else {
            return nil
        }

        guard orderedViewControllersCount > nextIndex else {
            return nil
        }

        return allViewControllers[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = allViewControllers.index(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else {
            return nil
        }

        guard allViewControllers.count > previousIndex else {
            return nil
        }

        return allViewControllers[previousIndex]
    }
    
  
    
    
//    func pageViewController(_ pageViewController: UIPageViewController,
//                            viewControllerBefore_ pageViewController: UIPageViewController,
//                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
//
//    }
//
//    func pageViewController(_ pageViewController: UIPageViewController,
//                            viewControllerBefore_ pageViewController: UIPageViewController,
//                            viewControllerAfter viewController: UIViewController) -> UIViewController? {

//    }
    
}







