//
//  EntitiesDetailsViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol EntityServicesViewControllerDelegate: class{
    
}

class EntityServicesViewController: TammViewController ,EntityDetailsStoryboardLodable{

    @IBOutlet weak var searchView: TammSearchView!
    @IBOutlet weak var servicesTableView: UITableView!
    @IBOutlet weak var onlineServicesSwitch: UISwitch!
    @IBOutlet weak var digitalServicesLabel: LocalizedLabel!
    
    public weak var delegate: EntityServicesViewControllerDelegate?
    public var viewModel: EntitiesDetailsViewModel?
    var disposeBag = DisposeBag()
    var servicesListToShow = [ServiceCategoryEntityModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIBinding()
        setupBinding()
        registerXib()
        addTapGesturseOnSearchView()
        searchView.addRoundCorners(radious: 24)
        searchView.addRoundCorners(radious: 24)
        servicesTableView.allowsSelection = false
        searchView.PlaceholderTextContent = L10n.searchForServices
        // Do any additional setup after loading the view.
    }
    
    fileprivate func addTapGesturseOnSearchView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(searchViewClicked))
        searchView.addGestureRecognizer(tap)
    }
    
     @objc func searchViewClicked() {
        let searchViewOverLay = SearchOverlayView(frame: UIScreen.main.bounds, currentViewModel: self.viewModel!, text: self.searchView.searchField.text!, currentSearchModel: (self.viewModel?.searchviewModel)!)
        searchViewOverLay.delegate = self
        UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay) 
        //Toast(message:L10n.commingSoon).Show()
    }
    
    fileprivate func registerXib() {
        self.servicesTableView.register(UINib(nibName: "EntityDetailsCollapsedCell", bundle: nil), forCellReuseIdentifier: "CollapsedServiceCell")
        self.servicesTableView.register(UINib(nibName: "EntityDetailsExpandedCell", bundle: nil), forCellReuseIdentifier: "ExpandedServiceCell")
        self.servicesTableView.register(UINib(nibName: "SectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderCell")
    }
    
    fileprivate func setupBinding(){
        viewModel!.servicecategories.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] servicesList in
                // assert(servicesList != nil)
                if let myservices = servicesList {
                    self.servicesListToShow = myservices
                    self.servicesTableView.reloadData();
                }else {
                    // To do someThing when list is nil
                }
            }).disposed(by: disposeBag)
    }
    
    
    fileprivate func setupUIBinding() {
        onlineServicesSwitch.rx.isOn.asObservable().subscribe(onNext: { _ in
            self.servicesTableView.reloadData()
        }).disposed(by: disposeBag)
        
    }
  
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   
    
}



extension EntityServicesViewController : UITableViewDelegate , UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return servicesListToShow.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let noOfrows = servicesListToShow[section].services?.count else {
            return 0
        }
        return noOfrows
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let service:ServiceModel = servicesListToShow[indexPath.section].services![indexPath.row]
        
        if service.isCollapsed {
            //cell is collapsed
                    let cell = servicesTableView.dequeueReusableCell(withIdentifier: "CollapsedServiceCell") as! CollapsedTableViewCell
                    cell.serviceTitleLabel.text = service.title
                    cell.expandCellAction.addTarget(self, action: #selector(expandCurrentCell(_:)), for: .touchUpInside)
                
                return cell
            
        }else {
            //cell is expanded
                    let cell = servicesTableView.dequeueReusableCell(withIdentifier: "ExpandedServiceCell") as! ExpandedTableViewCell
                    cell.serviceDescriptionLabel.text  = service.description
                    cell.serviceTitleLabel.text = service.title
                    cell.collapseServiceCellButton.addTarget(self, action: #selector(collapseCurrentCell(_:)), for: .touchUpInside)
                    cell.viewServiceDetailButton.addTarget(self, action: #selector(openDetailedService(_:)), for: .touchUpInside)
                return cell
        }
    }
    
     @objc func openDetailedService(_ sender:UIButton){
        let point = sender.convert(CGPoint.zero, to: servicesTableView as UIView)
        let indexPath: IndexPath! = servicesTableView.indexPathForRow(at: point)
        let service = servicesListToShow[indexPath.section].services?[indexPath.row]
        //navigate to detailed Service
        print("you clicked on service \(String(describing: service?.title))")
        Toast(message:L10n.commingSoon).Show()
    }
    
     @objc func expandCurrentCell(_ sender:UIButton){
        let point = sender.convert(CGPoint.zero, to: servicesTableView as UIView)
        let indexPath: IndexPath! = servicesTableView.indexPathForRow(at: point)
        servicesListToShow[indexPath.section].services?[indexPath.row].isCollapsed = false
        servicesTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
     @objc func collapseCurrentCell(_ sender:UIButton){
        let point = sender.convert(CGPoint.zero, to: servicesTableView as UIView)
        let indexPath: IndexPath! = servicesTableView.indexPathForRow(at: point)
        servicesListToShow[indexPath.section].services?[indexPath.row].isCollapsed = true
        servicesTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func shouldDisplayCell(service:ServiceModel)->Bool{
        
        if onlineServicesSwitch.isOn {
            if service.isOnlineService == true {
                return true
            } else {
                return false
            }
        }else {
            //switch is off return all ¬
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let service:ServiceModel = servicesListToShow[indexPath.section].services![indexPath.row]
        if shouldDisplayCell(service: service){
            let  rowHeight = UITableViewAutomaticDimension
            return rowHeight
            
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell") as! UITableViewHeaderFooterView
        let titlelabel = headerCell.viewWithTag(1) as! LocalizedLabel
        titlelabel.text = servicesListToShow[section].title
        
        return headerCell
    }

}

extension EntityServicesViewController : SearchOverlayViewDelegate{
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchView.searchField.text = message.name
        //self.searchView.PlaceholderTextContent = message.name
        self.servicesListToShow.removeAll()
        self.viewModel?.searchedText.value = message.name
    }
    func backPressed(){
        self.searchView.searchField.text = ""
    }
}



