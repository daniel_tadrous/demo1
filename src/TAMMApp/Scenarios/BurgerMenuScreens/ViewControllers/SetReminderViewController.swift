//
//  SetReminderViewController.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/7/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class SetReminderViewController: TammViewController , SetReminderStoryboardLoadable{

    public var viewModel: SetReminderViewModel!
    
    @IBOutlet weak var RemindmeLabel: LocalizedLabel!

    @IBOutlet weak var tenDaysReminderView: ReminderView!
    var remiderList:[ReminderModel] = [ReminderModel]()
    
    @IBOutlet weak var sevenDaysReminderView: ReminderView!
    
    @IBOutlet weak var dayReminderView: ReminderView!
    
    @IBOutlet var reminderViews: [ReminderView]!
    
    var remiderAfter:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         setNavigationBar(title: L10n.events, willSetSearchButton: true)
         setupRemiderViews()
         addTapOnReminderView()
    }


    
    func setupRemiderViews() {
        tenDaysReminderView.timeIntervalLabel.text = L10n.tendaysbefore
        sevenDaysReminderView.timeIntervalLabel.text = L10n.sevendaysbefore
        dayReminderView.timeIntervalLabel.text = L10n.onedaybefore
        //get which item is selected from api or user defaults
        if let reminder = remiderAfter {
            switch reminder {
            case 7 :sevenDaysReminderView.selectedView.backgroundColor = UIColor.darkBlueGrey
            case 10:tenDaysReminderView.selectedView.backgroundColor = UIColor.darkBlueGrey
            case 1 :dayReminderView.selectedView.backgroundColor = UIColor.darkBlueGrey
            default:
                return
            }
        }else {
            tenDaysReminderView.selectedView.backgroundColor = UIColor.darkBlueGrey
        }
        
    }
    
   @objc func handleTap(_ sender: UITapGestureRecognizer){
        let senderTag = sender.view!.tag
        for view in reminderViews {
            if senderTag == view.tag {
                view.selectedView.backgroundColor = UIColor.darkBlueGrey
            }else {
                view.selectedView.backgroundColor = UIColor.white
                //self.displayToast(message: view.timeIntervalLabel.text!)
            }
        }
    
        if senderTag == 1 {
            self.viewModel.remiderValue.value = 10
        }else if senderTag == 2 {
            self.viewModel.remiderValue.value = 7
        }else if senderTag == 3 {
            self.viewModel.remiderValue.value = 1
        }
    }
    
    
    func addTapOnReminderView() {

        let tenDaystapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        let sevenDaystapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        let oneDaytapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tenDaysReminderView.addGestureRecognizer(tenDaystapGesture)
        sevenDaysReminderView.addGestureRecognizer(sevenDaystapGesture)
        dayReminderView.addGestureRecognizer(oneDaytapGesture)
        tenDaysReminderView.tag = 1
        sevenDaysReminderView.tag = 2
        dayReminderView.tag = 3
        
    }

 
}



struct ReminderModel {
    var title:String
    var isSelected:Bool
}






