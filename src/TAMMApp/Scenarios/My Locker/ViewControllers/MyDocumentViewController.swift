//
//  MyDocumentViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MyDocumentViewControllerDelegate {
    func openActive()
    func openExpired()
}

enum DocumentType {
    case Active
    case Expired
}

class MyDocumentViewController: ContainerViewController,MyDocumentsStoryboardLoadable {
    @IBOutlet weak var activeButton: LocalizedSelectableButton!
    @IBOutlet weak var expiredButton: LocalizedSelectableButton!
    @IBOutlet weak var underLine: UIView!
    @IBOutlet weak var fullLineView: UIView!

    fileprivate var selectedFont = UIFont(name: "CircularStd-Bold",size: 18)
    fileprivate var unSelectedFont = UIFont(name: "CircularStd-Medium",size: 18)
    
    fileprivate var selectedArabicFont = UIFont(name: "Swissra-Bold",size: 18)
    fileprivate var unSelectedArabicFont = UIFont(name: "Swissra-Normal",size: 18)
    
    fileprivate var selectedFontColor = UIColor(hexString: "005971")
    fileprivate var unSelectedFontColor = UIColor(hexString: "161138")
  
    
    var delegate:MyDocumentViewControllerDelegate?
    
    var screenSelected:DocumentType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = L10n.MyDocsStrings.myDocuments
        
        activeButton.originalColor = unSelectedFontColor
        activeButton.selectedColor = selectedFontColor
        
        activeButton.RTLSelecttedFont = "Swissra-Bold"
        expiredButton.RTLSelecttedFont = "Swissra-Bold"
        
        if L10n.Lang == "en" {
            activeButton.originalFont = unSelectedFont
            activeButton.fontProxy = unSelectedFont
            
            activeButton.selectedFontReference = selectedFont
            
            expiredButton.originalFont = unSelectedFont
            expiredButton.fontProxy = unSelectedFont
            expiredButton.selectedFontReference = selectedFont
        }else {
            activeButton.originalFont = unSelectedFont
            activeButton.fontProxy = unSelectedArabicFont
            
            activeButton.selectedFontReference = selectedArabicFont
            
            expiredButton.originalFont = unSelectedArabicFont
            expiredButton.fontProxy = unSelectedArabicFont
            expiredButton.selectedFontReference = selectedArabicFont
        }
        
        expiredButton.originalColor = unSelectedFontColor
        expiredButton.selectedColor = selectedFontColor
        
        showScreen(.Active,forButton:activeButton)
    }
    
    @IBAction func activeButtonAction(_ sender: LocalizedSelectableButton) {
        
        showScreen(.Active, forButton:sender)
    }
    
    @IBAction func expiredButtonAction(_ sender: LocalizedSelectableButton) {
        showScreen(.Expired, forButton:sender)
    }
    
    func showScreen( _ screenName:DocumentType,forButton button:LocalizedSelectableButton){
        screenSelected = screenName
        if screenName == .Active{
            if L10n.Lang == "en"{
                activeButton.isButtonSelected = true
                expiredButton.isButtonSelected = false
            }
            else{
                screenSelected = .Expired
                activeButton.isButtonSelected = true
                expiredButton.isButtonSelected = false
            }
            delegate?.openActive()
        }
        else if screenName == .Expired{
            if L10n.Lang == "en"{
                activeButton.isButtonSelected = false
                expiredButton.isButtonSelected = true
            }
            else{
                screenSelected = .Active
                activeButton.isButtonSelected = false
                expiredButton.isButtonSelected = true
            }
            delegate?.openExpired()
        }
    }
    
    fileprivate func moveUnderLine(to button:LocalizedSelectableButton) {
        if button == activeButton {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x
        }
        else if button == expiredButton{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        if screenSelected == .Active {
            underLine.frame.origin.x = fullLineView.frame.origin.x
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
            
        }
            
            
        else if screenSelected == .Expired{
            underLine.frame.origin.x = fullLineView.frame.origin.x + fullLineView.frame.width/2
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
}
