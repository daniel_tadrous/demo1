//
//  MessagesViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import RxSwift

class MessageType {
    static let open = "open"
    static let closed = "closed"
}

class MessagesViewModel {
    
    
    var page: Variable<Int?> = Variable(nil)
    var searchedText : Variable<String?> = Variable(nil)
    var filteredByText : Variable<String?> = Variable(nil)
    var sortByText :  Variable<String?> = Variable(nil)
    var apiResponse : Variable<MsgResponse?> = Variable(nil)
    static var msgTypeStatic:String?
    var msgType:String?{
        set{
            MessagesViewModel.msgTypeStatic = newValue
        }
        get{
            return MessagesViewModel.msgTypeStatic
        }
    }
    private(set) var viewData: Variable<[MessageModel]?> = Variable(nil)
    private(set) var start: Variable<Bool?> = Variable(nil)
    private(set) var messages:Variable<[MessageModel]?> = Variable(nil)
    private(set) var messagesFilter: Variable<MessagesFilter?> = Variable(nil)
    private(set) var messagesUpdated: Variable<[MessageModel]?> = Variable(nil)
    fileprivate var apiService: ApiSupportServicesType!
    var searchModel = SearchModelWithoutTrending()
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiSupportServicesType) {
        self.apiService = apiService
        setupBinding()
    }
    
    
    fileprivate func requestMsgs() {
        if let pageNumber = page.value {
       apiService.getMsgs(withpage: pageNumber, andSearchedText: searchedText.value, andSorted: sortByText.value, andFilterBy: self.filteredByText.value,andMsgType: msgType!).asObservable().subscribe(onNext:{ response in
                self.apiResponse.value = response
                self.messages.value = response.messages
            }).disposed(by: disposeBag)
        }
    }
    
    fileprivate func setupBinding(){
        //new
        page.asObservable().subscribe(onNext: { [unowned self] number in
           self.requestMsgs()
        }).disposed(by: disposeBag)
        
//        searchedText.asObservable().subscribe(onNext: { [unowned self] searchedText in
//            self.requestMsgs()
//        }).disposed(by: disposeBag)
//
//        filteredByText.asObservable().subscribe(onNext: { [unowned self] filteredText in
//            self.requestMsgs()
//        }).disposed(by: disposeBag)
//
//        sortByText.asObservable().subscribe(onNext: { [unowned self] sortedText in
//            self.requestMsgs()
//        }).disposed(by: disposeBag)
        
        
        
        
        
        //old
        // page opened
//        start.asObservable()
//            .subscribe(onNext:{ [unowned self] serviceId in
//                self.requestDetails()
//            })
//            .disposed(by:disposeBag)
        // for
//        messagesFilter.asObservable()
//            .subscribe(onNext:{ [unowned self] serviceId in
//                self.requestDetailsWithFilter()
//            })
//            .disposed(by:disposeBag)
        
        
        messages.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
               self.updateMessage()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if start.value != nil && start.value!{
            apiService.getMyMessages(filter: nil)
                .subscribe(onNext: { [unowned self] polls in
                    self.viewData.value = polls
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
    fileprivate func requestDetailsWithFilter(){
        if messagesFilter.value != nil {
            apiService.getMyMessages(filter: messagesFilter.value)
                .subscribe(onNext: { [unowned self] messages in
                    self.viewData.value = messages
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func updateMessage(){
        if messages.value != nil{
            apiService.updateMessage(messages: messages.value!)
                .subscribe(onNext: { [unowned self] messagesAr in
                    
//                    for mess in self.messages.value!{
//                        let index = 0
//                        for m in self.viewData.value!{
//                            if mess.id == m.id{
//
//                                m.isArchived = mess.isArchived
//                                //m.isDeleted = message.isDeleted
//                                m.isStarred = mess.isStarred
//                                if mess.isDeleted!{
//                                    self.viewData.value?.remove(at: index)
//                                }
//
//                                break
//
//
//                            }
//                        }
//                    }
//                    self.messagesUpdated.value = self.messages.value
                  
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
    func isLastMsgPage() -> Bool {
        if let allPagesCount = apiResponse.value?.pageCount {
            if page.value == allPagesCount {
                return true
            }else{
                //page is not last page
                return false
            }
        }else {
            // total pages is nil
            return false
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}

