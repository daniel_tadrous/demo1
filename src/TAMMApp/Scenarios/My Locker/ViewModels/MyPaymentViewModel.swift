//
//  MyPaymentViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

struct SearchObject {
    var txt : String
    var from : String
    var to : String
}

class MyPaymentViewModel {
    
    var paymentType : PaymentType?

    var page: Variable<Int?> = Variable(nil)
    var searchedText : Variable<SearchObject?> = Variable(nil)
    var filteredByText : Variable<String?> = Variable(nil)
    var sortByText :  Variable<String?> = Variable(nil)
    var paymentResponse : Variable<PaymentResponseModel?> = Variable(nil)
    
    fileprivate var apiService: MyPaymentsServiceType!
    var searchModel = SearchModelWithoutTrending()
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: MyPaymentsServiceType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        //new
        page.asObservable().subscribe(onNext: { [unowned self] number in
            self.requestPayments()
        }).disposed(by: disposeBag)
        
        
    }
    
    func requestPayments() {
        if page.value != nil && paymentType != nil {
            apiService.getMyPayments(page: page.value!, paymentType: paymentType!, search: searchedText.value, filter: filteredByText.value, sort: sortByText.value).asObservable().subscribe(onNext: { paymentResponse in
                    self.paymentResponse.value = paymentResponse
                }).disposed(by: disposeBag)
        }
    }
    
     func getisFirstTimeStatus() ->Bool {
        let defaults = UserDefaults.standard
        
        let wifiStatus = defaults.object(forKey: UserDefaultsKeys.isFirstTimePayments)
        
        if let status = wifiStatus {
            return status as! Bool
        }else {
            return true
        }
    }
    
    func setIsFirstTimeStatus(value :Bool){
        let defaults = UserDefaults.standard
       defaults.set(value, forKey: UserDefaultsKeys.isFirstTimePayments)
        defaults.synchronize()
    }
    
    func getSortedOptions() -> [String] {
        return [L10n.dueDateAsc, L10n.dueDateDesc , L10n.amountAsc, L10n.amountDesc]
    }
    
    func getSortedText() -> String {
        
        if sortByText.value == nil {
            return L10n.dueDateAsc
        }else {
            return sortByText.value!
        }
    }
    
}
