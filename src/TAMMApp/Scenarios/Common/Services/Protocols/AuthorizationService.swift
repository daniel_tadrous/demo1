//
//  AuthorizationService.swift
//  TAMMApp
//
//  Created by kerolos on 3/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AuthenticationService: NSObject {

    @property(nonatomic, strong, nullable)
    id<OIDAuthorizationFlowSession> currentAuthorizationFlow;
    And your main class, a property to store the auth state:
    
    // property of the containing class
    @property(nonatomic, strong, nullable) OIDAuthState *authState;
}
