//
//  UILabel+Extension.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

extension UILabel{
    func addCharacterSpacing(space:Double) {
            if let labelText = self.text, labelText.count > 0 {
                let attributedString = NSMutableAttributedString(string: labelText)
                attributedString.addAttribute(NSAttributedStringKey.kern, value: 2.3, range: NSRange(location: 0, length: attributedString.length - 1))
                attributedText = attributedString
            }
        }
}
