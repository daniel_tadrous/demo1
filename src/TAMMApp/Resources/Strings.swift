// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum L10n {
    // name for general tabel tabel
    private static let t1 = "Localizable"
    /// About
    static var about: String { get{ return L10n.tr(t1, "about") } }
    // Personal
    static var personal: String{ get { return L10n.tr(t1, "Personal") } }
    // Business
    static var business: String { get { return L10n.tr(t1, "Business") } }
    // Back
    static var back: String { get { return L10n.tr(t1,"back") } }
    
    static var freq_asked_questions: String { get { return L10n.tr(t1,"freq_asked_questions") } }
    
    static var complete: String { get { return L10n.tr(t1,"Complete") } }
    
    static var upload_from_library: String { get { return L10n.tr(t1,"Upload from Library") } }

    static var open_camera: String { get { return L10n.tr(t1,"Open camera") } }

    static var ongoing_journeys: String { get { return L10n.tr(t1,"Ongoing Journeys") } }

    static var explore_journeys: String { get { return L10n.tr(t1,"Explore Journeys") } }

    static var show_all: String { get { return L10n.tr(t1,"Show All") } }

    static var show_more: String { get { return L10n.tr(t1,"Show More") } }
    
    static var searchEllipsized: String { get { return L10n.tr(t1,"Search...") } }
    
    static var talk_to_us_header: String { get { return L10n.tr(t1,"Talk To Us Header") } }

    static var upload_Over_WIFI: String { get { return L10n.tr(t1,"Upload_Over_Wifi") } }

    static var Status_tracker: String { get { return L10n.tr(t1,"Status Tracker") } }

    static var serviceCode: String { get { return L10n.tr(t1,"Service Code") } }

    static func getServiceSectionHeader(section: Int)->String{
        switch section {
        case 4:
            return L10n.tr(t1, "Related Journeys" )
        case 5:
            return L10n.tr(t1, "Top FAQs" )
        default:
            return L10n.tr(t1, " ")
        }
    }
    static func getServicePopupHeader(type: PopupType)->String{
        switch type {
        
        case .documents:
             return L10n.requiredDocs
        case .phones:
             return L10n.tr(t1, "Phone")
        case .location:
             return L10n.tr(t1, "Location")
        case .conditions:
             return L10n.serviceConditions
        case .apps:
             return L10n.mobileApps
        case .fees:
             return L10n.fees
        case .related_services:
             return L10n.relatedServices
        }
    }
    static func getServiceSection3RowText(row: Int)->String{
        switch row {
            case 0:
                return L10n.mainService
            
            case 1:
                return L10n.relatedServices
            case 2:
                return L10n.relatedSubTopics
            default:
            return L10n.tr(t1, " ")
        }
    }
    static var commingSoon: String { get { return L10n.tr(t1,"CommingSoon") } }
    
    // mainly intro page
    static var updates: String { get { return L10n.tr(t1,"Updates") } }
    static var updates_intro_text: String { get { return L10n.tr(t1,"updates_intro_text") } }
    static var get_started: String { get { return L10n.tr(t1,"Get Started") } }
    static var talk_to_us: String { get { return L10n.tr(t1,"Talk To Us") } }
    static var talk_to_us_intro_text: String { get { return L10n.tr(t1,"talk_to_us_intro_text") } }
    static var services: String { get { return L10n.tr(t1,"Services") } }
    static var services_intro_text: String { get { return L10n.tr(t1,"services_intro_text") } }
    static var tailored_journeys: String { get { return L10n.tr(t1,"Tailored Journeys") } }
    static var tailored_journeys_intro_text: String { get { return L10n.tr(t1,"tailored_journeys_intro_text") } }
    static var view_entity_page: String { get { return L10n.tr(t1,"View Entity Page") } }

    //faqs screen
    static var search_placeholder: String { get{ return L10n.tr(t1, "search_placeholder") } }
    static var load_more: String { get { return L10n.tr(t1,"Load More") }    }
    static var faqs: String { get { return L10n.tr(t1,"faqs") }}
    
    //faqs filter
    static var select_aspect_of_life: String { get { return L10n.tr(t1,"Select Aspect of Life") }}
    static var select_category: String { get { return L10n.tr(t1,"Select Category") }}
    static var Individual: String { get { return L10n.tr(t1,"Individual") }}
    static var Business: String { get { return L10n.tr(t1,"Business") }}
    static var Tourist: String { get { return L10n.tr(t1,"Tourist") }}
    static var Lang: String { get { return L10n.tr(t1,"lang") }}
    // contact message
    static func contactUsMessages(emailUs: String, chatWithUs: String, phone1: String, phone2: String ) -> String {
        return L10n.tr(t1,"contactUsMessages", emailUs, chatWithUs, phone1, phone2)
    }
    static func getContactUsLine1(emailUs: String, chatWithUs: String) -> String {
        return L10n.tr(t1,"contactUsLine1", emailUs, chatWithUs)
    }
    static func getContactUsLine2(phone1: String, phone2: String ) -> String {
        return L10n.tr(t1,"contactUsLine2", phone1, phone2)
    }
    static var emailUs: String { get { return L10n.tr(t1,"emailUsText") } }
    static var chatWithUs: String { get { return L10n.tr(t1,"chatWithUs") } }

    
    static var phone1: String { get { return L10n.tr(t1,"phone_1") } }
    static var phone2: String { get { return L10n.tr(t1,"phone_2") } }
    static var policeNumber: String { get { return L10n.tr(t1,"Police_Number") } }
    static var civilDefenseNumber: String { get { return L10n.tr(t1,"Civil_Defense_Number") } }
    static var electricityNumber: String { get { return L10n.tr(t1,"Electricity_Number") } }
    static var AmbulanceNumber: String { get { return L10n.tr(t1,"Ambulance_Number") } }
    
    //
    static var acceptTermsConditions: String{ get{ return L10n.tr(t1,"Accept Terms & conditions") } }
    static var messageTitleMandatory:String{ get{ return L10n.tr(t1,"Title Message is required") } }
    static var mustChooseContactMethod: String{ get{ return L10n.tr(t1,"Must choose a contact method") } }
    static var locationMandatory: String { get { return L10n.tr(t1,"LocationMandatoy") } }
    static var termsAndConditions: String { get { return L10n.tr(t1,"termsAndConditions") } }
    static func readTermsAndConditions(termsAndConditions: String) -> String {
        return L10n.tr(t1,"readTermsAndConditions", termsAndConditions)
    }
    // missing titles
    static var myLocker: String { get { return L10n.tr(t1, "My Locker") } }
    static var support: String { get { return L10n.tr(t1, "Support") } }
    static var checkStatus: String { get { return L10n.tr(t1, "Check status") } }
    static var confirmation: String { get { return L10n.tr(t1, "Confirmation") } }

    //drafts
    static var send: String { get { return L10n.tr(t1,"send") }}
    static var save_to_drafts: String { get { return L10n.tr(t1,"save_to_drafts") }}
    
    static func pendingItems(number: Int) -> String {
        return L10n.tr(t1,"pending_items", number)
    }
    static var servicesTitle: String { get { return L10n.tr(t1,"ServicesTitle") }}
    
    // Messages and drafts

    static var newTitle: String { get { return L10n.tr(t1,"newTitle") }}
    static var draftsTitle: String { get { return L10n.tr(t1,"draftsTitle") }}
    static var answer_all: String { get { return L10n.tr(t1,"answer_all")}}
    static var commentNotEmpty: String { get { return L10n.tr(t1,"Please add a comment")}}
    //Others
    
    
    
    class MyCommunity{
        static var myCommunity: String { get { return L10n.tr(t1, "My Community") } }
        static func getCommunityItemText(item: MyCommunityItems)->String{
            return L10n.tr(t1, item.rawValue)
        }
        class Polls {
            static func getSectionHeader(section: Int)->String{
                return L10n.tr(t1, section == 0 ? "Current Polls" : "Previous Polls")
            }
            static func pollClosesOn(dateStr: String) -> String {
                return L10n.tr(t1,"Poll closes on", dateStr)
            }
            static func pollClosedOn(dateStr: String) -> String {
                return L10n.tr(t1,"Poll closed on", dateStr)
            }
        }
        class CoCreate {
            static func getSectionHeader(section: Int)->String{
                switch section {
                case 0:
                    return L10n.tr(t1, "Current Surveys" )
                case 1:
                    return L10n.tr(t1, "Current Polls")
                default:
                    return L10n.tr(t1, "Collaborate with us")
                }
            }
            static var viewAllSurveys: String { get { return L10n.tr(t1, "View All Surveys") } }
            static var viewAllPolls: String { get { return L10n.tr(t1, "View All Polls") } }
        }
        class Events{
            static var eventAddedSuccessfully: String { get { return L10n.tr(t1, "Event is added successfully") } }
            static var permessionDenied: String { get { return L10n.tr(t1, "To add an event to your device calendar you have to grant permission.") } }
        }
    }
    
    static var additionalComments: String { get { return L10n.tr(t1,"Additional comments") }}
    static var errorMessage: String { get { return L10n.tr(t1,"error_message") }}
    
    static var pleaseSelectFeedbackEmoticon: String { get { return L10n.tr(t1,"Please select an emotion") }}
    
    
    static var locationPermissionNeeded: String { get { return L10n.tr(t1,"Location permission needed") }}
    static var locationPermissionRistricted: String { get { return L10n.tr(t1,"Location permission needed. It seems that you have ristricted access.") }}

    static var selectLocationTitle: String { get { return L10n.tr(t1,"Select Location") }}
    
    static var serviceNotAvailableForLocation: String { get { return L10n.tr(t1,"serviceNotAvailableForLocation") }}

    static var cancel: String { get { return L10n.tr(t1,"Cancel") }}

    class MyMessages{
        static func getSectionHeader(section: Int)->String{
            switch section {
            case 0:
                return L10n.tr(t1, "Today" )
            default:
                return L10n.tr(t1, "Yesterday")
            }
        }
        static var deleteMessageBody: String { get { return L10n.tr(t1,"confirm delete message body") }}
        static var archiveMessageBody: String { get { return L10n.tr(t1,"confirm archive message body") }}
        static var deleteMessageTitle: String { get { return L10n.tr(t1,"Confirm Delete") }}
        static var archiveMessageTitle: String { get { return L10n.tr(t1,"Confirm Archive") }}
        static var title: String { get { return L10n.tr(t1,"My Messages") }}
        static var selectOne: String { get { return L10n.tr(t1,"At least one message should be selected") }}
        
        static var sortBy: String { get { return L10n.tr(t1,"sortBy") }}
        static var starred: String { get { return L10n.tr(t1,"starred") }}
        static var all: String { get { return L10n.tr(t1,"All") }}
        static var desc: String { get { return L10n.tr(t1,"Date Desc") }}
        static var asc: String { get { return L10n.tr(t1,"Date Asc") }}
        static var applySort: String { get { return L10n.tr(t1,"Apply Sort") }}
        static var ref: String { get { return L10n.tr(t1,"Ref:") }}
    }
    
    static var thankYou: String { get { return L10n.tr(t1,"Thank You") }}
    static var messageRecievedSummary: String { get { return L10n.tr(t1,"messageRecievedSummary") }}
    static var messageRecievedDetails: String { get { return L10n.tr(t1,"messageRecievedDetails") }}
    static var goToMyComunity: String { get { return L10n.tr(t1,"Go to My Comunity") }}
    static var governmetEntities: String { get { return L10n.tr(t1,"Governmet Entities") }}
    
    
    class GovernmentEntities{
        static var informationTitle: String { get { return L10n.tr(t1,"Information_title") }}
        static var servicesTitle: String { get { return L10n.tr(t1,"services_title") }}
        
        static var officeHours: String { get { return L10n.tr(t1,"Office hours") }}
        static var publicHours: String { get { return L10n.tr(t1,"Public Service hours") }}

        static var entityDetails: String { get { return L10n.tr(t1,"Entity Details") }}


    }

    class Preferences{
        static var settingsTitle: String { get { return L10n.tr(t1,"Settings") }}
        static var profile: String { get { return L10n.tr(t1,"profile") }}
        static var language: String { get { return L10n.tr(t1,"language") }}
        static var notifications: String { get { return L10n.tr(t1,"notifications") }}
        static var mediaSettings: String { get { return L10n.tr(t1,"mediaSettings") }}
        static var enableLocationServices: String { get { return L10n.tr(t1,"Enable Location Services") }}
        static var syncCalendar: String { get { return L10n.tr(t1,"sync Calendar") }}
        static var about: String { get { return L10n.tr(t1,"About") }}
        static var tellAFriend: String { get { return L10n.tr(t1,"tell a Friend") }}
        static var appFeatures: String { get { return L10n.tr(t1,"App Features") }}
        static var accessibility: String { get { return L10n.tr(t1,"Accessibility") }}
        
        static var arabic: String { get { return L10n.tr(t1,"arabic") }}
        
        static var english: String { get { return L10n.tr(t1,"english") }}
        static var enableNotification: String { get { return L10n.tr(t1,"Enable_Push_Notification") }}
        static var setReminder: String { get { return L10n.tr(t1,"Set_Reminder") }}
        static var note: String { get { return L10n.tr(t1,"note:") }}
        static var stillReceiveCrucialNotif: String { get { return L10n.tr(t1,"You_will_still_receive_notifications") }}

    }
    class Profile{
        static var profileTitle: String { get { return L10n.tr(t1,"profile_title") }}
    }
    
    class MyDocsStrings{
        static var fromDateBeforeTo: String { get { return L10n.tr(t1,"From_date_must_be_before_To_date") }}
        static var myDocuments: String { get { return L10n.tr(t1,"myDocuments") }}
        static var monthsRange: String { get { return L10n.tr(t1,"Range_must_be") }}
        static var expiresOn: String { get { return L10n.tr(t1,"ExpiresOn") }}
        static var didYouMean: String { get { return L10n.tr(t1,"did_You_Mean") }}

    }
    class MySubscriptionsStrings{
        
        static var mySubscriptions: String { get { return L10n.tr(t1,"My subscriptions") }}
    }
    
    //Adge
    static var viewDetails: String { get { return L10n.tr(t1,"View details") }}
    static var applyFilter: String { get { return L10n.tr(t1,"Apply filters") }}
    static var search: String { get { return L10n.tr(t1,"Search") }}
    static var filter: String { get { return L10n.tr(t1,"Filter") }}
    static var sortByDate: String { get { return L10n.tr(t1,"sort by date") }}
    static var openInMap: String { get { return L10n.tr(t1,"Open in map") }}
    static var buyTickets: String { get { return L10n.tr(t1,"buy tickets") }}
    
    //Media centre
    static var viewAll: String { get { return L10n.tr(t1,"View all") }}
    static var events: String { get { return L10n.tr(t1,"Events") }}
    
    //Menu
    static var quickMenu: String { get { return L10n.tr(t1,"quick menu") }}
    static var aboutTamm: String { get { return L10n.tr(t1,"about TAMM") }}
    static var select_a_profile: String { get { return L10n.tr(t1,"select a profile") }}
    static var arabic: String { get { return L10n.tr(t1,"Arabic") }}
    //My community and events
    static var factsAndFigures: String { get { return L10n.tr(t1,"Facts and figures") }}
    static var communityEvents: String { get { return L10n.tr(t1,"Community events") }}
    static var initiatives: String { get { return L10n.tr(t1,"Initiatives") }}
    static var cocreate: String { get { return L10n.tr(t1,"Co-create") }}
    static var publicHolidays: String { get { return L10n.tr(t1,"Public holidays") }}
    static var addToCalender: String { get { return L10n.tr(t1,"Add to calender") }}
    //News listing
    static var news: String { get { return L10n.tr(t1,"News") }}
    //Service details changes
    static var apply: String { get { return L10n.tr(t1,"Apply") }}
    static var readMore: String { get { return L10n.tr(t1,"Read more") }}
    static var viewServicesConditions: String { get { return L10n.tr(t1,"View service conditions") }}
    static var wasThisInformationHelpful: String { get { return L10n.tr(t1,"Was this information helpful?") }}
    static var yesThankYou: String { get { return L10n.tr(t1,"Yes: Thank you for browsing TAMM, visit us again") }}
    static var noThankYou: String { get { return L10n.tr(t1,"No: Thank you for your participation,") }}
    static var weWillWorkOn: String { get { return L10n.tr(t1,"we will work on improving our services.") }}
    static var submit: String { get { return L10n.tr(t1,"Submit") }}
    static var getDirection: String { get { return L10n.tr(t1,"Get directions") }}
    static var viewMap: String { get { return L10n.tr(t1,"View map") }}
    ////////////
    static var mediaCenter: String { get { return L10n.tr(t1,"Media Center") }}
    static var social: String { get { return L10n.tr(t1,"Social") }}
    static var pressRelease: String { get { return L10n.tr(t1,"Press Release") }}
    static var downloadSuccessflyCompleted: String { get { return L10n.tr(t1,"Download Successfuly Completed") }}
    static var downloadFailed: String { get { return L10n.tr(t1,"Download Failed") }}
    static var upcoming: String { get { return L10n.tr(t1,"UpComing") }}
    static var earlier: String { get { return L10n.tr(t1,"Earlier") }}
    static var event: String { get { return L10n.tr(t1,"Event") }}
    static var multipleVenues: String { get { return L10n.tr(t1,"Multiple venues") }}
    static var governmentEntities: String { get { return L10n.tr(t1,"Government Entities") }}
    
    static var searchForGE: String { get { return L10n.tr(t1,"Search for a government entity" ) }}
    static var requiredDocs: String { get { return L10n.tr(t1,"Required Documents" ) }}
    //static var serviceCode: String { get { return L10n.tr(t1,"Service Code") }}
    static var fees: String { get { return L10n.tr(t1,"Fees") }}
    static var readLess: String { get { return L10n.tr(t1,"Read Less") }}
    static var whereToFindThisService: String { get { return L10n.tr(t1,"Where to find this service?") }}
    
    static var physicalChannel: String { get { return L10n.tr(t1,"Physical Channel") }}
    static var digitalChannel: String { get { return L10n.tr(t1,"Digital Channel") }}
    static var entityHQ: String { get { return L10n.tr(t1,"ENTITY HQ") }}
    static var otherLocations: String { get { return L10n.tr(t1,"OTHER LOCATIONS") }}
    static var kiosk: String { get { return L10n.tr(t1,"KIOSK") }}
    static var online: String { get { return L10n.tr(t1,"ONLINE") }}
    static var mobileApps: String { get { return L10n.tr(t1,"Mobile Apps") }}
    static var phones: String { get { return L10n.tr(t1,"Phones") }}
    static var mainService: String { get { return L10n.tr(t1,"Main Service") }}
    

    static var relatedJourneys: String { get { return L10n.tr(t1,"Related Journeys") }}
    static var serviceConditions: String { get { return L10n.tr(t1,"Service Conditions") }}
    static var relatedServices: String { get { return L10n.tr(t1,"Related Services") }}
    static var relatedSubTopics: String { get { return L10n.tr(t1,"Related Sub Topics") }}
    static var warning: String { get { return L10n.tr(t1,"Warning") }}
    static var youAreAboutToLeaveTAMM: String { get { return L10n.tr(t1,"You're about to leave TAMM") }}
    //static var phones: String { get { return L10n.tr(t1,"Phones") }}

    static var email: String { get { return L10n.tr(t1,"Email") }}
    static var updateProfile: String { get { return L10n.tr(t1,"Update Profile") }}
    static var youWillBeRedirectedToSmartPassPortal: String { get { return L10n.tr(t1,"You will be redirected to the SmartPass Portal" ) }}
    static var acceptTermsAndConditions: String { get { return L10n.tr(t1,"AcceptTermsandConditions." ) }}
    static var selectTypeOfReceiveingAnswer: String { get { return L10n.tr(t1,"Select type of receiving answer" ) }}
    static var mediaItemIsNotSupported: String { get { return L10n.tr(t1,"Media item is not supported") }}
    static var titleMsgIsReq: String { get { return L10n.tr(t1,"Title message is required" ) }}
    static var txtmshMore60Char: String { get { return L10n.tr(t1,"Text length is more than 60 character") }}
    static var selectValidLocation: String { get { return L10n.tr(t1,"Select a valid location") }}
    static var myEvents: String { get { return L10n.tr(t1,"Myevents") }}
    static var noPhoneReg : String { get { return L10n.tr(t1,"NoPhoneRegistered") }}
    static var searchForServices: String { get { return L10n.tr(t1,"SearchforServices") }}

    static var ok: String { get { return L10n.tr(t1,"ok") }}
    static var reminder: String { get { return L10n.tr(t1,"Reminder") }}

    
     static var tendaysbefore: String { get { return L10n.tr(t1,"10 days before") }}
    static var sevendaysbefore: String { get { return L10n.tr(t1,"7 days before") }}
    static var onedaybefore: String { get { return L10n.tr(t1,"24 hours before") }}

    
    class CameraStrings {
        static var cameraPermissionDenied: String { get { return L10n.tr(t1,"Permission to use camera denied.") }}
        static var storgePermissionDenied: String { get { return L10n.tr(t1,"Permission to use file storage was denied.") }}
    }
    static var micPermissionDenied: String { get { return L10n.tr(t1,"mic permission") }}
    static var gallareyPermissionDenied: String { get { return L10n.tr(t1,"gallarey permission") }}
    static var speechPermissionDenied: String { get { return L10n.tr(t1,"speech permission") }}
    static var recognizingVoice: String { get { return L10n.tr(t1,"recognizing voice") }}
    
    
    static var privacy: String { get { return L10n.tr(t1,"Privacy Policy") } }
    static var terms: String { get { return L10n.tr(t1,"Terms of Use") } }
    static var manageMedia: String { get { return L10n.tr(t1,"ManageMedia") } }
    
    static var youHaveChosenToUpload: String { get { return L10n.tr(t1,"You have chosen to upload media over wifi only") } }
    
    static var uploadMediaOverNetwork: String { get { return L10n.tr(t1,"upload media over Network") } }
    
    static var saveToDrafts: String { get { return L10n.tr(t1,"Save To Drafts") } }
    
    static func refrenceNumber(refNum: String) -> String {
        return L10n.tr(t1,"Ref Number", refNum)
    }
    static var selectMediaFirst: String { get { return L10n.tr(t1,"Please Select Media First") } }
    static var pleaseWriteComment: String { get { return L10n.tr(t1,"Please write a comment") } }
    static var emailIsNotValid: String { get { return L10n.tr(t1,"Email is not valid") } }
    static var subjectFieldIsNotEmpty: String { get { return L10n.tr(t1,"Subject field is not empty") } }
    static var chooseDocument: String { get { return L10n.tr(t1,"Choose a document") } }
    static var chooseSoundFile: String { get { return L10n.tr(t1,"Choose a sound file") } }

    static var letterCount: String { get { return L10n.tr(t1,"letter count should be between 1 and 16") } }
    
    static var megaByte: String { get { return L10n.tr(t1,"MB") } }
    
    static var payments: String { get { return L10n.tr(t1,"Payments") } }
    
    static var amountDesc: String { get { return L10n.tr(t1,"Amount Desc") } }
    static var amountAsc: String { get { return L10n.tr(t1,"Amount Asc") } }
    static var confirm: String { get { return L10n.tr(t1, "Confirm") } }
    
    static var youDontHaveAnyPayment: String { get { return L10n.tr(t1,"You don’t have any Payments at the moment.") } }
    
    static var offlineMapsAreUsedTo: String { get { return L10n.tr(t1, "Offline maps are used to point" ) } }
    
    static var mapSize: String { get { return L10n.tr(t1, "252 ") } }
    static var doYouWish: String { get { return L10n.tr(t1, "MB map for offline use") } }
    static var ifNo: String { get { return L10n.tr(t1, "if No ") } }
    
    
    static var areYouSureYouWantToDownload: String { get { return L10n.tr(t1, "Are you sure you want to download") } }
    
    static var address: String { get { return L10n.tr(t1, "Address") } }
    static var offlineAddress: String { get { return L10n.tr(t1, "Offline Address") } }
    
    static var downloadOfflineMap: String { get { return L10n.tr(t1, "Download Offline Map") } }
    
    static func getCaseTitle(type: CaseDetailEnum)->String{
        return L10n.tr(t1,type.toString())
    }
    
    static var noMapAvailable: String { get { return L10n.tr(t1, "No Map Available") } }
    
    static func fullOfflineAddress(point1: String, point2: String)->String{
       return L10n.tr(t1,"Offline Address you have Selected", point1,point2)
    }
    
     static var dueDateDesc: String { get { return L10n.tr(t1, "Duedate Desc") } }
     static var dueDateAsc: String { get { return L10n.tr(t1, "Duedate Asc") } }
    static var downloadCompleted: String { get { return L10n.tr(t1, "Download Completed") } }

    
    class Initiatives {
        static var searchPlaceHolder: String { get { return L10n.tr(t1,"Search Initiatives") } }
    }
    static func getGlobalSearchResultHeader(num: String, total: String) -> String {
        return L10n.tr(t1,"GlobalSearchResultHeader", num, total)
    }
}

// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
