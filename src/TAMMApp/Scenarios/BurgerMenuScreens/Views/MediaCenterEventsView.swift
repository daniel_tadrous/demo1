//
//  MediaCenterEventsView.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class MediaCenterEventsView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mediaImage: UIImageView!
    @IBOutlet weak var endDay: UILabel!
    @IBOutlet weak var endMonth: LocalizedLabel!
    @IBOutlet weak var startDay: UILabel!
    @IBOutlet weak var startMonth: LocalizedLabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var typeButton: UIView!
    @IBOutlet weak var venuesLabel: LocalizedLabel!
    @IBOutlet weak var venuesView: UIView!
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    @IBOutlet weak var holdngView: UIView!
    @IBOutlet weak var endDateView: UIView!
    
    let width:CGFloat = 0
    let height:CGFloat = 0
    let position = 1
    
    
    @IBAction func typeAction(_ sender: LocalizedButton) {
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let viewFileName: String = "MediaCenterEventsView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(contentView)
        self.frame = contentView.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        if L10n.Lang == "ar" {
            startDay.font = UIFont(  name: "Swissra-Bold",  size:  16)!
            endDay.font = UIFont(  name: "Swissra-Bold",  size:  16)!
        }
    }
    
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}

