//
//  MessagesViewController.swift
//  TAMMApp
//
//  Created by Daniel on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import UICheckbox_Swift
import RxSwift


protocol MessagesDelegate {
    func openCaseDetails(id:Int)
}
class MessagesViewController: TammViewController, MessagesStoryboardLodable{
    
    let sortAsc = "asc"
    let sortDesc = "desc"
    
    @IBOutlet weak var firstHeaderView: UIView!
    var delegate: MessagesDelegate?
    @IBOutlet weak var searchFilterSortView: SearchFilterSort!
    @IBOutlet weak var searchFilterSelectView: UIView!
    @IBOutlet weak var secondHeaderView: UIStackView!
    @IBOutlet weak var allCheckBox: UICheckbox!
    @IBOutlet weak var tableView: UITableView!
    
    var disposeBag: DisposeBag = DisposeBag()
    let showMoreCellIdentifier:String = "showmoreCell"
    var response:MsgResponse?
    var isFromOfflineScreen:Bool = false
    var viewModel:MessagesViewModel!
    //weak var delegate: AspectsOfLifeTableViewDelegate?
    //public  var delegate:TalkToUsDraftsViewDelegate?
    fileprivate let defaultReuseIdentifier = "Messages"
    fileprivate let defaultReuseIdentifierEdit = "Messages_Edit"
    public let defaultCellHeight = 156
    let inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let headerViewIdentifier = "message_header"
    private var activeMsgsList:[MessageModel] = [MessageModel]()
    var sortViewOverLay:MsgSortOverLay?
    var filterViewOverLay:MessageFilterOverLay?
    private var messages:[MessageModel]{
        get{
            return self.viewModel.viewData.value ?? []
        }
    }
    
    
    
    private var messagesInSection:[Int:[MessageModel]]{
        get{
            var messagesInSec = [Int:[MessageModel]]()
            messagesInSec[0] = []
            messagesInSec[1] = []
            for mess in self.messages{
                
                if Calendar.current.isDateInToday( (mess.createDate?.getDate(format: inputFormat))!){
                    messagesInSec[0]?.append(mess)
                }else{
                    messagesInSec[1]?.append(mess)
                }
            }
            return messagesInSec
        }
    }
    private var showExtension:Bool{
        get{
            return self.firstHeaderView.isHidden
        }
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ErrorView.show = !isFromOfflineScreen
        self.tableView?.allowsSelection = true
        self.tableView?.tableFooterView = UIView()
        let bundle = Bundle(for: type(of: self))
        self.tableView.register(UINib(nibName: "CoCreateSectionHeaderView", bundle: bundle), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        setupHooks()
        self.setNavigationBar(title: L10n.MyMessages.title, willSetSearchButton: true)
        addTapGesturseOnSearchView()
        searchFilterSortView.filterAction = {
            if self.viewModel.msgType == MessageType.closed {
                 Toast.init(message: L10n.commingSoon).Show()
            }
            
            if self.response?.options != nil && self.response?.options?.count != 0 {
                
                self.filterViewOverLay = MessageFilterOverLay(frame: UIScreen.main.bounds, availableStatuses: (self.response?.options)!,selectedOption:self.viewModel.filteredByText.value)
                  //  MsgFilter(frame: UIScreen.main.bounds, availableStatuses: (self.response?.options)!,
                self.filterViewOverLay?.delegate = self
                UIApplication.shared.keyWindow!.addSubview(self.filterViewOverLay!)
                UIApplication.shared.keyWindow!.bringSubview(toFront: self.filterViewOverLay!)
            }
        }
        
        searchFilterSortView.sortAction = {
             if self.response?.hasStarred != nil {
                self.sortViewOverLay = MsgSortOverLay(frame: UIScreen.main.bounds , type: MsgFilterType.sort, hasStarred : (self.response?.hasStarred!)!,selectedOption: self.viewModel.sortByText.value)
            //  MsgFilter(frame: UIScreen.main.bounds, availableStatuses: (self.response?.options)!,
            self.sortViewOverLay?.delegate = self
            UIApplication.shared.keyWindow!.addSubview(self.sortViewOverLay!)
            UIApplication.shared.keyWindow!.bringSubview(toFront: self.sortViewOverLay!)
            }

        }
        self.tableView?.register(UINib(nibName: "ShowMoreCell", bundle: nil), forCellReuseIdentifier: showMoreCellIdentifier)
    }
    
    fileprivate func addTapGesturseOnSearchView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(searchViewClicked))
        searchFilterSortView.searchView.addGestureRecognizer(tap)
    }
    
    @objc func searchViewClicked() {
       
        let searchViewOverLay = SearchWithoutTrending(frame: UIScreen.main.bounds, text: self.searchFilterSortView.searchView.searchField.text!, currentSearchModel: self.viewModel.searchModel)
        searchViewOverLay.delegate = self
        UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay)
        print("Search View Clicked")
    }
    
    
    var isOneItemSelected:Bool{
        let items = self.viewModel.viewData.value?.filter({ (mess) -> Bool in
            return mess.isSelected
        })
        if items?.count ?? 0 > 0{
            return true
        }
        return false
    }
    
    @IBAction func selectBtnClickHandler(_ sender: LocalizedButton) {
        toggleHeaders()
    }
    
    @IBAction func filterBtnClickHandler(_ sender: UIButton) {
        
    }
    
    @IBAction func deleteClickHandler(_ sender: LocalizedButton) {
        if !isOneItemSelected{
            Toast.init(message: L10n.MyMessages.selectOne).Show()
            return
        }
        let confirmDeleteView = ConfirmationView(frame: UIScreen.main.bounds,title:L10n.MyMessages.deleteMessageTitle,message:L10n.MyMessages.deleteMessageBody,okClickHandler:{
            let items = self.viewModel.viewData.value?.filter({ (mess) -> Bool in
                return mess.isSelected
            })
            
            self.selectUnselectAll(isSelected: false)
            for item in items!{
                item.isDeleted = true
            }
            self.viewModel.messages.value = items
            self.toggleHeaders()
        })
        UIApplication.shared.keyWindow!.addSubview(confirmDeleteView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: confirmDeleteView)
        
    }
    @IBAction func archiveClickHandler(_ sender: LocalizedButton) {
        if !isOneItemSelected{
            Toast.init(message: L10n.MyMessages.selectOne).Show()
            return
        }
        let confirmDeleteView = ConfirmationView(frame: UIScreen.main.bounds,title:L10n.MyMessages.archiveMessageTitle,message:L10n.MyMessages.archiveMessageBody,okClickHandler:{
            let items = self.viewModel.viewData.value?.filter({ (mess) -> Bool in
                return mess.isSelected
            })
            self.selectUnselectAll(isSelected: false)
            for item in items!{
                item.isArchived = true
            }
            self.viewModel.messages.value = items
            
            self.toggleHeaders()
        })
        UIApplication.shared.keyWindow!.addSubview(confirmDeleteView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: confirmDeleteView)
    }

    @IBAction func tagClickHandler(_ sender: LocalizedButton) {
        if !isOneItemSelected{
            Toast.init(message: L10n.MyMessages.selectOne).Show()
            return
        }
        let confirmDeleteView = AddTagView(frame: UIScreen.main.bounds,okClickHandler:{
            (tag) in
            let items = self.viewModel.viewData.value?.filter({ (mess) -> Bool in
                return mess.isSelected
            })
            self.selectUnselectAll(isSelected: false)
            for item in items!{
                item.tag = tag
            }
            self.viewModel.messages.value = items
            
            self.toggleHeaders()
        })
        UIApplication.shared.keyWindow!.addSubview(confirmDeleteView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: confirmDeleteView)
    }
    
    @IBAction func checkboxToggleHandler(_ sender: UIButton) {
        allCheckBox.isSelected = !allCheckBox.isSelected
        self.selectUnselectAll(isSelected: allCheckBox.isSelected)
        self.tableView?.reloadData()
    }
    
    private func toggleHeaders(){
        secondHeaderView.isHidden = !secondHeaderView.isHidden
        firstHeaderView.isHidden = !firstHeaderView.isHidden
        self.tableView.reloadData()
    }
    
    
    func setupHooks(){
        //new
        self.viewModel.apiResponse.asObservable().subscribe(onNext: { response in
            if response != nil {
                
                self.activeMsgsList += (response?.messages)!
                self.response = response
                self.tableView.reloadData()
            }
        }).disposed(by: disposeBag)

    }
}

extension MessagesViewController: UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row <= activeMsgsList.count-1 {
            return initiateDefaultCell(indexPath: indexPath)
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: showMoreCellIdentifier) as! ShowMoreCell
            
            cell.showMoreBtn.addTarget(self, action: #selector(showMoreNewsPressed(_:)), for: .touchUpInside)
            
            return cell
        }
        
    }
    
    fileprivate func initiateDefaultCell(indexPath:IndexPath)->MessageTableCell{
        let item = activeMsgsList[indexPath.row]
        var cell: MessageTableCell
        if item.isOpened && !showExtension{
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifierEdit, for: indexPath) as! MessageTableCell
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(openCloseGesture(_:)))
            
            swipeRight.direction = L10n.Lang == "en" ? .right : .left
            cell.addGestureRecognizer(swipeRight)
            
            cell.onDeleteClick = {
                let confirmDeleteView = ConfirmationView(frame: UIScreen.main.bounds,title:L10n.MyMessages.deleteMessageTitle,message:L10n.MyMessages.deleteMessageBody,okClickHandler:{
                    item.isOpened = false
                    item.isDeleted = true
                    self.viewModel.messages.value = [item]
                    self.activeMsgsList.remove(at: indexPath.row)
                    self.tableView.reloadData()
                })
                UIApplication.shared.keyWindow!.addSubview(confirmDeleteView)
                UIApplication.shared.keyWindow!.bringSubview(toFront: confirmDeleteView)
            }
            cell.onArchiveClick = {
                let confirmDeleteView = ConfirmationView(frame: UIScreen.main.bounds,title:L10n.MyMessages.archiveMessageTitle,message:L10n.MyMessages.archiveMessageBody,okClickHandler:{
                    item.isOpened = false
                    item.isArchived = true
                    self.viewModel.messages.value = [item]
                })
                UIApplication.shared.keyWindow!.addSubview(confirmDeleteView)
                UIApplication.shared.keyWindow!.bringSubview(toFront: confirmDeleteView)
            }
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: defaultReuseIdentifier, for: indexPath) as! MessageTableCell
            if showExtension{
                cell.messageView.extensionView.isHidden = false
                cell.messageView.checkbox.isSelected = item.isSelected
                
                cell.onSelectClick = {
                    (checked) in
                    item.isSelected = checked
                    self.setAllCheckBoxSelection()
                }
            }else{
                cell.messageView.extensionView.isHidden = true
                
                let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(openCloseGesture(_:)))
                swipeLeft.direction =  L10n.Lang == "en" ? .left : .right
                cell.addGestureRecognizer(swipeLeft)
                
            }
        }
        //status changed to object
        cell.messageView.titleLabel.text = item.status?.name
        cell.messageView.descLabel.text = item.statusMessage
        cell.messageView.receivedByValLabel.text = item.receivedBy
        cell.messageView.refNumLabel.text = "\(L10n.MyMessages.ref)\(item.caseId ?? "")"
        cell.messageView.messageTypeValLabel.text = item.type?.name ?? ""
        cell.indexPath = indexPath
        setStarStatus(cell: cell, isStarred: item.isStarred!, indexPath: indexPath)
        setTag(cell: cell, tag: item.tag)
        cell.messageView.starClickHandlerClosure = {
            item.isStarred = !item.isStarred!
            self.viewModel.messages.value = [item]
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
        let calendar = NSCalendar.current
        var timeStr = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: L10n.Lang)
        
        let date = item.createDate?.getDate(format: inputFormat)
        if calendar.isDateInToday(date!){
            dateFormatterGet.dateFormat = "HH:mm a"
            dateFormatterGet.timeStyle = .short
            timeStr = dateFormatterGet.string(from: date!)
        }else{
            dateFormatterGet.dateFormat = "MMM d"
            timeStr = dateFormatterGet.string(from: date!)
        }
        cell.messageView.timeLabel.text = timeStr
        
        return cell
    }
    private func setStarStatus(cell:MessageTableCell,isStarred:Bool, indexPath:IndexPath){
        if isStarred{
            cell.messageView.starLabel.text = "\u{e01e}"
            cell.messageView.starLabel.textColor = UIColor.orange
        }else{
            cell.messageView.starLabel.text = "\\"
            cell.messageView.starLabel.textColor = UIColor.darkBlueGrey
        }
      
    }
    private func setTag(cell:MessageTableCell,tag:String?){
        if tag != nil && !(tag?.isEmpty)!{
            cell.messageView.tagViewHolder.isHidden = false
            cell.messageView.tagLabel.text = tag
        }else{
            cell.messageView.tagViewHolder.isHidden = true
        }
    }
    private func selectUnselectAll(isSelected:Bool){
        for mess in viewModel.viewData.value!{
            mess.isSelected = isSelected
        }
    }
    private func setAllCheckBoxSelection(){
        
        for mess in viewModel.viewData.value!{
            if !mess.isSelected{
                if self.allCheckBox.isSelected{
                    self.allCheckBox.isSelected = false
                }
                return
            }
        }
        if !self.allCheckBox.isSelected{
            self.allCheckBox.isSelected = true
        }
    }
    @objc func openCloseGesture(_ sender: Any){
        let indexPath = ((sender as! UISwipeGestureRecognizer).view as! MessageTableCell).indexPath
        self.activeMsgsList[(indexPath?.row)!].isOpened = !self.activeMsgsList[(indexPath?.row)!].isOpened
        var animation:UITableViewRowAnimation
        if L10n.Lang == "en"{
            animation = self.activeMsgsList[(indexPath?.row)!].isOpened ? .left : .right
        }else{
            animation = self.activeMsgsList[(indexPath?.row)!].isOpened ? .right : .left
        }
        self.tableView.reloadRows(at: [indexPath!], with: animation)
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != activeMsgsList.count {
            let item = activeMsgsList[indexPath.row]
            self.delegate?.openCaseDetails(id:item.id!)
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       
        let  rowHeight = UITableViewAutomaticDimension
        return rowHeight
    }
    
    fileprivate func getNumberOfRows()->Int {
        if activeMsgsList.count == 0 {
            return 0
        }else if (viewModel?.isLastMsgPage())!{
            return activeMsgsList.count
        }
        return activeMsgsList.count+1
    }

    @objc func showMoreNewsPressed(_ sender:UIButton){
        
        viewModel?.page.value = viewModel.page.value! + 1
    }
    
    
}

extension MessagesViewController : SearchWithoutTrendingDelegate{
    
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchFilterSortView.searchView.searchField.text = message.name
        self.viewModel?.searchedText.value = message.name
        self.viewModel?.filteredByText.value = nil
        self.activeMsgsList.removeAll()
        self.viewModel.page.value = 1
    }
    func backPressed(){
        self.searchFilterSortView.searchView.searchField.text = ""
        self.searchFilterSortView.searchView.searchField.placeholder = L10n.search
    }
}


extension MessagesViewController :MsgSortDelegate {
  
    
    func applySortClicked(applyFilterOn:String) {
        self.activeMsgsList.removeAll()
        self.viewModel.sortByText.value = applyFilterOn
        self.viewModel.page.value = 1
    }
    
}



extension MessagesViewController : MessageFilterDelegate {
    
    func applyFilterClicked(applyFilterOn: String) {
        
        self.activeMsgsList.removeAll()
        self.viewModel.filteredByText.value = applyFilterOn
        self.viewModel.page.value = 1
    }
    
}

