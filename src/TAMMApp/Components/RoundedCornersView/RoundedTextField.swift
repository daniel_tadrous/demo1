//
//  RoundedTextField.swift
//  TAMMApp
//
//  Created by Daniel on 7/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class RoundedTextField: LocalizedTextField{
    
    @IBInspectable
    var Radius: CGFloat = 0{
        didSet{
            if self.AllRounded{
                self.addRoundAllCorners(radious: self.Radius)
            }else{
                self.addRoundCorners(radious: self.Radius)
            }
        }
    }
    
    @IBInspectable
    var AllRounded: Bool = false{
        didSet{
            if self.AllRounded{
                self.addRoundAllCorners(radious: self.Radius)
            }else{
                self.addRoundCorners(radious: self.Radius)
            }
        }
    }
    
    @IBInspectable
    var HasBorder: Bool = false{
        didSet{
            if self.HasBorder{
                self.layer.borderWidth = 1
            }
        }
    }
    
    @IBInspectable
    var BorderColor: UIColor = UIColor.clear{
        didSet{
            if self.HasBorder{
                self.layer.borderColor = self.BorderColor.cgColor
            }
        }
    }
}
