//
//  PickerView.swift
//  TAMMApp
//
//  Created by kerolos on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift


protocol PickerItemType {
    var id: AnyHashable {get}
    var displayName: String {get}
}

protocol PickerViewDelegate: class{
    func itemSelected(item: PickerItemType, pickerView: PickerView)
    func clickOutsidePicker()
}
class PickerView: UIPickerView {

    var items: [PickerItemType] = []
    
    weak var viewDelegate: PickerViewDelegate?
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    
    private func commonInit(){
        setupBinding()
    }
//
//    func setConstraints(view: UIView, childView: UIView){
//        let newView = childView
//        //        newView.translatesAutoresizingMaskIntoConstraints = false
//        let centerX = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
//        let centerY = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
//        let width = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(self.pickerWidth))
//        let height = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(self.pickerHeight))
//
//        view.addConstraints([centerX, centerY, width, height])
//    }
    
    private func setupBinding(){
        self.delegate = self as UIPickerViewDelegate
        self.dataSource = self as UIPickerViewDataSource
    }
}

extension PickerView : UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        viewDelegate?.itemSelected(item: items[row], pickerView: self)
    }
}

extension PickerView : UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return items[row].displayName
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = LocalizedLabel()
        label.font = UIFont(name: "Roboto-Regular", size: 16)
        label.originalFont = UIFont(name: "Roboto-Regular", size: 16)
        label.applyFontScalling = true
        label.RTLFont = "Swissra-Normal"
        label.textColor = UIColor.darkBlueGrey
        label.text = items[row].displayName
        label.textAlignment = .center
        return label
    }
    
}

