//
//  EventCell.swift
//  TAMMApp
//
//  Created by Daniel on 6/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var calenderIconLabel: LocalizedLabel?
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var imageAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titleLb: LocalizedLabel!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryLb: LocalizedLabel!
    @IBOutlet weak var categoryBtn: LocalizedButton!
    
    @IBOutlet weak var dateLb: LocalizedLabel!
    
    @IBOutlet weak var descrLb: LocalizedLabel?
    
    @IBOutlet weak var shareIconLb: UILabel?
    @IBAction func OnShareClickHandler(_ sender: UIButton) {
        shareClickHandlerClosure?()
    }
    @IBOutlet weak var addToMyEventsBtn: LocalizedButton?
    
    @IBOutlet weak var findMoreBtn: LocalizedButton?
    
    @IBAction func AddToMyEventsClickHandler(_ sender: LocalizedButton) {
        addToMyEventsClickHandlerClosure?()
    }
    
    @IBAction func FindOutMoreClickHandler(_ sender: LocalizedButton) {
        findOutMoreClickHandlerClosure?()
    }
    
    var shareClickHandlerClosure:(()->Void)?
    var addToMyEventsClickHandlerClosure:(()->Void)?
    var findOutMoreClickHandlerClosure:(()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        commonInit()
        
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    func commonInit(){
        
        adjustColors()
        
        self.categoryBtn.addRoundAllCorners(radious: 5)
        self.categoryBtn.layer.borderColor = UIColor.turquoiseBlue.getAdjustedColor().cgColor
        self.shareIconLb?.text = "\u{e002}"
        self.addToMyEventsBtn?.layer.borderColor = UIColor.turquoiseBlue.getAdjustedColor().cgColor
        
        self.addToMyEventsBtn?.contentMode = .center
        self.addToMyEventsBtn?.titleLabel?.textAlignment = .center
        self.findMoreBtn?.titleLabel?.textAlignment = .center
        
        
        
        self.containerView.addRoundCorners(radious: 17)
        
        
        
        applyRoundCornersToBtns()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func expand() {
        self.bottomConstraint.priority = .defaultLow
    }
    
    func collapse() {
        self.bottomConstraint.priority = .defaultHigh
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        applyRoundCornersToBtns()
    }
    
    private func applyRoundCornersToBtns(){
        self.addToMyEventsBtn?.layer.cornerRadius = (self.addToMyEventsBtn?.frame.height)!/2
        self.findMoreBtn?.layer.cornerRadius = (self.findMoreBtn?.frame.height)!/2
    }
    
    @IBOutlet weak var shareEventLabel: LocalizedLabel?
    @IBOutlet weak var shareEventButton: LocalizedButton?
    func adjustColors(){
        self.contentView.backgroundColor =  #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9529411765, alpha: 1).getAdjustedColor()
        self.containerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        self.headerContainer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        self.categoryLb.textColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor()
        self.categoryBtn.setTitleColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1), for: .normal)
        self.categoryBtn.BorderColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        self.titleLb.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        self.calenderIconLabel?.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        
        self.descrLb?.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        self.shareIconLb?.textColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor()
        self.shareEventLabel?.textColor = #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor()
//        self.shareEventButton
        self.addToMyEventsBtn?.setTitleColor( #colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor() , for: .normal)
        self.addToMyEventsBtn?.BorderColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        self.addToMyEventsBtn?.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        
        self.findMoreBtn?.setTitleColor( #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        self.findMoreBtn?.backgroundColor = #colorLiteral(red: 1, green: 0.8666666667, blue: 0, alpha: 1).getAdjustedColor()
        
        
    }

}
