//
//  MyCommunityViewModel.swift
//  TAMMApp
//
//  Created by Daniel Tadrous on 6/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class MyCommunityViewModel{
    private var MyCommunitySections:[MyCommunitySection] = []
    init() {
        self.FillMyCommunitySections()
    }
    
//    func getMyCommunitySectionsCount() -> Int{
//        self.FillMyCommunitySections()
//        return self.MyCommunitySections.count;
//    }
    
    func getMyCommunitySectionIcon(index:Int) -> String{
        return self.MyCommunitySections[index].icon;
    }
    
    func getMyCommunitySectionIconColor(index:Int) -> String{
        return self.MyCommunitySections[index].iconHexColor;
    }
    
    func getMyCommunitySectionIconBackgroundColor(index:Int) -> String{
        return self.MyCommunitySections[index].iconHexBackgroundColor;
    }
    func getMyCommunitySectionName(index:Int) -> String{
        return self.MyCommunitySections[index].name;
    }
    
    func getMyCommunitySectionCount() -> Int{
        return self.MyCommunitySections.count
    }
    
    func getMyCommunitySectionType(index:Int) -> MyCommunityItems{
        return self.MyCommunitySections[index].type
    }
    
    private func FillMyCommunitySections(){
        self.MyCommunitySections.append(MyCommunitySection(type: .FactsAndFigures, icon: "!", iconHexColor: String.MyPayemntsIconColorString, iconHexBackgroundColor: String.MyPayemntsIconBackgroundColorString))
        
        self.MyCommunitySections.append(MyCommunitySection(type: .CommunityEvents, icon: ">", iconHexColor: String.MySubscriptionIconColorString, iconHexBackgroundColor: String.MySubscriptionIconBackgroundColorString))
        
        self.MyCommunitySections.append(MyCommunitySection(type: .Initiative, icon: "[", iconHexColor: String.MyEventsIconColorString, iconHexBackgroundColor: String.MyEventsIconBackgroundColorString))
        
       
        
//        self.MyCommunitySections.append(MyCommunitySection(type: .AboutAbudhabi, icon: "\u{e021}", iconHexColor: String.MySupportIconColorString, iconHexBackgroundColor: String.MySupportIconBackgroundColorString))
        self.MyCommunitySections.append(MyCommunitySection(type: .CoCreation, icon: "\u{e006}", iconHexColor: String.CoCreationColorIconString, iconHexBackgroundColor: String.CoCreationBackgroundColorIconString))
        self.MyCommunitySections.append(MyCommunitySection(type: .News, icon: "\u{e03e}", iconHexColor: String.MySupportIconColorString, iconHexBackgroundColor: String.MySupportIconBackgroundColorString))
    }
}

struct MyCommunitySection{
    var type:MyCommunityItems
    var icon:String;
    var iconHexColor:String
    var iconHexBackgroundColor:String
    var name:String;
    
    init(type:MyCommunityItems,
         icon:String,
         iconHexColor:String,
         iconHexBackgroundColor:String){
        self.type  = type
        self.icon = icon
        self.iconHexColor = iconHexColor
        self.iconHexBackgroundColor = iconHexBackgroundColor
        self.name = L10n.MyCommunity.getCommunityItemText(item: type)
    }
}



