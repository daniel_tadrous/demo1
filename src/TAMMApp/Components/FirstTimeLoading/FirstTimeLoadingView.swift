//
//  FirstTimeLoadingView.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit




class FirstTimeLoadingView: UIView {
    
    @IBOutlet var view: UIView!
    
    private var text:String!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var descriptionLabel: LocalizedLabel!
    
    var closingClosure: (()->Void)?
    
    func dismiss() {
        self.removeFromSuperview()
        closingClosure?()
    }

    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    init(frame: CGRect,text:String) {
        super.init(frame:frame)
        self.text = text
        commonInit()
        setupHooks()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("FirstTimeLoadingView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        
    }
    
    
    
    func setupHooks(){
        descriptionLabel.text = self.text
        setupColors()
    }
    
    func setupColors() {
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
        activityIndicator.color =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        descriptionLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
    }


}
