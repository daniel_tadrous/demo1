//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ProcessHeaderView : UIView{
    
    @IBOutlet weak var firstSeparator: UIView!
    
    @IBOutlet weak var secondSeparator: UIView!
    var onFirstBtnClick: (()->Void)?
    var onSecondBtnClick: (()->Void)?
    var onThirdBtnClick: (()->Void)?
    
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet var contentView: UIView!
    
    var disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "ProcessHeaderView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
       
        contentView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
    }
    
   
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        self.firstBtn.makeItRound()
        self.secondBtn.makeItRound()
        self.thirdBtn.makeItRound()
        
        self.firstSeparator.addDashedBorder()
        self.secondSeparator.addDashedBorder()
    }
    
    @IBAction func firstBtnClickHandler(_ sender: UIButton) {
        onFirstBtnClick?()
    }
    @IBAction func secondBtnClickHandler(_ sender: UIButton) {
        onSecondBtnClick?()
    }
    @IBAction func thirdBtnClickHandler(_ sender: UIButton) {
        onThirdBtnClick?()
    }
    func makeBtnSelected(index:Int){
        self.firstBtn.setBackgroundImage(UIImage(named: "dashc"), for: .normal)
        self.secondBtn.setBackgroundImage(UIImage(named: "dashc"), for: .normal)
        self.thirdBtn.setBackgroundImage(UIImage(named: "dashc"), for: .normal)
        
        self.firstBtn.backgroundColor = UIColor.white
        self.secondBtn.backgroundColor = UIColor.white
        self.thirdBtn.backgroundColor = UIColor.white
        
        
        self.firstBtn.setTitleColor(UIColor.darkBlueGrey, for: .normal)
        self.secondBtn.setTitleColor(UIColor.darkBlueGrey, for: .normal)
        self.thirdBtn.setTitleColor(UIColor.darkBlueGrey, for: .normal)
        switch index {
        case 1:
            self.firstBtn.backgroundColor = UIColor.darkBlueGrey
            self.firstBtn.setTitleColor(UIColor.white, for: .normal)
            self.firstBtn.setBackgroundImage(nil, for: .normal)
        case 2:
            self.secondBtn.backgroundColor = UIColor.darkBlueGrey
            self.secondBtn.setTitleColor(UIColor.white, for: .normal)
            self.secondBtn.setBackgroundImage(nil, for: .normal)
        case 3:
            self.thirdBtn.backgroundColor = UIColor.darkBlueGrey
            self.thirdBtn.setTitleColor(UIColor.white, for: .normal)
            self.thirdBtn.setBackgroundImage(nil, for: .normal)
        case 0:
            print("unselect all")
        default:
            self.firstBtn.backgroundColor = UIColor.darkBlueGrey
            self.firstBtn.setTitleColor(UIColor.white, for: .normal)
            self.firstBtn.setBackgroundImage(nil, for: .normal)
        }
        
    }
}

