//
//  CaseDetailTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class CaseClosedTableViewCell: UITableViewCell {

    @IBOutlet weak var rejectedIconLbl: UILabel!

    @IBOutlet weak var bodyLbl: LocalizedLabel!
    
    var approveClick:(()->Void)?
    var rejectClick:(()->Void)?
    var submitClick:(()->Void)?
    var cancelClick:(()->Void)?
    @IBOutlet weak var rightLbl: UILabel!
    
    @IBOutlet weak var rightBgLbl: UILabel!
    @IBOutlet weak var xLbl: UILabel!
    
    @IBOutlet weak var xBgLbl: UILabel!
    
    @IBOutlet weak var feedBackView: RoundedView!

    @IBOutlet weak var commentTextView: LocalizedTextView!
    
    @IBOutlet weak var commentViewHolder: UIView!
    @IBOutlet weak var buttonsHolderView: UIView!
    
    
    
    @IBAction func callClick(_ sender: UIButton) {
        self.commentViewHolder.isHidden = true
        self.questionsStackView.isHidden = false
        approveClick?()
    }
    
    @IBAction func emailClick(_ sender: UIButton) {
        self.commentViewHolder.isHidden = false
        self.questionsStackView.isHidden = true
        rejectClick?()
    }
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.rejectedIconLbl.text = "\u{e042}"
        initializeButtons()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initializeButtons(){
        commentTextView.text = ""
        commentTextView.placeholder = L10n.additionalComments
        commentTextView.layer.borderWidth = 1.0
        commentTextView.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        
        
    }
    @IBOutlet weak var questionsStackView: UIStackView!
    
    @IBOutlet var cancelSubmitbtns: [LocalizedButton]!
    
    @IBAction func submitPressed(_ sender: Any) {
        submitClick?()
    }
    @IBAction func cancelPressed(_ sender: Any) {
        cancelClick?()
    }
}
