//
//  ErrorView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ServicePopup : UIView{
    
    var viewModel: AolServiceWithFaqsViewModel!
    
    @IBOutlet var bodyLabel: LocalizedLabel!
    @IBOutlet var titleLabel: LocalizedLabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    var disposeBag = DisposeBag()
    var isMap: Bool = false
    
    var title:String!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    convenience init(title:String ,isMap: Bool = false, viewModel: AolServiceWithFaqsViewModel) {
        self.init(frame: UIScreen.main.bounds)
        self.isMap = isMap
        self.viewModel = viewModel
        self.title = title
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        
        let viewFileName: String = isMap ? "ServicePopup" : "ServicePopup"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        setupHooks()
        UIApplication.shared.keyWindow?.addSubview(self)
        UIApplication.shared.keyWindow?.bringSubview(toFront: self)
    }
    
    @objc func cancelBtnClicked(){
        let height = UIApplication.shared.keyWindow!.bounds.height

        // animate from current position down out of the screen
        let superView = self.superview!
        superView.isUserInteractionEnabled = false
        UIView.animate(withDuration: Dimensions.animationDuration1, animations: { [weak self] in
            self?.center = (self?.center.applying(CGAffineTransform.init(translationX: 0, y: height)))!
            }, completion: { (finished) in
                // remove the view and put back the floating menu
                self.removeFromSuperview()
                self.displayFloatingMenu()
                superView.isUserInteractionEnabled = true
        })
            viewModel!.servicePopupModel.value = nil
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
   
    
    func setupHooks(){
        viewModel!.servicePopupModel.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] content in
                if(content == nil){
                    //TODO: should do somting ????
                }else{
                    self.setupUiData(content: content!)
                    
                }
            })
            .disposed(by: disposeBag)
    }
    
    func setupUiData(content:ServicePopupModel){
        self.bodyLabel.attributedText = NSAttributedString(string: "")
      self.titleLabel.text = title
        self.bodyLabel.attributedText = content.content?.getAttributedString()
    }
    
}
