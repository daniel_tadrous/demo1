//
//  CollapsedTableViewCell.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 7/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class CollapsedTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var serviceTitleLabel: LocalizedLabel!
    
    @IBOutlet weak var expandCellAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.addRoundCorners(radious: 5)
        self.containerView.layer.shadowOpacity = 0.1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
