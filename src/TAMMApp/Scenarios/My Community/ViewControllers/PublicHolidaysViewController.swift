//
//  PublicHolidaysViewController.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 7/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

protocol PublicHolidaysViewControllerDelegate {
}

class PublicHolidaysViewController: TammViewController,PublicHolidaysStoryboardLoadable {
    
    let expandedCellIdentifier = "HolidayExpandedCell"
    let collapsedCellIdentifier = "HolidayCollapsedCell"
    let headerViewIdentifier = "PublicHolidaySectionHeader"
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var searchView: TammSearchView!
    
    var eventFilterModel = EventsFilterApplyModel()
    
    @IBOutlet weak var eventsTableView: UITableView!
    public var viewModel:PublicHolidaysViewModel!
    private var disposeBag = DisposeBag()
    
    
    var upComingEvents:[Event]{
        get{
            return viewModel.holidays.value?.filter({ (event) -> Bool in
                let format = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                let eventDate = event.start?.getDate(format: format)
                return (!(eventDate?.timeIntervalSinceNow.isLess(than: 0))!)
            }) ?? []
        }
    }
    
    var PreviousEvents:[Event]{
        get{
            return viewModel.holidays.value?.filter({ (event) -> Bool in
                let format = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                let eventDate = event.start?.getDate(format: format)
                return ((eventDate?.timeIntervalSinceNow.isLess(than: 0))!)
            }) ?? []
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        // Do any additional setup after loading the view.
        eventsTableView.register(UINib(nibName: expandedCellIdentifier, bundle: nil), forCellReuseIdentifier: expandedCellIdentifier)
        eventsTableView.register(UINib(nibName: collapsedCellIdentifier, bundle: nil), forCellReuseIdentifier: collapsedCellIdentifier)
        eventsTableView.register(UINib(nibName: headerViewIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        sortButton.setTitle("\u{e008}", for: .normal)
        searchView.PlaceholderTextContent = L10n.search
        self.searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(searchClicked)))
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        let filterView = FilterView(frame: UIScreen.main.bounds, delegateViewModel: viewModel,filterViewDelegate:self,enableSearchLocation:false)
        UIApplication.shared.keyWindow!.addSubview(filterView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: filterView)
    }
    
    @IBAction func sortButtonAction(_ sender: UIButton) {
        print("sort")
        viewModel.sort()
    }
    
    @objc func searchClicked(){
        let searchViewOverLay = SearchOverlayView(frame: UIScreen.main.bounds, currentViewModel: self.viewModel!, text: self.searchView.searchField.text!, currentSearchModel: self.viewModel!)
        searchViewOverLay.delegate = self
        UIApplication.shared.keyWindow!.addSubview(searchViewOverLay)
        UIApplication.shared.keyWindow!.bringSubview(toFront: searchViewOverLay)
        print("Search View Clicked")
    }
    
}
extension PublicHolidaysViewController:UITableViewDelegate, UITableViewDataSource{
    
    func setupHooks(){
        viewModel!.holidays.asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] eventsRes in
                if(eventsRes == nil){
                    //TODO: should do somting ????
                }else{
                    print(eventsRes!.count)
                    self.eventsTableView.reloadData()
                    print(self.upComingEvents)
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier)
        headerView?.backgroundColor = UIColor.paleGreyTwo
        let titlelabel = headerView?.viewWithTag(1) as! LocalizedLabel
        switch section {
        case 0:
            titlelabel.text = L10n.upcoming
        default:
            titlelabel.text = L10n.earlier
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return upComingEvents.count
        default:
            return PreviousEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var event:Event
        
        switch indexPath.section {
        case 0:
            event = upComingEvents[indexPath.row]
        default:
            event = PreviousEvents[indexPath.row]
        }
        var cell:HolidayCell! = tableView.dequeueReusableCell(withIdentifier: expandedCellIdentifier) as! HolidayCell
        if event.isExpanded {
            
            cell.collapsedView.isHidden = true
            cell.expandedView.isHidden = false
            cell.expand()
            cell.Hijri?.text = event.hijri
        }
        else{
            
            cell.collapsedView.isHidden = false
            cell.expandedView.isHidden = true
            cell.collapse()
            cell.Hijri?.text = ""
        }
        cell.eventTitle.text = event.title
        if (event.addresses?.count)! > 1 {
            cell.eventVenues?.text = "Multiple Venues"
            
            cell.eventVenuesExpanded?.text = "Multiple Venues"
        }
        else if (event.addresses?.count)! == 1 {
            cell.eventVenues?.text = event.addresses![0].description
            cell.eventVenuesExpanded?.text = event.addresses![0].description
        }
        else{
            cell.eventVenues?.text = ""
            cell.eventVenuesExpanded?.text = ""
        }
        
        cell.eventStartMonth.text = ""
        cell.eventStartDay.attributedText = event.getFormattedDateAttributed()
        cell.eventEndMonth.text = event.getFormattedEndMonth(monthFormatOfthree: false)
        cell.eventEndDay.text = event.getFormattedEndDay()
        cell.eventDescription?.text = event.description
        cell.addToCalenderClosure = {
            self.viewModel.addToCalendar(event: event)
        }
        cell.findOutMoreClosure = {
            self.viewModel.findOutMore()
        }
        cell.shareEventClosure = {
            self.viewModel.shareViewModel.share(viewController: self, text: event.link!)
        }

        cell.timeLabel?.text = event.getFormattedTime()
        cell.eventTimeExpanded?.text = event.getFormattedTime()
        
        if !event.isReloaded && indexPath.row==2{
            event.isReloaded = true
            tableView.reloadData()
        }
        
        if event.isExpanded {
            let constraint = cell.imageAspectRatioConstraint.constraintWithMultiplier(event.imageAspectRatio)
            cell.eventImage.removeConstraint(cell.imageAspectRatioConstraint)
            cell.imageAspectRatioConstraint = constraint
            cell.eventImage.addConstraint(cell.imageAspectRatioConstraint)
        }
        
        
        
        ImageCachingUtil.loadImage(url: URL(string: (event.image)!)!, dispatchInMain: true
            , callBack: { (url, image) in
                if event.isExpanded && !event.isReloaded{
                }else{
                    event.isReloaded = true
                    event.imageAspectRatio = (image?.size.width)!/(image?.size.height)!
                    let constraint = cell.imageAspectRatioConstraint.constraintWithMultiplier(event.imageAspectRatio)
                    cell.eventImage.removeConstraint(cell.imageAspectRatioConstraint)
                    cell.imageAspectRatioConstraint = constraint
                    cell.eventImage.addConstraint(cell.imageAspectRatioConstraint)
                }
                
                cell.eventImage.image = image
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var event:Event
        switch indexPath.section {
        case 0:
            event = upComingEvents[indexPath.row]
        default:
            event = PreviousEvents[indexPath.row]
        }
        
        event.isExpanded = !event.isExpanded
        let cell = tableView.cellForRow(at: indexPath) as! HolidayCell
        //let rectOfCellInTableView = tableView.rectForRow(at: indexPath)
        if event.isExpanded{
            cell.expand()
            
            cell.Hijri?.text = event.hijri

        }else{
            cell.collapse()
            
            cell.Hijri?.text = ""

        }
        tableView.beginUpdates()
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 36
    }
    
}

extension PublicHolidaysViewController:SearchOverlayViewDelegate{
    
    func eventIsSelected(message: TalkToUsItemViewDataType) {
        self.searchView.searchField.text = message.name
        self.searchView.PlaceholderTextContent = message.name
        self.viewModel?.searchedText.value = message.name
    }
    func backPressed(){
        self.searchView.searchField.text = ""
    }
    
}

extension PublicHolidaysViewController:FilterViewDelegate{
    var filterModel: EventsFilterApplyModel {
        get {
            return eventFilterModel
        }set{
            eventFilterModel = newValue
        }
    }
    
    func applyFilter() {
        print(filterModel)
        viewModel.filterObject.value = filterModel
        viewModel.getMyEvents()
    }
    
    
    
}

