//
//  ManageMediaViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

enum MediaTypeName :String {
    case audio = "Audio"
    case image = "Image"
    case video = "Video"
}

struct MediaUrl {
   var isSelected = false
   var myUrl:URL
    var mediaType:MediaTypeName
}

class ManageMediaViewModel {
    
    var allMedia : Variable<[MediaUrl]?> = Variable(nil)
    var currentFont:UIFont?
    
    let regularFont = UIFont(name: "Roboto-Regular", size: UIFont.fontSizeMultiplier * 16.0)!
    
    let regularArabicFont = UIFont(name: "Swissra-Normal", size: UIFont.fontSizeMultiplier * 16.0)!
    
    let tammFont = UIFont(name: "tamm", size: UIFont.fontSizeMultiplier * 16.0)!
    
    func prepareTextInNoteLabel(withDuration duration:String)->NSMutableAttributedString {
        setCurrentFont()
        let durationText = NSMutableAttributedString(string: duration, attributes: [
            .font:  currentFont!])
        durationText.addAttribute(.font, value: currentFont!, range: NSRange(location: 0, length: duration.count))
        durationText.addAttribute(.foregroundColor, value: UIColor.petrol, range: NSRange(location: 0, length: duration.count))
        
        let videoIcon =  NSMutableAttributedString(string: "*", attributes: [
            .font:  tammFont])
        
        videoIcon.addAttribute(.font, value: currentFont!, range: NSRange(location: 0, length: 1))
        videoIcon.addAttribute(.foregroundColor, value: UIColor.petrol, range: NSRange(location: 0, length: 1))
        
        durationText.append(videoIcon)
        
        return durationText
    }
    
    
    
    func setCurrentFont() {
        if L10n.Lang == "en" {
            currentFont = regularFont
        }else {
            currentFont = regularArabicFont
        }
    }
    func getMediaData() {
        do {
            let documentsURL = IncidentUrl.getDirectoryForIncident()
            let docs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            var myMedia = [MediaUrl]()
            for url in docs {
                        var isAudio = false
                        for extensionType in MediaFormats.audioFormats{
                            if url.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame {
                                if url.createThumbnailOfVideoFromFileURL() == nil {
                                    myMedia.append(MediaUrl(isSelected: false, myUrl: url, mediaType: .audio))
                                    isAudio = true
                                    break
                                }
                            }
                        }
                
                      
                
                        for extensionType in MediaFormats.vedioFormats{
                            if url.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame && !isAudio{
                                myMedia.append(MediaUrl(isSelected: false, myUrl: url, mediaType: .video))
                                break
                            }
                        }
                
                        for extensionType in MediaFormats.imgFormats{
                            if url.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame {
                                myMedia.append(MediaUrl(isSelected: false, myUrl: url, mediaType: .image))
                                break
                            }
                        }
                
            }

            allMedia.value = myMedia
        } catch {
            print(error)
        }
    }
    
    
    func deleteUrl(removedUrl:URL) {
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: removedUrl)
            getMediaData()
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    //    fileprivate func sizePerMB(url: URL?) -> Double {
    //        guard let filePath = url?.path else {
    //            return 0.0
    //        }
    //        do {
    //            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
    //            if let size = attribute[FileAttributeKey.size] as? NSNumber {
    //                return Double(round(100*(size.doubleValue / 1000000.0))/100)
    //            }
    //        } catch {
    //            print("Error: \(error)")
    //        }
    //        return 0.0
    //    }
    
    func sizePerMB(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return (size.doubleValue / 1000000.0)
            }
        } catch {
            print("Error: \(error)")
        }
        return 0
    }
    func getDisplayedNumber(number:Double)->String {
        let number = Double(round(100*(number))/100)
        if number == 0 {
            return "0"
        }else {
            return String(number)
        }
    }
    
}



