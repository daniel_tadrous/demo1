//
//  MessageModel.swift
//  TAMMApp
//
//  Created by Daniel on 7/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class CaseMsg:Mappable{
    
    var text: String?
    var url: String?
   
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        text <- map["text"]
        url <- map["url"]
    }
}
