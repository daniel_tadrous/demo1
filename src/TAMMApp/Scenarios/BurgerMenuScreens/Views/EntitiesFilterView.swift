//
//  EntitiesFilterView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift

class EntitiesFilterView : UIView{
    
    var categories: [PickerItem] = [PickerItem(id: 0,displayName: L10n.Individual), PickerItem(id: 1,displayName: L10n.Business), PickerItem(id: 2,displayName: L10n.Tourist)]
    
    var aols: [PickerItem] = [PickerItem]()
    
    var selectedCategoryId = -1
    var selectedAolId = -1
    
    
    @IBOutlet weak var categoryArrowLabel: UILabel!
    @IBOutlet weak var aolArrowLabel: UILabel!
    
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var aolTextField: UITextField!
    
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var aolsView: UIView!
    
    

    private var viewModel:GovernmentEntityViewModel!
    private let cellIdentifier = "FilterTableCell"
    
    @IBOutlet weak var applyFiltersBtn: LocalizedButton!
    var disposeBag = DisposeBag()
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    var onApplyFilterClick: ((String,String)->Void)?
    var filtersStr:String = ""
    var filters:[GovernmentEntityFilter]{
        get{
            return self.viewModel.filters.value ?? []
        }
    }
    
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    init(frame: CGRect,viewModel: GovernmentEntityViewModel,onApplyFilterClick: @escaping (String,String)->Void){
        super.init(frame: frame)
        self.viewModel = viewModel
        self.onApplyFilterClick = onApplyFilterClick
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "EntitiesFilterView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey)
        
        setupUiData()
        setupHooks()
    }
   
    
    @objc func cancelBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
        self.filtersStr = ""
        self.viewModel.isToGetFilters.value = true
        self.viewModel.filters.asObservable().subscribe(onNext: { (filters) in
            if filters != nil{
                self.aols.append(PickerItem(id: "0",displayName: "All (\(filters!.count))"))
                for aspectOfLife in filters!{
                    self.aols.append(PickerItem(id: aspectOfLife.id!,displayName: aspectOfLife.name!))
                }
            }
        })
        
    }
    
    func setupUiData(){
       //category drop down
        self.categoryArrowLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.categoriesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCategoryDropDownClick)))
        self.categoriesView.addRoundAllCorners(radious: 5)
        self.categoriesView.layer.borderWidth = 1
        self.categoriesView.layer.borderColor = UIColor.cloudyBlue.cgColor
        self.categoryTextField.attributedPlaceholder =  NSAttributedString(string: L10n.select_category,                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkBlueGrey])
        
        //aspects of life drop down
        self.aolArrowLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.aolsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onAolDropDownClick)))
        self.aolsView.addRoundAllCorners(radious: 5)
        self.aolsView.layer.borderWidth = 1
        self.aolsView.layer.borderColor = UIColor.cloudyBlue.cgColor
        self.aolTextField.attributedPlaceholder =  NSAttributedString(string: L10n.select_aspect_of_life,                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkBlueGrey])
        
        
        
        //apply btn
        self.applyFiltersBtn.addRoundCorners(radious: 23)
          self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
    }
    
    
    @IBAction func ApplyFilterClickHandler(_ sender: LocalizedButton) {
        
        self.onApplyFilterClick?("\(self.selectedAolId)",self.categoryTextField.text!)
        self.cancelBtnClicked()
    }
   
    
    // Mark:  categories
    @objc func onCategoryDropDownClick(){
        self.openCategoryPickerView()
    }
    
   
    
    
    private func openCategoryPickerView(){
        let picker = PickerView()
        picker.items = self.categories
        var selectedIndex = -1
        for i in 0..<self.categories.count {
            if self.categories[i].id as! Int == selectedCategoryId {
                selectedIndex = i
                break
            }
        }
        if selectedIndex != -1{
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        self.categoryTextField.inputView = picker
        picker.tag = 1
        picker.viewDelegate = self
        self.categoryTextField.becomeFirstResponder()
    }
    
    
    // Mark: Aspect of life
    
    @objc func onAolDropDownClick(){
        self.openAolPickerView()
    }
   
   
    
    private func openAolPickerView(){
        let picker = PickerView()
        picker.items = self.aols
        var selectedIndex = -1
        for i in 0..<self.aols.count {
            if Int(self.aols[i].id as! String) == selectedAolId {
                selectedIndex = i
                break
            }
        }
        if selectedIndex != -1{
            picker.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
        self.aolTextField.inputView = picker
        picker.tag = 2
        picker.viewDelegate = self
        self.aolTextField.becomeFirstResponder()
    }
 
    
    
   
   //dismiss keyboard
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
}
extension EntitiesFilterView: PickerViewDelegate{
    func itemSelected(item: PickerItemType, pickerView: PickerView) {
        if pickerView.tag == 1{
            self.categoryTextField.text = item.displayName
            self.selectedCategoryId = item.id as! Int
        }else{
            self.aolTextField.text = item.displayName
            self.selectedAolId = Int(item.id as! String)!
        }
    }
    
    func clickOutsidePicker() {
        self.categoryTextField.resignFirstResponder()
    }
    
    
}
