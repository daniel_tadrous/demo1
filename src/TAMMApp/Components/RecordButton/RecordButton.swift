//
//  RecordButton.swift
//  TAMMApp
//
//  Created by kerolos on 6/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import DownloadButton
import Photos


protocol RecordButtonDelegate: class {
    func recordButtonDidRecordAnAudioFile(url: URL)
}

class RecordButton: PKCircleProgressView{
    
    weak var delegate: RecordButtonDelegate?
    
    var audioLimit: Double = 60 * 1
    
    private var voiceRecorder = VoiceRecorder()
//    private var voicePlayer = VoicePlayer()
    private var recordedURL: URL?
    
//    private(set) var asset: PHAsset?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    
    func commonInit(){
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.recordBtnLongPressed) )
        
        self.addGestureRecognizer(recognizer)
        
        self.tintColor = #colorLiteral(red: 0.01176470588, green: 0.3568627451, blue: 0.4509803922, alpha: 1)
        self.backgroundColor =  UIColor(patternImage: "V".tammImage(fontColor: self.tintColor, size: CGSize(width: 60, height: 60), fontSize: 40, backgroundColor: .white )! )
        
        self.layer.cornerRadius = self.bounds.width / 2
        
        
        voiceRecorder.delegate = self
        
        
        
    }
    private let startStates: [UIGestureRecognizerState] = [UIGestureRecognizerState.began]
    private let stopStates: [UIGestureRecognizerState] = [UIGestureRecognizerState.ended, UIGestureRecognizerState.failed, UIGestureRecognizerState.cancelled]
    private var audioOutputURL: URL!
    
    @objc func recordBtnLongPressed(_ sender:UILongPressGestureRecognizer){
        if startStates.contains(sender.state){
            startRecording()
        }
        else if stopStates.contains(sender.state) {
            stopRecording()
        }
    }
    
    
    private func startRecording() {
        if(voiceRecorder.requestPermissions() ){
            voiceRecorder.record()
            self.timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: self.increaseProgress)
        }
        else{
            //self.displayToast(message: L10n.micPermissionDenied)
        }
    }
    private func stopRecording(){
        voiceRecorder.stop()
        timer?.invalidate()
    }
    
    private var timer: Timer?
//    private let sessionQueue = DispatchQueue(label: "sessionQueue") // a serial queue
    
    
    func increaseProgress(_ currentTimer: Timer ){
        let step = 0.1 / Double(audioLimit)
        if(self.progress + CGFloat(step) < 1.0){
            self.progress += CGFloat(step)
            print(self.progress)
        }
        else{
            DispatchQueue.main.async {
                self.timer?.invalidate()
                self.stopRecording()
            }
        }
    }
    
    
    
    func handelRecordedFile(url: URL){
        self.recordedURL = url
        
//        voicePlayer.play(url: url)

//        SavePHAsset.saveAudioPHAsset(url) { (asset) in
//            print(#function)
//            print(asset)
//        }
        self.delegate?.recordButtonDidRecordAnAudioFile(url: url)

    }
    
    
}

extension RecordButton: VoiceRecorderDelegate{
    func voiceRecorderDidFinishRecording(url: URL?, success: Bool) {
        self.progress = 0
        self.timer?.invalidate()

        if (success){
            if let url = url {
                handelRecordedFile(url:url)
            }else{
                
            }
        } else{
            
        }
    }
    
    
}

