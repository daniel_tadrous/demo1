//
//  IncidentConfirmationViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
protocol IncidentConfirmationViewControllerDelegate: class{
    func goToMyLocker()

}
class IncidentConfirmationViewController: TammViewController , IncidentConfirmationStoryboardLodable{
    @IBOutlet weak var refrenceNoLbl: UILabel!
    @IBOutlet weak var goToMyLockerLbl: UILabel!
    @IBOutlet weak var myLockerIcon: UILabel!
    public weak var delegate: IncidentConfirmationViewControllerDelegate?
    public var viewModel:IncidentConfirmationViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideFloatingMenu()
        goToMyLockerLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToMyLocker)))
        myLockerIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToMyLocker)))
        refrenceNoLbl.addRoundCorners(radious: 25, borderColor: UIColor.turquoiseBlue)
        refrenceNoLbl.layer.cornerRadius = 25
        if #available(iOS 11.0, *) {
            if(L102Language.currentAppleLanguage().contains("ar")){
                refrenceNoLbl.layer.maskedCorners = [ .layerMaxXMaxYCorner,  .layerMinXMaxYCorner, .layerMinXMinYCorner]
            }
            else{
                refrenceNoLbl.layer.maskedCorners = [ .layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
        } else {
            // Fallback on earlier versions
        }
        refrenceNoLbl.clipsToBounds = true
        refrenceNoLbl.setNeedsLayout()
        refrenceNoLbl.text = viewModel.refrenceNumber
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setNavigationBar(title: L10n.confirmation, willSetSearchButton: false)

    }
    @objc func goToMyLocker(){
        self.displayFloatingMenu()
        delegate?.goToMyLocker()
    }

   
}
