//
//  MySubscriptionCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 9/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class MySubscriptionCell: UITableViewCell {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var typeLabel: LocalizedLabel!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    @IBOutlet weak var accountNumberLabel: LocalizedLabel!
    @IBOutlet weak var accountNumber: LocalizedLabel!
    @IBOutlet weak var expiryDateLabel: LocalizedLabel!
    @IBOutlet weak var expiryDate: LocalizedLabel!
    @IBOutlet weak var nextLabel: UILabel!
    @IBOutlet weak var roundedView: RoundedView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setViewColors() {
        let contentViewBackgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9529411765, alpha: 1).getAdjustedColor()
        let roundedVieBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        let labelTextColor = #colorLiteral(red: 0.1140267178, green: 0.09728414565, blue: 0.2838283181, alpha: 1).getAdjustedColor()
        let typeLabelTextColor = #colorLiteral(red: 0, green: 0.4236874282, blue: 0.5185461044, alpha: 1).getAdjustedColor()
        let shadowColor = #colorLiteral(red: 0.8506266475, green: 0.8481912017, blue: 0.8733561635, alpha: 1).getAdjustedColor()
        self.backgroundColor = contentViewBackgroundColor
        typeLabel.textColor = typeLabelTextColor
        titleLabel.textColor = labelTextColor
        accountNumberLabel.textColor = labelTextColor
        accountNumber.textColor = labelTextColor
        expiryDate.textColor = labelTextColor
        expiryDateLabel.textColor = labelTextColor
        nextLabel.textColor = labelTextColor
        roundedView.backgroundColor = roundedVieBackgroundColor
        roundedView.BorderColor = shadowColor
    }

}
