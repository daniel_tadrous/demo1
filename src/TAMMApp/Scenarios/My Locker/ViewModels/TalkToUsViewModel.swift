//
//  TalkToUsViewModel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import Photos
import RxSwift
import MediaPlayer
import AVFoundation
import RealmSwift


enum MessageTypes: String{
    case suggestion =  "Suggestion"
    case complaint = "Complaint"
    case compliment = "Compliment"
    case incident = "Incident"
    case requestForInformation = "Request for Information"
}

enum MessageTypesId: Int{
    case suggestion =  0
    case complaint = 1
    case compliment = 2
    case incident = 3
    case requestForInformation = 4
    case requestForService = 5
}
enum MessageOpenMethod : Int{
    case OpenEmptyMessage = 1
    case OpenMessage = 2
    case OpenDraft = 3
}

protocol MessageTypeViewDataType{
    var messageName: String {get}
    var messageId: Int {get}
}
protocol MessageViewDataBase{
    //The title of the message
    var title: String{get}
    //The type ID of the message
    var messageTypeId: Int?{get}
    //    Indicates whether the user shall receive notifications by email
    var notifiedByEmail: Bool?{get}
    //Indicates whether the user shall receive notifications by push notification
    var notifiedByMobileApplication: Bool?{get}
    var notifiedByPhone: Bool?{get}
    var hasMedia: Bool?{get}
    //The media file of the message. OAS 2 doesn’t support an array of files, so the parameter here is only one file. Note that the associated User Story is marked as ‘Incomplete’
    var media: [MediaViewModelData]?{get}
}

protocol Media {
    var name:String{get}
    var resolution:String{get}
    var fileSize:String?{get}
    var asset:PHAsset!{get}
    var assetDocument:URL!{get}
    var assetSound:MPMediaItem!{get}
    var type: FileTypeEnum{get}
}

class MediaViewModelData:Object,Media {
    
    var type: FileTypeEnum{
        get{
            let videoFormats = MediaFormats.vedioFormats
            let audioFormats = MediaFormats.audioFormats
            let imgFormats = MediaFormats.imgFormats
            
            //let docFormats = ["pdf","doc","docx","txt"]
            
            let format:String = String((name.split(separator: ".")[1]) )
            if videoFormats.contains(where: { (vformat) -> Bool in
                return vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.VIDEO
            }else if audioFormats.contains(where: { (vformat) -> Bool in
                return vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.AUDIO
            }else if imgFormats.contains(where: { (vformat) -> Bool in
                return  vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.IMAGE
            }else{
                return FileTypeEnum.TXT
            }
            
        }
    }
    
    override static func primaryKey() -> String? {
        return "attachmentId"
    }
    @objc dynamic var attachmentId:String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var resolution: String = ""
    @objc dynamic var fileSize: String?
    var asset: PHAsset!
    var assetDocument:URL!
    var assetSound: MPMediaItem!
    var voiceUri: URL?
    var comment: String?
    var url: URL?{
        get{
           return URL(fileURLWithPath: urlStr!)
        }
    }
    @objc dynamic var urlStr: String?
   
    convenience init(name:String, resolution:String, fileSize:String?, url:URL) {
        self.init()
        self.name =  name
        self.resolution = resolution
        self.fileSize =  fileSize
        self.urlStr = url.path
    }
    func save(){
        do {
            let realm = try! Realm()
            try realm.write {
                realm.add(self, update: true)
            }
        }
        catch let e as Realm.Error{
            print( e )
        }
        catch let e{
            print( e )
        }
    }
    
    func delete()->Bool{
        do {
            let realm = try! Realm()
            try realm.write {
                realm.delete(self)
            }
        }
        catch let e as Realm.Error{
            print( e )
            return false
        }
        catch let e{
            print( e )
            return false
        }
        return true
    }
    
}
protocol MessageTypesViewDataType{
    var types: [MessageTypeViewDataType]?{ get }
    var selectedId: Int? { get }
}


class TalkToUsViewModel {
    public let assetsLimit:Int = 3
    public var method:MessageOpenMethod!
    let fileSizeLimit = 8192
    public let staticMessagesTypes:MessageTypesResponse?
    
    private(set) var exceededLimitCheck:Variable<Bool?> = Variable(false)
    private(set) var notSupportedCheck = false
    var onMediaChange: (()->Void)?
    private var mediaList:[MediaViewModelData] = []{
        didSet{
            DispatchQueue.main.async {
                self.onMediaChange?()
            }
        }
    }
    var availableAssets:Int{
        
        var val = assetsLimit - mediaList.count
        if val < 0 {
            val = 0
        }
        return val
    }
    func removeFromMediaList(index: Int){
        mediaList.remove(at: index)
    }
    
    func getMediaList() -> [MediaViewModelData]{
        return mediaList
    }
    
    func setMediaList(_ list: [MediaViewModelData]){
        mediaList = list
    }
    
    
    func setRawMedia(rawMedia:[PHAsset]){
        updateMediaForPhotosAndVideos(mediaRaw: rawMedia)
    }
    func setDocumentsList(urls:[URL],closure:@escaping ()->Void){
        updateMediaForDocuments(urls: urls,closure: closure)
    }
    
    func setsoundMedia(rawMedia:[MPMediaItem],closure:@escaping ()->Void) {
        updateMediaForSound(media: rawMedia,closure:closure)
    }
    
    func updateMediaForDocuments(urls:[URL],closure:@escaping ()->Void){
        exceededLimitCheck.value = false
        notSupportedCheck = false
        for url in urls{
            if availableAssets <= 0 {
                break
            }
            let name = url.lastPathComponent
            let size = url.fileSizeInKB
            let sizeString = String("\(size) KB")
            if size > fileSizeLimit{
                self.exceededLimitCheck.value = true
            }else{
                url.export(){ fileURL, error in
                    if error != nil {
                        
                    }else{
                        self.mediaList.append(MediaViewModelData(name: name, resolution: "0", fileSize: sizeString, url: fileURL!))
                    }
                }
                
            }
            closure()
        }
    }
    
    func updateMediaForSound(media:[MPMediaItem],closure:@escaping ()->Void) {
        exceededLimitCheck.value = false
        notSupportedCheck = false
        for medium in media{
            if availableAssets == 0 {
                closure()
                break
            }
            medium.export() { fileURL, error in
                guard let fileURL = fileURL, error == nil else {
                    closure()
                    return
                }
                let size = fileURL.fileSizeInKB
                if size > self.fileSizeLimit {
                    self.exceededLimitCheck.value = true
                    _ = try? FileManager.default.removeItem(at: fileURL)
                }
                else{
                    let sizeString = String("\(size) KB")
                    let duration = String("\(medium.playbackDuration) seconds")
                    if self.availableAssets != 0 {
                        self.mediaList.append(MediaViewModelData(name:"\( medium.title!).m4a", resolution: duration, fileSize: sizeString, url: fileURL))
                    }
                }
                closure()
            }
            
        }
        
    }
    
    
    
    private func updateMediaForPhotosAndVideos(mediaRaw: [PHAsset]) {
        
        exceededLimitCheck.value = false
        notSupportedCheck = false
        
        for media in mediaRaw{
            
            if availableAssets <= 0 {
                break
            }
            
            let (_,sizeOnDiskFormated, fileName) = media.getFileSizeAndSizeFormatedAndName()
            
            
            let name = fileName //media.originalFilename
            var resolution =  String(media.pixelWidth) + "x" + String(media.pixelHeight)
            if(L102Language.currentAppleLanguage().contains("ar")){
                resolution =  String(media.pixelWidth) + "x" + String(media.pixelHeight)
            }
            let size =  sizeOnDiskFormated // media.size
            let sizeStr = size?.components(separatedBy: " ").first
            let absSize = (sizeStr! as NSString).intValue * 1024
            
            media.getURL { (url) in
                
            
                if url!.type != .VIDEO{
                    if absSize > self.fileSizeLimit && (size?.contains("MB"))!{
                        self.exceededLimitCheck.value = true
                        return
                    }
                }
                media.export { (url, err) in
                    if err != nil {
                        
                    }else{
                        let mediaItem = MediaViewModelData(name: name!, resolution: resolution, fileSize: size, url: url!)
                        self.mediaList.append(mediaItem)
                    }
                }
            }
            
        }
        
    }
    
    
    let speechDuration: Double = 10.0
    // should be changed to get ne values of incident type
    
    init(messageServices: ApiMessageServicesTypes ,speechRecognitionService: SpeechRecognitionServiceType, userConfigService: UserConfigServiceType, supportApiService: ApiSupportServicesType) {
        
        self.userConfigService = userConfigService
        self.messageServices = messageServices
        self.speechRecognitionService = speechRecognitionService
        self.supportApiService = supportApiService
        userId.value = userConfigService.getUserId()
        //        availableAssets =  assetsLimit
        self.staticMessagesTypes = self.messageServices.getStaticMessageTypes()
        setUpBinding()
    }
    func getDraftById(draftId: String)-> DALDrafts?{
        return DALDrafts.getById(id: draftId)
    }
    
    func sendMessage(message:MessageViewDataBase)-> Observable<String>{
        
        return messageServices.submitMessage(message: self.fillModel(message: message))
    }
    func saveMessageToDrafts(NewMessage:IncidentMessageViewData){
        let media = NewMessage.media ?? []
        let voiceURI = (NewMessage.voiceURI?.absoluteString) ?? ""
        let draft = DALDrafts(draftTitle: NewMessage.title, typeId: NewMessage.messageTypeId!, location: NewMessage.incidentLocation!, locationCoordinates: NewMessage.locationCoordinates, mediaURis: media , hasMedia: NewMessage.hasMedia!, isInformationPublic: NewMessage.isInformationPublic, notifiedByEmail: NewMessage.notifiedByEmail!, notifiedByMobileApplication: NewMessage.notifiedByMobileApplication!, notifiedByPhone: NewMessage.notifiedByPhone!, draftDate: Date(), voiceNote: voiceURI , comment: NewMessage.comment ?? "", long :NewMessage.long , lat :NewMessage.lat  )
        draft.save()
        
    }
    func fillModel(message:MessageViewDataBase) -> AddMessageRequestModel {
        let ourMessage:IncidentMessageViewData = message as! IncidentMessageViewData
        let modelMessage:AddMessageRequestModel = AddMessageRequestModel()
        modelMessage.messageTypeId = message.messageTypeId
        modelMessage.title = ourMessage.title
        modelMessage.incidentLocation = ourMessage.incidentLocation
        modelMessage.hasMedia = ourMessage.hasMedia
        modelMessage.media = message.media?.map({ (item) -> String in
            return item.name
        })
        modelMessage.notifiedByEmail = ourMessage.notifiedByEmail
        modelMessage.notifiedByMobileApplication = ourMessage.notifiedByMobileApplication
        modelMessage.notifiedByPhone = ourMessage.notifiedByPhone
        
        
        return modelMessage
    }
    
    var title: String {
        get{
            return titleVar.value
        }
        set {
            titleVar.value = newValue
        }
    }
    
    var message:MessageViewDataBase!
    
    var messagesTypes: Observable<MessageTypesViewDataType?>
    {
        get{
            return self.messagesTypesVar.asObservable().map( { (res: MessageTypesResponse?) -> MessageTypesViewDataType? in
                guard let res = res  else {
                    return nil
                }
                
                let types = res.types?.map({ (type: MessageTypeModel) -> MessageTypeViewDataType in
                    return MessageTypeViewData(messageName: (type.text)!, messageId: (type.id)!)
                })
                // whe n the API returns -1 it means no Suggested type for the given title query
                // nil means no selection id found
                return MessageTypesViewData(types: types, selectedId: res.selectedId == -1 ? nil : res.selectedId)
            })
        }
    }
    
    
    
    var currentQuery: String?{
        didSet{
            
            currentQueryVar.value = currentQuery
        }
    }
    
    private let currentQueryVar : Variable<String?> = Variable(nil)
    
    private let messageServices: ApiMessageServicesTypes
    private let userConfigService: UserConfigServiceType
    
    
    private let supportApiService: ApiSupportServicesType
    
    private let disposeBag =  DisposeBag()
    // can be accesed in some cases but currently we update its value from the user configuration service
    private var userId : Variable<Int> = Variable(-1)
    
    
    private func setUpBinding(){
        self.messagesTypesVar.value = messageServices.getStaticMessageTypes()
        
        titleVar.asObservable().subscribe(onNext:{
            [unowned self] val in
            self.getSuggestedType(query: val)
        }).disposed(by: disposeBag)
    }
    
    func getisWifiOnlyStatus() ->Bool {
        let defaults = UserDefaults.standard
        
        let wifiStatus = defaults.object(forKey: UserDefaultsKeys.isUploadWifi)
        
        if let status = wifiStatus {
            return status as! Bool
        }else {
            return false
        }
        
    }
    private func getSuggestedType(query: String){
        messageServices.getMessageTypes(query: query).subscribe(onNext:{
            [unowned self] messageType in
            
            self.messagesTypesVar.value?.selectedId = messageType.selectedId
            
        }).disposed(by: disposeBag)
        
        currentQueryVar.asObservable().debounce(0.1, scheduler: ConcurrentDispatchQueueScheduler(qos: .background) ).subscribe(onNext:{ [unowned self] query in
            self.searchFor(userId: self.userId.value,  query:query)
        }).disposed(by: disposeBag)
    }
    
    private let titleVar: Variable<String> = Variable("")
    
    
    private let queryResultVar : Variable<[String]?> = Variable(nil)
    
    
    private func searchFor(userId: Int, query: String?){
        if query != nil && !(query?.isEmpty)!{
            supportApiService.searchForMessages(userId: userId, query: query!).subscribe(onNext:{
                [unowned self] searchResultArr in
                
                self.queryResultVar.value = searchResultArr
            }).disposed(by: disposeBag)
        }
        else{
            // empty the results in case of no input
            // do not call the api
            self.queryResultVar.value = []
        }
    }
    
    var queryResult: Observable<[QueryResultItemItemViewDataType]?> {
        get {
            return queryResultVar.asObservable().map({ (arr) -> [QueryResultItemItemViewDataType]? in
                return arr?.map({ (str) -> QueryResultItemItemViewDataType in
                    return QueryResultItemItemViewData(name: str)
                })
            })
        }
    }
    
    public let messagesTypesVar: Variable<MessageTypesResponse?> = Variable(nil)
    
    
    
    ////////////////////////////////// Mic ///////////////////////////////////
    
    private var speechDoneSubject: PublishSubject<String>?
    func startSpeechService() -> Observable<String>{
        speechRecognitionService.start(delegate: self, duration: speechDuration)
        speechDoneSubject = PublishSubject<String>()
        return (speechDoneSubject!.asObservable())
    }
    
    private let speechRecognitionService: SpeechRecognitionServiceType
    
}

extension TalkToUsViewModel: SpeechRecognitionServiceTypeDelegate{
    func recognizerAvailable(isAvailable: Bool) {
        // indecates that the recognizer is available and most likely will start
        // should inform the subscribers with the change
        // disable the recording for some time
        if( isAvailable){
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onNext("")
            
        }
        else{
            //            micRecodingIsEnabledVar.value = true
            speechDoneSubject?.onError("Service is not currently available" as Error)
        }
        
    }
    
    func textRecived(text: String) {
        //        micRecognizedVar.value = text
        speechDoneSubject?.onNext(text)
    }
    
    func recognitionIsDone(_ error: Error?) {
        // should inform the ui with the status and reset the availablility
        //        micRecodingIsEnabledVar.value = true
        //        micRecordingErrorVar.value = "\(String(describing: error))"
        //
        speechDoneSubject?.onCompleted()
    }
}


struct  MessageTypeViewData : MessageTypeViewDataType{
    var messageName: String
    var messageId: Int
}

struct  MessageTypesViewData : MessageTypesViewDataType{
    var types: [MessageTypeViewDataType]?
    
    var selectedId: Int?
    
}

struct IncidentMessageViewData: MessageViewDataBase{

    var messageType: String?
    
    
    var title: String
    
    var messageTypeId: Int?
    
    var notifiedByEmail: Bool?
    
    var notifiedByMobileApplication: Bool?
    
    var notifiedByPhone: Bool?
    
    var voiceURI: URL?
    var comment: String?
    
    var incidentLocation: String?
    
    var locationCoordinates: String?
    
    var isInformationPublic: Bool = true
    var long:Double = 0
    var lat:Double = 0
    //Indicates whether the message has media file(s)
    var hasMedia: Bool?
    //The media file of the message. OAS 2 doesn’t support an array of files, so the parameter here is only one file. Note that the associated User Story is marked as ‘Incomplete’
    var media: [MediaViewModelData]?
    init(title:String , messageType:Int , notifiedByEmail:Bool , notifiedByMobileApplication:Bool , notifiedByPhone:Bool , incidentLocation:String ,  media:[MediaViewModelData] , hasMedia:Bool, voiceURI: URL?, comment: String, locationCoordinates: String?, isInformationPublic: Bool, long:Double, lat:Double) {
        self.title = title
        self.messageTypeId = messageType
        self.notifiedByEmail = notifiedByEmail
        self.notifiedByMobileApplication = notifiedByMobileApplication
        self.notifiedByPhone = notifiedByPhone
        
        self.incidentLocation = incidentLocation
        
        
        self.media = media
        self.hasMedia = hasMedia
        
        
        self.voiceURI = voiceURI
        self.comment = comment
        self.locationCoordinates = locationCoordinates
        self.isInformationPublic = isInformationPublic
        self.long = long
        self.lat = lat
    }
    init(title:String) {
        self.title = title
    }
    init() {
        self.title = ""
    }
}

