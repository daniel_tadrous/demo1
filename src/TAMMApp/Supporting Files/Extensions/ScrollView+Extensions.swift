//
//  ScrollView+Extensions.swift
//  TAMMApp
//
//  Created by Daniel on 5/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView{
    
    
    func resizeToFitContent(){
        var contentRect = CGRect.zero
        for view in self.subviews {
            contentRect = contentRect.union(view.frame)
        }
        self.contentSize = contentRect.size
    }
}
