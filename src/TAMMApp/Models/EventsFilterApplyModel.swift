//
//  EventsFilterApplyModel.swift
//  TAMMApp
//
//  Created by Daniel on 6/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class EventsFilterApplyModel{
    var categories: [String] = []
    var category: String {
        get{
            var str = ""
            for cat in categories{
                str += "\(cat),"
            }
            return str
        }
    }
    var location = ""
    var from = ""
    var to = ""
    var q = ""
    
    var sort:String?
}
