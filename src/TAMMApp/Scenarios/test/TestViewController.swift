//
//  TestViewController.swift
//  TAMMApp
//
//  Created by kerolos on 4/2/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import Macaw // fan menu
//import FanMenu
import SideMenu


//enum AuthenticationMenuItem: Int {
//    case libraries
//    case aboutAuthor
//    case authorsBlog
//}


class TestViewController: UIViewController, TestStoryboardLodable {
    
    
//    var viewModel: TestViewModel!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setupUI()
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    @IBOutlet weak var fanMenu: FanMenu!
//
//    /*
//     // MARK: - Navigation
//
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destinationViewController.
//     // Pass the selected object to the new view controller.
//     }
//     */
//
//    func setupUI(){
//
//
//        let btn = FanMenuButton(
//            id: "main",
//            image: "AppIcon",
//            color: Color(val: 0x7C93FE)
//        )
//        fanMenu.button = btn
//        fanMenu.items = [
////            FanMenuButton(
////                id: "exchange_id",
////                image: "AppIcon",
////                color: Color(val: 0x9F85FF)
////            ),
////            //            ...
////            FanMenuButton(
////                id: "visa_id",
////                image: "AppIcon",
////                color: Color(val: 0xF55B58)
////            ),
////            FanMenuButton(
////                id: "visa_id",
////                image: "AppIcon",
////                color: Color(val: 0xF55B58)
////            ),
////            FanMenuButton(
////                id: "visa_id",
////                image: "AppIcon",
////                color: Color(val: 0xF55B58)
////            ),
////            FanMenuButton(
////                id: "visa_id",
////                image: "AppIcon",
////                color: Color(val: 0xF55B58)
////            ),
//
//        ]
//
//        fanMenu.menuRadius = 90.0
//        fanMenu.duration = 0.2
//        fanMenu.delay = 0.05
//        fanMenu.interval = (Double.pi, 2 * Double.pi)
//        fanMenu.backgroundColor = UIColor.clear
////        fanMenu.scene
//
//
//        // call before animation
//        fanMenu.onItemDidClick = { button in
//            print("ItemDidClick: \(button.id)")
//            if button.id as? String == "visa_id" || button.id as? String == "exchange_id"{
////                self.viewModel.viewTest2()
//
//                self.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
//
//                // Similarly, to dismiss a menu programmatically, you would do this:
////                dismiss(animated: true, completion: nil)
//
//
//            }
//            if button.id as? String == "main"{
////                let invertedColor = Color(val: 0x1000000  - button.color.val )
//                let fanGroup = self.fanMenu.node as? Group
//                let circleGroup = fanGroup?.contents[2] as? Group
//                let shape = circleGroup?.contents[0] as? Shape
//                let invertedColor = Color(val: 0x1000000  - ((shape?.fill as? Color )?.val)! )
//                shape?.fill = invertedColor
//
//
//            }
//        }
//        // call after animation
//        fanMenu.onItemWillClick = { button in
//            print("ItemWillClick: \(button.id)")
//        }
//
//
//        let sideMenuViewController = SideMenuViewController()
//        let menuRightNavigationController = UISideMenuNavigationController(rootViewController: sideMenuViewController)
////        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
////        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
////        // let menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as! UISideMenuNavigationController
//        SideMenuManager.default.menuRightNavigationController = menuRightNavigationController
////
////        // Enable gestures. The left and/or right menus must be set up above for these to work.
////        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
////
//
//        SideMenuManager.default.menuPresentMode = .menuSlideIn
//        SideMenuManager.default.menuBlurEffectStyle = UIBlurEffectStyle.dark
//        SideMenuManager.default.menuAnimationFadeStrength = CGFloat(0.4)
//
//        SideMenuManager.default.menuShadowOpacity = 0.0
//
//        SideMenuManager.default.menuAnimationTransformScaleFactor = 0.9
//
//        SideMenuManager.default.menuFadeStatusBar = true
//
//
//
//
//    }
}

