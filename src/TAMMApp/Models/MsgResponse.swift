//
//  ActiveMsgsResponse.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/14/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class MsgResponse:Mappable {
    
    var pageCount:Int?
    var options:[MsgStatus]?
    var hasStarred:Bool?
    var messages:[MessageModel]?
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        pageCount <- map["pageCount"]
        options <- map["options"]
        messages <- map["messages"]
        hasStarred <- map["hasStarred"]
    }
}



class FilterOptions:Mappable {
    
    var availableStatuses:[MsgStatus]?
    var hasStarred:Bool?
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        availableStatuses <- map["availableStatuses"]
        hasStarred <- map["hasStarred"]
        
    }
}
