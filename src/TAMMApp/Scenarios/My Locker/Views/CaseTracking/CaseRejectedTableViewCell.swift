//
//  CaseDetailTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class CaseRejectedTableViewCell: UITableViewCell {

    @IBOutlet weak var rejectedIconLbl: UILabel!

    @IBOutlet weak var bodyLbl: LocalizedLabel!
    
    var callClick:(()->Void)?
    var emailClick:(()->Void)?
    
    @IBAction func callClick(_ sender: UIButton) {
        callClick?()
    }
    
    @IBAction func emailClick(_ sender: UIButton) {
        emailClick?()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.rejectedIconLbl.text = "\u{e043}"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
