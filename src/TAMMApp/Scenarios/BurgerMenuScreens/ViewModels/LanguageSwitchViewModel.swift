//
//  PrefrencesViewModel.swift
//  TAMMApp
//
//  Created by kero1 on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation



class LanguageSwitchViewModel{
    
    
    let userConfigService: UserConfigServiceType
    
    init(userConfigService: UserConfigServiceType){
        self.userConfigService = userConfigService
        
        let lang = userConfigService.getCurrentLanguage()
        
        self.items = [[
                LanguageSwitchViewModel.LanguageViewData(displayString: userConfigService.getLanguageLitral(lang: .english), item: .english, selected: lang == .english),
                LanguageSwitchViewModel.LanguageViewData(displayString: userConfigService.getLanguageLitral(lang: .arabic), item: .arabic, selected: lang == .arabic),
                ]]
    }
    
    struct LanguageViewData {
        var displayString: String
        var item: TammSupportedLanguages
        var selected: Bool
    }

    public private(set) var items: [[LanguageSwitchViewModel.LanguageViewData]]

    
    public func getLanguageLitral() -> String{
        return userConfigService.getCurrentLanguageLitral()
    }
    
    func getCurrentLanguage() -> TammSupportedLanguages {
        return userConfigService.getCurrentLanguage()
    }
    
    func changeLanguage(lang: LanguageViewData){
        userConfigService.changeLanguage(lang.item)
    }
    
}

