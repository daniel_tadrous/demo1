//
//  Date+Extension.swift
//  TAMMApp
//
//  Created by Daniel on 6/21/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

extension Date{
    
    func getFormattedString(format: String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        dateFormatterPrint.locale = Locale(identifier: L10n.Lang)
        return dateFormatterPrint.string(from: self)
    }
}
