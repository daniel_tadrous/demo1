//
//  GovernmentEntitiesViewController.swift
//  TAMMApp
//
//  Created by Daniel on 7/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage

protocol GovernmentEntitiesViewControllerDelegate: class{
    func openGovernmentEntityInformation(id: String)
}

class GovernmentEntitiesViewController: TammViewController, GovernmentEntitiesStoryboardLodable {

    @IBOutlet weak var searchView: TammSearchView!
    @IBOutlet weak var lettersStackView: UIStackView!
    weak var delegate: GovernmentEntitiesViewControllerDelegate?
   
    @IBOutlet weak var tableview: UITableView!
    
    @IBAction func filterBtnClick(_ sender: UIButton) {
        let entitiesFilterView = EntitiesFilterView(frame:UIScreen.main.bounds, viewModel: self.viewModel,onApplyFilterClick:{aol,cat in
            self.viewModel.category = cat
            self.viewModel.aolId.value = aol
        })
        UIApplication.shared.keyWindow!.addSubview(entitiesFilterView)
        UIApplication.shared.keyWindow!.bringSubview(toFront: entitiesFilterView)
        
    }
    let headerViewIdentifier = "SectionHeaderView"
    let cellIdentifier = "EntityCell"
    var viewModel:GovernmentEntityViewModel!
    
    var disposeBag: DisposeBag = DisposeBag()
    var list:[(String, [GovernmentEntity])] {
        get{
            var list1:[String: [GovernmentEntity]] = [:]
            
            for item in self.viewModel.viewData.value ?? []{
                var lett = String(item.name!.first!)
                if lett == "ا"{
                    lett = "أ"
                }
                if lett == "ي"{
                    lett = "ى"
                }
                if list1[lett] == nil{
                    list1[lett] = []
                }
                list1[lett]?.append(item)
            }
            return list1.sorted(by: { $0.0 < $1.0
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: L10n.governmentEntities, willSetSearchButton: true)
        searchView.PlaceholderTextContent = L10n.searchForGE
         setupUi()
        setupHooks()
       
    }
    private func setupHooks(){
        viewModel.viewData.asObservable().subscribe(onNext: { (governmentEntities) in
            if governmentEntities != nil{
                self.tableview.reloadData()
                let letter = String(governmentEntities?.first?.name?.first ?? " ")
                self.setLetterBtnSelected(char: letter)
            }
        }).disposed(by: disposeBag)
    }
    private func setupUi(){
        let bundle = Bundle(for: type(of: self))
        self.tableview.register(UINib(nibName: headerViewIdentifier, bundle: bundle), forHeaderFooterViewReuseIdentifier: headerViewIdentifier)
        
        
        var str = "@"
        let count = L10n.Lang == "en" ? 26 : self.viewModel.arabicLetters.count
        for i in 0..<count{
            str = L10n.Lang == "en" ? String(str.map {
                (ch: Character) -> Character in
                    let scalars = String(ch).unicodeScalars
                    let val = scalars[scalars.startIndex].value
                    return Character(UnicodeScalar(val + 1)!)
                }) : self.viewModel.arabicLetters[i]
            let btn = UIButton()
            btn.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11)
            btn.setTitleColor(UIColor.darkBlueGrey, for: .normal)
            btn.setTitleColor(UIColor.sunflowerYellow, for: .highlighted)
            btn.setTitle(str, for: .normal)
            btn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(letterBtnClicked(_:))))
            self.lettersStackView.addArrangedSubview(btn)
        }
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableview.frame.width, height: 36))
        footerView.backgroundColor = UIColor.paleGreyTwo
        self.tableview.tableFooterView = footerView
        
        
    }
    @objc func letterBtnClicked(_ sender: UITapGestureRecognizer){
        unselectAllLetterBtns()
    
        let btn = sender.view as! UIButton
        let str = btn.title(for: .normal)
        
        for i in 0..<self.list.count{
            let item = self.list[i]
            if item.0 == str{
                btn.isHighlighted = true
                self.tableview.scrollToRow(at: IndexPath(row: 0, section: i), at: .top, animated: true)
            }
        }
        
    }
    private func getLetterIndex(char: String)->Int{
        if L10n.Lang == "en"{
        var str = "@"
        for i in 0..<26{
            str = String(str.map {
                (ch: Character) -> Character in
                    let scalars = String(ch).unicodeScalars
                    let val = scalars[scalars.startIndex].value
                    return Character(UnicodeScalar(val + 1)!)
            })
            if str == char{
                return i
            }
        }
        }else{
            for i in 0..<self.viewModel.arabicLetters.count{
                if char == self.viewModel.arabicLetters[i]{
                    return i
                }
            }
        }
        return 0
    }

    private func setLetterBtnSelected(char: String){
        unselectAllLetterBtns()
        (self.lettersStackView.arrangedSubviews[getLetterIndex(char: char)] as! UIButton).isHighlighted = true
    }
    private func unselectAllLetterBtns(){
        for btn in self.lettersStackView.arrangedSubviews{
            (btn as! UIButton).isHighlighted = false
        }
    }
}
extension GovernmentEntitiesViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list[section].1.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 76
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerViewIdentifier)
        let label  = headerView?.viewWithTag(1) as! UILabel
        label.text = self.list[section].0
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        cell?.viewWithTag(3)?.addRoundCorners(radious: 5)
        let item = self.list[indexPath.section].1[indexPath.row]
        let label = cell?.viewWithTag(1) as! UILabel
        label.text = item.name
        let cellImage = cell?.viewWithTag(2) as! UICircularImage
        cellImage.CircularFontImage = false
        ImageCachingUtil.loadImage(url: URL(string: (item.icon)!)!, dispatchInMain: true
            , callBack: { (url, image) in
                cellImage.Image = image
        })
        return cell!
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        onScroll()
    }
    func onScroll() {
        let indexPaths = self.tableview.indexPathsForVisibleRows
        let firstVisibleIndexPath = indexPaths?.count != 0 ? indexPaths?[0] : self.tableview.indexPathsForRows(in: CGRect(x: 15, y: 200, width: self.tableview.frame.width, height: 300))?[0]
        let header = self.list[(firstVisibleIndexPath?.section)!].0
        setLetterBtnSelected(char: header)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.list[indexPath.section].1[indexPath.row]
        self.delegate?.openGovernmentEntityInformation(id: item.id!)
    }
    
}
