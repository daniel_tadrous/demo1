//
//  DocumentCell.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/15/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class DocumentCell: UITableViewCell {
    
    @IBOutlet weak var typeLabel: LocalizedLabel!
    @IBOutlet weak var titleLabel: LocalizedLabel!
    @IBOutlet weak var expiredOnLabel: LocalizedLabel!
    @IBOutlet weak var expiryDateLabel: LocalizedLabel!
    @IBOutlet weak var idLabel: LocalizedLabel!
    @IBOutlet weak var label1Title: LocalizedLabel!
    @IBOutlet weak var detailesLabel1: LocalizedLabel!
    @IBOutlet weak var horizontalStackView: UIStackView!
    @IBOutlet weak var VerticalStackView: UIStackView!
    @IBOutlet weak var logoImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func commonInit(){
        self.backgroundColor = UIColor.paleGreyTwo
        horizontalStackView.isHidden = true
    }
    
    func addRow(title:String, details:String) {
        
        let stack2 = horizontalStackView.copyView() as! UIStackView
        stack2.isHidden = false
        VerticalStackView.addArrangedSubview(stack2)
        (stack2.arrangedSubviews[0] as! UILabel).text = title
        (stack2.arrangedSubviews[1] as! UILabel).text = details
    }
}

extension UIView
{
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}
