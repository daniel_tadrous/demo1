//
//  IntroScreenViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 5/21/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

struct IntroScreenViewData{
    var title: String
    var text: String
    var image: UIImage
    var showGetStartedButton: Bool
    var order: Int
}




class IntroScreenViewModel{
    var pageNumber: Int!
    var pageCount = 4
    var viewData: IntroScreenViewData {
        get{
            return allPages[pageNumber]
        }
    }
    
    var allPages = [
        IntroScreenViewData(title: L10n.services, text: L10n.services_intro_text, image: #imageLiteral(resourceName: "services_intro"), showGetStartedButton: false, order: 0),
        IntroScreenViewData(title: L10n.tailored_journeys, text: L10n.tailored_journeys_intro_text, image: #imageLiteral(resourceName: "journeys_intro"), showGetStartedButton: false, order: 1),
        IntroScreenViewData(title: L10n.talk_to_us, text: L10n.talk_to_us_intro_text, image: #imageLiteral(resourceName: "talk-to-us_intro"), showGetStartedButton: false, order: 2),
        IntroScreenViewData(title: L10n.updates, text: L10n.updates_intro_text, image: #imageLiteral(resourceName: "updates_intro"), showGetStartedButton: true, order: 3)
    ]
    
    
}
