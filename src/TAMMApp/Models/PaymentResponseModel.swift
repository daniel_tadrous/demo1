//
//  PaymentResponseModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 9/5/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentResponseModel:Mappable {
    
    var payments: [PaymentTypeModel]?
    var pageCount: Int?
    
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        payments <- map["payments"]
        pageCount <- map["pageCount"]
    }
    
    
}
