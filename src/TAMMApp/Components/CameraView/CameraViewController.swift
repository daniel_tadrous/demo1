//
//  CameraViewController.swift
//  TAMMApp
//
//  Created by kerolos on 5/6/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
//import MobileCoreServices
//import AssetsLibrary
import Photos
import DownloadButton

protocol CameraViewControllerDelegate {
    func cameraIsDismissed(assests:[PHAsset])
}

class CameraViewController: TammViewController, CameraStoryboardLoadable {
    // following http://drivecurrent.com/using-swift-and-avfoundation-to-create-a-custom-camera-view-for-an-ios-app/
    // and this https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/PhotoCaptureGuide/index.html
    
    // Default: Capture a compressed JPEG.
    let photoSettings1 = AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecJPEG])
    var videoCaptureDevice : AVCaptureDevice?
    var selectedAssets: [PHAsset] = []
    var delegate:CameraViewControllerDelegate?
    
    @IBOutlet weak var previewView: VideoPreviewView!
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var flashBtn: UIButton!
    @IBOutlet weak var photosViews: UIView!
    @IBOutlet weak var image1: CameraImageView!
    @IBOutlet weak var image2: CameraImageView!
    @IBOutlet weak var image3: CameraImageView!
    @IBOutlet weak var image4: CameraImageView!
    var imageViews: [CameraImageView] = []
    
    @IBOutlet weak var space1Constraint: NSLayoutConstraint!
    @IBOutlet weak var space2Constraint: NSLayoutConstraint!
    @IBOutlet weak var takeBtnBackgroundView: UIView!
    @IBOutlet weak var takeBtnProgressView: PKCircleProgressView!
    @IBOutlet weak var takeBtnFullView: UIView!
    
    @IBOutlet weak var takeBtn: UIView!
    private var timer:Timer!
    
    static var numberOfImagesStatic: CGFloat = 3
    var numberOfImages:CGFloat{
        get{
            return CameraViewController.numberOfImagesStatic
        }
        set{
            CameraViewController.numberOfImagesStatic = newValue
        }
    }
    private var currentSelectedImages:Int = 0
    private let videoLimit = 60.0 // seconds
    private var images:[UIImage] = []{
        didSet{
            // remove access images
            if(images.count < imageViews.count){
                for i in images.count..<imageViews.count {
                    imageViews[i].clearImage()
                }
            }
            for i in 0..<images.count {
                imageViews[i].addImage(image: images[i])
            }
        }
    }
    var userDidSetFlashEnabled: Bool = false{
        didSet{
            if(userDidSetFlashEnabled){
                self.flashBtn.setTitle("\u{e00f}", for: .normal)
            }
            else{
                self.flashBtn.setTitle("\u{e010}", for: .normal)
            }
        }
    }
    
    var captureSession: AVCaptureSession = AVCaptureSession()
    var capturePhotoOutput: AVCapturePhotoOutput!
    var captureVideoOutput: AVCaptureVideoDataOutput!
    var captureMovieFileOutput: AVCaptureMovieFileOutput!

    var previewLayer: AVCaptureVideoPreviewLayer?
    var activeCameraInput: AVCaptureDeviceInput!
    var videoOutputURL: URL!
    
    var photoSampleBuffer: CMSampleBuffer?
    var previewPhotoSampleBuffer: CMSampleBuffer?
    
    
    @IBAction func flashBtnClicked(_ sender: Any) {
        self.userDidSetFlashEnabled = !self.userDidSetFlashEnabled
    }
    
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        self.delegate?.cameraIsDismissed(assests: selectedAssets)
        self.dismiss(animated: false, completion: nil)
    }
    
    func checkCameraAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        
        var micStatus = AVAuthorizationStatus.notDetermined
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.audio){
            case .authorized:
                //The user has previously granted access to the mic.
                micStatus = .authorized
            case .notDetermined:
                // The user has not yet been presented with the option to grant mic access so request access.
                AVCaptureDevice.requestAccess(for: AVMediaType.audio, completionHandler: { success in
                    self.checkCameraAuthorization(completionHandler)
                })
                return
            
            case .denied:
                // The user has previously denied access.
                completionHandler(false)
            
            case .restricted:
                // The user doesn't have the authority to request access e.g. parental restriction.
                completionHandler(false)
        }
        
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            //The user has previously granted access to the camera.
            if micStatus == .authorized{
                completionHandler(true)
            }
            else{
                print("mic not granted permission")
            }
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access so request access.
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { success in
                if micStatus == .authorized{
                    completionHandler(true)
                }
                else{
                    print("mic not granted permission")
                }
            })
            
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
            
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideFloatingMenu()
        // Do any additional setup after loading the view, typically from a nib.
       self.imageViews = [image1, image2 , image3 , image4]
        for i in 0..<imageViews.count {
            self.imageViews[i].index = i
            self.imageViews[i].delegate = self
        }
        self.previewView.session = self.captureSession
        self.previewView.videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.takeBtnProgressView.isHidden = true
        self.takeBtn.addRoundAllCorners(radious: self.takeBtn.frame.width/2)
        self.flashBtn.setTitle("\u{e010}", for: .normal)
        self.takeBtnFullView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(takePhotoBtnTapped)))
        self.takeBtnFullView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(takePhotoBtnLongPressed)))
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let widthOfimageViews = self.image1.frame.width
        let widthOfPhotosView =  self.photosViews.frame.width
        var space = self.photosViews.frame.width
        if numberOfImages>1{
            space = (widthOfPhotosView - (numberOfImages * widthOfimageViews)) / (numberOfImages - 1.0)
        }else{
            self.image2.isHidden = true
            self.image3.isHidden = true
            self.image4.isHidden = true
        }
        self.space1Constraint.constant = space
        self.space2Constraint.constant = space
        self.takeBtnBackgroundView.addRoundAllCorners(radious: self.takeBtnBackgroundView.frame.width/2)
        
    }
    
    @objc func takePhotoBtnTapped(){
        UIView.animate(withDuration: 0.5, animations: {
            self.takeBtn.backgroundColor = UIColor.redTwo
        }) { (complete) in
            self.takeBtn.backgroundColor = UIColor.white
        }
        if(images.count < Int(numberOfImages)){
            self.snapPhoto()
        }
    }
    
    @objc func takePhotoBtnLongPressed(_ sender:Any){
        let recognizer = sender as! UILongPressGestureRecognizer
        if (recognizer.state == UIGestureRecognizerState.began)
        {
            self.takeBtnProgressView.isHidden = false
            self.takeBtn.backgroundColor = UIColor.redTwo
            if(images.count < Int(numberOfImages)){
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(increaseProgress), userInfo: nil, repeats: true)
                sessionQueue.asyncAfter(deadline: .now() , execute: {
                    self.startRecording()
                })
            }

        }
        else
        {
            if (recognizer.state == UIGestureRecognizerState.cancelled
                || recognizer.state == UIGestureRecognizerState.failed
                || recognizer.state == UIGestureRecognizerState.ended)
            {
                if(images.count < Int(numberOfImages)){
                    self.timer.invalidate()
                    self.displayToast(message: "Wait Seconds to process video")
                    sessionQueue.asyncAfter(deadline: .now() + 2, execute: {
                        self.stopRecording()
                    })
                }
                self.takeBtnProgressView.progress = 0
                self.takeBtnProgressView.isHidden = true
                self.takeBtn.backgroundColor = UIColor.white
            }
            
        }
        
    }
    
    @objc func increaseProgress(){
        let step = 1.0 / videoLimit
        if(takeBtnProgressView.progress + CGFloat(step) < 1.0){
                self.takeBtnProgressView.progress += CGFloat(step)
                print(self.takeBtnProgressView.progress)
        }
        else{
            self.takeBtnProgressView.progress = 0
            self.takeBtnProgressView.isHidden = true
            self.timer.invalidate()
            self.displayToast(message: "Wait Seconds to process video")
            sessionQueue.asyncAfter(deadline: .now() + 2, execute: {
                self.stopRecording()
            })
        }
    }
    func defaultDevice() -> AVCaptureDevice {
        if let device = AVCaptureDevice.default(.builtInDuoCamera, for: AVMediaType.video, position: .back) {
            return device // use dual camera on supported devices
        } else if let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back) {
            return device // use default back facing camera otherwise
        } else {
            fatalError("All supported devices are expected to have at least one of the queried capture devices.")
        }
    }
    
    func configureCaptureSession(_ completionHandler: ((_ success: Bool) -> Void)) {
        var success = false
        defer { completionHandler(success) } // Ensure all exit paths call completion handler.
        
        // Get video input for the default camera.
        let videoCaptureDevice = defaultDevice()
        self.videoCaptureDevice = videoCaptureDevice
        
        // Setup Microphone
        let microphone = AVCaptureDevice.default(for: AVMediaType.audio)
    
        guard let videoInput = try? AVCaptureDeviceInput(device: videoCaptureDevice) else {
            print("Unable to obtain video input for default camera.")
            self.displayToast(message: "Unable to obtain video input for default camera.")
            return
        }
        guard let micInput = try? AVCaptureDeviceInput(device: microphone!) else{
            print("Unable to obtain mic input.")
            self.displayToast(message: "Unable to obtain mic input.")
            return
        }

        // Create and configure the photo output.
        let capturePhotoOutput = AVCapturePhotoOutput()
        let captureVideoOutput = AVCaptureVideoDataOutput()
        let captureMovieFileOutput = AVCaptureMovieFileOutput()
        
        capturePhotoOutput.isHighResolutionCaptureEnabled = true
        capturePhotoOutput.isLivePhotoCaptureEnabled = capturePhotoOutput.isLivePhotoCaptureSupported
        
        // Make sure inputs and output can be added to session.
        guard (self.captureSession.canAddInput(videoInput)) else { return }
        self.activeCameraInput = videoInput
        guard (self.captureSession.canAddInput(micInput)) else { return }

        guard (self.captureSession.canAddOutput(capturePhotoOutput)) else { return }
        guard (self.captureSession.canAddOutput(captureMovieFileOutput)) else { return }
//        self.captureSession.addOutput(captureVideoOutput)

        // Configure the session.
        self.captureSession.beginConfiguration()
        self.captureSession.sessionPreset = AVCaptureSession.Preset.photo
        self.captureSession.addInput(videoInput)
        self.captureSession.addInput(micInput)
        self.captureSession.addOutput(capturePhotoOutput)
        self.captureSession.addOutput(captureMovieFileOutput)
        
        //        self.captureSession?.addOutput(capturePhotoOutput)
        //                previewView.layer.addSublayer(previewLayer!)
        
        self.captureSession.commitConfiguration()
        
        self.capturePhotoOutput = capturePhotoOutput
        self.captureVideoOutput = captureVideoOutput
        self.captureMovieFileOutput = captureMovieFileOutput
        
        //        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        //        self.previewView.layer.addSublayer(previewLayer!)
        //        self.previewLayer?.frame = previewView.bounds
        // usinf VideoPreviewView
        DispatchQueue.main.sync {
            self.previewView.session = self.captureSession
        }
        success = true
    }
    func checkPhotoLibraryAuthorization(_ completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            // The user has previously granted access to the photo library.
            completionHandler(true)
            
        case .notDetermined:
            // The user has not yet been presented with the option to grant photo library access so request access.
            PHPhotoLibrary.requestAuthorization({ status in
                completionHandler((status == .authorized))
            })
            
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
            
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        }
    }
    
    
    var isCaptureSessionConfigured = false // Instance proprerty on this view controller class
    let sessionQueue = DispatchQueue(label: "sessionQueue") // a serial queue
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isCaptureSessionConfigured {
            if !(self.captureSession.isRunning) {
                self.captureSession.startRunning()
            }
        } else {
            // First time: request camera access, configure capture session and start it.
            self.checkCameraAuthorization({ authorized in
                guard authorized else {
                    print("Permission to use camera denied.")
                    self.displayToast(message: L10n.CameraStrings.cameraPermissionDenied)
                    
                    return
                }
                self.sessionQueue.async {
                    self.configureCaptureSession({ success in
                        guard success else { return }
                        self.isCaptureSessionConfigured = true
                        self.captureSession.startRunning()
                        DispatchQueue.main.async {
                            self.previewView.updateVideoOrientationForDeviceOrientation()
                        }
                    })
                }
            })
        }
        
        self.checkPhotoLibraryAuthorization { (authorized) in
            if authorized {
                
            } else {
                self.displayToast(message: L10n.CameraStrings.storgePermissionDenied)
            }
        }
        
    }
    
    func snapPhoto() {
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        
        let photoSettings =  AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecJPEG]) // photoSettings1.
        //
        
        let videoPreviewLayerOrientation = previewView.videoPreviewLayer.connection?.videoOrientation
//        DispatchQueue.main.sync{
//            if let orientation =
//                previewView.videoPreviewLayer.connection?.videoOrientation{
//                videoPreviewLayerOrientation = orientation
//            }
//        }
        
        
        let desiredPreviewPixelFormat = kCVPixelFormatType_32BGRA
        
        
        
        if photoSettings.availablePreviewPhotoPixelFormatTypes.contains(kCVPixelFormatType_32BGRA){
            photoSettings.previewPhotoFormat = [
                kCVPixelBufferPixelFormatTypeKey as String : desiredPreviewPixelFormat,
                kCVPixelBufferWidthKey as String : NSNumber(value: 512),
                kCVPixelBufferHeightKey as String : NSNumber(value: 512)
            ]
        }
        
        self.sessionQueue.async {
            // Update the photo output's connection to match the video orientation of the video preview layer.
            if let photoOutputConnection = capturePhotoOutput.connection(with: AVMediaType.video) {
                photoOutputConnection.videoOrientation = videoPreviewLayerOrientation!
            }
            
            let photoSettings = AVCapturePhotoSettings()
            photoSettings.isAutoStillImageStabilizationEnabled = true
            photoSettings.isHighResolutionPhotoEnabled = true
            // Detecting the flash
            if (self.videoCaptureDevice?.hasTorch) ?? false {
                // lock your device for configuration
                do {
                    let abv = try self.videoCaptureDevice?.lockForConfiguration()
                    
                    if (self.userDidSetFlashEnabled){
                        if self.videoCaptureDevice?.isFlashAvailable ?? false{
//                            photoSettings.flashMode = .auto
                            photoSettings.flashMode = .on
                        }
                    } else{
                        photoSettings.flashMode = .off
                    }
                    // unlock your device
                    self.videoCaptureDevice?.unlockForConfiguration()
                } catch {
                    print("aaaa")
                }
            }

            
            capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self as AVCapturePhotoCaptureDelegate)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession.isRunning) {
            captureSession.stopRunning()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        previewLayer!.frame = previewView.bounds
    }

    
//    func captureVideo(){
//        sessionQueue.asyncAfter(deadline: .now() , execute: {
//            self.startRecording()
//        })
//        sessionQueue.asyncAfter(deadline: .now() + 10, execute: {
//            self.stopRecording()
//        })
//
//
//    }
    
    
}


extension CameraViewController : AVCapturePhotoCaptureDelegate{
    
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
                     previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                     resolvedSettings: AVCaptureResolvedPhotoSettings,
                     bracketSettings: AVCaptureBracketedStillImageSettings?,
                     error: Error?) {
        guard error == nil, let photoSampleBuffer = photoSampleBuffer else {
            
            print("Error capturing photo: \(error)")
            displayToast(message: "Error capturing photo: \(error)")
            return
        }
        
        self.photoSampleBuffer = photoSampleBuffer
        self.previewPhotoSampleBuffer = previewPhotoSampleBuffer
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput,
                     didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings,
                     error: Error?) {
        guard error == nil else {
            print("Error in capture process: \(error)")
            displayToast(message: "Error in capture process: \(error)")
            
            return
        }
        
        if let photoSampleBuffer = self.photoSampleBuffer {
            saveSampleBufferToPhotoLibrary(photoSampleBuffer,
                                           previewSampleBuffer: self.previewPhotoSampleBuffer,
                                           completionHandler: { success, asset, error in
                                            if success {
                                                print("Added JPEG photo to library.")
                                                //self.displayToast(message: "Added JPEG photo to library.")
                                                
                                                // to trigger the did set
                                                self.addAsset(asset: asset)
                                                //requesting the image
                                                //self.imageViews[ self.currentSelectedImages].addImage(image: img)
                                                //
                                                
                                            } else {
                                                print("Error adding JPEG photo to library: \(error)")
                                                self.displayToast(message: "Error adding JPEG photo to library: \(error)")
                                            }
            })
        }
    }
    
    
    
    func saveSampleBufferToPhotoLibrary(_ sampleBuffer: CMSampleBuffer,
                                        previewSampleBuffer: CMSampleBuffer?,
                                        completionHandler: ((_ success: Bool, _ asset: PHAsset?,  _ error: Error?) -> Void)?) {
        self.checkPhotoLibraryAuthorization({ authorized in
            guard authorized else {
                print("Permission to access photo library denied.")
                completionHandler?(false, nil, nil)
                return
            }
            
            guard let jpegData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(
                forJPEGSampleBuffer: sampleBuffer,
                previewPhotoSampleBuffer: previewSampleBuffer)
                else {
                    print("Unable to create JPEG data.")
                    completionHandler?(false, nil, nil)
                    return
            }
            
            ////save captured image to Specific Folder
            let imgData = AVCapturePhotoOutput.jpegPhotoDataRepresentation( forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewSampleBuffer)
            if let mydata = imgData {
                let img = UIImage(data: mydata)
                 let data = UIImageJPEGRepresentation(img!, 1) ?? UIImagePNGRepresentation(img!)
               // print((img?.a))
                
                let directory = IncidentUrl.getDirectoryForIncident()
                do {
                    let format = DateFormatter()
                    format.dateFormat="yyyy-MM-dd-HH-mm-ss"
                    try data?.write(to: directory.appendingPathComponent("Img-\(format.string(from: Date())).jpg"))
                    
                } catch {
                    print(error.localizedDescription)
                   
                }
            }
            
            ///////
            var localId: String = ""
            PHPhotoLibrary.shared().performChanges( {
                let creationRequest = PHAssetCreationRequest.forAsset()
                
                creationRequest.addResource(with: PHAssetResourceType.photo, data: jpegData, options: nil)
                localId = (creationRequest.placeholderForCreatedAsset?.localIdentifier)!
            }, completionHandler: { success, error in
                DispatchQueue.main.async {
                    //                    let img = UIImage.init(data: jpegData)
                    let assetResult = PHAsset.fetchAssets(withLocalIdentifiers: [localId], options: nil)
                    let asset = assetResult.firstObject
                    //                    PHImageManager.default().requestImageData(for: asset!, options: nil, resultHandler: { (data, asset, dataUTI, orientation, info) in
                    //                        if let data = data {
                    //                            print("dataUTI")
                    //                            print(dataUTI!)
                    //                            let img = UIImage.init(data: data)
                    //                            completionHandler?(success, asset, img, error)
                    //                        }
                    //                    })
                    completionHandler?(success, asset, error)
                }
            })
        })
    }
    
}

extension CameraViewController {
    
    func tempURL() -> URL? {
        let directory = IncidentUrl.getDirectoryForIncident()
        
        if directory.absoluteString != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return path
        }
        
        return nil
    }
    
    
    func startRecording() {
        
        if captureMovieFileOutput.isRecording == false {
            
            
            self.captureSession.beginConfiguration()
            self.captureSession.sessionPreset = AVCaptureSession.Preset.high
            self.captureSession.commitConfiguration()

            
            let connection = captureMovieFileOutput.connection(with: AVMediaType.video)
            if (connection?.isVideoOrientationSupported)! {
                if let videoPreviewLayerOrientation = previewView.videoPreviewLayer.connection?.videoOrientation{
                    connection?.videoOrientation = videoPreviewLayerOrientation
                }

 //currentVideoOrientation()
            }
            
            if (connection?.isVideoStabilizationSupported)! {
                connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }
            
            let device = activeCameraInput.device
            if (device.isSmoothAutoFocusSupported) {
                do {
                    try device.lockForConfiguration()
                    device.isSmoothAutoFocusEnabled = false
                    device.unlockForConfiguration()
                } catch {
                    print("Error setting configuration: \(error)")
                }
                
            }
            
           
            //EDIT2: And I forgot this
            videoOutputURL = tempURL()!

//            captureMovieFileOutput.maxRecordedDuration =  CMTime.init(value: 1 + videoLimit , timescale: .second)
//            aMovieFileOutput.minFreeDiskSpaceLimit = <#An appropriate minimum given the quality of the movie format and the duration#>;
            

            
            captureMovieFileOutput.startRecording(to: videoOutputURL, recordingDelegate: self)
            
        }
        else {
            stopRecording()
        }
        
    }
    
    func stopRecording() {
        
        if captureMovieFileOutput.isRecording == true {
            captureMovieFileOutput.stopRecording()
        }
    }
    
    func addAsset(asset:PHAsset!){
        if(asset == nil){
            self.displayToast(message: "*** Error generating video")
            return
        }
        if(asset.mediaType == .image){
            PHImageManager.default().requestImageData(for: asset, options: nil, resultHandler: { (data, dataUTI, orientation, info) in
                if let data = data {
                    print("dataUTI")
                    print(dataUTI!)
                    let img = UIImage.init(data: data)
                    DispatchQueue.main.async {
                        self.images.append(img!)
                        self.currentSelectedImages += 1
                        self.selectedAssets.append(asset)
                    }
                    
                }
            })
        }
        else if(asset.mediaType == .video){
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: asset, options: options, resultHandler: {(asset1: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset1 as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    let img = self.addThumbnail(path: localVideoUrl)
                    self.currentSelectedImages += 1
                    DispatchQueue.main.async {
                        self.images.append(img!)
                    }
                    
                    self.selectedAssets.append(asset)
                }
                else{
                    self.displayToast(message: "*** Error generating video")
                }
            })
            
        }
        
    }
    
    func addThumbnail(path: URL)->UIImage!{
        do {
            
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            self.displayToast(message: "*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
            
        }
    }

}

extension CameraViewController : AVCaptureFileOutputRecordingDelegate{
    
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        
        // Detecting the flash
        if (self.videoCaptureDevice?.hasTorch) ?? false {
            // lock your device for configuration
            do {
                let abv = try self.videoCaptureDevice?.lockForConfiguration()
                
                if (userDidSetFlashEnabled){
                    if self.videoCaptureDevice?.isTorchModeSupported(.on) ?? false{
                        try self.videoCaptureDevice?.setTorchModeOn(level: 1.0)
                    }
                } else{
                    if self.videoCaptureDevice?.isTorchModeSupported(.off) ?? false{
                        self.videoCaptureDevice?.torchMode = .off
                    }
                }
                // unlock your device
                self.videoCaptureDevice?.unlockForConfiguration()
            } catch {
                print("aaaa")
            }
        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        
        // Detecting the flash
        if (self.videoCaptureDevice?.hasTorch) ?? false {
            // lock your device for configuration
            do {
                let abv = try self.videoCaptureDevice?.lockForConfiguration()
                self.videoCaptureDevice?.torchMode = .off
                // unlock your device
                self.videoCaptureDevice?.unlockForConfiguration()
            } catch {
                print("Torch could not be turned off ")
            }
        }
        
        
        if (error != nil) {
            print("Error recording movie: \(error!.localizedDescription)")
        } else {

            _ = videoOutputURL as URL
            var localId: String = ""
            
            PHPhotoLibrary.shared().performChanges( {
                let creationRequest = PHAssetCreationRequest.forAsset()
                creationRequest.addResource(with: .video, fileURL: self.videoOutputURL, options:nil)
                localId = (creationRequest.placeholderForCreatedAsset?.localIdentifier)!
            },completionHandler: { success, error in
                DispatchQueue.main.async {
                let assetResult = PHAsset.fetchAssets(withLocalIdentifiers: [localId], options: nil)
                let asset = assetResult.firstObject

                    self.addAsset(asset: asset!)
                }
            })

        }
//        self.videoOutputURL = nil
    }

    
    
}

extension CameraViewController : CameraImageViewDelegte{
    func imageIsRemoved(img:UIImage , index:Int) {
        self.images.remove(at: index)
        self.selectedAssets.remove(at: index)
        self.currentSelectedImages -= 1
    }
    
    func generateImgName() ->String {
        var imgName:String?
        let defaults = UserDefaults.standard
        let number = defaults.object(forKey: "imgNumber")
        if let imgNumber = number {
            var imgNo = imgNumber as! Int
            imgNo = imgNo + 1
            imgName = "Img" + String(imgNo)
            defaults.set(imgNo, forKey: "imgNumber")
            defaults.synchronize()
        }else {
            imgName = "Img" + String(0)
            defaults.set(0, forKey: "imgNumber")
            defaults.synchronize()
        }
        return imgName!
    }
}






