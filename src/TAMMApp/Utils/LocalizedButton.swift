//
//  LocalizedButton.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift

class LocalizedButton: UIButton, CustomFontComponent {
    
    var disposeBag = DisposeBag()
    @IBInspectable
    var AllRounded: Bool = false{
        didSet{
            if self.AllRounded{
                self.addRoundAllCorners(radious: self.Radius)
            }else{
                self.addRoundCorners(radious: self.Radius)
            }
        }
    }
    @IBInspectable
    var HasBorder: Bool = false{
        didSet{
            if self.HasBorder{
                self.layer.borderWidth = 1
            }
        }
    }
    @IBInspectable
    var Radius: CGFloat = 0{
        didSet{
            if self.AllRounded{
                self.addRoundAllCorners(radious: self.Radius)
            }else{
                self.addRoundCorners(radious: self.Radius)
            }
        }
    }
    @IBInspectable
    var BorderColor: UIColor = UIColor.clear{
        didSet{
            
            self.layer.borderColor = self.BorderColor.cgColor
            
        }
    }
    
    
    
    
    var attributedTextProxy: NSAttributedString?{
        get{
            return  titleLabel?.attributedText
        }
        set{
             titleLabel?.attributedText = newValue
        }
    }
    
    var textProxy: String?{
        return titleLabel?.text
    }
    
    var lineBreakModeProxy: NSLineBreakMode?{

        return titleLabel?.lineBreakMode
    }

    var textAlignment: NSTextAlignment{
        return titleLabel?.textAlignment ?? NSTextAlignment.natural
    }
    
    var fontProxy: UIFont?{
        get{
            return self.titleLabel?.font
        }
        set {
            self.titleLabel?.font = newValue
        }
    }
    
    @IBInspectable
    var applyFontScalling: Bool = false
    
    @IBInspectable var RTLFont:String = "Swissra-Normal"
    private func commonInit(){
        if self.AllRounded{
            self.addRoundAllCorners(radious: self.Radius)
        }else{
            self.addRoundCorners(radious: self.Radius)
        }
    }
    var originalFont: UIFont?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        print(fontProxy?.fontName)
        registerTextObserver()
        self.originalFont = fontProxy
        commonInit()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        self.adjustAlignment(RTL: isRTL)
//        self.adjustFont(RTL: isRTL)
    }
    
    fileprivate func adjustAlignment(RTL:Bool){
        if(RTL){
            if(self.titleLabel?.textAlignment != .center){
                self.titleLabel?.textAlignment = .right
            }
        }
        else{
            if(self.titleLabel?.textAlignment != .center){
                self.titleLabel?.textAlignment = .left
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        adjustFont()
    }
    
    fileprivate func registerTextObserver() {
        (UIApplication.shared.delegate as! AppDelegate).isTraitChanged.asObservable().subscribe(onNext: { [unowned self] (_) in
            
            self.adjustFont()
            
            
        }).disposed(by: disposeBag)
        self.addObserver(self, forKeyPath: #keyPath(titleLabel.text), options: [.old, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(titleLabel.text) {
            adjustFont()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: #keyPath(titleLabel.text) )
    }
}

