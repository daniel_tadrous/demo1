//
//  IncidentUrl.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/12/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class IncidentUrl {
    
    class func getDirectoryForIncident () -> URL {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("Incident", isDirectory: true)
        if !isIncidentFileExists() {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
        }
        return dataPath
    }
    

    
    
    
    
    
    fileprivate class func isIncidentFileExists() -> Bool {
        var isDir : ObjCBool = false
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("Incident", isDirectory: true).absoluteString
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path, isDirectory:&isDir) {
            return true
        } else {
            return false
        }
    }
}
