//
//  MiniPlayerView.swift
//  TAMMApp
//
//  Created by kerolos on 6/27/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import Photos

protocol MiniPlayerViewDelegate: class {
    
    func miniPlayerDidPressDelete()
    
    func miniPlayerDidStartPlaying()
    
    func miniPlayerDidStopPlaying()
    
    func miniPlayerDidChangeProgress(progress: Double, of: Double)
    
    
}
// making the functions optional
extension MiniPlayerViewDelegate {
    func miniPlayerDidStartPlaying(){}
    func miniPlayerDidStopPlaying(){}
    func miniPlayerDidChangeProgress(progress: Double, of: Double){}
}


class MiniPlayerView: UIView {

    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var timeLabel: UILabel!
    let playIconString = "\u{e020}"
    let pauseIconString = "\u{e01f}"

    
    var miniPlayerVoiceURL: URL?
    weak var delegate: MiniPlayerViewDelegate?
    
    private let voicePlayer = VoicePlayer()
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateDrawings()
        
        
        
    }
    
    private func updateDrawings(){
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.9098039216, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        self.layer.cornerRadius = 5
        playButton.layer.cornerRadius = playButton.bounds.width / 2
        deleteButton.layer.cornerRadius = deleteButton.bounds.width / 2
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
        voicePlayer.delegate = self
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        progressView?.progress = 0
        setPlayIcon()
    }
    
    func setPlayIcon(isPlaying:Bool = false){
        var icon = playIconString
        if isPlaying{
            icon = pauseIconString
        }
        self.playButton.setTitle(icon, for: .highlighted)
        self.playButton.setTitle(icon, for: .normal)
        self.playButton.setTitle(icon, for: .focused)
        self.playButton.setTitle(icon, for: .selected)
    }
    func play(){
        print(#function)
        guard let url = self.miniPlayerVoiceURL else { return }

        self.voicePlayer.delegate = self
        self.voicePlayer.play(url: url)
        setPlayIcon(isPlaying: true)
        
//        let options: PHVideoRequestOptions = PHVideoRequestOptions()
//        options.version = .original
//        options.isNetworkAccessAllowed = true
//        asset
//        PHImageManager.default().
        
//        PHImageManager.default().request
//        PHImageManager.default().requestAVAsset(forVideo: assetIn, options: options, resultHandler: { (asset, audioMix, info) in
//            if let urlAsset = asset as? AVURLAsset {
//                let localVideoUrl = urlAsset.url
//
//                self.voicePlayer.delegate = self
//                self.voicePlayer.play(url: localVideoUrl)
//
//
//
//            } else {
//                print("could not get asset")
//            }
//        })
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate",
                                                         ascending: false)]
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d || mediaType == %d",
                                             PHAssetMediaType.image.rawValue,
                                             PHAssetMediaType.video.rawValue)
        fetchOptions.fetchLimit = 100
        
        let imagesAndVideos = PHAsset.fetchAssets(with: fetchOptions)

    }
    
    func stop(){
        
        voicePlayer.stop()
        setPlayIcon(isPlaying: false)
    }
    
    @IBAction func playIsPressed(_ sender: Any){
        if voicePlayer.isPlaying(){
            stop()
        }else{
            play()
        }
    }
    
    @IBAction func deleteIsPressed(_ sender: Any) {
        stop()
        delegate?.miniPlayerDidPressDelete()
        
    }
}

extension MiniPlayerView: VoicePlayerDelegate{
    func voicePlayerDidStartPlaying() {
        delegate?.miniPlayerDidStartPlaying()
    }
    
    func voicePlayerProgress(progress: Double, of: Double) {
        
        progressView.progress  = Float(progress / of)
        timeLabel.text = "\(Int( progress / 60) ):" + String(format: "%02d",Int(progress) % 60)
        
        delegate?.miniPlayerDidChangeProgress(progress: progress, of: of)
        
        
    }
    
    func voicePlayerDidStopPlaying() {
        setPlayIcon(isPlaying: false)
        delegate?.miniPlayerDidStopPlaying()
    }
    
    
}


