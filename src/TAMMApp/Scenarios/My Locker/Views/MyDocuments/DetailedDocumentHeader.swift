//
//  DetailedDocumentHeader.swift
//  TAMMApp
//
//  Created by Mohamed elshiekh on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

enum SectionStatus:String{
    case open
    case closed
}

class DetailedDocumentHeader:  UITableViewHeaderFooterView{

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var expandLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var sectionStatus:SectionStatus = .closed //to be moved to the model
    
    var onClick:(()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
        
    }

    @IBAction func expandAction(_ sender: Any) {
        onClick?()
    }
    
    func commonInit() {
        
        contentView.backgroundColor = UIColor.gray
        expandLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi/(-2.0))
    }
    func rotateExpandLabel(_ status:Bool){
        switch status {
        case false:
            expandLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi/(-2.0))
        default:
            expandLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi/(2.0))

        }
    }
}
