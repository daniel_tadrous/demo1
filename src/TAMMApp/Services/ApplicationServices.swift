//
//  ApplicationServices.swift
//  TAMMApp
//
//  Created by kerolos on 4/18/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit

enum TammSupportedLanguages: String{
    case arabic = "ar"
    case english = "en"
}

protocol UserConfigServiceType{
    func getUserId() -> Int
    func getDrafts() -> [DraftViewModelData]
    func addDraft(draft:DraftViewModelData)
    func changeDrafts(drafts:[DraftViewModelData])
    var isFirstLaunch: Bool {get set}
    
    func changeLanguage(_ : TammSupportedLanguages)
    
    func getCurrentLanguageLitral() -> String
    func getLocationServicesStatus() -> Bool
    func getCalanderSyncStatus() -> Bool
    
    func setCalanderSyncStatus(isOn:Bool)
    func getCurrentLanguage() -> TammSupportedLanguages
    func getLanguageLitral(lang: TammSupportedLanguages) -> String 
    static func getFontAccessibilityMultiplier() -> Int
    func setFontAccessibilitySize(size:CGFloat)
}

class UserConfigService :UserConfigServiceType{
    
    
    private static let accessibilityFontKey = "accessibilityFontKey"
    
    func setFontAccessibilitySize(size: CGFloat) {
        UserConfigService.setUserDefault(UserConfigService.accessibilityFontKey, size)
    }
    static func getFontAccessibilityMultiplier() -> Int {
        return (UserConfigService.getUserDefault(UserConfigService.accessibilityFontKey) as? Int) ?? 7
    }
    
    func getLocationServicesStatus() -> Bool {
        return false
    }
    
    func getCalanderSyncStatus() -> Bool {
        if let isOn = UserConfigService.getUserDefault("syncCalender") as? Bool{
            return isOn
        }else{
            return true
        }
    }
    
    func setCalanderSyncStatus(isOn:Bool){
        UserConfigService.setUserDefault("syncCalender", isOn)
    }
    
    func getCurrentLanguage() -> TammSupportedLanguages {
        if(L102Language.currentAppleLanguage().contains("ar")){
            return .arabic
        }
        return .english
    }
    
    func getCurrentLanguageLitral() -> String {
        if(L102Language.currentAppleLanguage().contains("ar")){
            return L10n.Preferences.arabic
        }
        return L10n.Preferences.english
    }
    
    func getLanguageLitral(lang: TammSupportedLanguages) -> String {
        switch lang {
            
        case .arabic:
            return L10n.Preferences.arabic

        case .english:
            return L10n.Preferences.english

        }
    }
    
    
    func changeLanguage(_ lang: TammSupportedLanguages) {
        switch lang{
        case .arabic:
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        case .english:
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
    }
    
    private static let isFirstLaunchKey = "isFirstLaunchKey"
    var isFirstLaunch: Bool{
        get {
            guard let val =  UserConfigService.getUserDefault(UserConfigService.isFirstLaunchKey) as? Bool else{
                return true
            }
            return val
        }
        set{
            UserConfigService.setUserDefault(UserConfigService.isFirstLaunchKey, newValue)
        }
    }
    

    
    private static func setUserDefault(_ key: String, _ item: Any){
        // for production usage consider using the OS Keychain instead
        let archivedOrigin = NSKeyedArchiver.archivedData(withRootObject: item)
        UserDefaults.standard.set(archivedOrigin, forKey: key)
        UserDefaults.standard.synchronize()
    }
    private static func getUserDefault( _ key: String) -> Any?{
        // loads OIDAuthState from NSUSerDefaults
        guard let archivedOrigin = UserDefaults.standard.object(forKey: key) as? Data else{
            return nil
        }
        guard let Origin = NSKeyedUnarchiver.unarchiveObject(with: archivedOrigin) else{
            return nil
        }
        return Origin
    }
    
    
    init() {
        //remove read drafts
//        var items = getDrafts()
//        var i = 0
//        for item in items {
//            if(item.isRead){
//                items.remove(at: i)
//            }
//            else{
//                i += 1
//            }
//        }
//        let archivedOrigin = NSKeyedArchiver.archivedData(withRootObject: items)
//        UserDefaults.standard.set(archivedOrigin, forKey: "Drafts")
//
//        UserDefaults.standard.synchronize()
    }
    //TODO should get the system userId
    func getUserId() -> Int{
        return 1
    }
    
    func getDrafts() -> [DraftViewModelData]{
        // loads OIDAuthState from NSUSerDefaults
        guard let archivedOrigin = UserDefaults.standard.object(forKey: "Drafts") as? Data else{
            return []
        }
        
        guard let Origin = NSKeyedUnarchiver.unarchiveObject(with: archivedOrigin) as? [DraftViewModelData] else{
            return []
        }
        
        return Origin
    }
    func addDraft(draft:DraftViewModelData){
        // for production usage consider using the OS Keychain instead
        var items = getDrafts()
        items.append(draft)
        let archivedOrigin = NSKeyedArchiver.archivedData(withRootObject: items)
        UserDefaults.standard.set(archivedOrigin, forKey: "Drafts")
        
        UserDefaults.standard.synchronize()
    }
    
    func changeDrafts(drafts:[DraftViewModelData]){
        let archivedOrigin = NSKeyedArchiver.archivedData(withRootObject: drafts)
        UserDefaults.standard.set(archivedOrigin, forKey: "Drafts")
        
        UserDefaults.standard.synchronize()
    }
}
