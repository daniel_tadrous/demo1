//
//  IntroLanguageSwitchViewController.swift
//  TAMMApp
//
//  Created by kerolos on 5/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit


protocol IntroLanguageSwitchViewControllerDelegate: class {
    func introLanguageSelectionIsDone()
}

class IntroLanguageSwitchViewController: TammViewController, IntroScreensStoryboardLoadable {

    weak var delegate: IntroLanguageSwitchViewControllerDelegate?
    var viewModel: IntroLanguageSwitchViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arEnSpaceConstraint.constant = 0
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var englishButton: UITammResizableButton!
    @IBOutlet weak var arabicButton: UITammResizableButton!
    
    @IBOutlet weak var arEnSpaceConstraint: NSLayoutConstraint!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()

        englishButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        arabicButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        englishButton.setTitleColor(#colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .highlighted)
        arabicButton.setTitleColor(#colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .highlighted)
        
        englishButton.setTitleColor(#colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .selected)
        arabicButton.setTitleColor(#colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1), for: .selected)
        
        englishButton.adjustsImageWhenHighlighted = false
        arabicButton.adjustsImageWhenHighlighted = false
        
        englishButton.reversesTitleShadowWhenHighlighted = false

        arabicButton.reversesTitleShadowWhenHighlighted = false
        UIView.animate(withDuration: TimeInterval(Dimensions.introArEnMovingAnemationDurations)) { [weak self] in
            self?.arEnSpaceConstraint.constant = 25
            self?.view.layoutIfNeeded()
            
        }
        
    }
    @IBAction func touchDown(_ sender: UITammResizableButton) {
        sender.titleLabel?.alpha = 1.0
        
        sender.addRoundCorners(radious: sender.referenceHeigh * UIFont.fontSizeMultiplier / 2, borderColor: #colorLiteral(red: 0, green: 0.6117647059, blue: 0.737254902, alpha: 1))
        
    }
    @IBAction func touchCancel(_ sender: UITammResizableButton) {

        sender.titleLabel?.alpha = 1.0
        sender.roundCorners(radius: sender.referenceHeigh * UIFont.fontSizeMultiplier / 2)
    }
    
    @IBAction func touchUpOutside(_ sender:
        UITammResizableButton) {
        sender.titleLabel?.alpha = 1.0
        sender.roundCorners(radius: sender.referenceHeigh * UIFont.fontSizeMultiplier / 2)

    }
    
    fileprivate func applyRoundCorners() {
        englishButton.roundCorners( radius: englishButton.referenceHeigh * UIFont.fontSizeMultiplier / 2)
        arabicButton.roundCorners(radius: arabicButton.referenceHeigh * UIFont.fontSizeMultiplier / 2)
    }
    
    override func viewWillLayoutSubviews() {
        applyRoundCorners()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        applyRoundCorners()
    }
    
    @IBAction func btnEnglishClicked(_ sender: UITammResizableButton) {
        viewModel.switchToLanguage(.english)
        delegate?.introLanguageSelectionIsDone()
//        sender.roundCorners(radius: sender.referenceHeigh * UIFont.fontSizeMultiplier / 2)

    }
    
    @IBAction func btnArabicClicked(_ sender: UITammResizableButton) {
        viewModel.switchToLanguage(.arabic)
        delegate?.introLanguageSelectionIsDone()
//        sender.roundCorners(radius: sender.referenceHeigh * UIFont.fontSizeMultiplier / 2)

    }
    
    @IBAction func touchDragExit(_ sender: UITammResizableButton) {
        sender.roundCorners(radius: sender.referenceHeigh * UIFont.fontSizeMultiplier / 2)

    }
    
}
