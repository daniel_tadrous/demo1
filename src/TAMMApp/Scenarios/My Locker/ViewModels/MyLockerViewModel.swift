//
//  MyLockerViewModel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class MyLockerViewModel{
    private var MyLockerSections:[MyLockerSection] = []
    init() {
        self.FillMyLockerSections()
    }
    
    func getMyLockerSectionsCount() -> Int{
        self.FillMyLockerSections()
        return self.MyLockerSections.count;
    }
    
    func getMyLockerSectionIcon(index:Int) -> String{
        return self.MyLockerSections[index].icon;
    }
    
    func getMyLockerSectionIconColor(index:Int) -> String{
        return self.MyLockerSections[index].iconHexColor;
    }
    
    func getMyLockerSectionIconBackgroundColor(index:Int) -> String{
        return self.MyLockerSections[index].iconHexBackgroundColor;
    }
    func getMyLockerSectionName(index:Int) -> String{
        return self.MyLockerSections[index].name;
    }
    
    private func FillMyLockerSections(){
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.MyLockerSections.append(MyLockerSection(type: .MyPayemnts, icon: String.MyPayemntsIconString, iconHexColor: String.MyPayemntsIconColorString, iconHexBackgroundColor: String.MyPayemntsIconBackgroundColorString, name: "مدفوعاتي"))
            
            self.MyLockerSections.append(MyLockerSection(type: .MySubscription, icon: String.MySubscriptionIconString, iconHexColor: String.MySubscriptionIconColorString, iconHexBackgroundColor: String.MySubscriptionIconBackgroundColorString, name: "اشتراكاتي"))
            
            self.MyLockerSections.append(MyLockerSection(type: .MyEvents, icon: String.MyEventsIconString, iconHexColor: String.MyEventsIconColorString, iconHexBackgroundColor: String.MyEventsIconBackgroundColorString, name: "فعالياتي"))
            
            self.MyLockerSections.append(MyLockerSection(type: .MySupport, icon: String.MySupportIconString, iconHexColor: String.MySupportIconColorString, iconHexBackgroundColor: String.MySupportIconBackgroundColorString, name: "مركز الدعم"))
            
            self.MyLockerSections.append(MyLockerSection(type: .MyDocuments, icon: String.MyDocumentsIconString, iconHexColor: String.MyDocumentsColorIconString, iconHexBackgroundColor: String.MyDocumentsBackgroundColorIconString, name: "وثائقي"))
        }
        else{
            self.MyLockerSections.append(MyLockerSection(type: .MyPayemnts, icon: String.MyPayemntsIconString, iconHexColor: String.MyPayemntsIconColorString, iconHexBackgroundColor: String.MyPayemntsIconBackgroundColorString, name: "My Payments"))
        
            self.MyLockerSections.append(MyLockerSection(type: .MySubscription, icon: String.MySubscriptionIconString, iconHexColor: String.MySubscriptionIconColorString, iconHexBackgroundColor: String.MySubscriptionIconBackgroundColorString, name: "My Subscription"))
        
            self.MyLockerSections.append(MyLockerSection(type: .MyEvents, icon: String.MyEventsIconString, iconHexColor: String.MyEventsIconColorString, iconHexBackgroundColor: String.MyEventsIconBackgroundColorString, name: "My Events"))
        
            self.MyLockerSections.append(MyLockerSection(type: .MySupport, icon: String.MySupportIconString, iconHexColor: String.MySupportIconColorString, iconHexBackgroundColor: String.MySupportIconBackgroundColorString, name: "My Support"))
        
            self.MyLockerSections.append(MyLockerSection(type: .MyDocuments, icon: String.MyDocumentsIconString, iconHexColor: String.MyDocumentsColorIconString, iconHexBackgroundColor: String.MyDocumentsBackgroundColorIconString, name: "My Documents"))
        }
        
    }
    
}

struct MyLockerSection{
    var type:MyLockerTypes
    var icon:String;
    var iconHexColor:String
    var iconHexBackgroundColor:String
    var name:String;
}

enum MyLockerTypes{
    case MyPayemnts
    case MySubscription
    case MyEvents
    case MySupport
    case MyDocuments
}

extension String{
    public static var MyPayemntsIconString: String{
        return "f"
    }
    public static var MySubscriptionIconString: String{
        return "c"
    }
    public static var MyEventsIconString: String{
        return "i"
    }
    public static var MySupportIconString: String{
        return "b"
    }
    public static var MyDocumentsIconString: String{
        return "j"
    }
    // Font Color
    public static var MyPayemntsIconColorString: String{
        return "#F6891B"
    }
    public static var MySubscriptionIconColorString: String{
        return "#7CB879"
    }
    public static var MyEventsIconColorString: String{
        return "#004154"
    }
    public static var MySupportIconColorString: String{
        return "#EC8570"
    }
    public static var MyDocumentsColorIconString: String{
        return "#FDB014"
    }
    public static var CoCreationColorIconString: String{
        return "#00abc5"
    }
    
    // Background Color
    public static var MyPayemntsIconBackgroundColorString: String{
        return "#FEECD8"
    }
    public static var MySubscriptionIconBackgroundColorString: String{
        return "#EAF4EA"
    }
    public static var MyEventsIconBackgroundColorString: String{
        return "#D4E2E7"
    }
    public static var MySupportIconBackgroundColorString: String{
        return "#EFDFDB"
    }
    public static var MyDocumentsBackgroundColorIconString: String{
        return "#FFF3D7"
    }
    public static var CoCreationBackgroundColorIconString: String{
        return "#d9f3f7"
    }
}

