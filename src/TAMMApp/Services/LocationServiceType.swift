//
//  LocationServiceType.swift
//  TAMMApp
//
//  Created by Daniel on 7/31/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationServiceTypeDelegate: class {
    func onLocationUpdated(location: CLLocation)
}

protocol LocationServiceType {
    func start(delegate:LocationServiceTypeDelegate)
}
