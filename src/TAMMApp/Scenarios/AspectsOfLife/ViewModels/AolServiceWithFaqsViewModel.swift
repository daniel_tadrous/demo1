//
//  AolServiceWithFaqsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/10/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AolServiceWithFaqsViewModel{
    
    //    // Just set aspectId.value and the api will be called
    static var serviceIdStatic:String?
    private(set) var serviceId: Variable<String?> = Variable(nil)
    //    private(set) var aspectTitle: Variable<String?> = Variable(nil)
    private(set) var viewData: Variable<ServiceModel?> = Variable(nil)
    private(set) var servicePopupModel: Variable<ServicePopupModel?> = Variable(nil)
    
    fileprivate var apiService: ApiServiceAspectsOfLifeType
    
    fileprivate var disposeBag = DisposeBag()
    
    static var serviceTitleStatic:String?
    private(set) var serviceTitle: Variable<String?> = Variable(nil)
    
    private(set) var serviceDescription: Variable<String?> = Variable(nil)
    
    var isInformationHelpfulCellExpanded = false
    var isYesSelected: Bool?
    var comment: String?
    
    private(set) var popupType: Variable<PopupType?> = Variable(nil)
    
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        serviceId.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        popupType.asObservable().subscribe(onNext:{ [unowned self] popupTypeE in
            self.requestPopupBody()
        })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if serviceId.value != nil && !serviceId.value!.isEmpty{
            apiService.getAspectOfLifeServiceWithFaqs(id: serviceId.value!)
                .subscribe(onNext: { [unowned self] serviceDetail in
                    self.viewData.value = serviceDetail
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func requestPopupBody(){
        if serviceId.value != nil && popupType.value != nil {
            apiService.getServicePopupContent(popupType: popupType.value!, id: serviceId.value!)
                .subscribe(onNext: { [unowned self] content in
                    self.servicePopupModel.value = content
                }).disposed(by: disposeBag)
        }else{
            self.servicePopupModel.value  = nil
        }
    }
    func submitFeedback(comment:String,isYesSelected:Bool){
        
            apiService.submitFeedback(comments: comment, helpful: isYesSelected, id: serviceId.value!)
                .subscribe(onNext: { [unowned self] emptyRes in
                    
                }).disposed(by: disposeBag)
        
    }
}
