//
//  MySupportMainViewController.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/23/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol MySupportMainViewControllerDelegate: class {
    func talkToUsClicked()
    func checkStatusClicked()
    func faqClicked()
}

class MySupportMainViewController: TammViewController , MySupportMainStoryboardLodable{

    weak var delegate: MySupportMainViewControllerDelegate?
    
    var viewModel: MySupportMainViewModel?

    //@IBOutlet weak var contentViewHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var spaceBetweenEmergencyNumbers: NSLayoutConstraint!
    @IBOutlet weak var leadingSpaceEmergencyNumber: NSLayoutConstraint!
    @IBOutlet weak var TalkNowBtn: UITammButton!
    @IBOutlet weak var TalkToUsIcon: UICircularImage!
    @IBOutlet weak var TalkToUsView: UIView!
    @IBOutlet weak var FAQView: UIView!
    @IBOutlet weak var CheckStatusView: UIView!
    @IBOutlet weak var CheckStatusBtn: UITammButton!
    @IBOutlet weak var CantFindView: UIView!
    @IBOutlet weak var callUsLine1Label: UILabel!
    @IBOutlet weak var callUsLine2Label: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var EmergencyNumbersView: UIView!
    @IBOutlet weak var electricityAuthoritylbl: LocalizedLabel!
    @IBOutlet weak var ambulanceLbl: LocalizedLabel!
    @IBOutlet weak var abuDhabiPoliceLbl: LocalizedLabel!
    @IBOutlet weak var emergencyIcon: UILabel!
    
    @IBOutlet weak var viewAllBtn: LocalizedButton!
    
    @IBOutlet weak var civilDefenseNumberLbl: LocalizedLabel!
    
    @IBOutlet weak var civilDefenseLbl: LocalizedLabel!
    
    @IBOutlet weak var electricityNumberLbl: LocalizedLabel!
    @IBOutlet weak var policeNumberLbl: LocalizedLabel!
    @IBOutlet weak var ambulanceNumberLbl: LocalizedLabel!
    fileprivate var layoutManager1:NSLayoutManager!
    fileprivate var textContainer1:NSTextContainer!
    fileprivate var textStorage1:NSTextStorage!
    fileprivate var layoutManager2:NSLayoutManager!
    fileprivate var textContainer2:NSTextContainer!
    fileprivate var textStorage2:NSTextStorage!
    fileprivate var emailUsRange:NSRange!
    fileprivate var chatWithUsRange:NSRange!
    fileprivate var phone1Range:NSRange!
    fileprivate var phone2Range:NSRange!
    override func viewDidLoad() {
        // setting how the Email will be presented
        viewModel!.emailServicePresenter = self
        
        self.title = L10n.myLocker
        self.setNavigationBar(title: L10n.support)
        self.FAQView.addRoundCorners(radious: 8, shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.15))
        self.TalkToUsView.addRoundCorners(radious: 8, shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.15))
        self.CheckStatusView.addRoundCorners(radious: 8, shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.15))
        self.CantFindView.addRoundCorners(radious: 8, shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.15))
        self.EmergencyNumbersView.addRoundCorners(radious: 8, shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.15))
        
       self.viewAllBtn.addRoundCorners(radious: 20, borderColor: UIColor.turquoiseBlue)
       
        self.policeNumberLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(policeNumberClicked(_:))))
        
            self.emergencyIcon.text = "\u{e018}"
        self.ambulanceNumberLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ambulanceNumberClicked(_:))))
        self.civilDefenseNumberLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(civilDefenseNumberClicked(_:))))
        
        self.electricityNumberLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(electricityNumberClicked(_:))))
        self.FAQView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(fAQViewClicked(_:))))
        self.callUsLine1Label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callUsLine1LabelClicked(_:))))
        self.callUsLine2Label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callUsLine2LabelClicked(_:))))
        //self.TalkToUsIcon.layer.

    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        let popup = UIApplication.shared.keyWindow?.subviews.first(where: { (view) -> Bool in
//            return view.tag == 1001
//        })
//        if popup != nil{
//            UIApplication.shared.keyWindow?.bringSubview(toFront: popup!)
//        }
//    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.textContainer1.size = self.callUsLine1Label.bounds.size;
        self.textContainer2.size = self.callUsLine2Label.bounds.size;
        self.scrollView.resizeToFitContent()
        self.emergencyIcon.addRoundAllCorners(radious: self.emergencyIcon.frame.width/2)
        self.calculateEmergencyViewDistances()
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        self.setCallUsLabel()
        self.TalkNowBtn.roundCorners(radius: 17 * UIFont.fontSizeMultiplier)
        self.CheckStatusBtn.roundCorners(radius: 17 * UIFont.fontSizeMultiplier)
//        let popup = UIApplication.shared.keyWindow?.subviews.first(where: { (view) -> Bool in
//            return view.tag == 1001
//        })
//        if popup != nil{
//            UIApplication.shared.keyWindow?.bringSubview(toFront: popup!)
//        }
    }
  
    fileprivate func calculateEmergencyViewDistances(){
        let w1 = (self.abuDhabiPoliceLbl.frame.width > self.civilDefenseLbl.frame.width ? self.abuDhabiPoliceLbl.frame.width : self.civilDefenseLbl.frame.width)
        
        let w2 =  (self.electricityAuthoritylbl.frame.width > self.ambulanceLbl.frame.width ? self.electricityAuthoritylbl.frame.width : self.ambulanceLbl.frame.width)
        
        let fullWidth  =  self.EmergencyNumbersView.frame.width
        var restOfDistance = fullWidth - w1 - w2
        var betweenSpace:CGFloat = 20
        var space  =  restOfDistance / 3
        if(space > betweenSpace){
            betweenSpace = space
        }
        else{
            restOfDistance = restOfDistance - betweenSpace
            space =  restOfDistance / 2
        }
        
        self.leadingSpaceEmergencyNumber.constant = space
        self.spaceBetweenEmergencyNumbers.constant =  space
    
    }
    fileprivate func setCallUsLabel(){
        let emailUsText = L10n.emailUs
        let chatWithUs = L10n.chatWithUs
        let phone1 = L10n.phone1
        let phone2 = L10n.phone2
        let isRTL = L102Language.currentAppleLanguage().contains("ar")
        let fontSize = (14.0 * UIFont.fontSizeMultiplier)
        
        let contactUsMessage1 = L10n.getContactUsLine1(emailUs: emailUsText, chatWithUs: chatWithUs) as NSString
        let contactUsMessage2 = L10n.getContactUsLine2(phone1: phone1, phone2: phone2 )  as NSString
        var fontName = "Roboto-Regular"
        
        if (isRTL){
            fontName = "Swissra-Normal"
        }
        let attributedString1 = NSMutableAttributedString(string: contactUsMessage1 as String, attributes: [
            .font: UIFont(name: fontName, size: fontSize)!,
            .foregroundColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1),
            ])
        let attributedString2 = NSMutableAttributedString(string: contactUsMessage2 as String, attributes: [
            .font: UIFont(name: fontName, size: fontSize)!,
            .foregroundColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1),
            ])
       
        let mailRange =  contactUsMessage1.range(of: emailUsText as String)
        attributedString1.addAttributes([
            .font: UIFont(name: fontName, size: fontSize)!,
            .foregroundColor: UIColor.init(red: 0, green: 89/255, blue: 113/255, alpha: 1),
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue
            ], range: mailRange)
        self.emailUsRange = mailRange
        let chatRange =  contactUsMessage1.range(of: chatWithUs as String)
        attributedString1.addAttributes([
            .font: UIFont(name: fontName, size: fontSize)!,
            .foregroundColor: UIColor.init(red: 0, green: 89/255, blue: 113/255, alpha: 1),
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue
            ], range: chatRange)
        
        self.chatWithUsRange = chatRange
        
        var boldfontName = "Roboto-Bold"
        if (isRTL){
            boldfontName = "Swissra-Bold"
        }
        
        let phone1Range = contactUsMessage2.range(of: phone1 as String)
        attributedString2.addAttributes([
            .font: UIFont(name: boldfontName, size: fontSize)!,
            .foregroundColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1),
            ], range: phone1Range)
        self.phone1Range = phone1Range
        
        let phone2Range = contactUsMessage2.range(of: phone2 as String)
        attributedString2.addAttributes([
            .font: UIFont(name: boldfontName, size: fontSize)!,
            .foregroundColor: UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1),
            ], range: phone2Range)
        self.phone2Range = phone2Range
//        if(isRTL){
//            let paragraphStyle = NSMutableParagraphStyle()
//            paragraphStyle.lineSpacing = 1; attributedString.addAttributes([NSAttributedStringKey.paragraphStyle : paragraphStyle], range: NSMakeRange(0, attributedString.length))
//        }
        self.callUsLine1Label.attributedText =  attributedString1
        self.callUsLine2Label.attributedText =  attributedString2
        layoutManager1 = NSLayoutManager()
        // Configure layoutManager and textStorage
        textContainer1 = NSTextContainer(size: CGSize.zero)
        self.textStorage1 = NSTextStorage(attributedString: attributedString1)
        // Configure textContainer
        layoutManager1.addTextContainer(textContainer1)
        textStorage1.addLayoutManager(layoutManager1)
        textContainer1.lineFragmentPadding = 0.0;
        textContainer1.lineBreakMode = self.callUsLine1Label.lineBreakMode;
        textContainer1.maximumNumberOfLines = self.callUsLine1Label.numberOfLines;
        
        layoutManager2 = NSLayoutManager()
        // Configure layoutManager and textStorage
        textContainer2 = NSTextContainer(size: CGSize.zero)
        self.textStorage2 = NSTextStorage(attributedString: attributedString2)
        // Configure textContainer
        layoutManager2.addTextContainer(textContainer2)
        textStorage2.addLayoutManager(layoutManager2)
        textContainer2.lineFragmentPadding = 0.0;
        textContainer2.lineBreakMode = self.callUsLine2Label.lineBreakMode;
        textContainer2.maximumNumberOfLines = self.callUsLine2Label.numberOfLines;
    }
    
    
    @objc func callUsLine1LabelClicked(_ sender:Any){
        let tapGesture = sender as! UITapGestureRecognizer
        let locationOfTouchInLabel = tapGesture.location(in: tapGesture.view)
        let labelSize = tapGesture.view?.bounds.size;
        //self.layoutManager.glyphRange(forCharacterRange: NSRange(location:0, length:(self.CallUsLabel.attributedText?.length)!), actualCharacterRange: nil);
        self.layoutManager1.ensureGlyphs(forGlyphRange: NSRange(location:0, length:(self.callUsLine1Label.attributedText?.length)!))
        let textBoundingBox = self.layoutManager1.usedRect(for: self.textContainer1)
        
        let textContainerOffset = CGPoint(x: ((labelSize?.width)! - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x ,  y: ((labelSize?.height)! - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        var locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x ,  y: locationOfTouchInLabel.y - textContainerOffset.y)
        if(L102Language.currentAppleLanguage().contains("ar")){
            locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x  ,  y: locationOfTouchInLabel.y)
        }
        //CallUsLabel.textc
        let indexOfCharacter = self.layoutManager1.characterIndex(for: locationOfTouchInTextContainer, in: self.textContainer1, fractionOfDistanceBetweenInsertionPoints: nil)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate

        if(NSLocationInRange(indexOfCharacter, self.emailUsRange)){
            print("Email Us")
            if !(viewModel!.sendEmail(onCompleted: {
                appDelegate?.addfloatingMenu()
            }))
            {
                displayToast(message: (viewModel?.mailErrorToast)!)
            }
            else{
                appDelegate?.removeFloatingMenu()
            }
        }
    }
    
    @objc func callUsLine2LabelClicked(_ sender:Any){
        let tapGesture = sender as! UITapGestureRecognizer
        let locationOfTouchInLabel = tapGesture.location(in: tapGesture.view)
        let labelSize = tapGesture.view?.bounds.size;
        //self.layoutManager.glyphRange(forCharacterRange: NSRange(location:0, length:(self.CallUsLabel.attributedText?.length)!), actualCharacterRange: nil);
        self.layoutManager2.ensureGlyphs(forGlyphRange: NSRange(location:0, length:(self.callUsLine2Label.attributedText?.length)!))
        let textBoundingBox = self.layoutManager2.usedRect(for: self.textContainer2)
        
        let textContainerOffset = CGPoint(x: ((labelSize?.width)! - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x ,  y: ((labelSize?.height)! - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        var locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x ,  y: locationOfTouchInLabel.y - textContainerOffset.y)
        if(L102Language.currentAppleLanguage().contains("ar")){
            locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x  ,  y: locationOfTouchInLabel.y)
        }
        //CallUsLabel.textc
        let indexOfCharacter = self.layoutManager2.characterIndex(for: locationOfTouchInTextContainer, in: self.textContainer2, fractionOfDistanceBetweenInsertionPoints: nil)
        
        if(NSLocationInRange(indexOfCharacter, self.phone1Range)){
            print("phone 1 call")
            if !(viewModel!.makeAPhoneCall(phone: .phone1)){
                displayToast(message: "Failed making a phone call")
            }
            
        }
        else if(NSLocationInRange(indexOfCharacter, self.phone2Range)){
            print("phone 2 call")
            if !(viewModel!.makeAPhoneCall(phone: .phone2)){
                displayToast(message: "Failed making a phone call")
            }
            
        }
    }
    
    @objc func fAQViewClicked(_ sender:Any){
        delegate?.faqClicked()
    }
    @objc func policeNumberClicked(_ sender:Any){
        if !(viewModel!.makeAPhoneCall(phone: .police)){
            displayToast(message: "Failed making a phone call")
        }
    }
    
    @objc func ambulanceNumberClicked(_ sender:Any){
        if !(viewModel!.makeAPhoneCall(phone: .ambulance)){
            displayToast(message: "Failed making a phone call")
        }
    }
    
    @objc func civilDefenseNumberClicked(_ sender:Any){
        if !(viewModel!.makeAPhoneCall(phone: .civilDefense)){
            displayToast(message: "Failed making a phone call")
        }
    }
    
    @objc func electricityNumberClicked(_ sender:Any){
        if !(viewModel!.makeAPhoneCall(phone: .electricity)){
            displayToast(message: "Failed making a phone call")
        }
    }
    
    @IBAction func talkNowBtnClicked(_ sender: Any) {
        delegate?.talkToUsClicked()
    }
    
    @IBAction func checkStatusBtnClicked(_ sender: Any) {
        delegate?.checkStatusClicked()
    }
    
    @IBAction func viewAllBtnClicked(_ sender: Any) {
        self.displayToast(message: "View All Clicked")
    }
    
    
    
    
    
    
}

extension MySupportMainViewController: EmailServiceTypePresenter{
    var presenter: UIViewController {
        get{
            return self
        }
    }
    
    
}
