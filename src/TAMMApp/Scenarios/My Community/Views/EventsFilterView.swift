//
//  FaqFilterView.swift
//  TAMMApp
//
//  Created by Daniel on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift
import CoreLocation

class EventsFilterView : UIView{
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var fromTF: LocalizedTextField!
    
    @IBOutlet weak var toTF: LocalizedTextField!
    
    @IBOutlet weak var applyFiltersBtn: LocalizedButton!
 
    @IBOutlet weak var categoryStackView: UIStackView!
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var searchTF: LocalizedTextField!
  
    @IBOutlet weak var toBtn: LocalizedButton!
    @IBOutlet weak var fromBtnClick: LocalizedButton!
   
    var onApplyFilterClicked:((EventsFilterApplyModel)->Void)?
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var disposeBag = DisposeBag()
    var viewModel: FilterEventsViewModelProtocol?
    private var eventsFilterApplyModel = EventsFilterApplyModel()
    private var locations = [String]()
    var selectedIndex = -1
    var dateFormat = "MMM yyyy"
    var currentAddress: Variable<String?> = Variable(nil)
    @IBOutlet weak var locationsStackView: UIStackView!
    @IBOutlet weak var locationStackContainer: UIView!
    
    var toDate = Date()
    var fromDate = Date()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    init(frame: CGRect,viewModel: FilterEventsViewModelProtocol,onApplyFilterClicked:@escaping (EventsFilterApplyModel)->Void){
        super.init(frame: frame)
        self.viewModel = viewModel
        self.onApplyFilterClicked = onApplyFilterClicked
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "EventsFilterView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        okButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelBtnClicked)))
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        
        applyColorTheme()

        self.viewModel?.setLocationQueryValue(q: nil)
        
        setupHooks()
        setupUiData()
    }
    
    @IBOutlet weak var dimmingView: UIView!
    @IBOutlet weak var filterHeaderHoldingView: UIView!
    @IBOutlet weak var closeIconLabel: UILabel!
    @IBOutlet weak var filterLabel: LocalizedLabel!
    @IBOutlet weak var fromLabel: LocalizedLabel!
    @IBOutlet weak var toLabel: LocalizedLabel!
    
    @IBOutlet weak var dropinIconLabel: UILabel!
    @IBOutlet weak var magnifierIconLabel: UILabel!
    
    func applyColorTheme(){
        self.mainView.addRoundCorners(radious: 8, borderColor: UIColor.paleGrey.getAdjustedColor())
        
        dimmingView.backgroundColor = #colorLiteral(red: 0.08255860955, green: 0.07086443156, blue: 0.2191311121, alpha: 1).getAdjustedColor()
        mainView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        filterHeaderHoldingView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        closeIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2191311121, alpha: 1).getAdjustedColor()
        filterLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).getAdjustedColor()
        applyFiltersBtn.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
        applyFiltersBtn.setTitleColor(#colorLiteral(red: 0, green: 0.3490196078, blue: 0.4431372549, alpha: 1).getAdjustedColor(), for: .normal)
        applyFiltersBtn.BorderColor = #colorLiteral(red: 0, green: 0.631372549, blue: 0.7411764706, alpha: 1).getAdjustedColor()
        fromLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        fromBtnClick.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.9647058824, blue: 0.9764705882, alpha: 1).getAdjustedColor()
//        fromBtnClick.setTitleColor( #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor(), for: .normal)
        fromTF.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()

        toLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        toBtn.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9647058824, blue: 0.9294117647, alpha: 1).getAdjustedColor()
        
        toTF.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        searchView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).getAdjustedColor()
        
        dropinIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        magnifierIconLabel.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        
        searchTF.textColor = #colorLiteral(red: 0.0862745098, green: 0.06666666667, blue: 0.2196078431, alpha: 1).getAdjustedColor()
        //searchTF.placeholderTextColor =
        
        
        
    }
   
    
    @objc func cancelBtnClicked(){
        self.removeFromSuperview()
        self.displayFloatingMenu()
        self.disposeBag = DisposeBag()
        
    }
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
    
    func setupHooks(){
        self.viewModel?.startLocationService()
        viewModel?.getCategoriesVar().asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] cats in
                if(cats == nil){
                 
                }else{
                   self.setupCategories(cats: cats!)
                }
            })
            .disposed(by: disposeBag)
          viewModel?.setIsToGetCategories()
        
        viewModel?.getLocationsVar().asObservable()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] locs in
                if(locs == nil){
                    
                }else{
                    self.locations = locs!
                    self.setupLocations()
                }
            })
            .disposed(by: disposeBag)
        currentAddress.asObservable().subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] locs in
                    self.setupLocations()
            }).disposed(by: disposeBag)
        
        viewModel?.getCurrentLocation().asObservable()
            .subscribe(onNext: { [unowned self] location in
                    location?.getAddress(completion: { (address:String?) in
                        
                        self.currentAddress.value = address ?? ""
                    
                    },parts: [EAddressPart.Locality, EAddressPart.AdministrativeArea])
                
            })
            .disposed(by: disposeBag)
        
        
    }
    
   
    private func setupLocations(){
        for view in self.self.locationsStackView.arrangedSubviews{
            view.isHidden = true
            view.removeFromSuperview()
        }
        if let loc =  self.currentAddress.value{
        let currentLocationView = LocationView(frame: CGRect(x: 0, y: 0, width: self.locationsStackView.frame.width, height: 50), isCurrentLocation: true, location: loc)
        currentLocationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(locationSelected(_:))))
        self.locationsStackView.addArrangedSubview(currentLocationView)
        }
        for loc in self.locations{
            let locationView = LocationView(frame: CGRect(x: 0, y: 0, width: self.locationsStackView.frame.width, height: 50), isCurrentLocation: false, location: loc)
            locationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(locationSelected(_:))))
            self.locationsStackView.addArrangedSubview(locationView)
        }
    }
    
    @objc func locationSelected(_ sender: Any){
        let locationView = (sender as! UITapGestureRecognizer).view as! LocationView
        if locationView.isCurrentLocation{
            self.searchTF.text = self.currentAddress.value
        }else{
            self.searchTF.text = locationView.location
        }
        self.locationStackContainer.isHidden = true
    }
    private func setupCategories(cats:[Event.Category]){
        for v in  self.categoryStackView.arrangedSubviews{
            v.isHidden = true
        }
        for cat in cats{
            self.categoryStackView.addArrangedSubview(CategoryCheckboxView(frame: CGRect(x: 0, y: 0, width: self.categoryStackView.frame.width, height: 60), category: cat.title!, OnCheckChanged: { (category:String, isSelected:Bool) in
                if isSelected{
                    self.eventsFilterApplyModel.categories.append(category)
                }else{
                    for i in 0..<self.eventsFilterApplyModel.categories.count{
                        if self.eventsFilterApplyModel.categories[i] == category{
                            self.eventsFilterApplyModel.categories.remove(at: i)
                            break
                        }
                    }
                }
            }))
        }
    }
    
   
    func setupUiData(){
        
        self.searchView.layer.borderColor = UIColor.paleGrey.cgColor
        self.searchView.addRoundAllCorners(radious: 5)
        self.locationStackContainer.addRoundAllCorners(radious: 5)
        self.locationStackContainer.layer.borderWidth = 1
        self.locationStackContainer.layer.borderColor = UIColor.paleGrey.cgColor
        self.locationStackContainer.isHidden = true
        self.searchTF.addTarget(self, action: #selector(searchTextChanged), for: [.editingChanged, .editingDidBegin])
        //from date
        let dateStr = Date().getFormattedString(format: self.dateFormat)
        self.fromTF.text = dateStr
        self.fromTF.inputView = getDatePicker(textField: self.fromTF)
        //to date
        self.toTF.text = dateStr
        self.toTF.inputView =  getDatePicker(textField: self.toTF)
        
        //apply btn
        self.applyFiltersBtn.addRoundCorners(radious: 23)
        
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
    }
    @objc func searchTextChanged() {
            self.viewModel?.setLocationQueryValue(q: self.searchTF.text)
        self.locationStackContainer.isHidden = false
    }
    
    @IBAction func ApplyFilterClickHandler(_ sender: LocalizedButton) {
        self.eventsFilterApplyModel.location = self.searchTF.text!
        let dF = DateFormatter(withFormat: "yyyy-MMM", locale: "en")
        self.eventsFilterApplyModel.from = dF.string(from: fromDate)
        self.eventsFilterApplyModel.to = dF.string(from: toDate)
        self.onApplyFilterClicked?(self.eventsFilterApplyModel)
        self.cancelBtnClicked()
    }
  
  
    
   
    func getDatePicker(textField: UITextField)->TammMonthYearPickerView{
        let monthYearPickerView = TammMonthYearPickerView()
            monthYearPickerView.onDateSelectedDate = { (date: Date) in
                textField.text = date.getFormattedString(format: self.dateFormat)
                if textField.tag == 1{
                    self.fromDate = date
                }else{
                    self.toDate = date
                }
        }
        return monthYearPickerView
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.scrollView.resizeToFitContent()
    }
    
    
}

