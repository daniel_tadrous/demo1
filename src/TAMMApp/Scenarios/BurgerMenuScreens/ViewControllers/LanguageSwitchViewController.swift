//
//  PrefrencesViewController.swift
//  TAMMApp
//
//  Created by kero1 on 7/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol LanguageSwitchViewControllerDelegate: class {
    func languageSwitchViewControllerDidChangeLanguage()
}

class LanguageSwitchViewController: TammViewController, LanguageSwitchStoryboardLoadable {
 
    @IBOutlet weak var tableView: UITableView!
    
    private let headerNibName = "LanguageSwitchSectionHeader"
    
    public var viewModel: LanguageSwitchViewModel!
    
    public weak var delegate: LanguageSwitchViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(title: L10n.Preferences.language, willSetSearchButton: true, backTitleString: L10n.Preferences.language)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: headerNibName, bundle: nil), forHeaderFooterViewReuseIdentifier: headerNibName)
        // Do any additional setup after loading the view.
        
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerNibName)

        footer?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        footer?.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: tableView.bounds.width, height: 50))
        tableView.tableFooterView = footer
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private var items: [[LanguageSwitchViewModel.LanguageViewData]]  {
        return viewModel.items
    }
    
}



extension LanguageSwitchViewController: UITableViewDelegate{
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        let sectionIndex = indexPath.section
//        let index = indexPath.row
//        let item = items[sectionIndex][index]
//
//        
//        return indexPath
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let sectionIndex = indexPath.section
        let index = indexPath.row
        let item = items[sectionIndex][index]
        
//        switch item.type {
//        case .navigationType:
//            break
//        case .selectionType:
//            break
//        case .switchType:
//            return
//        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        if !item.selected{
            viewModel.changeLanguage(lang: item)
            delegate?.languageSwitchViewControllerDidChangeLanguage()
        }
    }
}
extension LanguageSwitchViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionIndex = indexPath.section
        let index = indexPath.row
        let item = items[sectionIndex][index]
        var cell: LanguageSwitchTableViewCell
        
        cell = tableView.dequeueReusableCell(withIdentifier: LanguageSwitchTableViewCell.identifierNavigation) as! LanguageSwitchTableViewCell
  
        
//        title = item.item.rawValue
        
        cell.set(title: item.displayString, isSelected: item.selected, fixedLanguage: item.item == .arabic ? "ar" : "en")
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: headerNibName)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
}

