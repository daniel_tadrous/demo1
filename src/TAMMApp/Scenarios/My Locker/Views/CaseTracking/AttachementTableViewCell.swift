//
//  AttachementTableViewCell.swift
//  TAMMApp
//
//  Created by Daniel on 8/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

class AttachementTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
   
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    let cellID = "AttachmentCollectionViewCell"
    let cellHeight = 209
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.register(UINib(nibName: cellID, bundle: nil), forCellWithReuseIdentifier: cellID)
        collectionView.isScrollEnabled = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var attachments:[MsgAttachment] = []
    var click:((MsgAttachment,@escaping ()->Void)->Void)?
    func setData(attachments:[MsgAttachment],click:((MsgAttachment,@escaping ()->Void)->Void)?){
        self.click = click
        self.heightConstraint.constant = CGFloat(ceil(Double(attachments.count)/2) * Double(cellHeight)) + 40
        self.attachments = attachments
        self.collectionView.reloadData()
    }
    
}

extension AttachementTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachments.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.attachments[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! AttachmentCollectionViewCell
        cell.xLbl.isHidden = true
        cell.xBtn.isHidden = true
        cell.setData(attachment: item, click: click)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width-90.0)/2, height: CGFloat(cellHeight))
    }
    
}
