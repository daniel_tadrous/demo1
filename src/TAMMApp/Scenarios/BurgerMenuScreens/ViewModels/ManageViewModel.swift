//
//  ManageViewModel.swift
//  TAMMApp
//
//  Created by mohamed.elshawarby on 8/13/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

struct MediaTypeEntity {
    var name:String
    var size:Double
    var fileUrl:URL
}

class ManageViewModel {
    
    var allMedia : Variable<[DocumentUrl]?> = Variable(nil)

    func getMediaData() {
        do {
                let documentsURL = IncidentUrl.getDirectoryForIncident()
                let docs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: [], options:  [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
//                let images = docs.filter{
//                            for extensionType in MediaFormats.imgFormats {
//                                if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame { return true }
//                                    }
//                            return false
//                        }
//                let videos = docs.filter{
//                    for extensionType in MediaFormats.vedioFormats {
//                    if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame { return true }
//                    }
//                    return false
//                    }
//                let audios = docs.filter{
//                    for extensionType in MediaFormats.audioFormats {
//                    if  $0.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame { return true }
//                    }
//                    return false
//                    }
                    var documents = [DocumentUrl]()
                    for url in docs {
                        print(url)
                        for extensionType in MediaFormats.docsFormat{
                            if url.pathExtension.caseInsensitiveCompare(extensionType) == ComparisonResult.orderedSame {
                                documents.append(DocumentUrl(isSelected: false, myUrl: url))
                                continue
                            }
                        } 
                    }
                allMedia.value = documents
        } catch {
            print(error)
        }
    }
    
    
    func deleteUrl(removedUrl:URL) {
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: removedUrl)
            getMediaData()
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
//    fileprivate func sizePerMB(url: URL?) -> Double {
//        guard let filePath = url?.path else {
//            return 0.0
//        }
//        do {
//            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
//            if let size = attribute[FileAttributeKey.size] as? NSNumber {
//                return Double(round(100*(size.doubleValue / 1000000.0))/100)
//            }
//        } catch {
//            print("Error: \(error)")
//        }
//        return 0.0
//    }
    
     func sizePerMB(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return (size.doubleValue / 1000000.0)
            }
        } catch {
            print("Error: \(error)")
        }
        return 0
    }
    func getDisplayedNumber(number:Double)->String {
        let number = Double(round(100*(number))/100)
        if number == 0 {
            return "0"
        }else {
            return String(number)
        }
    }
    
}
