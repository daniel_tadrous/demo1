//
//  FeedbackViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 6/20/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift


enum FeedbackEmotion: String{
    case sad = "sad"
    case neutral = "neutral"
    case happy = "happy"
}

class FeedbackViewModel{
    
    var feedbackEmotion:FeedbackEmotion?
    var feedbackText:String?
    
    private var feedbackAPIService: FeedbackServiceType!
    
    private let disposeBag = DisposeBag()
    
    
    init(feedbackAPIService:FeedbackServiceType){
        self.feedbackAPIService = feedbackAPIService
    }
    
    
    func isModelValid() -> (Bool, String?){
        if feedbackEmotion == nil {
            return (false, L10n.pleaseSelectFeedbackEmoticon)
        }
        
        return (true, nil)
    }
    
    func sendFeedback(completionHandler: @escaping ( ()->() ), errorHandler: @escaping ( (Error)->() )){
        feedbackAPIService.sendFeedBack(emotion: feedbackEmotion?.rawValue ?? "", msg: feedbackText ?? "").subscribe(onNext: { (model) in
            
            // compleeted with a value
        }, onError: { (error) in
            errorHandler(error)
            print (error)
        }, onCompleted: {
            completionHandler()
        }) {
            // when disposed
        }.disposed(by: disposeBag)
        
    }
    
}
