//
//  AolDetailsViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 4/11/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa



protocol AspectOfLifeDetailViewData{
    var aspectId: String {get}
    var aspectDescription: String {get}
    var aspectTitle: String {get}
    var aspectServiceTopics: [AspectOfLifeServiceTopicViewData] {get}
    var aspectIconUrl: URL? {get}
}
protocol AspectOfLifeServiceTopicViewData {
    var serviceTopicId: String {get}
    var serviceTopicTitle: String {get}
    var serviceTopicDescription: String {get}
    
}

extension AspectOfLifeDetailModel: AspectOfLifeDetailViewData{
    
    var aspectId: String {
        get{
            return id ?? "-1"
        }
    }
    var aspectDescription: String {
        get {
            return self.description ?? "NO Description"
        }
    }
    var aspectTitle: String{
        get{
            return title ?? "No Title"
        }
    }
    var aspectServiceTopics: [AspectOfLifeServiceTopicViewData] {
        get {
            return serviceTopics ?? []
        }
    }
    var aspectIconUrl: URL?{
        get{
            if(icon == nil){
                return nil
            }
            return  URL(string:icon!)
        }
        
    }
}

extension AspectOfLifeServiceTopicModel: AspectOfLifeServiceTopicViewData{
    var serviceTopicId: String {
        get{
            return id ?? "-1"
        }
    }
    
    var serviceTopicTitle: String {
        get{
            return title ?? "No Title"
        }
    }
    
    var serviceTopicDescription: String {
        get{
            return self.description ?? "NO Description"
        }
    }
}




class AolDetailsViewModel{
    
    // Just set aspectId.value and the api will be called
    private(set) var aspectId: Variable<String?> = Variable(nil)
    private(set) var aspectTitle: Variable<String?> = Variable(nil)
    private(set) var viewData: Variable<AspectOfLifeDetailViewData?> = Variable(nil)
    
    fileprivate var apiService: ApiServiceAspectsOfLifeType
    fileprivate var disposeBag = DisposeBag()
    
    
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        aspectId.asObservable()
            .subscribe(onNext:{ [unowned self] aspectId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if aspectId.value != nil && !aspectId.value!.isEmpty{
            apiService.getAspectOfLifeDetails(id: aspectId.value!)
                .subscribe(onNext:{ [unowned self] aspectDetail in
                    self.viewData.value = aspectDetail
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    
    
    
    //    var subTopicDescription : Variable<String> = Variable("")
    //    var subTopicIcon : Variable<AolImageType> = Variable(AolImage.testImage)
    //
    //    var subTopics :Variable<[TopicItemType]> =
    ////        Variable([])
    //        Variable( DummyTopicItem.testItems )
    
    //    init() {
    ////        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
    ////
    ////
    ////            self.myServices.value =  (0..<15).map { (num) -> DummyServiceItem in
    ////                let d = DummyServiceItem()
    ////                d.serviceName = "\(num)"
    ////                return d
    ////            }
    ////        }
    //        // we can load data here into self.myServices.value and it will be reflected in the UI :D
    //
    //    }
    
    
    
    
    
}
