//
//  MessageModel.swift
//  TAMMApp
//
//  Created by Daniel on 7/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

enum FileTypeEnum:Int{
    case VIDEO,AUDIO,IMAGE,TXT
}


class MsgAttachment:Mappable{
    
    var id: Int?
    var name: String?
    var size: String?
    var url: String?
    var date: String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        id            <- map["id"]
        name         <- map["name"]
        size  <- map["size"]
        url <- map["url"]
        date <- map["date"]
    }
    
    var type: FileTypeEnum{
        get{
    let videoFormats = MediaFormats.vedioFormats
    let audioFormats = MediaFormats.audioFormats
    let imgFormats = MediaFormats.imgFormats
    
            
            let format:String = String((url?.split(separator: ".")[(url?.split(separator: ".").count)!-1]) ?? "")
            if videoFormats.contains(where: { (vformat) -> Bool in
                return  vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.VIDEO
            }else if audioFormats.contains(where: { (vformat) -> Bool in
                return  vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.AUDIO
            }else if imgFormats.contains(where: { (vformat) -> Bool in
                return  vformat.caseInsensitiveCompare(format) == ComparisonResult.orderedSame
            }){
                return FileTypeEnum.IMAGE
            }else{
                return FileTypeEnum.TXT
            }
            
        }
    }
    
    func isAttachmentDownloaded()->Bool{
        
        return FileManager.default.fileExists(atPath: attachmentFullUrl.path)
    }
    
    var attachmentFullUrl:URL{
        let parts = url?.split(separator: ".")
        let format  = String(parts![parts!.count - 1])
        let documentsURL =  IncidentUrl.getDirectoryForIncident()
        let fileURL = documentsURL.appendingPathComponent("\(attachmentFileName).\(format)")
        
        return fileURL
    }
    var attachmentFileName:String{
        return "attachment\(self.id ?? 0)"
    }
}
