//
//  TalkToUsDraftsViewModel.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//
import Foundation
import RxSwift

class TalkToUsDraftsViewModel {
    //public var drafts: [DraftViewModelData] = []
    fileprivate var draftsDB:[DALDrafts] = []
    fileprivate var userConfigService:UserConfigServiceType
    init(userConfigService: UserConfigServiceType, messageServices: ApiMessageServicesTypes) {
        self.messageServices = messageServices
        self.userConfigService = userConfigService
        setUpBinding()
        //self.getDrafts()
    }
    private func setUpBinding(){
        self.messagesTypesVar.value = messageServices.getStaticMessageTypes()
    }
    private let messageServices: ApiMessageServicesTypes
    private let messagesTypesVar: Variable<MessageTypesResponse?> = Variable(nil)
    lazy var messagesTypes: [Int: String] = self.getMessageTypes()
    
    private func getMessageTypes()->[Int:String]{
        var mesTypes: [Int:String] = [:]
        for item in (messagesTypesVar.value?.types)!{
            mesTypes[item.id!] = item.text
        }
        return mesTypes
    }
    func getDrafts()->[DraftViewModelData]{
        let DBdrafts = DALDrafts.getAll() as! [DALDrafts]
        draftsDB = DBdrafts
        var drafts = [DraftViewModelData]()
        for d in DBdrafts{
            
            drafts.append(self.mapDALtoViewModel(DAL: d))
        }
        drafts.sort { $0.time.timeIntervalSince1970 > $1.time.timeIntervalSince1970 }
        return drafts
    }
    
    fileprivate func mapDALtoViewModel(DAL draft:DALDrafts)->DraftViewModelData{
        
        
        
       
        let url = (try? draft.voiceUri?.asURL()) ?? nil
        
        
        let incidentMessage = IncidentMessageViewData(title:draft.draftTitle! , messageType:draft.typeId , notifiedByEmail:draft.notifiedByEmail , notifiedByMobileApplication:draft.notifiedByMobileApplication , notifiedByPhone:draft.notifiedByPhone , incidentLocation:draft.location! ,  media: draft.mediaUris, hasMedia:draft.hasMedia, voiceURI: url , comment: draft.comments ?? "", locationCoordinates: draft.locationCoordinates, isInformationPublic: draft.isInformationPublic, long:draft.long ?? 0 , lat: draft.lat  ?? 0 )
        
//        (title:String , messageType:Int , notifiedByEmail:Bool , notifiedByMobileApplication:Bool , notifiedByPhone:Bool , incidentLocation:String ,  media:[String] , hasMedia:Bool, voiceURI: URL?, comment: String, locationCoordinates: String?)
        
        return DraftViewModelData(message:  incidentMessage, draftId: draft.draftId, time: draft.draftDate!, type: self.messagesTypes[draft.typeId]!)
    }
    
    
//    fileprivate func initiateDraft(id:Int, type:String, message:String, date:Date , hasAttachments:Bool){
//        let item = DraftViewModelData(message: IncidentMessageViewData(), draftId: 1, time: Date())
//        //item.hasAttachments = hasAttachments
//        self.drafts.append(item)
//        //self.userConfigService.addDraft(draft: item)
//    }
    
    func deleteDraftById(id:String){
        DALDrafts.getById(id: id)?.delete()
    }
    
}

class DraftViewModelData{
    
    var message:MessageViewDataBase
    
    var draftId:String?
    var isOpened:Bool = false
    var time:Date
    var type: String?
    init(message:MessageViewDataBase , time:Date) {
        self.message = message
        self.time = time
    }
    init(message:MessageViewDataBase, draftId:String, time:Date) {
        self.message = message
        self.draftId = draftId
        self.time = time
    }
    init(message:MessageViewDataBase, draftId:String, time:Date, type: String) {
        self.message = message
        self.draftId = draftId
        self.time = time
        self.type = type
    }
    
}
