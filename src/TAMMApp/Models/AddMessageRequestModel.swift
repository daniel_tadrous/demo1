//
//  AddMessageRequestModel.swift
//  TAMMApp
//
//  Created by kerolos on 4/25/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

/*

title *
string
(formData)
The title of the message
messageTypeId *
integer
(formData)
The type ID of the message
incidentLocation *
string
(formData)
The location of the message
hasMedia *
boolean
(formData)
Indicates whether the message has media file(s)

media
file
(formData)
The media file of the message. OAS 2 doesn’t support an array of files, so the parameter here is only one file. Note that the associated User Story is marked as ‘Incomplete’

notifiedByEmail *
boolean
(formData)
Indicates whether the user shall receive notifications by email
notifiedByMobileApplication *
boolean
(formData)
Indicates whether the user shall receive notifications by push notification
notifiedByPhone *
boolean
(formData)
 
 
 */

class AddMessageRequestModel: Mappable{
    
    //The title of the message
    var title: String?
    //The type ID of the message
    var messageTypeId: Int?
    //The location of the message
    var incidentLocation: String?
    //Indicates whether the message has media file(s)
    var hasMedia: Bool?
    //The media file of the message. OAS 2 doesn’t support an array of files, so the parameter here is only one file. Note that the associated User Story is marked as ‘Incomplete’
    var media: [String]?
    var mediaList: [MediaViewModelData]?
    //    Indicates whether the user shall receive notifications by email
    var notifiedByEmail: Bool?
    //Indicates whether the user shall receive notifications by push notification
    var notifiedByMobileApplication: Bool?
    var notifiedByPhone: Bool?
    
    init(){
        
    }
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        title                        <- map["title"]
        messageTypeId                <- map["messageTypeId"]
        incidentLocation             <- map["incidentLocation"]
        hasMedia                     <- map["hasMedia"]
        media                        <- map["media"]
        notifiedByEmail              <- map["notifiedByEmail"]
        notifiedByMobileApplication  <- map["notifiedByMobileApplication"]
        notifiedByPhone              <- map["notifiedByPhone"]
    }
    
    
}

