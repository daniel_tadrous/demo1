//
//  DALDraftTypes.swift
//  TAMMApp
//
//  Created by Marina.Riad on 5/17/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RealmSwift


class DALDraftTypes:Object{
    
    override static func primaryKey() -> String? {
        return "draftTypeName"
    }
    
//    @objc dynamic var uuid = UUID().uuidString
    @objc dynamic var draftTypeId:Int = 0
    @objc dynamic var draftTypeName:String?
    
     convenience init(id:Int , name:String) {
        self.init()
        self.draftTypeId = id
        self.draftTypeName = name
    }
    
    class func getAll()->[DALDraftTypes]{

        let realm = try! Realm()
        
        let objs = realm.objects(DALDraftTypes.self)
        
        return objs.map({ (item) -> DALDraftTypes in
            item
        })
    }
    
    class func getBy(id:Int)->DALDraftTypes?{

        let realm = try! Realm()
        
        let objs = realm.objects(DALDraftTypes.self)
        
        return objs.first
        
    }
    
    class func deleteAll()->Bool{
        do {
            do {
                let realm = try! Realm()
                try realm.write {
                    let objs = realm.objects(DALDraftTypes.self)
            
            
            
                    realm.delete(objs)
                }
            }
        }
        catch let e as Realm.Error{
            print( e )
            return false
        }
        catch let e{
            print( e )
            return false
        }
        return true
    }
    
    class func saveAll(types:[DALDraftTypes]){
        for type in types{
            type.save()
        }
    }
    
    func save() {
        do {
            let realm = try! Realm()
            try realm.write {

        realm.add(self, update: true)
            }
        }
        catch let e as Realm.Error{
            print( e )
        }
        catch let e{
            print( e )
        }
        
    }
    
    func delete() -> Bool {
        do {
            let realm = try! Realm()
            try realm.write {

                realm.delete(self)
            }
        }
        catch let e as Realm.Error{
            print( e )
            return false
        }
        catch let e{
            print( e )
            return false
        }
        return true
    }
    
    
    
    
}
