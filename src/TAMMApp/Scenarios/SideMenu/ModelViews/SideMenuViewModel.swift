//
//  SideMenuViewModel.swift
//  TAMMApp
//
//  Created by kerolos on 4/16/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

enum SideMenuItemTypes{
    case text
    case logout
    case switchProfile
}

class SideMenuItem{
    var type: SideMenuItemTypes
    var text: String
    
    
    init(_ type: SideMenuItemTypes, _ text: String ){
        self.type = type
        self.text = text
    }
    
}

enum SideMenuProfileTypes {
    case personal
    case business
}

class SideMenuSwitchProfileItem: SideMenuItem {
    var selectedProfile: SideMenuProfileTypes
    
    init(_ type: SideMenuItemTypes, _ text: String, _ selectedProfile: SideMenuProfileTypes ){
        self.selectedProfile = selectedProfile
        super.init(type, text)
    }
}

class SideMenuViewModel{
    
    let sections: Variable<[[SideMenuItem]]> = Variable([
        [SideMenuItem(.text, "Aspects of life"),
         SideMenuItem(.text, "Abu Dhabi Govt. Entities"),
         ],
        [SideMenuItem(.text, "My Updates"),
         SideMenuItem(.text, "My Events"),
         SideMenuItem(.text, "My Community"),
         SideMenuItem(.text, "My Support"),
         ],
        [SideMenuItem(.text, "Co Create"),
         SideMenuItem(.text, "News & Events"),
         SideMenuItem(.text, "Settings"),
         ],
        [SideMenuSwitchProfileItem(.switchProfile, "Select a profile", .personal),
         ],
        [SideMenuItem(.text, "Logout"),
         ],
        ] )
    
}
