//
//  GovernmentEntityViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 7/8/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import RxSwift

class GovernmentEntityViewModel{
    
    
    
    let arabicLetters = ["أ"
        ,"ب"
        ,"ت"
        ,"ث"
        ,"ج"
        ,"ح"
        ,"خ"
        ,"د"
        ,"ذ"
        ,"ر"
        ,"ز"
        ,"س"
        ,"ش"
        ,"ص"
        ,"ض"
        ,"ط"
        ,"ظ "
        ,"ع"
        ,"غ"
        ,"ف"
        ,"ق"
        ,"ك"
        ,"ل"
        ,"م"
        ,"ن"
        ,"ه"
        ,"و"
        ,"ى"
    ]
    private(set) var viewData: Variable<[GovernmentEntity]?> = Variable(nil)
    private(set) var start: Variable<Bool?> = Variable(nil)
    private(set) var isToGetFilters: Variable<Bool?> = Variable(nil)
    private(set) var filters: Variable<[GovernmentEntityFilter]?> = Variable(nil)
    private(set) var query: Variable<String?> = Variable(nil)
    private(set) var aolId: Variable<String?> = Variable(nil)
    var category: String? = ""
    
    fileprivate var apiService: ApiServiceAspectsOfLifeType!
   
    fileprivate var disposeBag = DisposeBag()
    
    init(apiService: ApiServiceAspectsOfLifeType) {
        self.apiService = apiService
        setupBinding()
    }
    
    fileprivate func setupBinding(){
        start.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        aolId.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.requestDetails()
            })
            .disposed(by:disposeBag)
        isToGetFilters.asObservable()
            .subscribe(onNext:{ [unowned self] serviceId in
                self.getFilters()
            })
            .disposed(by:disposeBag)
    }
    
    
    fileprivate func requestDetails(){
        if (start.value != nil && start.value!) || (query.value != nil) || (aolId.value != nil){
            apiService.getGovernmentEntities(q: query.value,aolId: aolId.value,category: category)
                .subscribe(onNext: { [unowned self] GovernmentEntities in
                    self.viewData.value = GovernmentEntities
                }).disposed(by: disposeBag)
        }else{
            self.viewData.value  = nil
        }
    }
    fileprivate func getFilters(){
        if isToGetFilters.value != nil && isToGetFilters.value!{
            apiService.getGovernmentEntitiesFilter()
                .subscribe(onNext: { [unowned self] filters in
                    self.filters.value = filters
                }).disposed(by: disposeBag)
        }else{
            self.filters.value  = nil
        }
    }
}
