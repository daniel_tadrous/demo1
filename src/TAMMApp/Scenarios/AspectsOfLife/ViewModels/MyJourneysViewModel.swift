//
//  MyJourneysViewModel.swift
//  TammUI
//
//  Created by Marina.Riad on 4/16/18.
//  Copyright © 2018 Marina.Riad. All rights reserved.
//
import Foundation
import RxSwift

class MyJourneysViewModel{
    
    // the values are bound to actions that get data from the api
    var userId : Variable<Int> = Variable(-1)
    private(set) var journeysVar : Variable<[JourneyAspectOfLifeViewData]>  = Variable([])

    fileprivate var onGoingJourenys:[JourneyAspectOfLifeViewData] = []
    fileprivate var NotTakenJourneys:[JourneyAspectOfLifeViewData] = []
    
    fileprivate var apiService: ApiServiceAspectsOfLifeType
    fileprivate var userConfigService: UserConfigServiceType
    fileprivate var disposeBag = DisposeBag()
    
    
    init(apiService: ApiServiceAspectsOfLifeType, userConfigService: UserConfigServiceType) {
        self.userConfigService = userConfigService
        self.apiService = apiService
        userId.value = userConfigService.getUserId()
        self.fillJourneys()
        // sets the binding so that any change in the userId the journeys will be fetched from server
        setUpBinding()
    }
    
    func setUpBinding(){
        userId.asObservable().subscribe(onNext:{ [unowned self] userId in
            self.fetchJourneys(userId: userId)
        }).disposed(by: disposeBag)
    }
    
    func fetchJourneys(userId: Int){
        apiService.getUserJourneys(userId: userId).subscribe(onNext:{ [unowned self] userJourneys in
            
            var userJourneys1 = userJourneys
            userJourneys1 = userJourneys1 + userJourneys1 + userJourneys1
            userJourneys1 = userJourneys1 + userJourneys1
            var allJourneys:[JourneyAspectOfLifeViewData] = []
            var i = 0
            var completence = 0
            for item in userJourneys1{
                completence = item.completionPercentage!
                if(i > 1){
                    completence = 0
                }
                let journey:JourneyAspectOfLifeViewData = JourneyAspectOfLifeViewData(AspectOfLifeImageUrl: (item.journey?.AspectOfLifeImageUrl)!, AspectOfLifeID: item.id!, AspectOfLifeHeader: (item.journey?.AspectOfLifeHeader)!, AspectOfLifeDescription: (item.journey?.AspectOfLifeDescription)!, AspectOfLifeImage: (item.journey?.AspectOfLifeImage)!, AspectOfLifeImageColor: (item.journey?.AspectOfLifeImageColor)!, completionPercentage: Double(completence))
                allJourneys.append(journey)
                i += 1
            }
            self.journeysVar.value = allJourneys
           // self.journeysVar.value = userJourneys1
        }).disposed(by:disposeBag)
    }
    
    fileprivate func fillJourneys(){
        // call services
        //TODO: remove the initialization
        //let fixedUrl =  URL(string:"http://google.com")!
        //jourenys.append(JourneyAspectOfLifeViewData(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 1, AspectOfLifeHeader: "Moving to New Home", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.MovingToNewHomeIconString, AspectOfLifeImageColor:String.MovingToNewHomeBackgroundColorString, completionPercentage: 0 ))
        
       // jourenys.append(JourneyAspectOfLifeViewData(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 2, AspectOfLifeHeader: "Starting a New Business", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.StartingANewBusinessIconString, AspectOfLifeImageColor:String.StartingANewBusinessBackgroundColorString, completionPercentage: 0 ))
        
        //jourenys.append(JourneyAspectOfLifeViewData(AspectOfLifeImageUrl: fixedUrl, AspectOfLifeID: 3, AspectOfLifeHeader: "Get a New Driving License", AspectOfLifeDescription: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut", AspectOfLifeImage: String.GetANewDrivingLicenseIconString, AspectOfLifeImageColor:String.GetANewDrivingLicenseBackgroundColorString, completionPercentage: 65 ))
    }
    func getJourneys()->[JourneyAspectOfLifeViewData]{
        for journey in journeysVar.value {
            if(journey.completionPercentage == 0){
                NotTakenJourneys.append(journey)
            }
        }
        return NotTakenJourneys
    }
    
    func getOnGoingJourenys()->[JourneyAspectOfLifeViewData]{
        for journey in journeysVar.value {
            if(journey.completionPercentage > 0){
                onGoingJourenys.append(journey)
            }
        }
        return onGoingJourenys
    }
    
    
    func journeyIsSelected(index:Int, onGoing:Bool){
        // call coordinator
    }
}


//extension UserJourneyModel: AspectOfLifeViewData{
//    var AspectOfLifeID: Int {
//        get{ return self.id! }
//    }
//    
//    var AspectOfLifeHeader: String {
//        get { return (self.journey?.title!)! }
//    }
//    
//    var AspectOfLifeDescription: String {
//        get{ return (self.journey?.description)!}
//    }
//    
//    var AspectOfLifeImage: String {
//        return String.MovingToNewHomeIconString // self.journey?.icon
//    }
//    
//    var AspectOfLifeImageColor: String {
//        return String.GetANewDrivingLicenseBackgroundColorString
//    }
//    
//    var AspectOfLifeImageUrl: URL {
//        return URL(string: self.journey!.icon!)!
//        
//    }
//    
//    
//}


struct JourneyAspectOfLifeViewData:AspectOfLifeViewData{
    var AspectOfLifeImageUrl: URL
    
    
    var AspectOfLifeID:Int
    
    var AspectOfLifeHeader: String
    
    var AspectOfLifeDescription: String
    
    var AspectOfLifeImage: String
    
    var AspectOfLifeImageColor: String
    
    var completionPercentage: Double
    
    
}

extension String{
    public static var MovingToNewHomeIconString: String{
        return "J"
    }
    public static var StartingANewBusinessIconString: String{
        return "n"
    }
    public static var GetANewDrivingLicenseIconString: String{
        return "o"
    }
    
    // Background Color
    public static var MovingToNewHomeBackgroundColorString: String{
        return "#00A1BD"
    }
    public static var StartingANewBusinessBackgroundColorString: String{
        return "#7CB879"
    }
    public static var GetANewDrivingLicenseBackgroundColorString: String{
        return "#004F66"
    }
}
