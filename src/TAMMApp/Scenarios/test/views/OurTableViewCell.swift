//
//  OurCellViewTableViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 4/4/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol OurTableViewCellDelegate: class {
    func contentDidChange(cell: OurTableViewCell)
}

class OurTableViewCell: UITableViewCell {

    public static let generalIdentifier = "OurTableViewCell"
    var expanded : Bool = false
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //toglevisability()
        // Configure the view for the selected state
    }
    
//    @IBOutlet var labelDescription: UILabel!
//    @IBOutlet var labelTerms: UILabel!
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    
    
    let shortText = "Lorem ipsum dolor"
    
    let longText = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    

    weak var delegate: OurTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label1.text = shortText
        label2.text = longText
        label2.isHidden = true
    }
    func toglevisability(){
        label2.isHidden = !label2.isHidden
        delegate?.contentDidChange(cell: self)

    }
    func hideExtra(){
        label2.isHidden = true
//        setNeedsLayout()
    }
    func showExtra(){
        label2.isHidden = false
//        setNeedsLayout()
    }
}
