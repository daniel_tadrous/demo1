//
//  InformationAboutPagerViewCell.swift
//  TAMMApp
//
//  Created by kerolos on 7/9/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import FSPagerView


class InformationAboutPagerViewCell: FSPagerViewCell {
    
    public static let namedNibName = "InformationAboutPagerViewCell"

    @IBOutlet weak var imageView1: UIImageView!
    var viewIdentfier: AnyHashable!
    
    
}
