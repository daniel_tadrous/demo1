//
//  ProfileViewController.swift
//  TAMMApp
//
//  Created by kerolos on 7/19/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit
import RxSwift
import SafariServices

protocol ProfileViewControllerDelegate {
    
}

class ProfileViewController: TammViewController, ProfileStoryboardLoadable, SFSafariViewControllerDelegate {

    private var disposeBag = DisposeBag()
    public var viewModel: ProfileViewModel!
    public var delegate: ProfileViewControllerDelegate?
    
    @IBOutlet weak var updateProfileButton: UITammResizableButton!
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet var separatorts: [UIView]!
    
    @IBOutlet weak var roundingView: UIView!
    @IBOutlet weak var innerRoundingView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: LocalizedLabel!
    @IBOutlet weak var emailLabel: LocalizedLabel!
    @IBOutlet weak var phoneLabel: LocalizedLabel!
    
    @IBOutlet var blueLabels: [LocalizedLabel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ApplicationCoordinator.isToOpenUserProfile = false
        // Do any additional setup after loading the view.
        setTitle()
        setImageRounding()
        setHooks()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainView.backgroundColor = UIColor.whiteTwo.getAdjustedColor()
        roundingView.backgroundColor = UIColor.paleGrey.getAdjustedColor()
        for separator in separatorts {
            separator.backgroundColor = UIColor.paleGrey.getAdjustedColor()
        }
        updateProfileButton.backgroundColor = UIColor.duckEggBlueTwo.getAdjustedColor()
        updateProfileButton.BorderColor = UIColor.turquoiseBlue.getAdjustedColor()
        
        for label in blueLabels {
            label.textColor = UIColor.darkBlueGrey.getAdjustedColor()
        }
        emailLabel.textColor = UIColor.petrol.getAdjustedColor()
        phoneLabel.textColor = UIColor.petrol.getAdjustedColor()
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setTitle(){
        setNavigationBar(title:  L10n.Profile.profileTitle, willSetSearchButton: true, backTitleString: L10n.Profile.profileTitle)
    }
    
    private func setImageRounding(){
        roundingView.layer.cornerRadius = roundingView.frame.width / 2
        innerRoundingView.layer.cornerRadius = innerRoundingView.frame.width / 2
    }
    
    private func setHooks(){
        
        viewModel.image.subscribeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] (img) in
            self.imageView.image = img
        }).disposed(by: disposeBag)
        
        
        viewModel.fullName.subscribeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] (fullName) in
            self.nameLabel.text = fullName
        }).disposed(by: disposeBag)
        
        
        viewModel.email.subscribeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] (email) in
            self.emailLabel.text = email
        }).disposed(by: disposeBag)
        
        
        
        viewModel.phone.subscribeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] (phone) in
            self.phoneLabel.text = phone
        }).disposed(by: disposeBag)
    }
    
    

    
    @IBAction func buttonClicked(_ sender: UITammResizableButton) {

    let vc = ConfirmationMessageViewController()
        vc.initialize(title: L10n.warning, message: L10n.youWillBeRedirectedToSmartPassPortal, CancelBtnTitle: L10n.cancel, confirmationBtnTitle: L10n.ok, okClickHandler: {
                        self.goToSmartPassWebPage()
                      })
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.removeFloatingMenu()
        self.present(vc, animated: true, completion: nil)
    
    }
    
    fileprivate func goToSmartPassWebPage() {
        let clientIDKey = "ClientID";
        let clientId = (Bundle.main.object(forInfoDictionaryKey: clientIDKey) as? String)!
        let siteUrlString = "https://stage.smartpass.government.net.ae/dashboard?spEntityId=\(clientId)&mobileApp=true&lang=\(L10n.Lang)"
        let siteUrl = URL(string:siteUrlString)!
        let vc = SFSafariViewController(url: siteUrl)
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        dismiss(animated: true)
        // update profile
        viewModel.updateUserInfo()
    }

  
    
}
