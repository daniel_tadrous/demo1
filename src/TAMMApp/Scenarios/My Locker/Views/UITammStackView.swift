//
//  UITammStackView.swift
//  TAMMApp
//
//  Created by Marina.Riad on 4/24/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import UIKit

protocol UITammStackViewDelegate {
    func itemIsSelected( message:TalkToUsItemViewDataType)
}
class UITammStackView: UIView {
    
    @IBInspectable
    var heightReference: CGFloat = 40
    
    private var scaledHeight: CGFloat = 40
    
    public var items:[TalkToUsItemViewDataType] = []{
        didSet{
                for v in views{
                    v.removeFromSuperview()
                }
                views = []
                itemsLabels = []
        }
    }
    public var delegate:UITammStackViewDelegate?
    fileprivate var views:[UIView] = []
    fileprivate var itemsLabels:[UITammStackViewLabel] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    fileprivate func generate(){
        var index = 0
        let frame:CGRect = CGRect(x: 0, y: 0, width: self.bounds.width, height: scaledHeight)
        var heightOfLabel:CGFloat = scaledHeight
        var currentWidth:CGFloat = 0
        var currentHeight:CGFloat = 0
        let space:CGFloat = 10
        if(views.count == 0){
        for item in items {
            heightOfLabel = scaledHeight
            let label = UITammStackViewLabel(text: item.name, index: index, frame: frame)
            label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(itemDidSelect(_:))))
            if(currentWidth == 0){
                let view = UIView()
                label.frame = CGRect(origin: CGPoint(x:0, y:currentHeight), size: label.frame.size)
                if(label.frame.height > scaledHeight){
                    heightOfLabel = label.frame.height
                }
                view.frame = CGRect(x: self.frame.width/2 - label.frame.width/2 , y: 0, width: label.frame.width, height: heightOfLabel)
                currentWidth = label.frame.width
                view.addSubview(label)
                self.addSubview(view)
                //view.center = self.center
                views.append(view)
            }
            else if(currentWidth + label.frame.width <= self.bounds.width){
                let view = views[ views.count - 1]
                currentWidth += space
                label.frame = CGRect(origin: CGPoint(x:currentWidth, y:0), size: label.frame.size)
                currentWidth += label.frame.width
                if(label.frame.height > scaledHeight){
                    heightOfLabel = label.frame.height
                }
                view.frame = CGRect(x: self.frame.width/2 - currentWidth/2, y: currentHeight, width: currentWidth, height: heightOfLabel)
                view.addSubview(label)
                //view.center = self.center
            }
            else{
                let view = UIView()
                currentHeight += heightOfLabel + space
                label.frame = CGRect(origin: CGPoint(x:0, y:0), size: label.frame.size)
                if(label.frame.height > scaledHeight){
                    heightOfLabel = label.frame.height
                }
                view.frame = CGRect(x: self.frame.width/2 - label.frame.width/2, y: currentHeight, width: label.frame.width, height: heightOfLabel)
                currentWidth = label.frame.width
                view.addSubview(label)
                self.addSubview(view)
                //view.center = self.center
                views.append(view)
            }
            
            index += 1
            
        }
        }
    }
    fileprivate func clearViews(){
        for v in views{
            v.removeFromSuperview()
        }
        views = []
        itemsLabels = []
        items = []
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.generate()
    }
    @objc func itemDidSelect(_ sender:Any){
        let tapGesture = sender as! UITapGestureRecognizer
        let label = tapGesture.view as! UITammStackViewLabel
        delegate?.itemIsSelected(message: items[label.index])
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        //scaledHeight = heightReference * UIFont.fontSizeMultiplier
        // just to trigger the did set
        items = items  + []
    }
}

class UITammStackViewLabel: LocalizedLabel {
    private(set) var index = -1
    init(text:String , index:Int, frame:CGRect) {
        super.init(frame:frame)
        self.index = index
        self.text = text
        self.isUserInteractionEnabled = true
        self.textAlignment = .center
        self.textColor = UIColor.init(red: 22/255, green: 17/255, blue: 56/255, alpha: 1)
        self.originalFont = UIFont(name: "CircularStd-Book", size: 16)
        self.RTLFont = "Swissra-Normal"
        self.applyFontScalling = true
        
        self.font = self.originalFont
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.adjustFont(RTL: true)
        }
        else{
            self.adjustFont(RTL: false)
        }
        self.sizeToFit()
        if(self.frame.width + 25 > frame.width){
            self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: frame.width , height: frame.height))
        }
        else{
            self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.width + 25 , height: frame.height))
        }
        
    }
    fileprivate func redraw(){
        if(L102Language.currentAppleLanguage().contains("ar")){
            self.adjustFont(RTL: true)
        }
        else{
            self.adjustFont(RTL: false)
        }
        self.sizeToFit()
        if(self.frame.width + 25 > frame.width){
            self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: frame.width , height: frame.height))
        }
        else{
            self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.width + 25 , height: frame.height))
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = UIColor.init(red: 217/255, green: 243/255, blue: 247/255, alpha: 1)
        self.addRoundCorners(radious: 20, borderColor: UIColor.init(red: 0, green: 171/255, blue: 197/255, alpha: 1))
        self.clipsToBounds = true

        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.adjustFont(RTL: L102Language.currentAppleLanguage().contains("ar"))
    }
}

