//
//  OfflineDraftsViewModel.swift
//  TAMMApp
//
//  Created by Daniel on 5/28/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation

class OfflineDraftsViewModel{
     var numberOfPendingItems: Int {
        get{
            return DALDrafts.getAll().count
        }
    }
}
