////
////  UITammTopTabBar.swift
////  TAMMApp
////
////  Created by Marina.Riad on 5/1/18.
////  Copyright © 2018 Igor Kulman. All rights reserved.
////
//
//import UIKit
//
//protocol UITammTopTabBarDelegate {
//
//}
//@IBDesignable
//class UITammTopTabBarController:UIViewController , UITammTopTabBarDelegate{
//
//    fileprivate var stackView: UIStackView!
//    fileprivate var containerView: UIView!
//    fileprivate var selectedViewController:UIViewController!
//    private(set) var items: [UIButton] = []
//    public var selectedButtonBorder:CGFloat = 4
//    fileprivate let stackViewBottomBorder:CGFloat = 1
//    fileprivate let unSelectedFont:UIFont = UIFont.init(name: "CircularStd-Medium", size: 18)!
//    public var selectedButtonBorderColor:String = "#10596F"
//    fileprivate let stackViewBottomBorderColor:String = "#CAC8D1"
//    fileprivate var layer:CALayer?
//    public var selectedIndex = -1 {
//        didSet{
//            buttonIsSelected(index: selectedIndex, openJournyes: false)
//        }
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        stackView = UIStackView()
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        addBorderToStackView()
//
//    }
//
//    public func addItem(title:String, textColor:UIColor, font:UIFont , selectedFont:UIFont , viewController:UIViewController){
//        let newButton = UITammBarItem()
//        newButton.setTitle(title, for: .normal)
//        newButton.setTitleColor(textColor, for: .normal)
//        newButton.titleLabel?.font = font
//        newButton.selectedFont = selectedFont
//        stackView.addArrangedSubview(newButton)
//        items.append(newButton)
//        if(selectedIndex < -1){
//            selectedIndex = 0
//        }
//    }
//
//
//    fileprivate func commonInit(){
//        buttonIsSelected(index: -1, openJournyes: false)
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        addBorderToStackView()
//    }
//
//
//    @IBAction func btnServicesClicked(_ sender: UIButton) {
//       // buttonIsSelected(button: sender, openJournyes: false)
//    }
//    @IBAction func btnJourneysClickes(_ sender: UIButton) {
//       // buttonIsSelected(button: sender, openJournyes: true)
//    }
//
//    fileprivate func addBorderToStackView(){
//
//        let bottomBorder:CALayer = CALayer.init()
//        bottomBorder.frame = CGRect(x: 0, y: stackView.frame.height - stackViewBottomBorder, width: stackView.frame.width, height: stackViewBottomBorder)
//
//        bottomBorder.backgroundColor = UIColor.init(hexString: stackViewBottomBorderColor).cgColor
//
//        stackView.layer.addSublayer(bottomBorder)
//    }
//    fileprivate func buttonIsSelected(index:Int, openJournyes:Bool){
//        if(items.count > 0 && index >= 0){
//            let button = items[index]
//        }
//        else{
//            return
//        }
//
//        if(layer != nil){
//            layer?.removeFromSuperlayer()
//        }
//        let btnServiceString = NSAttributedString(string: btnServices.title(for: .normal)!, attributes: [NSAttributedStringKey.font:unSelectedFont ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#151331")])
//        btnServices.setAttributedTitle(btnServiceString, for: .normal)
//        let btnJourneysString = NSAttributedString(string: btnJourneys.title(for: .normal)!, attributes: [NSAttributedStringKey.font:unSelectedFont ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#151331")])
//        btnJourneys.setAttributedTitle(btnJourneysString, for: .normal)
//        let bottomBorder:CALayer = CALayer.init()
//        bottomBorder.frame = CGRect(x: 0, y: button.frame.height - selectedButtonBorder, width: button.frame.width, height: selectedButtonBorder)
//        bottomBorder.backgroundColor = UIColor.init(hexString: selectedButtonBorderColor).cgColor
//
//        button.layer.addSublayer(bottomBorder)
//        let btnString = NSAttributedString(string: button.title(for: .normal)!, attributes: [NSAttributedStringKey.font:UIFont.init(name: "CircularStd-Bold", size: 18)! ,  NSAttributedStringKey.foregroundColor:UIColor.init(hexString: "#11596F")])
//        button.setAttributedTitle(btnString, for: .normal)
//        layer = bottomBorder
//        if(openJournyes){
//            delegate?.openJourneys()
//        }
//        else{
//            delegate?.openAspectsOfLifeList()
//        }
//
//}
//
//    class UITammBarItem: UIButton {
//        public var selectedFont:UIFont!
//    }
//}

