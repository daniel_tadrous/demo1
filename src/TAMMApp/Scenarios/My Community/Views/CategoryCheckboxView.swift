//
//  ErrorView.swift
//  TAMMApp
//
//  Created by kerolos on 5/3/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import UICheckbox_Swift

class CategoryCheckboxView : UIView{
 
    @IBOutlet weak var container: UIView!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var checkbox: UICheckbox!
    
    @IBOutlet weak var categoryLb: LocalizedLabel!
    var OnCheckChanged:((String,Bool)->Void)?
    
    var category: String = ""
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    init(frame: CGRect,category: String,OnCheckChanged:@escaping (String,Bool)->Void){
        super.init(frame: frame)
        self.category = category
        self.OnCheckChanged = OnCheckChanged
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    private func commonInit(){
        let viewFileName: String = "CategoryCheckboxView"
        Bundle.main.loadNibNamed(viewFileName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setConstraints(view: self, childView: contentView)
        setupUiData()
    }
   
   
    func setConstraints(view: UIView, childView: UIView){
        let newView = childView
        //        newView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let right = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: newView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        
        view.addConstraints([top, right, bottom, trailing])
    }
    
 
 
    func setupUiData(){
        self.categoryLb.text = self.category
        checkbox.onSelectStateChanged = { (checkb , isSelected) in
            self.OnCheckChanged!(self.category,isSelected)
            var font = UIFont(name: L10n.Lang == "en" ? "Roboto-Regular" : "Swissra-Normal", size: 16)
            if isSelected{
             font = UIFont(name: L10n.Lang == "en" ? "Roboto-Bold" : "Swissra-Bold", size: 16)
            }
            
            self.categoryLb.font = font
        }
    }
}
