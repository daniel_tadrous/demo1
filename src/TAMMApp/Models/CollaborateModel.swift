//
//  CollaborateModel.swift
//  TAMMApp
//
//  Created by Daniel on 7/1/18.
//  Copyright © 2018 Igor Kulman. All rights reserved.
//

import Foundation
import ObjectMapper

class CollaborateModel: Mappable{
    var email: String?
    var phone: String?
    var detailsText: String?
    var detailsAudio: String?
    
    required init?(map: Map) {
        
    }
    init(){
        
    }
    // Mappable
    func mapping(map: Map) {
        email <- map["email"]
        phone <- map["phone"]
        detailsText <- map["detailsText"]
        detailsAudio <- map["detailsAudio"]
    }
    
    
}
