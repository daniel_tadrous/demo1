////
////  AppDelegate.swift
////  TAMMApp
////
////  Created by Marina.Riad on 3/25/18.
////  Copyright © 2018 Marina.Riad. All rights reserved.
////
//
//import UIKit
//import AppAuth
//
//@UIApplicationMain
//class AppDelegate: UIResponder, UIApplicationDelegate {
//
//    var window: UIWindow?
//    var currentAuthorizationFlow:OIDAuthorizationFlowSession?
//    var authState:OIDAuthState?
//    var jsonDictionaryUserInfo:Any?
//    var logText:String = ""
//    let kAppAuthExampleAuthStateKey = "authState";
//    let kAppAuthExampleUserInfoKey = "userInfo";
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        // Override point for customization after application launch.
//        self.loadState();
//        if(authState != nil ) {
//            self.loadUserInfo();
//            if(jsonDictionaryUserInfo != nil){
//                let Profile = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "Profile") as! ProfileViewController;
//                Profile.setVC(userInfo: jsonDictionaryUserInfo!, displayLog: false)
//                self.window?.rootViewController = Profile;
//            }
//        }
//        return true
//    }
//
//    /*! @fn application:openURL:options:
//     @brief Handles inbound URLs. Checks if the URL matches the redirect URI for a pending
//     AppAuth authorization request.
//     */
//    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
//        // Sends the URL to the current authorization flow (if any) which will process it if it relates to
//        // an authorization response.
//        if currentAuthorizationFlow!.resumeAuthorizationFlow(with: url){
//            currentAuthorizationFlow = nil
//            return true
//        }
//
//        // Your additional URL handling (if any) goes here.
//
//        return false;
//    }
//
//    /*! @fn application:openURL:sourceApplication:annotation:
//     @brief Forwards inbound URLs for iOS 8.x and below to @c application:openURL:options:.
//     @discussion When you drop support for versions of iOS earlier than 9.0, you can delete this
//     method. NB. this implementation doesn't forward the sourceApplication or annotations. If you
//     need these, then you may want @c application:openURL:options to call this method instead.
//     */
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        return self.application(application, open: url, options:[:])
//    }
//
//
//    func applicationWillResignActive(_ application: UIApplication) {
//        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//    }
//
//    func applicationDidEnterBackground(_ application: UIApplication) {
//        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    }
//
//    func applicationWillEnterForeground(_ application: UIApplication) {
//        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//    }
//
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    }
//
//    func applicationWillTerminate(_ application: UIApplication) {
//        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    }
//
//    func loadState(){
//        // loads OIDAuthState from NSUSerDefaults
//        guard let archivedAuthState = UserDefaults.standard.object(forKey: kAppAuthExampleAuthStateKey) as? Data else{
//            return
//        }
//        guard let authState = NSKeyedUnarchiver.unarchiveObject(with: archivedAuthState) as? OIDAuthState else{
//            return
//        }
//        self.authState = authState;
//        //assignAuthState(authState)
//    }
//
//    func loadUserInfo(){
//        // loads OIDAuthState from NSUSerDefaults
//        guard let archivedUserInfo = UserDefaults.standard.object(forKey: kAppAuthExampleUserInfoKey) as? Data else{
//            return
//        }
//        guard let userInfo = NSKeyedUnarchiver.unarchiveObject(with: archivedUserInfo)  else{
//            return
//        }
//        self.jsonDictionaryUserInfo = userInfo;
//
//    }
//
//
//}
//
//
//
//
